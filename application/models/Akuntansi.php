<?php

/**
 * Created by PhpStorm.
 * User: gradindesign4
 * Date: 08/02/2017
 * Time: 13.52
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Akuntansi extends CI_Model {
    private $user;
    public function __construct()
    {
        parent::__construct();
        $this->user=$this->session->userdata('user');
    }
    public function save_jurnal(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $this->db->where('type','0A');
        $increment=$this->db->get('akuntansi.jurnal')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/JM/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $this->db->insert('akuntansi.jurnal',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'type'=>'0A',
                'mk'=>0,
                'note'=>$form['note'],
                'status'=>true,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            )
        );
        $no=1;
        foreach ($form['items'] as $item){
            $this->db->insert('akuntansi.jurnald',
                array(
                    'id'=>$id,
                    'no'=>$no++,
                    'coa'=>$item['coa'],
                    'jumlah'=>$item['jumlah'],
                    'posisi'=>$item['dk'],
                    'note'=>$item['note'],
                )
            );
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function save_jurnal_kbg(){
        return;
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $this->db->where('type',0);
        $increment=$this->db->get('akuntansi.jurnal')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/JM/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $this->db->insert('akuntansi.jurnal',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'type'=>'0A',
                'mk'=>0,
                'note'=>$form['note'],
                'status'=>true,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            )
        );
        $no=1;
        foreach ($form['items'] as $item){
            $this->db->insert('akuntansi.jurnald',
                array(
                    'id'=>$id,
                    'no'=>$no++,
                    'coa'=>$item['coa'],
                    'jumlah'=>$item['jumlah'],
                    'posisi'=>$item['dk'],
                    'note'=>$item['note'],
                )
            );
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function set_coa_config($coa=array()){
        $this->db->trans_begin();
        foreach ($coa as $i) {
            $v=explode(';',$i);
            if(isset($v[2])){
                $this->db->where('id',$v[0]);
                $this->db->where('no',$v[1]);
                $this->db->update('akuntansi.configd',array('coa'=>$v[2]));
            }else{
                $this->db->where('detail',false);
                $this->db->where('id',$v[0]);
                $this->db->update('akuntansi.config',array('coa'=>$v[1]));
            }
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function get_coa_option($coa=null){
        $this->db->order_by('k1,k2,k3,k4','asc');
        $this->db->where('jurnal',true);
        $this->db->where('status',true);
        $this->db->where('deleted',false);
        if(isset($coa)){
            $option[0]=array();
            $option[1]=array();
            $option[0][]='k1';
            $option[1][]=$coa['k1'];
            if($coa['level']>1){
                $option[0][]='k2';
                $option[1][]=$coa['k2'];
                if($coa['level']>2){
                    $option[0][]='k3';
                    $option[1][]=$coa['k3'];
                    if($coa['level']>3){
                        $option[0][]='k4';
                        $option[1][]=$coa['k4'];
                    }
                }
            }
            $this->db->where("(".implode('||',$option[0]).") <> ",implode('',$option[1]));
        }
        $this->db->select("coa.*,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
        return $this->db->get('akuntansi.coa')->result_array();
    }
    public function get_coa($option){
        if(is_array($option)) {
            $this->db->where('configd.id',$option[0]);
            $this->db->where('configd.no',$option[1]);
            $this->db->join('akuntansi.configd','configd.coa=coa.id');
        }else{
            switch($option){
                case 'kas':
                    $this->db->where('config.transaksi','kas');
                    break;
                case 'bank':
                    $this->db->where('config.transaksi','bank');
                    break;
                case 'giro':
                    $this->db->where('config.transaksi','giro');
                    break;
            }
            $this->db->join('akuntansi.config','config.coa=coa.id');
        }
        $coa=$this->db->get('akuntansi.coa')->row_array();
        if(!isset($coa['level'])){
            return array(array(),array());
        }
        $this->db->order_by('k1,k2,k3,k4','asc');
        $this->db->where('k1',$coa['k1']);
        if($coa['level']>1){
            $this->db->where('k2',$coa['k2']);
            if($coa['level']>2){
                $this->db->where('k3',$coa['k3']);
                if($coa['level']>3){
                    $this->db->where('k4',$coa['k4']);
                }
            }
        }
        $this->db->where('jurnal',true);
        $this->db->where('status',true);
        $this->db->where('deleted',false);
        $this->db->select("coa.*,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
        return array($coa,$this->db->get('akuntansi.coa')->result_array());
    }
    public function get_coa_payment(){
        $options=array('b'=>'kas','c'=>'bank','d'=>'giro');
        $result=array();
        foreach ($options as $i=>$o){
            $result[$i]=$this->get_coa($o);
        }
        return $result;
    }
    public function get_coa_umk($status,$payment=true){
        if($payment){
            $result=$this->get_coa_payment();
        }
        switch ($status){
            case 'pembelian':
                $result['']=$this->get_coa(array('edab64d6-88f0-4cf6-8258-9a564df9c223','1'));
                break;
            case 'penjualan':
                $result['']=$this->get_coa(array('feca075a-4903-4978-9a51-6ccf090dc437','1'));
                break;
        }
        return $result;
    }
    public function set_coa_status($id){
        $data=$this->db->query("UPDATE \"akuntansi\".\"coa\" set status= NOT status WHERE id = ? RETURNING status",array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function add_coa($data){
        $this->db->where('id',$data['parent']);
        $parent=$this->db->get('akuntansi.coa')->row_array();

        $this->db->where('k'.($parent['level']+1),$data['kode']);
        $this->db->where('level',($parent['level']+1));
        $this->db->where('k1', $parent['k1']);
        if($parent['level']>1) {
            $this->db->where('k2', $parent['k2']);
            if ($parent['level'] > 2) {
                $this->db->where('k3', $parent['k3']);
            }
        }
        $exist=$this->db->get('akuntansi.coa')->num_rows();
        if($exist>0){
            return false;
        }else{
            $this->load->library('uuid');
            $update=array(
                'id'=>$this->uuid->v4(),
                'k1'=>$parent['k1'],
                'k2'=>$parent['k2'],
                'k3'=>$parent['k3'],
                'k4'=>$parent['k4'],
                'nama'=>$data['nama'],
                'dk'=>$data['dk'],
                'level'=>($parent['level']+1),
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            );
            $update['k'.$update['level']]=$data['kode'];
            return $this->db->insert('akuntansi.coa',$update);
        }
    }
    public function update_coa($data){
        $this->db->where('id',$data['id']);
        $coa=$this->db->get('akuntansi.coa')->row_array();

        $this->db->where('k'.$coa['level'],$data['kode']);
        $this->db->where('id <> ',$coa['id']);
        $this->db->where('level',$coa['level']);
        if($data['level']>1) {
            $this->db->where('k1', $coa['k1']);
            if ($data['level'] > 2) {
                $this->db->where('k2', $coa['k2']);
                if ($data['level'] > 3) {
                    $this->db->where('k3', $coa['k3']);
                }
            }
        }
        $exist=$this->db->get('akuntansi.coa')->num_rows();
        if($exist>0){
            return false;
        }else{
            $update=array(
                'k'.$coa['level']=>$data['kode'],
                'nama'=>$data['nama'],
                'dk'=>$data['dk'],
                'modified_by'=>$this->user['id'],
                'modified_at'=>date('Y-m-d H:i:s+07'),
            );
            $this->db->trans_begin();
            $this->db->where('id',$data['id']);
            $this->db->update('akuntansi.coa',$update);
            if($data['level']<4){
                unset($update['nama']);
                $this->db->where('k1',$coa['k1']);
                if($data['level']>1){
                    $this->db->where('k2',$coa['k2']);
                    if($data['level']>2){
                        $this->db->where('k3',$coa['k3']);
                    }
                }
                if(!isset($data['recursive']) || @$data['recursive']!=1){
                    unset($update['dk']);
                }
                $this->db->update('akuntansi.coa',$update);
            }
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }
    public function set_coa_delete($id){
        $this->db->where('id',$id);
        $coa=$this->db->get('akuntansi.coa')->row_array();
        if($coa['level']<>4){
            $this->db->where('deleted',false);
            $this->db->where('k1',$coa['k1']);
            if($coa['level']>1){
                $this->db->where('k2',$coa['k2']);
                if($coa['level']>2){
                    $this->db->where('k3',$coa['k3']);
                }
            }
            $child=$this->db->get('akuntansi.coa')->num_rows();
            $status=$child>1?false:true;
        }else{
            $status=true;
        }
        if($status){
            $this->db->where('deleted',false);
            $this->db->where('id',$id);
            $this->db->update('akuntansi.coa',array(
                'modified_by'=>$this->user['id'],
                'modified_at'=>date('Y-m-d H:i:s+07'),
                'deleted'=>true,
            ));
            $data['status']=200;
            $data['message']="Delete [".format_coa("$coa[k1].$coa[k2].$coa[k3].$coa[k4]")."] $coa[nama] success :)";
        }else{
            $data['status']=403;
            $data['message']="Delete [".format_coa("$coa[k1].$coa[k2].$coa[k3].$coa[k4]")."] $coa[nama] failed :(";
        }
        return json_encode($data);
    }
}