<?php

/**
 * Created by PhpStorm.
 * User: erick
 * Date: 7/23/17
 * Time: 9:39 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Audit extends CI_Model
{
    private $user;

    public function __construct()
    {
        parent::__construct();
        $this->user = $this->session->userdata('user');
    }

    public function meeting($type, $input)
    {
        if(@$input['id_meeting']){//edit
            $this->db->where('id', $input['id_meeting']);
            $sebelumnya = $this->db->get('interpro.meeting')->row_array();
            if(!isset($sebelumnya['id'])){
                return array('error', 'Data realisasi gagal tersimpan');
            }

            $kk = json_decode($sebelumnya['file'], TRUE);
            if(!is_array($kk)){
                $kk=array();
            }
            $input['isian']=$kk;
            for ($i = 0; $i < count($_FILES['file']['name']); $i++) :
                $_FILES['userfile']['name']     = $_FILES['file']['name'][$i];
                $_FILES['userfile']['type']     = $_FILES['file']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                $_FILES['userfile']['error']    = $_FILES['file']['error'][$i];
                $_FILES['userfile']['size']     = $_FILES['file']['size'][$i];

                $config['upload_path'] = './img/';
                $config['allowed_types'] = '*';
                $config['max_size'] = 1024 * 8;
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('userfile')) {
                    return array('danger', $this->upload->display_errors());
                } else {
                    $file = $this->upload->data();
                    $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);
                }
                unset($this->upload);
                unset($_FILES['userfile']);
            endfor;

            if(count($input['isian'])>0) {
                $this->db->trans_begin();
                $this->db->where('id', $id);
                $this->db->where('tao', $input['id']);
                $update = array(
                    'realisasi_by' => $input['realisasi_by'],
                    'realisasi_date' => format_waktu($input['realisasi_date'], true),
                    'file' => json_encode($input['isian']),
                    'progres' => 1,
                );
                if ($input['kesimpulan']) {
                    $update['kesimpulan'] = $input['kesimpulan'];
                }
                $this->db->update('interpro.perencanaan_detail', $update);
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return array('danger', 'Data gagal tersimpan');
                } else {
                    $this->db->trans_commit();
                    return array('success', 'Data berhasil tersimpan');
                }
            }
            else{
                return array('danger', 'Berkas belum terpilih');
            }
        }
        else{
            //new
//echo $_FILES['file']['size'][0];die();
            if($_FILES['file']['size'][0]>0) {
                for ($i = 0; $i < count($_FILES['file']['name']); $i++) :
                    $_FILES['userfile']['name'] = $_FILES['file']['name'][$i];
                    $_FILES['userfile']['type'] = $_FILES['file']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                    $_FILES['userfile']['error'] = $_FILES['file']['error'][$i];
                    $_FILES['userfile']['size'] = $_FILES['file']['size'][$i];

                    $config['upload_path'] = './img/';
                    $config['allowed_types'] = '*';
                    $config['max_size'] = 1024 * 8;
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('userfile')) {
                        return array('danger', $this->upload->display_errors());
                    } else {
                        $file = $this->upload->data();
                        $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);
                    }
                    unset($this->upload);
                    unset($_FILES['userfile']);
                endfor;
            }
            if(count(@$input['isian'])>0) {

                $user = $this->session->userdata('user');
                $this->load->library('uuid');
                $id = $this->uuid->v4();
                $this->db->insert('interpro.meeting', array(
                    'id' => $id,
                    'no_agenda' => $input['no'],
                    'judul' => $input['judul'],
                    'deskripsi' => $input['deskripsi'],
                    'status' => true,
                    'closed' => false,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $user['id'],
                    'file' => json_encode($input['isian']),
                    'type' => $type,
                ));
            }else{

                $user = $this->session->userdata('user');
                $this->load->library('uuid');
                $id = $this->uuid->v4();
                $this->db->insert('interpro.meeting', array(
                    'id' => $id,
                    'no_agenda' => $input['no'],
                    'judul' => $input['judul'],
                    'deskripsi' => $input['deskripsi'],
                    'status' => true,
                    'closed' => false,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $user['id'],
                    'type'=>$type,
                ));
            }
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('danger', "Data gagal tersimpan");
            } else {
                $this->db->trans_commit();
                return array('success', "Data berhasil tersimpan");
            }
        }

    }
    public function meeting_r($type,$id, $input)
    {
        //new
        if($_FILES['file']['size'][0]>0){
            for ($i = 0; $i < count($_FILES['file']['name']); $i++) :
                $_FILES['userfile']['name']     = $_FILES['file']['name'][$i];
                $_FILES['userfile']['type']     = $_FILES['file']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                $_FILES['userfile']['error']    = $_FILES['file']['error'][$i];
                $_FILES['userfile']['size']     = $_FILES['file']['size'][$i];

                $config['upload_path'] = './img/';
                $config['allowed_types'] = '*';
                $config['max_size'] = 1024 * 8;
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('userfile')) {
                    return array('danger', $this->upload->display_errors());
                } else {
                    $file = $this->upload->data();
                    $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);
                }
                unset($this->upload);
                unset($_FILES['userfile']);
            endfor;
        }
        if(count(@$input['isian'])>0) {

            $this->load->library('uuid');
            $id2 = $this->uuid->v4();
            $user=$this->session->userdata('user');
            $this->db->insert('interpro.meeting_d',array(
                'id'=>$id,
                'id_reply'=>$id2,
                'isi'=>$input['deskripsi'],
                'created_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$user['id'],
                'reply_by'=>$user['id'],
                'file' => json_encode($input['isian']),
            ));
        }
        else{
            $this->load->library('uuid');
            $id2 = $this->uuid->v4();
            $user=$this->session->userdata('user');
            $this->db->insert('interpro.meeting_d',array(
                'id'=>$id,
                'id_reply'=>$id2,
                'isi'=>$input['deskripsi'],
                'created_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$user['id'],
                'reply_by'=>$user['id'],
            ));
        }
        $this->db->where('id',$id);
        $meet=$this->db->get('interpro.meeting')->row_array();
        $id_us='';
        if($meet['type']=='k'){
            if($meet['created_by']==$user['id']){
                $id_us=$meet['tim'];
            }else{
                $id_us=$meet['created_by'];
            }
            $this->load->library('uuid');
            $id2 = $this->uuid->v4();
            $this->db->insert('support.notif',array(
                'id'=>$id2,
                'id_user'=>$id_us,
                'message'=>"Ada yang menanggapi di '$meet[judul]'",
                'reff_id1'=>$id,
                'reff_id2'=>$id,
                'url'=>"https://e-meeting.emjes.id/internalproses/konsultasi?reply=$id",
                'created_at'=>date('Y-m-d H:i:s'),
            ));
        }
        else{
            $tim=array();
            $js=json_decode($meet['tim'], true);
            $js[]=$meet['created_by'];
            unset( $js[array_search( $user['id'], $js )] );
            $jdl=($type=='m'?'meeting':'diskusi');
            foreach ($js as $t){
                $this->load->library('uuid');
                $id2 = $this->uuid->v4();
                $this->db->insert('support.notif',array(
                    'id'=>$id2,
                    'id_user'=>$t,
                    'message'=>"Ada yang menanggapi di '$meet[judul]'",
                    'reff_id1'=>$id,
                    'reff_id2'=>$id,
                    'url'=>"https://e-meeting.emjes.id/internalproses/$jdl?reply=$id",
                    'created_at'=>date('Y-m-d H:i:s'),
                ));
            }
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return array('danger', "Data $jdl gagal tersimpan");
        } else {
            $this->db->trans_commit();
            return array('success', "Data $jdl berhasil tersimpan");
        }

    }
    public function meeting_r2($type,$id, $input)
    {
        //new
        if($_FILES['file']['size'][0]>0){
            for ($i = 0; $i < count($_FILES['file']['name']); $i++) :
                $_FILES['userfile']['name']     = $_FILES['file']['name'][$i];
                $_FILES['userfile']['type']     = $_FILES['file']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                $_FILES['userfile']['error']    = $_FILES['file']['error'][$i];
                $_FILES['userfile']['size']     = $_FILES['file']['size'][$i];

                $config['upload_path'] = './img/';
                $config['allowed_types'] = '*';
                $config['max_size'] = 1024 * 8;
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('userfile')) {
                    return array('danger', $this->upload->display_errors());
                } else {
                    $file = $this->upload->data();
                    $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);
                }
                unset($this->upload);
                unset($_FILES['userfile']);
            endfor;
        }
        if(count(@$input['isian'])>0) {

            $this->load->library('uuid');
            $id2 = $this->uuid->v4();
            $user=$this->session->userdata('user');
            $this->db->insert('interpro.meeting_d',array(
                'id'=>$id,
                'id_reply'=>$id2,
                'refid'=>$input['m_id_r'],
                'lvl'=>2,
                'isi'=>$input['deskripsi'],
                'created_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$user['id'],
                'reply_by'=>$user['id'],
                'file' => json_encode($input['isian']),
            ));
        }
        else{
            $this->load->library('uuid');
            $id2 = $this->uuid->v4();
            $user=$this->session->userdata('user');
            $this->db->insert('interpro.meeting_d',array(
                'id'=>$id,
                'id_reply'=>$id2,
                'refid'=>$input['m_id_r'],
                'lvl'=>2,
                'isi'=>$input['deskripsi'],
                'created_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$user['id'],
                'reply_by'=>$user['id'],
            ));
        }
        $this->db->where('id',$id);
        $meet=$this->db->get('interpro.meeting')->row_array();
        $id_us='';
        if($meet['type']=='k'){
            if($meet['created_by']==$user['id']){
                $id_us=$meet['tim'];
            }else{
                $id_us=$meet['created_by'];
            }
            $this->load->library('uuid');
            $id2 = $this->uuid->v4();
            $this->db->insert('support.notif',array(
                'id'=>$id2,
                'id_user'=>$id_us,
                'message'=>"Ada yang menanggapi di '$meet[judul]'",
                'reff_id1'=>$id,
                'reff_id2'=>$id,
                'url'=>"https://e-meeting.emjes.id/internalproses/konsultasi?reply=$id",
                'created_at'=>date('Y-m-d H:i:s'),
            ));
        }
        else{
            $tim=array();
            $js=json_decode($meet['tim'], true);
            $js[]=$meet['created_by'];
            unset( $js[array_search( $user['id'], $js )] );
            $jdl=($type=='m'?'meeting':'diskusi');
            foreach ($js as $t){
                $this->load->library('uuid');
                $id2 = $this->uuid->v4();
                $this->db->insert('support.notif',array(
                    'id'=>$id2,
                    'id_user'=>$t,
                    'message'=>"Ada yang menanggapi di '$meet[judul]'",
                    'reff_id1'=>$id,
                    'reff_id2'=>$id,
                    'url'=>"https://e-meeting.emjes.id/internalproses/$jdl?reply=$id",
                    'created_at'=>date('Y-m-d H:i:s'),
                ));
            }
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return array('danger', "Data $jdl gagal tersimpan");
        } else {
            $this->db->trans_commit();
            return array('success', "Data $jdl berhasil tersimpan");
        }

    }
    public function meeting_n($type,$id, $input)
    {
        //new
        if($_FILES['file']['size'][0]>0){
            for ($i = 0; $i < count($_FILES['file']['name']); $i++) :
                $_FILES['userfile']['name']     = $_FILES['file']['name'][$i];
                $_FILES['userfile']['type']     = $_FILES['file']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                $_FILES['userfile']['error']    = $_FILES['file']['error'][$i];
                $_FILES['userfile']['size']     = $_FILES['file']['size'][$i];

                $config['upload_path'] = './img/';
                $config['allowed_types'] = '*';
                $config['max_size'] = 1024 * 8;
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('userfile')) {
                    return array('danger', $this->upload->display_errors());
                } else {
                    $file = $this->upload->data();
                    $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);
                }
                unset($this->upload);
                unset($_FILES['userfile']);
            endfor;
        }
        if(count(@$input['isian'])>0) {

            $this->load->library('uuid');
            $id2 = $this->uuid->v4();
            $user=$this->session->userdata('user');
            $this->db->insert('interpro.meeting_n',array(
                'id'=>$id2,
                'id_rapat'=>$id,
                'isi'=>$input['deskripsi'],
                'created_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$user['id'],
                'file' => json_encode($input['isian']),
            ));
            $this->db->where('id',$id);
            $this->db->update('interpro.meeting',array(
                'tl'=>true
            ));
        }
        else{
            $this->load->library('uuid');
            $id2 = $this->uuid->v4();
            $user=$this->session->userdata('user');
            $this->db->insert('interpro.meeting_n',array(
                'id'=>$id2,
                'id_rapat'=>$id,
                'isi'=>$input['deskripsi'],
                'created_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$user['id'],
            ));
            $this->db->where('id',$id);
            $this->db->update('interpro.meeting',array(
                'tl'=>true
            ));
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return array('danger', "Data gagal tersimpan");
        } else {
            $this->db->trans_commit();
            return array('success', "Data berhasil tersimpan");
        }

    }
    public function meeting_notulen($type,$id, $input)
    {
        //new
        if($_FILES['file']['size'][0]>0){
            for ($i = 0; $i < count($_FILES['file']['name']); $i++) :
                $_FILES['userfile']['name']     = $_FILES['file']['name'][$i];
                $_FILES['userfile']['type']     = $_FILES['file']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                $_FILES['userfile']['error']    = $_FILES['file']['error'][$i];
                $_FILES['userfile']['size']     = $_FILES['file']['size'][$i];

                $config['upload_path'] = './img/';
                $config['allowed_types'] = '*';
                $config['max_size'] = 1024 * 8;
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('userfile')) {
                    return array('danger', $this->upload->display_errors());
                } else {
                    $file = $this->upload->data();
                    $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);
                }
                unset($this->upload);
                unset($_FILES['userfile']);
            endfor;
        }
        if(count(@$input['isian'])>0) {

            $this->db->where('id',$input['m_id']);
            $this->db->update('interpro.meeting',array(
                'notulen_note'=>$input['kesimpulan'],
                'notulen'=>true,
                'notulen_file' => json_encode($input['isian']),
            ));
        }
        else{

            $this->db->where('id',$input['m_id']);
            $this->db->update('interpro.meeting',array(
                'notulen_note'=>$input['kesimpulan'],
                'notulen'=>true,
            ));
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return array('danger', "Data gagal tersimpan");
        } else {
            $this->db->trans_commit();
            return array('success', "Data berhasil tersimpan");
        }

    }
    public function update_meeting($type,$id, $input)
    {
        $this->db->where('id', $input['id_meeting']);
        $sebelumnya = $this->db->get('interpro.meeting')->row_array();
        if(!isset($sebelumnya['id'])){
            return array('error', "Data gagal tersimpan");
        }

        $kk = json_decode($sebelumnya['file'], TRUE);
        if(!is_array($kk)){
            $kk=array();
        }
        $input['isian']=$kk;
        if(count($_FILES['file']['name']>0)){
            for ($i = 0; $i < count($_FILES['file']['name']); $i++) :
                $_FILES['userfile']['name']     = $_FILES['file']['name'][$i];
                $_FILES['userfile']['type']     = $_FILES['file']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                $_FILES['userfile']['error']    = $_FILES['file']['error'][$i];
                $_FILES['userfile']['size']     = $_FILES['file']['size'][$i];

                $config['upload_path'] = './img/';
                $config['allowed_types'] = '*';
                $config['max_size'] = 1024 * 8;
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('userfile')) {
                    return array('danger', $this->upload->display_errors());
                } else {
                    $file = $this->upload->data();
                    $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);
                }
                unset($this->upload);
                unset($_FILES['userfile']);
            endfor;
        }
        $date=explode('/',$input['tgl']);
        $tgl=$date[2].'-'.$date[1].'-'.$date[0];
        $date2=explode('/',$input['tgl_akhir']);
        $tgl2=$date2[2].'-'.$date2[1].'-'.$date2[0];
        $this->db->trans_begin();


        if($type=='k'){
            $tim=$input['tim'];
        }else{
            $tim=json_encode($input['tim']);
        }
        if(count($input['isian'])>0) {
            $this->db->where('id', $id);
            $this->db->update('interpro.meeting',array(
                'id'=>$id,
                'tema'=>$input['tema'],
                'judul'=>$input['judul'],
                'tim'=>$tim,
                'tgl'=>$tgl,
                'tgl_berakhir'=>$tgl2,
                'deskripsi'=>$input['deskripsi'],
                'file' => json_encode($input['isian']),
                'type' => $type,
            ));
        }
        else{
            $this->db->where('id', $id);
            $this->db->update('interpro.meeting',array(
                'id'=>$id,
                'tema'=>$input['tema'],
                'judul'=>$input['judul'],
                'tim'=>$tim,
                'tgl'=>$tgl,
                'tgl_berakhir'=>$tgl2,
                'deskripsi'=>$input['deskripsi'],
            ));
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return array('danger', "Data $input[judul] gagal tersimpan");
        } else {
            $this->db->trans_commit();
            return array('success', "Data $input[judul] berhasil tersimpan");
        }

    }
    public function hapus_perencanaan($id)
    {
        $this->db->where('id', $id);
        $this->db->where('draft', true);
        $data = $this->db->get('interpro.perencanaan')->row_array();
        if (!isset($data['id'])) {
            return false;
        } else {
            $this->db->trans_begin();
            $this->db->where('id', $id);
            $this->db->delete('interpro.perencanaan_detail');
            $this->db->where('id', $id);
            $this->db->delete('interpro.perencanaan_pedoman');
            $this->db->where('id', $id);
            $this->db->delete('interpro.perencanaan_aturan');
            $this->db->where('id', $id);
            $this->db->delete('interpro.perencanaan_tujuan');
            $this->db->where('id', $id);
            $this->db->delete('interpro.perencanaan_sasaran');
            $this->db->where('id', $id);
            $this->db->delete('interpro.perencanaan');
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }
    function array_to_object($array) {
        return (object) $array;
    }
    public function perencanaan()
    {
        error_reporting(0);
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form = array();
        foreach ($data['form'] as $i => $d) {
            if (strpos($d['name'], '[]') == false) {
                $column = str_replace(array('header[', ']'), '', $d['name']);
                $form['header'][$column] = $d['value'];
            } else {
                $column = str_replace('[]', '', $d['name']);
                $form['detail'][$column][] = $d['value'];
            }
        }
        $form['detail']['aturan'] = $data['aturan'];
        $form['detail']['pedoman'] = $data['pedoman'];
        $form['detail']['tao'] = $data['tao'];

        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $form['header']['start_date'] = format_waktu($form['header']['start_date'], true);
        $form['header']['end_date'] = format_waktu($form['header']['end_date'], true);
        $form['header']['spt_date'] = format_waktu($form['header']['spt_date'], true);
        $form['header']['id'] = $id;
        $form['header']['jml_hari'] = blank_is_null($form['header']['jml_hari']);
        $form['header']['periode'] = blank_is_null($form['header']['periode']);
        $form['header']['jenis'] = blank_is_null($form['header']['jenis']);
        $form['header']['satker'] = blank_is_null($form['header']['satker']);
        $form['header']['ketuatim'] = blank_is_null($form['header']['ketuatim']);
        $form['header']['irban'] = blank_is_null($form['header']['irban']);
        $form['header']['dalnis'] = blank_is_null($form['header']['dalnis']);
        $form['header']['tim'] = json_encode(is_array(@$data['tim']) ? $data['tim'] : []);
        $form['header']['created_by'] = $this->user['id'];
        $form['header']['created_at'] = date('Y-m-d H:i:s+07');
        $form['header']['closed'] = false;
        $form['header']['status'] = true;
        $form['header']['draft'] = $data['draft'] == '1' ? true : false;
        $this->db->trans_begin();
        $this->db->insert('interpro.perencanaan', $form['header']);
        if (is_array(@$form['detail']['sasaran'])) {
            $sasaran = array();
            foreach ($form['detail']['sasaran'] as $i => $f) {
                if (strlen($f) < 30) {
                    continue;
                }
                $sasaran[] = $f;
                $this->db->insert('interpro.perencanaan_sasaran', array(
                    'id' => $id,
                    'sasaran' => $f
                ));
            }
        }
        if (is_array(@$form['detail']['tujuan']))
            foreach ($form['detail']['tujuan'] as $i => $f) {
                if (strlen($f) < 30) {
                    continue;
                }
                $this->db->insert('interpro.perencanaan_tujuan', array(
                    'id' => $id,
                    'tujuan' => $f
                ));
            }
        if (is_array(@$form['detail']['aturan']))
            foreach ($form['detail']['aturan'] as $i => $f) {
                if (@$f['checked']
                    && $form['header']['jenis'] == $f['jenis']
                    && in_array($f['sasaran'], $sasaran)
                ) {
                    $this->db->insert('interpro.perencanaan_aturan', array(
                        'id' => $id,
                        'aturan' => $f
                    ));
                }
            }
        if (is_array(@$form['detail']['pedoman']))
            foreach ($form['detail']['pedoman'] as $i => $f) {
                if (@$f['checked']
                    && $form['header']['jenis'] == $f['jenis']
                    && in_array($f['sasaran'], $sasaran)
                ) {
                    $this->db->insert('interpro.perencanaan_pedoman', array(
                        'id' => $id,
                        'pedoman' => $f
                    ));
                }
            }
        if (is_array(@$form['detail']['tao']))
            foreach ($form['detail']['tao'] as $i => $f) {
                if ($form['header']['jenis'] == $f['jenis']
                    && in_array($f['sasaran'], $sasaran)
                    && (is_array($f['pelaksana']) && count($f['pelaksana'])>0)
                    && is_not_empty_string($f['pelaksanaan'])
                ) {
//                    echo json_encode($f['pelaksana']);
//                    $obj = new ArrayObject();
//                    foreach($f['pelaksana'] as $r){
//                        $obj->offsetSet($r);
//                    }
//                    var_dump($obj);
//                    $obj=json_encode($f['pelaksana'],JSON_FORCE_OBJECT);
                    $f['pelaksanaan'] = format_waktu($f['pelaksanaan'], true);
                    for($i=0;$i<count($f['pelaksana']);$i++){
                        $this->db->insert('interpro.perencanaan_detail', array(
                            'id' => $id,
                            'tao' => $f['id'],
                            'jumlah_hari' => $f['jumlah'],
                            'rencana_by' => $f['pelaksana'][$i],
                            'rencana_by2' => $f['pelaksana'][$i],
                            'rencana_date' => $f['pelaksanaan'],
                            'created_by' => $this->user['id'],
                            'created_at' => date('Y-m-d H:i:s+07'),
                            'status' => true
                        ));
                    }

                }
            }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
//            $this->db->trans_rollback();
            $this->db->trans_commit();
            return array('success', "Nomor SPT: " . $form['header']['spt_no'] . " tersimpan");
        }
    }
    public function perencanaan2()
    {
        error_reporting(0);
        $postdata = file_get_contents("php://input");
        // print_r("<pre>");
        // print_r($_POST);
        // die();


        // $data = $this->input->get_post();
        $data = $_POST;
        $form = array();
//        foreach ($data['form'] as $i => $d) {
//            if (strpos($d['name'], '[]') == false) {
//                $column = str_replace(array('header[', ']'), '', $d['name']);
//                $form['header'][$column] = $d['value'];
//            }
//        }
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $form['header']['start_date'] = format_waktu($this->input->get_post('header[start_date]'), true);
        $form['header']['end_date'] = format_waktu($this->input->get_post('header[end_date]'),true);
        $form['header']['spt_date'] = format_waktu($this->input->get_post('header[spt_date]'), true);
        $form['header']['id'] = $id;
        $form['header']['spt_no'] = blank_is_null($this->input->get_post('header[spt_no]'));
        $form['header']['periode'] = blank_is_null($this->input->get_post('header[periode]'));
        $form['header']['pkpt_no'] = blank_is_null($this->input->get_post('header[pkpt_no]'));
        $form['header']['jml_hari'] = blank_is_null($this->input->get_post('header[jml_hari]'));
        $jml = $this->input->get_post('header[jml]')>0?$this->input->get_post('header[jml]'):0;
        $form['header']['periode'] = blank_is_null($this->input->get_post('header[periode]'));
        $form['header']['jenis'] = blank_is_null($this->input->get_post('header[jenis]'));
        $form['header']['satker'] = blank_is_null($this->input->get_post('header[satker]'));
        $form['header']['judul'] = blank_is_null($this->input->get_post('header[judul]'));
        $form['tujuan'] = ($this->input->get_post('tujuan'));
        $form['sasaran'] = ($this->input->get_post('sasaran'));
        $form['header']['ketuatim'] = blank_is_null($this->input->get_post('header[ketuatim]'));
        $form['header']['irban'] = blank_is_null($this->input->get_post('header[irban]'));
        $form['header']['dalnis'] = blank_is_null($this->input->get_post('header[dalnis]'));
        $tims=json_decode($this->input->get_post('tim'),true);
        $arr_tim=array();
        foreach ($tims as $t){
        $arr_tim[]=$t['id'];
        }
        $form['header']['tim'] = json_encode($arr_tim);

        $form['header']['satker_p'] = json_encode(@$this->input->get_post('header[satker_p]'));
        $form['header']['created_by'] = $this->user['id'];
        $form['header']['created_at'] = date('Y-m-d H:i:s+07');
        $form['header']['closed'] = false;
        $form['header']['status'] = true;
        $form['header']['draft'] = $data['draft'] == '1' ? true : false;
//        echo json_encode($form['header']);die();
        $this->db->trans_begin();
        $this->db->insert('interpro.perencanaan', $form['header']);
        //sini belum
        for($i=0;$i<=$jml;$i++){
//            echo json_encode($this->input->get_post("jumlah$i"));
//            die();
            if($this->input->get_post("jumlah$i")!=''){
                $start = explode('/',$this->input->get_post("pelaksanaan$i"));
                $tgl_s = $start['2'] . '-' . $start['1'] . '-' . $start[0];
                $this->db->insert('interpro.perencanaan_detail', array(
                    'id' => $id,
                    'tao' => $this->input->get_post("tao$i"),
                    'jumlah_hari' => $this->input->get_post("jumlah$i"),
                    'rencana_by' => $this->input->get_post("pelaksana$i")[0],
                    'rencana_by2' => json_encode($this->input->get_post("pelaksana$i")),
//                    'rencana_by3' => json_encode($this->input->get_post("pelaksana$i")),
                    'rencana_date' => $tgl_s,
                    'created_by' => $this->user['id'],
                    'created_at' => date('Y-m-d H:i:s+07'),
                    'status' => true
                ));
            }
        }
//        echo json_encode($form['sasaran']);die();
        if (is_array(@$form['sasaran'])) {
            $sasaran = array();
            foreach ($form['sasaran'] as $i => $f) {
                if (strlen($f) < 30) {
                    continue;
                }
                $sasaran[] = $f;
                $this->db->insert('interpro.perencanaan_sasaran', array(
                    'id' => $id,
                    'sasaran' => $f
                ));
            }
        }
        if (is_array(@$form['tujuan']))
            foreach ($form['tujuan'] as $i => $f) {
                if (strlen($f) < 30) {
                    continue;
                }
                $this->db->insert('interpro.perencanaan_tujuan', array(
                    'id' => $id,
                    'tujuan' => $f
                ));
            }
//        if (is_array(@$form['detail']['aturan']))
//            foreach ($form['detail']['aturan'] as $i => $f) {
//                if (@$f['checked']
//                    && $form['header']['jenis'] == $f['jenis']
//                    && in_array($f['sasaran'], $sasaran)
//                ) {
//                    $this->db->insert('interpro.perencanaan_aturan', array(
//                        'id' => $id,
//                        'aturan' => $f
//                    ));
//                }
//            }
//        if (is_array(@$form['detail']['pedoman']))
//            foreach ($form['detail']['pedoman'] as $i => $f) {
//                if (@$f['checked']
//                    && $form['header']['jenis'] == $f['jenis']
//                    && in_array($f['sasaran'], $sasaran)
//                ) {
//                    $this->db->insert('interpro.perencanaan_pedoman', array(
//                        'id' => $id,
//                        'pedoman' => $f
//                    ));
//                }
//            }
//        if (is_array(@$form['detail']['tao']))
//            foreach ($form['detail']['tao'] as $i => $f) {
//                if ($form['header']['jenis'] == $f['jenis']
//                    && in_array($f['sasaran'], $sasaran)
//                    && (is_array($f['pelaksana']) && count($f['pelaksana'])>0)
//                    && is_not_empty_string($f['pelaksanaan'])
//                ) {
////                    echo json_encode($f['pelaksana']);
////                    $obj = new ArrayObject();
////                    foreach($f['pelaksana'] as $r){
////                        $obj->offsetSet($r);
////                    }
////                    var_dump($obj);
////                    $obj=json_encode($f['pelaksana'],JSON_FORCE_OBJECT);
//                    $f['pelaksanaan'] = format_waktu($f['pelaksanaan'], true);
//                    for($i=0;$i<count($f['pelaksana']);$i++){
//                        $this->db->insert('interpro.perencanaan_detail', array(
//                            'id' => $id,
//                            'tao' => $f['id'],
//                            'jumlah_hari' => $f['jumlah'],
//                            'rencana_by' => $f['pelaksana'][$i],
//                            'rencana_by2' => $f['pelaksana'][$i],
//                            'rencana_date' => $f['pelaksanaan'],
//                            'created_by' => $this->user['id'],
//                            'created_at' => date('Y-m-d H:i:s+07'),
//                            'status' => true
//                        ));
//                    }
//
//                }
//            }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
//            $this->db->trans_rollback();
            $this->db->trans_commit();
            return array('success', "Nomor SPT: " . $form['header']['spt_no'] . " tersimpan");
        }
    }

    public function update_perencanaan($id)
    {
        error_reporting(0);
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form = array();
        foreach ($data['form'] as $i => $d) {
            if (strpos($d['name'], '[]') == false) {
                $column = str_replace(array('header[', ']'), '', $d['name']);
                $form['header'][$column] = $d['value'];
            } else {
                $column = str_replace('[]', '', $d['name']);
                $form['detail'][$column][] = $d['value'];
            }
        }
        $form['detail']['aturan'] = $data['aturan'];
        $form['detail']['pedoman'] = $data['pedoman'];
        $form['detail']['tao'] = $data['tao'];

        $form['header']['start_date'] = format_waktu($form['header']['start_date'], true);
        $form['header']['end_date'] = format_waktu($form['header']['end_date'], true);
        $form['header']['spt_date'] = format_waktu($form['header']['spt_date'], true);
        $form['header']['jenis'] = blank_is_null($form['header']['jenis']);
        $form['header']['jml_hari'] = blank_is_null($form['header']['jml_hari']);
        $form['header']['periode'] = blank_is_null($form['header']['periode']);
        $form['header']['satker'] = blank_is_null($form['header']['satker']);
        $form['header']['ketuatim'] = blank_is_null($form['header']['ketuatim']);
        $form['header']['irban'] = blank_is_null($form['header']['irban']);
        $form['header']['dalnis'] = blank_is_null($form['header']['dalnis']);
        $form['header']['tim'] = json_encode(is_array(@$data['tim']) ? $data['tim'] : []);
        $form['header']['modified_by'] = $this->user['id'];
        $form['header']['modified_at'] = date('Y-m-d H:i:s+07');
        $form['header']['closed'] = false;
        $form['header']['status'] = true;
        $form['header']['draft'] = $data['draft'] == '1' ? true : false;
        $total_hari=$form['header']['total_hari'];
        if (!$form['header']['draft']) {
            $form['header']['proses'] = 1;
        }
        $this->db->where('id', $id);
        $this->db->where('proses <= ', 1);
        $rencana = $this->db->get('interpro.perencanaan')->row_array();
        if (!isset($rencana['id'])) {
            return false;
        }
        $this->db->trans_begin();
        unset($form['header']['total_hari']);
        $this->db->where('id', $id);
        $this->db->update('interpro.perencanaan', $form['header']);

        $this->db->where('id', $id);
        $this->db->delete('interpro.perencanaan_sasaran');

        if (is_array(@$form['detail']['sasaran'])) {
            $sasaran = array();
            foreach ($form['detail']['sasaran'] as $i => $f) {
                if (strlen($f) < 30) {
                    continue;
                }
                $sasaran[] = $f;
                $this->db->insert('interpro.perencanaan_sasaran', array(
                    'id' => $id,
                    'sasaran' => $f
                ));
            }
        }

        $this->db->where('id', $id);
        $this->db->delete('interpro.perencanaan_tujuan');

        if (is_array(@$form['detail']['tujuan']))
            foreach ($form['detail']['tujuan'] as $i => $f) {
                if (strlen($f) < 30) {
                    continue;
                }
                $this->db->insert('interpro.perencanaan_tujuan', array(
                    'id' => $id,
                    'tujuan' => $f
                ));
            }

        $this->db->where('id', $id);
        $this->db->delete('interpro.perencanaan_aturan');

        if (is_array(@$form['detail']['aturan']))
            foreach ($form['detail']['aturan'] as $i => $f) {
                if (@$f['checked']
                    && $form['header']['jenis'] == $f['jenis']
                    && in_array($f['sasaran'], $sasaran)
                ) {
                    $this->db->insert('interpro.perencanaan_aturan', array(
                        'id' => $id,
                        'aturan' => $f
                    ));
                }
            }

        $this->db->where('id', $id);
        $this->db->delete('interpro.perencanaan_pedoman');

        if (is_array(@$form['detail']['pedoman']))
            foreach ($form['detail']['pedoman'] as $i => $f) {
                if (@$f['checked']
                    && $form['header']['jenis'] == $f['jenis']
                    && in_array($f['sasaran'], $sasaran)
                ) {
                    $this->db->insert('interpro.perencanaan_pedoman', array(
                        'id' => $id,
                        'pedoman' => $f
                    ));
                }
            }

        $this->db->where('id', $id);
        $this->db->delete('interpro.perencanaan_detail');

        if (is_array(@$form['detail']['tao']))
            foreach ($form['detail']['tao'] as $i => $f) {
                if ($form['header']['jenis'] == $f['jenis']
                    && in_array($f['sasaran'], $sasaran)
                    && is_not_empty_string($f['pelaksana'])
                    && is_not_empty_string($f['pelaksanaan'])
                ) {
                    $f['pelaksanaan'] = format_waktu($f['pelaksanaan'], true);
                    $this->db->insert('interpro.perencanaan_detail', array(
                        'id' => $id,
                        'tao' => $f['id'],
                        'jumlah_hari' => $f['jumlah'],
                        'rencana_by' => $f['pelaksana'],
                        'rencana_date' => $f['pelaksanaan'],
                        'created_by' => $this->user['id'],
                        'created_at' => date('Y-m-d H:i:s+07'),
                        'status' => true
                    ));
                }
            }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success', "Nomor SPT: " . $form['header']['spt_no'] . " diperbarui");
        }
    }
    public function update_temuan($input)
    {
        $temuan=json_decode($this->input->get_post('temuans'),true);
        $this->db->where('id_rencana', $this->input->get_post('id_test'));
        $rencana = $this->db->get('interpro.tindak_lanjut')->row_array();
        $user=$this->session->userdata('user');
        $this->db->trans_begin();
        if (isset($rencana['id'])) {
            $this->db->where('id', $this->input->get_post('id_test'));
            $a = $this->db->get('interpro.perencanaan')->row_array();
            $this->db->where('id_rencana', $this->input->get_post('id_test'));
            $this->db->update('interpro.tindak_lanjut',array(
                'id_rencana'=>$a['id'],
                'modified_at'=>date('Y-m-d H:i:s'),
                'modified_by'=>$user['id'],
                'judul'=>$a['judul'],
                'spt_no'=>$a['spt_no'],
                'skpd'=>$a['judul'],
                'no_lhp'=>$a['no_laporan'],
                'thn_lhp'=>$a['thn_laporan'],
                'pkpt_no'=>$a['pkpt_no'],
                'spt_date'=>$a['spt_date'],
            ));
            $this->db->where('id', $rencana['id']);
            $this->db->delete('interpro.tindak_lanjutd');

            foreach ($temuan as $t){
                $this->db->insert('interpro.tindak_lanjutd',array(
                    'id'=>$rencana['id'],
                    'kode_temuan'=>$t['id_t'],
                    'kode_rekom'=>$t['id_r'],
                    'negara'=>$t['nk1'],
                    'daerah'=>$t['nk2'],
                    'nd'=>$t['nd'],
                    'tindak_lanjut'=>$t['tl'],
                    'hasil_rekom'=>$t['kode_hr'],
                    'nilai_rekom'=>$t['nilai_r'],
                    'keterangan'=>$t['keterangan'],
                ));
            }
        }
        else{
            $this->load->library('uuid');
            $id = $this->uuid->v4();
            $this->db->where('id', $this->input->get_post('id_test'));
            $a = $this->db->get('interpro.perencanaan')->row_array();
            $this->db->insert('interpro.tindak_lanjut',array(
                'id'=>$id,
                'id_rencana'=>$a['id'],
                'created_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$user['id'],
                'status'=>TRUE,
                'judul'=>$a['judul'],
                'spt_no'=>$a['spt_no'],
                'pkpt_no'=>$a['pkpt_no'],
                'spt_date'=>$a['spt_date'],
//                'skpd'=>$a['judul'],
//                'no_lhp'=>$a['no_laporan'],
//                'thn_lhp'=>$a['thn_laporan'],
            ));
            foreach ($temuan as $t){
                $this->db->insert('interpro.tindak_lanjutd',array(
                    'id'=>$id,
                    'kode_temuan'=>$t['id_t'],
                    'kode_rekom'=>$t['id_r'],
                    'negara'=>$t['nk1'],
                    'daerah'=>$t['nk2'],
                    'nd'=>$t['nd'],
                    'akibat'=>$t['akibat'],
                    'penyebab'=>$t['penyebab'],
                    'kriteria'=>$t['kriteria'],
//                    'kondisi'=>$t['kondisi'],
                    'tujuan_tl'=>$t['tujuan_tl'],
                    'sasaran_tl'=>$t['sasaran_tl'],
                    'judul_tl'=>$t['judul_tl'],
                    'evaluasi'=>$t['evaluasi'],
                    'tanggapan'=>$t['tanggapan'],
                    'ruang'=>$t['ruang'],
                    'obrik'=>$t['obrik'],
                    'tindak_lanjut'=>$t['tindak_lanjut'],
                    'tindak_lanjut_s'=>$t['tindak_lanjut_s'],
                    'hasil_rekom'=>$t['kode_hr'],
                    'nilai_rekom'=>$t['nilai_r'],
                    'keterangan'=>$t['keterangan'],
                ));
            }
    }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->where('id', $this->input->get_post('id_test'));
            $this->db->update('interpro.perencanaan',array(
                'status_tl'=>'t'
            ));
            $this->db->trans_commit();
            return array('success', "Temuan berhasil disimpan");
        }
    }
    public function insert_temuan($input)
    {

        $total_temuan=$input['no_temuan'];
//        echo json_encode($form);die();
        $user=$this->session->userdata('user');
        $this->db->trans_begin();
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $date=explode('/',$input['tgl_lhp']);
        $tgl=$date[2].'-'.$date[1].'-'.$date[0];
        $this->db->insert('interpro.tindak_lanjut',array(
                'id'=>$id,
                'created_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$user['id'],
                'status'=>TRUE,
                'skpd'=>$input['judul_spt'],
                'no_lhp'=>$input['spt_no'],
                'satker'=>$input['satker'],
                'thn_lhp'=>@$input['spt_date'],
                'tgl_lhp'=>@$tgl,
                'pelaksana'=>$input['pelaksana'],
                'jml_temuan'=>$total_temuan,
            ));
        $nomor=1;

        for($i=1;$i<=$total_temuan;$i++){
                $this->db->insert('interpro.tindak_lanjutd',array(
                    'id'=>$id,
                    'kode_temuan'=>$input["kt$nomor"],
                    'judul_temuan'=>$input["judul_tl$nomor"],
                    'uraian_temuan'=>$input["ut$nomor"],
                    'nilai_temuan'=>$input["nilai_tl$nomor"],
                    'kriteria'=>$input["kriteria$nomor"],
                    'tanggapan'=>$input["tang$nomor"],
                    'akibat'=>json_encode($input["a$nomor"]),
                    'kondisi'=>$input["kondisi$nomor"],
                    'urutan'=>$nomor,
                ));
                //rekom
                for($j=0;$j<count($input["kr$nomor"]);$j++){
                    $this->db->insert('interpro.tindak_lanjut_rekom',array(
                        'id'=>$id,
                        'kode_rekom'=>$input["kr$nomor"][$j],
                        'status_rekom'=>$input["sr$nomor"][$j],
                        'uraian_rekom'=>$input["uk$nomor"][$j],
                        'nilai_rekom'=>$input["nilai_r$nomor"][$j],
                        'no_tl'=>$input["ntl$nomor"][$j],
                        'uraian_tl'=>$input["utl$nomor"][$j],
                        'nilai_tl'=>$input["nilai_tindak_l$nomor"][$j],
                        'urutan'=>$nomor,
                    ));

                    if(isset($_FILES["file$nomor"][$j])&&$_FILES["file$nomor"][$j]['size']>0){
                        $_FILES['userfile']['name']     = $_FILES["file$nomor"][$j]['name'];
                        $_FILES['userfile']['type']     = $_FILES["file$nomor"][$j]['type'];
                        $_FILES['userfile']['tmp_name'] = $_FILES["file$nomor"][$j]['tmp_name'];
                        $_FILES['userfile']['error']    = $_FILES["file$nomor"][$j]['error'];
                        $_FILES['userfile']['size']     = $_FILES["file$nomor"][$j]['size'];

                        $config['upload_path'] = './img/';
                        $config['allowed_types'] = '*';
                        $config['max_size'] = 1024 * 8;
                        $config['encrypt_name'] = TRUE;
                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('userfile')) {
                            redirect(base_url('internalproses/tindak_lanjut'));
                            return array('danger', $this->upload->display_errors());
                        } else {
                            $file = $this->upload->data();
                            $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);

                            $update = array(
                                'file_tl' => (@$input['isian']?json_encode(@$input['isian']):null),
                            );
                            $this->db->where('id', $id);
                            $this->db->where('no_tl', $input["ntl$nomor"][$j]);
                            $this->db->where('uraian_tl', $input["utl$nomor"][$j]);
                            $this->db->update('interpro.tindak_lanjut_rekom', $update);
                        }
                    }
                }
                //penyebab
                for($k=0;$k<count($input["penyebab$nomor"]);$k++){
                    $this->db->insert('interpro.tindak_lanjut_penyebab',array(
                        'id'=>$id,
                        'kode_penyebab'=>$input["penyebab$nomor"][$k],
                        'uraian_penyebab'=>$input["up$nomor"][$k],
                        'urutan'=>$nomor,
                    ));
                }
                for($k=0;$k<count($input["a$nomor"]);$k++){
                    $this->db->insert('interpro.tindak_lanjut_akibat',array(
                        'id'=>$id,
                        'kode_akibat'=>$input["a$nomor"][$k],
                        'uraian_akibat'=>$input["ap$nomor"][$k],
                        'urutan'=>$nomor,
                    ));
                }
                $nomor++;
            }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return array('danger', "Temuan gagal disimpan");
        } else {
            $this->db->trans_commit();
            return array('success', "Temuan berhasil disimpan");
        }
    }
    public function insert_lhp_o($input)
    {

        $total_temuan=$input['no_temuan'];
//        echo json_encode($form);die();
        $user=$this->session->userdata('user');
        $this->db->trans_begin();
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $date=explode('/',$input['tgl_lhp']);
        $tgl=$date[2].'-'.$date[1].'-'.$date[0];
        $date2=explode('/',$input['pelaksanaan']);
        $tgl2=$date2[2].'-'.$date2[1].'-'.$date2[0];
        $this->db->insert('interpro.lhpo',array(
                'id'=>$id,
                'created_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$user['id'],
                'status'=>TRUE,
                'judul'=>$input['judul_spt'],
                'no_laporan'=>$input['spt_no'],
                'lampiran'=>$input['lampiran'],
                'hal'=>$input['hal'],
                'ditujukan'=>$input['ditujukan'],
                'pendahuluan'=>$input['pendahuluan'],
                'dasar'=>$input['dasar'],
                'sasaran'=>$input['sasaran'],
                'tujuan'=>$input['tujuan'],
                'ruang'=>$input['ruang'],
                'metodologi'=>$input['metodologi'],
                'keuangan'=>$input['keuangan'],
                'organisasi'=>$input['organisasi'],
                'data'=>$input['data'],
                'pelaksanaan'=>$tgl2,
                'tgl_lhp'=>@$tgl,
                'jml_temuan'=>$total_temuan,
            ));
        $nomor=1;

        for($i=1;$i<=$total_temuan;$i++){
                $this->db->insert('interpro.lhpod',array(
                    'id'=>$id,
                    'judul_temuan'=>$input["judul_tl$nomor"],
                    'uraian_temuan'=>$input["ut$nomor"],
                    'nilai_temuan'=>$input["nilai_tl$nomor"],
                    'kriteria'=>$input["kriteria$nomor"],
                    'tanggapan'=>$input["tang$nomor"],
//                    'akibat'=>json_encode($input["ap$nomor"]),
                    'kondisi'=>$input["kondisi$nomor"],
                    'urutan'=>$nomor,
                ));
                //rekom
                for($j=0;$j<count($input["uk$nomor"]);$j++){
                    $this->db->insert('interpro.lhpo_rekom',array(
                        'id'=>$id,
                        'uraian_rekom'=>$input["uk$nomor"][$j],
                        'nilai_rekom'=>$input["nilai_r$nomor"][$j],
                        'uraian_tl'=>$input["utl$nomor"][$j],
                        'nilai_tl'=>$input["nilai_tl$nomor"][$j],
                        'urutan'=>$nomor,
                    ));

                    if(isset($_FILES["file$nomor"][$j])&&$_FILES["file$nomor"][$j]['size']>0){
                        $_FILES['userfile']['name']     = $_FILES["file$nomor"][$j]['name'];
                        $_FILES['userfile']['type']     = $_FILES["file$nomor"][$j]['type'];
                        $_FILES['userfile']['tmp_name'] = $_FILES["file$nomor"][$j]['tmp_name'];
                        $_FILES['userfile']['error']    = $_FILES["file$nomor"][$j]['error'];
                        $_FILES['userfile']['size']     = $_FILES["file$nomor"][$j]['size'];

                        $config['upload_path'] = './img/';
                        $config['allowed_types'] = '*';
                        $config['max_size'] = 1024 * 8;
                        $config['encrypt_name'] = TRUE;
                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('userfile')) {
                            redirect(base_url('laporan/lhp_o'));
                            return array('danger', $this->upload->display_errors());
                        } else {
                            $file = $this->upload->data();
                            $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);

                            $update = array(
                                'file_tl' => (@$input['isian']?json_encode(@$input['isian']):null),
                            );
                            $this->db->where('id', $id);
                            $this->db->where('no_tl', $input["ntl$nomor"][$j]);
                            $this->db->where('uraian_tl', $input["utl$nomor"][$j]);
                            $this->db->update('interpro.lhpo_rekom', $update);
                        }
                    }
                }
                //penyebab
                for($k=0;$k<count($input["up$nomor"]);$k++){
                    $this->db->insert('interpro.lhpo_penyebab',array(
                        'id'=>$id,
                        'uraian_penyebab'=>$input["up$nomor"][$k],
                        'urutan'=>$nomor,
                    ));
                }
                for($k=0;$k<count($input["ap$nomor"]);$k++){
                    $this->db->insert('interpro.lhpo_akibat',array(
                        'id'=>$id,
                        'uraian_akibat'=>$input["ap$nomor"][$k],
                        'urutan'=>$nomor,
                    ));
                }
                $nomor++;
            }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return array('danger', "Temuan gagal disimpan");
        } else {
            $this->db->trans_commit();
            return array('success', "Temuan berhasil disimpan");
        }
    }
    public function insert_temuan_edit($input)
    {
//        $this->db->where('id', $this->input->get_post('id_rencana'));
//        $a = $this->db->get('interpro.perencanaan')->row_array();

        $total_temuan=$input['no_temuan'];
        $user=$this->session->userdata('user');
        $this->db->trans_begin();
            $this->load->library('uuid');
            $id = $this->uuid->v4();
        $date=explode('/',$input['tgl_lhp']);
        $tgl=$date[2].'-'.$date[1].'-'.$date[0];
        if(@$input['id_rencana']!=''){
            $this->db->where('id', $input['id_rencana']);
            $this->db->update('interpro.perencanaan',array(
                'status_tl'=>true
            ));
        }
        if(@$input['id_tl']==''){
            $this->db->insert('interpro.tindak_lanjut',array(
                'id'=>$id,
                'created_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$user['id'],
                'status'=>TRUE,
                'skpd'=>$input['judul_spt'],
                'satker'=>$input['satker'],
                'no_lhp'=>$input['spt_no'],
                'thn_lhp'=>@$input['spt_date'],
                'id_rencana'=>@$input['id_rencana']?$input['id_rencana']:null,
                'tgl_lhp'=>@$tgl,
                'pelaksana'=>@$input['pelaksana'],
                'jml_temuan'=>$total_temuan,
            ));
            $nomor=1;
            for($i=1;$i<=$total_temuan;$i++){
                $this->db->insert('interpro.tindak_lanjutd',array(
                    'id'=>$id,
                    'kode_temuan'=>$input["kt$nomor"],
                    'judul_temuan'=>$input["judul_tl$nomor"],
                    'uraian_temuan'=>$input["ut$nomor"],
                    'nilai_temuan'=>$input["nilai_tl$nomor"],
                    'kriteria'=>$input["kriteria$nomor"],
//                    'kondisi'=>$input["kondisi$nomor"],
                    'tanggapan'=>$input["tang$nomor"],
                    'akibat'=>json_encode($input["a$nomor"]),
                ));
                //rekom
                for($j=0;$j<count($input["kr$nomor"]);$j++){
                    $this->db->insert('interpro.tindak_lanjut_rekom',array(
                        'id'=>$id,
                        'kode_rekom'=>$input["kr$nomor"][$j],
                        'uraian_rekom'=>$input["uk$nomor"][$j],
                        'no_tl'=>$input["ntl$nomor"][$j],
                        'uraian_tl'=>$input["utl$nomor"][$j],
                        'urutan'=>$nomor,
                    ));
                }
                //penyebab
                for($k=0;$k<count($input["penyebab$nomor"]);$k++){
                    $this->db->insert('interpro.tindak_lanjut_penyebab',array(
                        'id'=>$id,
                        'kode_penyebab'=>$input["penyebab$nomor"][$k],
                        'uraian_penyebab'=>$input["up$nomor"][$k],
                        'urutan'=>$nomor,
                    ));
                }
                $nomor++;
            }
        }else{
            $this->db->where('id', $input['id_tl']);
            $this->db->delete('interpro.tindak_lanjutd');
            $this->db->where('id', $input['id_tl']);
            $this->db->delete('interpro.tindak_lanjut_rekom');
            $this->db->where('id', $input['id_tl']);
            $this->db->delete('interpro.tindak_lanjut_penyebab');
            $this->db->where('id', $input['id_tl']);
            $this->db->update('interpro.tindak_lanjut',array(
                'modified_at'=>date('Y-m-d H:i:s'),
                'modified_by'=>$user['id'],
                'skpd'=>$input['judul_spt'],
                'no_lhp'=>$input['spt_no'],
                'thn_lhp'=>@$input['spt_date'],
                'satker'=>$input['satker'],
                'id_rencana'=>@$input['id_rencana'],
                'tgl_lhp'=>@$tgl,
                'pelaksana'=>$input['pelaksana'],
                'jml_temuan'=>$total_temuan,
            ));
            $nomor=1;
            for($i=1;$i<=$total_temuan;$i++){
                $this->db->insert('interpro.tindak_lanjutd',array(
                    'id'=>$input['id_tl'],
                    'kode_temuan'=>$input["kt$nomor"],
                    'judul_temuan'=>$input["judul_tl$nomor"],
                    'uraian_temuan'=>$input["ut$nomor"],
                    'nilai_temuan'=>$input["nilai_tl$nomor"],
                    'kriteria'=>$input["kriteria$nomor"],
                    'kriteria'=>$input["kriteria$nomor"],
//                    'kondisi'=>$input["kondisi$nomor"],
                ));
                //rekom
                for($j=0;$j<count($input["kr$nomor"]);$j++){
                    $this->db->insert('interpro.tindak_lanjut_rekom',array(
                        'id'=>$input['id_tl'],
                        'kode_rekom'=>$input["kr$nomor"][$j],
                        'uraian_rekom'=>$input["uk$nomor"][$j],
                        'no_tl'=>$input["ntl$nomor"][$j],
                        'uraian_tl'=>$input["utl$nomor"][$j],
                        'urutan'=>$nomor,
                    ));
                }
                //penyebab
                for($k=0;$k<count($input["penyebab$nomor"]);$k++){
                    $this->db->insert('interpro.tindak_lanjut_penyebab',array(
                        'id'=>$input['id_tl'],
                        'kode_penyebab'=>$input["penyebab$nomor"][$k],
                        'uraian_penyebab'=>$input["up$nomor"][$k],
                        'urutan'=>$nomor,
                    ));
                }
                $nomor++;
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            echo 'f';die();
            return array('danger', "Temuan gagal disimpan");
        } else {
            $this->db->trans_commit();
            return array('success', "Temuan berhasil disimpan");
        }
    }

    public function pelaksanaan($id, $input)
    {
        $this->db->where('id', $id);
        $this->db->where('progres <=', 1);
        $this->db->where('tao', $input['id']);
        $sebelumnya = $this->db->get('interpro.perencanaan_detail')->row_array();
        if(!isset($sebelumnya['id'])){
            return array('error', 'Data realisasi gagal tersimpan');
        }

        $kk = json_decode($sebelumnya['file'], TRUE);
        if(!is_array($kk)){
            $kk=array();
        }
        $input['isian']=$kk;
        for ($i = 0; $i < count($_FILES['file']['name']); $i++) :
            $_FILES['userfile']['name']     = $_FILES['file']['name'][$i];
            $_FILES['userfile']['type']     = $_FILES['file']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
            $_FILES['userfile']['error']    = $_FILES['file']['error'][$i];
            $_FILES['userfile']['size']     = $_FILES['file']['size'][$i];

            $config['upload_path'] = './img/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                return array('danger', $this->upload->display_errors());
            } else {
                $file = $this->upload->data();
                $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);
            }
            unset($this->upload);
            unset($_FILES['userfile']);
        endfor;
        if(count($input['isian'])>0) {
            $this->db->trans_begin();
            $this->db->where('id', $id);
            $this->db->where('tao', $input['id']);
            $update = array(
                'realisasi_by' => $input['realisasi_by'],
                'realisasi_date' => format_waktu($input['realisasi_date'], true),
                'file' => json_encode($input['isian']),
                'progres' => 1,
            );
            if ($input['kesimpulan']) {
                $update['kesimpulan'] = $input['kesimpulan'];
            }
            $this->db->update('interpro.perencanaan_detail', $update);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('danger', 'Data realisasi gagal tersimpan');
            } else {
                $this->db->trans_commit();
                return array('success', 'Data realisasi berhasil tersimpan');
            }
        }else{
            return array('danger', 'Berkas belum terpilih');
        }
    }
}
