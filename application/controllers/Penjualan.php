<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        check_access('946653d4-231f-4805-9c7a-5a104bcd46aa');
        $this->template->set_view('penjualan');
    }

    public function index()
    {
        $this->order();
    }
    public function order($page=0){
        $menu='92559c74-927e-4edf-85c8-203447141840';
        $data['access']=list_access($menu);
        $this->load->model('barang');
        if($this->session->flashdata('status_update')){
            $data['status_update'] = $this->session->flashdata('status_update');
        }
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->barang->order_penjualan());
        }elseif($this->input->get_post('update')){
            check_access($menu,'u');
            $this->session->set_flashdata('status_update', $this->barang->order_penjualan_update());
        }elseif($this->input->get_post('e')){
            exit('belum');
            check_access($menu,'u');
            $id=$this->input->get_post('e');
            $this->db->order_by('kode,nama');
            $this->db->where('status',TRUE);
            $data['barang']=$this->db->get('main.mstitem')->result_array();
            $this->db->where('id',$id);
            $this->db->where('editable',true);
            $data['header']=$this->db->get('penjualan.vpenjualanorder')->row_array();
            $this->db->where('id',$data['header']['id']);
            $data['data']=$this->db->get('penjualan.trpenjualanorderd')->result_array();

            $this->db->order_by('kode,nama');
            $this->db->where('status',TRUE);
            $data['customer']=$this->db->get('main.mstcustomer')->result_array();
            $this->template->user('updateorderpenjualan',$data,array('title'=>'Form','breadcrumbs'=>array('Penjualan','Order')));
        }elseif(in_array(strtolower($this->input->get_post('new')),array('jasa','barang'))){
            check_access($menu,'c');
            $data['type']=strtolower($this->input->get_post('new'));
            $data['data']=array();
            $this->db->order_by('kode,nama');
            $this->db->where('status',TRUE);
            $data['customer']=$this->db->get('main.mstcustomer')->result_array();
            $this->db->order_by('keterangan');
            $data['metode']=$this->db->get('main.mstmetodepembayaran')->result_array();
            $this->template->user('addorder',$data,array('title'=>'Form','breadcrumbs'=>array('Penjualan','Order')));
        }elseif($this->input->get_post('d')){
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
            $this->db->where('id',$this->input->get_post('d'));
            $item=$this->db->get('penjualan.vorderd')->result_array();
            $data['data']='';
            foreach ($item as $no=>$d){
                $data['data'].="<tr><td>$d[nama]</td><td class='center'>$d[qty]</td><td class='right'>".format_uang($d['harga'])."</td><td class='right'>".format_uang($d['dpp'])."</td><td class='right'>".format_uang($d['ppn'])."</td><td class='right'>".format_uang($d['pph'])."</td><td class='right'>".format_uang($d['diskon'])."</td><td class='right'>".format_uang($d['grandtotal'])."</td><td class='center'>".($d['type']=='t'?'BARANG':'JASA')."</td><td class='center'>".getLabelProcess($d['status'],false)."</td></tr>";
            }
            $this->db->where('penjualan.trorder.id',$this->input->get_post('d'));
            $this->db->join('main.mstkaryawan','main.mstkaryawan.id=penjualan.trorder.request_by');
            $this->db->join('main.mstmetodepembayaran','mstmetodepembayaran.id=trorder.metodepembayaran','left');
            $this->db->select('main.mstkaryawan.nik,main.mstkaryawan.nama,uangmuka,keterangan');
            $header=$this->db->get('penjualan.trorder')->row_array();
            $data['mengetahui']=isset($header['nik'])?"$header[nik] - $header[nama]":"";
            $data['uangmuka']=isset($header['keterangan'])?($header['keterangan'].": ".format_uang($header['uangmuka'])):"-";
            echo json_encode($data);
            return;
        }elseif($this->input->get_post('query')){
            $this->db->limit(10);
            if($this->input->get_post('k')){
                $this->db->order_by('nik,nama','asc');
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(nik)',strtolower($this->input->get_post('query')));
                $this->db->where('status',TRUE);
                $this->db->select("id as id,nik||' - '||nama as label");
                $data=$this->db->get('main.mstkaryawan')->result_array();
            }elseif($this->input->get_post('bj')){
                $this->db->order_by('kode,nama','asc');
                $this->db->where('status',TRUE);
                $this->db->where('stok > ',0);
                $this->db->where('type',($this->input->get_post('bj')=='b'?TRUE:FALSE));
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(kode)',strtolower($this->input->get_post('query')));
                $this->db->select("id as id,kode||' - '||nama as label,hargajual,stok");
                $data=$this->db->get('main.mstitem')->result_array();
            }
            echo json_encode($data);
            return;
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'f');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $total=$this->db->get('penjualan.vorder')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $data['data']=$this->db->get('penjualan.vorder')->result_array();
            $data['pagination']=$this->template->pagination('penjualan/order',$total,$perpage);
            $data['page']=$page;
            $this->template->user('order',$data,array('title'=>'Order','breadcrumbs'=>array('Penjualan')));
        }
    }
    public function pengiriman($page=0){
        $menu='b4be4621-3e9e-4e3d-a47b-df549ae100f4';
        $data['access']=list_access($menu);
        $this->load->model('barang');
        if($this->session->flashdata('status_update')){
            $data['status_update'] = $this->session->flashdata('status_update');
        }
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->barang->penerimaan_penjualan());
        }elseif($this->input->get_post('new') && $this->input->get_post('nota_id')){
            check_access($menu,'c');
            $id=$this->input->get_post('nota_id_id');
            $this->db->where('jumlah > diterima',null,FALSE);
            $this->db->where('type',true);
            $this->db->where('id',$id);
            $data['data']=$this->db->get('penjualan.trorderd')->result_array();
            if(!isset($data['data'][0]['id'])){
                $this->session->set_flashdata('status_update', array('warning','OPJ tidak valid'));
                redirect(base_url('penjualan/pengiriman'));
            }
            $this->db->where('id',$id);
            $data['header']=$this->db->get('penjualan.vorder')->row_array();
            $this->template->user('addpengiriman',$data,array('title'=>'Form Pengiriman','breadcrumbs'=>array('Penjualan','Surat Jalan')));
        }elseif($this->input->get_post('d')){
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
            $this->db->where('id',$this->input->get_post('d'));
            $item=$this->db->get('penjualan.trsuratjaland')->result_array();
            $data['data']='';
            foreach ($item as $no=>$d){
                $data['data'].="<tr><td>$d[nama]</td><td class='center'>$d[jumlah]</td><td style='white-space: pre-wrap'>".htmlveryspecialchars($d['note'])."</td></tr>";
            }
            echo json_encode($data);
            return;
        }elseif($this->input->get_post('query')){
            $this->db->limit(10);
            if($this->input->get_post('k')){
                $this->db->order_by('nik,nama','asc');
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(nik)',strtolower($this->input->get_post('query')));
                $this->db->where('status',TRUE);
                $this->db->select("id as id,nik||' - '||nama as label");
                $data=$this->db->get('main.mstkaryawan')->result_array();
            }elseif($this->input->get_post('opj')){
                $this->db->order_by('tanggal,nota','asc');
                $this->db->where('closed',FALSE);
                $this->db->where('barangjasa',TRUE);
                $this->db->like('lower(nota)',strtolower($this->input->get_post('query')));
                $this->db->select("id as id,nota as label");
                $data=$this->db->get('penjualan.trorder')->result_array();
            }
            echo json_encode($data);
            return;
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'f');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $total=$this->db->get('penjualan.vsuratjalan')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $data['data']=$this->db->get('penjualan.vsuratjalan')->result_array();
            $data['pagination']=$this->template->pagination('penjualan/penerimaan',$total,$perpage);
            $data['page']=$page;
            $this->template->user('pengiriman',$data,array('title'=>'Pengiriman Barang','breadcrumbs'=>array('Penjualan')));
        }
    }
    public function retur($page=0){
        $menu='17eebe14-ed40-4a30-a689-c141c2d149fc';
        $data['access']=list_access($menu);
        $this->load->model('barang');
        if($this->session->flashdata('status_update')){
            $data['status_update'] = $this->session->flashdata('status_update');
        }
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->barang->retur_penjualan());
        }elseif($this->input->get_post('delete')){
            check_access($menu,'d');
            $this->db->where('id',$this->input->get_post('delete'));
            $this->db->where('closed',false);
            $this->db->where('cancel',false);
            $this->db->where('editable',true);
            $this->db->update('penjualan.trretur',array(
                'closed'=>true,
                'editable'=>false,
                'cancel'=>true,
                'status'=>false,
            ));
        }elseif($this->input->get_post('new') && $this->input->get_post('nota_id')){
            check_access($menu,'c');
            $id=$this->input->get_post('nota_id_id');
            $this->db->where('jumlah > retur',null,FALSE);
            $this->db->where('id',$id);
            $data['data']=$this->db->get('penjualan.trsuratjaland')->result_array();
            if(!isset($data['data'][0]['id'])){
                $this->session->set_flashdata('status_update', array('warning','SJ tidak valid'));
                redirect(base_url('penjualan/retur'));
            }
            $this->db->where('id',$id);
            $data['header']=$this->db->get('penjualan.vsuratjalan')->row_array();
            $this->template->user('addretur',$data,array('title'=>'Form Retur Penjualan','breadcrumbs'=>array('Penjualan','Retur')));
        }elseif($this->input->get_post('d')){
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
            $this->db->where('id',$this->input->get_post('d'));
            $item=$this->db->get('penjualan.trreturd')->result_array();
            $data['data']='';
            foreach ($item as $no=>$d){
                $data['data'].="<tr><td class='center'>$d[nota]</td><td>$d[nama]</td><td class='center'>$d[jumlah]</td><td style='white-space: pre-wrap'>".htmlveryspecialchars($d['note'])."</td></tr>";
            }
            echo json_encode($data);
            return;
        }elseif($this->input->get_post('query')){
            $this->db->limit(10);
            if($this->input->get_post('k')){
                $this->db->order_by('nik,nama','asc');
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(nik)',strtolower($this->input->get_post('query')));
                $this->db->where('status',TRUE);
                $this->db->select("id as id,nik||' - '||nama as label");
                $data=$this->db->get('main.mstkaryawan')->result_array();
            }elseif($this->input->get_post('sj')){
                $this->db->order_by('tanggal,nota','asc');
                $this->db->like('lower(nota)',strtolower($this->input->get_post('query')));
                $this->db->select("id as id,nota as label");
                $data=$this->db->get('penjualan.vsuratjalan')->result_array();
            }
            echo json_encode($data);
            return;
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'f');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $total=$this->db->get('penjualan.trretur')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $data['data']=$this->db->get('penjualan.vretur')->result_array();
            $data['pagination']=$this->template->pagination('penjualan/retur',$total,$perpage);
            $data['page']=$page;
            $this->template->user('retur',$data,array('title'=>'Retur Barang','breadcrumbs'=>array('Penjualan')));
        }
    }
    public function tagihan($page=0){
        $menu='2f6c338e-1d6a-467c-a8a1-5db563bb66c8';
        $data['access']=list_access($menu);
        $this->load->model('barang');
        if($this->session->flashdata('status_update')){
            $data['status_update'] = $this->session->flashdata('status_update');
        }
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->barang->tagihan_penjualan());
        }elseif($this->input->get_post('entry')){
            check_access($menu,'c');
            $id=$this->input->get_post('entry');
            $this->db->where('customer',$id);
            $data['data']=$this->db->get('penjualan.vlisttagihan')->result_array();
            if(!isset($data['data'][0]['customer'])){
                $this->session->set_flashdata('status_update', array('warning','Tagihan dari customer tidak ada'));
                redirect(base_url('penjualan/tagihan'));
            }
            $this->db->where('id',$id);
            $data['header']=$this->db->get('main.mstcustomer')->row_array();
            $this->template->user('tagihanbycustomer',$data,array('title'=>'Tagihan Customer','breadcrumbs'=>array('Penjualan')));
        }elseif($this->input->get_post('d')){
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
            $this->db->where('id',$this->input->get_post('d'));
            $this->db->select('orderdate,orderno,nota,tanggal,harga,dpp,ppn,pph,diskon');
            $item=$this->db->get('penjualan.trtagihand')->result_array();
            $data['data']='';
            foreach ($item as $no=>$d){
                $data['data'].="<tr><td class='center'>$d[orderno]</td><td class='center'>".format_waktu($d['orderdate'])."</td><td class='center'>$d[nota]</td><td class='center'>".format_waktu($d['tanggal'])."</td><td class='right'>".format_uang($d['dpp'])."</td><td class='right'>".format_uang($d['ppn'])."</td><td class='right'>".format_uang($d['pph'])."</td><td class='right'>".format_uang($d['diskon'])."</td><td class='right'>".format_uang($d['harga'])."</td></tr>";
            }
            echo json_encode($data);
            return;
        }elseif($this->input->get_post('query')){
            $this->db->limit(10);
            if($this->input->get_post('k')){
                $this->db->order_by('nik,nama','asc');
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(nik)',strtolower($this->input->get_post('query')));
                $this->db->where('status',TRUE);
                $this->db->select("id as id,nik||' - '||nama as label");
                $data=$this->db->get('main.mstkaryawan')->result_array();
            }elseif($this->input->get_post('sj')){
                $this->db->order_by('tanggal,nota','asc');
                $this->db->where('closed',FALSE);
                $this->db->like('lower(nota)',strtolower($this->input->get_post('query')));
                $this->db->select("id as id,nota as label");
                $data=$this->db->get('penjualan.vsuratjalan')->result_array();
            }
            echo json_encode($data);
            return;
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'f');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $total=$this->db->get('penjualan.trtagihan')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $data['data']=$this->db->get('penjualan.vtagihan')->result_array();
            $data['pagination']=$this->template->pagination('penjualan/tagihan',$total,$perpage);

            $this->db->order_by('customer_id');
            $this->db->group_by('customer,customer_id,customer_name,hutangpiutang');
            $this->db->select('customer,customer_id,customer_name,hutangpiutang,SUM(harga-diskon) as tagihan');
            $data['pending']=$this->db->get('penjualan.vlisttagihan')->result_array();
            $data['page']=$page;
            $this->template->user('tagihan',$data,array('title'=>'Tagihan Customer','breadcrumbs'=>array('Penjualan')));
        }
    }
    public function pembayaran($page=0){
        $menu='fb2045aa-bcc6-4494-9760-7f2f28ff6cf6';
        $data['access']=list_access($menu);
        $this->load->model('barang');
        if($this->session->flashdata('status_update')){
            $data['status_update'] = $this->session->flashdata('status_update');
        }
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->barang->pembayaran_penjualan());
        }elseif($this->input->get_post('entry')){
            check_access($menu,'c');
            $id=$this->input->get_post('entry');
            $this->db->where('customer',$id);
            $this->db->where('closed',false);
            $data['data']=$this->db->get('penjualan.vtagihan')->result_array();
            if(!isset($data['data'][0]['customer'])){
                $this->session->set_flashdata('status_update', array('warning','Tagihan dari customer tidak ada'));
                redirect(base_url('penjualan/pembayaran'));
            }
            $this->db->order_by('keterangan');
            $data['metode']=$this->db->get('main.mstmetodepembayaran')->result_array();
            $this->db->where('id',$id);
            $data['header']=$this->db->get('main.mstcustomer')->row_array();
            $this->template->user('bayarbycustomer',$data,array('title'=>'Pembayaran Piutang Usaha','breadcrumbs'=>array('Penjualan')));
        }elseif($this->input->get_post('d')){
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
            $this->db->where('id',$this->input->get_post('d'));
            $this->db->select('nota,harga,ppn,pph,diskon,(harga-diskon) as tagihan');
            $item=$this->db->get('penjualan.trpembayarand')->result_array();
            $data['data']='';
            foreach ($item as $no=>$d){
                $data['data'].="<tr><td class='center'>$d[nota]</td><td class='right'>".format_uang($d['ppn'])."</td><td class='right'>".format_uang($d['pph'])."</td><td class='right'>".format_uang($d['harga'])."</td><td class='right'>".format_uang($d['diskon'])."</td><td class='right'>".format_uang($d['tagihan'])."</td></tr>";
            }
            echo json_encode($data);
            return;
        }elseif($this->input->get_post('query')){
            $this->db->limit(10);
            if($this->input->get_post('k')){
                $this->db->order_by('nik,nama','asc');
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(nik)',strtolower($this->input->get_post('query')));
                $this->db->where('status',TRUE);
                $this->db->select("id as id,nik||' - '||nama as label");
                $data=$this->db->get('main.mstkaryawan')->result_array();
            }elseif($this->input->get_post('sj')){
                $this->db->order_by('tanggal,nota','asc');
                $this->db->where('closed',FALSE);
                $this->db->like('lower(nota)',strtolower($this->input->get_post('query')));
                $this->db->select("id as id,nota as label");
                $data=$this->db->get('penjualan.vsuratjalan')->result_array();
            }
            echo json_encode($data);
            return;
        }else{
            $data['nota']=$this->input->get_post('nota');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $total=$this->db->get('penjualan.vpembayaran')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $data['data']=$this->db->get('penjualan.vpembayaran')->result_array();
            $data['pagination']=$this->template->pagination('penjualan/pembayaran',$total,$perpage);

            $this->db->order_by('customer_id');
            $this->db->group_by('customer,customer_id,customer_name,hutangpiutang');
            $this->db->where('closed',false);
            $this->db->select('customer,customer_id,customer_name,hutangpiutang,SUM(harga-diskon-diskontambahan) as tagihan');
            $data['pending']=$this->db->get('penjualan.vtagihan')->result_array();
            $data['page']=$page;
            $this->template->user('pembayaran',$data,array('title'=>'Pembayaran Piutang Usaha','breadcrumbs'=>array('Penjualan')));
        }
    }
}
