<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resiko extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        check_access('923143fd-ca98-489d-88d7-642d2001b6ef');
        $this->template->set_view('resiko');
    }

    public function index()
    {
        redirect(base_url());
    }

    public function programBU()
    {
        $menu = '388deab8-36ff-4a8b-a1d7-b88eab8b4c26';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->insert_program($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/program'));
            }
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->template->user('updateprogram', $data, array('title' => 'Tambah Data Program Satker', 'breadcrumbs' => array('Manajemen Resiko', 'Tambah Program Satker')));
            return;
        }
        elseif ($this->input->get_post('e')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('e');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->update_program($this->input->get_post(null),$data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/program'));
            }

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('id', $data['id']);
            $data['user']  = $this->db->get('support.program_satker')->row_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan,resiko');
            $data['resiko_kr']  = $this->db->get('support.program_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.program_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.program_kr')->num_rows();
            $this->template->user('updateprogram', $data, array('title' => $data['user']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Perbarui Program Satker')));
            return;
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_program_status($this->input->get_post('status'));
            return;
        }
        elseif ($this->input->get_post('resiko')) {
            $no=$this->input->get_post('resiko');
            check_access($menu, 'c');

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();
            ?>
            <div class="form-group <?=$no?>">
                <h3>Resiko <button type="button" onclick="tambah_resiko(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h3> <br>

                <div class="form-group resiko<?=$no?>">

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Resiko<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="resiko<?=$no?>" data-placeholder="Pilih Tipe Resiko" required>
                                <option value="">Pilih Tipe Resiko</option>
                                <?php
                                foreach ($tipe_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-sm-offset-1">
                        <div class="form-group row">
                            <h5>Kendali <button type="button" onclick="tambah_kendali(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h5> <br>
                        </div>
                        <div class="form-group kendali<?=$no?>">

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" name="kendali_s<?=$no?>[]" data-placeholder="Pilih Kendali Seharusnya" required>
                                        <option value="">Pilih Kendali Sementara</option>
                                        <?php
                                        foreach ($kendali as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" name="kendali_a<?=$no?>[]" data-placeholder="Pilih Kendali Yang Ada" required>
                                        <option value="">Pilih Kendali Yang Ada</option>
                                        <?php
                                        foreach ($kendali as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            return;
        }
        elseif ($this->input->get_post('kendali')) {
            $no=$this->input->get_post('kendali');
            check_access($menu, 'c');

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();
            ?>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span
                            class="text-danger">*</span></label>
                <div class="col-sm-6">
                    <select class="select2 form-control select2-multiple" name="kendali_s<?=$no?>[]" data-placeholder="Pilih Kendali Seharusnya" required>
                        <option value="">Pilih Kendali Sementara</option>
                        <?php
                        foreach ($kendali as $v):
                            ?>
                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada<span
                            class="text-danger">*</span></label>
                <div class="col-sm-6">
                    <select class="select2 form-control select2-multiple" name="kendali_a<?=$no?>[]" data-placeholder="Pilih Kendali Yang Ada" required>
                        <option value="">Pilih Kendali Yang Ada</option>
                        <?php
                        foreach ($kendali as $v):
                            ?>
                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <?php
            return;
        }
        else{

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->db->order_by('nama','desc');
            $data['p_sekarang'] = $this->db->get('periode')->row_array();
            $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : $data['p_sekarang']['id'];
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
            $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
            $data['pkpt'] = $this->user->get_list_p_satker($data['closed'],$data['periode_s']);
            $this->template->user('program_satker', $data, array('title' => 'Program Satker', 'breadcrumbs' => array('Manajemen Resiko')));
        }
       }
    public function kegiatan()
    {
        $menu = '0d59e730-4d54-4ef2-a892-2bdb88f4e732';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->insert_konteks_kegiatan_satker($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/kegiatan'));
            }

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['keg'] = $this->db->get('master.kegiatan')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();


            $this->template->user('updatekegiatan', $data, array('title' => 'Tambah Konteks Kegiatan Satker', 'breadcrumbs' => array('Manajemen Resiko', 'Tambah Kegiatan Satker')));
            return;
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_kegiatan_satker_status($this->input->get_post('status'));
            return;
        }
        elseif ($this->input->get_post('resiko')) {
            $no=$this->input->get_post('resiko');
            check_access($menu, 'c');

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $pe_resiko = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();

            $this->db->order_by('kode');
            $this->db->where('status', true);
            $da_resiko = $this->db->get('master.dampak')->result_array();
            ?>
            <div class="form-group <?=$no?>">
                <h3>Resiko <button type="button" onclick="tambah_resiko(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h3> <br>

                <div class="form-group resiko<?=$no?>">

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Resiko<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="resiko<?=$no?>" data-placeholder="Pilih Tipe Resiko" required>
                                <option value="">Pilih Tipe Resiko</option>
                                <?php
                                foreach ($tipe_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Penyebab Resiko<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="p_resiko<?=$no?>" data-placeholder="Pilih Penyebab Resiko" required>
                                <option value="">Pilih Penyebab Resiko</option>
                                <?php
                                foreach ($pe_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div><div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Dampak Resiko<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="da_resiko1" data-placeholder="Pilih Dampak Resiko" required>
                                <option value="">Pilih Dampak Resiko</option>
                                <?php
                                foreach ($da_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group kendali<?=$no?>">

                        <div class="form-group row">
                            <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-6">
                                <select class="select2 form-control select2-multiple" multiple="multiple" multiple data-placeholder="Pilih Kendali Seharusnya" name="kendali_s<?=$no?>[]" required>
                                    <option value="">Pilih Kendali Seharusnya</option>
                                    <?php
                                    foreach ($kendali as $v):
                                        ?>
                                        <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                    <?php
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            return;
        }
        elseif ($this->input->get_post('del')) {
            $id=$this->input->get_post('del');
            check_access($menu, 'c');

            $this->db->where('id', $id);
            $this->db->delete('support.konteks_kegiatan');

            $this->db->where('id', $id);
            $data['status_update'] = $this->db->delete('support.konteks_kegiatand');
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/kegiatan'));
            return;
        }
        else{
            $data['pkpt'] = $this->user->get_list_konteks_k();
            $this->template->user('konteks_kegiatan', $data, array('title' => 'Konteks Kegiatan', 'breadcrumbs' => array('Manajemen Resiko')));
        }
       }
    public function program()
    {
        $menu = '388deab8-36ff-4a8b-a1d7-b88eab8b4c26';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->insert_konteks_program_satker($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/program'));
            }

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['keg'] = $this->db->get('master.program')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();


            $this->template->user('updateprogram', $data, array('title' => 'Tambah Konteks Program Satker', 'breadcrumbs' => array('Manajemen Resiko', 'Tambah Kegiatan Satker')));
            return;
        }
        elseif ($this->input->get_post('resiko')) {
            $no=$this->input->get_post('resiko');
            check_access($menu, 'c');

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $pe_resiko = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();

            $this->db->order_by('kode');
            $this->db->where('status', true);
            $da_resiko = $this->db->get('master.dampak')->result_array();
            ?>
            <div class="form-group <?=$no?>">
                <h3>Resiko <button type="button" onclick="tambah_resiko(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h3> <br>

                <div class="form-group resiko<?=$no?>">

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Resiko<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="resiko<?=$no?>" data-placeholder="Pilih Tipe Resiko" required>
                                <option value="">Pilih Tipe Resiko</option>
                                <?php
                                foreach ($tipe_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Penyebab Resiko<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="p_resiko<?=$no?>" data-placeholder="Pilih Penyebab Resiko" required>
                                <option value="">Pilih Penyebab Resiko</option>
                                <?php
                                foreach ($pe_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div><div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Dampak Resiko<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="da_resiko1" data-placeholder="Pilih Dampak Resiko" required>
                                <option value="">Pilih Dampak Resiko</option>
                                <?php
                                foreach ($da_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group kendali<?=$no?>">

                        <div class="form-group row">
                            <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-6">
                                <select class="select2 form-control select2-multiple" multiple="multiple" multiple data-placeholder="Pilih Kendali Seharusnya" name="kendali_s<?=$no?>[]" required>
                                    <option value="">Pilih Kendali Seharusnya</option>
                                    <?php
                                    foreach ($kendali as $v):
                                        ?>
                                        <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                    <?php
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            return;
        }
        elseif ($this->input->get_post('del')) {
            $id=$this->input->get_post('del');
            check_access($menu, 'c');

            $this->db->where('id', $id);
            $this->db->delete('support.konteks_program');

            $this->db->where('id', $id);
            $data['status_update'] = $this->db->delete('support.konteks_programd');
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/program'));
            return;
        }
        else{
            $data['pkpt'] = $this->user->get_list_konteks_p();
            $this->template->user('konteks_program', $data, array('title' => 'Konteks Program', 'breadcrumbs' => array('Manajemen Resiko')));
        }
       }
    public function n_kegiatan()
    {
        $menu = '0d59e730-4d54-4ef2-a892-2bdb88f4e732';
        $data['access'] = list_access($menu);
        $this->load->model('user');

        if($this->input->get_post('save')) {
            check_access($menu, 'c');
            $data = $_POST;
            // $data = $this->input->get_post(null);
            // print_r("<pre>");
            // print_r($_POST);
            // die();
            $data['status_update'] = $this->user->insert_kegiatan_satker($data);
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/n_kegiatan'));
        }
        if ($this->input->get_post('i')) {
            check_access($menu, 'c');
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['keg'] = $this->db->get('master.kegiatan')->result_array();
            $this->db->order_by('ket');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['konteks'] = $this->db->get('support.konteks_kegiatan')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['program'] = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->template->user('n_updatekegiatan', $data, array('title' => 'Tambah Data Kegiatan Satker', 'breadcrumbs' => array('Manajemen Resiko', 'Tambah Kegiatan Satker')));
            return;
        }
        elseif ($this->input->get_post('e')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('e');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->update_kegiatan_satker($this->input->get_post(null),$data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/kegiatan'));
            }

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();

            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $data['program']  = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->db->where('id', $data['id']);
            $data['user']  = $this->db->get('support.kegiatan_satker')->row_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan,resiko,p_resiko,da_resiko');
            $data['resiko_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.kegiatan_kr')->num_rows();

            $this->template->user('updatekegiatan', $data, array('title' => $data['user']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Perbarui Kegiatan Satker')));
            return;
        }
        elseif ($this->input->get_post('ev')) {
//            echo 's';die();
            check_access($menu, 'c');
            $data['status_update'] = $this->user->update_anal_kegiatan_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/kegiatan'));
        }
        elseif ($this->input->get_post('upval')) {
//            echo "s";die();
            check_access($menu, 'c');
            $data['status_update'] = $this->user->update_eval_kegiatan_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/kegiatan'));
        }
        elseif ($this->input->get_post('anal')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('anal');
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['keg'] = $this->db->get('master.kegiatan')->result_array();
            $this->db->order_by('ket');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $data['program']  = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->db->where('k.id', $data['id']);
            $this->db->select('mk.*');
            $this->db->join('master.kegiatan mk', 'mk.id=k.kegiatan', 'left');
            $data['users']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('k.id', $data['id']);
            $data['user']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('id,urutan,resiko,p_resiko,da_resiko,sk,sd');
            $data['resiko_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.kegiatan_kr')->num_rows();
//            echo json_encode($data['jml_kr']);die();

            $this->template->user('updateanal', $data, array('title' => 'Analisa - '.$data['users']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Update Analaisa')));
            return;
        }
        elseif ($this->input->get_post('eval')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('eval');
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $data['program']  = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();


            $this->db->where('k.id', $data['id']);
            $this->db->select('mk.*');
            $this->db->join('master.kegiatan mk', 'mk.id=k.kegiatan', 'left');
            $data['users']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('k.id', $data['id']);
            $data['user']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('id', $data['id']);
            $data['resiko_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.kegiatan_kr')->num_rows();

            $this->template->user('updateeval', $data, array('title' => 'Evaluasi - '.$data['users']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Update Analaisa')));
            return;
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_kegiatan_satker_status($this->input->get_post('status'));
            return;
        }
        elseif ($this->input->get_post('resiko')) {
            $no=$this->input->get_post('resiko');
            check_access($menu, 'c');

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $pe_resiko = $this->db->get('master.penyebab_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();

            $this->db->order_by('kode');
            $this->db->where('status', true);
            $da_resiko = $this->db->get('master.dampak')->result_array();
            ?>
            <div class="form-group <?=$no?>">
                <h3>Resiko <button type="button" onclick="tambah_resiko(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h3> <br>

                <div class="form-group resiko<?=$no?>">

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Resiko<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="resiko<?=$no?>" data-placeholder="Pilih Tipe Resiko" required>
                                <option value="">Pilih Tipe Resiko</option>
                                <?php
                                foreach ($tipe_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Penyebab Resiko<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="p_resiko<?=$no?>" data-placeholder="Pilih Penyebab Resiko" required>
                                <option value="">Pilih Penyebab Resiko</option>
                                <?php
                                foreach ($pe_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[kode]" ?> - <?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div><div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Dampak Resiko<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="da_resiko1" data-placeholder="Pilih Dampak Resiko" required>
                                <option value="">Pilih Dampak Resiko</option>
                                <?php
                                foreach ($da_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-sm-offset-1">
                        <div class="form-group row">
                            <h5>Kendali <button type="button" onclick="tambah_kendali(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h5> <br>
                        </div>
                        <div class="form-group kendali<?=$no?>">

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" name="kendali_s<?=$no?>[]" data-placeholder="Pilih Kendali Seharusnya" required>
                                        <option value="">Pilih Kendali Sementara</option>
                                        <?php
                                        foreach ($kendali as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" name="kendali_a<?=$no?>[]" data-placeholder="Pilih Kendali Yang Ada" required>
                                        <option value="">Pilih Kendali Yang Ada</option>
                                        <?php
                                        foreach ($kendali as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Belum Ada<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" name="kendali_ba<?=$no?>[]" data-placeholder="Pilih Kendali Yang Belum Ada" required>
                                        <option value="">Pilih Kendali Yang Belum Ada</option>
                                        <?php
                                        foreach ($kendali as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            return;
        }
        elseif ($this->input->get_post('kegiatan')) {
            $id=$this->input->get_post('kegiatan');

            $this->db->where('id', $id);
            $data['jml'] = $this->db->get('support.setting_kegiatan')->row_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $konteks = $this->db->get('support.konteks_kegiatan')->result_array();
            echo '<select class="select2 form-control\" name="konteks" id="konteks" onchange="kontex()" data-placeholder="Pilih Konteks Kegiatan" required>
                        <option value="">Pilih Konteks Kegiatan</option>';
                foreach ($konteks as $r) {
                    echo '<option value="'.$r['id'].'">'.$r['nama'].'</option>';
                }
                echo'</select>';
            return;
        }
        elseif ($this->input->get_post('konteks')) {
            $id=$this->input->get_post('konteks');
            $no=$this->input->get_post('no');

            $this->db->where('id', $id);
            $data['jml'] = $this->db->get('support.setting_kegiatan')->row_array();
            $this->db->where('id', $id);
            $resiko_kr= $this->db->get('support.konteks_kegiatand')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $pe_resiko = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $da_resiko = $this->db->get('master.dampak')->result_array();
//            $no=1;
                foreach ($resiko_kr as $r) {
                    ?>
                    <div class="form-group <?= $no ?>">
                        <h3>Resiko <a onclick="rem(<?=$no?>)" class="btn btn-xs btn-inverse"> - </a> </h3> <br>

                        <div class="risk form-group resiko<?= $no ?>">

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Resiko <span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <input type="hidden" name="urutan" value="<?=$no?>">
                                    <select class="select2 form-control select2-multiple" readonly name="resiko<?= $no ?>"
                                            data-placeholder="Pilih Tipe Resiko">
                                        <option value="">Pilih Tipe Resiko</option>
                                        <?php
                                        foreach ($tipe_resiko as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>" <?= @$r['resiko'] == $v['id'] ? 'selected' : '' ?> ><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Penyebab Resiko<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" readonly name="p_resiko<?= $no ?>"
                                            data-placeholder="Pilih Penyebab Resiko">
                                        <option value="">Pilih Penyebab Resiko</option>
                                        <?php
                                        foreach ($pe_resiko as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>" <?= @$r['p_resiko'] == $v['id'] ? 'selected' : '' ?>><?= "$v[kode]" ?>
                                                - <?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Dampak Resiko<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" readonly name="da_resiko<?= $no ?>"
                                            data-placeholder="Pilih Dampak Resiko">
                                        <option value="">Pilih Dampak Resiko</option>
                                        <?php
                                        foreach ($da_resiko as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>" <?= @$r['da_resiko'] == $v['id'] ? 'selected' : '' ?> ><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>

                        <div class="form-group kendali<?= $no ?>">
                            <?php
//                                if($r['urutan']==$no){
                                    ?>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya
                                        <span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control select2-multiple" multiple="multiple"
                                                multiple data-placeholder="Pilih Kendali Seharusnya"
                                                name="kendali_s<?= $no ?>[]" readonly="">
                                            <option value="">Pilih Kendali Seharusnya</option>
                                            <?php
                                            foreach ($kendali as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>" <?= in_array($v['id'], json_decode($r['kendali_s'], true)) ? 'selected' : '' ?>><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div><div class="form-group row">
                                    <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada
                                        <span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="hidden" name="resiko<?= $no ?>" value="<?=@$r['resiko']?>">
                                        <select class="select2 form-control select2-multiple" multiple="multiple"
                                                multiple data-placeholder="Pilih Kendali Yang Ada"
                                                name="kendali_a<?= $no ?>[]">
                                            <?php
                                            foreach ($kendali as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
//                    }
            $no++;
                }
            return;
        }
        elseif ($this->input->get_post('kendali')) {
            $no=$this->input->get_post('kendali');
            check_access($menu, 'c');

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();
            ?>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span
                        class="text-danger">*</span></label>
                <div class="col-sm-6">
                    <select class="select2 form-control select2-multiple" name="kendali_s<?=$no?>[]" data-placeholder="Pilih Kendali Seharusnya" required>
                        <option value="">Pilih Kendali Sementara</option>
                        <?php
                        foreach ($kendali as $v):
                            ?>
                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada<span
                        class="text-danger">*</span></label>
                <div class="col-sm-6">
                    <select class="select2 form-control select2-multiple" name="kendali_a<?=$no?>[]" data-placeholder="Pilih Kendali Yang Ada" required>
                        <option value="">Pilih Kendali Yang Ada</option>
                        <?php
                        foreach ($kendali as $v):
                            ?>
                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Belum Ada<span
                            class="text-danger">*</span></label>
                <div class="col-sm-6">
                    <select class="select2 form-control select2-multiple" name="kendali_ba<?=$no?>[]" data-placeholder="Pilih Kendali Yang Belum Ada" required>
                        <option value="">Pilih Kendali Yang Belum Ada</option>
                        <?php
                        foreach ($kendali as $v):
                            ?>
                            <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <?php
            return;
        }
        else{
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->db->order_by('nama','desc');
            $data['p_sekarang'] = $this->db->get('periode')->row_array();
            $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : $data['p_sekarang']['id'];
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
            $data['pkpt'] = $this->user->get_list_k_satker($data['closed'],$data['periode_s']);
            $this->template->user('kegiatan_satker', $data, array('title' => 'Kegiatan Satker', 'breadcrumbs' => array('Manajemen Resiko')));
        }
       }
    public function n_program()
    {
        $menu = '388deab8-36ff-4a8b-a1d7-b88eab8b4c26';
        $data['access'] = list_access($menu);
        $this->load->model('user');

        if($this->input->get_post('save')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_program_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/n_program'));
        }
        if ($this->input->get_post('i')) {
            check_access($menu, 'c');
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['keg'] = $this->db->get('master.kegiatan')->result_array();
            $this->db->order_by('ket');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['konteks'] = $this->db->get('support.konteks_program')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['program'] = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->template->user('n_updateprogram', $data, array('title' => 'Tambah Data Program Satker', 'breadcrumbs' => array('Manajemen Resiko', 'Tambah Program Satker')));
            return;
        }
        elseif ($this->input->get_post('e')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('e');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->update_kegiatan_satker($this->input->get_post(null),$data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/kegiatan'));
            }

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();

            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $data['program']  = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->db->where('id', $data['id']);
            $data['user']  = $this->db->get('support.kegiatan_satker')->row_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan,resiko,p_resiko,da_resiko');
            $data['resiko_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.kegiatan_kr')->num_rows();

            $this->template->user('updatekegiatan', $data, array('title' => $data['user']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Perbarui Kegiatan Satker')));
            return;
        }
        elseif ($this->input->get_post('ev')) {
//            echo 's';die();
            check_access($menu, 'c');
            $data['status_update'] = $this->user->update_anal_kegiatan_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/program'));
        }
        elseif ($this->input->get_post('upval')) {
//            echo "s";die();
            check_access($menu, 'c');
            $data['status_update'] = $this->user->update_eval_kegiatan_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/program'));
        }
        elseif ($this->input->get_post('anal')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('anal');
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['keg'] = $this->db->get('master.program')->result_array();
            $this->db->order_by('ket');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $data['program']  = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->db->where('k.id', $data['id']);
            $this->db->select('mk.*');
            $this->db->join('master.program mk', 'mk.id=k.program', 'left');
            $data['users']  = $this->db->get('support.program_satker k')->row_array();
            $this->db->where('k.id', $data['id']);
            $data['user']  = $this->db->get('support.program_satker k')->row_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('id,urutan,resiko,p_resiko,da_resiko,sk,sd');
            $data['resiko_kr']  = $this->db->get('support.program_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.program_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.program_kr')->num_rows();

            $this->template->user('updateanal', $data, array('title' => 'Analisa - '.$data['users']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Update Analaisa')));
            return;
        }
        elseif ($this->input->get_post('eval')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('eval');
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $data['program']  = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();


            $this->db->where('k.id', $data['id']);
            $this->db->select('mk.*');
            $this->db->join('master.kegiatan mk', 'mk.id=k.kegiatan', 'left');
            $data['users']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('k.id', $data['id']);
            $data['user']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('id', $data['id']);
            $data['resiko_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.kegiatan_kr')->num_rows();

            $this->template->user('updateeval', $data, array('title' => 'Evaluasi - '.$data['users']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Update Analaisa')));
            return;
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_kegiatan_satker_status($this->input->get_post('status'));
            return;
        }
        elseif ($this->input->get_post('resiko')) {
            $no=$this->input->get_post('resiko');
            check_access($menu, 'c');

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $pe_resiko = $this->db->get('master.penyebab_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();

            $this->db->order_by('kode');
            $this->db->where('status', true);
            $da_resiko = $this->db->get('master.dampak')->result_array();
            ?>
            <div class="form-group <?=$no?>">
                <h3>Resiko <button type="button" onclick="tambah_resiko(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h3> <br>

                <div class="form-group resiko<?=$no?>">

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Resiko<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="resiko<?=$no?>" data-placeholder="Pilih Tipe Resiko" required>
                                <option value="">Pilih Tipe Resiko</option>
                                <?php
                                foreach ($tipe_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Penyebab Resiko<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="p_resiko<?=$no?>" data-placeholder="Pilih Penyebab Resiko" required>
                                <option value="">Pilih Penyebab Resiko</option>
                                <?php
                                foreach ($pe_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[kode]" ?> - <?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div><div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Dampak Resiko<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="da_resiko1" data-placeholder="Pilih Dampak Resiko" required>
                                <option value="">Pilih Dampak Resiko</option>
                                <?php
                                foreach ($da_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-sm-offset-1">
                        <div class="form-group row">
                            <h5>Kendali <button type="button" onclick="tambah_kendali(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h5> <br>
                        </div>
                        <div class="form-group kendali<?=$no?>">

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" name="kendali_s<?=$no?>[]" data-placeholder="Pilih Kendali Seharusnya" required>
                                        <option value="">Pilih Kendali Sementara</option>
                                        <?php
                                        foreach ($kendali as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" name="kendali_a<?=$no?>[]" data-placeholder="Pilih Kendali Yang Ada" required>
                                        <option value="">Pilih Kendali Yang Ada</option>
                                        <?php
                                        foreach ($kendali as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Belum Ada<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" name="kendali_ba<?=$no?>[]" data-placeholder="Pilih Kendali Yang Belum Ada" required>
                                        <option value="">Pilih Kendali Yang Belum Ada</option>
                                        <?php
                                        foreach ($kendali as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            return;
        }
        elseif ($this->input->get_post('kegiatan')) {
            $id=$this->input->get_post('kegiatan');

            $this->db->where('id', $id);
            $data['jml'] = $this->db->get('support.setting_kegiatan')->row_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $konteks = $this->db->get('support.konteks_kegiatan')->result_array();
            echo '<select class="select2 form-control\" name="konteks" id="konteks" onchange="kontex()" data-placeholder="Pilih Konteks Kegiatan" required>
                        <option value="">Pilih Konteks Kegiatan</option>';
                foreach ($konteks as $r) {
                    echo '<option value="'.$r['id'].'">'.$r['nama'].'</option>';
                }
                echo'</select>';
            return;
        }
        elseif ($this->input->get_post('konteks')) {
            $id=$this->input->get_post('konteks');
            $no=$this->input->get_post('no');

            $this->db->where('id', $id);
            $data['jml'] = $this->db->get('support.setting_program')->row_array();
            $this->db->where('id', $id);
            $resiko_kr= $this->db->get('support.konteks_programd')->result_array();
//echo json_encode($resiko_kr);die();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $pe_resiko = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $da_resiko = $this->db->get('master.dampak')->result_array();
//            $no=1;
                foreach ($resiko_kr as $r) {
                    ?>
                    <div class="form-group <?= $no ?>">
                        <h3>Resiko <a onclick="rem(<?=$no?>)" class="btn btn-xs btn-inverse"> - </a> </h3> <br>

                        <div class="risk form-group resiko<?= $no ?>">

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Resiko <span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <input type="hidden" name="urutan" value="<?=$no?>">
                                    <select class="select2 form-control select2-multiple" disabled name="resiko<?= $no ?>"
                                            data-placeholder="Pilih Tipe Resiko">
                                        <option value="">Pilih Tipe Resiko</option>
                                        <?php
                                        foreach ($tipe_resiko as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>" <?= @$r['resiko'] == $v['id'] ? 'selected' : '' ?> ><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Penyebab Resiko<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" disabled name="p_resiko<?= $no ?>"
                                            data-placeholder="Pilih Penyebab Resiko">
                                        <option value="">Pilih Penyebab Resiko</option>
                                        <?php
                                        foreach ($pe_resiko as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>" <?= @$r['p_resiko'] == $v['id'] ? 'selected' : '' ?>><?= "$v[kode]" ?>
                                                - <?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Dampak Resiko<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" disabled name="da_resiko<?= $no ?>"
                                            data-placeholder="Pilih Dampak Resiko">
                                        <option value="">Pilih Dampak Resiko</option>
                                        <?php
                                        foreach ($da_resiko as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>" <?= @$r['da_resiko'] == $v['id'] ? 'selected' : '' ?> ><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>

                        <div class="form-group kendali<?= $no ?>">
                            <?php
//                                if($r['urutan']==$no){
                                    ?>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya
                                        <span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control select2-multiple" multiple="multiple"
                                                multiple data-placeholder="Pilih Kendali Seharusnya"
                                                name="kendali_s<?= $no ?>[]" disabled>
                                            <option value="">Pilih Kendali Seharusnya</option>
                                            <?php
                                            foreach ($kendali as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>" <?= in_array($v['id'], json_decode($r['kendali_s'], true)) ? 'selected' : '' ?>><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div><div class="form-group row">
                                    <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada
                                        <span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="hidden" name="resiko<?= $no ?>" value="<?=@$r['resiko']?>">
                                        <select class="select2 form-control select2-multiple" multiple="multiple"
                                                multiple data-placeholder="Pilih Kendali Yang Ada"
                                                name="kendali_a<?= $no ?>[]">
                                            <?php
                                            foreach ($kendali as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
//                    }
            $no++;
                }
            ?>
            <input type="hidden" name="jml_program" required class="form-control" id="jml_program" value="<?=@$no-1?>">

            <?php
            return;
        }
        elseif ($this->input->get_post('kendali')) {
            $no=$this->input->get_post('kendali');
            check_access($menu, 'c');

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();
            ?>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span
                        class="text-danger">*</span></label>
                <div class="col-sm-6">
                    <select class="select2 form-control select2-multiple" name="kendali_s<?=$no?>[]" data-placeholder="Pilih Kendali Seharusnya" required>
                        <option value="">Pilih Kendali Sementara</option>
                        <?php
                        foreach ($kendali as $v):
                            ?>
                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada<span
                        class="text-danger">*</span></label>
                <div class="col-sm-6">
                    <select class="select2 form-control select2-multiple" name="kendali_a<?=$no?>[]" data-placeholder="Pilih Kendali Yang Ada" required>
                        <option value="">Pilih Kendali Yang Ada</option>
                        <?php
                        foreach ($kendali as $v):
                            ?>
                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Belum Ada<span
                            class="text-danger">*</span></label>
                <div class="col-sm-6">
                    <select class="select2 form-control select2-multiple" name="kendali_ba<?=$no?>[]" data-placeholder="Pilih Kendali Yang Belum Ada" required>
                        <option value="">Pilih Kendali Yang Belum Ada</option>
                        <?php
                        foreach ($kendali as $v):
                            ?>
                            <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <?php
            return;
        }
        else{
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->db->order_by('nama','desc');
            $data['p_sekarang'] = $this->db->get('periode')->row_array();
            $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : $data['p_sekarang']['id'];
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
            $data['pkpt'] = $this->user->get_list_p_satker($data['closed'],$data['periode_s']);
            $this->template->user('program_satker', $data, array('title' => 'Program Satker', 'breadcrumbs' => array('Manajemen Resiko')));
        }
       }
    public function opsi()
    {
        $menu = '0d59e730-4d54-4ef2-a892-2bdb88f4e7ab';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('opsi')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('opsi');
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['opsi'] = $this->db->get('master.opsi')->result_array();
//            echo json_encode($data['opsi']);die();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['tindak'] = $this->db->get('master.tindak')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['target'] = $this->db->get('master.target')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['keg'] = $this->db->get('master.kegiatan')->result_array();
            $this->db->order_by('ket');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $data['program']  = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->db->where('k.id', $data['id']);
            $this->db->select('mk.*');
            $this->db->join('master.kegiatan mk', 'mk.id=k.kegiatan', 'left');
            $data['users']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('k.id', $data['id']);
            $data['user']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('id', $data['id']);
            $data['resiko_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.kegiatan_kr')->num_rows();

            $this->template->user('updateopsi', $data, array('title' => 'Opsi - '.$data['users']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Update Analaisa')));
            return;
        }
        elseif ($this->input->get_post('o')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->update_opsi_kegiatan_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/opsi'));
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_kegiatan_satker_status($this->input->get_post('status'));
            return;
        }
        else{
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->db->order_by('nama','desc');
            $data['p_sekarang'] = $this->db->get('periode')->row_array();
            $data['opsi'] = true;
            $yes=1;
            $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : $data['p_sekarang']['id'];
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
            $data['pkpt'] = $this->user->get_list_k_satker($data['closed'],$data['periode_s'],$yes);
            $this->template->user('kegiatan_satker', $data, array('title' => 'Opsi & Mitigasi', 'breadcrumbs' => array('Manajemen Resiko')));
        }
       }
    public function monitoring()
    {
        $menu = '0d59e730-4d54-4ef2-a892-2bdb88f4e7ac';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('monitoring')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('monitoring');
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['opsi'] = $this->db->get('master.opsi')->result_array();
//            echo json_encode($data['opsi']);die();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['tindak'] = $this->db->get('master.tindak')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['target'] = $this->db->get('master.target')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['keg'] = $this->db->get('master.kegiatan')->result_array();
            $this->db->order_by('ket');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $data['program']  = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->db->where('k.id', $data['id']);
            $this->db->select('mk.*');
            $this->db->join('master.kegiatan mk', 'mk.id=k.kegiatan', 'left');
            $data['users']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('k.id', $data['id']);
            $data['user']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('id', $data['id']);
            $data['resiko_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.kegiatan_kr')->num_rows();

            $this->template->user('updatemonitoring', $data, array('title' => 'Monitoring - '.$data['users']['nama'], 'breadcrumbs' => array('Manajemen Resiko')));
            return;
        }
        elseif ($this->input->get_post('m')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->update_monitoring_kegiatan_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/monitoring'));
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_kegiatan_satker_status($this->input->get_post('status'));
            return;
        }
        else{
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->db->order_by('nama','desc');
            $data['p_sekarang'] = $this->db->get('periode')->row_array();
            $data['monitoring'] = true;
            $yes=2;
            $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : $data['p_sekarang']['id'];
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
            $data['pkpt'] = $this->user->get_list_k_satker($data['closed'],$data['periode_s'],$yes);
            $this->template->user('kegiatan_satker', $data, array('title' => 'Monitoring', 'breadcrumbs' => array('Manajemen Resiko')));
        }
       }
    public function kejadian()
    {
        $menu = 'cb7ac7ac-9d00-4acd-a1fd-2afefb0c8a81';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->insert_kejadian_satker($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/kejadian'));
            }
            $this->db->order_by('id','desc');
            $this->db->where('status', true);
            $data['periodes'] = $this->db->get('periode')->row_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('nama');
            $this->db->where('periode', $data['periodes']['id']);
            $this->db->where('status', true);
            $data['program'] = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('periode', $data['periodes']['id']);
            $this->db->where('status', true);
            $data['kegiatan'] = $this->db->get('support.kegiatan_satker')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->template->user('updatekejadian', $data, array('title' => 'Tambah Data Kejadian Satker', 'breadcrumbs' => array('Manajemen Resiko', 'Tambah Kejadian Satker')));
            return;
        }
        elseif ($this->input->get_post('e')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('e');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->update_kejadian_satker($this->input->get_post(null),$data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/kejadian'));
            }

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('id','desc');
            $this->db->where('status', true);
            $data['periodes'] = $this->db->get('periode')->row_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $this->db->where('periode', $data['periodes']['id']);
            $data['program']  = $this->db->get('support.program_satker')->result_array();

            $this->db->order_by('nama');
            $this->db->where('periode', $data['periodes']['id']);
            $this->db->where('status', true);
            $data['kegiatan'] = $this->db->get('support.kegiatan_satker')->result_array();
            $this->db->where('id', $data['id']);
            $data['user']  = $this->db->get('support.kejadian_satker')->row_array();
//            echo $data['user']['hitung'];die();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->template->user('updatekejadian', $data, array('title' => $data['user']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Perbarui Kejadian Satker')));
            return;
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_kejadian_satker_status($this->input->get_post('status'));
            return;
        }
        elseif ($this->input->get_post('periodes')) {
            check_access($menu, 'u');
            $this->user->set_kejadian_satker_status($this->input->get_post('status'));
            $data['programs']='';
            echo json_encode($data);
            return;
        }
        else{
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->db->order_by('nama','desc');
            $data['p_sekarang'] = $this->db->get('periode')->row_array();
            $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : $data['p_sekarang']['id'];
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
            $data['pkpt'] = $this->user->get_list_ke_satker($data['closed'],$data['periode_s']);
            $this->template->user('kejadian_satker', $data, array('title' => 'Kejadian Satker', 'breadcrumbs' => array('Manajemen Resiko')));
        }
       }

    public function test(){
        $this->template->user('excel','', array('title' => 'Group Access', 'breadcrumbs' => array('Master', 'User')));
    }
    public function upload(){
        $this->db->trans_begin();
        $config['upload_path'] = './img/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload("file")) {
            $data['status_update'] = 'error';
            $data['status_message'] =  $this->upload->display_errors('', '');
        } else if($this->upload->do_upload("file")){
            $file = $this->upload->data();
            $full_path=$file["full_path"];

            $this->load->library('PHPExcel');
            $objPHPExcel = PHPExcel_IOFactory::load($full_path);
            $max=$objPHPExcel->getActiveSheet(0)->getHighestRow();
            set_time_limit(0);
            for($no=1;$no<$max;$no++){
                $row['A']=$objPHPExcel->getActiveSheet()->getCell("A".$no)->getValue();//kode
                $row['B']=$objPHPExcel->getActiveSheet()->getCell("B".$no)->getValue();//nama
                $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
                $id=$raw['id'];
                $this->db->insert('master.satker',array(
                    'id'=>$id,
                    'kode'=>$row['A'],
                    'nama'=>$row['B'],
                    'status'=>'t',
                ));
            }
        }
        if ($this->db->trans_status()){
            $this->db->trans_commit();
            echo "ok";
        }else{
            echo "fail";
        }
        redirect(base_url());
    }
}
