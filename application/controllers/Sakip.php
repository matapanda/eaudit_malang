<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sakip extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->template->set_view('master');
    }

    // public function sakip()
	// {
		//$this->load->library('curl');
		//echo 'Hi Sitaru!';
		//echo $this->curl->simple_get('https://api.github.com/repos/guzzle/guzzle');

		//$data = $this->sitaru->query("select * from persyaratan")->result_array();
		//echo "<pre>", print_r($data), "</pre>";

		// $this->load->library('sitaru');
		// $this->sitaru->Send('18884', 'krk');
	// }


    public function index()
    {

        $this->load->library('curl');
		echo $this->curl->simple_get('https://e-sakip.banyuwangikab.go.id/api/perencanaan/2021');
		echo 'Hi!';

        
       
    }
	public function test(){
        // $this->template->user('excel','', array('title' => 'Group Access', 'breadcrumbs' => array('Master', 'User')));
    }
	public function perencanaan($tahun='')
	{
		$tahun = $tahun != '' ? $tahun : date('Y');
		$this->load->library('curl');
		$rencana = $this->curl->simple_get('https://e-sakip.banyuwangikab.go.id/api/perencanaan/'.$tahun);
		$data['perencanaan'] = json_decode($rencana, TRUE);
		$data['tahun'] = $tahun;

		$this->template->user('sakip_perencanaan', $data, array('title' => 'Perencanaan', 'breadcrumbs' => array('Master', 'User')));
	}

	public function pengukuran($tahun='')
	{
		$tahun = $tahun != '' ? $tahun : date('Y');
		$this->load->library('curl');
		$ukur = $this->curl->simple_get('https://e-sakip.banyuwangikab.go.id/api/pengukuran/'.$tahun);
		$data['ukur'] = json_decode($ukur, TRUE);
		$data['tahun'] = $tahun;

		$this->template->user('sakip_pengukuran', $data, array('title' => 'Pengukuran', 'breadcrumbs' => array('Master', 'User')));
	}

	public function pelaporan($tahun='')
	{
		$tahun = $tahun != '' ? $tahun : date('Y');
		$this->load->library('curl');
		$lapor = $this->curl->simple_get('https://e-sakip.banyuwangikab.go.id/api/pelaporan/'.$tahun);
		$data['lapor'] = json_decode($lapor, TRUE);
		$data['tahun'] = $tahun;

		$this->template->user('sakip_pelaporan', $data, array('title' => 'Pelaporan', 'breadcrumbs' => array('Master', 'User')));
	}

	public function evaluasi($tahun='')
	{
		$tahun = $tahun != '' ? $tahun : date('Y');
		$this->load->library('curl');
		$evaluasi = $this->curl->simple_get('https://e-sakip.banyuwangikab.go.id/api/evaluasi/'.$tahun);
		$data['evaluasi'] = json_decode($evaluasi, TRUE);
		$data['tahun'] = $tahun;

		$this->template->user('sakip_evaluasi', $data, array('title' => 'Evaluasi', 'breadcrumbs' => array('Master', 'User')));
	}

	public function prestasi($tahun='')
	{
		$tahun = $tahun != '' ? $tahun : date('Y');
		$this->load->library('curl');
		$prestasi = $this->curl->simple_get('https://e-sakip.banyuwangikab.go.id/api/prestasi/'.$tahun);
		$data['prestasi'] = json_decode($prestasi, TRUE);
		$data['tahun'] = $tahun;

		$this->template->user('sakip_prestasi', $data, array('title' => 'Prestasi', 'breadcrumbs' => array('Master', 'User')));
	}

    
}
