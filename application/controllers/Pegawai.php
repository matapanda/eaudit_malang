<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->template->set_view('master');
		$this->load->library('curl');
    }

    public function index()
    {        
		$satker = $this->curl->simple_get('http://presensi.banyuwangikab.go.id/api/inspektorat/unors');  
		$satker = json_decode($satker, TRUE);
		$satker = [];
		//echo "<pre>", print_r($satker), "</pre>"; return;

		$data = array(
			'satker' => $satker,
		);

		$this->template->user('pegawai', $data, array('title' => 'Pegawai', 'breadcrumbs' => array('Master', 'Pegawai')));
    }
	
	public function pegawai_list($unor)
	{
		$pegawai = $this->curl->simple_get('http://presensi.banyuwangikab.go.id/api/inspektorat/pegawai?unor='.$unor);
		$pegawai = json_decode($pegawai, TRUE);
		$pegawai = [];
		
		$this->output
			->set_content_type('application/json')
            ->set_output(json_encode($pegawai));
	}    
}
