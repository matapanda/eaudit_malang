<div class="row">
    <div class="col-sm-12">
        <div class="card-box row"><h2 class="visible-print center">Rekap Temuan Dan Tindak Lajut</h2>
            <form method="get" class="row hidden-print" action="?">
                <div class="col-md-1">
                    <div class="dataTables_wrapper form-inline">
                        <label>Tahun :</label>
                        <select name="tahun" onchange="this.form.submit()" class="form-control input-sm">
                            <?php
                            foreach($list_tahun as $l){
                                echo "<option value='$l[tahun]' ".($l['tahun']==$tahun?'selected=""':'').">$l[tahun]</option>";
                            }
                            ?>
                        </select>

                    </div>
<!--                        <button type="button" onclick="window.print()" class="btn btn-inverse hidden-print hidden-xs"><span class="ace-icon fa fa-print icon-on-right bigger-110"></span> PRINT</button>-->
                </div> <div class="col-md-2">
                    <div class="dataTables_wrapper form-inline">
                        <label>SKPD :</label>
                        <select name="skpd" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="" <?=@$skpd==''?'selected':''?>> All</option>
                            <?php
                            foreach($list_skpd as $l){
                                echo "<option value='$l[skpd]' ".($l['skpd']==$skpd?'selected=""':'').">$l[skpd]</option>";
                            }
                            ?>
                        </select>

                    </div>
<!--                        <button type="button" onclick="window.print()" class="btn btn-inverse hidden-print hidden-xs"><span class="ace-icon fa fa-print icon-on-right bigger-110"></span> PRINT</button>-->
                </div>  <div class="col-md-1">
                    <div class="dataTables_wrapper form-inline">
                        <label>Status :</label>
                        <select name="status" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="" <?=@$sts==''?'selected':''?>> All</option>
                            <option value="ts" <?=@$sts=='ts'?'selected':''?>> TS</option>
                            <option value="tt" <?=@$sts=='tt'?'selected':''?>> TT</option>
                            <option value="bt" <?=@$sts=='bt'?'selected':''?>> BT</option>
                            <option value="tb" <?=@$sts=='tb'?'selected':''?>> TB</option>
                        </select>
                    </div>
<!--                        <button type="button" onclick="window.print()" class="btn btn-inverse hidden-print hidden-xs"><span class="ace-icon fa fa-print icon-on-right bigger-110"></span> PRINT</button>-->
                </div>
            </form>
            <div class="col-xs-12">
                <hr>
                <?php
                if(isset($temuan)){
                ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="center hidden-print col-xs-1">No</th>
                            <th class="center col-xs-1">No LHP</th>
                            <th class="center col-xs-1">Tgl LHP</th>
                            <th class="center col-xs-1">SKPD</th>
                            <th class="center col-xs-1">Kode Temuan</th>
                            <th class="center col-xs-1 ">Temuan</th>
                            <th class="center col-xs-1 ">Nilai Temuan</th>
                            <th class="center col-xs-1 ">Kode Rekomendasi</th>
                            <th class="center col-xs-1 ">Rekomendasi</th>
                            <th class="center col-xs-1 ">Nilai Rekomendasi</th>
                            <th class="center col-xs-1 ">Tindak Lanjut</th>
                            <th class="center col-xs-1 ">Nilai Tindak Lanjut</th>
                            <th class="center col-xs-1 ">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1;
                        foreach ($temuan as $i=>$r) {
                            ?>
                            <tr>
                                <td class="center"><?=$no;?></td>
                                <td class="center"><?=$r['no_lhp'];?></td>
                                <td class="center"><?=format_waktu($r['created_at'])?></td>
                                <td class="center"><?=$r['skpd'];?></td>
                                <td class="center"><?=$r['kode_t']?></td>
                                <td class="center"><?=$r['temuan']?></td>
                                <td class="center">
                                    <a href="#" id="nilait<?=$no?>"><?=($r['nilai_temuan'])?></a>
                                    <script>
                                        $(function () {
                                            $('#nilait<?=$no?>').editable({
                                                type: 'text',
                                                name: 'nilai_temuan',
                                                pk:'<?=$r['increment']?>',
                                                url: '?',
                                                title: 'nilai temuan'
                                            });
                                        });
                                    </script>
                                </td>
<!--                                <td class="center">--><?//=$r['nilai_temuan']?><!--</td>-->
                                <td class="center"><?=$r['kode_r']?></td>
                                <td class="center"><?=$r['rekom']?></td>
                                <td class="center">
                                    <a href="#" id="nilair<?=$no?>"><?=($r['nilai_rekom'])?></a>
                                    <script>
                                        $(function () {
                                            $('#nilair<?=$no?>').editable({
                                                type: 'text',
                                                name: 'nilai_rekom',
                                                pk:'<?=$r['increment_r']?>',
                                                url: '?',
                                                title: 'nilai rekom'
                                            });
                                        });
                                    </script>
<!--                                    --><?//=format_uang($r['nk1'])?>
                                </td>
                                <td class="center"><?=$r['uraian_tl']?></td>
                                <td class="center">
                                    <a href="#" id="nilaitl<?=$no?>"><?=($r['nilai_tl'])?></a>
                                    <script>
                                        $(function () {
                                            $('#nilaitl<?=$no?>').editable({
                                                type: 'text',
                                                name: 'nilai_tl',
                                                pk:'<?=$r['increment_r']?>',
                                                url: '?',
                                                title: 'nilai tindak lanjut'
                                            });
                                        });
                                    </script>
                                    <!--                                    --><?//=format_uang($r['nk1'])?>
                                </td>
                                <td class="">
                                    <a href='#' class="emjes-editable" id='status<?=$no?>' data-type='select2' data-pk='<?=$r['increment_r']?>'></a>
                                    <script>
                                        $(function(){
                                            $('#status<?=$no?>').editable({
                                                url: '?',
                                                mode:'inline',
                                                name: 'status_rekom',
                                                title:'Status Rekom',
                                                source:<?=json_encode($irban2)?>
                                            });
                                            $("#status<?=$no?>").editable('setValue', '<?=$r['status_rekom']?>');
                                        });
                                    </script>
                                </td>
                            </tr>
                            <?php
                        $no++;}
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.full.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!--<script type="text/javascript" src="--><?//=base_url()?><!--assets/plugins/select2/js/select2.full.min.js"></script>-->
<script>

    $.fn.editable.defaults.mode = 'popup';

    function detail(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(1).text()+' - '+$('tr#'+_id).children('td').eq(2).text();
        $('.modal-title',_modal).html(_title);
        $.post('?tao='+_id,function(data,status){
            $('.modal-body',_modal).html(data);
            _modal.modal('show');
        });
    }
    $('select').select2();
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    $('.btn-submit-data').on('click',function (e) {
        var _typ=$(this).data('type');
        if(_typ=='post'){
            $($(this).closest('form')).attr('target','_self');
            $($(this).closest('form')).attr('action','');
        }else{
            var _act=$(this).data('mode');
            $($(this).closest('form')).attr('target','_blank');
            $('[name=type]',$(this).closest('form')).val(_act);
            $($(this).closest('form')).attr('action',"<?=base_url('export/stok')?>");
        }
        $($(this).closest('form')).attr('method',_typ);
        $('[type=submit]',$(this).closest('form')).click();
    });
</script>
