<br>
<br>
<br>
<table width="100%" style="text-align: justify;vertical-align: text-top;">
    <tr>
        <td colspan="3">
            ___________
        </td>
    </tr>
    <tr>
        <td width="20%">
        Lampiran Surat Nomor
                </td>
        <td width="10%">
        :
        </td>
        <td width="10%">
        <?=$laporan['no_p']?>
        </td>
    </tr>
    <tr>
        <td width="20%">
        Tanggal
                </td>
        <td width="10%">
        :
        </td>
        <td width="10%">
            <?=date("d M Y", strtotime($laporan['date']));?>
        </td>
    </tr>
</table>
<div class="center" style="width: 100%">
    <h5>DAFTAR DATA YANG DIPERLUKAN</h5>
</div>
<?php
$no=1;
    foreach ($data as $c) {
        ?>
        <?=$no?>. <?=$c['isian']?><br>
        <?php
    $no++;
    }
    ?>
<htmlpagefooter name="footer">
    <img src="./assets/barc.jpg" style="width:10%;">

</htmlpagefooter>
<style>
    @page {
        footer: html_footer;
        header: html_header;
    }

    @page

    * {
        margin-top: 1.8cm;
        margin-bottom: 2cm;
        margin-left: 2.5cm;
        margin-right: 5cm;
    }

    .list {
        width: 100%;
        margin-top: 20px;
        margin-bottom: 10px;
        border-bottom: 1px solid black;
    }

    th {
        margin: 10px;
        text-transform: uppercase;
    }

    .right {
        text-align: right;
    }

    .center {
        text-align: center;
    }

    th.line {
        border-bottom: 1px solid black;
    }

    .title {
        margin: 0px;
    }

    .bdr {
        border-bottom: 1px solid black;
    }

    .table tr td, .table tr th {
        border: 1px solid #000;
        padding: 3px;
    }

    .double-line {
        height: 5px !important;
        border-top: 3px solid #000;
        border-bottom: 1px solid #000;
    }
</style>
