<div class="row">
    <div class="col-sm-12">
        <div class="card-box row"><h2 class="visible-print center">Rekap Temuan Dan Tindak Lajut</h2>
            <form method="get" class="row hidden-print" action="?">
                <div class="col-md-1">
                    <div class="dataTables_wrapper form-inline">
                        <label>Tahun :</label>
                        <select name="tahun" onchange="this.form.submit()" class="form-control input-sm">
                            <?php
                            foreach($list_tahun as $l){
                                echo "<option value='$l[tahun]' ".($l['tahun']==$tahun?'selected=""':'').">$l[tahun]</option>";
                            }
                            ?>
                        </select>
                    </div>
<!--                        <button type="button" onclick="window.print()" class="btn btn-inverse hidden-print hidden-xs"><span class="ace-icon fa fa-print icon-on-right bigger-110"></span> PRINT</button>-->
                </div>
            </form>
            <div class="col-xs-12">
                <hr>
                <?php
                if(isset($temuan)){
                ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="center hidden-print" rowspan="3">No</th>
                            <th class="center col-xs-1" rowspan="3">No LHP</th>
                            <th class="center col-xs-1" rowspan="3">Tgl LHP</th>
                            <th class="center col-xs-1" rowspan="3">SKPD</th>
                            <th class="center col-xs-1" rowspan="3">Kode Temuan</th>
                            <th class="center col-xs-1 " rowspan="3">Temuan</th>
                            <th class="center col-xs-1 " rowspan="3">Kode Rekomendasi</th>
                            <th class="center col-xs-1 " rowspan="1" colspan="3">Rekomendasi</th>
                            <th class="center col-xs-1 " rowspan="3">Tindak Lanjut Entitas</th>
                            <th class="center col-xs-1 " rowspan="1" colspan="8">Hasil Pemantauan Tindak Lanjut</th>
                            <th class="center col-xs-1 " rowspan="3">Keterangan</th>
                        </tr>
                        <tr>
                            <th class="center col-xs-1" rowspan="2" colspan="1">Uraian Rekomendasi</th>
                            <th class="center col-xs-1" rowspan="1" colspan="2">Nilai Kerugian</th>
                            <th class="center col-xs-1" rowspan="1" colspan="2">Sesuai Rekomendasi</th>
                            <th class="center col-xs-1" rowspan="1" colspan="2">Blm Sesuai Rekomendasi atau dalam proses (TB)</th>
                            <th class="center col-xs-1" rowspan="1" colspan="2">Belum Ditindaklanjuti (BT)</th>
                            <th class="center col-xs-1" rowspan="1" colspan="2">Tidak dpt ditindaklanjuti dgn alasan yang sah (TD)</th>
                        </tr>
                        <tr>
                            <th class="center col-xs-1" colspan="1">Negara</th>
                            <th class="center col-xs-1" colspan="1">Daerah</th>
                            <th class="center col-xs-1" colspan="1">Hasil</th>
                            <th class="center col-xs-1" colspan="1">Nilai</th>
                            <th class="center col-xs-1" colspan="1">Hasil</th>
                            <th class="center col-xs-1" colspan="1">Nilai</th>
                            <th class="center col-xs-1" colspan="1">Hasil</th>
                            <th class="center col-xs-1" colspan="1">Nilai</th>
                            <th class="center col-xs-1" colspan="1">Hasil</th>
                            <th class="center col-xs-1" colspan="1">Nilai</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1;
                        foreach ($temuan as $i=>$r) {
                            ?>
                            <tr>
                                <td class="center"><?=$no;?></td>
                                <td class="center"><?=$r['no_lhp'];?></td>
                                <td class="center"><?=format_waktu($r['created_at'])?></td>
                                <td class="center"><?=$r['skpd'];?></td>
                                <td class="center"><?=$r['kode_t']?></td>
                                <td class="center"><?=$r['temuan']?></td>
                                <td class="center"><?=$r['kode_r']?></td>
                                <td class="center"><?=$r['rekom']?></td>
                                <td class="center">
                                    <a href="#" id="nilai<?=$no?>"><?=format_uang($r['nk1'])?></a>
                                    <script>
                                        $(function () {
                                            $('#nilai<?=$no?>').editable({
                                                type: 'text',
                                                name: 'nilai',
                                                pk:'<?=$no?>',
                                                url: '?',
                                                title: 'nilai'
                                            });
                                        });
                                    </script>
<!--                                    --><?//=format_uang($r['nk1'])?>
                                </td>
                                <td class="center"><?=format_uang($r['nk2'])?></td>
                                <td class="center"><?=$r['tl']?></td>
                                <?php
                                if($r['hasil_r']==1){
                                    ?>
                                    <td class="center"><?=format_uang($r['hasil_r'])?></td>
                                    <td class="center"><?=format_uang($r['nilai_r'])?></td>
                                    <?php
                                }else{
                                    ?>
                                    <td class="center"> - </td>
                                    <td class="center"> - </td>
                                    <?php
                                }
                                if($r['hasil_r']==2){
                                    ?>
                                    <td class="center"><?=format_uang($r['hasil_r'])?></td>
                                    <td class="center"><?=format_uang($r['nilai_r'])?></td>
                                    <?php
                                }else{
                                    ?>
                                    <td class="center"> - </td>
                                    <td class="center"> - </td>
                                    <?php
                                }
                                if($r['hasil_r']==3){
                                    ?>
                                    <td class="center"><?=format_uang($r['hasil_r'])?></td>
                                    <td class="center"><?=format_uang($r['nilai_r'])?></td>
                                    <?php
                                }else{
                                    ?>
                                    <td class="center"> - </td>
                                    <td class="center"> - </td>
                                    <?php
                                }
                                if($r['hasil_r']>=3){
                                    ?>
                                    <td class="center"><?=format_uang($r['hasil_r'])?></td>
                                    <td class="center"><?=format_uang($r['nilai_r'])?></td>
                                    <?php
                                }else{
                                    ?>
                                    <td class="center"> - </td>
                                    <td class="center"> - </td>
                                    <?php
                                }
                                ?>
                                <td class=""><?=$r['keterangan']?></td>
                            </tr>
                            <?php
                        $no++;}
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>

    $.fn.editable.defaults.mode = 'popup';

    function detail(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(1).text()+' - '+$('tr#'+_id).children('td').eq(2).text();
        $('.modal-title',_modal).html(_title);
        $.post('?tao='+_id,function(data,status){
            $('.modal-body',_modal).html(data);
            _modal.modal('show');
        });
    }
    $('select').select2();
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    $('.btn-submit-data').on('click',function (e) {
        var _typ=$(this).data('type');
        if(_typ=='post'){
            $($(this).closest('form')).attr('target','_self');
            $($(this).closest('form')).attr('action','');
        }else{
            var _act=$(this).data('mode');
            $($(this).closest('form')).attr('target','_blank');
            $('[name=type]',$(this).closest('form')).val(_act);
            $($(this).closest('form')).attr('action',"<?=base_url('export/stok')?>");
        }
        $($(this).closest('form')).attr('method',_typ);
        $('[type=submit]',$(this).closest('form')).click();
    });
</script>
