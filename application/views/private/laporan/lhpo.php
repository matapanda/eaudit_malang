<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="?i=true" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> TAMBAH DATA</a>
            <!--            <a href="?" class="btn btn-default"><i class="fa fa-refresh"></i></a>-->
            <hr>
            <?php
            show_alert();
            ?>
            <form method="get" class="row" action="<?=base_url('laporan/lhp_o')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" readonly class="form-control input-sm" name="start" onchange="this.form.submit()" value="<?=$start?>">
                            <span class="input-group-addon input-sm">~</span>
                            <input type="text" readonly class="form-control input-sm" name="end" onchange="this.form.submit()" value="<?=$end?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-1">Tanggal LHP</th>
                        <th class="center col-xs-3">Judul</th>
                        <th class="center col-xs-1">No LHP</th>
                        <th class="center col-xs-3">Uraian Temuan</th>
                        <th class="center col-xs-2">Rekomendasi</th>
                        <th class="center col-xs-2">Uraian Tindak Lanjut</th>
                        <th class="center col-xs-1"></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $no=1;
                    foreach($rencana as $r) {
                        ?>
                        <tr class="data" id="<?= $r['id'] ?>">
                            <td class="center"><?=$no;?></td>
                            <td class="center"><?=format_waktu(@$r['tgl_laporan'])?></td>
                            <td class="judul"><?=@$r['judul']?></td>
                            <td class="center"><?=@$r['no_laporan']?></td>
                            <td class="">
                                <?php
                                $arra=array();
                                $role=explode(";",$r['uraian']);
                                echo "<ol>";
                                $temp1='';
                                foreach ($role as $g){
                                    if(strlen($g)>0){
                                        if(!in_array($g,$arra)){
                                            $arra[]=$g;
                                            echo "<li>$g</li>";
                                        }
                                    }
                                }
                                echo "</ol>";
                                ?></td>
                            <td class=""><?php
                                $role2=explode(";",$r['rekom']);
                                echo "<ol>";
                                foreach ($role2 as $g){
                                    if(strlen($g)>0){
                                        if($temp1!=$g){
                                            $temp1=$g;
                                            echo "<li>$g</li>";
                                        }
                                    }
                                }
                                echo "</ol>";
                                ?></td>
                            <td class=""><?php
                                $role3=explode(";",$r['tl']);
                                echo "<ol>";
                                foreach ($role3 as $g){
                                    if(strlen($g)>0){
                                        if($temp1!=$g){
                                            $temp1=$g;
                                            echo "<li>$g</li>";
                                        }
                                    }
                                }
                                echo "</ol>";
                                ?></td>
                            <td class="center">
                                    <a href="?export=<?=$r['id']?>" class="btn btn-xs btn-block btn-success <?= is_authority(@$access['u']) ?>">EXPORT KE WORD</a>
                                    <a href="?del=<?=$r['id']?>" class="btn btn-xs btn-block btn-danger <?= is_authority(@$access['u']) ?>">HAPUS LHP</a>
                            </td>
                        </tr>
                        <?php
                        $no++; }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    function laksanakan(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(1).text()+' '+$('tr#'+_id).children('td').eq(2).text();
        $('.modal-title',_modal).html(_title);
        $('[name=id]',_modal).val(_id);
        _modal.modal('show');
    }
    function hapus(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang terhapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{delete:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_r) {
        $.post('?',{ri:_i,rn:_r},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html("Detail");
            $('.modal-body','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>