<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bitter:wght@200&family=Shippori+Antique&family=Tinos&display=swap" rel="stylesheet">

<!-- edit cetak spt -->
<table style="width: 100%;margin-top:-1cm" border="0">
    <tr style="margin-bottom: 1cm">
        <td style="width: 7%;"></td>
        <td style="width: 20%; padding: 10px;" align="center" valign="center">
            <img src="./assets/images/logo.jpg" style="width:90px;">
        </td>
        <td style="width: 66%" class="center">
            <h2 class="title center" style="letter-spacing: 6px;font-family: 'Tinos', serif; font-size: 25px; font-weight: 700;">PEMERINTAH KOTA MALANG</h2>
            <h2 class="title center" style="font-family: 'Shippori Antique', sans-serif;font-size: 32px; font-weight: bold;">INSPEKTORAT DAERAH</h2>
            <p class="title center">Jl. Gajahmada No. 2A, Telp/Fax. (0341) 364450, Kode Pos. 65119</p>
            <p class="title center"><b>MALANG</b></p>
        </td>
        <td style="width: 7%;"></td>
    </tr>
    <tr>
        <td class="double-line" colspan="4"></td>
    </tr>
</table>
<br>
<?php
$tahun = explode('-', $laporan['spt_date']);
?>
<table width="100%" style="text-align: justify;vertical-align: text-top;">
    <tr>
        <td colspan="4" style="text-align: center">
            <h3 class="title" style="border-bottom: solid 3px #000">SURAT PERINTAH TUGAS</h3>
            <h3 class="title">NOMOR : 700/<?= $laporan['spt_no'] ?>/35.73.300/<?= $tahun[0] ?></h3>
        </td>
    </tr>

    <?php
    foreach ($conf_spt as $i=>$c) {
        ?>
        <tr>
            <?php
            if ($i == 0) {
                ?>
                <td rowspan="<?= count($conf_spt) ?>" style="width: 3cm">Dasar</td>
                <td rowspan="<?= count($conf_spt) ?>" style="width: 1cm">:</td>
                <?php
            }
            ?>
            <td style="width: 1cm"><?= $i + 1 ?>.</td>
            <td style="width: 15cm"><?= $c['ket'] ?></td>
        </tr>
        <?php
    }
    ?>
    <tr>
        <td colspan="4" style="text-align: center">&nbsp;</td>
    </tr>
</table>

<table width="100%" style="text-align: justify;vertical-align: text-top;">
    <tr>
        <td colspan="4" style="text-align: center">
            <h3 class="title" style="border-bottom: solid 3px #000">MEMERINTAHKAN</h3>
        </td>
    </tr>
    <tr>
        <td colspan="1">
          Kepada :
        </td>
    <td colspan="1"></td>
    <td colspan="2"></td>
    </tr>
</table>
<table width="100%" class="table" cellpadding="0" cellspacing="0" style="vertical-align: text-top">
    <tr>
        <th width="1cm" style="text-align: center">NO</th>
        <th width="5cm" class="center">NAMA DAN NIP</th>
        <th width="5cm" class="center">KETERANGAN</th>
    </tr>
    <tbody>
    <?php
    $no=1;
    foreach($tim as $g){
        if($no==1){
            ?>
            <tr>
                <td style="text-align: center"><?=$no?>.</td>
                <td><?=$inspektur['nama']?>
                    <br>NIP. <?=$inspektur['nip']?></td>
                <td>Penanggung Jawab</td>
            </tr>
            <?php
        }else{
            ?>
            <tr>
                <td style="text-align: center"><?=$no?>.</td>
                <td><?=$g['nama']?>
                    <br>NIP. <?=$g['nip']?></td>
                <td><?=$g['jabatan']?></td>
            </tr>
            <?php
        }
        $no++;
    }
    foreach($anggota_tim as $g){
        ?>
        <tr>
            <td style="text-align: center"><?=$no?>.</td>
            <td><?=$g['name']?>
                <br>NIP. <?=$g['nip']?></td>
            <td>Anggota Tim</td>
        </tr>
        <?php
        $no++;
    }

    ?>
    </tbody>
</table>

<br>
<table width="100%">
    <tr>
        <?php
        $tujuan[] = array('ket' => $laporan['judul']);
        $tujuan[] = array('ket' => 'Membuat laporan kegiatan dimaksud');
        ?>
        <td style="width: 3cm;vertical-align: top">Untuk</td>
        <td style="width: 1cm;vertical-align: top">:</td>
        <td style="font-size: 10pt;vertical-align: top"><?= 1; ?>.</td>
        <td  style="font-size: 10pt"><?= $laporan['judul']; ?>.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td style="font-size: 10pt;vertical-align: top"><?= 2; ?>.</td>
        <td style="font-size: 10pt;vertical-align: top">Membuat laporan hasil kegiatan dimaksud</td>
    </tr>
<!--    --><?php
//        foreach ($tujuan as $no => $t) {
//            if ($no > 0) {
//                echo "</tr><tr>";
//            }
//            ?>
<!--            <td>--><?//= 1; ?><!--.</td>-->
<!--            <td>--><?//= $laporan['judul']; ?><!--.</td>-->
<!--            --><?php
//        }
//        ?>
<!--    <tr>-->
</table>
<table style="width: 100%;height: 100%">
    <tr>
        <td style="width: 70%"></td>
        <td ><p style="font-size: 10pt;">Dikeluarkan di : Malang<br>
                Pada tanggal : <?=format_tanggal($laporan['spt_date'])?>
            </p></td>
    </tr>
    <tr>
        <td style="width: 70%"></td>
        <td><p style="font-size: 10pt">INSPEKTUR<br>KOTA MALANG</p></td>

    </tr>
    <tr>
        <td style="width: 70%"></td>
        <td>
            <p><br><br><br><br></p>
        </td>
    </tr>
    <tr style="padding-top: 100px">
        <td style="width: 70%"></td>
        <td>
            <p style="font-size: 10pt">
                <span style="font-weight: bold;border-bottom: 1px solid #000;text-transform: uppercase"><?=$inspektur['nama']?></span><br>
                <?=$inspektur['golongan']?><br>
                NIP.<?=$inspektur['nip']?>
            </p>
        </td>
    </tr>
</table>
<htmlpagefooter name="footer">
    <img src="./assets/barc.jpg" style="width:10%;">

</htmlpagefooter>
<style>
    @page {
        footer: html_footer;
        header: html_header;
    }

    @page

    * {
        margin-top: 0cm;
        margin-bottom: 2cm;
        margin-left: 2.5cm;
        margin-right: 5cm;
    }

    .list {
        width: 100%;
        margin-top: 20px;
        margin-bottom: 10px;
        border-bottom: 1px solid black;
    }

    th {
        margin: 10px;
        text-transform: uppercase;
    }

    .right {
        text-align: right;
    }

    .center {
        text-align: center;
    }

    th.line {
        border-bottom: 1px solid black;
    }

    .title {
        margin: 0px;
    }

    .bdr {
        border-bottom: 1px solid black;
    }

    .table tr td, .table tr th {
        border: 1px solid #000;
        padding: 3px;
    }

    .double-line {
        height: 5px !important;
        border-top: 3px solid #000;
        border-bottom: 1px solid #000;
    }
</style>
