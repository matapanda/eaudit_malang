<div class="row">
    <div class="col-sm-12">
        <div class="card-box row"><h2 class="visible-print center">Rekap Temuan Dan Tindak Lajut</h2>

            <form method="post" action="?cek=true" class="col-sm-8 col-xs-11 hidden-print">
                <div class="form-group row">
                    <div class="col-sm-2">
                        <select name="periode" id="periode" class="select2 form-control" required>
                            <?php
                            foreach($periode as $p){
                                ?>
                                <option value="<?=$p['id']?>" <?=@$periode_s==$p['id']?'selected':''?>><?=$p['nama']?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select class="select2 form-control select2-multiple" required onchange="tipes(this.value)" name="tipe" data-placeholder="Pilih Tipe Kejadian">
                            <option value="">Pilih Tipe Kejadian</option>
                            <option value="A" <?=@$tipe=='A'?'selected':''?>>ALL</option>
                            <option value="P" <?=@$tipe=='P'?'selected':''?>>Kejadian Program</option>
                            <option value="K" <?=@$tipe=='K'?'selected':''?>>Kejadian Kegiatan</option>
                        </select>
                    </div>

                    <div class="col-sm-3 hasil">
                        <?php
                        if(@$tipe!=''){
                            echo '<select class="select2 form-control select2-multiple" name="pk" data-placeholder="Pilih Resiko Satker">
                                <option value="">Pilih Resiko Satker</option>';
                            foreach ($kegiatan as $v){
                                echo '<option value="'.$v['id'].'" '.(@$id_s==$v['id']?'selected':'').'>'.$v['nama'].'</option>';
                            }
                            echo'</select>';
                        }
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <input type="hidden" name="type">
                        <input type="submit" class="btn btn-submit-data btn-primary" value="VIEW">
                        <button type="button" onclick="window.print()" class="btn btn-inverse hidden-print hidden-xs"><span class="ace-icon fa fa-print icon-on-right bigger-110"></span> PRINT</button>
                    </div>
                </div>
            </form>
            <div class="col-xs-12">
                <hr>
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
<?php
if(@$title!='Kegiatan & Program'){
    ?>
Highcharts.chart('container', {
    title: {
        text: 'Perbandingan Kejadian <?=@$title?> dengan <?=@$title?>'
    },
    xAxis: {
        categories: ['<?=@$title?>']
    },
    labels: {
        items: [{
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Kejadian <?=@$title?>',
        data: [<?=@$kejadian['kejadian']+@$tmbh_kejadian?>]
    }, {
        type: 'column',
        name: '<?=@$title?>',
        data: [<?=@$total['total']?>]
    }]
});
<?php
}else{
    ?>
// alert('hahaha');
Highcharts.chart('container', {
    title: {
        text: 'Perbandingan Kejadian <?=@$title?> dengan <?=@$title?>'
    },
    xAxis: {
        categories: ['<?=@$title?>']
    },
    labels: {
        items: [{
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Kejadian <?=@$title?>',
        data: [<?=@$kejadian['kejadian']+@$tmbh_kejadian?>]
    }, {
        type: 'column',
        name: 'Total Program',
        data: [<?=@$total_program['total']?>]
    }, {
        type: 'column',
        name: 'Total Kegiatan',
        data: [<?=@$total_kegiatan['total']?>]
    }]
});
<?php
}
?>

    <?php
    if(isset($kejadian['kejadian'])){
    ?>
    $('.hasil').show();
    $('#container').show();
    <?php
    }else{
        ?>
    $('.hasil').hide();
    $('#container').hide();
    <?php
    }
    ?>
    function detail(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(1).text()+' - '+$('tr#'+_id).children('td').eq(2).text();
        $('.modal-title',_modal).html(_title);
        $.post('?tao='+_id,function(data,status){
            $('.modal-body',_modal).html(data);
            _modal.modal('show');
        });
    }
    function tipes(i) {
        if(i!=''){
var periode=$('#periode').val();
            $.post('?tipes='+i+'&periode='+periode,function(data,status){
                $('.hasil').show();
                $('.hasil').html();
                $('.hasil').html(data);
                $('.select2').select2();
            });
        }
    }
    $('select').select2();
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
</script>