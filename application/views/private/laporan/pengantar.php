<table style="width: 100%">
    <tr style="margin-bottom: 1cm">
            <td style="width: 3cm">
                <img src="./assets/images/logo.jpg" style="width:20%;">
            </td>
        <td style="width: 15cm" class="center">
            <h2 class="title center">PEMERINTAH KOTA MALANG</h2>
            <h2 class="title center">INSPEKTORAT DAERAH</h2>
            <p class="title center">Jl. Gajahmada No. 2A Telp/ Fax. (0341) 364450 Kode Pos: 65119</p>
            <p class="title center"><b>MALANG</b></p>
           
        </td>
    </tr>
    <tr>
        <td class="double-line" colspan="2"></td>
    </tr>
</table>
<br><table width="100%" style="text-align: justify;vertical-align: text-top;">
    <tr>
        <td colspan="3" style="text-align:left;width: 60%">
        </td>
        <td style="text-align:left;width: 40%">
        Malang, <?=date("d M Y", strtotime($laporan['date']));?>
        </td>
    </tr> <tr>
        <td>
        Nomor
        </td>        <td>
        :
        </td>        <td>
        <?=$laporan['no_p']?>
        </td>
        <td>
Kepada
        </td>
    </tr><tr>
        <td>
            Sifat
        </td>        <td>
        :
        </td>        <td>
        Penting
        </td>
        <td>
Yth. <?=$n_satker['kepala']?>
        </td>
    </tr><tr>
        <td>
        Lampiran
        </td>        <td>
        :
        </td>        <td>
        -.
        </td>
        <td>
            Kota Malang
        </td>
    </tr><tr>
        <td>
        Perihal
        </td>        <td>
        :
        </td>        <td>
        <b><u>Pemberitahuan</u></b>
        </td>
        <td>
            di Malang
        </td>
    </tr>
</table>
<br>
<br>
<table>
    <tr>
        <td width="20%">&nbsp;</td>
        <td width="80%">
            <p>&emsp; Bersama ini diberitahukan bahwa Inspektorat Kota Malang akan melaksanakan <b><?=$laporan['isian']?></b><br>
                &emsp; Guna kelancaran pelaksanaan dimaksud, mohon bantuan pihak-pihak terkait untuk menyiapkan data-data sebagaimana daftar terlampir, serta dapat berkoordinasi dengan Tim Inspektorat Kota Malang
                Demikian  untuk menjadikan maklum.
            </p>
        </td>
    </tr>

</table>
<br>
<table width="100%" style="margin-left: 0.5cm">
    <tr>
        <td width="70%"></td>
        <td width="30%">
            <p style="font-size: 10pt">INSPEKTUR<br>KOTA MALANG</p>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
                    <tr style="padding-top: 100px">
                        <td width="70%"></td>
                        <td width="30%">
                            <p style="font-size: 10pt">
                                <span style="font-weight: bold;border-bottom: 1px solid #000;text-transform: uppercase"><?=$inspektur['nama']?></span><br>
                                <?=$inspektur['golongan']?><br>
                                NIP.<?=$inspektur['nip']?>
                            </p>
                        </td>
                    </tr>
    <tr>
        <td width="86%" colspan="2">
            <b>Tembusan :</b><br>
            Yth. Wakil Walikota Malang<br>
            (selaku Koordinator Pengawasan)

        </td>
    </tr>
</table>
<htmlpagefooter name="footer">
    <img src="./assets/barc.jpg" style="width:10%;">

</htmlpagefooter>
<newpage>

</newpage>
<style>
    @page {
        footer: html_footer;
        header: html_header;
    }

    @page

    * {
        margin-top: 1.8cm;
        margin-bottom: 2cm;
        margin-left: 2.5cm;
        margin-right: 5cm;
    }

    .list {
        width: 100%;
        margin-top: 20px;
        margin-bottom: 10px;
        border-bottom: 1px solid black;
    }

    th {
        margin: 10px;
        text-transform: uppercase;
    }

    .right {
        text-align: right;
    }

    .center {
        text-align: center;
    }

    th.line {
        border-bottom: 1px solid black;
    }

    .title {
        margin: 0px;
    }

    .bdr {
        border-bottom: 1px solid black;
    }

    .table tr td, .table tr th {
        border: 1px solid #000;
        padding: 3px;
    }

    .double-line {
        height: 5px !important;
        border-top: 3px solid #000;
        border-bottom: 1px solid #000;
    }
</style>
