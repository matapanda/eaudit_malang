<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <form method="post" class="col-sm-12 col-xs-11">
                <div class="form-group row">
                    <div class="col-sm-2">
                        <select name="bulan" class="select2 form-control" required>
                            <?php
                            $list_bulan=array('-','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                            foreach ($list_bulan as $i=>$b){
                                echo "<option value='$i' ".($i==$bulan?'selected=""':"").">$b</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select name="tahun" class="select2 form-control" required>
                            <?php
                            for($i=date('Y');$i>(date('Y')-5);$i--){
                                echo "<option value='$i' ".($i==$tahun?'selected=""':"").">$i</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select name="detail" class="select2 form-control" required>
                            <option value="1">Summary</option>
                            <option value="0" <?=@$detail=='FALSE'?'selected':''?>>Detail</option>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <input type="hidden" name="type">
                        <button type="submit" class="hidden">SUBMIT</button>
                        <button type="button" data-type="post" class="btn btn-submit-data btn-primary waves-effect waves-light">VIEW</button>
                        <button type="button" data-mode="pdf" data-type="get" class="btn btn-submit-data btn-danger waves-effect waves-light">PDF</button>
                        <button type="button" data-mode="excel" data-type="get" class="btn btn-submit-data btn-success waves-effect waves-light">EXCEL</button>
                    </div>
                </div>
            </form>
            <div class="col-xs-12">
                <hr>
                <?php
                if(isset($data)){
                if ($bulan > 0) {
                    if($bulan==1){
                        $sebelum = $list_bulan[12] . " " . ($tahun - 1);
                        $sekarang = $list_bulan[1] . " " . $tahun;
                        $tahun=array(($tahun-1),($tahun));
                        $bulan=array((12),(1));
                    }else{
                        $sebelum = $list_bulan[$bulan - 1] . " " . $tahun;
                        $sekarang = $list_bulan[$bulan - 0] . " " . $tahun;
                        $tahun=array(($tahun),($tahun));
                        $bulan=array(($bulan - 1),($bulan));
                    }
                } else {
                    $bulan = 12;
                    $sebelum = $list_bulan[$bulan] . " " . ($tahun - 1);
                    $sekarang = $list_bulan[$bulan] . " " . $tahun;
                    $tahun=array(($tahun - 1),($tahun));
                    $bulan=array(($bulan),($bulan));
                }
                $sebelum = date("t", strtotime("$tahun[0]-$bulan[0]-1")) . " " . $sebelum;
                $sekarang = date("t", strtotime("$tahun[1]-$bulan[1]-1")) . " " . $sekarang;
                ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="center" rowspan="2" style="vertical-align: middle;">Kode Rek</th>
                            <th class="col-xs-2 center" rowspan="2" style="vertical-align: middle;">Nama Perkiraan</th>
                            <th class="col-xs-2 center" colspan="2"><?php echo $sebelum;?></th>
                            <th class="col-xs-2 center" colspan="2">Mutasi</th>
                            <th class="col-xs-2 center" colspan="2"><?php echo $sekarang;?></th>
                            <th class="col-xs-2 center" colspan="2">Laba Rugi</th>
                            <th class="col-xs-2 center" colspan="2">Neraca</th>
                        </tr>
                        <tr>
                            <th class="col-xs-1 center">Debet</th>
                            <th class="col-xs-1 center">Kredit</th>
                            <th class="col-xs-1 center">Debet</th>
                            <th class="col-xs-1 center">Kredit</th>
                            <th class="col-xs-1 center">Debet</th>
                            <th class="col-xs-1 center">Kredit</th>
                            <th class="col-xs-1 center">Debet</th>
                            <th class="col-xs-1 center">Kredit</th>
                            <th class="col-xs-1 center">Debet</th>
                            <th class="col-xs-1 center">Kredit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $temp=null;
                        foreach ($data as $d) {
                            if($d['status']=='f'){
                                if(@$temp<>$d['k1']){
                                    if(isset($temp)){
                                        ?>
                                        <tr style="font-size: larger;font-weight: bolder">
                                            <td colspan="12">&nbsp;</td>
                                        </tr>
                                        <?php
                                    }
                                    $temp=$d['k1'];
                                }
                                ?>
                                <tr style="font-size: larger;font-weight: bolder">
                                    <td class="center"></td>
                                    <td colspan="11"><?= $d['nama'] ?></td>
                                </tr>
                                <?php
                            }else{
                                ?>
                                <tr>
                                    <td class="center"><?= format_coa("$d[k1].$d[k2].$d[k3].$d[k4]") ?></td>
                                    <td><?= $d['nama'] ?></td>
                                    <td class="right"><?=format_uang($d['sebelum_debet'])?></td>
                                    <td class="right"><?=format_uang($d['sebelum_kredit'])?></td>
                                    <td class="right"><?=format_uang(pure_money($d['sekarang_debet'])-pure_money($d['sebelum_debet']))?></td>
                                    <td class="right"><?=format_uang(pure_money($d['sekarang_kredit'])-pure_money($d['sekarang_kredit']))?></td>
                                    <td class="right"><?=format_uang($d['sekarang_debet'])?></td>
                                    <td class="right"><?=format_uang($d['sekarang_kredit'])?></td>
                                    <td class="right"><?=$d['nl']=='L'?format_uang($d['sekarang_debet']):''?></td>
                                    <td class="right"><?=$d['nl']=='L'?format_uang($d['sekarang_kredit']):''?></td>
                                    <td class="right"><?=$d['nl']=='N'?format_uang($d['sekarang_debet']):''?></td>
                                    <td class="right"><?=$d['nl']=='N'?format_uang($d['sekarang_kredit']):''?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script>
    $('select').select2();
    $('.btn-submit-data').on('click',function (e) {
        var _typ=$(this).data('type');
        if(_typ=='post'){
            $($(this).closest('form')).attr('target','_self');
            $($(this).closest('form')).attr('action','');
        }else{
            var _act=$(this).data('mode');
            $($(this).closest('form')).attr('target','_blank');
            $('[name=type]',$(this).closest('form')).val(_act);
            $($(this).closest('form')).attr('action',"<?=base_url('export/neracalajur')?>");
        }
        $($(this).closest('form')).attr('method',_typ);
        $('[type=submit]',$(this).closest('form')).click();
    });
</script>