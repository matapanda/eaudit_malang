<table>
    <tr>
        <td><img src="<?=base_url('assets/images/logo.png')?>" style="width:20%;" ></td>
        <td class="center"><h1 class="title center">PEMERINTAH  KOTA MALANG<br> INSPEKTORAT DAERAH</h1><br>
            <h4 class="title center">Jl. Gajahmada No. 2A Telp/ Fax. (0341) 364450 Kode Pos: 65119</h4><br>
            <h4 class="title center">MALANG</h4><br>
           
        </td>
    </tr>
</table>
<hr>

<h2 class="title center"><u>SURAT PERINTAH TUGAS</u></h2>
<h2 class="title center">NOMOR : 700/ <?=$laporan['spt_no']?>/35.73.300/2017</h2><br>
<table width="100%">
    <tr>
        <td>Dasar</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>1.  Peraturan Menteri Dalam Negeri Nomor 76 Tahun 2016 tentang Kebijakan Pengawasan di Lingkungan Kementerian Dalam Negeri dan Penyelenggaraan Pemerintahan  Daerah Tahun 2017;</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>2.	Peraturan Daerah Kota Malang Nomor 8 Tahun 2016 tentang Pembentukan dan Susunan Perangkat Daerah Kota Malang;</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>3.	Peraturan Walikota Malang Nomor 71 Tahun 2016 tentang Kedudukan, Susunan Organisasi, Tugas dan Fungsi serta Tata Kerja Inspektorat Kota Malang;</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>4.	Dokumen Pelaksanaan Anggaran (DPA-SKPD) Tahun 2017 Nomor : 1.20.07.20.01.5.2.</td>
    </tr>
</table>
<br>
<h4 class="title center">MEMERINTAHKAN</h4>
<table width="100%">
    <tr>
        <td>Kepada</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
<td></td>
<td></td>
<td width="85%"> <table width="100px">
        <tr class="center bdr">
        <td class="bdr" width="100px" style="text-align: center">NO</td>
        <td class="bdr" width="200px">NAMA DAN NIP</td>
        <td class="bdr" width="100px">KETERANGAN</td>
        </tr>
        <tbody>
        <?php
        $no=1;
        for($i=0;$i<count($nama);$i++){
            ?>
            <tr class="center bdr">
                <td class="bdr" style="text-align: center"><?=$no?></td>
                <td class="bdr"><?=$nama[$i]?><br><?=$nip[$i]?></td>
                <td class="bdr">-</td>
            </tr>
            <?
            $no++;
        }
        ?>

        </tbody>
    </table>
</td>
       </tr>
    <br>
    <br>
    <tr>
        <td>Untuk</td>
        <td>:</td>
        <td><?php
            $no=1;
            foreach($tujuan as $t){
                echo $no." ".$t['ket'];
                $no++;}
            ?></td>
    </tr> <tr>
        <td></td>
        <td></td>
        <td><?=$no?> Membuat laporan kegiatan dimaksud</td>
    </tr>
    <br>

</table>
<table width="100%">


    <tr>
        <td></td>
        <td></td>
        <td  width=50%>Dikeluarkan di : Malang</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td width=50%>Pada tanggal     :  <?=format_waktu($laporan['spt_date'])?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td width=50%>INSPEKTUR<br>
            KOTA MALANG
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
    </tr><br><br><br>
    <tr>
        <td></td>
        <td></td>
        <td width=50%>ISKANDAR AZIS, S.H., M.M.<br>
            Pembina Utama Muda<br>
            NIP. <?=$nip['nip']?>
        </td>
    </tr>
</table>

<htmlpagefooter name="footer">
    <table width="100%">
        <tr>
            <td style="text-align: left"><?= 'Export on: '.date('d/m/Y H:i:s') ?></td>
            <td style="text-align: right">{PAGENO}</td>
        </tr>
    </table>
</htmlpagefooter>
<style>
    @page {
        margin-top: 30px;
        margin-bottom: 70px;
        margin-left: 30px;
        margin-right: 30px;
        footer: html_footer;
        header: html_header;
    }
    .list {
        width: 100%;
        margin-top: 20px;
        margin-bottom: 10px;
        border-bottom: 1px solid black;
    }
    th{
        margin: 10px;
        text-transform: uppercase;
    }
    .right{
        text-align: right;
    }
    .center{
        text-align: center;
    }
    th.line{
        border-bottom: 1px solid black;
    }
    .title{
        margin: 0px;
    }
    .bdr{
        border-bottom: 1px solid black;
    }
</style>