<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <hr>
            <form method="get" class="row" action="<?=base_url('laporan/satker')?>">
                <div class="col-md-1">
                    <div class="dataTables_wrapper form-inline">
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="" <?=$closed==''?'selected':''?>>Semua Status</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status Aktif</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status Non Aktif</option>
                        </select>
                    </div>
                </div><div class="col-md-1">
                    <div class="dataTables_wrapper form-inline">
                        <select name="date" onchange="this.form.submit()" class="form-control input-sm">
                            <?php
                            foreach ($periode as $p){
                                ?>
                                <option value="<?=$p['nama']?>" <?=$p['nama']==$date?'selected':''?>><?=$p['nama']?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <br><table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-1">Kode</th>
                        <th class="center col-xs-5">Nama</th>
                        <th class="center col-xs-1">Ranking Resiko</th>
                        <th class="center col-xs-1">Nilai Manajemen Resiko</th>
                        <th class="center col-xs-1">Nilai Faktor Resiko</th>
                        <th class="center col-xs-1">Nilai Gabungan Resiko</th>
                         <th class="center col-xs-1">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($satker as $g) {
                        $tot=(@$g['s_r']>0?$g['s_r']:-1);
                        if($tot>=15&&$tot<=25){
                            $ket='Setiap Tahun : Perlu  tindakan  segera  untuk  mengatasi  risiko';
                            $resiko='Tidak Diterima';
                        }
                        elseif($tot>=10&&$tot<=14){
                            $ket='Dua Tahun : Perlu  tindakan  untuk  mengatasi  risiko';
                            $resiko='Issue Utama';
                        }
                        elseif($tot>=5&&$tot<=9){
                            $ket='Tiga Tahun : Tindakan  disarankan  dilakukan  jika  sumber daya  tersedia';
                            $resiko='Issue Tambahan';
                        }
                        elseif($tot>=0&&$tot<=4){
                            $ket='Tidak Perlu : Tidak  perlu  ditindaklanjuti';
                            $resiko='Dapat Diterima';
                        }
                        else{
                            $resiko='Belum Dinilai';
                            $ket='Belum Dinilai';
                        }
//                        if($g['jumlah_anggaran']==0){
//                            $anggaran='< 1 M';
//                        }else if($g['jumlah_anggaran']==1){
//                            $anggaran='1 M s.d 2 M';
//                        }else if($g['jumlah_anggaran']==2){
//                            $anggaran='2 M s.d 5 M';
//                        }else if($g['jumlah_anggaran']==3){
//                            $anggaran='5 M s.d 10 M';
//                        }else {
//                            $anggaran='10 M Keatas';
//                        }if($g['nilai_aset']==0){
//                            $aset='< 1 M';
//                        }else if($g['nilai_aset']==1){
//                            $aset='1 M s.d 2 M';
//                        }else if($g['nilai_aset']==2){
//                            $aset='2 M s.d 5 M';
//                        }else if($g['nilai_aset']==3){
//                            $aset='5 M s.d 10 M';
//                        }else {
//                            $aset='10 M Keatas';
//                        }
                        ?>
                        <tr>
                            <td scope="row" class="center"><?=$no?></td>
                            <td class="center">
                                <?=$g['kode']?>
                            </td><td class="">
                              <?=$g['nama']?>
                            </td><td class="center">
                              <?=$no?>
                            </td><td class="center">
                              <?=$g['s_r']?>
                            </td>
                            <td class="center">
                              <?=$g['tot']!=''?$g['tot']:0?>
                            </td>
                            <td class="center">
                              <?=((($g['tot']!=''?$g['tot']:0)*4)+$g['s_r'])/2?>
                            </td>
                            <td class="center <?= $g['id'] ?> hand-cursor" ><?= getLabelStatus($g['status']) ?></td>
<!--                            <td class="center --><?//= $g['id'] ?><!-- hand-cursor" >--><?//= getLabelKP($g['sk'],$g['opsi'],$g['eval']) ?><!--</td>-->

                        </tr>
                        <?php
                        $no++;}
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>

<script>
    $.fn.editable.defaults.mode = 'popup';
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
    $(function(){
        <?php
        if(!isset($access['u'])){
        ?>
        $(".emjes-editable").editable('option', 'disabled', true);
        <?php
        }
        ?>
    });
</script>