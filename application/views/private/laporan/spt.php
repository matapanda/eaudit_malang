<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?><h2 class="visible-print center">LAPORAN SPT</h2>

            <form method="get" class="row hidden-print" action="<?=base_url('laporan/spt')?>">
                <div class="col-md-12">
                    <button type="button" onclick="window.print()" class="btn btn-primary hidden-print hidden-xs"><span class="ace-icon fa fa-print icon-on-right bigger-110"></span> PRINT</button>
                    <a href="?" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                        <input type="submit" hidden>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">No PKPT</th>
                        <th class="center col-xs-1">No SPT</th>
                        <th class="center col-xs-1">Tgl SPT</th>
                        <th class="center col-xs-4">Uraian SPT</th>
                        <th class="center col-xs-1">No LHP</th>
                        <th class="center col-xs-4">Progres</th>
<!--                        <th class="center col-xs-1 hidden-print --><?//=is_authority(@$access['u'])?><!--"></th>-->
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    foreach($spt as $r) {
                        $progress_disetujui=0;
                        if($r['disetujui']>0){
                            $progress_disetujui=($r['disetujui']/$r['total']*100);
                        }
                        $r['realisasi']=$r['realisasi']-$r['disetujui'];
                        $progress_realisasi=0;
                        if($r['realisasi']>0){
                            $progress_realisasi=($r['realisasi']/$r['total']*100);
                        }
                        ?>
                        <tr>
                            <td class="center"><?=$r['pkpt_no']?></td>
                            <td class=""><?=$r['spt_no']?></td>
                            <td class=""><?=format_waktu($r['spt_date'])?></td>
                            <td class=""><?=$r['judul']?></td>
                            <td class=""><?=$r['no_lhp']?></td>
                            <td class="">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?=$r['disetujui']?>" aria-valuemin="0" aria-valuemax="<?=$r['total']?>" style="width: <?=$progress_disetujui?>%">
                                        <span class="sr-only"><?=$progress_disetujui?>% disetujui</span>
                                    </div>
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?=$r['realisasi']?>" aria-valuemin="0" aria-valuemax="<?=$r['total']?>" style="width: <?=$progress_realisasi?>%">
                                        <span class="sr-only"><?=$progress_realisasi?>% realisasi</span>
                                    </div>
                                </div>
                            </td>
<!--                            <td class="center hidden-print">-->
<!--                                <a href="--><?//=base_url("internalproses/rencana?i=$r[id]")?><!--" class="btn btn-sm btn-inverse --><?//= is_authority(@$access['u']) ?><!--"><i class="fa fa-pencil"></i></a>-->
<!--                            </td>-->
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>