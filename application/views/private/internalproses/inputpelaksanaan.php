<div class="row" ng-app="emjesSA" ng-controller="emjesController" ng-init="init()">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-xs-12">
                <div id="form-data" name="datauserForm" novalidate="" method="post" class="form-horizontal"
                     ng-submit="simpan(1)">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nomor SPT</label>
                            <div class="col-md-7" style="padding: .5em">
                                <?= ($rencana['spt_no']) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal SPT</label>

                            <div class="col-md-7" style="padding: .5em">
                                <?= format_waktu($rencana['spt_date']) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Judul SPT</label>

                            <div class="col-md-7" style="padding: .5em">
                                <?= ($rencana['judul']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6" style="text-align: right">
                        <table class="table">
                            <tr>
                                <td style="padding-top: 0;padding-bottom: 0">Proses realisasi/revisi</td>
                                <td class="danger"></td>
                            </tr>
                            <tr>
                                <td style="padding-top: 0;padding-bottom: 0">Proses persetujuan</td>
                                <td class="warning"></td>
                            </tr>
                            <tr>
                                <td style="padding-top: 0;padding-bottom: 0">Telah disetujui</td>
                                <td class="success"></td>
                            </tr>
                        </table>
                        <br>
                        <a class="btn btn-default" href="<?=base_url('internalproses/pelaksanaan')?>"><i class="fa fa-backward"></i> KEMBALI</a>
                    </div>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th class="col-xs-1 center" rowspan="2">Kode</th>
                            <th rowspan="2">Langkah Kerja</th>
                            <th colspan="2" class="center">Rencana</th>
                            <th colspan="2" class="center">Realisasi</th>
                            <th class="col-xs-1" rowspan="2" class="center"></th>
                        </tr>
                        <tr>
                            <th class="col-xs-2 center">Oleh</th>
                            <th class="col-xs-1 center">Tanggal</th>
                            <th class="col-xs-2 center">Oleh</th>
                            <th class="col-xs-1 center">Tanggal</th>
                        </tr>
                        </thead>
                        <?php
                        $temp_tahapan = "";
                        $temp_tao = "";
                        foreach ($tao as $t):
                            if ($temp_tahapan != $t['tahapan']) {
                                $temp_tahapan = $t['tahapan'];
                                ?>
                                <tr>
                                    <th class="center"><?= $t['tahapan'] ?></th>
                                    <th class="" colspan="6"><?= $t['ket_tahapan'] ?></th>
                                </tr>
                                <?php
                            }
                            if ($temp_tao != $t['id_tao']) {
                                $temp_tao = $t['id_tao'];
                                ?>
                                <tr>
                                    <th class="center"><?= $t['kode_tao'] ?></th>
                                    <th class="" colspan="6"><?= $t['tao'] ?></th>
                                </tr>
                                <?php
                            }
                            $class = "success";
                            if ($t['progres'] == 0) {
                                $class = "danger";
                            } elseif ($t['progres'] < 4) {
                                $class = "warning";
                            }
                            ?>
                            <tr class="<?= $class ?>" id="<?= $t['id'] ?>">
                                <td class="center"><?= $t['kode_kk'] ?></td>
                                <td class=""><?= $t['langkah'] ?></td>
                                <td class=""><?php
                                    if (preg_match('/[\'\/~`\!@#\$%\^&\*\(\)_\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/',  $t['rencana_by2'])) {
                                        $arr=json_decode($t['rencana_by2'],true);
//                                        echo json_encode($arr);die();
                                        for ($i=0;$i<count($arr);$i++){
                                            $this->db->where('id',$arr[$i]);
                                            $u=$this->db->get('users')->row_array();
                                            echo $u['name'].', ';
                                    }}else{
                                        $this->db->where('id',$t['rencana_by']);
                                        $u=$this->db->get('users')->row_array();
                                        echo $u['name'];

                                    }?></td>
                                <td class="center"><?= $t['rencana_date'] ?></td>
                                <td class=""><?php
                                    $this->db->where('id',$t['realisasi_by']);
                                    $u=$this->db->get('users')->row_array();
                                    echo $u['name'];
                                    ?></td>
                                <td class="center"><?= $t['realisasi_date'] ?></td>
                                <td class="right">
                                    <?php
                                    if (isset($t['isian'])) {
                                            ?>
                                            <button type="button" onclick="detail('<?= $t['id'] ?>')"
                                                    class="btn btn-xs btn-inverse btn-block"><i
                                                    class="fa fa-search pull-left"></i>
                                                RINCIAN
                                            </button>
                                            <?php
                                    } else {
                                        ?>
                                        <a href="<?= base_url('img/' . $t['template']) ?>"
                                           class="btn btn-xs btn-inverse btn-block" target="_blank"><i
                                                class="fa fa-file-text pull-left"></i> TEMPLATE</a>
                                        <?php
                                    }
                                    if ($t['progres'] <= 1) {
                                        ?>
                                        <button type="button" onclick="laksanakan('<?= $t['id'] ?>')"
                                                class="btn btn-xs btn-inverse btn-block"><i
                                                class="fa fa-pencil pull-left"></i>
                                            UPLOAD
                                        </button>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                    </table>
                </div>
            </div><!-- end col-->
        </div>
    </div>
</div>
<div class="modal fade" id="uploadModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" enctype="multipart/form-data" onsubmit="$('#preloader').show();">
                    <input type="hidden" name="id" value="">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Direalisasikan Oleh<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select name="realisasi_by" class="select2">
                                <option value="">Pilih pelaksana</option>
                                <?php
                                foreach($listtim as $l):
                                    echo "<option value='$l[id]'>$l[name]</option>";
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Direalisasikan Tanggal<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="realisasi_date" readonly autocomplete="off" placeholder="Tanggal pelaksanaan" required="" value="" class="form-control datepicker">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Upload Berkas<span class="text-danger">*</span>
                            <br>
                            <button type="button" class="btn btn-xs btn-inverse add-file-upload"><i class="fa fa-plus"></i></button>
                        </label>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-xs-12 list-upload">
                                    <input type="file" name="file[]" required>
                                </div>
                            </div>
                            <div class="row list-uploads"></div>
                            <div class="row past-uploads"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Kesimpulan</label>
                        <div class="col-sm-8">
                            <textarea name="kesimpulan" placeholder="Kesimpulan" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <button type="button" data-dismiss="modal" class="btn btn-default waves-effect m-l-5">Batal</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"
      type="text/css"/>
<script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script>
    $(function () {
        $('.select2').select2();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('.add-file-upload').on('click',function(){
            add_list_upload();
        });
        $('.list-uploads').html('');
    });
    function add_list_upload(){
        $(".list-uploads").append('<div class="row list-upload">\
        <div class="col-xs-10">\
                <input type="file" name="file[]" required>\
        </div>\
        <div class="col-xs-2"><button type="button" class="btn btn-xs btn-danger remove-upload"><i class="fa fa-remove"></i></button>\
        </div>\
                </div>');
        $('.remove-upload').on('click',function(){
            $(this).closest('.list-upload').remove();
        });
    }
    function laksanakan(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(0).text()+' '+$('tr#'+_id).children('td').eq(1).text();
        $('.modal-title',_modal).html(_title);
        $('[name=id]',_modal).val(_id);
        $('[name=realisasi_by]',_modal).val('<?=$user['id']?>');
        $('[name=file]',_modal).val('');
        $('[name=kesimpulan]',_modal).val('');
        $('[name=realisasi_date]',_modal).val('<?=date('d/m/Y')?>');
        $('.select2').select2();
        $('.list-uploads').html('');
        $('.past-uploads').html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.get('?',{tao_files:'<?=$this->input->get_post('i')?>',files:_id},function(data){
            $('.past-uploads').html(data);
            $('.remove-past-upload').on('click',function(){
                $(this).closest('.list-upload').remove();
                var _fn=$(this).data('id');
                var _idtao=_id;
                $.get('?',{remove_files:_fn,remove_id:'<?=$this->input->get_post('i')?>',remove_tao:_idtao},function(data){
                    $('.remove-past-upload').on('click',function(){
                        $(this).closest('.list-upload').remove();
                    });
                });
            });
        });
        _modal.modal('show');
    }
    function detail(_id) {
        var v='<?=$this->input->get_post('i')?>';
        var _modal=$('#detailModal');
        var _title=$('tr#'+_id).children('td').eq(0).text()+' '+$('tr#'+_id).children('td').eq(1).text();
        $('.modal-title',_modal).html(_title);
        $.post('?tao='+_id+'&idt='+v,function(data,status){
            $('.modal-body',_modal).html(data);
            _modal.modal('show');
        });
    }
</script>
<style>
    .list-upload{
        padding: .5em;
    }
</style>
