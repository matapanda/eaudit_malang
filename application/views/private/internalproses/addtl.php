<style>
    .v-mid{
        vertical-align: middle!important;
    }
    .ada-border{
        border: 1px solid #ddd ;
    }
</style>
<div class="row" ng-app="emjesSA" ng-controller="emjesController" ng-init="init()">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-xs-12">
                <form id="form-data" name="datauserForm" novalidate="" method="post" action="?baru=true" class="form-horizontal" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tanggal LHP</label>
                        <div class="col-md-5">
                            <input type="text" name="tgl_lhp" id="tgl_lhp" autocomplete="off" ng-model="tgl_lhp" placeholder="Tanggal LHP" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Periode LHP</label>
                        <div class="col-md-5">
                            <input type="text" name="spt_date" autocomplete="off" ng-model="spt_date" placeholder="Periode LHP" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Judul SKPD</label>
                        <div class="col-md-5">
                            <input type="text" name="judul_spt" autocomplete="off" ng-model="judul" placeholder="Judul SKPD" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Nomor LHP</label>
                        <div class="col-md-5">
                            <input type="text" name="spt_no" ng-model="spt_no" autocomplete="off" placeholder="Nomor LHP"  class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tim Pelaksana</label>
                        <div class="col-md-5">
                            <input type="text" name="pelaksana" ng-model="pelaksana" autocomplete="off" placeholder="Tim Pelaksana"  class="form-control">
                            <input type="hidden" name="no_temuan" id="no_temuan" class="form-control" value="1">

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputName" class="col-sm-3 form-control-label right">Satker<span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <select name="satker" required class="form-control select2">
                                <option value="">-</option>
                                <?php
                                foreach ($satker as $p){
                                    echo "<option value='$p[id]'>$p[kode] - $p[ket]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Temuan & Tindak Lanjut</label>

                        <div class="col-md-3">
                            <button type="button" onclick="tambah_temuan()" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i> TAMBAH TEMUAN</button>
<!--                        <label class="col-md-9 control-label">Temuan & Tindak Lanjut</label>-->
                    </div>
                    </div>
                    <hr>
                        <div class="col-md-12 tambahan">
                            <div class="form-group 1">
                                <h3>TEMUAN</h3> <br>
                            <div class="form-group">
                                <label for="inputName" class="col-sm-3 form-control-label right">Judul Temuan<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-5">
                                    <input  type="text" class="form-control" name="judul_tl1" placeholder="Judul Temuan" required value="<?=@@$rencana['judul_tl']?>">
                                </div>
                            </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 form-control-label right">Kode Temuan<span class="text-danger">*</span></label>
                                    <div class="col-sm-5">
                                        <select name="kt1" required class="form-control select2">
                                            <option value="">-</option>
                                            <?php
                                            foreach ($parent as $p){
                                                echo "<option value='$p[id]' ".($p['id']==@$form['parent']?'selected=""':"")." data-posisi='$p[dk]'>$p[kode] - $p[nama]</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 form-control-label right">Uraian Temuan<span class="text-danger">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="ut1" autocomplete="off" placeholder="Temuan" required value="<?=(@@$rencana['rekom'])?>" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 form-control-label right">Nilai Temuan<span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-5">
                                        <input  type="text" class="form-control" name="nilai_tl1" placeholder="Nilai Temuan" required value="<?=@@$rencana['nilai_tl']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 form-control-label right">Kondisi<span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-5">
                                        <textarea class="form-control" class="mytextarea" name="kondisi1" placeholder="Kondisi" required><?=@@$rencana['kondisi']?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 form-control-label right">Kriteria<span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-5">
                                        <textarea class="form-control" class="mytextarea" name="kriteria1" placeholder="Kriteria" required><?=@$rencana['kriteria']?></textarea>
                                    </div>
                                </div>
                                <h4>Penyebab <button type="button" onclick="tambah_penyebab(1)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h4><br>

                                <div class="form-group penyebab1">
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-3 form-control-label right">Kode Penyebab<span class="text-danger">*</span></label>
                                        <div class="col-sm-5">
                                            <select name="penyebab1[]" required class="form-control select2" data-placeholder="pilih kode penyebab" >
                                                <option value="">-</option>
                                                <?php
                                                foreach ($penyebab as $p){
                                                    echo "<option value='$p[id]' >$p[kode] - $p[nama]</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group kn">
                                        <label for="inputName" class="col-sm-3 form-control-label right">Uraian penyebab<span class="text-danger">*</span></label>
                                        <div class="col-sm-5">
                                            <textarea class="form-control" class="mytextarea" name="up1[]" placeholder="Uraian penyebab" required><?=(@@$rencana['rekom'])?></textarea>
                                        </div>
                                    </div>
                                </div>

<hr>

                                <h4>Akibat <button type="button" onclick="tambah_akibat(1)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h4><br>

                                <div class="form-group akibat1">
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-3 form-control-label right">Kode akibat<span class="text-danger">*</span></label>
                                        <div class="col-sm-5">
                                            <select name="a1[]" required class="form-control select2" data-placeholder="pilih kode penyebab" >
                                                <option value="">-</option>
                                                <?php
                                                foreach ($akibat as $p){
                                                    echo "<option value='$p[id]' >$p[kode] - $p[nama]</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group kn">
                                        <label for="inputName" class="col-sm-3 form-control-label right">Uraian akibat<span class="text-danger">*</span></label>
                                        <div class="col-sm-5">
                                            <textarea class="form-control" class="mytextarea" name="ap1[]" placeholder="Uraian akibat" required></textarea>
                                        </div>
                                    </div>
                                </div>

                                <hr>
<!--                                //rekom-->
                                <h4>Rekomendasi Temuan <button type="button" onclick="tambah_rekom(1)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h4><br>

                            <div class="form-group rekom1">
                                <div class="form-group">
                                <label for="inputName" class="col-sm-3 form-control-label right">Kode Rekom<span class="text-danger">*</span></label>
                                <div class="col-sm-5">
                                      <select name="kr1[]" required class="form-control select2" data-placeholder="pilih kode rekom" >
                                        <option value="">-</option>
                                        <?php
                                        foreach ($parent2 as $p){
                                            echo "<option value='$p[id]' ".($p['id']==@$form['parent']?'selected=""':"")." data-posisi='$p[dk]'>$p[kode] - $p[nama]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                                <div class="form-group">
                                <label for="inputName" class="col-sm-3 form-control-label right">Status Rekom<span class="text-danger">*</span></label>
                                <div class="col-sm-5">
                                      <select name="sr1[]" required class="form-control select2" data-placeholder="pilih status rekom" >
                                        <option value="">-</option>
                                        <option value="sesuai">Sesuai</option>
                                        <option value="belum sesuai">Belum Sesuai</option>
                                        <option value="belum ditindaklanjuti">Belum Ditindaklanjuti</option>
                                        <option value="tidak dapat ditindaklanjuti">Tidak Dapat Ditindaklanjuti</option>
                                    </select>
                                </div>
                            </div>
                                <div class="form-group">
                                <label for="inputName" class="col-sm-3 form-control-label right">Uraian Rekom<span class="text-danger">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="uk1[]" autocomplete="off" placeholder="Rekomendasi" required value="" class="form-control">
                                </div>
                            </div>
                                <div class="form-group">
                                <label for="inputName" class="col-sm-3 form-control-label right">Nilai Rekom<span class="text-danger">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="nilai_r1[]" autocomplete="off" placeholder="Nilai Rekomendasi" required value="" class="form-control">
                                </div>
                            </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 form-control-label right">No Tindak Lanjut<span class="text-danger">*</span></label>
                                    <div class="col-sm-5">
                                        <select name="ntl1[]" class="form-control select2" data-placeholder="No Tindak Lanjut" >
                                            <option value="">-</option>
                                            <?php
                                            foreach ($tl as $p){
                                                echo "<option value='$p[id]'>$p[kode] - $p[nama]</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 form-control-label right">Uraian Tindak Lanjut<span class="text-danger">*</span></label>
                                    <div class="col-sm-5">

                                        <input type="text" name="utl1[]" autocomplete="off" placeholder="Uraian Tindak Lanjut" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 form-control-label right">Nilai Tindak Lanjut<span class="text-danger">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="nilai_tindak_l1[]" autocomplete="off" placeholder="Nilai Tindak Lanjut" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Upload File Tindak Lanjut</label>
                                    <div class="col-md-7">
                                            <input type="file" name="file1[]">
                                    </div>
                                </div>
                                <hr>
                            </div>


                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 form-control-label right">Tanggapan </label>
                                    <div class="col-sm-5">
                                        <input type="text" name="tang1" autocomplete="off" placeholder="Tanggapan" value="<?=(@@$rencana['tanggapan'])?>" class="form-control">
                                    </div>
                                </div>
                        </div>
                        </div>

                    <div class="form-group">
                        <div class="col-md-offset-5 col-md-4">
                            <input type="submit" class="btn btn-inverse" value="SUBMIT">
<!--                            <button type="button" ng-click="simpan(1)" class="btn btn-success"><i class="fa fa-file-text"></i> DRAFT</button>-->
                            <a href="<?=base_url('internalproses/tindak_lanjut')?>" class="btn btn-default"><i class="fa fa-backward"></i> KEMBALI</a>
                        </div>
                    </div>
                </form>
            </div><!-- end col-->
        </div>
    </div>

    <div class="modal fade none-border" id="tambahModal">
        <div class="modal-dialog modal-md" style="overflow-y: scroll">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Kode Temuan</h4>
                </div>
                <div class="modal-body p-20" style="height:100%;overflow-y: scroll" >
                    <form method="post" id="tambah_hari">
                        <div class="form-group row">
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Judul Temuan<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input  type="text" class="form-control" name="judul_tl" ng-model="judul_tl" placeholder="Judul Temuan" required value="<?=@@$rencana['judul_tl']?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kode Temuan<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <select name="kt" ng-model="kt" id="kt" required class="form-control select2">
                                        <option value="">-</option>
                                        <?php
                                        foreach ($parent as $p){
                                            echo "<option value='$p[id]' ".($p['id']==@$form['parent']?'selected=""':"")." data-posisi='$p[dk]'>$p[kode] - $p[nama]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Uraian Temuan<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="ut" ng-model="ut" autocomplete="off" placeholder="Temuan" required value="<?=(@@$rencana['rekom'])?>" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kondisi<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="kondisi" ng-model="kondisi" placeholder="Kondisi" required><?=@@$rencana['kondisi']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kriteria<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="kriteria" ng-model="kriteria" placeholder="Kriteria" required><?=@$rencana['kriteria']?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Judul Temuan<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input  type="text" class="form-control" name="judul_tl" ng-model="judul_tl" placeholder="Judul Temuan" required value="<?=@@$rencana['judul_tl']?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kode Temuan<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <select name="kt" ng-model="kt" id="kt" required class="form-control select2">
                                        <option value="">-</option>
                                        <?php
                                        foreach ($parent as $p){
                                            echo "<option value='$p[id]' ".($p['id']==@$form['parent']?'selected=""':"")." data-posisi='$p[dk]'>$p[kode] - $p[nama]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Uraian Temuan<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="ut" ng-model="ut" autocomplete="off" placeholder="Temuan" required value="<?=(@@$rencana['rekom'])?>" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kondisi<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="kondisi" ng-model="kondisi" placeholder="Kondisi" required><?=@@$rencana['kondisi']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kriteria<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="kriteria" ng-model="kriteria" placeholder="Kriteria" required><?=@$rencana['kriteria']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kode Rekom<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <select name="kr" ng-model="kr" id="kr" required class="form-control select2" data-placeholder="pilih kode rekom" >
                                        <option value="">-</option>
                                        <?php
                                        foreach ($parent2 as $p){
                                            echo "<option value='$p[id]' ".($p['id']==@$form['parent']?'selected=""':"")." data-posisi='$p[dk]'>$p[kode] - $p[nama]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Uraian Rekom<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="uk" ng-model="uk" placeholder="Rekomendasi" required><?=(@@$rencana['rekom'])?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Negara/Daerah<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <select name="nd" ng-model="nd" id="nd" required class="form-control select2">
                                        <option value="">-</option>
                                        <option value="1">Negara</option>
                                        <option value="2">Daerah</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Nilai Kerugian<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input type="number" name="nk" id="nk" ng-model="nk" autocomplete="off" placeholder="Nilai Kerugian" required value="<?=(@$temuan['kerugian'])?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Tindak Lanjut Entitas<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="tl" id="tl" ng-model="tl" autocomplete="off" placeholder="Tindak Lanjut Entitas" value="<?=(@$temuan['tl'])?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Hasil Tindak Lanjut<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <select name="htl" ng-model="htl" id="htl" class="form-control select2">
                                        <option value="">-</option>
                                        <option value="1">Sesuai</option>
                                        <option value="2">Belum Sesuai</option>
                                        <option value="3">Belum Ditindaklanjuti</option>
                                        <option value="4">Tidak Dapat Ditindaklanjuti</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Nilai Tindak Lanjut<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input type="number" name="ntl" id="ntl" ng-model="ntl" autocomplete="off" placeholder="Nilai Tindak Lanjut" value="<?=(@$temuan['ntl']?$temuan['ntl']:0)?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Keterangan</label>
                                <div class="col-sm-9">
                                    <input type="text" name="keterangan" id="keterangan" ng-model="keterangan" autocomplete="off" placeholder="Keterangan" required value="<?=(@$temuan['keterangan'])?>" class="form-control">
                                </div>
                            </div>
                        </div>
                            <div class="col-sm-offset-3">
                                <button type="button" ng-click="tambah_temuan()" id="tambah" class="btn btn-success save-event waves-effect waves-light">Simpan</button>
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                <input type="submit" id="submit_tambah" class="hidden">
                            </div>
<!--                        </div>-->
                </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade none-border" id="detailModal">
        <div class="modal-dialog modal-lg" style="overflow-y: scroll">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Detail Temuan</h4>
                </div>
                <div class="modal-body p-20" style="height:100%;" >
                    <div class="form-group row">

                        <div class="form-group row">
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Data Umum Obrik<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" readonly style="background-color: #d7d6d6;" ng-model="obrik_d" name="obrik" placeholder="Data Umum Obrik" required><?=@$rencana['obrik']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Tujuan<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" ng-model="tujuan_tl_d" style="background-color: #d7d6d6;" disabled name="tujuan_tl_d" placeholder="Tujuan" required><?=@$rencana['tujuan_tl']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Sasaran<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" ng-model="sasaran_tl_d" style="background-color: #d7d6d6;" disabled name="sasaran_tl_d" placeholder="Sasaran" required><?=@$rencana['tujuan_tl']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Ruang Lingkup<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="ruang" disabled style="background-color: #d7d6d6;" ng-model="ruang_d" placeholder="Ruang Lingkup" required><?=@$rencana['ruang']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Evaluasi SPI<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="evaluasi" readonly style="background-color: #d7d6d6;" ng-model="evaluasi_d" placeholder="Evaluasi SPI" required><?=@$rencana['evaluasi']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Judul Temuan<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="judul_tl" readonly style="background-color: #d7d6d6;" ng-model="judul_tl_d" placeholder="Judul Temuan" required><?=@$rencana['judul_tl']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kondisi<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="kondisi" readonly style="background-color: #d7d6d6;" ng-model="kondisi_d" placeholder="Kondisi" required><?=@$rencana['kondisi']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kriteria<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="kriteria" readonly style="background-color: #d7d6d6;" ng-model="kriteria_d" placeholder="Kriteria" required><?=@$rencana['kriteria']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Penyebab<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="penyebab" readonly style="background-color: #d7d6d6;" ng-model="penyebab_d" placeholder="Penyebab" required><?=@$rencana['penyebab']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Akibat<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" readonly name="akibat" style="background-color: #d7d6d6;" ng-model="akibat_d" placeholder="Akibat" required><?=@$rencana['kriteria']?></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Tanggapan<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" readonly class="mytextarea" name="tanggapan" style="background-color: #d7d6d6;" ng-model="tanggapan_d" placeholder="Tanggapan" required><?=@$rencana['tanggapan']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Tindak Lanjut<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" readonly class="mytextarea" name="tindak_lanjut" style="background-color: #d7d6d6;" ng-model="tindak_lanjut_d" placeholder="Tindak Lanjut" required><?=@$rencana['tindak_lanjut']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Tindak Lanjut Sebelumnya<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" readonly class="mytextarea" name="tindak_lanjut_s " style="background-color: #d7d6d6;" ng-model="tindak_lanjut_s_d" placeholder="Tindak Lanjut Sebelumnya" required><?=@$rencana['tindak_lanjut_s']?></textarea>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="taoModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="tambahModal2" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Kode Temuan</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Kode Temuan</label>
                    <div class="col-md-4">
                        <input type="text" name="header[kode_temuan]" autocomplete="off" placeholder="Kode Temuan" required value="<?=(@@$rencana['kode_temuan'])?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Temuan</label>
                    <div class="col-md-4">
                        <input type="text" name="header[temuan]" autocomplete="off" placeholder="Temuan" required value="<?=(@@$rencana['temuan'])?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Kode Rekomendasi</label>
                    <div class="col-md-4">
                        <input type="text" name="header[kode_rekom]" autocomplete="off" placeholder="Kode Rekomendasi" required value="<?=(@@$rencana['kode_rekom'])?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Data Umum Obrik</label>
                    <div class="col-md-4">
                        <textarea class="form-control" class="mytextarea" name="header[obrik]" placeholder="Data Umum Obrik" required><?=@$rencana['obrik']?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Uraian Rekomendasi</label>
                    <div class="col-md-4">
                        <input type="text" name="header[rekom]" autocomplete="off" placeholder="Rekomendasi" required value="<?=(@@$rencana['rekom'])?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">NK Negara</label>
                    <div class="col-md-4">
                        <input type="text" name="header[nk_n]" autocomplete="off" placeholder="NK Negara" required value="<?=(@@$rencana['nk_n'])?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">NK Daerah</label>
                    <div class="col-md-4">
                        <input type="text" name="header[nk_d]" autocomplete="off" placeholder="NK Daerah" required value="<?=(@@$rencana['nk_d'])?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Tindak Lanjut Entitas</label>
                    <div class="col-md-4">
                        <input type="text" name="header[tindak_lanjut]" autocomplete="off" placeholder="Tindak Lanjut" value="<?=(@@$rencana['tindak_lanjut'])?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Hasil </label>
                    <div class="col-md-4">
                        <select class="form-control select2" name="header[hasil]" required >
                            <option value="">pilih hasil</option>
                            <?php
                            $pkpt=array('Sesuai', 'Belum Sesuai','Belum Ditindaklanjuti', 'Tidak Dapat ditindaklanjuti');
                            foreach ($pkpt as $v):
                                ?>
                                <option value="<?= $v ?>" <?=(@@$rencana['pkpt_no']==$v)?'selected=""':""?>><?= "$v" ?></option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Nilai Tindak Lanjut</label>
                    <div class="col-md-4">
                        <input type="text" name="header[nilai]" autocomplete="off" placeholder="Nilai"  value="<?=(@@$rencana['nilai'])?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Keterangan</label>
                    <div class="col-md-4">
                        <input type="text" name="header[keterangan]" autocomplete="off" placeholder="Keterangan" required value="<?=(@@$rencana['keterangan'])?>" class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script src="<?=base_url()?>assets/js/angular.min.js"></script>
<script src="<?=base_url()?>assets/js/ng-messages.min.js"></script>
<script type="text/javascript">
    function tambah_rekom(i){
        var no=i;
        $.post('?',{rekom:i},function (data,status) {
                $('.rekom'+i).append(data);
            $('.select2').select2();
        });
    }function tambah_penyebab(i){
        var no=i;
        $.post('?',{penyebab:i},function (data,status) {
                $('.penyebab'+i).append(data);
            $('.select2').select2();
        });
    }function tambah_akibat(i){
        var no=i;
        $.post('?',{akibat:i},function (data,status) {
                $('.akibat'+i).append(data);
            $('.select2').select2();
        });
    }function tambah_temuan(i){
        var no=$('#no_temuan').val();
        var akhir=parseFloat(no)+1;
        $.post('?',{temuan_baru:no},function (data,status) {
                $('.tambahan').append(data);
            $('.select2').select2();
            $('#no_temuan').val(akhir);
            $("input[name='judul_t"+akhir+"']").focus();
        });


    }
    $('.select2').select2();
    $('#tgl_lhp').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    var app = angular.module("emjesSA", ['ngMessages']);
    app.filter("arrayFilter",function() {
        return function (items, idx, filterarray) {
            var filtered = [];
            angular.forEach(items, function (value, key) {
                angular.forEach(filterarray, function (v, k) {
                    if (value[idx] == v){
                        filtered.push(value);
                    }
                });
            });
            return filtered;
        }
    });
    app.filter("matchFilter",function() {
        return function (items, idx, filterarray) {
            var filtered = [];
            angular.forEach(items, function (value, key) {
                if (value[idx] == filterarray){
                    filtered.push(value);
                }
            });
            return filtered;
        }
    });
    app.directive('taoRepeatDirective', function() {
        return function(scope, element, attrs) {
            if (scope.$last){
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    todayHighlight: true
                });
            }
        };
    });
    app.directive('validNumber', function() {
        return {
            require: '?ngModel',
            link: function(scope, element, attrs, ngModelCtrl) {
                if(!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function(val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^-0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if(!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0,2);
                        clean =decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function(event) {
                    if(event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
    app.controller('emjesController', function($scope,$http) {
        $scope.filtertim = function(item) {
            if ($scope.timpelaksana.indexOf(item.id) > -1) {
                return item;
            }
        };
        $scope.aturan=[];
        $scope.pedoman=[];
        $scope.pilihpelaksana='';
        $scope.listtim=[];
        $scope.init = function(){
            $scope.aturan=<?=json_encode(@$aturan)?>;
            $scope.temuan=<?=@$temuan?json_encode(@$temuan):'[]'?>;
            $scope.pedoman=<?=json_encode(@$pedoman)?>;
            $scope.tao=<?=json_encode(@$tao)?>;
            $scope.listtim=<?=json_encode(@$listtim)?>;
            $scope.sasaran_tl=<?=strlen(@@$rencana['sasaran_tl'])>1?@@$rencana['sasaran_tl']:"''"?>;
            $scope.tujuan_tl=<?=strlen(@@$rencana['tujuan_tl'])>1?@@$rencana['tujuan_tl']:"''"?>;
            $scope.judul_tl='<?=@@$rencana['judul_tl']?>';
        };  
        $scope.detail_temuan = function(_i){
            console.log($scope.temuan[_i]);
            $scope.ut_d=$scope.temuan[_i].temuan;
            $scope.akibat_d=$scope.temuan[_i].akibat;
            $scope.penyebab_d=$scope.temuan[_i].penyebab;
            $scope.kriteria_d=$scope.temuan[_i].kriteria;
            $scope.kondisi_d=$scope.temuan[_i].kondisi;
            $scope.tujuan_tl_d=$scope.temuan[_i].tujuan_tl;
            $scope.sasaran_tl_d=$scope.temuan[_i].sasaran_tl;
            $scope.judul_tl_d=$scope.temuan[_i].judul_tl;
            $scope.evaluasi_d=$scope.temuan[_i].evaluasi;
            $scope.ruang_d=$scope.temuan[_i].ruang;
            $scope.obrik_d=$scope.temuan[_i].obrik;
            $scope.tanggapan_d=$scope.temuan[_i].tanggapan;
            $scope.tindak_lanjut_d=$scope.temuan[_i].tindak_lanjut;
            $scope.tindak_lanjut_s_d=$scope.temuan[_i]  .tindak_lanjut_s;

            $('#detailModal').modal('show');
        };
        $scope.pilihaturan = function(_i) {
            $scope.aturan[_i].checked=!$scope.aturan[_i].checked;
        };
        $scope.tambahpelaksana = function() {
            if ($scope.timpelaksana.indexOf($scope.pilihpelaksana) < 0 && $scope.pilihpelaksana!="") {
                $scope.timpelaksana.push($scope.pilihpelaksana);
            }
            $scope.pilihpelaksana="";
        };
        $scope.hapuspelaksana = function(_id) {
            var _index=$scope.timpelaksana.indexOf(_id);
            $scope.timpelaksana.splice(_index);
        };
        $scope.hapustemuan = function(_id) {
            var _index=$scope.temuan.indexOf(_id);
            $scope.temuan.splice(_index);
        };
        $scope.pilihpedoman = function(_i) {
            $scope.pedoman[_i].checked=!$scope.pedoman[_i].checked;
        };
        $scope.detailaturan = function(_tao){
            $('.modal-title','#taoModal').html(_tao.kode_kk+' '+_tao.langkah);
            $.post('?tao='+_tao.id,function(data,status){
                $('.modal-body','#taoModal').html(data);
                $('#taoModal').modal('show');
            });
        };
        $scope.tambah = function(){
            $('#tambahModal').modal('show');
            $('.select2').select2();
//            $.post('?tao='+_tao.id,function(data,status){
//                $('.modal-body','#taoModal').html(data);
//                $('#taoModal').modal('show');
//            });
        };
        $scope.tambah_temuan = function(){
            var _isExist=false;
            console.log($scope.kt);
            console.log($scope.kr);
            console.log($scope.nd);
            console.log($scope.tl);
            if($scope.ut == '' ||  $scope.uk == '' || typeof $scope.kt ==='undefined' || typeof $scope.kr === 'undefined' || typeof $scope.nd === 'undefined' || typeof $scope.htl === 'undefined' || typeof $scope.nk === 'undefined'|| typeof $scope.tl === 'undefined'|| typeof $scope.ntl === 'undefined'){

                $('#alert-kurang').empty();
                $('#alert-kurang').html('Ada data yang belum terisi');
            }else{

//            for (var i in $scope.temuan) {
//                if ($scope.temuan[i].id_t == $scope.kt) {
//                    alert('Kode Temuan Sudah Ada');
//                    $('#tambahModal').modal('hide');
//                    _isExist=true;
//                    break;
//                }
//            }
            if(_isExist==false){
                var kode_t=$( "#kt option:selected" ).text();
                var kode_temuan=kode_t.split(' - ');
                var kode_r=$( "#kr option:selected" ).text();
                var kode_rekom=kode_r.split(' - ');
                var nd=$( "#nd option:selected" ).text();
                var htl=$( "#htl option:selected" ).text();

                $scope.addtemuan={};
                $scope.addtemuan.id_t=$scope.kt;
                $scope.addtemuan.kode_t=kode_temuan[0];
//                $scope.addtemuan.temuan=kode_temuan[1];
                $scope.addtemuan.temuan=$scope.ut;
                $scope.addtemuan.akibat=$scope.akibat;
                $scope.addtemuan.penyebab=$scope.penyebab;
                $scope.addtemuan.kriteria=$scope.kriteria;
                $scope.addtemuan.kondisi=$scope.kondisi;
                $scope.addtemuan.tujuan_tl=$scope.tujuan_tl;
                $scope.addtemuan.sasaran_tl=$scope.sasaran_tl;
                $scope.addtemuan.judul_tl=$scope.judul_tl;
                $scope.addtemuan.evaluasi=$scope.evaluasi;
                $scope.addtemuan.ruang=$scope.ruang;
                $scope.addtemuan.obrik=$scope.obrik;
                $scope.addtemuan.tanggapan=$scope.tanggapan;
                $scope.addtemuan.tindak_lanjut=$scope.tindak_lanjut;
                $scope.addtemuan.tindak_lanjut_s=$scope.tindak_lanjut_s;
                $scope.addtemuan.id_r=$scope.kr;
                $scope.addtemuan.kode_r=kode_rekom[0];
                $scope.addtemuan.rekom=$scope.uk;
                $scope.addtemuan.tl=$scope.tl;
                $scope.addtemuan.kode_hr=$scope.htl;
                $scope.addtemuan.hasil_r=htl;
                $scope.addtemuan.nilai_r=$scope.ntl;
                $scope.addtemuan.keterangan=$scope.keterangan;
                if($scope.nd==1){
                    $scope.addtemuan.nd=1;
                    $scope.addtemuan.nk1=$scope.nk;
                    $scope.addtemuan.nk2=0;
                }
                else{
                    $scope.addtemuan.nd=2;
                    $scope.addtemuan.nk2=$scope.nk;
                    $scope.addtemuan.nk1=0;
                }
                $scope.temuan.unshift($scope.addtemuan);
                console.log($scope.temuan);
                $scope.addtemuan=null;
                $('#tambahModal').modal('hide');
            }

            $scope.kt='';
            $scope.ut='';
            $scope.uk='';
            $scope.kr='';
            $scope.nd='';
            $scope.nk='';
            $scope.tl='';
            $scope.htl='';
            $scope.ntl='';
            $scope.akibat='';
            $scope.penyebab='';
            $scope.kriteria='';
            $scope.kondisi='';
            $scope.judul_tl='';
            $scope.sasaran_tl='';
            $scope.tujuan_tl='';
            $scope.ruang='';
            $scope.evaluasi='';
            $scope.obrik='';
            $scope.keterangan='';
            $scope.tanggapan='';
            $scope.tindak_lanjut='';
            $scope.tindak_lanjut_s='';
            }
        };
        $scope.simpan = function(_draft) {
            $('#preloader').show();
            $('#status','#preloader').show();
            $scope.form = $('#form-data').serializeArray();
            $http({
                method: 'POST',
                url: "?baru=true",
                responseType: 'json',
                data: {
                    temuans:JSON.stringify($scope.temuan),
                    form:$scope.form
                }
            }).success(function(data){
                    window.location.assign('?');
                setTimeout(function () {
                    $('#preloader').hide();
                    $('#status','#preloader').hide();
                },2000);
                console.log("OK", data)
            }).error(function(err){"ERR", console.log(err)});
        };
    });
</script>
