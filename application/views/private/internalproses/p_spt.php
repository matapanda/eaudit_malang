<div class="row">
    <div class="col-sm-12">
        <div class="card-box">

            <?php
            show_alert();
            ?>
            <div class="">
                <h2>List Pengantar Terdaftar</h2>
                            <table id="simple-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="center col-xs-2">No Pengantar</th>
                                    <th class="center col-xs-9">Isian</th>
                                    <th class="center col-xs-1"><input type="hidden" id="no" value="0"> </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $n=1;
                                if(@$pengantar){
                                foreach($pengantar as $l){
                                    ?>
                                    <tr class="<?=$n?>">
                                        <td class="center"><?=@$l['no_p']?></td>
                                        <td class=""><?=@$l['isian']?></td>
                                        <td class="center">
                                            <?php
                                            $no=0;
                                            if(@$skpd){

                                            foreach($skpd as $s){
                                                ?>
                                                <input type="hidden" id="nama<?=$no?>" name="nama<?=$no?>" value="<?=$s['nama']?>">
                                                <input type="hidden" id="id<?=$no?>" name="id<?=$no?>" value="<?=$s['id']?>">
                                                <?php
                                                $no++;
                                            }                                            }

                                            ?>
                                            <input type="hidden" name="tot" id="tot" value="<?=$no?>">
                                            <a class="btn btn-success btn-xs" href="?pd=<?=@$l['id']?>"><i class="fa fa-file"></i></a>
                                            <a class="btn btn-info btn-xs" onclick="printings('<?=@$l['id']?>')"><i class="fa fa-print"></i></a>
                                            <a class="btn btn-danger btn-xs" href="?del=<?=@$l['id']?>"><i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                }
                                ?>

                                </tbody>
                            </table>
                    </div>
        </div>
        <div class="card-box">
             <div class="" style="text-align: right">
                        <button onclick="tambah()" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah Data</button>
                    </div>
            <hr>

            <div class="">
                <h2>Pengantar Tambahan</h2>
                        <form method="post" action="?pen=true">
                            <table id="simple-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="center col-xs-2">No Pengantar</th>
                                    <th class="center col-xs-9">Isian</th>
                                    <th class="center col-xs-1">
                                        <input type="hidden" id="no" value="0">
                                        <input type="hidden" id="id_spt" name="id_spt" value="<?=$id_spt?>">
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="materi">

                                </tbody>
                            </table>
                            <input type="submit" value="SAVE" class="btn btn-primary">
                        </form>

                    </div>
        </div>
    </div>
</div>
<div class="modal fade none-border" id="printModal">
    <div class="modal-dialog modal-sm" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Print</h4>
            </div>
            <div class="modal-body p-20 center " id="printing" style="height:100%;" >
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    function printings(id) {
        var jml=parseFloat($('#tot').val());
        var htm='';
        for(var i=0;i<jml;i++){
            var nama_s=$('#nama'+i).val();
            var skpd=$('#id'+i).val();
            htm+='<a href="?printing='+id+'&&skpd='+skpd+'" class="btn btn-block btn-inverse">'+nama_s+'</a>\n\n';
        }
        $('#printModal').modal('show');
        $('#printing').html(htm);
    }
    function tambah() {
        var no=$('#no').val();
        var urutan=parseFloat(no)+1;
        var html='<tr id="'+urutan+'">\n' +
            '                        <td style="vertical-align: bottom"><input type="text" name="no[]" required class="form-control"></td>\n' +
            '                        <td ><textarea name="isian[]" rows="2" style="width: 100%!important;" class="from-control"></textarea> </td>\n' +
            '                        <td class="center"><button class="btn btn-danger btn-xs" onclick="rem('+urutan+')"><i class="fa fa-remove"></i></button></td>\n' +
            '                    </tr>';
        $('#materi').append(html);
        $('#no').val(urutan);
    }
    function rem(_i) {
        $('#'+_i).remove();
    }
</script>
