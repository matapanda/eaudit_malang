<div class="row" ng-app="emjesSA" ng-controller="emjesController" ng-init="init()">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-xs-12">
                <div id="form-data" name="datauserForm" novalidate="" method="post" class="form-horizontal"
                     ng-submit="simpan(1)">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nomor PKPT</label>

                        <div class="col-md-7" style="padding: .5em">
                            <?php
                            foreach ($pkpt as $v):
                                if ($v['id'] != $rencana['pkpt_no'])
                                    continue;
                                echo "$v[no] - $v[tema]";
                            endforeach;
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Jenis audit</label>

                        <div class="col-md-7" style="padding: .5em">
                            <?php
                            foreach ($jenis as $v):
                                if ($v['id'] != $rencana['jenis'])
                                    continue;
                                echo "$v[kode] - $v[ket]";
                            endforeach;
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Satuan kerja yang diaudit</label>

                        <div class="col-md-7" style="padding: .5em">
                            <?php
                            foreach ($satker as $v):
                                if ($v['id'] != $rencana['satker'])
                                    continue;
                                echo "$v[kode] - $v[ket]";
                            endforeach;
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Judul SPT</label>

                        <div class="col-md-7" style="padding: .5em">
                            {{judul}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Sasaran audit</label>

                        <div class="col-md-9">
                            <div class="col-md-7" style="padding: .5em">
                                <?php
                                $raw = json_decode($rencana['sasaran'], TRUE);
                                foreach ($sasaran as $v):
                                    if (!in_array($v['id'], $raw))
                                        continue;
                                    echo "<li>$v[kode] - $v[ket]</li>";
                                endforeach;
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tujuan audit</label>

                        <div class="col-md-9">
                            <div class="col-md-7" style="padding: .5em">
                                <?php
                                $raw = json_decode($rencana['tujuan'], TRUE);
                                foreach ($tujuan as $v):
                                    if (!in_array($v['id'], $raw))
                                        continue;
                                    echo "<li>$v[kode] - $v[ket]</li>";
                                endforeach;
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Jangka waktu audit</label>

                        <div class="col-md-7" style="padding: .5em">
                            <div class="input-daterange input-group" id="jangkawaktu">
                                <?= format_waktu($rencana['start_date']) ?> ~ <?= format_waktu($rencana['end_date']) ?>
                                ( <?= floor((strtotime($rencana['end_date']) - strtotime($rencana['start_date'])) / (60 * 60 * 24)) ?>
                                hari )
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tim Pelaksana</label>

                        <div class="col-md-7" style="padding: .5em">
                            <li ng-repeat="(timkey, timitem) in listtim | filter:filtertim">
                                {{timitem.nama}}
                            </li>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Peraturan yang terkait</label>

                        <div class="col-md-9">
                            <span ng-hide="jenis && sasaran" class="col-md-4 text-muted font-17 pd-025">pilih jenis dan sasaran</span>
                            <table class="table table-striped">
                                <tbody>
                                <tr ng-repeat="(key, item) in aturan | matchFilter:'jenis':jenis | arrayFilter:'sasaran':sasaran"
                                    ng-click="pilihaturan(key)" ng-class="item.checked?'selected':'unselect'"
                                    class="selection">
                                    <td class="">{{item.kode}}</td>
                                    <td class="right">
                                        <a href="<?= base_url("img") . "/" ?>{{item.file}}"
                                           class="btn btn-sm btn-success" target="_blank"><i
                                                class="fa fa-file-text"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Pedoman audit</label>

                        <div class="col-md-9">
                            <span ng-hide="jenis && sasaran" class="col-md-4 text-muted font-17 pd-025">pilih jenis dan sasaran</span>
                            <table class="table table-striped">
                                <tbody>
                                <tr ng-repeat="(key, item) in pedoman | matchFilter:'jenis':jenis | arrayFilter:'sasaran':sasaran"
                                    ng-click="pilihpedoman(key)" ng-class="item.checked?'selected':'unselect'"
                                    class="selection">
                                    <td class="">{{item.kode}}</td>
                                    <td class="right">
                                        <a href="<?= base_url("img") . "/" ?>{{item.file}}"
                                           class="btn btn-sm btn-success" target="_blank"><i
                                                class="fa fa-file-text"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Audit program</label>

                        <div class="col-md-12">
                            <span ng-hide="jenis && sasaran" class="col-md-4 text-muted font-17 pd-025">pilih jenis dan sasaran</span>
                            <table class="table table-striped" ng-show="jenis && sasaran">
                                <?php
                                foreach ($tahapan as $t):
                                    ?>
                                    <thead>
                                    <th class="center" colspan="4"><?= $t['tahapan'] ?></th>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($detail as $d){
                                        if($t['kode']==$d['tahapan']){
                                            ?>
                                            <tr>
                                                <td class="col-xs-4"><?=$d['kode_kk']?> <?=$d['langkah']?></td>
                                                <td class="col-xs-4">
                                                    <select class="form-control select2 select2-multiple" multiple disabled>
                                                        <?php
                                                        foreach ($user as $u){
                                                            if($d['rencana_by2']!=null){
                                                                $tim=json_decode($d['rencana_by2'],true);
                                                                ?>
                                                                <option value="<?=$u['id']?>" <?=(in_array($u['id'],$tim)?'selected':'')?>><?=$u['name']?></option>
                                                                <?php
                                                            }else{
                                                                ?>
                                                                <option value="<?=$u['id']?>" <?=@$d['rencana_by']==$u['id']?'selected':''?>><?=$u['name']?></option>
                                                                <?php
                                                            }?>

                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                                <td class="col-xs-2">
                                                    <input type="text" placeholder="tanggal" ng-disabled="" value="<?=format_waktu($d['rencana_date'])?>" readonly="" class="form-control center">
                                                </td>
                                                <td class="col-xs-2">
                                                    <input type="text" placeholder="jumlah" ng-disabled="" value="<?=$d['jumlah_hari']?>" readonly="" class="form-control center">
                                                </td>
                                                <td class="right">
                                                    <button type="button" ng-click="detailaturan(item)" class="btn btn-sm btn-success"><i class="fa fa-search"></i></button>
                                                </td>
                                            </tr>

                                            <?php
                                        }
                                    }
                                    ?>
<!--                                    <tr ng-repeat="(key, item) in tao | filter:{tahapan:'--><?//= $t['kode'] ?><!--'} | matchFilter:'jenis':jenis | arrayFilter:'sasaran':sasaran | arrayFilter:'tujuan':tujuan"-->
<!--                                        ng-class="item.pelaksana && item.pelaksanaan?'selected':'unselect'"-->
<!--                                        class="selection" tao-repeat-directive>-->
<!--                                        <td class="col-xs-4">{{item.kode_kk}} {{item.langkah}}</td>-->
<!--                                        <td class="col-xs-5">-->
<!--                                            <select class="form-control" ng-model="item.pelaksana" disabled>-->
<!--                                                <option value="">-</option>-->
<!--                                                <option ng-repeat="(timkey, timitem) in listtim | filter:filtertim"-->
<!--                                                        value="{{timitem.id}}">{{timitem.nama}}-->
<!--                                                </option>-->
<!--                                            </select>-->
<!--                                        </td>-->
<!--                                        <td class="col-xs-3">-->
<!--                                            <input type="text" ng-model="item.pelaksanaan" placeholder="tanggal"-->
<!--                                                   ng-disabled="" value="" readonly="" class="form-control center">-->
<!--                                        </td>-->
<!--                                        <td class="right">-->
<!--                                            <button type="button" ng-click="detailaturan(item)"-->
<!--                                                    class="btn btn-sm btn-success"><i class="fa fa-search"></i></button>-->
<!--                                        </td>-->
<!--                                    </tr>-->
                                    </tbody>
                                    <?php
                                endforeach;
                                ?>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nomor SPT</label>

                        <div class="col-md-7" style="padding: .5em">
                            <?= ($rencana['spt_no']) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tanggal SPT</label>

                        <div class="col-md-7" style="padding: .5em">
                            <?= format_waktu($rencana['spt_date']) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Persetujuan Dalnis</label>

                        <div class="col-md-7">
                            <?php
                            if ( $rencana['proses'] == 1 && $dalnis == true && $rencana['draft']=='f') {
                                echo '<a href="#new-group" class="' . is_authority(@$access['c']) . 'btn btn-inverse" data-animation="blur" data-plugin="custommodal" data-overlayspeed="100" data-overlaycolor="#36404a">PERSETUJUAN</a>';
                            } elseif ($rencana['proses'] > 1 && $rencana['app_sv1_status'] == 't' && ($dalnis == true || $irban == true)) {
                                echo '<label class="control-label ">Disetujui Oleh : ' . $dalnis_name['name'] . ' Pada ' . format_waktu($rencana['app_sv1_tstamp']) . '</label><br>';
                                if (@$rencana['app_sv1_note']) {
                                    echo '<label class="control-label ">Note : ' . @$rencana['app_sv1_note'] . '</label>';
                                }
                            } elseif ( $rencana['proses'] == 0 && $rencana['app_sv1_status'] == 'f' && ($dalnis != true) ) {
                                echo '<label class="control-label ">Tidak Disetujui Oleh : ' . $dalnis_name['name'] . ' Pada ' . format_waktu($rencana['app_sv1_tstamp']) . '</label><br>';
                                if (@$rencana['app_sv1_note']) {
                                    echo '<label class="control-label ">Note : ' . @$rencana['app_sv1_note'] . '</label> <br>';
                                }
                            } else {
                                echo '<label class="control-label ">Menunggu Persetujuan</label>';

                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Persetujuan Irban</label>

                        <div class="col-md-7">
                            <?php
                            if ($rencana['proses'] == 2 && $irban == true ) {
                                echo '<a href="#new-group2" class="' . is_authority(@$access['c']) . 'btn btn-inverse" data-animation="blur" data-plugin="custommodal" data-overlayspeed="100" data-overlaycolor="#36404a"> PERSETUJUAN</a>';
                            } elseif ($rencana['app_sv2_status'] == 't' && ($dalnis == true || $irban == true) && $rencana['proses'] == 3) {
                                echo '<label class="control-label ">Disetujui Oleh : ' . $irban_name['name'] . ' Pada ' . format_waktu($rencana['app_sv2_tstamp']) . '</label><br>';
                                if (@$rencana['app_sv2_note']) {
                                    echo '<label class="control-label ">Note : ' . @$rencana['app_sv2_note'] . '</label>';
                                }
                            } elseif ($rencana['app_sv2_status'] == 'f' && ($dalnis == true || $irban == true) && $rencana['proses'] == 0) {
                                echo '<label class="control-label ">Tidak Disetujui Oleh : ' . $irban_name['name'] . ' Pada ' . format_waktu($rencana['app_sv2_tstamp']) . '</label><br>';
                                if (@$rencana['app_sv2_note']) {
                                    echo '<label class="control-label ">Note : ' . @$rencana['app_sv2_note'] . '</label><br>';
                                }
                            } elseif($rencana['proses']==2 && $irban != true && $rencana['app_sv1_status'] <> null ) {
                                echo '<label class="control-label ">Menunggu Persetujuan</label>';
                            }
                            else{
                                echo '<label class="control-label ">-</label>';
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div><!-- end col-->
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<script src="<?= base_url() ?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/custombox/js/legacy.min.js"></script>

<div id="new-group" class="modal-gradin">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Persetujuan Dalnis</h4>

    <div class="custom-modal-text">
        <form action="?" method="post" style="text-align: left">
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="radio radio-success radio-inline">
                        <input type="radio" id="inlineRadioDalnis1" value="t" name="persetujuanD" checked="">
                        <label for="inlineRadioDalnis1"> Setuju </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                        <input type="radio" id="inlineRadioDalnis2" value="f" name="persetujuanD">
                        <label for="inlineRadioDalnis2"> Tidak Setuju </label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <br>
                    <input type="hidden" name="id" value="<?= $rencana['id'] ?>">
                    <textarea class=" form-control" name="note" placeholder="Catatan"></textarea>
                </div>
                <div class="col-sm-12" style="padding-top: 1em">
                    <button type="submit" class="btn btn-inverse">SIMPAN</button>
                    <button type="button" onclick="Custombox.close();" class="btn btn-default">BATAL</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="new-group2" class="modal-gradin">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Persetujuan Irban</h4>

    <div class="custom-modal-text">
        <form action="?" method="post" style="text-align: left">
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="radio radio-success radio-inline">
                        <input type="radio" id="inlineRadio1" value="t" name="persetujuanI" checked="">
                        <label for="inlineRadio1"> Setuju </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                        <input type="radio" id="inlineRadio2" value="f" name="persetujuanI">
                        <label for="inlineRadio2"> Tidak Setuju </label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <br>
                    <input type="hidden" name="id" value="<?= $rencana['id'] ?>">
                    <textarea class="form-control" name="noteI" placeholder="Catatan"></textarea>
                </div>
                <div class="col-sm-12" style="padding-top: 1em">
                    <button type="submit" class="btn btn-inverse">SIMPAN</button>
                    <button type="button" onclick="Custombox.close();" class="btn btn-default">BATAL</button>
                </div>
            </div>
        </form>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>

<link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"
      type="text/css"/>
<script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<link href="<?= base_url() ?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script src="<?= base_url() ?>assets/js/angular.min.js"></script>
<script src="<?= base_url() ?>assets/js/ng-messages.min.js"></script>
<script type="text/javascript">

    // function jangkawaktuaudit() {
    //     var _sd = $('input[name="header[start_date]"]').datepicker("getDate");
    //     var _ed = $('input[name="header[end_date]"]').datepicker("getDate");
    //     var timeDiff = Math.abs(_ed.getTime() - _sd.getTime());
    //     var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    //     $('span#jangkawaktuday').html(diffDays + ' Hari');
    // }
    $(function () {
        $('.select2').select2();
        // jangkawaktuaudit();
    });
    var app = angular.module("emjesSA", ['ngMessages']);
    app.filter("arrayFilter", function () {
        return function (items, idx, filterarray) {
            var filtered = [];
            angular.forEach(items, function (value, key) {
                angular.forEach(filterarray, function (v, k) {
                    if (value[idx] == v) {
                        filtered.push(value);
                    }
                });
            });
            return filtered;
        }
    });
    app.filter("matchFilter", function () {
        return function (items, idx, filterarray) {
            var filtered = [];
            angular.forEach(items, function (value, key) {
                if (value[idx] == filterarray) {
                    filtered.push(value);
                }
            });
            return filtered;
        }
    });
    app.directive('validNumber', function () {
        return {
            require: '?ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^-0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
    app.controller('emjesController', function ($scope, $http) {
        $scope.filtertim = function (item) {
            if ($scope.timpelaksana.indexOf(item.id) > -1) {
                return item;
            }
        };
        $scope.aturan = [];
        $scope.pedoman = [];
        $scope.pilihpelaksana = '';
        $scope.timpelaksana =<?=$rencana['tim']?>;
        $scope.listtim = [];
        $scope.init = function () {
            $scope.sasaran =<?=strlen($rencana['sasaran'])>1?$rencana['sasaran']:"''"?>;
            $scope.tujuan =<?=strlen($rencana['tujuan'])>1?$rencana['tujuan']:"''"?>;
            $scope.aturan =<?=json_encode($aturan)?>;
            $scope.pedoman =<?=json_encode($pedoman)?>;
            $scope.tao =<?=json_encode($tao)?>;
            $scope.listtim =<?=json_encode($listtim)?>;
            $scope.jenis = '<?=$rencana['jenis']?>';
            $scope.satker = '<?=$rencana['satker']?>';
            $scope.judul = '<?=$rencana['judul']?>';
            $scope.spt_no = '<?=$rencana['spt_no']?>';
            $scope.start_date = '<?=format_waktu($rencana['start_date'])?>';
            $scope.end_date = '<?=format_waktu($rencana['end_date'])?>';
            $scope.spt_date = '<?=format_waktu($rencana['spt_date'])?>';
        };
        $scope.detailaturan = function (_tao) {
            $('.modal-title', '#taoModal').html(_tao.kode_kk + ' ' + _tao.langkah);
            $.post('?tao=' + _tao.id, function (data, status) {
                $('.modal-body', '#taoModal').html(data);
                $('#taoModal').modal('show');
            });
        };
    });
</script>
