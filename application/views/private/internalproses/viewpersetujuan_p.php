<div class="row" ng-app="emjesSA" ng-controller="emjesController" ng-init="init()">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-xs-12">
                <div id="form-data" name="datauserForm" novalidate="" method="post" class="form-horizontal"
                     ng-submit="simpan(1)">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nomor SPT</label>
                            <div class="col-md-7" style="padding: .5em">
                                <?= ($rencana['spt_no']) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal SPT</label>

                            <div class="col-md-7" style="padding: .5em">
                                <?= format_waktu($rencana['spt_date']) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Judul SPT</label>

                            <div class="col-md-7" style="padding: .5em">
                                <?= ($rencana['judul']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6" style="text-align: right">
                        <table class="table">
                            <tr>
                                <td style="padding-top: 0;padding-bottom: 0">Proses realisasi/revisi</td>
                                <td class="danger"></td>
                            </tr>
                            <tr>
                                <td style="padding-top: 0;padding-bottom: 0">Proses persetujuan</td>
                                <td class="warning"></td>
                            </tr>
                            <tr>
                                <td style="padding-top: 0;padding-bottom: 0">Telah disetujui</td>
                                <td class="success"></td>
                            </tr>
                        </table>
                        <br>
                        <a class="btn btn-default" href="<?=base_url('internalproses/pelaksanaan')?>"><i class="fa fa-backward"></i> KEMBALI</a>
                    </div>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th class="col-xs-1 center" rowspan="2">Kode</th>
                            <th rowspan="2">Langkah Kerja</th>
                            <th colspan="2" class="center">Rencana</th>
                            <th colspan="2" class="center">Realisasi</th>
                            <th class="col-xs-1" rowspan="2" class="center"></th>
                        </tr>
                        <tr>
                            <th class="col-xs-2 center">Oleh</th>
                            <th class="col-xs-1 center">Tanggal</th>
                            <th class="col-xs-2 center">Oleh</th>
                            <th class="col-xs-1 center">Tanggal</th>
                        </tr>
                        </thead>
                        <?php
                        $temp_tahapan = "";
                        $temp_tao = "";
                        foreach ($tao as $t):
                            $no=1;
                            if ($temp_tahapan != $t['tahapan']) {
                                $temp_tahapan = $t['tahapan'];
                                ?>
                                <tr>
                                    <th class="center"><?= $t['tahapan'] ?></th>
                                    <th class="" colspan="6"><?= $t['ket_tahapan'] ?></th>
                                </tr>
                                <?php
                            }
                            if ($temp_tao != $t['id_tao']) {
                                $temp_tao = $t['id_tao'];
                                ?>
                                <tr>
                                    <th class="center"><?= $t['kode_tao'] ?></th>
                                    <th class="" colspan="6"><?= $t['tao'] ?></th>
                                </tr>
                                <?php
                            }
                            $class = "success";
                            if ($t['progres'] == 0) {
                                $class = "danger";
                            } elseif ($t['progres'] <= 4) {
                                $class = "warning";
                            }
                            ?>
                            <tr class="<?= $class ?>" id="<?= $t['id'] ?>">
                                <td class="center"><?= $t['kode_kk'] ?></td>
                                <input type="hidden" id="kode_kk_<?=$t['id']?>" value="<?=$t['kode_kk']?>">

                                <td class=""><?= $t['langkah'] ?></td>
                                <input type="hidden" id="langkah_<?=$t['id']?>" value="<?=$t['langkah']?>">

                                <td class=""><?= $t['rencana_by'] ?></td>
                                <input type="hidden" id="rencana_by_<?=$t['id']?>" value="<?=$t['rencana_by']?>">

                                <td class="center"><?= $t['rencana_date'] ?></td>
                                <input type="hidden" id="rencana_date_<?=$t['id']?>" value="<?=$t['rencana_date']?>">

                                <td class=""><?= $t['realisasi_by'] ?></td>
                                <input type="hidden" id="realisasi_by_<?=$t['id']?>" value="<?=$t['realisasi_by']?>">

                                <td class="center"><?= $t['realisasi_date'] ?></td>
                                <input type="hidden" id="realisasi_date_<?=$t['id']?>" value="<?=$t['realisasi_date']?>">
                                <input type="hidden" id="isian_<?=$t['id']?>" value="<?=$t['isian']?>">

                                <td class="right">
                                    <?php
                                    if (isset($t['isian'])) {
                                        ?>
                                        <button type="button" onclick="detail('<?= $t['id'] ?>')"
                                                class="btn btn-inverse btn-block"><i
                                                class="fa fa-search"></i>

                                        </button>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                        $no++;
                        endforeach;
                        ?>
                    </table>
                </div>
            </div><!-- end col-->
        </div>
    </div>
</div>
<div class="modal fade" id="uploadModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"
      type="text/css"/>
<script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script>
    $(function () {
        $('.select2').select2();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
    });
    function laksanakan(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(0).text()+' '+$('tr#'+_id).children('td').eq(1).text();
        $('.modal-title',_modal).html(_title);
        $('[name=id]',_modal).val(_id);
        $('[name=realisasi_by]',_modal).val('');
        $('[name=file]',_modal).val('');
        $('[name=realisasi_date]',_modal).val('<?=date('d/m/Y')?>');
        _modal.modal('show');
    }
    function detail(_id) {
        var v='<?=$_GET['v']?>';
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(0).text()+' '+$('tr#'+_id).children('td').eq(1).text();
        $('.modal-title',_modal).html(_title);
        $.post('?tao='+_id+'&idt='+v,function(data,status){
            $('.modal-body',_modal).html(data);
            _modal.modal('show');
        });
    }
</script>