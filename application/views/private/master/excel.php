<a href="javascript:uploaddata()" class="btn btn-info" style="margin-left: 10px"><i class='ace-icon fa fa-file'></i> Upload Data Transaksi</a>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="uploaddata">
    <div class="modal-dialog" role="document">
        <form class="modal-content" method="post" id="excel"  enctype="multipart/form-data" action="<?=base_url('master/upload')?>">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Uplad Data Transaksi</h4>
            </div>
            <div class="modal-body">
                <input type="file" name="file" class="hidden" onchange="$('[type=submit]','form#excel').click()" required>
                <input type="submit" name="upload" value="UPLOAD" class="hidden">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="javascript:updatedata()" class="btn btn-info"><i class='ace-icon fa fa-download'></i> Upload
                    File</a>
            </div>
        </form><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script>

    function updatedata() {
        $('input[name=file]', 'form#excel').trigger('click');
    }
    function uploaddata() {
        $('#uploaddata').modal('show');
    }
</script>