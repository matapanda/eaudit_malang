<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="mails">
                        <?php
                        $no=1;

                        $user=$this->session->userdata('user');
                        foreach($rencana as $r) {
                            ?>
                            <div class="table-box">

                                <div class="table-detail mail-right">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box m-t-20">
                                                <h4 class="m-t-0"><b>Judul : <?=$r['judul']?></b>
                                                </h4>
                                                <!--                                            <h4 class="m-t-0"><b>Hi Bro, How are you?</b></h4>-->

                                                <hr/>

                                                <div class="media m-b-30 ">
                                                    <a href="#" class="pull-left">
                                                        <img alt="" src="<?=imageExist($user['avatar'])?>" class="media-object thumb-sm img-circle">
                                                    </a>
                                                    <div class="media-body">
                                                        <span class="media-meta pull-right"><?=$r['created_at']?></span>
                                                        <h4 class="text-primary m-0"><?=$r['nama']?></h4>
                                                        <small class="text-muted">Dari : <?=$r['email']?></small>
                                                    </div>
                                                </div> <!-- media -->
                                                <?=$r['deskripsi']?>
                                                <hr/>

                                                <?php
                                                $kk = json_decode($r['file'], TRUE);
                                                ?>
                                                <h4> <i class="fa fa-paperclip m-r-10 m-b-10"></i> Attachments <span>(<?=$kk?count($kk):""?>)</span> </h4>
                                                <?php
                                                if($kk>0) {
                                                    ?>
                                                    <div class="row">
                                                        <?php

                                                        echo '<label id="file_r" class="col-sm-8 form-control-label">';
                                                        foreach ($kk as $d):
                                                            echo '<a href= ' . base_url('img/' . $d['file']) . ' class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-text"></i> ' . $d['nama'] . '</a> ';
                                                        endforeach;
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <!-- card-box -->
                                        </div> <!-- end col -->
                                    </div> <!-- end row -->
                                </div> <!-- table detail -->
                            </div>
                            <!-- end table box -->

                            <?php
                            $no++; }
                        ?>
                    </div> <!-- mails -->
                </div>

                <hr>

                <?php
                if(@$tanggapan){
                $user=$this->session->userdata('user');
                $no=0;
                foreach($tanggapan as $r) {
                    ?>
                <div class="col-xs-12">
                    <div class="mails">

                            <div class="table-box">

                                <div class="table-detail mail-right">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box m-t-20">
<!--                                           1 card box -->
                                               <div class="media m-b-30 ">
                                                    <a href="#" class="pull-left">
                                                        <img alt="" src="<?=imageExist($r['avatar'])?>" class="media-object thumb-sm img-circle">
                                                    </a>
                                                    <div class="media-body">
                                                        <span class="media-meta pull-right"><?=$r['created_at']?></span>
                                                        <h4 class="text-primary m-0"><?=$r['nama']?></h4>
                                                        <small class="text-muted">Dari : <?=$r['email']?></small>

                    <?php
                    if($type!='v') {
                        ?>
                        <a onclick="repl(<?=$no?>)" class="btn btn-primary btn-xs"><i
                                class="mdi mdi-reply m-r-10"></i>| Reply </a>
                        <a onclick="clos(<?=$no?>)" class="btn btn-danger btn-xs clos clos-<?=$no?>"><i class="mdi mdi-window-close m-r-10 "></i>
                            | Close </a>

                        <?php
                    }?>
                                                    </div>
                                                </div>
                                                <!-- isi -->
                                                <?=$r['isi']?>
                                                <hr/>
<!--                                            Header Attachment-->
                                                <?php
                                                $kk = json_decode($r['file'], TRUE);
                                                ?>
                                                <h4> <i class="fa fa-paperclip m-r-10 m-b-10"></i> Attachments <span>(<?=$kk?count($kk):""?>)</span> </h4>
                                                <?php
                                                if($kk>0){
                                                    ?>
<!--                                                isi attachment-->
                                                <div class="row">
                                                    <?php

                                                    echo '<label id="file_r" class="col-sm-8 form-control-label">';
                                                    foreach ($kk as $d):
                                                        echo '<a href= ' . base_url('img/' . $d['file']) . ' class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-text"></i> ' . $d['nama'] . '</a> ';
                                                    endforeach;
                                                    ?>
                                                </div>
                                                    <?php
                                                }
                                                ?>
                                                <hr>
                                                <?php foreach($tanggapan2 as $t){
                                                    if($r['id_reply']==$t['refid']){
                                                        ?>
                                                        <div class="col-sm-1 center"><img width="60%" src="<?=base_url('assets/arrw.png')?>"></div>
                                                        <div class="col-sm-offset-1 replies">
                                                            <div class="media m-b-30 ">
                                                                <a href="#" class="pull-left">
                                                                    <img alt="" src="<?=imageExist($t['avatar'])?>" class="media-object thumb-sm img-circle">
                                                                </a>
                                                                <div class="media-body">
                                                                    <span class="media-meta pull-right"><?=$t['created_at']?></span>
                                                                    <h4 class="text-primary m-0"><?=$t['nama']?></h4>
                                                                    <small class="text-muted">Dari : <?=$t['email']?></small>
                                                                </div>
                                                            </div>
                                                            <!-- isi -->
                                                            <?=$t['isi']?>
                                                            <hr/>
                                                            <!--                                            Header Attachment-->
                                                            <?php
                                                            $kk3 = json_decode($t['file'], TRUE);
                                                            ?>
                                                            <h4> <i class="fa fa-paperclip m-r-10 m-b-10"></i> Attachments <span>(<?=count($kk3)?>)</span> </h4>
                                                            <?php
                                                            if($kk3>0){
                                                                ?>
                                                                <!--                                                isi attachment-->
                                                                <div class="row">
                                                                    <?php

                                                                    echo '<label id="file_r" class="col-sm-8 form-control-label">';
                                                                    foreach ($kk3 as $d):
                                                                        echo '<a href= ' . base_url('img/' . $d['file']) . ' class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-text"></i> ' . $d['nama'] . '</a> ';
                                                                    endforeach;
                                                                    ?>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>

                                                        <?php
                                                    }
                                                }
                                                ?>
<!--                                           komentar-->
                                                <div class="h-komentar <?=$no?>">
                                                    <hr>
                                                    <form role="form" enctype="multipart/form-data" method="post" action="?reply=true">
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 control-label">Deskripsi<span class="asterisk text-danger">*</span></label>
                                                            <div class="col-sm-8">

                                                                <input type="hidden" name="m_id2" value="<?=$id?>">
                                                                <input type="hidden" name="m_id_r" value="<?=$r['id_reply']?>">
                                                                <textarea name="deskripsi"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="inputName" class="col-sm-2 form-control-label">Upload Berkas<span class="text-danger">*</span><button type="button" class="btn btn-xs btn-inverse add-file-upload"><i class="fa fa-plus"></i></button>
                                                            </label>
                                                            <div class="col-sm-8">
                                                                <div class="row">
                                                                    <div class="col-xs-12 list-upload">
                                                                        <input type="file" name="file[]">
                                                                    </div>
                                                                </div>
                                                                <div class="row list-uploads">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-sm-8 col-sm-offset-4">
                                                                <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                                                                <a href="?" class="btn btn-default waves-effect m-l-5">Batal</a>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- card-box -->
                                        </div> <!-- end col -->
                                    </div> <!-- end row -->
                                </div> <!-- table detail -->
                            </div>
                            <!-- end table box -->

                    </div> <!-- mails -->
                </div>

                <?php
                $no++; }
                $no++;
                }

                ?>
                <?php
                if($type!='v'){
                    ?>
                    <hr>
                    <div class="col-sm-12 komentar">
                    <form role="form" enctype="multipart/form-data" method="post" action="?reply=true">
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Deskripsi<span class="asterisk text-danger">*</span></label>
                            <div class="col-sm-8">

                                <input type="hidden" name="m_id" value="<?=$id?>">
                                <textarea name="deskripsi"><?=@$user['deskripsi']?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-2 form-control-label">Upload Berkas<span class="text-danger">*</span><button type="button" class="btn btn-xs btn-inverse add-file-upload"><i class="fa fa-plus"></i></button>
                            </label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-xs-12 list-upload">
                                        <input type="file" name="file[]">
                                    </div>
                                </div>
                                <div class="row list-uploads">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-8 col-sm-offset-4">
                                <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                                <a href="?" class="btn btn-default waves-effect m-l-5">Batal</a>
                            </div>
                        </div>
                    </form>
                    </div><?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url() ?>assets/js/tinymce/tinymce.min.js"></script>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    $(function () {
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('.add-file-upload').on('click',function(){
            add_list_upload();
        });
        $('.list-uploads').html('');
        $('.h-komentar').hide();
        $('.clos').hide();
    });
    function repl(_i){
        $('.'+_i).show();
        $('.clos-'+_i).show();
        $('.komentar').hide();
    }
    function clos(_i){
        $('.'+_i).hide();
        $('.clos-'+_i).hide();
        $('.komentar').show();
    }
    function add_list_upload(){
        $(".list-uploads").append('<div class="row col-xs-12 list-upload">\
        <div class="col-xs-10">\
                <input type="file" name="file[]" required>\
        </div>\
        <div class="col-xs-2"><button type="button" class="btn btn-xs btn-danger remove-upload"><i class="fa fa-remove"></i></button>\
        </div>\
                </div>');
        $('.remove-upload').on('click',function(){
            $(this).closest('.list-upload').remove();
        });
    }
    tinymce.init({
        selector: "textarea",
        menubar:false,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
//            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste jbimages"
        ],
        toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link ",
        content_css : []
    });

    function hapus(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang terhapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{delete:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_r) {
        $.post('?',{ri:_i,rn:_r},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html("Detail");
            $('.modal-body','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>