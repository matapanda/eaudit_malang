<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="?i=true" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> ADD NEW DATA</a>
            <a href="?" class="btn btn-primary"><i class="fa fa-refresh"></i> RESET</a>
            <hr>
            <form method="get" class="row" action="<?=base_url('master2/barang')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label><select name="type" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="">Barang dan Jasa</option>
                            <option value="t" <?=$type=='t'?'selected':''?>>Barang Saja</option>
                            <option value="f" <?=$type=='f'?'selected':''?>>Jasa Saja</option>
                        </select>
                        <select name="status" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="">Semua Item</option>
                            <option value="t" <?=$status=='t'?'selected':''?>>Item aktif</option>
                            <option value="f" <?=$status=='f'?'selected':''?>>Item non-aktif</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=$search?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-1">Kode</th>
                        <th>Keterangan</th>
                        <th class="center col-xs-1">Barang/Jasa</th>
                        <th class="center hidden col-xs-1">Hitung Stok</th>
                        <th class="center col-xs-1">Stok</th>
                        <th class="<?=is_authority(@$access['u'])?> center col-xs-1">Status</th>
                        <th class="<?=is_authority(@$access['u'])?> center col-xs-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=$page;
                    foreach($data as $g) {
                        ?>
                        <tr class="data<?=$g['id']?>">
                            <td scope="row" class="center"><?=++$no?></td>
                            <td class="center"><?=$g['kode']?$g['kode']:'-'?></td>
                            <td><?=$g['nama']?></td>
                            <td class="center"><?=$g['type']=='t'?'BARANG':'JASA'?></td>
                            <td class="center hidden hitung<?=$g['id']?> hand-cursor" <?=isset($access['u'])?"onclick=\"setStatusHitung('$g[id]')\"":''?>><?=$g['type']=='t'?getLabelAccessRights($g['hitungstok']):''?></td>
                            <td class="center"><?=$g['hitungstok']=='t'?"<a href='?stok=$g[id]' target='_blank'>".$g['stok']."</a>":'-'?></td>
                            <td class="<?=is_authority(@$access['u'])?> center <?=$g['id']?> hand-cursor" onclick="setStatusActive('<?=$g['id']?>')"><?=getLabelStatus($g['status'])?></td>
                            <td class="<?=is_authority(@$access['u'])?> center">
                                <a href="?e=<?=$g['id']?>" class="btn btn-xs btn-block btn-inverse">UPDATE</a>
                                <?php
                                if(isset($access['d'])){
                                ?>
                                <button onclick="hapus('<?=$g['id']?>')" class="btn btn-xs btn-block btn-danger">DELETE</button>
                                <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <?=$pagination?>
        </div>
    </div>
</div>
<script>
    function hapus(_i) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{delete:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    function setStatusHitung(_i) {
        $('.hitung'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?e=true',{hitung:_i},function (data,status) {
            $('.hitung'+_i).html(data);
        });
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?e=true',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>
