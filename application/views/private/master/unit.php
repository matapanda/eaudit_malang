<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            include VIEWPATH.'alert.php';
            ?>
            <a href="#new-unit" class="<?=is_authority(@$access['c'])?> btn btn-inverse" data-animation="blur" data-plugin="custommodal" data-overlayspeed="100" data-overlaycolor="#36404a"><i class="fa fa-plus"></i> ADD NEW DATA</a>
            <a href="?" class="btn btn-primary"><i class="fa fa-refresh"></i> RELOAD</a>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-2">ID Unit Kerja</th>
                        <th>Nama Unit Kerja</th>
                        <th class="center col-xs-2">Nomor Telepon</th>
                        <th class="center col-xs-1">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($data as $no=>$g) {
                        ?>
                        <tr>
                            <td scope="row" class="center"><?=$no+1?></td>
                            <?php
                            if (isset($access['u'])){
                            ?>
                            <td class="center">
                                <a href="#" id="kode<?=$g['id']?>"><?=$g['kode']?></a>
                                <script>
                                    $(function () {
                                        $('#kode<?=$g['id']?>').editable({
                                            type: 'text',
                                            pk: 'kode',
                                            url: '?e=<?=$g['id']?>',
                                            title: 'id unit kerja'
                                        });
                                    });
                                </script>
                            </td>
                            <td>
                                <a href="#" id="nama<?=$g['id']?>"><?=$g['nama']?></a>
                                <script>
                                    $(function () {
                                        $('#nama<?=$g['id']?>').editable({
                                            type: 'text',
                                            pk: 'nama',
                                            url: '?e=<?=$g['id']?>',
                                            title: 'nama unit kerja'
                                        });
                                    });
                                </script>
                            </td>
                            <td class="center">
                                <a href="#" id="telp<?=$g['id']?>"><?=$g['telp']?></a>
                                <script>
                                    $(function () {
                                        $('#telp<?=$g['id']?>').editable({
                                            type: 'text',
                                            pk: 'telp',
                                            url: '?e=<?=$g['id']?>',
                                            title: 'nomor telepon unit kerja'
                                        });
                                    });
                                </script>
                            </td>
                            <td class="center <?=$g['id']?> hand-cursor" onclick="setStatusActive('<?=$g['id']?>')"><?=getLabelStatus($g['status'])?></td>
                            <?php
                            }else{
                            ?>
                            <td class="center"><?=$g['kode']?></td>
                            <td><?=$g['nama']?></td>
                            <td class="center"><?=$g['telp']?></td>
                            <td class="center"><?=getLabelStatus($g['status'])?></td>
                            <?php
                            }
                            ?>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>
<div id="new-unit" class="modal-gradin">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Tambah Unit Kerja</h4>
    <div class="custom-modal-text">
        <form action="?" method="post">
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" name="kode" required class="form-control" placeholder="ID Unit Kerja">
                </div>
                <div class="col-sm-12">
                    <input type="text" name="nama" required class="form-control" placeholder="Nama Unit Kerja">
                </div>
                <div class="col-sm-12">
                    <input type="text" name="telp" class="form-control" placeholder="Nomor Telepon Unit Kerja">
                </div>
                <div class="col-sm-12 right" style="padding-top: 1em">
                    <button type="button" onclick="Custombox.close();" class="btn btn-default">BATAL</button>
                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $.fn.editable.defaults.mode = 'popup';
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>