<style>
    .select2-selection.select2-selection--single {
        width: 300px;
    }

</style><div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <form method="get" class="row" action="">
                <div class="col-md-3 col-sm-3 ">
                    <label> Pilih Satuan Kerja </label>
                    <select name="satker" id="satker" class="form-control form-control-sm select2" onchange="load_pegawai()">
                    <?php foreach( $satker AS $key => $s ){ ?>
                        <OPTION value='<?php echo $key ?>'><?php echo $s?> </OPTION>
                    <?php } ?>
                    </select>
                </div>                
            </form>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="tabel_pegawai">
                    <thead>
                        <tr>
                            <th class="center col-xs-1">#</th>
                            <th class="center col-xs-1">NIP</th>
                            <th class="center col-xs-3">Nama Pegawai</th>
                            <th class="center col-xs-1">Gol/Ru</th>
                            <th class="center col-xs-1">Eselon</th>
                            <th class="center col-xs-2">Jabatan</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>

load_pegawai();

function load_pegawai(){
    
    var url = "<?php echo base_url()?>";
    var satker=$('#satker').val();
    
    $.ajax({
        'url': url+'pegawai/pegawai_list/'+satker,
        'type': 'GET',
        success: function(res){
            //console.log(res);
            $("#tabel_pegawai > tbody").empty();
            var html = '';
            no = 1;
            $.each(res, function(i, val){
                console.log(val)
                html += '<tr>';
                    html += '<td style="text-align:center;">'+(no)+'</td>';
                    html += '<td>'+val.nip+'</td>';
                    html += '<td>'+val.nama+'</td>';
                    html += '<td>'+val.golru+' ('+val.pangkat+')</td>';
                    html += '<td>'+val.eselon+'</td>';
                    html += '<td>'+val.jabatan+'</td>';
                html += '</tr>';
                no++;
            });
            $("#tabel_pegawai > tbody").html(html);            
        }
    })
}

 

    function terima(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang anda terima tidak akan bisa di tolak kembali!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{terima:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }function tolak(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang anda tolak tidak dapat di setujui kembali!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{tolak:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }function terimaI(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang anda terima tidak akan bisa di tolak kembali!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{terimaI:_i},function () {
                    location.reload();
                });
            }
        });
    }function tolakI(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang anda tolak tidak dapat di setujui kembali!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{tolakI:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_r) {
        $.post('?',{ri:_i,rn:_r},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html("Detail");
            $('.modal-body','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>