<style>
    .select2-selection.select2-selection--single {
        width: 300px;
    }

</style><div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <form method="get" class="row" action="">
            <div class="col-md-3 col-sm-3 ">
                                <label> Pilih Tahun </label>

                                <SELECT name="tahun" id="tahun" class="form-control form-control-sm select2" onchange="load_tabel_prestasi()">
                                   <?php 
                                    $now = date('Y');
                                    while($now >= 2019){ 
                                   ?>
                                   <OPTION value='<?php echo $now ?>' <?php if( $now == $tahun ){ ?>selected="selected"<?php } ?>><?php echo $now?> </OPTION>
                                   <?php
                                            $now--; 
                                        } 
                                    ?>
                                </select>
                            </div>
                
            </form>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-3">Nama SKPD</th>
                        <th class="center col-xs-1">Prestasi</th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                    $no=1;
                    foreach($prestasi as $r) {
                        ?>
                        <tr>
                            <td class="center"><?=$no;?></td>
                            <td class="left"><?=$r['NM_UNOR']?></td>
                            <td>
                                <a style="width:100%;"  class="btn btn-success btn-sm" target="_blank" href="https://e-sakip.banyuwangikab.go.id/uploads/prestasi/<?=$r['prestasi']?>" data-toggle="tooltip" data-placement="bottom" title="Unduh Data" ><i class="fa fa-share"> Unduh</i></a>
                            </td>
                                                  
                        </tr>
                        <?php
                        $no++; }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
function load_tabel_prestasi(){
    
    var url = "<?php echo base_url()?>";
    var tahun=$('#tahun').val();
    
    window.location = url+"sakip/prestasi/"+tahun;
}
 
    function terima(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang anda terima tidak akan bisa di tolak kembali!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{terima:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }function tolak(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang anda tolak tidak dapat di setujui kembali!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{tolak:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }function terimaI(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang anda terima tidak akan bisa di tolak kembali!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{terimaI:_i},function () {
                    location.reload();
                });
            }
        });
    }function tolakI(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang anda tolak tidak dapat di setujui kembali!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{tolakI:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_r) {
        $.post('?',{ri:_i,rn:_r},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html("Detail");
            $('.modal-body','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>