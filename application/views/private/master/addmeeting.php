<script type="text/css">
    .it .btn-orange
    {
        background-color: blue;
        border-color: #777!important;
        color: #777;
        text-align: left;
        width:100%;
    }
    .it input.form-control
    {

        border:none;
        margin-bottom:0px;
        border-radius: 0px;
        border-bottom: 1px solid #ddd;
        box-shadow: none;
    }
    .it .form-control:focus
    {
        border-color: #ff4d0d;
        box-shadow: none;
        outline: none;
    }
    .fileUpload {
        position: relative;
        overflow: hidden;
    }
    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
</script>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-sm-12">
                <form role="form" enctype="multipart/form-data" method="post" action="<?=@$this->input->get_post('edit')?'?edit='.$this->input->get_post('edit').'':'?save=true'?>">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">No Agenda <?=$judul?><span
                                class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <input type="text" name="no" placeholder="No Agenda" required parsley-type="text" class="form-control" id="no_agenda" value="<?=@$meet['no_agenda']?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Judul <?=$judul?><span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <input type="text" name="judul" placeholder="Judul Meeting" required parsley-type="text" class="form-control" id="judul" value="<?=@$meet['judul']?>">
                            <?php
                            if(@$meet['id']){
                                ?>
                                <input type="hidden" name="id_meeting" class="form-control" value="<?=@$meet['id']?>">
                                <?php
                            }
                            ?>
                          </div>
                    </div>

                    <hr>
                    <div class="form-group row">
                        <label class="col-sm-2 control-label">Deskripsi<span class="asterisk text-danger">*</span></label>
                        <div class="col-sm-8">
                            <textarea name="deskripsi"><?=@$meet['deskripsi']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Upload Berkas<span class="text-danger">*</span><button type="button" class="btn btn-xs btn-inverse add-file-upload"><i class="fa fa-plus"></i></button>
                        </label>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-xs-12 list-upload">
                                    <input type="file" name="file[]" <?=@$this->input->get_post('edit')?$this->input->get_post('edit'):''?>>
                                </div>
                            </div>
                            <?php
                            if(@$this->input->get_post('edit')){
                            $kk = json_decode($meet['file'], TRUE);
                            if (is_array($kk)) {
                            foreach ($kk as $d):?>
                            <div class="row list-upload">

                            <div class="col-xs-10">

                                <a href= '<?= base_url('img/'.$d['file'])?>' class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-text"></i> <?=$d['nama']?></a>

                            </div>
                            <div class="col-xs-2"><button type="button" data-id='<?=$d['file']?>' class="btn btn-xs btn-danger remove-past-upload"><i class="fa fa-remove"></i></button>

                            </div>
                            </div>

                            <?php
                            endforeach;
                            }

                            }else{
?>
                                <div class="row list-uploads"></div>
                                <div class="row past-uploads"></div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<script src="<?= base_url() ?>assets/js/tinymce/tinymce.min.js"></script>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">


    $(function () {
        $('.select2').select2();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('.add-file-upload').on('click',function(){
            add_list_upload();
        });
        $('.list-uploads').html('');
    });


    function add_list_upload(){
        $(".list-uploads").append('<div class="row col-xs-12 list-upload">\
        <div class="col-xs-10">\
                <input type="file" name="file[]">\
        </div>\
        <div class="col-xs-2"><button type="button" class="btn btn-xs btn-danger remove-upload"><i class="fa fa-remove"></i></button>\
        </div>\
                </div>');

        <?php
        if(@$meet['file']&&count(json_decode($meet['file'], TRUE))>0){
        ?>

        $('.remove-past-upload').on('click',function(){
            $(this).closest('.list-upload').remove();
            var _fn=$(this).data('id');
            $.get('?',{remove_files:_fn,remove_id:'<?=$this->input->get_post('edit')?>'},function(data){
                $('.remove-past-upload').on('click',function(){
                    $(this).closest('.list-upload').remove();
                });
            });
        });
        <?php
        }else{
        ?>

        $('.remove-upload').on('click',function(){
            $(this).closest('.list-upload').remove();
        });
        <?php
        }
        ?>
    }
    tinymce.init({
        selector: "textarea",
        menubar:false,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
//            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste jbimages"
        ],
        toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link ",
        content_css : []
    });

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function tambah_kendali(i){
        var no=i;
        $.post('?',{kendali:i},function (data,status) {
            $('.kendali'+i).append(data);
            $('.select2').select2();
        });
    }
    function tambah_resiko(i){
        var no=$('#jml_program').val();
        var akhir=parseFloat(no)+1;
        $.post('?',{resiko:akhir},function (data,status) {
            $('.tambahan').append(data);
            $('.select2').select2();
            $('#jml_program').val(akhir);
            $("input[name='resiko"+akhir+"']").focus();
        });
    }
    function kegiat(i){
        var akhir=$('#keg').val();
        $.post('?',{kegiatan:akhir},function (data,status) {
            $('.tambahan').append(data);
            $('.select2').select2();
        });
    }
</script>
