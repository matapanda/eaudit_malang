<ul>
    <li><a href="<?=base_url('welcome')?>" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span> Dashboard </span></a></li>
    <?php
    if(isset($role['f10293a6-79c7-41e2-840e-a597e5ace426']['r'])){
    ?>
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-archive"></i> <span> Master</span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <?php
                if(isset($role['b0014449-4706-48e1-b81d-d758057ef0d0']['r'])) {
                    ?>
                    <li><a href="<?= base_url('pegawai') ?>"><span>Data Pegawai </span></a></li>
                    <?php
                }
                if(isset($role['b0014449-4706-48e1-b81d-d758057ef0d0']['r'])) {
                    ?>
                    <li><a href="<?= base_url('master2/periode') ?>"><span>Data Periode </span></a></li>
                    <?php
                }
                if(isset($role['2a5a511b-7bd4-424f-8b38-252f6115640f']['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/pkpt')?>"><span>Data PKPT</span></a></li>
                    <?php
                }
                if(isset($role['72e9e5f5-9b6c-4213-9c23-85588627d997']['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/prosedur_audit')?>"><span>Data Prosedur Audit </span></a></li>
                    <?php
                }
                if(isset($role['c43ac70d-a037-4148-827d-d5c0a8b00b97']['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/penyebab')?>"><span>Data Penyebab</span></a></li>
                    <?php
                }
                if(isset($role['2a5a511b-7bd4-424f-8b38-252f6115640d']['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/akibat')?>"><span>Data Akibat</span></a></li>
                    <?php
                }
                if(isset($role['bfa6447a-7dc4-4495-9397-db2e5865ade1']['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/kendali')?>"><span>Data Kendali</span></a></li>
                    <?php
                }
                if(isset($role['340671b5-d101-46ec-b86f-6dc06e9d5668']['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/tipe_r')?>"><span>Data Tipe Resiko</span></a></li>
                    <?php
                }if(isset($role['acda06a9-1a27-45f0-bceb-1b77c81b8749']['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/jabatan')?>"><span>Data Jabatan</span></a></li>
                    <?php
                }
                if(isset($role['50492ec8-26b3-4178-9d66-501ccef6ef8b']['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/jenis')?>"><span>Data Jenis Audit</span></a></li>
                    <?php
                }
                if(isset($role['8a1dfd71-6c85-412f-91de-970ca913dc60']['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/satker')?>"><span>Data Satker</span></a></li>
                    <?php
                }
                if(isset($role['6ea5af6e-aeb0-4bde-9e9b-8dd58f322c75']['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/sasaran')?>"><span>Data Sasaran Audit</span></a></li>
                    <?php
                }
                $menuid='b4be4621-3e9e-4e3d-a47b-df549ae100f4';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/wilayah')?>">Data Wilayah </a></li>
                    <?php
                }
                $menuid='292e5ff3-1095-49b3-aafd-565794ee545e';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/conf_spt')?>">Konfigurasi Dasar SPT </a></li>
                    <?php
                }$menuid='92559c74-927e-4edf-85c8-203447141840';
                if(isset($role[$menuid]['u'])){
                    ?>
                    <li><a href="<?=base_url('master2/group')?>">Data Role</a></li>
                    <?php
                }
                if(isset($role['9312a5c1-7991-4eb1-be6d-05fef767935b']['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/tujuan')?>"><span>Data Tujuan Audit</span></a></li>
                    <?php
                }
                if(isset($role['e89cdedb-32f8-47fd-bbfc-960c894dae01']['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/golongan')?>"><span>Data Golongan</span></a></li>
                    <?php
                }
                $menuid='2f4e5764-01c0-49f5-b178-555f814a015a';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/tao')?>">Data TAO</a></li>
                    <?php
                }
                $menuid='1541696d-a165-4a06-9181-88472bd61293';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/aturan')?>">Data Aturan</a></li>
                    <?php
                }
                $menuid='c6fabac7-e2b3-4245-a69a-84992efa4bc1';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/kode_temuan')?>">Data Kode Temuan</a></li>
                    <?php
                }$menuid='c6fabac7-e2b3-4245-a69a-84992efa4bc1';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/kode_rekom')?>">Data Kode Rekomendasi</a></li>
                    <?php
                }$menuid='e36e779a-e2aa-468e-a11c-972f75c1c052';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/tindak_lanjut')?>">Data Kode Tindak Lanjut</a></li>
                    <?php
                }
                $menuid='7a2eee70-8883-4c74-9a9b-e820f2249b4b';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/pedoman')?>">Data Pedoman</a></li>
                    <?php
                }
                $menuid='1541696d-a165-4a06-9181-88472bd61294';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/dampak')?>">Data Dampak</a></li>
                    <?php
                }
                $menuid='d66ab356-6792-451d-8f42-84f07e48b9e7';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/user')?>">Data User</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e743';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/kegiatan')?>">Data Kegiatan</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e743';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/program')?>">Data Program</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e743';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/kejadian')?>">Data Kejadian</a></li>
                    <?php
                }
                $menuid='23ca0398-00b3-4547-9312-63633c0e4a19';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/opsi')?>">Data Opsi</a></li>
                    <?php
                }
                $menuid='23ca0398-00b3-4547-9312-63633c0e4a10';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/tk')?>">Rencana Tindak Terkait</a></li>
                    <?php
                }
                $menuid='23ca0398-00b3-4547-9312-63633c0e4b29';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/target')?>">Data Target Penurunan</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e734';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/program')?>">Data Program</a></li>
                    <?php
                }
                $menuid='36fbf7b0-d4ba-49d1-af67-1e0af4a05766';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master2/tim')?>">Data Tim</a></li>
                    <?php
                }?>

            </ul>
        </li>
    <?php
    }
    if(isset($role['50eba0ee-9760-4355-80c7-27e2f970bfc0']['r'])){
    ?>
        <li class="ini-haram"><a href="<?=base_url('produksi')?>" class="waves-effect"><i class="mdi mdi-auto-fix"></i><span> Produksi </span></a></li>
        <?php
    }
    if(isset($role['fb7ca00c-ef46-4dc1-bf9f-5d00358213a0']['r'])){
        ?>
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-scale-balance"></i> <span> Internal Proses</span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled"><?php
                $menuid='74fa394a-0fee-4b9d-827c-91a09f5c68f5';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/rencana')?>">Rencana Audit</a></li>
                    <?php
                }
                $menuid='e3033e60-42d8-47fd-8c8a-6e51cebaba33';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/persetujuan_r')?>">Persetujuan Rencana Audit</a></li>
                    <?php
                }
                $menuid='ca530176-384e-4c7d-8377-1171ec3a8d05';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/pelaksanaan')?>">Pelaksanaan Audit</a></li>
                    <?php
                }
                $menuid='b7d62610-7f21-43e4-a95b-43f1856411b4';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/persetujuan_p')?>">Persetujuan Pelaksanaan Audit</a></li>
                    <?php
                }
                $menuid='fe012b79-adf5-42f4-bdbd-5ffd5417289c';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/proses')?>">Pelaporan Hasil Audit</a></li>
                    <?php
                }
                $menuid='fe012b79-adf5-42f4-bdbd-5ffd5417289c';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/tindak_lanjut')?>">Temuan Tindak Lanjut</a></li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    }
    if(isset($role['923143fd-ca98-489d-88d7-642d2001b6ef']['r'])){
        ?>
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-account-box-outline"></i> <span> Manajemen Resiko</span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled"><?php
                $menuid='388deab8-36ff-4a8b-a1d7-b88eab8b4c26';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('resiko/program')?>">Konteks Program</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e732';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('resiko/kegiatan')?>">Konteks Kegiatan</a></li>
                    <?php
                } $menuid='388deab8-36ff-4a8b-a1d7-b88eab8b4c26';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('resiko/n_program')?>">Program</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e732';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('resiko/n_kegiatan')?>">Kegiatan</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e7ab';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('resiko/opsi')?>">Opsi & Mitigasi</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e7ac';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('resiko/monitoring')?>">Monitoring</a></li>
                    <?php
                }
//                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e7ac';
//                if(isset($role[$menuid]['r'])){
//                    ?>
<!--                    <li><a href="--><?//=base_url('resiko/Mitigasi')?><!--">Mitigasi</a></li>-->
<!--                    --><?php
//                }
                $menuid='cb7ac7ac-9d00-4acd-a1fd-2afefb0c8a81';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('resiko/kejadian')?>">Kejadian Satker</a></li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    }

    if(isset($role['923143fd-ca98-489d-88d7-642d2001b6ef']['r'])){
        ?>
        <li class="has_sub" hidden>
        
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-file-pdf"></i><span> Dokumen Sakip</span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled"><?php
                $menuid='388deab8-36ff-4a8b-a1d7-b88eab8b4c26';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('sakip/perencanaan')?>">Perencanaan</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e732';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('sakip/pengukuran')?>">Pengukuran</a></li>
                    <?php
                } $menuid='388deab8-36ff-4a8b-a1d7-b88eab8b4c26';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('sakip/pelaporan')?>">Pelaporan</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e732';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('sakip/evaluasi')?>">Evaluasi</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e7ab';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('sakip/prestasi')?>">Prestasi</a></li>
                    <?php
                }
               
                ?>
            </ul>
        </li>
        <?php
    }

   if(isset($role['e964618e-29bd-4e17-91f2-1f42c0fa0488']['r'])){
       ?>
       <li class="has_sub">
           <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-blinds"></i> <span> Laporan</span> <span class="menu-arrow"></span></a>
           <ul class="list-unstyled"><?php
               $menuid='82f810bf-86ce-4c88-927e-5e9a4ea82587';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/spt')?>">Monitoring SPT</a></li>
                   <?php
               }?>
                   <li><a href="<?=base_url('laporan/cetak_spt')?>">Pencarian No SPT</a></li>
                   <?php
               if(isset($role['8a1dfd71-6c85-412f-91de-970ca913dc60']['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/satker')?>"><span>Laporan Gabungan Resiko Per Satker</span></a></li>
                   <?php
               }
               if(isset($role['93391995-8d3d-447c-a50f-1cec916d3c27']['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/kejadian')?>"><span>Laporan Kejadian Satker</span></a></li>
                   <?php
               }
               if(isset($role['93391995-8d3d-447c-a50f-1cec916d3c27']['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/s_keg')?>"><span>Laporan Kegiatan Per Satker</span></a></li>
                   <?php
               }
               if(isset($role['e7dadcd0-c8ce-41e1-af4b-4fdc566a8262']['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/skala_resiko')?>"><span>Laporan Per Tipe Resiko</span></a></li>
                   <?php
               }
               if(isset($role['93391995-8d3d-447c-a50f-1cec916d3c27']['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/pr_p')?>"><span>Laporan Per Resiko & Kegiatan</span></a></li>
                   <?php
               }
               if(isset($role['cb7ac7ac-9d00-4acd-a1fd-2afefb0c8a81']['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/risiko_p')?>"><span>Laporan Resiko Per Satker</span></a></li>
                   <?php
               }
               $menuid='97c15763-d7b1-4846-9fc9-f5a36dfa5468';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/lhp')?>">Monitoring LHP</a></li>
                   <?php
               }
               $menuid='a9aeebbb-d0d1-4640-9867-2024c2ca71bf';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/lhp_o')?>">Laporan LHP Online</a></li>
                   <?php
               }
               $menuid='59b034e9-a1ed-4267-afba-606ead1b9500';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/file_manager')?>">File Manager</a></li>
                   <?php
               }
               $menuid='08f6f5e6-0933-48e2-88ce-ce52f2709040';
//               if(isset($role['08f6f5e6-0933-48e2-88ce-ce52f2709040']['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/rekap')?>">Rekap Temuan & Tindak Lanjut</a></li>
                   <?php
//               }
               $menuid='08f6f5e6-0933-48e2-88ce-ce52f2709040';
//               if(isset($role['08f6f5e6-0933-48e2-88ce-ce52f2709040']['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/temuan')?>">Rekap Temuan Satker</a></li>
                   <?php
//               }
   $menuid='e741e54c-9c5f-454e-a2f9-a38b8806091b';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/per_user')?>">Monitoring Penugasan</a></li>
                   <?php
               }$menuid='08f6f5e6-0933-48e2-88ce-ce52f2709040';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/lap_tl')?>">Laporan Tindak Lanjut</a></li>
                   <?php
               }$menuid='e964618e-29bd-4e17-91f2-1f42c0fa0428';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/lap_rgs')?>">Laporan Resiko Gabungan Per Satker</a></li>
                   <?php
               }
               ?>
           </ul>
       </li>
       <li><a href="<?=base_url('master/forum')?>" class="waves-effect"><i class="mdi mdi-account-network"></i><span> Q&A </span></a></li>
       <li><a href="<?=base_url('assets/manual_book.docx')?>" target="_blank" class="waves-effect"><i class="mdi mdi-file"></i><span> Manual Book </span></a></li>

       <?php
   }
   ?>
</ul>
