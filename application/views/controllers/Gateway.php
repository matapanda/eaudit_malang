<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gateway extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if($this->session->userdata('user')){
            redirect(base_url('welcome'));
        }
        $this->template->guest('login');
    }
    public function login(){
        if($this->input->get_post('username') && $this->input->get_post('password')){
            header("Content-Type: application/json");
            $this->load->model('user');
            echo json_encode($this->user->login($this->input->get_post('username'),$this->input->get_post('password')));
        }else{
            $this->index();
        }
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }
    public function forgot()
    {
        $this->template->guest('forgot');
    }
    public function lock()
    {
        if($this->session->userdata('user')){
            $data['user']=$this->session->userdata('user');
            if($this->input->get_post('password')){
                header("Content-Type: application/json");
                $this->load->model('user');
                echo json_encode($this->user->unlock($data['user']['id'],$this->input->get_post('password')));
            }else{
                $this->session->set_userdata('locked', true);
                $this->template->guest('lock',$data);
            }
        }else{
            $this->index();
        }
    }
    public function nyasar()
    {
        $this->template->guest('nyasar',array(),array('title'=>'403 Akses Haram'));
    }
}
