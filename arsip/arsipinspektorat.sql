-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2020 at 03:35 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arsipinspektorat`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_nama` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_nama`, `admin_username`, `admin_password`, `admin_foto`) VALUES
(1, 'ZUL FAHMI AKMAL', 'admin', '0192023a7bbd73250516f069df18b500', '372025300_pirik.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `arsip`
--

CREATE TABLE `arsip` (
  `arsip_id` int(11) NOT NULL,
  `arsip_waktu_upload` datetime NOT NULL,
  `arsip_petugas` varchar(100) NOT NULL,
  `arsip_kode` varchar(255) NOT NULL,
  `arsip_nama` varchar(255) NOT NULL,
  `arsip_jenis` varchar(255) NOT NULL,
  `arsip_kategori` int(11) NOT NULL,
  `arsip_keterangan` text NOT NULL,
  `arsip_file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `arsip`
--

INSERT INTO `arsip` (`arsip_id`, `arsip_waktu_upload`, `arsip_petugas`, `arsip_kode`, `arsip_nama`, `arsip_jenis`, `arsip_kategori`, `arsip_keterangan`, `arsip_file`) VALUES
(15, '2019-11-22 10:57:48', '35', 'asd', 'asd', 'jpg', 12, 'asda', '1192932053_era.jpg'),
(16, '2019-11-27 15:38:01', '76', '123', '1312', 'pdf', 12, 'sdsad', '1011456282_cv komputer mas dimas.pdf'),
(18, '2020-01-07 07:22:38', '76', '546464', 'surat undangan bla', 'jpg', 11, 'asdasd', '638801469_Untitled-1.jpg'),
(19, '2020-01-17 13:24:05', '56', '23', 'achmad faisol noerdie afwandy', 'xlsx', 11, 'bjkvbjbvk', '1364508391_kka.xlsx');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_nama` varchar(255) NOT NULL,
  `kategori_keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`kategori_id`, `kategori_nama`, `kategori_keterangan`) VALUES
(1, 'Tidak berkategori', 'Semua yang tidak memiliki kategori'),
(11, 'SURAT MASUK', 'SEKRETARIAT'),
(13, 'STS/RC/BUNGA BANK', 'EVALAP'),
(14, 'PAJAK PPN DAN PPH', 'EVALAP'),
(15, 'SURAT TEGURAN/PERINGATAN', 'EVALAP'),
(16, 'SK/PERDES', 'EVALAP'),
(17, 'SPJ EVALAP', 'EVALAP'),
(18, 'SURAT PERNYATAAN', 'EVALAP'),
(19, 'SURAT TINDAK LANJUT TEMUAN BPK', 'EVALAP'),
(20, 'SKTJM', 'EVALAP');

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `petugas_id` int(11) NOT NULL,
  `petugas_nama` varchar(255) NOT NULL,
  `petugas_username` varchar(255) NOT NULL,
  `petugas_password` varchar(255) NOT NULL,
  `petugas_foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`petugas_id`, `petugas_nama`, `petugas_username`, `petugas_password`, `petugas_foto`) VALUES
(7, 'Ir. PUDJO HARTANTO', 'pudjo', '202cb962ac59075b964b07152d234b70', ''),
(8, 'Drs. CHAIR EFFENDI', 'chair', '202cb962ac59075b964b07152d234b70', ''),
(9, 'I NENGAH ADNYANA, SE', 'nengah', '202cb962ac59075b964b07152d234b70', ''),
(10, 'SAMSUL ARIFIN, SH', 'samsul', '202cb962ac59075b964b07152d234b70', ''),
(11, 'ACHMAD SAEHO, SE', 'saeho', '21232f297a57a5a743894a0e4a801fc3', ''),
(12, 'Drs. AHMAD FAISHOL NS, MM', 'faishol', '202cb962ac59075b964b07152d234b70', ''),
(13, 'SLAMET SUKIRNO, SH', 'sukir', '202cb962ac59075b964b07152d234b70', ''),
(14, 'drh. KARTIKA WULANSARI', 'kartika', '202cb962ac59075b964b07152d234b70', ''),
(15, 'HARTATIK, SE, MM', 'hartatik', '202cb962ac59075b964b07152d234b70', ''),
(16, 'INDAH SURYANI, SE', 'indah', '202cb962ac59075b964b07152d234b70', ''),
(17, 'MOHAMAD ROFIQ CAHYONO, SH', 'rofiq', '202cb962ac59075b964b07152d234b70', ''),
(18, 'SUWANDI, SE', 'suwandi', '202cb962ac59075b964b07152d234b70', ''),
(19, 'MUSKHOLIFAH, ST', 'muskholifah', '202cb962ac59075b964b07152d234b70', ''),
(20, 'MARWOTO, SE', 'marwoto', '202cb962ac59075b964b07152d234b70', ''),
(21, 'ENDANG SRI WAHYUNINGSIH, S.Sos', 'endang', '202cb962ac59075b964b07152d234b70', ''),
(22, 'TIM 1', 'tim1', '202cb962ac59075b964b07152d234b70', ''),
(23, 'TIM 2', 'tim2', '202cb962ac59075b964b07152d234b70', ''),
(24, 'TIM 3', 'tim3', '202cb962ac59075b964b07152d234b70', ''),
(25, 'TIM 4', 'tim4', '202cb962ac59075b964b07152d234b70', ''),
(26, 'TIM 5', 'tim5', '202cb962ac59075b964b07152d234b70', ''),
(27, 'TIM 6', 'tim6', '202cb962ac59075b964b07152d234b70', ''),
(28, 'TIM 7', 'tim7', '202cb962ac59075b964b07152d234b70', ''),
(29, 'TIM 8', 'tim8', '202cb962ac59075b964b07152d234b70', ''),
(30, 'TIM 9', 'tim9', '202cb962ac59075b964b07152d234b70', ''),
(31, 'KASUBAG UMUM', 'umum', '202cb962ac59075b964b07152d234b70', ''),
(32, 'KASUBAG EVALAP', 'evalap', '202cb962ac59075b964b07152d234b70', ''),
(33, 'KASUBBAG PERENCANAAN', 'perencanaan', '202cb962ac59075b964b07152d234b70', ''),
(34, 'ARIE HARIOKO KURNIAWAN, S.Pt', 'harioko', '202cb962ac59075b964b07152d234b70', ''),
(35, 'DINAR REFA HARIMBI, SE,M', 'dinar', '202cb962ac59075b964b07152d234b70', ''),
(36, 'EKO KUSWOYO, SE.,M.Si', 'eko', '202cb962ac59075b964b07152d234b70', ''),
(37, 'HUSNUL BARIYAH, SE', 'husnul', '202cb962ac59075b964b07152d234b70', ''),
(38, 'ACHMAD ALFIAN, SE', 'alfian', '202cb962ac59075b964b07152d234b70', ''),
(39, 'FATKHURROJI', 'fatkhurroji', '202cb962ac59075b964b07152d234b70', ''),
(40, 'HAISAH, SH', 'haisah', '202cb962ac59075b964b07152d234b70', ''),
(41, 'ARY SUNARINGTYAS, SE', 'ary', '202cb962ac59075b964b07152d234b70', ''),
(42, 'BAMBANG HERYADIE, SH', 'bambang', '202cb962ac59075b964b07152d234b70', ''),
(43, 'DIAN FIRDAUS, SE', 'dian', '202cb962ac59075b964b07152d234b70', ''),
(44, 'FIRDAUS DWI PURWANTO', 'firdaus', '202cb962ac59075b964b07152d234b70', ''),
(45, 'KUS HARDINI, S.Pd', 'hardini', '202cb962ac59075b964b07152d234b70', ''),
(46, 'RINA SUMANTI, S.Sos', 'rina', '202cb962ac59075b964b07152d234b70', ''),
(47, 'RUSMIYATI, SH', 'rusmiyati', '202cb962ac59075b964b07152d234b70', ''),
(48, 'SUGIARTO MUSI, SH', 'sugiarto', '202cb962ac59075b964b07152d234b70', ''),
(49, 'IRIN CAHYAWATI, SE', 'irin', '202cb962ac59075b964b07152d234b70', ''),
(50, 'MAHFUD GURUH BUDIAWAN, SH', 'mahfud', '202cb962ac59075b964b07152d234b70', ''),
(51, 'HARLI ANGGARITA AKHIRING PUTRI, S.H', 'harli', '202cb962ac59075b964b07152d234b70', ''),
(52, 'RIZKI AMALIA PRATIWI, SE', 'rizki', '202cb962ac59075b964b07152d234b70', ''),
(53, 'MOHAMMAD NOERHADI', 'noerhadi', '202cb962ac59075b964b07152d234b70', ''),
(54, 'YUNAN HELMI ASSIDIQI, S.AB ', 'yunan', '202cb962ac59075b964b07152d234b70', ''),
(55, 'RADEN DYAH KUSUMAWATI AL RASYID', 'dyah', '202cb962ac59075b964b07152d234b70', ''),
(56, 'ACHMAD FAISOL NOERDIE AFWANDY, S.E.', 'faisol', '202cb962ac59075b964b07152d234b70', ''),
(57, 'APRILLIA SINTYA DEWI, S.E', 'april', '202cb962ac59075b964b07152d234b70', ''),
(58, 'BAKHRUL FADLILLAH AKBAR, S.E.', 'bakhrul', '202cb962ac59075b964b07152d234b70', ''),
(59, 'DRAJAD SURYA MAULANA, S.E.', 'drajad', '202cb962ac59075b964b07152d234b70', ''),
(60, 'MUHAMAD LUKMANIL HAKIM, S.E.', 'lukman', '202cb962ac59075b964b07152d234b70', ''),
(61, 'MUNFARIDA RIZKY MUTAMIMMAH, S.E.', 'kikik', '202cb962ac59075b964b07152d234b70', ''),
(62, 'NUR EFENDI, S.E.', 'efendi', '202cb962ac59075b964b07152d234b70', ''),
(63, 'RISKY AMALIASARI, S.E.', 'amel', '202cb962ac59075b964b07152d234b70', ''),
(64, 'TYAS DYAH FATMAWATI, S.E.', 'tyas', '202cb962ac59075b964b07152d234b70', ''),
(65, 'WAHYU GUSNARDIYANTO, S.E.', 'wahyu', '202cb962ac59075b964b07152d234b70', ''),
(66, 'ARIEF RAHMAN', 'arief', '202cb962ac59075b964b07152d234b70', ''),
(67, 'haryanti', 'haryanti', '202cb962ac59075b964b07152d234b70', ''),
(68, 'HARUN', 'harun', '202cb962ac59075b964b07152d234b70', ''),
(69, 'ACHMAD MULYADI', 'mulyadi', '202cb962ac59075b964b07152d234b70', ''),
(70, 'ARIEF PRASETYO ARIBOWO', 'wowok', '202cb962ac59075b964b07152d234b70', ''),
(71, 'SLAMET SURYADI', 'slamet', '202cb962ac59075b964b07152d234b70', ''),
(72, 'ZUL FAHMI AKMAL, S.Kom', 'zulfahmi', '202cb962ac59075b964b07152d234b70', ''),
(73, 'ERA NANDYA FEBRIANA, S.H', 'eranandya', '202cb962ac59075b964b07152d234b70', ''),
(74, 'WINDA DWI YUSNITA, S.E', 'winda', '202cb962ac59075b964b07152d234b70', ''),
(75, 'IWAN PRABOWO, S.E', 'cerahkanharimu', '202cb962ac59075b964b07152d234b70', ''),
(76, 'NURUL HIDAYATI', 'nurul', '202cb962ac59075b964b07152d234b70', '');

-- --------------------------------------------------------

--
-- Table structure for table `riwayat`
--

CREATE TABLE `riwayat` (
  `riwayat_id` int(11) NOT NULL,
  `riwayat_waktu` datetime NOT NULL,
  `riwayat_user` int(11) NOT NULL,
  `riwayat_arsip` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `riwayat`
--

INSERT INTO `riwayat` (`riwayat_id`, `riwayat_waktu`, `riwayat_user`, `riwayat_arsip`) VALUES
(11, '2019-11-26 07:52:56', 12, 15),
(12, '2019-11-26 07:53:02', 12, 15),
(13, '2020-01-07 07:25:05', 12, 16);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_nama` varchar(100) NOT NULL,
  `user_username` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_nama`, `user_username`, `user_password`, `user_foto`) VALUES
(12, 'INSPEKTUR KAB. BANYUWANGI', 'inspektur', '202cb962ac59075b964b07152d234b70', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `arsip`
--
ALTER TABLE `arsip`
  ADD PRIMARY KEY (`arsip_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`petugas_id`);

--
-- Indexes for table `riwayat`
--
ALTER TABLE `riwayat`
  ADD PRIMARY KEY (`riwayat_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `arsip`
--
ALTER TABLE `arsip`
  MODIFY `arsip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `kategori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `petugas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `riwayat`
--
ALTER TABLE `riwayat`
  MODIFY `riwayat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
