    <!doctype html>
    <html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>INSPEKTORAT KABUPATEN BANYUWANGI</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    </head>

    <body>

        <style type="text/css">

            body{margin-bottom: 0px;
            padding: 0px;
            background: url(bg.jpg);
            background-size: cover;
            background-position: center;
            font-family: sans-serif;
            height: 100vh;}
            
            .navbar-siad{
                background: #006df0;
                border-radius: 0px;
                border: 1px solid #006df0;
                margin: 0px;
                padding: 50px 0px;
				 
            }

            .navbar-inverse .navbar-brand {

                color: #fff;

            }

            .navbar-inverse .navbar-nav > li > a {

                color: #fff;

            }
            .navbar-siad > li > a {

                color: #9d9d9d !important;

            }

            .banner{
                background: #006df0;
                border-radius: 0px;
                border: 1px solid #006df0;
                padding: 8px 0px;
            }

            .banner{
                color: white;
            }

            .banner a{
                padding: 10px 15px;
                color: white;
                border: 1px solid white;
                -webkit-transition: all 0.5s; 
                transition: all 0.5s;
                margin-right: 10px;
            }

            .banner a:hover{
                text-decoration: none;
                border: 1px dashed white;
            }

            .banner p{
                font-size: 13pt;
            }
			
			.banner z{
                font-size: 30pt;
				
            }
        </style>



        <nav class="navbar navbar-inverse navbar-siad">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header" >
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button> 
                    <a class="navbar-brand" href="#"><h1>SELAMAT DATANG DI ARSIP DIGITAL</h1></a>
                    <a class="navbar-brand" href="#"><h1></h1></a>
					<br> </br>
					<a class="navbar-brand" href="#"><h1>INSPEKTORAT KABUPATEN BANYUWANGI</h1></a>
                   
                
				</div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                
            </div>
        </nav>


        <div class="banner">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6">
                        <br>
                        <div style="margin-top: 0px;">
                            <z>Sistem Informasi Arsip Digital</z>
							<p>Manajemen file arsip dengan mudah dan cepat.</p>

                           
                            <br>

                            <a href="user_login.php">LOGIN USER</a>
                            <br>
                            <br>
                            <br>
                            <a href="login.php">LOGIN ADMIN / PETUGAS</a>

                        </div>
                         <div class="col-lg-6">

                        <img src="gambar/depan/2.png">
                        
                    </div>
                    </div>
                    
                </div>

            </div>
        </div>





        <script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    </body>

    </html>