<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
       
    </div>

<div class="card" style="width: 75%; margin-bottom: 100px">
	<div class="card-body">
		
		<form method="POST" action ="<?php echo base_url('admin/dataaset/tambah_data_aksi') ?>" enctype="multipart/form-data" >
			
			<div class="form-group">
				<label>Jenis Barang</label>
				<input type="text" name="jenisbarang" class="form-control">
				<?php echo form_error ('jenisbarang','<div class="text-small text-danger"></div>') ?>
			</div>

			<div class="form-group">
				<label>ID Data</label>
				<input type="text" name="iddata" class="form-control">
				<?php echo form_error ('iddata','<div class="text-small text-danger"></div>') ?>
			</div>

			<div class="form-group">
				<label>Kode Barang</label>
				<input type="text" name="kodebarang" class="form-control">
				<?php echo form_error ('kodebarang','<div class="text-small text-danger"></div>') ?>
			</div>

			<div class="form-group">
				<label>Nama Barang</label>
				<input type="text" name="namabarang" class="form-control">
				<?php echo form_error ('namabarang','<div class="text-small text-danger"></div>') ?>
			</div>

			<div class="form-group">
				<label>Merk Barang</label>
				<input type="text" name="merkbarang" class="form-control">
				<?php echo form_error ('merkbarang','<div class="text-small text-danger"></div>') ?>
			</div>

			<div class="form-group">
				<label>Tipe Barang</label>
				<input type="text" name="tipebarang" class="form-control">
				<?php echo form_error ('tipebarang','<div class="text-small text-danger"></div>') ?>
			</div>

			<div class="form-group">
				<label>Tahun Barang</label>
				<input type="text" name="tahun" class="form-control">
				<?php echo form_error ('tahun','<div class="text-small text-danger"></div>') ?>
			</div>

			<div class="form-group">
				<label>Harga Barang</label>
				<input type="text" name="hargabarang" class="form-control">
				<?php echo form_error ('hargabarang','<div class="text-small text-danger"></div>') ?>
			</div>

			<div class="form-group">
				<label>Tim Pengguna</label>
				<input type="text" name="tim" class="form-control">
				<?php echo form_error ('tim','<div class="text-small text-danger"></div>') ?>
			</div>

			<div class="form-group">
				<label>Pengguna</label>
				<input type="text" name="pengguna" class="form-control">
				<?php echo form_error ('pengguna','<div class="text-small text-danger"></div>') ?>
			</div>

			<div class="form-group">
				<label>Keterangan</label>
				<input type="text" name="keterangan" class="form-control">
				<?php echo form_error ('keterangan','<div class="text-small text-danger"></div>') ?>
			</div>

			<div class="form-group">
				<label>Foto</label>
				<input type="file" name="foto" class="form-control">
				<?php echo form_error ('foto','<div class="text-small text-danger"></div>') ?>
			</div>

			
			<button type="submit" class="btn btn-sm btn-success mb-2" >Tambah Data</button>

		</form>

	</div>
	

</div>
   

   
</div>

 
