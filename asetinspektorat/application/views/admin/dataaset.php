<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><?php echo $title ?></h1>
       
    </div>

    <a class="btn btn-sm btn-success mb-2" href="<?php echo base_url('admin/dataaset/tambah_data') ?>"><i class="fas fa-plus"></i> Tambah Data</a>

    <?php echo $this->session->flashdata('pesan') ?>

   <table class="table table-bordered table-striped mt-2">
   	<tr>
   		<th class="test-center">No</th>
   		<th class="test-center">Jenis</th>
   		<th class="test-center">ID Data</th>
   		<th class="test-center">Kode</th>
   		<th class="test-center">Nama</th>
   		<th class="test-center">Merk</th>
   		<th class="test-center">Tipe</th>
   		<th class="test-center">Tahun</th>
      <th class="test-center">Harga</th>
      <th class="test-center">TIM</th>
      <th class="test-center">Pengguna</th>
      <th class="test-center">Keterangan</th>
      <th class="test-center">Foto</th>


   		<th class="test-center">Action</th>
   		
   		
   	</tr>

   	<?php $no=1; foreach ($aset as $as) : ?>
   	<tr>
   		<td><?php echo $no++ ?></td>
   		<td><?php echo $as->jenisbarang ?></td>
      <td><?php echo $as->iddata ?></td>
      <td><?php echo $as->kodebarang ?></td>
      <td><?php echo $as->namabarang ?></td>
      <td><?php echo $as->merkbarang ?></td>
      <td><?php echo $as->tipebarang ?></td> 
   		<td><?php echo $as->tahun ?></td> 
   		<td><?php echo $as->hargabarang ?></td> 
   		<td><?php echo $as->tim ?></td> 
   		<td><?php echo $as->pengguna ?></td> 
   		<td><?php echo $as->keterangan ?></td> 
  		<td><img src="<?php echo base_url().'assets/foto/'.$as->foto ?>" width="75px" ></td> 
   		<td>
   			<center>
   				<a class="btn btn-sm btn-primary" href="<?php echo base_url('admin/dataaset/update_data/'.$as->id_aset) ?>"><i class="fas fa-edit"></i></a>

   				<a onclick="return confirm('Yakin Hapus')" class="btn btn-sm btn-danger" href="<?php echo base_url('admin/dataaset/delete_data/'.$as->id_aset) ?>"><i class="fas fa-trash"></i></a>
  			</center>
   		</td>
   	</tr>
   <?php endforeach; ?>

</table>
   
</div>


