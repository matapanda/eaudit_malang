<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	public function index()
	{
		$this->_rules();

		if($this->form_validation->run()==FALSE){
			
			$data['title'] = "Halaman Login";
			$this->load->view('template_admin/header', $data);
			$this->load->view('formlogin');
		}else{
			$username=$this->input->post('username');
			$password=$this->input->post('password');

			$cek = $this->asetmodel->cek_login($username, $password);

			if($cek == FALSE)
			{

				$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible fade show" role="alert">
					  <strong> Username Atau Password Salah </strong>
					  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
					<span arial-hidden="true">&times;</span>
					  </button>
					</div>');
				redirect('welcome');

			}else{
				redirect ('admin/Dashboard');
			}

		}

		
	}

	public function _rules()
	{
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('password','password','required');
		
	}

}
