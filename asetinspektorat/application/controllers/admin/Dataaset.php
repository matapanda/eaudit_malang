<?php 

class Dataaset extends CI_Controller{
	public function index()
	{	
		$data['title'] = "DATA ASET KHUSUS PAK HARUN :)";
		$data['aset'] = $this->asetmodel->get_data('data_aset')->result();
		$this->load->view('template_admin/header', $data);
    	$this->load->view('template_admin/sidebar', $data);
    	$this->load->view('admin/dataaset', $data);
        $this->load->view('template_admin/footer', $data);
		
	}

	public function tambah_data()
	{	
		$data['title'] = "TAMBAH DATA ASET";
		$data['aset'] = $this->asetmodel->get_data('data_aset')->result();
		$this->load->view('template_admin/header', $data);
    	$this->load->view('template_admin/sidebar', $data);
    	$this->load->view('admin/tambahdataaset', $data);
        $this->load->view('template_admin/footer', $data);
		
	}

	public function tambah_data_aksi()
	{
		$this->_rules();
		if ($this->form_validation->run()== FALSE){
		$this->tambah_data();
		}else{
			$jenis_barang				= $this->input->post('jenisbarang');
			$id_data					= $this->input->post('iddata');
			$kode_barang				= $this->input->post('kodebarang');
			$nama_barang 				= $this->input->post('namabarang');
			$merk_barang				= $this->input->post('merkbarang');
			$tipe_barang				= $this->input->post('tipebarang');
			$tahun_barang				= $this->input->post('tahun');
			$harga_barang 				= $this->input->post('hargabarang');
			$tim 						= $this->input->post('tim');
			$pengguna_barang 			= $this->input->post('pengguna');
			$keterangan_barang 			= $this->input->post('keterangan');
			// $foto 						= $this->input->post('foto');
			if($foto=''){}else{
				$config ['upload_path']		= './assets/foto';
				$config ['allowed_types']	= 'jpg|jpeg|png|tiff';
				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('foto')){
					echo "Foto gagal Diupload!";
				}else {
					// $foto=$this->upload->data();
					$nama = $_FILES['foto']['name'];
				}
				
			}


			$data = array(

				'jenisbarang'			=> $jenis_barang,
				'iddata' 				=> $id_data,
				'kodebarang' 			=> $kode_barang,
				'namabarang' 			=> $nama_barang,
				'merkbarang' 			=> $merk_barang,
				'tipebarang' 			=> $tipe_barang,
				'tahun' 				=> $tahun_barang,
				'hargabarang' 			=> $harga_barang,
				'tim' 					=> $tim,
				'pengguna' 				=> $pengguna_barang,
				'keterangan' 			=> $keterangan_barang,
				'foto' 					=> $nama,

			);

			$this->asetmodel->tambah_data($data,'data_aset');
			$this->session->set_flashdata('pesan','<div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>DATA BERHASIL DITAMBAHKAN</strong>
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
<span arial-hidden="true">&times;</span>
  </button>
</div>');
			redirect('admin/dataaset');

		}
	}


public function update_data($id)
	{	
		$where = array('id_aset' => $id);
		$data['aset'] = $this->db->query("SELECT * FROM data_aset WHERE id_aset = '$id'")->result();
		$data['title'] = 'Data Aset';
		$this->load->view('template_admin/header', $data);
    	$this->load->view('template_admin/sidebar', $data);
    	$this->load->view('admin/updatedataaset', $data);
        $this->load->view('template_admin/footer', $data);
		
	}

	public function update_data_aksi()
	{
		$this->_rules();
		if ($this->form_validation->run()== FALSE){
		$this->update_data();
		}else{
			$id 						= $this->input->post('id_aset');
			$jenis_barang				= $this->input->post('jenisbarang');
			$id_data					= $this->input->post('iddata');
			$kode_barang				= $this->input->post('kodebarang');
			$nama_barang 				= $this->input->post('namabarang');
			$merk_barang				= $this->input->post('merkbarang');
			$tipe_barang				= $this->input->post('tipebarang');
			$tahun_barang				= $this->input->post('tahun');
			$harga_barang 				= $this->input->post('hargabarang');
			$tim 						= $this->input->post('tim');
			$pengguna_barang 			= $this->input->post('pengguna');
			$keterangan_barang 			= $this->input->post('keterangan');
			// $foto 						= $this->input->post('foto');
			if($_FILES['foto']['name'] != '' || !empty($_FILES['foto']['name'])){
				$config ['upload_path']		= './assets/foto';
				$config ['allowed_types']	= 'jpg|jpeg|png|tiff';
				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('foto')){
					echo "Foto gagal Diupload!";
				}else {
					// $foto=$this->upload->data();
					$nama = $_FILES['foto']['name'];
				}
			}else{
				$nama = null;
				
			}


			$data = array(

				'jenisbarang'			=> $jenis_barang,
				'iddata' 				=> $id_data,
				'kodebarang' 			=> $kode_barang,
				'namabarang' 			=> $nama_barang,
				'merkbarang' 			=> $merk_barang,
				'tipebarang' 			=> $tipe_barang,
				'tahun' 				=> $tahun_barang,
				'hargabarang' 			=> $harga_barang,
				'tim' 					=> $tim,
				'pengguna' 				=> $pengguna_barang,
				'keterangan' 			=> $keterangan_barang,
				'foto' 					=> $nama,

			);

			$where = array(
				'id_aset' => $id
			);

			$this->asetmodel->update_data('data_aset', $data, $where);
			$this->session->set_flashdata('pesan','<div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>DATA BERHASIL DIUPDATE</strong>
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
<span arial-hidden="true">&times;</span>
  </button>
</div>');
			redirect('admin/dataaset');

		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('jenisbarang','jenis barang','required'); //jenisbarang sama kaya DB
		$this->form_validation->set_rules('iddata','kode barang','required');
		$this->form_validation->set_rules('kodebarang','tipe barang','required');
		$this->form_validation->set_rules('namabarang','tim','required');
		$this->form_validation->set_rules('merkbarang','pengguna barang','required');
		$this->form_validation->set_rules('tipebarang','tahun barang','required');
		$this->form_validation->set_rules('tahun','keterangan barang','required');
		$this->form_validation->set_rules('hargabarang','keterangan barang','required');
		$this->form_validation->set_rules('tim','keterangan barang','required');
		$this->form_validation->set_rules('pengguna','keterangan barang','required');
		$this->form_validation->set_rules('keterangan','keterangan barang','required');
		// $this->form_validation->set_rules('foto','foto barang','required');
		if (empty($_FILES['foto']['name'])){
       		$this->form_validation->set_rules('foto', 'foto barang', 'required');
      	}


	}

	public function delete_data($id){
	
	$where = array('id_aset' => $id);
	$this->asetmodel->delete_data($where, 'data_aset');
	$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong> DATA BERHASIL DIHAPUS! </strong>
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
<span arial-hidden="true">&times;</span>
  </button>
</div>');
			redirect('admin/dataaset');
	}

}

 ?>