<?php 

class Dashboard extends CI_Controller{
    public function index()
    {
    	$data['title'] = "Dashboard";
    	$aset = $this->db->query("SELECT * FROM data_aset");
		$data['aset'] = $aset ->num_rows();
    	$this->load->view('template_admin/header', $data);
    	$this->load->view('template_admin/sidebar', $data);
    	$this->load->view('admin/dashboard', $data);
        $this->load->view('template_admin/footer', $data);
    }
}

?>