<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user extends CI_Model
{
    public function login($username, $password)
    {
        $retry = $this->session->tempdata('authorize') + 1;
        $this->session->set_tempdata('authorize', $retry, 300);
        $data['status'] = 0;
        if ($retry < 5) {
            $this->db->where('username', $username);
            $this->db->where('password', md5($password));
            $user = $this->db->get('users')->row_array();
            if (isset($user['id'])) {
                if ($user['status']) {
                    $data['status'] = 1;
                    $data['message'] = 'Login success!';
                    $this->session->set_userdata('user', $user);
                } else {
                    $data['message'] = 'Account inactive!';
                }
            } else {
                $data['message'] = 'Invalid username/password!';
            }
        } else {
            $data['message'] = 'Too many login attempts. Try again later!';
        }
        return $data;
    }

    public function unlock($id, $password)
    {
        $retry = $this->session->tempdata('authorize') + 1;
        $this->session->set_tempdata('authorize', $retry, 300);
        $data['status'] = 0;
        if ($retry < 5) {
            $this->db->where('id', $id);
            $this->db->where('password', md5($password));
            $user = $this->db->get('users')->row_array();
            if (isset($user['id'])) {
                $data['status'] = 1;
                $data['message'] = 'Login success!';
                $this->session->set_userdata('locked', null);
            } else {
                $data['message'] = 'Invalid password!';
            }
        } else {
            $data['message'] = 'Too many login attempts. Try again later!';
        }
        return $data;
    }

    //Program e-audit

    public function get_data_program($id)
    {
        $this->db->where('support.audit_program.id', $id);
        $this->db->join('master.jenis', 'master.jenis.id=support.audit_program.jenis_audit');
        $this->db->join('master.tujuan', 'master.tujuan.id=support.audit_program.tujuan');
        return $this->db->get('support.audit_program')->result_array();
    }

    public function get_group_access($options = array())
    {
        $this->db->order_by('name');
        if (count($options) > 0) {
            $this->db->where($options);
        }
        return $this->db->get("role")->result_array();
    }

    public function get_users_group($id)
    {
        $this->db->where('user', $id);
        $data = $this->db->get("roleusers")->result_array();
        $result = array();
        foreach ($data as $d) {
            $result[] = $d['role'];
        }
        return $result;
    }

    //TIM E-audit
    public function get_tim_group($id)
    {
        $this->db->where('tim', $id);
        $data = $this->db->get("support.anggota")->result_array();
        $result = array();
        foreach ($data as $d) {
            $result[] = $d['anggota'];
        }
        return $result;
    }


    //Pedoman e-audit
    public function get_list_pedoman()
    {
        $this->db->order_by('support.pedoman.kode');
        $this->db->group_by('support.pedoman.id, master.sasaran.id, master.jenis.id');
        $this->db->join('master.sasaran', 'master.sasaran.id=support.pedoman.sasaran', 'left');
        $this->db->join('master.jenis', 'master.jenis.id=support.pedoman.jenis', 'left');
        $this->db->select('support.pedoman.file as file ,support.pedoman.id as id, support.pedoman.status as status, support.pedoman.kode as kode, master.jenis.id as idj, master.jenis.ket as jenis, master.jenis.kode as jk, master.sasaran.id as ids, master.sasaran.ket as sasaran, master.sasaran.kode as sk');
        return $this->db->get("support.pedoman")->result_array();
    }public function get_list_p_satker($closed,$periode)
    {
        if($closed!=''){
            $this->db->where('p.status', $closed);
        }
        if($periode!=''){
            $this->db->where('p.periode', $periode);
        }
        $this->db->order_by('p.nama');
        $this->db->join('master.prosedur_audit a', 'a.id=p.prosedur_audit', 'left');
        $this->db->join('master.penyebab pe', 'pe.id=(p.penyebab)::uuid', 'left');
        $this->db->join('master.kendali ks', 'ks.id=p.kendali_s', 'left');
        $this->db->join('master.kendali ka', 'ka.id=p.kendali_a', 'left');
        $this->db->join('master.tipe_resiko tr', 'tr.id=p.resiko', 'left');
        $this->db->select('p.*, a.nama as nama_prosedur,pe.nama as penyebab_n, ks.nama as kendali_s_n, ka.nama as kendali_a_n, tr.nama as resiko_n ');
        return $this->db->get("support.program_satker p")->result_array();
    }
    public function get_list_k_satker($closed,$periode,$yes=0)
    {

        $this->db->order_by('p.nama');
        if($closed!=''){
            $this->db->where('k.status', $closed);
        }
        if($periode!=''){
            $this->db->where('k.periode', $periode);
        }
        if($yes==1){
            $this->db->where('kr.nilai!=', '');
            $this->db->join('support.kegiatan_kr kr', 'kr.id=k.id', 'left');
        }
        elseif($yes==2){
            $this->db->where('kr.target!=', null);
            $this->db->join('support.kegiatan_kr kr', 'kr.id=k.id', 'left');
        }

        $this->db->distinct();
        $this->db->join('master.prosedur_audit a', 'a.id=k.prosedur_audit', 'left');
        $this->db->join('support.program_satker p', 'p.id=k.program_satker', 'left');
        $this->db->join('master.penyebab pe', 'pe.id=(k.penyebab)::uuid', 'left');
        $this->db->join('master.kendali ks', 'ks.id=k.kendali_s', 'left');
        $this->db->join('master.satker sa', 'sa.id=k.satker', 'left');
        $this->db->join('master.kendali ka', 'ka.id=k.kendali_a', 'left');
        $this->db->join('master.kegiatan mk', 'mk.id=k.kegiatan', 'left');
        $this->db->join('master.tipe_resiko tr', 'tr.id=k.resiko', 'left');
        $this->db->select('k.*,sa.nama as nm_sa,mk.nama as nama_keg,p.nama as nama_program, a.nama as nama_prosedur,pe.nama as penyebab_n, ks.nama as kendali_s_n, ka.nama as kendali_a_n, tr.nama as resiko_n ');
        return $this->db->get("support.kegiatan_satker k")->result_array();
    }
    public function get_list_ke_satker($closed,$periode)
    {
        if($closed!=''){
            $this->db->where('k.status', $closed);
        }
        if($periode!=''){
            $this->db->where('k.periode', $periode);
        }
        $this->db->order_by('p.nama');
        $this->db->join('master.prosedur_audit a', 'a.id=k.prosedur_audit', 'left');
        $this->db->join('support.program_satker p', 'p.id=k.pk', 'left');
        $this->db->join('support.kegiatan_satker ke', 'ke.id=k.pk', 'left');
        $this->db->join('master.penyebab pe', 'pe.id=(k.penyebab)::uuid', 'left');
        $this->db->join('master.kendali ks', 'ks.id=k.kendali_s', 'left');
        $this->db->join('master.kendali ka', 'ka.id=k.kendali_a', 'left');
        $this->db->join('master.tipe_resiko tr', 'tr.id=k.resiko', 'left');
        $this->db->select('k.*,ke.nama as nama_kegiatan,p.nama as nama_program, a.nama as nama_prosedur,pe.nama as penyebab_n, ks.nama as kendali_s_n, ka.nama as kendali_a_n, tr.nama as resiko_n ');
        return $this->db->get("support.kejadian_satker k")->result_array();
    }

    //TAO e-audit
    public function get_list_tao()
    {
        $this->db->order_by('support.tao.kode');
        $this->db->group_by('master.tahapan_audit.tahapan,master.tahapan_audit.kode,support.tao.id, master.sasaran.id, master.jenis.id, master.tujuan.id', 'master.tahapan_audit.kode');
        $this->db->join('master.sasaran', 'master.sasaran.id=support.tao.sasaran', 'left');
        $this->db->join('master.tujuan', 'master.tujuan.id=support.tao.tujuan', 'left');
        $this->db->join('master.jenis', 'master.jenis.id=support.tao.jenis', 'left');
        $this->db->join('master.tahapan_audit', 'master.tahapan_audit.kode=support.tao.tahapan', 'left');
        $this->db->select('master.tahapan_audit.tahapan as tahapan,master.tahapan_audit.kode as kt,support.tao.langkah as langkah ,support.tao.id as id, support.tao.status as status, support.tao.kode as kode, master.jenis.id as idj, master.jenis.ket as jenis, master.jenis.kode as jk, master.sasaran.id as ids, master.sasaran.ket as sasaran, master.sasaran.kode as sk, master.tujuan.id as idt, master.tujuan.ket as tujuan, master.tujuan.kode as tk');
        return $this->db->get("support.tao")->result_array();
    }

    //Aturan e-audit
    public function get_list_aturan()
    {
        $this->db->order_by('support.aturan.kode');
        $this->db->group_by('support.aturan.id, master.sasaran.id, master.jenis.id');
        $this->db->join('master.sasaran', 'master.sasaran.id=support.aturan.sasaran', 'left');
        $this->db->join('master.jenis', 'master.jenis.id=support.aturan.jenis', 'left');
        $this->db->select('support.aturan.file as file ,support.aturan.id as id, support.aturan.status as status, support.aturan.kode as kode, master.jenis.id as idj, master.jenis.ket as jenis, master.jenis.kode as jk, master.sasaran.id as ids, master.sasaran.ket as sasaran, master.sasaran.kode as sk');
        return $this->db->get("support.aturan")->result_array();
    }

    //Kertas Kerja e-audit
    public function get_list_kk()
    {
        $this->db->order_by('support.kertas_kerja.kode');
        $this->db->group_by('support.kertas_kerja.id, master.sasaran.id, master.jenis.id');
        $this->db->join('master.sasaran', 'master.sasaran.id=support.kertas_kerja.sasaran', 'left');
        $this->db->join('master.jenis', 'master.jenis.id=support.kertas_kerja.jenis', 'left');
        $this->db->select('support.kertas_kerja.file as file ,support.kertas_kerja.id as id, support.kertas_kerja.status as status, support.kertas_kerja.kode as kode, master.jenis.id as idj, master.jenis.ket as jenis, master.jenis.kode as jk, master.sasaran.id as ids, master.sasaran.ket as sasaran, master.sasaran.kode as sk');
        return $this->db->get("support.kertas_kerja")->result_array();
    }

    //User e-audit
    public function get_list_user()
    {
        $this->db->order_by('users.name');
        $this->db->group_by('users.avatar,users.status,users.modified_at,users.created_at,users.password,users.username,"users"."id","users"."name",users.email');
        $this->db->where('users.username is not null', null,false);
        $this->db->join('roleusers', 'roleusers.user=users.id', 'left');
        $this->db->join('role', 'roleusers.role=role.id', 'left');
        $this->db->select('users.*,string_agg(role.name, \';\') as role');
        return $this->db->get("users")->result_array();
    }
    public function add_kode_temuan($data){
        $user = $this->session->userdata('user');
        if(@$data['parent']){
        $this->db->where('id',$data['parent']);
        $parent=$this->db->get('support.kode_temuan')->row_array();

        $this->db->where('level',($parent['level']+1));
        $this->db->where('k2', $parent['k1']);
        if($parent['level']>1) {
            $this->db->where('k2', $parent['k2']);
            if ($parent['level'] > 2) {
                $this->db->where('k3', $parent['k3']);
            }
        }
        $exist=$this->db->get('support.kode_temuan')->num_rows();
//echo json_encode($parent['level']);
//        die();
        if($exist>0){
            return false;
        }else{
            $this->load->library('uuid');
            $update=array(
                'id'=>$this->uuid->v4(),
                'k1'=>$parent['k1'],
                'k2'=>$parent['k2'],
                'k3'=>$parent['k3'],
                'k4'=>$parent['k4'],
                'nama'=>$data['nama'],
                'level'=>($parent['level']+1),
                'created_by'=>$user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            );
            $update['k'.$update['level']]=$data['kode'];
            return $this->db->insert('support.kode_temuan',$update);
        }
        }else{
//            echo "a";die();
            $this->load->library('uuid');
            $update=array(
                'id'=>$this->uuid->v4(),
                'k1'=>$data['kode'],
                'k2'=>'',
                'k3'=>'',
                'k4'=>'',
                'nama'=>$data['nama'],
                'level'=>1,
                'created_by'=>$user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            );
            $update['k'.$update['level']]=$data['kode'];
            return $this->db->insert('support.kode_temuan',$update);
        }
    }
    public function get_list_user_kinerja($tahun)
    {
        $this->db->order_by("realisasi,jumlah",'desc');
        return $this->db->get("interpro.kinerja('$tahun')")->result_array();
    }

    //Wilayah e-audit

    public function get_list_wilayah()
    {
        $this->db->order_by('support.wilayah.kode');
        $this->db->group_by('support.wilayah.id, public.users.id');
        $this->db->join('public.users', 'public.roleusers.user=public.users.id');
        $this->db->join('support.wilayah', 'public.roleusers.user=support.wilayah.irban', 'right');
        $this->db->select('support.wilayah.kode as kode, support.wilayah.wilayah as wilayah, support.wilayah.id as id,support.wilayah.status as status ,public.users.name as name,public.users.id as uid');
        return $this->db->get('public.roleusers')->result_array();
    }

    //Tim e-audit
    public function get_list_tim()
    {
        $this->db->order_by('support.tim.tim');
        $this->db->join('public.users t1', 't1.id=support.tim.ketua', 'left');
        $this->db->join('public.users t2', 't2.id=support.tim.dalnis', 'left');
        $this->db->join('support.wilayah', 'support.wilayah.id=support.tim.wilayah');
        $this->db->select('support.tim.status as status,support.wilayah.wilayah as wilayah,support.wilayah.id as wid,support.wilayah.kode as wk, support.tim.id as id, support.tim.tim as tim,t1.name as name1, t2.name as name2, t1.id as tid1, t2.id as tid2');
        return $this->db->get('support.tim')->result_array();
    }

//PKPT e-audit

    public function get_list_pkpt($tahun)
    {
        $this->db->order_by('p.no');
        $this->db->group_by('p.id,t.id,s.id,j.id');
        $this->db->where('substr(p.no,0,5)',$tahun);
        $this->db->select('p.*,t.ket as tujuan_n,s.ket as sasaran_n,j.ket as jenis_n');
        $this->db->join('master.tujuan t', '((t.id)::varchar)=p.tujuan', 'left');
        $this->db->join('master.sasaran s', 's.id=p.sasaran', 'left');
        $this->db->join('master.jenis j', 'j.id=p.jenis', 'left');
        return $this->db->get('master.pkpt p')->result_array();
    }

    //Jabatan e-audit

    public function get_list_jabatan()
    {
        $this->db->order_by('order');
        $this->db->group_by('id');
        return $this->db->get('master.jabatan')->result_array();
    }
    public function get_list_prosedur_audit()
    {
        $this->db->order_by('nama');
        $this->db->group_by('id');
        return $this->db->get('master.prosedur_audit')->result_array();
    }
    public function get_list_tipe_resiko()
    {
        $this->db->order_by('nama');
        return $this->db->get('master.tipe_resiko')->result_array();
    }
    public function get_list_kendali()
    {
        $this->db->order_by('nama');
        return $this->db->get('master.kendali')->result_array();
    }
    public function get_list_penyebab()
    {
        $this->db->order_by('nama');
        return $this->db->get('master.penyebab')->result_array();
    }
    public function get_list_dampak()
    {
        $this->db->order_by('nama');
        return $this->db->get('master.dampak')->result_array();
    }
    public function get_list_akibat()
    {
        $this->db->order_by('nama');
        return $this->db->get('master.akibat')->result_array();
    }
    public function get_list_kegiatan()
    {
        $this->db->order_by('nama');
        return $this->db->get('master.kegiatan')->result_array();
    }
    public function get_list_m_program()
    {
        $this->db->order_by('nama');
        return $this->db->get('master.program')->result_array();
    } public function get_list_mtindak_lanjut()
    {
        $this->db->order_by('nama');
        return $this->db->get('master.tindak_lanjut')->result_array();
    }
    public function get_list_conf_spt()
    {
        $this->db->order_by('order');
        $this->db->group_by('id');
        return $this->db->get('master.conf_spt')->result_array();
    }

    //Jenis e-audit

    public function get_list_jenis()
    {
        $this->db->order_by('order');
        $this->db->group_by('id');
        return $this->db->get('master.jenis')->result_array();
    }
    //Narasumber e-audit

    public function get_list_narasumber()
    {
        $this->db->order_by('order');
        $this->db->group_by('id');
        return $this->db->get('master.narasumber')->result_array();
    }

    //Tema e-audit

    public function get_list_tema()
    {
        $this->db->order_by('order');
        $this->db->group_by('id');
        return $this->db->get('master.tema')->result_array();
    }
    //Tamu e-audit

    public function get_list_tamu()
    {
        $this->db->order_by('order');
        $this->db->group_by('id');
        return $this->db->get('master.tamu')->result_array();
    }
    //Tindak Lanjut e-audit

    public function get_list_tindak_lanjut()
    {
        $this->db->order_by('order');
        $this->db->group_by('id');
        return $this->db->get('master.tindak_lanjut')->result_array();
    }
    //Satker e-audit

    public function get_list_satker()
    {
        $this->db->order_by('(resiko)::int','desc');
        $this->db->group_by('id');
        return $this->db->get('master.satker')->result_array();
    }

    //Periode e-audit

    public function get_list_periode()
    {
        $this->db->order_by('nama');
        return $this->db->get('periode')->result_array();
    }

    //Sasaran e-audit

    public function get_list_sasaran()
    {
        $this->db->order_by('order');
        $this->db->group_by('id');
        return $this->db->get('master.sasaran')->result_array();
    }

    //Tujuan e-audit

    public function get_list_tujuan()
    {
        $this->db->order_by('order');
        $this->db->group_by('id');
        return $this->db->get('master.tujuan')->result_array();
    }
    public function get_list_opsi()
    {
        $this->db->order_by('kode');
        $this->db->group_by('id');
        return $this->db->get('master.opsi')->result_array();
    }
    public function get_list_tk()
    {
        $this->db->order_by('kode');
        $this->db->group_by('id');
        return $this->db->get('master.tindak')->result_array();
    }
    public function get_list_target()
    {
        $this->db->order_by('kode');
        $this->db->group_by('id');
        return $this->db->get('master.target')->result_array();
    }

    //Golongan e-audit

    public function get_list_golongan()
    {
        $this->db->order_by('order');
        $this->db->group_by('id');
        return $this->db->get('master.golongan')->result_array();
    }

    //Peran e-audit

    public function get_list_peran()
    {
        $this->db->order_by('order');
        $this->db->group_by('id');
        return $this->db->get('master.peran')->result_array();
    }

    //Program e-audit

    public function get_list_program()
    {
        $this->db->order_by('support.audit_program.kode');
        $this->db->group_by('support.audit_program.id, master.jenis.ket, master.tujuan.ket');
        $this->db->select('support.audit_program.id as id,support.audit_program.tahapan as tahapan,support.audit_program.status as status, support.audit_program.kode as kode, master.jenis.ket as jenis, master.tujuan.ket as tujuan');
        $this->db->join('master.jenis', 'master.jenis.id=support.audit_program.jenis_audit');
        $this->db->join('master.tujuan', 'master.tujuan.id=support.audit_program.tujuan');
        return $this->db->get('support.audit_program')->result_array();
    }


    public function get_user_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get("users")->row_array();
    }
    public function get_setting_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get("users")->row_array();
    }

    //PKPT e-audit
    public function get_pkpt_by_id($id)
    {
        $this->db->where('master.pkpt.id', $id);
        $this->db->order_by('master.pkpt.no');
        return $this->db->get("master.pkpt")->row_array();
    }
    //Pedoman e-audit
    public function get_pedoman_by_id($id)
    {
        $this->db->where('support.pedoman.id', $id);
        $this->db->order_by('support.pedoman.kode');
        $this->db->group_by('support.pedoman.id, master.sasaran.id, master.jenis.id');
        $this->db->join('master.sasaran', 'master.sasaran.id=support.pedoman.sasaran', 'left');
        $this->db->join('master.jenis', 'master.jenis.id=support.pedoman.jenis', 'left');
        $this->db->select('support.pedoman.file as file ,support.pedoman.id as id, support.pedoman.kode as kode, master.jenis.id as idj, master.jenis.ket as jenis, master.jenis.kode as jk, master.sasaran.id as ids, master.sasaran.ket as sasaran, master.sasaran.kode as sk');
        return $this->db->get("support.pedoman")->row_array();
    }

    //Aturan e-audit
    public function get_aturan_by_id($id)
    {
        $this->db->where('support.aturan.id', $id);
        $this->db->order_by('support.aturan.kode');
        $this->db->group_by('support.aturan.id, master.sasaran.id, master.jenis.id');
        $this->db->join('master.sasaran', 'master.sasaran.id=support.aturan.sasaran', 'left');
        $this->db->join('master.jenis', 'master.jenis.id=support.aturan.jenis', 'left');
        $this->db->select('support.aturan.file as file ,support.aturan.id as id, support.aturan.kode as kode, master.jenis.id as idj, master.jenis.ket as jenis, master.jenis.kode as jk, master.sasaran.id as ids, master.sasaran.ket as sasaran, master.sasaran.kode as sk');
        return $this->db->get("support.aturan")->row_array();
    }

    //Kertas Kerja e-audit
    public function get_kk_by_id($id)
    {
        $this->db->where('support.kertas_kerja.id', $id);
        $this->db->order_by('support.kertas_kerja.kode');
        $this->db->group_by('support.kertas_kerja.id, master.sasaran.id, master.jenis.id');
        $this->db->join('master.sasaran', 'master.sasaran.id=support.kertas_kerja.sasaran', 'left');
        $this->db->join('master.jenis', 'master.jenis.id=support.kertas_kerja.jenis', 'left');
        $this->db->select('support.kertas_kerja.file as file ,support.kertas_kerja.id as id, support.kertas_kerja.kode as kode, master.jenis.id as idj, master.jenis.ket as jenis, master.jenis.kode as jk, master.sasaran.id as ids, master.sasaran.ket as sasaran, master.sasaran.kode as sk');
        return $this->db->get("support.kertas_kerja")->row_array();
    }

    //TAO Detail e-audit
    public function get_tao_detail_by_id($id)
    {
        $this->db->where('support.tao_detail.id', $id);
        $this->db->order_by('support.tao_detail.id');
        $this->db->group_by('support.tao_detail.id');
        $this->db->join('support.tao', 'support.tao.id=support.tao_detail.id_tao');
        $this->db->select('support.tao_detail.*');
        return $this->db->get("support.tao_detail")->row_array();
    }

    //TAO e-audit
    public function get_tao_by_id($id)
    {
        $this->db->where('support.tao.id', $id);
        $this->db->order_by('support.tao.kode');
        $this->db->group_by('master.tahapan_audit.tahapan ,master.tahapan_audit.kode,support.tao.id, master.sasaran.id, master.jenis.id, master.tujuan.id', 'master.tahapan_audit.kode');
        $this->db->join('master.sasaran', 'master.sasaran.id=support.tao.sasaran', 'left');
        $this->db->join('master.tujuan', 'master.tujuan.id=support.tao.tujuan', 'left');
        $this->db->join('master.jenis', 'master.jenis.id=support.tao.jenis', 'left');
        $this->db->join('master.tahapan_audit', 'master.tahapan_audit.kode=support.tao.tahapan');
        $this->db->select('master.tahapan_audit.tahapan as tahapan,master.tahapan_audit.kode as kt,support.tao.langkah as langkah ,support.tao.id as id,support.tao.kode as kode, support.tao.status as status, support.tao.kode as kode, master.jenis.id as idj, master.jenis.ket as jenis, master.jenis.kode as jk, master.sasaran.id as ids, master.sasaran.ket as sasaran, master.sasaran.kode as sk, master.tujuan.id as idt, master.tujuan.ket as tujuan, master.tujuan.kode as tk');
        return $this->db->get("support.tao")->row_array();
    }

    //TIM E-audit
    public function get_tim_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get("support.tim")->row_array();
    }

    //Upload Pedoman E-audit
    public function upload_pedoman($input)
    {
        $this->db->trans_begin();
        if ($_FILES['file']['size'] == 0) {
            $this->db->where('id', $input['id_file']);
            $this->db->update('support.pedoman', array(
                    'kode' => $input['kode'],
                    'jenis' => $input['jenis'],
                    'sasaran' => $input['sasaran'],
                )
            );
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        } else if ($_FILES['file']['size'] <> 0) {
            $config['upload_path'] = './img/';
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $data['status_update'] = 'error';
                $data['status_message'] = $this->upload->display_errors('', '');
            } else {
                $file = $this->upload->data();

                $this->db->where('id', $input['id_file']);
                $sebelumnya = $this->db->get('support.pedoman')->row_array();

                if (strlen($sebelumnya['file']) > 1) {
                    if (file_exists($config['upload_path'] . $sebelumnya['file'])) {
                        unlink($config['upload_path'] . $sebelumnya['file']);
                    }
                }

                $this->db->where('id', $input['id_file']);
                $this->db->update('support.pedoman', array(
                        'kode' => $input['kode'],
                        'jenis' => $input['jenis'],
                        'sasaran' => $input['sasaran'],
                        'file' => $file["file_name"]
                    )
                );
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return false;
                } else {
                    $this->db->trans_commit();
                    return true;
                }
            }
        }
    }

    //Insert Detail TAO E-audit
    public function insert_detail_tao($tao, $input)
    {
        $config['upload_path'] = './img/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file_download')) {
            return array('error', $this->upload->display_errors());
        } else {
            $this->db->trans_begin();
            $file = $this->upload->data();
            $this->load->library('uuid');
            $id = $this->uuid->v4();
            $this->db->insert('support.tao_detail', array(
                    'id' => $id,
                    'id_tao' => $tao,
                    'langkah' => $input['langkah'],
                    'kertas_kerja' => $input['kertas'],
                    'file_download' => $file["file_name"],
                    'status' => 't'
                )
            );
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('error', 'Kertas kerja gagal tersimpan');
            } else {
                $this->db->trans_commit();
                return array('success', 'Kertas kerja telah tersimpan');
            }
        }
    }

    //Update Detail TAO E-audit
    public function update_detail_tao($tao,$input)
    {
        $config['upload_path'] = './img/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file_download')) {
            return array('error', $this->upload->display_errors());
        } else {
            $this->db->trans_begin();
            $file = $this->upload->data();
            $this->db->where('id', $input['id']);
            $this->db->where('id_tao', $tao);
            $sebelumnya = $this->db->get('support.tao_detail')->row_array();
            if (strlen($sebelumnya['file_download']) > 1) {
                if (file_exists($config['upload_path'] . $sebelumnya['file_download'])) {
                    unlink($config['upload_path'] . $sebelumnya['file_download']);
                }
            }
            $this->db->where('id', $input['id']);
            $this->db->where('id_tao', $tao);
            $this->db->update('support.tao_detail', array(
                    'langkah' => $input['langkah'],
                    'kertas_kerja' => $input['kertas'],
                    'file_download' => $file['file_name'],
                )
            );
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('error', 'Kertas kerja gagal diperbarui');
            } else {
                $this->db->trans_commit();
                return array('success', 'Kertas kerja telah diperbarui');
            }
        }
    }

    //Insert Pedoman E-audit
    public function insert_pedoman($input)
    {
        $this->db->trans_begin();
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $config['upload_path'] = './img/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
            $data['status_update'] = 'error';
            $data['status_message'] = $this->upload->display_errors('', '');
        } else {
            $file = $this->upload->data();
            $this->db->insert('support.pedoman', array(
                    'id' => $raw['id'],
                    'kode' => $input['kode'],
                    'jenis' => $input['jenis'],
                    'sasaran' => $input['sasaran'],
                    'file' => $file["file_name"],
                    'status' => 't'
                )
            );
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }

    }

    //Insert Periode E-audit
    public function insert_periode($input)
    {
        $this->db->trans_begin();

        $this->load->library('uuid');
        $id=$this->uuid->v4();
        $this->db->where('nama',$input['nama']);
        $num=$this->db->get('periode')->num_rows();
        if($num>0){
            $this->db->trans_rollback();
            return array('error', 'Periode '.$input['nama'].' Sudah ada');
        }else{
            $tgl_start=explode('/',$input['sd']);
            $tgl_m=$tgl_start[2].'-'.$tgl_start['1'].'-'.$tgl_start['0'];
            $tgl_end=explode('/',$input['ed']);
            $tgl_e=$tgl_end[2].'-'.$tgl_end['1'].'-'.$tgl_end['0'];
            $this->db->insert('periode', array(
                    'id' =>$id,
                    'nama' => $input['nama'],
                    'start' => $tgl_m,
                    'end' => $tgl_e,
                    'status' => 't'
                )
            );

            $start_date = $tgl_m;
            $end_date = $tgl_e;
            $weekdays = [6,7]; // 0 = sunday, 1 = monday ...

            $range_date = array();
            for ($i = strtotime($start_date); $i <= strtotime($end_date); $i = strtotime('+1 day', $i))
            {
                if(in_array(date('N', $i), $weekdays))
                {
                    $range_date[]=date('Y-m-d',$i);
                }
            }
            for($i=0;$i<count($range_date);$i++){
                $this->db->insert('periode_holiday', array(
                    'id' =>$id,
                    'holiday' =>$range_date[$i],
                    'ket' =>'Libur Sabtu - Minggu',
                ));
            }
        }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
    }
    //Edit Periode E-audit
    public function edit_periode($input)
    {
        $this->db->trans_begin();
            $this->db->where('id',$input['id']);
        $this->db->delete('periode_holiday');
        $this->db->where('id',$input['id']);
            $this->db->update('periode', array(
                    'nama' => $input['nama'],
                    'start' => $input['sd'],
                    'end' => $input['ed']
                )
            );

        $start_date = $this->input->get_post('sd');
        $end_date = $this->input->get_post('ed');
        $weekdays = [6,7]; // 0 = sunday, 1 = monday ...

        $range_date = array();
        for ($i = strtotime($start_date); $i <= strtotime($end_date); $i = strtotime('+1 day', $i))
        {
            if(in_array(date('N', $i), $weekdays))
            {
                $range_date[]=date('Y-m-d',$i);
            }
        }
        for($i=0;$i<count($range_date);$i++){
            $this->db->insert('periode_holiday', array(
                'id' =>$input['id'],
                'holiday' =>$range_date[$i],
                'ket' =>'Libur Sabtu - Minggu',
            ));
        }
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
    }

    //Upload Aturan E-audit
    public function upload_aturan($input)
    {
        $this->db->trans_begin();
        if ($_FILES['file']['size'] == 0) {
            $this->db->where('id', $input['id_file']);
            $this->db->update('support.aturan', array(
                    'kode' => $input['kode'],
                    'jenis' => $input['jenis'],
                    'sasaran' => $input['sasaran'],
                )
            );
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        } else if ($_FILES['file']['size'] <> 0) {
            $config['upload_path'] = './img/';
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $data['status_update'] = 'error';
                $data['status_message'] = $this->upload->display_errors('', '');
            } else {
                $file = $this->upload->data();

                $this->db->where('id', $input['id_file']);
                $sebelumnya = $this->db->get('support.aturan')->row_array();

                if (strlen($sebelumnya['file']) > 1) {
                    if (file_exists($config['upload_path'] . $sebelumnya['file'])) {
                        unlink($config['upload_path'] . $sebelumnya['file']);
                    }
                }
                $this->db->where('id', $input['id_file']);
                $this->db->update('support.aturan', array(
                        'kode' => $input['kode'],
                        'jenis' => $input['jenis'],
                        'sasaran' => $input['sasaran'],
                        'file' => $file["file_name"]
                    )
                );
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return false;
                } else {
                    $this->db->trans_commit();
                    return true;
                }
            }

        }
    }

    //Insert Aturan E-audit
    public function insert_aturan($input)
    {
        $this->db->trans_begin();
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $config['upload_path'] = './img/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
            $data['status_update'] = 'error';
            $data['status_message'] = $this->upload->display_errors('', '');
        } else {
            $file = $this->upload->data();
            $this->db->insert('support.aturan', array(
                    'id' => $raw['id'],
                    'kode' => $input['kode'],
                    'jenis' => $input['jenis'],
                    'sasaran' => $input['sasaran'],
                    'file' => $file["file_name"],
                    'status' => 't'
                )
            );
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }

    }

    //Upload Kertas Kerja E-audit
    public function upload_kk($input)
    {
        $this->db->trans_begin();
        if ($_FILES['file']['size'] == 0) {
            $this->db->where('id', $input['id_file']);
            $this->db->update('support.kertas_kerja', array(
                    'kode' => $input['kode'],
                    'jenis' => $input['jenis'],
                    'sasaran' => $input['sasaran'],
                )
            );
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        } else if ($_FILES['file']['size'] <> 0) {
            $config['upload_path'] = './img/';
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $data['status_update'] = 'error';
                $data['status_message'] = $this->upload->display_errors('', '');
            } else {
                $file = $this->upload->data();

                $this->db->where('id', $input['id_file']);
                $sebelumnya = $this->db->get('support.kertas_kerja')->row_array();

                if (strlen($sebelumnya['file']) > 1) {
                    if (file_exists($config['upload_path'] . $sebelumnya['file'])) {
                        unlink($config['upload_path'] . $sebelumnya['file']);
                    }
                }
                $this->db->where('id', $input['id_file']);
                $this->db->update('support.kertas_kerja', array(
                        'kode' => $input['kode'],
                        'jenis' => $input['jenis'],
                        'sasaran' => $input['sasaran'],
                        'file' => $file["file_name"]
                    )
                );
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return false;
                } else {
                    $this->db->trans_commit();
                    return true;
                }
            }

        }
    }

    //Insert Kertas Kerja E-audit
    public function insert_kk($input)
    {
        $this->db->trans_begin();
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $config['upload_path'] = './img/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
            $data['status_update'] = 'error';
            $data['status_message'] = $this->upload->display_errors('', '');
        } else {
            $file = $this->upload->data();
            $this->db->insert('support.kertas_kerja', array(
                    'id' => $raw['id'],
                    'kode' => $input['kode'],
                    'jenis' => $input['jenis'],
                    'sasaran' => $input['sasaran'],
                    'file' => $file["file_name"],
                    'status' => 't'
                )
            );
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }

    }

    public function get_karyawan($id)
    {
        $this->db->where('id', $id);
        return $this->db->get("main.mstkaryawan")->row_array();
    }

    public function set_rolemenu_status($menu, $access, $group)
    {
        $this->db->where("role", $group);
        $this->db->where("menu", $menu);
        $data = $this->db->get("rolemenu")->row_array();
        if (isset($data['menu'])) {
            $data = $this->db->query("UPDATE \"rolemenu\" set $access= case $access when true then false else true end WHERE menu = ? AND role = ? RETURNING $access", array($menu, $group))->row_array();
        }
        else {
            $this->db->insert("rolemenu", array(
                'menu' => $menu,
                'role' => $group,
                $access => true
            ));
            $data[$access] = 't';
        }
        getLabelAccessRights($data[$access]);
    }

    public function set_group_status($id)
    {
        $data = $this->db->query("UPDATE \"role\" set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_periode_status($id)
    {
        $data = $this->db->query("UPDATE \"periode\" set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_tipe_resiko_status($id)
    {
        $data = $this->db->query("UPDATE master.tipe_resiko set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_kendali_status($id)
    {
        $data = $this->db->query("UPDATE master.kendali set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_penyebab_status($id)
    {
        $data = $this->db->query("UPDATE master.penyebab set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_dampak_status($id)
    {
        $data = $this->db->query("UPDATE master.dampak set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_akibat_status($id)
    {
        $data = $this->db->query("UPDATE master.akibat set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_m_program_status($id)
    {
        $data = $this->db->query("UPDATE master.program set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_kegiatan_status($id)
    {
        $data = $this->db->query("UPDATE master.kegiatan set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_user_status($id)
    {
        $data = $this->db->query("UPDATE \"users\" set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // Kertas Kerja e-audit
    public function set_kk_status($id)
    {
        $data = $this->db->query("UPDATE support.kertas_kerja set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // Pedoman e-audit
    public function set_pedoman_status($id)
    {
        $data = $this->db->query("UPDATE support.pedoman set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // Detail TAO e-audit
    public function set_detail_tao_status($id)
    {
        $data = $this->db->query("UPDATE support.tao_detail set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // TAO e-audit
    public function set_tao_status($id)
    {
        $data = $this->db->query("UPDATE support.tao set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // Aturan e-audit
    public function set_aturan_status($id)
    {
        $data = $this->db->query("UPDATE support.aturan set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // tim e-audit
    public function set_tim_status($id)
    {
        $data = $this->db->query("UPDATE support.tim set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // Wilayah e-audit
    public function set_wilayah_status($id)
    {
        $data = $this->db->query("UPDATE support.wilayah set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // PKPT e-audit
    public function set_pkpt_status($id)
    {
        $data = $this->db->query("UPDATE master.pkpt set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_program_status($id)
    {
        $data = $this->db->query("UPDATE support.program_satker set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_opsi_status($id)
    {
        $data = $this->db->query("UPDATE master.opsi set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_kegiatan_satker_status($id)
    {
        $data = $this->db->query("UPDATE support.kegiatan_satker set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_kejadian_satker_status($id)
    {
        $data = $this->db->query("UPDATE support.kejadian_satker set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // Jabatan e-audit
    public function set_jabatan_status($id)
    {
        $data = $this->db->query("UPDATE master.jabatan set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    } public function set_prosedur_status($id)
    {
        $data = $this->db->query("UPDATE master.prosedur_audit set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    } // Jabatan e-audit
    public function set_conf_spt_status($id)
    {
        $data = $this->db->query("UPDATE master.conf_spt set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // Jenis e-meeting
    public function set_jenis_status($id)
    {
        $data = $this->db->query("UPDATE master.jenis set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    // narasumber e-meeting
    public function set_narasumber_status($id)
    {
        $data = $this->db->query("UPDATE master.narasumber set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    // tema e-meeting
    public function set_tema_status($id)
    {
        $data = $this->db->query("UPDATE master.tema set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    // tamu e-meeting
    public function set_tamu_status($id)
    {
        $data = $this->db->query("UPDATE master.tamu set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    // tindak_lanjut e-meeting
    public function set_tindak_lanjut_status($id)
    {
        $data = $this->db->query("UPDATE master.tindak_lanjut set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // Satker e-audit
    public function set_satker_status($id)
    {
        $data = $this->db->query("UPDATE master.satker set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_tk_status($id)
    {
        $data = $this->db->query("UPDATE master.tindak set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }
    public function set_target_status($id)
    {
        $data = $this->db->query("UPDATE master.target set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // Sasaran e-audit
    public function set_sasaran_status($id)
    {
        $data = $this->db->query("UPDATE master.sasaran set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // Tujuan e-audit
    public function set_tujuan_status($id)
    {
        $data = $this->db->query("UPDATE master.tujuan set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // Golongan e-audit
    public function set_golongan_status($id)
    {
        $data = $this->db->query("UPDATE master.golongan set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    // Peran e-audit
    public function set_peran_status($id)
    {
        $data = $this->db->query("UPDATE master.peran set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    public function set_karyawan_status($id)
    {
        $data = $this->db->query("UPDATE \"main\".\"mstkaryawan\" set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    public function set_supplier_status($id)
    {
        $data = $this->db->query("UPDATE \"main\".\"mstsupplier\" set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    public function set_unit_status($id)
    {
        $data = $this->db->query("UPDATE \"main\".\"mstunit\" set status= NOT status WHERE id = ? RETURNING status", array($id))->row_array();
        getLabelStatus($data['status']);
    }

    public function get_access_rights($id)
    {
        $this->db->order_by('menu.level,menu.name', 'asc');
        $this->db->join('rolemenu', "rolemenu.menu=menu.id AND rolemenu.role='$id'", 'left');
        $data = $this->db->get('menu')->result_array();
        $result = array();
        foreach ($data as $d) {
            $result[isset($d['parent']) ? $d['parent'] : 0][] = $d;
        }
        return $result;
    }

    //TAO E-audit
    public function update_tao($id, $input)
    {
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'jenis' => $input['jenis'],
            'tujuan' => $input['tujuan'],
            'langkah' => $input['langkah'],
            'sasaran' => $input['sasaran'],
            'tahapan' => $input['tahapan'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('support.tao', $users);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

//TIM E-audit
    public function update_pkpt($input, $id)
    {
        if($input['no']<10){
            $input['no']='00'.$input['no'];
        }
        if($input['no']>=10 && $input['no']<100 ){
            $input['no']='0'.$input['no'];
        }
        if($input['no']>=100 && $input['no']<1000 ){
            $input['no']=$input['no'];
        }
        $users = array(
            'id' => $id,
            'no' => $input['thn'].$input['no'],
            'tema' => $input['tema'],
            'tim' => json_encode($input['tim']),
            'nama' => $input['nama'],
            'jenis' => $input['jenis'],
            'sasaran' => $input['sasaran'],
            'tujuan' => $input['tujuan'],
            'kegiatan' => $input['kegiatan'],
            'hp' => $input['hp'],
            'rmp' => $input['rmp'],
            'rpl' => $input['rpl'],
            'dana' => $input['dana'],
            'risiko' => $input['risiko'],
            'keterangan' => $input['keterangan'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('master.pkpt', $users);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }//TIM E-audit
    public function update_tim($input, $id)
    {
        $raw = $this->db->query("SELECT NOW() as tstamp")->row_array();
        $users = array(
            'wilayah' => $input['wilayah'],
            'dalnis' => $input['dalnis'],
            'ketua' => $input['ketua'],
            'tim' => $input['tim'],
        );

        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('support.tim', $users);
        $this->db->where('tim', $id);
        $this->db->delete('support.anggota');
        if (count(@$input['anggota']) > 0) {
            foreach ($input['anggota'] as $r) {
                $this->db->insert('support.anggota', array('tim' => $id, 'anggota' => $r));
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }


    public function update_user($input, $id)
    {
        $raw = $this->db->query("SELECT NOW() as tstamp")->row_array();
        foreach($input as $i=>$v){
            if($input[$i]==''){
                $input[$i]=null;
            }
            else{
                $input[$i]=$input[$i];
            }
        }
        $users = array(
            'name' => $input['name'],
            'nip' => $input['nip'],
            'alamat' => $input['alamat'],
            'telp' => $input['telp'],
            'jabatan' => $input['jabatan'],
            'golongan' => $input['golongan'],
            'email' => $input['email'],
            'modified_at' => $raw['tstamp'],
        );

        if (strlen(@$input['password']) > 0 && @$input['password'] == @$input['konfirm']) {
            $users['password'] = md5($input['password']);
        }
        if (strlen(@$input['username']) > 0) {
            $users['username'] = $input['username'];
        }
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('users', $users);
        $this->db->where('user', $id);
        $this->db->delete('roleusers');
        if (count(@$input['role']) > 0) {
            foreach ($input['role'] as $r) {
                $this->db->insert('roleusers', array('user' => $id, 'role' => $r));
            }
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function update_profile($input, $id)
    {
        $raw = $this->db->query("SELECT NOW() as tstamp")->row_array();
        $users = array(
            'email' => $input['email'],
            'modified_at' => $raw['tstamp'],
        );

        if (strlen(@$input['password']) > 0 && @$input['password'] == @$input['konfirm']) {
            $users['password'] = md5($input['password']);
        }
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('users', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    } public function update_setting($input, $id)
    {
        $this->db->where('id',$id);
        $cek=$this->db->get('users')->num_rows();

        $this->db->trans_begin();
        $raw = $this->db->query("SELECT NOW() as tstamp")->row_array();
        $users = array(
            'name' => $input['nama'],
            'alamat' => $input['alamat'],
            'telp' => $input['telp'],
        );
            $this->db->where('id', $id);
            $this->db->update('users', $users);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function update_coa($data){
        $this->db->where('id',$data['id']);
        $coa=$this->db->get('support.kode_temuan')->row_array();
        $user = $this->session->userdata('user');

        $this->db->where('k'.$coa['level'],$data['kode']);
        $this->db->where('id <> ',$coa['id']);
        $this->db->where('level',$coa['level']);
        if($data['level']>1) {
            $this->db->where('k1', $coa['k1']);
            if ($data['level'] > 2) {
                $this->db->where('k2', $coa['k2']);
                if ($data['level'] > 3) {
                    $this->db->where('k3', $coa['k3']);
                }
            }
        }
        $exist=$this->db->get('support.kode_temuan')->num_rows();
        if($exist>0){
            return false;
        }else{
            $update=array(
                'k'.$coa['level']=>$data['kode'],
                'nama'=>$data['nama'],
                'modified_by'=>$user['id'],
                'modified_at'=>date('Y-m-d H:i:s+07'),
            );
            $this->db->trans_begin();
            $this->db->where('id',$data['id']);
            $this->db->update('support.kode_temuan',$update);
            if($data['level']<4){
                unset($update['nama']);
                $this->db->where('k1',$coa['k1']);
                if($data['level']>1){
                    $this->db->where('k2',$coa['k2']);
                    if($data['level']>2){
                        $this->db->where('k3',$coa['k3']);
                    }
                }
                $this->db->update('support.kode_temuan',$update);
            }
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }
    public function set_coa_delete($id){
        $this->db->where('id',$id);
        $coa=$this->db->get('support.kode_temuan')->row_array();
        $user = $this->session->userdata('user');
        if($coa['level']<>4){
            $this->db->where('deleted',false);
            $this->db->where('k1',$coa['k1']);
            if($coa['level']>1){
                $this->db->where('k2',$coa['k2']);
                if($coa['level']>2){
                    $this->db->where('k3',$coa['k3']);
                }
            }
            $child=$this->db->get('support.kode_temuan')->num_rows();
            $status=$child>1?false:true;
        }else{
            $status=true;
        }
        if($status){
            $this->db->where('deleted',false);
            $this->db->where('id',$id);
            $this->db->update('support.kode_temuan',array(
                'modified_by'=>$user['id'],
                'modified_at'=>date('Y-m-d H:i:s+07'),
                'deleted'=>true,
            ));
            $data['status']=200;
            $data['message']="Delete [".format_coa("$coa[k1].$coa[k2].$coa[k3].$coa[k4]")."] $coa[nama] success :)";
        }else{
            $data['status']=403;
            $data['message']="Delete [".format_coa("$coa[k1].$coa[k2].$coa[k3].$coa[k4]")."] $coa[nama] failed :(";
        }
        return json_encode($data);
    }
    public function update_customer($input, $id = null)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $users = array(
            'kode' => $input['kode'],
            'nama' => $input['nama'],
            'alamat' => $input['alamat'],
            'npwp' => $input['npwp'],
            'email' => $input['email'],
            'cpnama' => $input['cpnama'],
            'cpkontak' => $input['cpkontak'],
            'telp' => $input['cpkontak'],
            'note' => $input['note'],
        );
        if (isset($id)) {
            $users['modified_at'] = $raw['tstamp'];
            $this->db->where('id', $id);
            return $this->db->update('main.mstcustomer', $users);
        } else {
            $users['id'] = $raw['id'];
            $users['status'] = true;
            $users['created_at'] = $raw['tstamp'];
            return $this->db->insert('main.mstcustomer', $users);
        }
    }

    public function update_karyawan($input, $id = null)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $users = array(
            'nik' => $input['nik'],
            'nama' => $input['nama'],
            'alamat' => $input['alamat'],
            'npwp' => $input['npwp'],
            'telp' => $input['kontak'],
            'tempatlahir' => $input['tempatlahir'],
        );
        if (isset($id)) {
            $users['modified_at'] = $raw['tstamp'];
            $this->db->where('id', $id);
            return $this->db->update('main.mstkaryawan', $users);
        } else {
            $users['id'] = $raw['id'];
            $users['status'] = true;
            $users['created_at'] = $raw['tstamp'];
            return $this->db->insert('main.mstkaryawan', $users);
        }
    }

    public function update_supplier($input, $id = null)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $users = array(
            'kode' => $input['kode'],
            'nama' => $input['nama'],
            'alamat' => $input['alamat'],
            'npwp' => $input['npwp'],
            'email' => $input['email'],
            'cpnama' => $input['cpnama'],
            'cpkontak' => $input['cpkontak'],
            'telp' => $input['cpkontak'],
            'note' => $input['note'],
        );
        if (isset($id)) {
            $users['modified_at'] = $raw['tstamp'];
            $this->db->where('id', $id);
            return $this->db->update('main.mstsupplier', $users);
        } else {
            $users['id'] = $raw['id'];
            $users['status'] = true;
            $users['created_at'] = $raw['tstamp'];
            return $this->db->insert('main.mstsupplier', $users);
        }
    }

    public function insert($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        foreach($input as $i=>$v){
            if($input[$i]==''){
                $input[$i]=null;
            }
            else{
                $input[$i]=$input[$i];
            }
        }
        $users = array(
            'id' => $id,
            'name' => $input['name'],
            'username' => $input['username'],
            'email' => $input['email'],
            'created_at' => $raw['tstamp'],
            'password' => md5($input['password']),
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('users', $users);
        if (count(@$input['role']) > 0) {
            foreach ($input['role'] as $r) {
                $this->db->insert('roleusers', array('user' => $id, 'role' => $r));
            }
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    //Wilayah e-audit

    public function insert_wilayah($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('support.wilayah')->num_rows();
        $kode = $order + 1;
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $kode,
            'wilayah' => $input['wilayah'],
            'irban' => $input['irban'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('support.wilayah', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    //TAO e-audit

    public function insert_tao($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('support.tao')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'jenis' => $input['jenis'],
            'tujuan' => $input['tujuan'],
            'langkah' => $input['langkah'],
            'sasaran' => $input['sasaran'],
            'tahapan' => $input['tahapan'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('support.tao', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    //Tim e-audit

    public function insert_tim($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('support.tim')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'wilayah' => $input['wilayah'],
            'tim' => $input['tim'],
            'dalnis' => $input['dalnis'],
            'ketua' => $input['ketua'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('support.tim', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

//PKPT e-audit

    public function insert_pkpt($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('master.pkpt')->num_rows();
        $id = $raw['id'];
        if($input['no']<10){
            $input['no']='00'.$input['no'];
        }
        if($input['no']>=10 && $input['no']<100 ){
            $input['no']='0'.$input['no'];
        }
        if($input['no']>=100 && $input['no']<1000 ){
            $input['no']=$input['no'];
        }
        $users = array(
            'id' => $id,
            'no' => $input['thn'].$input['no'],
            'tema' => $input['tema'],
            'tim' => json_encode($input['tim']),
            'nama' => $input['nama'],
            'kegiatan' => $input['kegiatan'],
            'jenis' => $input['jenis'],
            'sasaran' => $input['sasaran'],
            'tujuan' => $input['tujuan'],
            'hp' => $input['hp'],
            'rmp' => $input['rmp'],
            'rpl' => $input['rpl'],
            'dana' => $input['dana'],
            'risiko' => $input['risiko'],
            'keterangan' => $input['keterangan'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.pkpt', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_program($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'nama' => $input['nama'],
            'periode' => $input['periode'],
            'status' => TRUE,
            'jumlah' => $input['jml_program'],
        );
        $this->db->trans_begin();
        $this->db->insert('support.program_satker', $users);
        $urutan=1;
        for($i=0;$i<$input['jml_program'];$i++){
            for($j=0;$j<count($input["kendali_s$urutan"]);$j++){
                $this->db->insert('support.program_kr',array(
                    'id' => $id,
                    'resiko' => $input["resiko$urutan"],
                    'kendali_s' => $input["kendali_s$urutan"][$j],
                    'kendali_a' => $input["kendali_a$urutan"][$j],
                    'urutan' => $urutan,
                ));
            }
            $urutan++;
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_kegiatan_satker($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];

        $this->db->trans_begin();

        $this->db->where('id', $input['nama']);
        $ett=$this->db->get('support.setting_kegiatan_detail')->result_array();
        //ambil setting, masukin kegiatan_kr, kendali blm nya dicari dari kendali yang sudah ada - harus ada
        $no=1;
        $totjks=0;
        $totjka=0;
        $totjkb=0;
        foreach($ett as $e){
            $kendali_s=json_decode($e['kendali_s'], true);
            $array=array();
            if($e["kendali_s"]!=''){
                $totjks=$totjks+count($kendali_s);
                for($i=0;$i<count($kendali_s);$i++){
                    if(!in_array($kendali_s[$i],$input["kendali_a$no"],true)){
                        $array[]=$kendali_s[$i];
                        $totjkb++;
                    }
                }
            }
            $totjka=$totjka+count($input["kendali_a$no"]);
            $this->db->insert('support.kegiatan_kr',array(
                'id' => $id,
                'resiko' => $e['resiko'],
                'urutan' => $e['urutan'],
                'kendali_s' => $e["kendali_s"],
                'kendali_a' => json_encode($input["kendali_a$no"]),
                'kendali_ba' => json_encode($array),
                'p_resiko' => $e["p_resiko"],
                'da_resiko' => $e["da_resiko"],
            ));
            $no++;
            unset($array);
        }
        $users = array(
            'id' => $id,
            'kegiatan' => $input['nama'],
            'program_satker' => $input['program'],
            'satker' => $input['satker'],
            'periode' => $input['periode'],
            'status' => TRUE,
            'jumlah' => $input['jml_program'],
            'jka' => $totjka,
            'jks' => $totjks,
            'jkb' => $totjkb,
        );
        $this->db->insert('support.kegiatan_satker', $users);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function insert_kejadian_satker($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'nama' => $input['nama'],
//            'pk' => $input['program']==''?$input['kegiatan']:$input['program'],
            'resiko' => $input['resiko'],
//            'penyebab' => $input['penyebab'],
            'jumlah' => $input['jumlah'],
//            'kendali_s' => $input['kendali_s'],
//            'kendali_a' => $input['kendali_a'],
//            'prosedur_audit' => $input['prosedur'],
//            'program_kejadian' => $input['program']==''?'K':'P',
            'hitung' => $input['hitung'],
            'periode' => $input['periode'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('support.kejadian_satker', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function update_program($input,$id)
    {
         $users = array(
             'nama' => $input['nama'],
             'periode' => $input['periode'],
             'status' => TRUE,
             'jumlah' => $input['jml_program'],
        );
        $this->db->trans_begin();
        $this->db->where('id', $id);

        $this->db->update('support.program_satker', $users);
        $this->db->where('id', $id);
        $this->db->delete('support.kegiatan_kr');

        $urutan=1;
        for($i=0;$i<$input['jml_program'];$i++){
            for($j=0;$j<count($input["kendali_s$urutan"]);$j++){
                $this->db->insert('support.program_kr',array(
                    'id' => $id,
                    'resiko' => $input["resiko$urutan"],
                    'kendali_s' => $input["kendali_s$urutan"][$j],
                    'kendali_a' => $input["kendali_a$urutan"][$j],
                    'urutan' => $urutan,
                ));
            }
            $urutan++;
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function update_kejadian_satker($input,$id)
    {
         $users = array(
             'nama' => $input['nama'],
//             'pk' => $input['program']==''?$input['kegiatan']:$input['program'],
             'resiko' => $input['resiko'],
//             'penyebab' => $input['penyebab'],
             'jumlah' => $input['jumlah'],
//             'kendali_s' => $input['kendali_s'],
//             'kendali_a' => $input['kendali_a'],
//             'prosedur_audit' => $input['prosedur'],
//             'program_kejadian' => $input['program']==''?'K':'P',
             'hitung' => $input['hitung'],
             'periode' => $input['periode'],
        );
        $this->db->trans_begin();
        $this->db->where('id', $id);

        $this->db->update('support.kejadian_satker', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function update_kegiatan_satker($input,$id)
    {

        $users = array(
            'nama' => $input['nama'],
            'program_satker' => $input['program'],
            'prosedur_audit' => $input['prosedur'],
            'periode' => $input['periode'],
            'status' => TRUE,
            'jumlah' => $input['jml_program'],
        );
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('support.kegiatan_satker', $users);

        $this->db->where('id', $id);
        $this->db->delete('support.kegiatan_kr');

        $urutan=1;
        for($i=0;$i<$input['jml_program'];$i++){
            for($j=0;$j<count($input["kendali_s$urutan"]);$j++){
                $this->db->insert('support.kegiatan_kr',array(
                    'id' => $id,
                    'resiko' => $input["resiko$urutan"],
                    'p_resiko' => $input["p_resiko$urutan"],
                    'da_resiko' => $input["da_resiko$urutan"],
                    'kendali_s' => $input["kendali_s$urutan"][$j],
                    'kendali_a' => $input["kendali_a$urutan"][$j],
                    'kendali_ba' => $input["kendali_ba$urutan"][$j],
                    'urutan' => $urutan,
                ));
            }
            $urutan++;
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function update_anal_kegiatan_satker($input)
    {
        $urutan=1;
            for($j=0;$j<$input["jml_program"];$j++){
                $this->db->where('id', $input["idrs$urutan"]);
                $this->db->where('resiko', $input["rs$urutan"]);
                $this->db->update('support.kegiatan_kr',array(
                    'sk' => $input["sk$urutan"],
                    'sd' => $input["sd$urutan"],
                ));
                $urutan++;
            }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function update_monitoring_kegiatan_satker($input)
    {
        $urutan=1;

        $this->db->where('id', $input["idrs1"]);
        $this->db->update('support.kegiatan_satker',array(
            'prosedur_audit' => $input["prosedur"],
        ));
            for($j=0;$j<$input["jml_program"];$j++){
                $this->db->where('id', $input["idrs$urutan"]);
                $this->db->where('resiko', $input["rs$urutan"]);
                $this->db->update('support.kegiatan_kr',array(
                    'jml' => $input["jml$urutan"],
                    'real' => $input["real$urutan"],
                ));
                $urutan++;
            }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function update_opsi_kegiatan_satker($input)
    {
        $urutan=1;
            for($j=0;$j<$input["jml_program"];$j++){
                $this->db->where('id', $input["idrs$urutan"]);
                $this->db->where('resiko', $input["rs$urutan"]);
                $this->db->update('support.kegiatan_kr',array(
                    'opsi' => $input["opsi$urutan"],
                    'tindak' => $input["tindak$urutan"],
                    'target' => $input["target$urutan"],
                    'target_t' => format_waktu($input["end$urutan"], true),
                ));
                $urutan++;
            }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }


    public function update_eval_kegiatan_satker($input)
    {
        $urutan=1;
        for($j=0;$j<$input["jml_program"];$j++){
            $this->db->where('id', $input["id$urutan"]);
            $this->db->where('resiko', $input["res$urutan"]);
            $this->db->update('support.kegiatan_kr',array(
                'nilai' => $input["nilai$urutan"],
                'penanggung' => $input["pj$urutan"],
            ));
            $urutan++;
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_jabatan($input){
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('master.jabatan')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'ket' => $input['ket'],
            'order' => $order + 1,
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.jabatan', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_prosedur($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'nama' => $input['nama'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.prosedur_audit', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_tipe_resiko($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
//            'kode' => $input['kode'],
            'nama' => $input['nama'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.tipe_resiko', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_kendali($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'nama' => $input['nama'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.kendali', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_penyebab($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'nama' => $input['nama'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.penyebab', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_dampak($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'nama' => $input['nama'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.dampak', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_akibat($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'nama' => $input['nama'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.akibat', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_kegiatan($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'nama' => $input['nama'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.kegiatan', $users);

        $users = array(
            'id' => $id,
            'nama' => $input['nama'],
            'status' => TRUE,
            'jumlah' => $input['jml_program'],
        );
        $this->db->insert('support.setting_kegiatan', $users);
        $urutan=1;
        for($i=0;$i<$input['jml_program'];$i++){
                $this->db->insert('support.setting_kegiatan_detail',array(
                    'id' => $id,
                    'resiko' => $input["resiko$urutan"],
                    'p_resiko' => $input["p_resiko$urutan"],
                    'da_resiko' => $input["da_resiko$urutan"],
                    'kendali_s' => json_encode($input["kendali_s$urutan"]),
                    'urutan' => $urutan,
                ));
            $urutan++;
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function update_kegiatan($input)
    {
        $this->db->trans_begin();

        $users = array(
            'nama' => $input['nama'],
            'kode' => $input['kode'],
        );
        $this->db->where('id', $input['ids']);
        $this->db->update('support.setting_kegiatan', array(
            'jumlah'=>$input['jml_program']
        ));
        $this->db->where('id', $input['ids']);
        $this->db->update('master.kegiatan',$users);
        $this->db->where('id', $input['ids']);
        $this->db->delete('support.setting_kegiatan_detail');
        $urutan=1;
        for($i=0;$i<$input['jml_program'];$i++){
                $this->db->insert('support.setting_kegiatan_detail',array(
                    'id' => $input['ids'],
                    'resiko' => $input["resiko$urutan"],
                    'p_resiko' => $input["p_resiko$urutan"],
                    'da_resiko' => $input["da_resiko$urutan"],
                    'kendali_s' => json_encode($input["kendali_s$urutan"]),
                    'urutan' => $urutan,
                ));
            $urutan++;
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_m_program($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'nama' => $input['nama'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.program', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_opsi($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'nama' => $input['nama'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.opsi', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }public function insert_target($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'nama' => $input['nama'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.target', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }public function insert_tk($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'nama' => $input['nama'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.tindak', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_mtindak_lanjut($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'nama' => $input['nama'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.tindak_lanjut', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function insert_conf($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('master.conf_spt')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'ket' => $input['ket'],
            'order' => $order + 1,
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.conf_spt', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    //Jenis e-audit
    public function insert_jenis($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('master.jenis')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'ket' => $input['ket'],
            'order' => $order + 1,
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.jenis', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    //narasumber e-audit
    public function insert_narasumber($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('master.narasumber')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'ket' => $input['ket'],
            'order' => $order + 1,
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.narasumber', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    //tema e-audit
    public function insert_tema($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('master.tema')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'ket' => $input['ket'],
            'order' => $order + 1,
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.tema', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    //tindak_lanjut e-audit
    public function insert_tindak_lanjut($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('master.tindak_lanjut')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'ket' => $input['nama'],
            'order' => $order + 1,
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.tindak_lanjut', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    //narasumber e-audit
    public function insert_tamu($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('master.tamu')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'ket' => $input['ket'],
            'order' => $order + 1,
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.tamu', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    //Satker e-audit
    public function insert_satker($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('master.satker')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'alamat' => $input['alamat'],
            'ket' => $input['nama'],
            'nama' => $input['nama'],
            'email' => $input['email'],
            'order' => $order + 1,
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.satker', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    //Satker resiko e-audit
    public function insert_resiko_satker($input)
    {
        $ket='';
        $resiko='';
        $tot=$input['tot_lp']+$input['tot_km']+$input['tot_im']+$input['tot_ko']+$input['tot_si'];
//        echo $input['tot_lp'];
//        echo $input['tot_km'];
//        echo $input['tot_im'];
//        echo $input['tot_si'];
//        die();
        $this->db->trans_begin();
                $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
                $this->db->where('id_satker', $input['satker']);
                $this->db->where('periode', $input['periode_h']);
                $a=$this->db->get('master.resiko_satker')->row_array();
                if($a['id']==''){
                    $this->db->insert('master.resiko_satker',array(
                        'id'=>$raw['id'],
                        'id_satker'=>$input['satker'],
                        'periode'=>$input['periode_h'],
                        'ket_resiko'=>'',
                        'resiko'=>$tot,
                        'jumlah_anggaran'=>$input['ja'],
                        'nilai_aset'=>$input['na'],
                        'jumlah_pegawai'=>$input['jp'],
                        'jumlah_unit'=>$input['ju'],
                        'jumlah_jenis_keg'=>$input['jk'],
                        'jumlah_pemeriksaan'=>$input['jp5'],
                        'nilai_sakip'=>$input['ns'],
                        'nilai_spip'=>$input['nsp'],
                        'tertib_adm'=>$input['ta'],
                        'jumlah_pengaduan'=>$input['jpm'],
                        'jumlah_jabatan'=>$input['jj'],
                        'hasil_monitoring'=>$input['hm'],
                        'lp'=>$input['lp'],
                        'lp2'=>$input['lp2'],
                        'lp3'=>$input['lp3'],
                        'lp4'=>$input['lp4'],
                        'lp5'=>$input['lp5'],
                        'tot_lp'=>$input['tot_lp'],
                        'km'=>$input['km'],
                        'km2'=>$input['km2'],
                        'km3'=>$input['km3'],
                        'km4'=>$input['km4'],
                        'km5'=>$input['km5'],
                        'tot_km'=>$input['tot_km'],
                        'im'=>$input['im'],
                        'im2'=>$input['im2'],
                        'im3'=>$input['im3'],
                        'im4'=>$input['im4'],
                        'im5'=>$input['im5'],
                        'tot_im'=>$input['tot_im'],
                        'ko'=>$input['ko'],
                        'ko2'=>$input['ko2'],
                        'ko3'=>$input['ko3'],
                        'ko4'=>$input['ko4'],
                        'ko5'=>$input['ko5'],
                        'tot_ko'=>$input['tot_ko'],
                        'si'=>$input['si'],
                        'si2'=>$input['si2'],
                        'si3'=>$input['si3'],
                        'si4'=>$input['si4'],
                        'si5'=>$input['si5'],
                        'tot_si'=>$input['tot_si'],
                    ));
                }
                else{
                    $this->db->where('id_satker', $input['satker']);
                    $this->db->where('periode', $input['periode_h']);
                    $this->db->update('master.resiko_satker',array(
                        'id_satker'=>$input['satker'],
                        'periode'=>$input['periode_h'],
                        'ket_resiko'=>'',
                        'resiko'=>$tot,
                        'jumlah_anggaran'=>$input['ja'],
                        'nilai_aset'=>$input['na'],
                        'jumlah_pegawai'=>$input['jp'],
                        'jumlah_unit'=>$input['ju'],
                        'jumlah_jenis_keg'=>$input['jk'],
                        'jumlah_pemeriksaan'=>$input['jp5'],
                        'nilai_sakip'=>$input['ns'],
                        'nilai_spip'=>$input['nsp'],
                        'tertib_adm'=>$input['ta'],
                        'jumlah_pengaduan'=>$input['jpm'],
                        'jumlah_jabatan'=>$input['jj'],
                        'hasil_monitoring'=>$input['hm'],
                        'lp'=>$input['lp'],
                        'lp2'=>$input['lp2'],
                        'lp3'=>$input['lp3'],
                        'lp4'=>$input['lp4'],
                        'lp5'=>$input['lp5'],
                        'tot_lp'=>$input['tot_lp'],
                        'km'=>$input['km'],
                        'km2'=>$input['km2'],
                        'km3'=>$input['km3'],
                        'km4'=>$input['km4'],
                        'km5'=>$input['km5'],
                        'tot_km'=>$input['tot_km'],
                        'im'=>$input['im'],
                        'im2'=>$input['im2'],
                        'im3'=>$input['im3'],
                        'im4'=>$input['im4'],
                        'im5'=>$input['im5'],
                        'tot_im'=>$input['tot_im'],
                        'ko'=>$input['ko'],
                        'ko2'=>$input['ko2'],
                        'ko3'=>$input['ko3'],
                        'ko4'=>$input['ko4'],
                        'ko5'=>$input['ko5'],
                        'tot_ko'=>$input['tot_ko'],
                        'si'=>$input['si'],
                        'si2'=>$input['si2'],
                        'si3'=>$input['si3'],
                        'si4'=>$input['si4'],
                        'si5'=>$input['si5'],
                        'tot_si'=>$input['tot_si'],
                    ));
                }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    //Sasaran e-audit
    public function insert_sasaran($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('master.sasaran')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'ket' => $input['ket'],
            'order' => $order + 1,
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.sasaran', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    //Tujuan e-audit
    public function insert_tujuan($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('master.tujuan')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'ket' => $input['ket'],
            'order' => $order + 1,
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.tujuan', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    //Golongan e-audit
    public function insert_golongan($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('master.golongan')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'ket' => $input['ket'],
            'order' => $order + 1,
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.golongan', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    //Peran e-audit
    public function insert_peran($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $order = $this->db->get('master.peran')->num_rows();
        $id = $raw['id'];
        $users = array(
            'id' => $id,
            'ket' => $input['ket'],
            'order' => $order + 1,
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('master.peran', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }


    public function insert_supplier($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $user = $this->session->userdata('user');
        $users = array(
            'id' => $id,
            'kode' => $input['kode'],
            'nama' => $input['nama'],
            'alamat' => $input['alamat'],
            'npwp' => $input['npwp'],
            'email' => $input['email'],
            'cpnama' => $input['cpnama'],
            'cpkontak' => $input['cpkontak'],
            'telp' => $input['cpkontak'],
            'note' => $input['note'],
            'created_at' => $raw['tstamp'],
            'created_by' => $user['id'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('main.mstsupplier', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }


    public function insert_karyawan($input)
    {
        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
        $id = $raw['id'];
        $user = $this->session->userdata('user');
        $users = array(
            'id' => $id,
            'nama' => $input['nama'],
            'nik' => $input['nik'],
            'alamat' => $input['alamat'],
            'telp' => $input['kontak'],
            'npwp' => $input['npwp'],
            'tgllahir' => $input['tgllahir'],
            'tempatlahir' => $input['tempatlahir'],
            'tglmasuk' => $input['tglmasuk'],
            'created_at' => $raw['tstamp'],
            'created_by' => $user['id'],
            'status' => TRUE,
        );
        $this->db->trans_begin();
        $this->db->insert('main.mstkaryawan', $users);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}