<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-6 col-md-offset-3">
                <form role="form" method="post">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Periode<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select class="select2 form-control select2-multiple" name="periode" data-placeholder="Pilih Periode" required>
                                <option value="">Pilih Periode</option>
                                <?php
                                foreach ($periode as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['periode']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Nama Kejadian<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="nama" required class="form-control" id="nama"
                                   placeholder="Nama Kejadian" value="<?= @$user['nama'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Resiko<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select class="select2 form-control select2-multiple" name="resiko" data-placeholder="Pilih Tipe Resiko" required>
                                <option value="">Pilih Tipe Resiko</option>
                                <?php
                                foreach ($tipe_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Jumlah Resiko Program<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="number" step="any" name="jumlah" required class="form-control" id="jumlah"
                                   placeholder="Jumlah Resiko Kejadian" value="<?= @$user['jumlah'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Termasuk Dalam Penghitungan Program Satker<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <div class="radio radio-success radio-inline">
                                <input type="radio" id="inlineRadioIrban1" required value="f" name="hitung" <?= @$user['hitung']=='f'?'checked':'' ?>>
                                <label for="inlineRadioIrban1"> Tidak Dihitung </label>
                            </div>
                            <div class="radio radio-success radio-inline">
                                <input type="radio" id="inlineRadioIrban2" required value="t" name="hitung" <?= @$user['hitung']=='t'?'checked':'' ?>>
                                <label for="inlineRadioIrban2"> Dihitung </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    $(function () {
        <?php
        if(@$user['program_kejadian']=='P'){
            ?>
            $('#programs').show();
        $('.kegiatan').hide();
        <?php
        }else if(@$user['program_kejadian']=='K'){
            ?>
            $('.kegiatan').show();
        $('#programs').hide();
        <?php
        }else{
            ?>
        $('#programs').hide();
        $('.kegiatan').hide();
        <?php
        }?>

        $('.select2').select2({});
        $('form').parsley();
        $("form").submit(function (event) {
            $(this).parsley().validate();
            if (!$(this).parsley().isValid()) {
                event.preventDefault();
            } else if ($('#hori-pass1').val() != $('#hori-pass2').val()) {
                event.preventDefault();
                $('#hori-pass2').focus();
            }
        });
    });
    function ganti2(i) {
        if(i=='P'){
            $('#programs').show();
            $('.kegiatan').hide();
            $('#inlineRadioIrban1').prop( "checked", false);
            $('#inlineRadioIrban2').prop( "checked", true );
        }else if(i=='K'){
            $('#programs').hide();
            $('.kegiatan').show();
            $('#inlineRadioIrban1').prop( "checked", true);
            $('#inlineRadioIrban2').prop( "checked", false );
        }else{
            $('#inlineRadioIrban1').prop( "checked", false);
            $('#inlineRadioIrban2').prop( "checked", false);
            $('#programs').hide();
            $('.kegiatan').hide();
        }

    }
</script>