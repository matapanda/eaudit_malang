<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="?i=true" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> FORM ISIAN</a>
            <a href="?" class="btn btn-default"><i class="fa fa-refresh"></i></a>
            <hr>
            <?php
            show_alert();
            ?>
            <form method="get" class="row" action="<?=base_url("internalproses/$judul")?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" readonly class="form-control input-sm" name="start" onchange="this.form.submit()" value="<?=$start?>">
                            <span class="input-group-addon input-sm">~</span>
                            <input type="text" readonly class="form-control input-sm" name="end" onchange="this.form.submit()" value="<?=$end?>">
                        </div>
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="">Semua status</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status aktif</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status closed</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-xs-12">
                    <div class="mails">
                        <?php
                        $no=1;

                        $user=$this->session->userdata('user');
                        foreach($rencana as $r) {
                        ?>
                        <div class="table-box">

                            <div class="table-detail mail-right">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-box m-t-20">
                                            <h4 class="m-t-0"><b>Judul : <?=$r['judul']?></b>
                                                <?php
                                                if($r['closed']=='f'){
                                                    ?>
                                                <a href="?reply=<?=$r['id']?>" class="btn btn-primary btn-xs"><i class="mdi mdi-reply m-r-10"></i>| Reply </a>
                                                    <a href="?edit=<?=$r['id']?>" class="btn <?= $user['id']!=$r['created_by']?'hidden':''?> btn-success btn-xs"> Edit </a>
                                                    <a href="?close=<?=$r['id']?>" class="btn <?= $user['id']!=$r['created_by']?'hidden':''?> btn-warning btn-xs"><i class="mdi mdi-window-close "></i> Close </a>
                                                    <?php
                                                }
                                                ?>
                                                <a href="?v=<?=$r['id']?>" class="btn btn-primary btn-xs"><i class="mdi mdi-account-search m-r-10"></i>| View </a>

                                            </h4>
                                            <!--                                            <h4 class="m-t-0"><b>Hi Bro, How are you?</b></h4>-->

                                            <hr/>

                                            <div class="media m-b-30 ">
                                                <a href="#" class="pull-left">
                                                    <img alt="" src="<?=imageExist($r['avatar'])?>" class="media-object thumb-sm img-circle">
                                                </a>
                                                <div class="media-body">
                                                    <span class="media-meta pull-right"><?=$r['created_at']?></span>
                                                    <h4 class="text-primary m-0"><?=$r['nama']?></h4>
                                                    <small class="text-muted">Dari : <?=$r['email']?></small>
                                                </div>
                                            </div> <!-- media -->
                                            <?=$r['deskripsi']?>
                                            <hr/>

                                            <?php
                                            $kk = json_decode($r['file'], TRUE);
                                            ?>
                                            <h4> <i class="fa fa-paperclip m-r-10 m-b-10"></i> Attachments <span>(<?=count($kk)?>)</span> </h4>
                                            <?php
                                            if($kk>0) {
                                                ?>
                                                <div class="row">
                                                    <?php

                                                    echo '<label id="file_r" class="col-sm-8 form-control-label">';
                                                    foreach ($kk as $d):
                                                        echo '<a href= ' . base_url('img/' . $d['file']) . ' class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-text"></i> ' . $d['nama'] . '</a> ';
                                                    endforeach;
                                                    ?>
                                                    <!--                                                <div class="col-sm-2 col-xs-4">-->
                                                    <!--                                                    <a href="#"> <img src="assets/images/small/img-1.jpg" alt="attachment" class="img-thumbnail img-responsive"> </a>-->
                                                    <!--                                                </div>-->
                                                    <!--                                                <div class="col-sm-2 col-xs-4">-->
                                                    <!--                                                    <a href="#"> <img src="assets/images/small/img-2.jpg" alt="attachment" class="img-thumbnail img-responsive"> </a>-->
                                                    <!--                                                </div>-->
                                                    <!--                                                <div class="col-sm-2 col-xs-4">-->
                                                    <!--                                                    <a href="#"> <img src="assets/images/small/img-3.jpg" alt="attachment" class="img-thumbnail img-responsive"> </a>-->
                                                    <!--                                                </div>-->
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <!-- card-box -->
                                    </div> <!-- end col -->
                                </div> <!-- end row -->

<!--                                <div class="row">-->
<!--                                    <div class="col-sm-12">-->
<!--                                        <div class="text-right">-->
<!--                                            <button type="button" class="btn btn-primary waves-effect waves-light w-md m-b-30"><i class="mdi mdi-reply m-r-10"></i>Reply</button>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->

                                <!-- End row -->

                            </div> <!-- table detail -->
                        </div>
                        <!-- end table box -->

                            <?php
                            $no++; }
                        ?>
                    </div> <!-- mails -->
                </div>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    function hapus(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang terhapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{delete:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_r) {
        $.post('?',{ri:_i,rn:_r},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html("Detail");
            $('.modal-body','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>