<div class="row" ng-app="emjesSA" ng-controller="emjesController" ng-init="init()">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-xs-12">
                <div id="form-data" name="datauserForm" novalidate="" method="post" class="form-horizontal"
                     ng-submit="simpan(1)">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nomor SPT</label>
                            <div class="col-md-7" style="padding: .5em">
                                <?= ($rencana['spt_no']) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal SPT</label>

                            <div class="col-md-7" style="padding: .5em">
                                <?= format_waktu($rencana['spt_date']) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Judul SPT</label>

                            <div class="col-md-7" style="padding: .5em">
                                <?= ($rencana['judul']) ?>
                            </div>
                        </div>
                        <form method="post" action="?">
                        <div class="form-group">
                            <label class="col-md-3 control-label">No Laporan</label>

                            <div class="col-md-7" style="padding: .5em">
                                <input type="hidden"name="id_laporan" value="<?=@$rencana['id']?>">
                                <input type="text" class="form-control" name="no_laporan" required value="<?=@$rencana['no_laporan']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tahun Laporan</label>
                            <div class="col-md-7" style="padding: .5em">
<?php
$year=date('Y');
$beforeyear=date('Y')-1;
$nextyear=date('Y')+1;
?>
                                <select name='tahun' class='form-control' required>
                                    <option value='<?=$beforeyear?>' <?=($rencana['thn_laporan']==$beforeyear?'selected':'')?>><?=$beforeyear?></option>
                                    <option value='<?=$year?>' <?=($rencana['thn_laporan']==$year?'selected':'')?>><?=$year?></option>
                                    <option value='<?=$nextyear?>' <?=($rencana['thn_laporan']==$nextyear?'selected':'')?>><?=$nextyear?></option>
                                </select>
                                <br>
                            </div>
                            <div class="form-group">
                            <input type="submit" class="col-md-offset-3 btn btn-inverse" value="Simpan">
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="col-xs-6" style="text-align: right">
                        <table class="table">
                            <tr>
                                <td style="padding-top: 0;padding-bottom: 0">Proses realisasi/revisi</td>
                                <td class="danger"></td>
                            </tr>
                            <tr>
                                <td style="padding-top: 0;padding-bottom: 0">Proses persetujuan</td>
                                <td class="warning"></td>
                            </tr>
                            <tr>
                                <td style="padding-top: 0;padding-bottom: 0">Telah disetujui</td>
                                <td class="success"></td>
                            </tr>
                        </table>
                        <br>
                        <a class="btn btn-default" href="<?=base_url('internalproses/proses')?>"><i class="fa fa-backward"></i> KEMBALI</a>
                    </div>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th rowspan="2" class="center">Kode TAO</th>
                            <th rowspan="2" class="center">Nomer SPT</th>
                            <th rowspan="2" class="center">Tanggal SPT</th>
                            <th class="col-xs-1" rowspan="2" class="center"></th>
                        </tr>
                        </thead>
                        <?php

                        foreach ($tao as $t):
                                ?>
                                <tr>
                                <td class="center">
                                   <?=$t['kode']?>
                                </td>
                                    <td class="center">
                                   <?=$t['spt_no']?>
                                </td>
                                    <td class="center">
                                   <?=$t['spt_date']?>
                                </td>
                                  <?php  if($kesimpulan>0){ ?>
                                    <td class="center">
                                        <button type="button" onclick="kesimpulan('<?= $t['id'] ?>')"
                                                class="btn btn-inverse btn-block"><i
                                                class="fa fa-download"></i>
                                        </button>
                                    </td>
                                <?php
                                  } else{ ?>
                                      <td class="center">
                                          <button type="button" onclick="cek('<?= $t['id'] ?>')"
                                                  class="btn btn-inverse btn-block"><i
                                                  class="fa fa-search"></i>
                                          </button>
                                      </td>
                                    <?php
                                  } ?>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                    </table>
                </div>
            </div><!-- end col-->
        </div>
    </div>
</div>
<div class="modal fade" id="uploadModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"
      type="text/css"/>
<script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script>
    $(function () {
        $('.select2').select2();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
    });
    function laksanakan(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(0).text()+' '+$('tr#'+_id).children('td').eq(1).text();
        $('.modal-title',_modal).html(_title);
        $('[name=id]',_modal).val(_id);
        $('[name=realisasi_by]',_modal).val('');
        $('[name=file]',_modal).val('');
        $('[name=realisasi_date]',_modal).val('<?=date('d/m/Y')?>');
        _modal.modal('show');
    }
    function kesimpulan(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(0).text()+' '+$('tr#'+_id).children('td').eq(1).text();
        $('.modal-title',_modal).html("Kesimpulan");
        $.post('?kesimpulan='+_id,function(data,status){
            $('.modal-body',_modal).html(data);
            _modal.modal('show');
        });
    }
    function cek(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(0).text()+' '+$('tr#'+_id).children('td').eq(1).text();
        $('.modal-title',_modal).html("Kesimpulan");
        $.post('?cek='+_id,function(data,status){
            $('.modal-body',_modal).html(data);
            _modal.modal('show');
        });
    }
</script>