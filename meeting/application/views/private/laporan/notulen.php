<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <form method="get" class="row" action="<?=base_url("laporan/$judul")?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" readonly class="form-control input-sm" name="start" onchange="this.form.submit()" value="<?=$start?>">
                            <span class="input-group-addon input-sm">~</span>
                            <input type="text" readonly class="form-control input-sm" name="end" onchange="this.form.submit()" value="<?=$end?>">
                        </div>
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="">Semua status</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status aktif</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status closed</option>
                        </select>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-xs-12">
                    <div class="mails">
                        <?php
                        $no=1;

                        $user=$this->session->userdata('user');
                        foreach($rencana as $r) {
                            ?>
                            <div class="table-box">

                                <div class="table-detail mail-right">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box m-t-20">
                                                <input type="hidden" id="kom<?= $r['id'] ?>" value="<?=$r['notulen_note']?>">
                                                <h4 class="m-t-0"><b>Judul : <?=$r['judul']?></b>
                                                        <a class="btn btn-success btn-xs" onclick="laksanakan('<?= $r['id'] ?>')"><i class="mdi mdi-bell m-r-10"></i>| UPLOAD </a>
                                                    <a href="<?=base_url('assets/notulen.docx')?>" TARGET="_blank" class="btn btn-primary btn-xs"><i class="mdi mdi-download m-r-10"></i>| DOWNLOAD </a>

                                                </h4>
                                                <!--                                            <h4 class="m-t-0"><b>Hi Bro, How are you?</b></h4>-->

                                                <hr/>

                                                <div class="media m-b-30 ">
                                                    <a href="#" class="pull-left">
                                                        <img alt="" src="<?=imageExist($r['avatar'])?>" class="media-object thumb-sm img-circle">
                                                    </a>
                                                    <div class="media-body">
                                                        <span class="media-meta pull-right"><?=$r['created_at']?></span>
                                                        <h4 class="text-primary m-0"><?=$r['nama']?></h4>
                                                        <small class="text-muted">Dari : <?=$r['email']?></small>
                                                    </div>
                                                </div> <!-- media -->
                                                <?=$r['deskripsi']?>
                                                <hr/>

                                                <?php
                                                $kk = json_decode($r['file'], TRUE);
                                                ?>
                                                <h4> <i class="fa fa-paperclip m-r-10 m-b-10"></i> Attachments <span>(<?=count($kk)?>)</span> </h4>
                                                <?php
                                                if($kk>0) {
                                                    ?>
                                                    <div class="row">
                                                        <?php

                                                        echo '<label id="file_r" class="col-sm-8 form-control-label">';
                                                        foreach ($kk as $d):
                                                            echo '<a href= ' . base_url('img/' . $d['file']) . ' class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-text"></i> ' . $d['nama'] . '</a> ';
                                                        endforeach;
                                                        ?>
                                                        <!--                                                <div class="col-sm-2 col-xs-4">-->
                                                        <!--                                                    <a href="#"> <img src="assets/images/small/img-1.jpg" alt="attachment" class="img-thumbnail img-responsive"> </a>-->
                                                        <!--                                                </div>-->
                                                        <!--                                                <div class="col-sm-2 col-xs-4">-->
                                                        <!--                                                    <a href="#"> <img src="assets/images/small/img-2.jpg" alt="attachment" class="img-thumbnail img-responsive"> </a>-->
                                                        <!--                                                </div>-->
                                                        <!--                                                <div class="col-sm-2 col-xs-4">-->
                                                        <!--                                                    <a href="#"> <img src="assets/images/small/img-3.jpg" alt="attachment" class="img-thumbnail img-responsive"> </a>-->
                                                        <!--                                                </div>-->
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <!-- card-box -->
                                        </div> <!-- end col -->
                                    </div> <!-- end row -->

                                    <!--                                <div class="row">-->
                                    <!--                                    <div class="col-sm-12">-->
                                    <!--                                        <div class="text-right">-->
                                    <!--                                            <button type="button" class="btn btn-primary waves-effect waves-light w-md m-b-30"><i class="mdi mdi-reply m-r-10"></i>Reply</button>-->
                                    <!--                                        </div>-->
                                    <!--                                    </div>-->
                                    <!--                                </div>-->

                                    <!-- End row -->

                                </div> <!-- table detail -->
                            </div>
                            <!-- end table box -->

                            <?php
                            $no++; }
                        ?>
                    </div> <!-- mails -->
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="uploadModal" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-large" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form role="form" method="post" enctype="multipart/form-data" onsubmit="$('#preloader').show();">
                        <input type="hidden" name="m_id" value="">
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Upload Berkas<span class="text-danger">*</span>
                                <br>
<!--                                <button type="button" class="btn btn-xs btn-inverse add-file-upload"><i class="fa fa-plus"></i></button>-->
                            </label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-xs-12 list-upload">
                                        <input type="file" name="file[]" required>
                                    </div>
                                </div>
                                <div class="row list-uploads"></div>
                                <div class="row past-uploads"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Kesimpulan</label>
                            <div class="col-sm-8">
                                <textarea name="kesimpulan" placeholder="Kesimpulan" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-8 col-sm-offset-4">
                                <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                                <button type="button" data-dismiss="modal" class="btn btn-default waves-effect m-l-5">Batal</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>


    function laksanakan(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(0).text()+' '+$('tr#'+_id).children('td').eq(1).text();
        var _komentar=$('#kom'+_id).val();
        $('.modal-title',_modal).html(_title);
        $('[name=m_id]',_modal).val(_id);
        $('[name=file]',_modal).val('');
        $('[name=kesimpulan]',_modal).val(_komentar);
        $('.list-uploads').html('');
        //$('.past-uploads').html('<img src="<?//=base_url('assets/loading.gif')?>//">');
        _modal.modal('show');
        $.get('?',{files:_id},function(data){
            $('.past-uploads').html(data);

            $('.remove-past-upload').on('click',function(){
                $(this).closest('.list-upload').remove();
                var _fn=$(this).data('id');
                var _idtao=_id;
                $.get('?',{remove_files:_fn,remove_id:_id},function(data){
                    $('.remove-past-upload').on('click',function(){
                        $(this).closest('.list-upload').remove();
                    });
                });
            });
        });
    }

    // function hapus2() {
    //     $(this).closest('.list-upload').remove();
    //     var _fn=$(this).data('id');
    //     $.get('?',{remove_files:_fn,remove_id:_id},function(data){
    //         $('.remove-past-upload').on('click',function(){
    //             $(this).closest('.list-upload').remove();
    //         });
    //     });
    // }
    function hapus(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang terhapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{delete:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_r) {
        $.post('?',{ri:_i,rn:_r},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html("Detail");
            $('.modal-body','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>