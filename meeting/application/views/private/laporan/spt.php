<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?><h2 class="visible-print center">LAPORAN SPT</h2>

            <form method="get" class="row hidden-print" action="<?=base_url('laporan/spt')?>">
                <div class="col-md-12">
                    <button type="button" onclick="window.print()" class="btn btn-primary hidden-print hidden-xs"><span class="ace-icon fa fa-print icon-on-right bigger-110"></span> PRINT</button>
                    <a href="?" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                        <input type="submit" hidden>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-2">No PKPT</th>
                        <th class="center col-xs-4">Nama PKPT</th>
                        <th class="center col-xs-2">Tema PKPT</th>
                        <th class="center col-xs-4">Kegiatan PKPT</th>
                        <th class="center col-xs-1 hidden-print <?=is_authority(@$access['u'])?>"></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    foreach($pkpt as $r) {
                        ?>
                        <tr>
                            <td class="center"><?=$r['no']?></td>
                            <td class=""><?=$r['nama']?></td>
                            <td class=""><?=$r['tema']?></td>
                            <td class=""><?=$r['kegiatan']?></td>
                            <td class="center hidden-print">
                                <a href="<?=base_url("internalproses/rencana?i=$r[id]")?>" class="btn btn-sm btn-inverse <?= is_authority(@$access['u']) ?>"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>