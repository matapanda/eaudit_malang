<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <hr>
            <form method="get" class="row" action="<?=base_url('laporan/hasil_r')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="" <?=$closed==''?'selected':''?>>Semua Status</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status Aktif</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status Non Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                    </div>
                </div>

            </form>
            <div class="table-responsive">
                <br><table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-1">No Agenda</th>
                        <th class="center col-xs-2">Tema</th>
                        <th class="center col-xs-2">Judul</th>
                        <th class="center col-xs-5">Notulen</th>
                        <th class="center col-xs-1">Action</th>
                        <!--                        <th class="center col-xs-2 --><?//=is_authority(@$access['u'])?><!--">Action</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($rencana as $g) {
                        ?>
                        <tr>
                            <td scope="row" class="center"><?=$no?></td>
                            <td class="center"><?=$g['no_agenda']?>
                            </td>
                            <td class="center"><?=$g['tema_n']?>
                            </td>
                            <td class="center"><?=$g['judul']?>
                            </td>
                            <td class="right">
                              <?php
                              $kk = json_decode($g['notulen_file'], TRUE);
                              if (is_array($kk)) {
                              foreach ($kk as $d):
                                  if (strlen($d['nama']) > 30) {
                                      $d['nama'] = substr($d['nama'], 0, 30);
                                  }
                                  echo "<a href= '" . base_url('img/' . $d['file']) . "' class='btn btn-sm btn-secondary' target='_blank'><i class='fa fa-file-text'></i>$d[nama]</a>";
                              endforeach;
                              }
                              ?>
                            </td>
                            <td class="center  hand-cursor">
                                <?php
                                if($g['tl']=='f'){
                                    ?>
                                    <a href="?reply=<?=$g['id']?>" class="btn btn-success btn-block btn-xs">Upload Dokumen </a>
                                    <?php
                                }
                                ?>
                                <a href="?v=<?=$g['id']?>" class="btn btn-warning btn-block btn-xs">Lihat Dokumen</a>

                            </td>
                        </tr>
                        <?php
                        $no++;}
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>
<div id="new-group" class="modal-gradin">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Tambah Jenis Rapat</h4>
    <div class="custom-modal-text">
        <form action="?" method="post">
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" name="kode" required class="form-control" placeholder="Kode Jenis Rapat">
                </div> <br>
                <div class="col-sm-12">
                    <input type="text" name="ket" required class="form-control" placeholder="Nama Jenis Rapat">
                </div>
                <div class="col-sm-12 right" style="padding-top: 1em">
                    <button type="button" onclick="Custombox.close();" class="btn btn-default">BATAL</button>
                    <button type="submit" class="btn btn-inverse">SIMPAN</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $.fn.editable.defaults.mode = 'popup';
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
    $(function(){
        <?php
        if(!isset($access['u'])){
        ?>
        $(".emjes-editable").editable('option', 'disabled', true);
        <?php
        }
        ?>
    });
</script>