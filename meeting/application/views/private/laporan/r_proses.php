<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <td colspan="2"><h5>Tanggal</h5></td>
                        <td>:</td>
                        <td><?= format_waktu($r['created_at']) ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>Tema Rapat</h5></td>
                        <td>:</td>
                        <td><?= $r['tema_n'] ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>Judul Rapat</h5></td>
                        <td>:</td>
                        <td><?= $r['judul'] ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>Peserta</h5></td>
                        <td>:</td>
                        <td><?php
                            if(is_array($r['tim'])!==false){
                                $this->db->where('id',$r['tim']);
                                $a=$this->db->get('users')->row_array();
                                echo $a['name'];
                            }else{
                            $tim=json_decode($r['tim'],true);
                                foreach($tim as $t){
                                    $this->db->where('id',$t);
                                    $a=$this->db->get('users')->row_array();
                                    echo $a['name'].' ; ';
                                }
                            }

                            ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>Ringkasan Materi</h5></td>
                        <td>:</td>
                        <td><?= $r['deskripsi']?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>File Ringkasan Materi</h5></td>
                        <td>:</td>
                        <td><?php
                            if($r['file']!=''){

                            $kk = json_decode($r['file'], TRUE);
                            if (is_array($kk)) {
                                foreach ($kk as $d):
                                    if (strlen($d['nama']) > 30) {
                                        $d['nama'] = substr($d['nama'], 0, 30);
                                    }
                                    echo "<a href= '" . base_url('img/' . $d['file']) . "' class='btn btn-sm btn-secondary' target='_blank'><i class='fa fa-file-text'></i>$d[nama]</a>";
                                endforeach;
                            }                            }

                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">

                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>Tanggapan Peserta Rapat</h5></td>
                        <td>:</td>
                        <td></td>
                    </tr>

                    <?php
                    $urutan=1;
                    foreach ($tanggapan as $t) {
                        ?>
                        <tr>
                            <td colspan="4"><h5>
                                <?= $t['nama'] ?></h5></td>
                        </tr>
                        <tr>
                            <td class="right"></td>
                            <td class="right">Tanggal</td>
                            <td class="right">:</td>
                            <td class=""><?= format_waktu($t['created_at']) ?></td>
                        </tr>
                        <tr>
                            <td class="right"></td>
                            <td class="right">Uraian</td>
                            <td class="right">:</td>
                            <td class=""><?= $t['isi'] ?></td>
                        </tr>
                        <tr>
                            <td class="right"></td>
                            <td class="right">File</td>
                            <td class="right">:</td>
                            <td class=""><?php
                                if ($t['file'] != '') {
                                    $kk = json_decode($t['file'], TRUE);
                                    if (is_array($kk)) {
                                        foreach ($kk as $d):
                                            if (strlen($d['nama']) > 30) {
                                                $d['nama'] = substr($d['nama'], 0, 30);
                                            }
                                            echo "<a href= '" . base_url('img/' . $d['file']) . "' class='btn btn-sm btn-secondary' target='_blank'><i class='fa fa-file-text'></i>$d[nama]</a>";
                                        endforeach;
                                    }
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <td class="right" colspan="2"></td>
                            <td colspan="2">
                                <table class="table">
                                    <?php
                                    foreach($tanggapan2 as $t2){
                                        if($t['id_reply']==$t2['refid']) {
                                        ?>

                                            <tr>
                                                <td colspan="4"><h5>
                                                    <?= $t2['nama'] ?></h5></td>
                                            </tr>
                                            <tr>
                                                <td class="" width="10%">Tanggal</td>
                                                <td class="" width="5%">:</td>
                                                <td class="" colspan="2"><?= format_waktu($t2['created_at']) ?></td>
                                            </tr>
                                            <tr>
                                                <td class="" width="10%">Uraian</td>
                                                <td class="" width="5%">:</td>
                                                <td class="" colspan="2"><?= $t2['isi'] ?></td>
                                            </tr>
                                            <tr>
                                                <td class="" width="10%">File</td>
                                                <td class="" width="5%">:</td>
                                                <td class="" colspan="2"><?php
                                                    if ($t2['file'] != '') {

                                                        $kk = json_decode($t2['file'], TRUE);
                                                        if (is_array($kk)) {
                                                            foreach ($kk as $d):
                                                                if (strlen($d['nama']) > 30) {
                                                                    $d['nama'] = substr($d['nama'], 0, 30);
                                                                }
                                                                echo "<a href= '" . base_url('img/' . $d['file']) . "' class='btn btn-sm btn-secondary' target='_blank'><i class='fa fa-file-text'></i>$d[nama]</a>";
                                                            endforeach;
                                                        }
                                                    }
                                                    ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </table>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>