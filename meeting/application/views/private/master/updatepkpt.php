<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-6 col-md-offset-3">
                <form role="form" method="post">
                    <div class="form-group row">
                        <label for="tim" class="col-sm-4 form-control-label">No PKPT<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select class="form-control select2" name="thn" ng-model="thn" required>
                                <option value="">
                                    Pilih Tahun PKPT..
                                </option>
                                <?php
                                if(isset($user['no'])){
                                    $no=substr(@$user['no'], 4);
                                }
                                foreach($list_tahun as $l){
                                    echo "<option value='" . $l['tahun'] . "' " . (substr(@$user['no'], 0, 4) == $l['tahun'] ? 'selected' : '') . ">" . $l['tahun'] . "</option>";
                                }
                                ?>
                            </select>
                            <input type="text" name="no" required class="form-control" id="no" placeholder="no"
                                   value="<?= @$no ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Tema PKPT<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="tema" required class="form-control" id="tema"
                                   placeholder="Tema PKPT" value="<?= @$user['tema'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Nama PKPT<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="nama" required class="form-control" id="nama"
                                   placeholder="Nama PKPT" value="<?= @$user['nama'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Kegiatan PKPT<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="kegiatan" required class="form-control" id="kegiatan"
                                   placeholder="Kegiatan PKPT" value="<?= @$user['kegiatan'] ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Jenis PKPT<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select class="select2 form-control select2-multiple" name="jenis" data-placeholder="pilih sasaran audit" required>
                                <option value="">pilih jenis</option>
                                <?php
                                foreach ($jenis as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['jenis']==$v['id']?'selected':'' ?>><?= "$v[kode] - $v[ket]" ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Sasaran PKPT<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select class="select2 form-control select2-multiple" name="sasaran" data-placeholder="pilih sasaran audit" required>
                                <option value="">pilih sasaran</option>
                                <?php
                                foreach ($sasaran as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['sasaran']==$v['id']?'selected':'' ?>><?= "$v[kode] - $v[ket]" ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Tujuan PKPT<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select class="select2 form-control select2-multiple" name="tujuan" data-placeholder="pilih sasaran audit" required>
                                <option value="">pilih tujuan</option>
                                <?php
                                foreach ($tujuan as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['tujuan']==$v['id']?'selected':'' ?>><?= "$v[kode] - $v[ket]" ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">HP <span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="hp" required class="form-control" id="hp" placeholder="HP"
                                   value="<?= @$user['hp'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">RMP <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="rmp" required class="form-control" id="rmp" placeholder="RMP"
                                   value="<?= @$user['rmp'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">RPL <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="rpl" required class="form-control" id="hp" placeholder="RPL"
                                   value="<?= @$user['rpl'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Dana <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="dana" required class="form-control" id="dana" placeholder="Dana"
                                   value="<?= @$user['dana'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Resiko <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="risiko" required class="form-control" id="resiko"
                                   placeholder="Resiko" value="<?= @$user['risiko'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Keterangan <span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="keterangan" required class="form-control" id="keterangan"
                                   placeholder="Keterangan" value="<?= @$user['keterangan'] ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('.select2').select2({});
        $('form').parsley();
        $("form").submit(function (event) {
            $(this).parsley().validate();
            if (!$(this).parsley().isValid()) {
                event.preventDefault();
            } else if ($('#hori-pass1').val() != $('#hori-pass2').val()) {
                event.preventDefault();
                $('#hori-pass2').focus();
            }
        });

    });
</script>