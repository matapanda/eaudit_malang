<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            include VIEWPATH.'alert.php';
            ?>
            <div class="col-md-6 col-md-offset-3">
                <form role="form" method="post">
                    <div class="form-group row">
                        <label for="kode" class="col-sm-4 form-control-label">Kode<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="kode" required parsley-type="text" class="form-control" id="kode" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-4 form-control-label">Keterangan<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="nama" required parsley-type="text" class="form-control" id="nama" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="barangjasa" class="col-sm-4 form-control-label">Jenis Item</label>
                        <div class="col-sm-7">
                            <label class="radio-inline"><input type="radio" name="type" value="1" checked="" onclick="checkBarang()">Barang</label>
                            <label class="radio-inline"><input type="radio" name="type" value="0" onclick="checkBarang()">Jasa</label>
                        </div>
                    </div>
                    <div class="form-group row hidden hitungstok_">
                        <label class="col-sm-4 form-control-label">Hitung Stok</label>
                        <div class="col-sm-7">
                            <div class="btn-switch btn-switch-inverse">
                                <input type="checkbox" name="hitung" checked value="1" id="input-switch-hitung"/>
                                <label for="input-switch-hitung" class="btn btn-sm btn-rounded btn-inverse waves-effect waves-light">
                                    <em class="glyphicon glyphicon-ok"></em>
                                    <strong> Hitung Stok</strong>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    function checkBarang() {
        if($('[name=type][value=1]').is(':checked')){
            $('.hitungstok').show();
        }else{
            $('.hitungstok').hide();
        }
    }
    $(function () {
        checkBarang();
        $('.select2').select2();
        $('form').parsley();
        $('input[name=kode]').focus();
    });
</script>