<htmlpageheader name="header">
</htmlpageheader>
<h2 class="title">BUKU BESAR</h2>
<h3 class="title"><?= strtoupper($nama_coa)?></h3>
<h4 class="title">Periode : <?= $start .'-'. $end?></h4>
<table class="list">
    <thead>
    <tr>
        <th class="center line" width="100px">Tanggal</th>
        <th class="center line" width="150px">No Bukti</th>
        <th class="center line" width="200px">Keterangan</th>
        <th class="center line" width="100px">Debit</th>
        <th class="center line" width="100px">Kredit</th>
        <th class="center line" width="100px">Saldo</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($data as $d) {
        ?>
        <tr>
            <td class="center"><?= format_waktu($d['tanggal']) ?></td>
            <td class="center"><?= ($d['no']==0?"-":"$d[nobukti]-$d[no]")?></td>
            <td><?= $d['uraian'] ?></td>
            <td class="right"><?=format_uang($d['debet']) ?></td>
            <td class="right"><?=format_uang($d['kredit']) ?></td>
            <td class="right"><?=format_uang($d['saldo']) ?></td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>
<htmlpagefooter name="footer">
    <table width="100%">
        <tr>
            <td style="text-align: left"><?= 'Export on: '.date('d/m/Y H:i:s') ?></td>
            <td style="text-align: right">{PAGENO}</td>
        </tr>
    </table>
</htmlpagefooter>
<style>
    @page {
        margin-top: 30px;
        margin-bottom: 70px;
        margin-left: 30px;
        margin-right: 30px;
        footer: html_footer;
        header: html_header;
    }
    .list {
        width: 100%;
        margin-top: 20px;
        margin-bottom: 10px;
        border-bottom: 1px solid black;
    }
    th{
        margin: 10px;
        text-transform: uppercase;
    }
    .right{
        text-align: right;
    }
    .center{
        text-align: center;
    }
    .line{
        border-bottom: 1px solid black;
    }
    .title{
        margin: 0px;
    }
</style>