<ul>
    <li><a href="<?=base_url('welcome')?>" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span> Dashboard </span></a></li>
    <?php
    if(isset($role['f10293a6-79c7-41e2-840e-a597e5ace426']['r'])){
    ?>
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-archive"></i> <span> Master</span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <?php
               $menuid='d66ab356-6792-451d-8f42-84f07e48c9e7';
                if(isset($role[$menuid]['u'])){
                    ?>
                    <li><a href="<?=base_url('master/jabatan')?>">Data Jabatan</a></li>
                    <?php
                }
               $menuid='d66ab356-6792-451d-8f42-84f07e48c937';
                if(isset($role[$menuid]['u'])){
                    ?>
                    <li><a href="<?=base_url('master/jenis')?>">Data Jenis</a></li>
                    <?php
                }
               $menuid='d66ab356-6793-451d-8f42-84f07e48c933';
                if(isset($role[$menuid]['u'])){
                    ?>
                    <li><a href="<?=base_url('master/narasumber')?>">Data Narasumber</a></li>
                    <?php
                }
               $menuid='92559c74-927e-4edf-85c8-203447141840';
                if(isset($role[$menuid]['u'])){
                    ?>
                    <li><a href="<?=base_url('master/group')?>">Data Role</a></li>
                    <?php
                }
                $menuid='d66ab356-6792-451d-8f42-84f07e48b9e7';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master/user')?>">Data User</a></li>
                    <?php
                }
                $menuid='366ab356-6793-451d-8f42-84f07e48c933';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master/tamu')?>">Data Tamu</a></li>
                    <?php
                }
                $menuid='d66ab356-6792-451d-8f42-84f07e48c933';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master/tema')?>">Data Tema</a></li>
                    <?php
                }
                $menuid='466ab356-6793-451d-8f42-84f07e48c933';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master/tindak_lanjut')?>">Data Tindak Lanjut</a></li>
                    <?php
                }
               ?>

            </ul>
        </li>
    <?php
    }
    if(isset($role['fb7ca00c-ef46-4dc1-bf9f-5d00358213a0']['r'])){
        ?>
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-scale-balance"></i> <span> Kegiatan </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled"><?php
                $menuid='ca530176-384e-4c7d-8377-1171ec3a8d05';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/konsultasi')?>">Konsultasi</a></li>
                    <?php
                }
                $menuid='74fa394a-0fee-4b9d-827c-91a09f5c68f5';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/diskusi')?>">Diskusi</a></li>
                    <?php
                }
                $menuid='e3033e60-42d8-47fd-8c8a-6e51cebaba33';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/meeting')?>">Meeting</a></li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    }
   if(isset($role['e964618e-29bd-4e17-91f2-1f42c0fa0488']['r'])){
       ?>
       <li class="has_sub">
           <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-blinds"></i> <span> Laporan</span> <span class="menu-arrow"></span></a>
           <ul class="list-unstyled"><?php

               if(isset($role['8a1dfd71-6c85-412f-91de-970ca913dc60']['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/notulen')?>"><span>Hasil/Notulen Rapat</span></a></li>
                   <?php
               }
               if(isset($role['93391995-8d3d-447c-a50f-1cec916d3c27']['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/hasil_r')?>"><span>Laporan Tindak Lanjut Hasil Rapat</span></a></li>
                   <?php
               }
               $menuid='82f810bf-86ce-4c88-927e-5e9a4ea82587';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/proses')?>">Ringkasan Proses</a></li>
                   <?php
               }
               ?>
           </ul>
       </li>
       <?php
   }
   ?>
</ul>