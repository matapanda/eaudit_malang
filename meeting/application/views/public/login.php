<link rel="stylesheet" type="text/css" href="<?=base_url('assets/vendor/bootstrap/css/bootstrap.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/vendor/animate/animate.css')?>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/vendor/css-hamburgers/hamburgers.min.css')?>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/vendor/animsition/css/animsition.min.css')?>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/vendor/select2/select2.min.css')?>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/vendor/daterangepicker/daterangepicker.css')?>">
<!--=============================================== ================================================-->
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/util2.css')?>">
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/main2.css')?>">
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-form-title">
                <img src="<?=base_url('assets/images/logo.png')?>">
					<span class="login100-form-title-1">
						<?=$config['logotext']?>
					</span>
            </div>

            <form class="login100-form validate-form">
                <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
                    <span class="label-input100">Username</span>
                    <input class="input100" type="text" name="username" placeholder="Enter username">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                    <span class="label-input100">Password</span>
                    <input class="input100" type="password" name="password" placeholder="Enter password">
                    <span class="focus-input100"></span>
                </div>
                <div class=" m-b-26">

                <p class="text-center" id="response-login"></p>
                </div>
                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Login
                    </button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>


<!--===============================================================================================-->
<script src="<?=base_url('assets/vendor/jquery/jquery-3.2.1.min.js')?>"></script>
<!--===============================================================================================-->
<script src="<?=base_url('assets/vendor/animsition/js/animsition.min.js')?>"></script>
<!--===============================================================================================-->
<script src="<?=base_url('assets/vendor/bootstrap/js/popper.js')?>"></script>
<script src="<?=base_url('assets/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
<!--===============================================================================================-->
<script src="<?=base_url('assets/vendor/select2/select2.min.js')?>"></script>
<!--===============================================================================================-->
<script src="<?=base_url('assets/vendor/daterangepicker/moment.min.js')?>"></script>
<script src="<?=base_url('assets/vendor/daterangepicker/daterangepicker.js')?>"></script>
<!--===============================================================================================-->
<script src="<?=base_url('assets/vendor/countdowntime/countdowntime.js')?>"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>
<script type="text/javascript">
    $(function () {
        $('input[name=username]').focus();
        $('form').on('submit', function (e) {
            e.preventDefault();
            var _form=this;
            $('button',_form).hide();
            $("#response-login").html('<img src="<?=base_url('assets/loading.gif')?>">');
            $.post("<?= base_url() ?>gateway/login", $(this).serializeArray(), function (data, status) {
                if (data.status == 1) {
                    $('#preloader').show();
                    $("#response-login").html('');
                    window.location.assign('<?= base_url('gateway') ?>');
                } else {
                    $("#response-login").html("<code>"+data.message+"</code>");
                }
                $('button',_form).show();
            },'json');
        });
    });
</script>