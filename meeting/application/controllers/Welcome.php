<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        if (get_authority('17c15763-d7b1-4846-9fc9-f5a36dfa5468')) {
            $this->db->where('status', true);
            $this->db->where('id not in (select pkpt_no::uuid from interpro.perencanaan WHERE pkpt_no <> \'\')', null, false);
            $this->db->select('count(id) as total');
            $data['tidak'] = $this->db->get('master.pkpt')->row_array();

            $this->db->where('status', true);
            $this->db->where('id in (select pkpt_no::uuid from interpro.perencanaan WHERE pkpt_no <> \'\')', null, false);
            $this->db->select('count(id) as total');
            $data['sesuai'] = $this->db->get('master.pkpt')->row_array();

            $this->db->where('no_laporan is not null');
            $this->db->where('proses', 3);
            $this->db->select('count(id) as total');
            $data['lhp_sesuai'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('no_laporan is null');
            $this->db->where('proses', 3);
            $this->db->select('count(id) as total');
            $data['lhp_tidak'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('proses <>', 0);
            $this->db->select('count(id) as total');
            $data['perencanaan'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->select('count(id) as total');
            $data['perencanaan_all'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('proses <>', 0);
            $this->db->select('count(id) as total');
            $data['pelaksanaan'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('proses >=', 4);
            $this->db->select('count(id) as total');
            $data['pelaporan'] = $this->db->get('interpro.perencanaan')->row_array();

            $tahun = date('Y');
            $this->db->order_by("realisasi,jumlah", 'desc');
            $data['kinerja'] = $this->db->get("interpro.kinerja('$tahun')")->result_array();

            $this->template->user('dashboard', $data, array('title' => 'Dashboard'));

        } else {
            $this->notif();
        }
    }

    public function notif()
    {
        if ($this->input->get_post('id')) {
            $link = base_url();
            $this->db->where('readed_at is null', null, false);
            $this->db->where('id', $this->input->get_post('id'));
            $this->db->update('support.notif', array('readed_at' => date('Y-m-d H:i:s') . "+07"));

            $this->db->where('id', $this->input->get_post('id'));
            $data = $this->db->get('support.notif')->row_array();

            $this->load->library('uuid');
            $id = $this->uuid->v4();
            $this->db->insert('interpro.meeting_absen',array(
                'id'=>$id,
                'user'=>$data['id_user'],
                'status'=>true,
                'absen'=>date('Y-m-d H:i:s'),
                'meeting'=>$this->input->get_post('id'),
            ));
            $link = "$data[url]";
            redirect($link);
        }
        $user = $this->session->userdata('user');
        $this->db->limit(100);
        $this->db->order_by('created_at', 'desc');
        $this->db->where('id_user', $user['id']);
        $data['notif'] = $this->db->get("support.notif")->result_array();
        $this->template->user('notif', $data, array('title' => 'Pemberitahuan'));
    }

    public function haram()
    {
        $this->template->guest('haram', array(), array('title' => '403 Akses Haram'));
    }
}
