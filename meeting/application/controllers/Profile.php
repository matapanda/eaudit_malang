<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->template->set_view('master');
    }

    public function index()
    {
        $this->load->model('user');
        $sessid=$this->session->userdata('user');
        $data['id']=$sessid['id'];
        $data['user'] = $this->user->get_setting_by_id($data['id']);
            if ($this->input->get_post('name') && $this->input->get_post('email')) {
                $data['status_update'] = $this->user->update_profile($this->input->get_post(null), $data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('profile'));
            }
            $data['role'] = $this->user->get_group_access(array('status' => true));
            $data['roleusers'] = $this->user->get_users_group($data['id']);
            $this->template->user('profile', $data, array('title' => $data['user']['name'], 'breadcrumbs' => array('Master', 'Setting', 'Perbarui')));
            return;
    }

    public function settings()
    {
        $this->load->model('user');
        $sessid=$this->session->userdata('user');
        $data['id']=$sessid['id'];
        $data['user'] = $this->user->get_setting_by_id($data['id']);
            if ($this->input->get_post('nama')) {
                $data['status_update'] = $this->user->update_setting($this->input->get_post(null), $data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('profile/settings'));
                return;
            }
            $data['role'] = $this->user->get_group_access(array('status' => true));
            $data['roleusers'] = $this->user->get_users_group($data['id']);
            $this->template->user('setting', $data, array('title' => $data['user']['name'], 'breadcrumbs' => array('Master', 'Profile', 'Perbarui')));
            return;
    }

    public function upload() {
        $id=$this->input->post('id');
        $config['upload_path'] = './img/';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload("imagefile")) {
            $status = 'error';
            $msg = $this->upload->display_errors('', '');
        } else {
            $data = $this->upload->data();
            $status = "success";
            $msg = $data['file_name'];
            $image_config["image_library"] = "gd2";
            $image_config["source_image"] = $data["full_path"];
            $image_config['create_thumb'] = TRUE;
            $image_config['maintain_ratio'] = TRUE;
            $image_config['new_image'] = $data["full_path"];
            $image_config['quality'] = "100%";
            $image_config['width'] = 1280;
            $image_config['height'] = 620;
            $dim = (intval($data["image_width"]) / intval($data["image_height"])) - ($image_config['width'] / $image_config['height']);
            $image_config['master_dim'] = ($dim > 0) ? "height" : "width";
            $this->load->library('image_lib');
            $this->image_lib->initialize($image_config);
            $this->image_lib->resize();
            $image_config['maintain_ratio'] = FALSE;
            $image_config['width'] = 1280;
            $image_config['height'] = 620;
            $this->image_lib->initialize($image_config);
            $this->image_lib->crop();
            $this->db->where('id',$id);
            $this->db->update('users',array('avatar'=>$msg));
            $user=$this->session->userdata('user');
            $user['avatar']=$msg;
            $this->session->set_userdata('user', $user);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }
    //ttd
    public function upload2() {
        $id=$this->input->post('id');
        $config['upload_path'] = './img/';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload("imagefile2")) {
            $status = 'error';
            $msg = $this->upload->display_errors('', '');
        } else {
            $data = $this->upload->data();
            $status = "success";
            $msg = $data['file_name'];
            $image_config["image_library"] = "gd2";
            $image_config["source_image"] = $data["full_path"];
            $image_config['create_thumb'] = TRUE;
            $image_config['maintain_ratio'] = TRUE;
            $image_config['new_image'] = $data["full_path"];
            $image_config['quality'] = "100%";
            $image_config['width'] = 1280;
            $image_config['height'] = 620;
            $dim = (intval($data["image_width"]) / intval($data["image_height"])) - ($image_config['width'] / $image_config['height']);
            $image_config['master_dim'] = ($dim > 0) ? "height" : "width";
            $this->load->library('image_lib');
            $this->image_lib->initialize($image_config);
            $this->image_lib->resize();
            $image_config['maintain_ratio'] = FALSE;
            $image_config['width'] = 1280;
            $image_config['height'] = 620;
            $this->image_lib->initialize($image_config);
            $this->image_lib->crop();
            $this->db->where('id',$id);
            $this->db->update('users',array('ttd'=>$msg));
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }
}
