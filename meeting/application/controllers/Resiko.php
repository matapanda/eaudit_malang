<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resiko extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        check_access('923143fd-ca98-489d-88d7-642d2001b6ef');
        $this->template->set_view('resiko');
    }

    public function index()
    {
        redirect(base_url());
    }

    public function program()
    {
        $menu = '388deab8-36ff-4a8b-a1d7-b88eab8b4c26';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->insert_program($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/program'));
            }
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->template->user('updateprogram', $data, array('title' => 'Tambah Data Program Satker', 'breadcrumbs' => array('Manajemen Resiko', 'Tambah Program Satker')));
            return;
        }
        elseif ($this->input->get_post('e')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('e');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->update_program($this->input->get_post(null),$data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/program'));
            }

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('id', $data['id']);
            $data['user']  = $this->db->get('support.program_satker')->row_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan,resiko');
            $data['resiko_kr']  = $this->db->get('support.program_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.program_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.program_kr')->num_rows();
            $this->template->user('updateprogram', $data, array('title' => $data['user']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Perbarui Program Satker')));
            return;
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_program_status($this->input->get_post('status'));
            return;
        }
        elseif ($this->input->get_post('resiko')) {
            $no=$this->input->get_post('resiko');
            check_access($menu, 'c');

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();
            ?>
            <div class="form-group <?=$no?>">
                <h3>Resiko <button type="button" onclick="tambah_resiko(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h3> <br>

                <div class="form-group resiko<?=$no?>">

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Resiko<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="resiko<?=$no?>" data-placeholder="Pilih Tipe Resiko" required>
                                <option value="">Pilih Tipe Resiko</option>
                                <?php
                                foreach ($tipe_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-sm-offset-1">
                        <div class="form-group row">
                            <h5>Kendali <button type="button" onclick="tambah_kendali(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h5> <br>
                        </div>
                        <div class="form-group kendali<?=$no?>">

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" name="kendali_s<?=$no?>[]" data-placeholder="Pilih Kendali Seharusnya" required>
                                        <option value="">Pilih Kendali Sementara</option>
                                        <?php
                                        foreach ($kendali as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" name="kendali_a<?=$no?>[]" data-placeholder="Pilih Kendali Yang Ada" required>
                                        <option value="">Pilih Kendali Yang Ada</option>
                                        <?php
                                        foreach ($kendali as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            return;
        }
        elseif ($this->input->get_post('kendali')) {
            $no=$this->input->get_post('kendali');
            check_access($menu, 'c');

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();
            ?>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span
                            class="text-danger">*</span></label>
                <div class="col-sm-6">
                    <select class="select2 form-control select2-multiple" name="kendali_s<?=$no?>[]" data-placeholder="Pilih Kendali Seharusnya" required>
                        <option value="">Pilih Kendali Sementara</option>
                        <?php
                        foreach ($kendali as $v):
                            ?>
                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada<span
                            class="text-danger">*</span></label>
                <div class="col-sm-6">
                    <select class="select2 form-control select2-multiple" name="kendali_a<?=$no?>[]" data-placeholder="Pilih Kendali Yang Ada" required>
                        <option value="">Pilih Kendali Yang Ada</option>
                        <?php
                        foreach ($kendali as $v):
                            ?>
                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <?php
            return;
        }
        else{

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->db->order_by('nama','desc');
            $data['p_sekarang'] = $this->db->get('periode')->row_array();
            $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : $data['p_sekarang']['id'];
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
            $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
            $data['pkpt'] = $this->user->get_list_p_satker($data['closed'],$data['periode_s']);
            $this->template->user('program_satker', $data, array('title' => 'Program Satker', 'breadcrumbs' => array('Manajemen Resiko')));
        }
       }
    public function kegiatan()
    {
        $menu = '0d59e730-4d54-4ef2-a892-2bdb88f4e732';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->insert_kegiatan_satker($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/kegiatan'));
            }
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['keg'] = $this->db->get('master.kegiatan')->result_array();
            $this->db->order_by('ket');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['program'] = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->template->user('updatekegiatan', $data, array('title' => 'Tambah Data Kegiatan Satker', 'breadcrumbs' => array('Manajemen Resiko', 'Tambah Kegiatan Satker')));
            return;
        }
        elseif ($this->input->get_post('e')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('e');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->update_kegiatan_satker($this->input->get_post(null),$data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/kegiatan'));
            }

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();

            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $data['program']  = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->db->where('id', $data['id']);
            $data['user']  = $this->db->get('support.kegiatan_satker')->row_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan,resiko,p_resiko,da_resiko');
            $data['resiko_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.kegiatan_kr')->num_rows();

            $this->template->user('updatekegiatan', $data, array('title' => $data['user']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Perbarui Kegiatan Satker')));
            return;
        }
        elseif ($this->input->get_post('ev')) {
//            echo 's';die();
            check_access($menu, 'c');
            $data['status_update'] = $this->user->update_anal_kegiatan_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/kegiatan'));
        }
        elseif ($this->input->get_post('upval')) {
//            echo "s";die();
            check_access($menu, 'c');
            $data['status_update'] = $this->user->update_eval_kegiatan_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/kegiatan'));
        }
        elseif ($this->input->get_post('anal')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('anal');
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['keg'] = $this->db->get('master.kegiatan')->result_array();
            $this->db->order_by('ket');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $data['program']  = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->db->where('k.id', $data['id']);
            $this->db->select('mk.*');
            $this->db->join('master.kegiatan mk', 'mk.id=k.kegiatan', 'left');
            $data['users']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('k.id', $data['id']);
            $data['user']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('id,urutan,resiko,p_resiko,da_resiko,sk,sd');
            $data['resiko_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.kegiatan_kr')->num_rows();

            $this->template->user('updateanal', $data, array('title' => 'Analisa - '.$data['users']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Update Analaisa')));
            return;
        }
        elseif ($this->input->get_post('eval')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('eval');
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $data['program']  = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();


            $this->db->where('k.id', $data['id']);
            $this->db->select('mk.*');
            $this->db->join('master.kegiatan mk', 'mk.id=k.kegiatan', 'left');
            $data['users']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('k.id', $data['id']);
            $data['user']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('id', $data['id']);
            $data['resiko_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.kegiatan_kr')->num_rows();

            $this->template->user('updateeval', $data, array('title' => 'Evaluasi - '.$data['users']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Update Analaisa')));
            return;
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_kegiatan_satker_status($this->input->get_post('status'));
            return;
        }
        elseif ($this->input->get_post('resiko')) {
            $no=$this->input->get_post('resiko');
            check_access($menu, 'c');

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $pe_resiko = $this->db->get('master.penyebab_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();

            $this->db->order_by('kode');
            $this->db->where('status', true);
            $da_resiko = $this->db->get('master.dampak')->result_array();
            ?>
            <div class="form-group <?=$no?>">
                <h3>Resiko <button type="button" onclick="tambah_resiko(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h3> <br>

                <div class="form-group resiko<?=$no?>">

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Resiko<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="resiko<?=$no?>" data-placeholder="Pilih Tipe Resiko" required>
                                <option value="">Pilih Tipe Resiko</option>
                                <?php
                                foreach ($tipe_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Penyebab Resiko<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="p_resiko<?=$no?>" data-placeholder="Pilih Penyebab Resiko" required>
                                <option value="">Pilih Penyebab Resiko</option>
                                <?php
                                foreach ($pe_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[kode]" ?> - <?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div><div class="form-group row">
                        <label for="inputName" class="col-sm-2 right form-control-label">Dampak Resiko<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" name="da_resiko1" data-placeholder="Pilih Dampak Resiko" required>
                                <option value="">Pilih Dampak Resiko</option>
                                <?php
                                foreach ($da_resiko as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-sm-offset-1">
                        <div class="form-group row">
                            <h5>Kendali <button type="button" onclick="tambah_kendali(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h5> <br>
                        </div>
                        <div class="form-group kendali<?=$no?>">

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" name="kendali_s<?=$no?>[]" data-placeholder="Pilih Kendali Seharusnya" required>
                                        <option value="">Pilih Kendali Sementara</option>
                                        <?php
                                        foreach ($kendali as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" name="kendali_a<?=$no?>[]" data-placeholder="Pilih Kendali Yang Ada" required>
                                        <option value="">Pilih Kendali Yang Ada</option>
                                        <?php
                                        foreach ($kendali as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Belum Ada<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" name="kendali_ba<?=$no?>[]" data-placeholder="Pilih Kendali Yang Belum Ada" required>
                                        <option value="">Pilih Kendali Yang Belum Ada</option>
                                        <?php
                                        foreach ($kendali as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            return;
        }
        elseif ($this->input->get_post('kegiatan')) {
            $id=$this->input->get_post('kegiatan');

            $this->db->where('id', $id);
            $keg = $this->db->get('master.kegiatan')->row_array();
            $this->db->where('id', $id);
            $data['jml'] = $this->db->get('support.setting_kegiatan')->row_array();
            $this->db->where('id', $id);
            $resiko_kr = $this->db->get('support.setting_kegiatan_detail')->result_array();
            $tot=$data['jml']['jumlah'];

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $pe_resiko = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();

            $this->db->order_by('kode');
            $this->db->where('status', true);
            $da_resiko = $this->db->get('master.dampak')->result_array();
            $no=1;
                foreach ($resiko_kr as $r) {
                    ?>
                    <div class="form-group <?= $no ?>">
                        <h3>Resiko</h3> <br>

                        <div class="form-group resiko<?= $no ?>">

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Resiko<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <input type="hidden" name="urutan" value="<?=$no?>">
                                    <select class="select2 form-control select2-multiple" disabled name="resiko<?= $no ?>"
                                            data-placeholder="Pilih Tipe Resiko">
                                        <option value="">Pilih Tipe Resiko</option>
                                        <?php
                                        foreach ($tipe_resiko as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>" <?= @$r['resiko'] == $v['id'] ? 'selected' : '' ?> ><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Penyebab Resiko<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" disabled name="p_resiko<?= $no ?>"
                                            data-placeholder="Pilih Penyebab Resiko">
                                        <option value="">Pilih Penyebab Resiko</option>
                                        <?php
                                        foreach ($pe_resiko as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>" <?= @$r['p_resiko'] == $v['id'] ? 'selected' : '' ?>><?= "$v[kode]" ?>
                                                - <?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 right form-control-label">Dampak Resiko<span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-6">
                                    <select class="select2 form-control select2-multiple" disabled name="da_resiko<?= $no ?>"
                                            data-placeholder="Pilih Dampak Resiko">
                                        <option value="">Pilih Dampak Resiko</option>
                                        <?php
                                        foreach ($da_resiko as $v):
                                            ?>
                                            <option value="<?= $v['id'] ?>" <?= @$r['da_resiko'] == $v['id'] ? 'selected' : '' ?> ><?= "$v[nama]" ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>

                        <div class="form-group kendali<?= $no ?>">
                            <?php
                                if($r['urutan']==$no){
                                    ?>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya
                                        <span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control select2-multiple" multiple="multiple"
                                                multiple data-placeholder="Pilih Kendali Seharusnya"
                                                name="kendali_s<?= $no ?>[]" disabled>
                                            <option value="">Pilih Kendali Seharusnya</option>
                                            <?php
                                            foreach ($kendali as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>" <?= in_array($v['id'], json_decode($r['kendali_s'], true)) ? 'selected' : '' ?>><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div><div class="form-group row">
                                    <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada
                                        <span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control select2-multiple" multiple="multiple"
                                                multiple data-placeholder="Pilih Kendali Yang Ada"
                                                name="kendali_a<?= $no ?>[]">
                                            <?php
                                            foreach ($kendali as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
            $no++;
                }
            ?>
            <input type="hidden" name="jml_program" required class="form-control" id="jml_program" value="<?=@$no-1?>">

            <?php
            return;
        }
        elseif ($this->input->get_post('kendali')) {
            $no=$this->input->get_post('kendali');
            check_access($menu, 'c');

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $tipe_resiko = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $kendali = $this->db->get('master.kendali')->result_array();
            ?>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span
                        class="text-danger">*</span></label>
                <div class="col-sm-6">
                    <select class="select2 form-control select2-multiple" name="kendali_s<?=$no?>[]" data-placeholder="Pilih Kendali Seharusnya" required>
                        <option value="">Pilih Kendali Sementara</option>
                        <?php
                        foreach ($kendali as $v):
                            ?>
                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada<span
                        class="text-danger">*</span></label>
                <div class="col-sm-6">
                    <select class="select2 form-control select2-multiple" name="kendali_a<?=$no?>[]" data-placeholder="Pilih Kendali Yang Ada" required>
                        <option value="">Pilih Kendali Yang Ada</option>
                        <?php
                        foreach ($kendali as $v):
                            ?>
                            <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Belum Ada<span
                            class="text-danger">*</span></label>
                <div class="col-sm-6">
                    <select class="select2 form-control select2-multiple" name="kendali_ba<?=$no?>[]" data-placeholder="Pilih Kendali Yang Belum Ada" required>
                        <option value="">Pilih Kendali Yang Belum Ada</option>
                        <?php
                        foreach ($kendali as $v):
                            ?>
                            <option value="<?= $v['id'] ?>" ><?= "$v[nama]" ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <?php
            return;
        }
        else{
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->db->order_by('nama','desc');
            $data['p_sekarang'] = $this->db->get('periode')->row_array();
            $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : $data['p_sekarang']['id'];
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
            $data['pkpt'] = $this->user->get_list_k_satker($data['closed'],$data['periode_s']);
            $this->template->user('kegiatan_satker', $data, array('title' => 'Kegiatan Satker', 'breadcrumbs' => array('Manajemen Resiko')));
        }
       }
    public function opsi()
    {
        $menu = '0d59e730-4d54-4ef2-a892-2bdb88f4e7ab';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('opsi')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('opsi');
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['opsi'] = $this->db->get('master.opsi')->result_array();
//            echo json_encode($data['opsi']);die();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['tindak'] = $this->db->get('master.tindak')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['target'] = $this->db->get('master.target')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['keg'] = $this->db->get('master.kegiatan')->result_array();
            $this->db->order_by('ket');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $data['program']  = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->db->where('k.id', $data['id']);
            $this->db->select('mk.*');
            $this->db->join('master.kegiatan mk', 'mk.id=k.kegiatan', 'left');
            $data['users']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('k.id', $data['id']);
            $data['user']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('id', $data['id']);
            $data['resiko_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.kegiatan_kr')->num_rows();

            $this->template->user('updateopsi', $data, array('title' => 'Opsi - '.$data['users']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Update Analaisa')));
            return;
        }
        elseif ($this->input->get_post('o')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->update_opsi_kegiatan_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/opsi'));
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_kegiatan_satker_status($this->input->get_post('status'));
            return;
        }
        else{
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->db->order_by('nama','desc');
            $data['p_sekarang'] = $this->db->get('periode')->row_array();
            $data['opsi'] = true;
            $yes=1;
            $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : $data['p_sekarang']['id'];
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
            $data['pkpt'] = $this->user->get_list_k_satker($data['closed'],$data['periode_s'],$yes);
            $this->template->user('kegiatan_satker', $data, array('title' => 'Opsi & Mitigasi', 'breadcrumbs' => array('Manajemen Resiko')));
        }
       }
    public function monitoring()
    {
        $menu = '0d59e730-4d54-4ef2-a892-2bdb88f4e7ac';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('monitoring')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('monitoring');
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['pe_resiko'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['da_resiko'] = $this->db->get('master.dampak')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['opsi'] = $this->db->get('master.opsi')->result_array();
//            echo json_encode($data['opsi']);die();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['tindak'] = $this->db->get('master.tindak')->result_array();
            $this->db->order_by('kode');
            $this->db->where('status', true);
            $data['target'] = $this->db->get('master.target')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['keg'] = $this->db->get('master.kegiatan')->result_array();
            $this->db->order_by('ket');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $data['program']  = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->db->where('k.id', $data['id']);
            $this->db->select('mk.*');
            $this->db->join('master.kegiatan mk', 'mk.id=k.kegiatan', 'left');
            $data['users']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('k.id', $data['id']);
            $data['user']  = $this->db->get('support.kegiatan_satker k')->row_array();
            $this->db->where('id', $data['id']);
            $data['resiko_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $data['kendali_kr']  = $this->db->get('support.kegiatan_kr')->result_array();
            $this->db->where('id', $data['id']);
            $this->db->distinct();
            $this->db->select('urutan');
            $data['jml_kr']= $this->db->get('support.kegiatan_kr')->num_rows();

            $this->template->user('updatemonitoring', $data, array('title' => 'Monitoring - '.$data['users']['nama'], 'breadcrumbs' => array('Manajemen Resiko')));
            return;
        }
        elseif ($this->input->get_post('m')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->update_monitoring_kegiatan_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('resiko/monitoring'));
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_kegiatan_satker_status($this->input->get_post('status'));
            return;
        }
        else{
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->db->order_by('nama','desc');
            $data['p_sekarang'] = $this->db->get('periode')->row_array();
            $data['monitoring'] = true;
            $yes=2;
            $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : $data['p_sekarang']['id'];
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
            $data['pkpt'] = $this->user->get_list_k_satker($data['closed'],$data['periode_s'],$yes);
            $this->template->user('kegiatan_satker', $data, array('title' => 'Monitoring', 'breadcrumbs' => array('Manajemen Resiko')));
        }
       }
    public function kejadian()
    {
        $menu = 'cb7ac7ac-9d00-4acd-a1fd-2afefb0c8a81';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->insert_kejadian_satker($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/kejadian'));
            }
            $this->db->order_by('id','desc');
            $this->db->where('status', true);
            $data['periodes'] = $this->db->get('periode')->row_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('nama');
            $this->db->where('periode', $data['periodes']['id']);
            $this->db->where('status', true);
            $data['program'] = $this->db->get('support.program_satker')->result_array();
            $this->db->order_by('nama');
            $this->db->where('periode', $data['periodes']['id']);
            $this->db->where('status', true);
            $data['kegiatan'] = $this->db->get('support.kegiatan_satker')->result_array();

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->template->user('updatekejadian', $data, array('title' => 'Tambah Data Kejadian Satker', 'breadcrumbs' => array('Manajemen Resiko', 'Tambah Kejadian Satker')));
            return;
        }
        elseif ($this->input->get_post('e')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('e');
            if($this->input->get_post('nama')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->update_kejadian_satker($this->input->get_post(null),$data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('resiko/kejadian'));
            }

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['prosedur'] = $this->db->get('master.prosedur_audit')->result_array();
            $this->db->order_by('id','desc');
            $this->db->where('status', true);
            $data['periodes'] = $this->db->get('periode')->row_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tipe_resiko'] = $this->db->get('master.tipe_resiko')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['kendali'] = $this->db->get('master.kendali')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->where('status', true);
            $this->db->where('periode', $data['periodes']['id']);
            $data['program']  = $this->db->get('support.program_satker')->result_array();

            $this->db->order_by('nama');
            $this->db->where('periode', $data['periodes']['id']);
            $this->db->where('status', true);
            $data['kegiatan'] = $this->db->get('support.kegiatan_satker')->result_array();
            $this->db->where('id', $data['id']);
            $data['user']  = $this->db->get('support.kejadian_satker')->row_array();
//            echo $data['user']['hitung'];die();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();

            $this->template->user('updatekejadian', $data, array('title' => $data['user']['nama'], 'breadcrumbs' => array('Manajemen Resiko', 'Perbarui Kejadian Satker')));
            return;
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_kejadian_satker_status($this->input->get_post('status'));
            return;
        }
        elseif ($this->input->get_post('periodes')) {
            check_access($menu, 'u');
            $this->user->set_kejadian_satker_status($this->input->get_post('status'));
            $data['programs']='';
            echo json_encode($data);
            return;
        }
        else{
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['periode'] = $this->db->get('periode')->result_array();
            $this->db->order_by('nama','desc');
            $data['p_sekarang'] = $this->db->get('periode')->row_array();
            $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : $data['p_sekarang']['id'];
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
            $data['pkpt'] = $this->user->get_list_ke_satker($data['closed'],$data['periode_s']);
            $this->template->user('kejadian_satker', $data, array('title' => 'Kejadian Satker', 'breadcrumbs' => array('Manajemen Resiko')));
        }
       }

    public function test(){
        $this->template->user('excel','', array('title' => 'Group Access', 'breadcrumbs' => array('Master', 'User')));
    }
    public function upload(){
        $this->db->trans_begin();
        $config['upload_path'] = './img/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload("file")) {
            $data['status_update'] = 'error';
            $data['status_message'] =  $this->upload->display_errors('', '');
        } else if($this->upload->do_upload("file")){
            $file = $this->upload->data();
            $full_path=$file["full_path"];

            $this->load->library('PHPExcel');
            $objPHPExcel = PHPExcel_IOFactory::load($full_path);
            $max=$objPHPExcel->getActiveSheet(0)->getHighestRow();
            set_time_limit(0);
            for($no=1;$no<$max;$no++){
                $row['A']=$objPHPExcel->getActiveSheet()->getCell("A".$no)->getValue();//kode
                $row['B']=$objPHPExcel->getActiveSheet()->getCell("B".$no)->getValue();//nama
                $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
                $id=$raw['id'];
                $this->db->insert('master.satker',array(
                    'id'=>$id,
                    'kode'=>$row['A'],
                    'nama'=>$row['B'],
                    'status'=>'t',
                ));
            }
        }
        if ($this->db->trans_status()){
            $this->db->trans_commit();
            echo "ok";
        }else{
            echo "fail";
        }
        redirect(base_url());
    }
    public function user()
    {
        $menu = 'd66ab356-6792-451d-8f42-84f07e48b9e7';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('u')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('u');
            $this->db->where('status','t');
            $data['jabatan']=$this->db->get('master.jabatan')->result_array();
            $this->db->where('status','t');
            $data['golongan']=$this->db->get('master.golongan')->result_array();
            $data['user'] = $this->user->get_user_by_id($data['id']);
            if (!isset($data['user']['name'])) {
                die("403");
            }
            if ($this->input->get_post('name') && $this->input->get_post('email')) {
                $data['status_update'] = $this->user->update_user($this->input->get_post(null), $data['id']);
                $data['user'] = $this->user->get_user_by_id($data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/user'));
            }
            $data['role'] = $this->user->get_group_access(array('status' => true));
            $data['roleusers'] = $this->user->get_users_group($data['id']);
            $this->template->user('updateuser', $data, array('title' => $data['user']['name'], 'breadcrumbs' => array('Master', 'User', 'Perbarui')));
            return;
        } elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if ($this->input->get_post('name')) {
                $data['status_update'] = $this->user->insert($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/user'));
            }
            $data['role'] = $this->user->get_group_access(array('status' => true));
            $this->template->user('adduser', $data, array('title' => 'Tambah Data', 'breadcrumbs' => array('Master', 'User')));
            return;
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_user_status($this->input->get_post('status'));
            return;
        } elseif ($this->input->get_post('is_exist')) {
            $this->db->like('username', $this->input->get_post('is_exist'));
            $data['jumlah'] = $this->db->get('users')->num_rows();
            echo json_encode($data);
            return;
        }
        $data['user'] = $this->user->get_list_user();
        $this->template->user('user', $data, array('title' => 'User Access', 'breadcrumbs' => array('Master', 'User')));
    }


    //PKPT e-audit Banyuwangi
    public function pkpt()
    {
        $menu = '2a5a511b-7bd4-424f-8b38-252f6115640f';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        $this->db->order_by('nama','desc');
        $this->db->where('status','t');
        $this->db->select('nama as tahun');
        $data['list_tahun'] = $this->db->get('public.periode')->result_array();
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.pkpt', array($this->input->get_post('name') => $value));
            return;
        }
        elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if($this->input->get_post('tema')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->insert_pkpt($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/pkpt'));
            }
            $this->db->order_by('sasaran.order,sasaran.kode', 'asc');
            $this->db->where('status', true);
            $data['sasaran'] = $this->db->get('master.sasaran')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['jenis'] = $this->db->get('master.jenis')->result_array();

            $this->db->order_by('tujuan.order,tujuan.kode', 'asc');
            $this->db->where('tujuan.status', true);
            $data['tujuan'] = $this->db->get('master.tujuan')->result_array();

            $data['role'] = $this->user->get_group_access(array('status' => true));
            $this->template->user('updatepkpt', $data, array('title' => 'Tambah Data PKPT', 'breadcrumbs' => array('Master', 'PKPT')));
            return;
        }
        elseif ($this->input->get_post('e')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('e');
            $data['user'] = $this->user->get_pkpt_by_id($data['id']);
            if($this->input->get_post('tema')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->update_pkpt($this->input->get_post(null),$data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/pkpt'));
            }

            $this->db->order_by('sasaran.order,sasaran.kode', 'asc');
            $this->db->where('status', true);
            $data['sasaran'] = $this->db->get('master.sasaran')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['jenis'] = $this->db->get('master.jenis')->result_array();

            $this->db->order_by('tujuan.order,tujuan.kode', 'asc');
            $this->db->where('tujuan.status', true);
            $data['tujuan'] = $this->db->get('master.tujuan')->result_array();
            $this->template->user('updatepkpt', $data, array('title' => $data['user']['no'], 'breadcrumbs' => array('Master', 'PKPT', 'Perbarui')));
            return;
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_pkpt_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.pkpt.nama)', $data['search']);
            $this->db->or_like('lower(master.pkpt.hp)', $data['search']);
            $this->db->or_like('lower(master.pkpt.kegiatan)', $data['search']);
            $this->db->or_like('lower(master.pkpt.rmp)', $data['search']);
            $this->db->or_like('lower(master.pkpt.tema)', $data['search']);
            $this->db->or_like('lower(master.pkpt.rpl)', $data['search']);
            $this->db->or_like('lower(master.pkpt.dana)', $data['search']);
            $this->db->or_like('lower(master.pkpt.risiko)', $data['search']);
            $this->db->or_like('lower(master.pkpt.keterangan)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.pkpt.status', $data['closed']);
        }
        if ($this->input->get_post('tahun')) {
            $data['tahun'] = $this->input->get_post('tahun');
        } else {
            $data['tahun'] = @$data['list_tahun'][0]['tahun'];
        }
        $data['pkpt'] = $this->user->get_list_pkpt($data['tahun']);
        $this->template->user('pkpt', $data, array('title' => 'PKPT', 'breadcrumbs' => array('Master')));
    }

    //JABATAN e-audit Banyuwangi
    public function jabatan()
    {
        $menu = 'acda06a9-1a27-45f0-bceb-1b77c81b8749';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.jabatan', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_jabatan($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/jabatan'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_jabatan_status($this->input->get_post('status'));
            return;
        } elseif ($this->input->get_post('is_exist')) {
            $this->db->like('username', $this->input->get_post('is_exist'));
            $data['jumlah'] = $this->db->get('users')->num_rows();
            echo json_encode($data);
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.jabatan.ket)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.jabatan.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_jabatan();
        $this->template->user('jabatan', $data, array('title' => 'Jabatan', 'breadcrumbs' => array('Master')));
    }
    //JABATAN e-audit Banyuwangi
    public function conf_spt()
    {
        $menu = '292e5ff3-1095-49b3-aafd-565794ee545e';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.conf_spt', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_conf($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/conf_spt'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_conf_spt_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.conf_spt.ket)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.conf_spt.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_conf_spt();
        $this->template->user('conf_spt', $data, array('title' => 'Config SPT', 'breadcrumbs' => array('Master')));
    }

    //WILAYAH e-audit Banyuwangi
    public function wilayah()
    {
        $menu = '92559c74-927e-4edf-85c8-203447141840';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('support.wilayah', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('new')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_wilayah($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/wilayah'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_wilayah_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(public.users.name)', $data['search']);
            $this->db->or_like('lower(support.wilayah.wilayah)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('support.wilayah.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_wilayah();
        $this->db->where('public.roleusers.role', '2332800e-edb0-49cd-92d7-1abc3d981207');
        $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
        $data['irban'] = $this->db->get('public.users')->result_array();
        $data['list'] = array();
        foreach ($data['irban'] as $d) {
            $data['list'][] = array('id' => $d['id'], 'text' => $d['name']);
        }
        $data['irban2'] = array();
        foreach ($data['irban'] as $d) {
            $data['irban2'][] = array('value' => $d['id'], 'text' => $d['name']);
        }
        $this->template->user('wilayah', $data, array('title' => 'Wilayah', 'breadcrumbs' => array('Master')));
    }

    //TIM e-audit Banyuwangi
    public function tim()
    {
        $menu = '36fbf7b0-d4ba-49d1-af67-1e0af4a05766';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('support.tim', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('e')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('e');
            $data['user'] = $this->user->get_tim_by_id($data['id']);
            if ($this->input->get_post('tim') && $this->input->get_post('wilayah')) {
                $data['status_update'] = $this->user->update_tim($this->input->get_post(null), $data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                $data['user'] = $this->user->get_tim_by_id($data['id']);
            }
            $data['dalnis'] = array();
            $this->db->where('public.roleusers.role', '108ce450-d740-4bcb-9aaa-be9504bf94d8');
            $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
            $dalnis = $this->db->get('public.users')->result_array();

            foreach ($dalnis as $d) {
                $data['dalnis'][] = array('id' => $d['id'], 'text' => $d['name']);
            }

            $this->db->where('public.roleusers.role', '97cc7404-490c-4827-beba-425c85acef5d');
            $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
            $ketua = $this->db->get('public.users')->result_array();

            $data['ketua'] = array();
            foreach ($ketua as $d) {
                $data['ketua'][] = array('id' => $d['id'], 'text' => $d['name']);
            }

            $wilayah = $this->db->get('support.wilayah')->result_array();

            $data['wilayah'] = array();
            foreach ($wilayah as $d) {
                $data['wilayah'][] = array('id' => $d['id'], 'text' => $d['wilayah']);
            }

            $this->db->where('public.roleusers.role', '9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26');
            $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
            $anggota = $this->db->get('public.users')->result_array();

            $data['anggota'] = array();
            foreach ($anggota as $d) {
                $data['anggota'][] = array('id' => $d['id'], 'text' => $d['name']);
            }

            $data['anggotatim'] = $this->user->get_tim_group($data['id']);
            $this->template->user('updatetim', $data, array('title' => $data['user']['tim'], 'breadcrumbs' => array('Master', 'Tim', 'Perbarui')));
            return;
        } elseif ($this->input->get_post('new')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_tim($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/tim'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_tim_status($this->input->get_post('status'));
            return;
        }

        $data['wilayahnew'] = $this->db->get('support.wilayah')->result_array();

        $this->db->where('public.roleusers.role', '108ce450-d740-4bcb-9aaa-be9504bf94d8');
        $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
        $data['dalnisnew'] = $this->db->get('public.users')->result_array();

        $this->db->where('public.roleusers.role', '97cc7404-490c-4827-beba-425c85acef5d');
        $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
        $data['ketuanew'] = $this->db->get('public.users')->result_array();

        $this->db->where('public.roleusers.role', '108ce450-d740-4bcb-9aaa-be9504bf94d8');
        $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
        $dalnis = $this->db->get('public.users')->result_array();

        $data['dalnis'] = array();
        foreach ($dalnis as $d) {
            $data['dalnis'][] = array('id' => $d['id'], 'text' => $d['name']);
        }

        $this->db->where('public.roleusers.role', '97cc7404-490c-4827-beba-425c85acef5d');
        $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
        $ketua = $this->db->get('public.users')->result_array();

        $data['ketua'] = array();
        foreach ($ketua as $d) {
            $data['ketua'][] = array('id' => $d['id'], 'text' => $d['name']);
        }

        $wilayah = $this->db->get('support.wilayah')->result_array();

        $data['wilayah'] = array();
        foreach ($wilayah as $d) {
            $data['wilayah'][] = array('id' => $d['kode'], 'text' => $d['wilayah']);
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(support.wilayah.wilayah)', $data['search']);
            $this->db->or_like('lower(support.tim.tim)', $data['search']);
            $this->db->or_like('lower(t1.name)', $data['search']);
            $this->db->or_like('lower(t2.name)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('support.tim.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_tim();
        $this->template->user('tim', $data, array('title' => 'Tim', 'breadcrumbs' => array('Master')));
    }

    //get irban
    public function get_irban()
    {
        $this->db->where('id', '52e68b85-90c1-4910-b9b0-df9a3b3574c4');
        $data['irban'] = $this->db->get('public.users')->result_array();
        foreach ($data['irban'] as $d) {
            $output[] = array('id' => $d['id'], 'text' => $d['name']);
        }
        echo json_encode($output);
    }

    //JENIS e-audit Banyuwangi
    public function jenis()
    {
        $menu = 'd66ab356-6792-451d-8f42-84f07e48b9e7';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.jenis', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_jenis($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/jenis'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_jenis_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.jenis.ket)', $data['search']);
            $this->db->or_like('lower(master.jenis.kode)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.jenis.status', $data['closed']);
        }
        $data['jenis'] = $this->user->get_list_jenis();
        $this->template->user('jenis', $data, array('title' => 'Jenis Audit', 'breadcrumbs' => array('Master')));
    }

    //Pedoman e-audit Banyuwangi
    public function pedoman()
    {
        $menu = '7a2eee70-8883-4c74-9a9b-e820f2249b4b';
        $data['access'] = list_access($menu);
        $jenis = $this->db->get('master.jenis')->result_array();

        $data['jenis'] = array();
        foreach ($jenis as $d) {
            $data['jenis'][] = array('id' => $d['id'], 'text' => $d['kode'] . "/" . $d['ket']);
        }

        $sasaran = $this->db->get('master.sasaran')->result_array();

        $data['sasaran'] = array();
        foreach ($sasaran as $d) {
            $data['sasaran'][] = array('id' => $d['id'], 'text' => $d['kode'] . "/" . $d['ket']);
        }

        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('support.pedoman', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if (isset($_FILES['file']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->insert_pedoman($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/pedoman'));
            }
            $this->db->where('id', $this->input->get_post('e'));
            $this->db->select('file');
            $data['jumlah'] = $this->db->get('support.pedoman')->num_rows();
            $this->template->user('updatepedoman', $data, array('title' => 'Data Baru', 'breadcrumbs' => array('Master', 'Pedoman')));
            return;
        } elseif ($this->input->get_post('e')) {
            check_access($menu, 'c');
            if (isset($_FILES['file']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->upload_pedoman($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/pedoman'));
            }
            $this->db->where('id', $this->input->get_post('e'));
            $this->db->select('file');
            $data['jumlah'] = $this->db->get('support.pedoman')->num_rows();
            $data['user'] = $this->user->get_pedoman_by_id($this->input->get_post('e'));
            $this->template->user('updatepedoman', $data, array('title' => $data['user']['kode'], 'breadcrumbs' => array('Master', 'Pedoman', 'Perbarui')));
            return;
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_pedoman_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.jenis.ket)', $data['search']);
            $this->db->or_like('lower(master.sasaran.ket)', $data['search']);
            $this->db->or_like('lower(support.pedoman.kode)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('support.pedoman.status', $data['closed']);
        }
        $data['pedoman'] = $this->user->get_list_pedoman();
        $this->template->user('pedoman', $data, array('title' => 'Pedoman Audit', 'breadcrumbs' => array('Master')));
    }

    //Detail TAO e-audit Banyuwangi
    public function detail_tao($id = null)
    {
        $this->db->where('id',$id);
        $header=$this->db->get("support.v_tao_header")->row_array();
        if (!isset($header['id'])) {
            redirect(base_url('master/tao'));
        }
        $data['header'] = $header;
        $menu = '2f4e5764-01c0-49f5-b178-555f814a015a';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_detail_tao_status($this->input->get_post('status'));
            return;
        }elseif ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('support.tao_detail', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');
            $data['id_detail'] = $id;
            if (isset($_FILES['file_download']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->insert_detail_tao($id,$this->input->get_post(null));
                $this->session->set_flashdata('status_update', $data['status_update']);
                redirect(base_url('master/detail_tao/' . $data['id_detail']));
            }
            $this->template->user('updatedetailtao', $data, array('title' => 'Input Detail Baru', 'breadcrumbs' => array('Master', 'TAO', 'Audit Program')));
            return;
        } elseif ($this->input->get_post('e')) {
            check_access($menu, 'c');
            $data['id_detail'] = $id;
            $data['id'] = $this->input->get_post('e');
            if (isset($_FILES['file_download']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->update_detail_tao($id,$this->input->get_post(null));
                $this->session->set_flashdata('status_update', $data['status_update']);
                redirect(base_url('master/detail_tao/' . $data['id_detail']));
            }
            $data['user'] = $this->user->get_tao_detail_by_id($this->input->get_post('e'));
            $this->template->user('updatedetailtao', $data, array('title' => 'Perbarui data TAO', 'breadcrumbs' => array('Master', 'TAO', 'Audit Program')));
            return;
        }

        $data['id_tao'] = $id;
        $this->db->order_by('kertas_kerja');
        $this->db->where('id_tao', $data['id_tao']);
        $data['detail'] = $this->db->get('support.tao_detail')->result_array();
        $this->template->user('detailtao', $data, array('title' => "$header[kode] - $header[langkah]", 'breadcrumbs' => array('Master', 'TAO')));
        return;
    }

    //TAO e-audit Banyuwangi
    public function tao()
    {
        $menu = '2f4e5764-01c0-49f5-b178-555f814a015a';
        $data['access'] = list_access($menu);
        $jenis = $this->db->get('master.jenis')->result_array();

        $data['jenis'] = array();
        foreach ($jenis as $d) {
            $data['jenis'][] = array('id' => $d['id'], 'text' => $d['kode'] . " - " . $d['ket']);
        }

        $sasaran = $this->db->get('master.sasaran')->result_array();

        $data['sasaran'] = array();
        foreach ($sasaran as $d) {
            $data['sasaran'][] = array('id' => $d['id'], 'text' => $d['kode'] . " - " . $d['ket']);
        }

        $tujuan = $this->db->get('master.tujuan')->result_array();

        $data['tujuan'] = array();
        foreach ($tujuan as $d) {
            $data['tujuan'][] = array('id' => $d['id'], 'text' => $d['kode'] . " - " . $d['ket']);
        }

        $tahapan = $this->db->get('master.tahapan_audit')->result_array();
        $data['tahapan'] = array();

        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('support.tao', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if ($this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->insert_tao($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/tao'));
            }
            foreach ($tahapan as $d) {
                $data['tahapan'][] = array('id' => $d['kode'], 'text' => $d['tahapan']);
            }
            $this->template->user('updatetao', $data, array('title' => 'Tambah Data TAO', 'breadcrumbs' => array('Master', 'TAO')));
            return;
        } elseif ($this->input->get_post('e')) {
            check_access($menu, 'c');
            $data['id'] = $this->input->get_post('e');
            if ($this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->update_tao($data['id'], $this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/tao'));
            }

            foreach ($tahapan as $d) {
                $data['tahapan'][] = array('id' => $d['kode'], 'text' => $d['tahapan']);
            }
            $data['user'] = $this->user->get_tao_by_id($this->input->get_post('e'));
            $this->template->user('updatetao', $data, array('title' => $data['user']['kode'], 'breadcrumbs' => array('Master', 'TAO', 'Perbarui')));
            return;
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_tao_status($this->input->get_post('status'));
            return;
        }
        foreach ($tahapan as $d) {
            $data['tahapan'][$d['kode']] = $d['tahapan'];
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        $data['thp'] = $this->input->get_post('thp') ? strtolower($this->input->get_post('thp')) : '';
        $this->db->order_by('tahapan,kode');
        if ($data['search'] <> '') {
            $this->db->or_like('lower(supprort.tao.langkah)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('support.tao.status', $data['closed']);
        }
        if ($data['thp'] <> '') {
            $this->db->where('lower(support.tao.tahapan)', $data['thp']);
        }
        $data['tao'] = $this->db->get('support.v_tao_header')->result_array();
        $this->template->user('tao', $data, array('title' => 'TAO', 'breadcrumbs' => array('Master')));
    }

    //KM3 laporan
    public function km3()
    {
        $data = "";
        $this->load->library('pdf');
        $html = $this->load->view('private/pdf/km3', $data, true);
        ini_set('memory_limit', '32M');
        $pdf = $this->pdf->load(array('mode' => 'UTf-8', 'format' => 'A4', 'FontFamily' => 'roboto'));
        $pdf->WriteHTML($html);
        $filename = "img/test" . time() . ".pdf";
        $pdf->Output($filename);
    }

//Aturan e-audit Banyuwangi
    public function aturan()
    {
        $menu = '1541696d-a165-4a06-9181-88472bd61293';
        $data['access'] = list_access($menu);
        $jenis = $this->db->get('master.jenis')->result_array();

        $data['jenis'] = array();
        foreach ($jenis as $d) {
            $data['jenis'][] = array('id' => $d['id'], 'text' => $d['kode'] . "/" . $d['ket']);
        }

        $sasaran = $this->db->get('master.sasaran')->result_array();

        $data['sasaran'] = array();
        foreach ($sasaran as $d) {
            $data['sasaran'][] = array('id' => $d['id'], 'text' => $d['kode'] . "/" . $d['ket']);
        }

        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('support.aturan', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if (isset($_FILES['file']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->insert_aturan($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/aturan'));
            }
            $this->db->where('id', $this->input->get_post('e'));
            $this->db->select('file');
            $data['jumlah'] = $this->db->get('support.aturan')->num_rows();
            $this->template->user('updateaturan', $data, array('title' => 'Data Baru', 'breadcrumbs' => array('Master', 'Aturan')));
            return;
        } elseif ($this->input->get_post('e')) {
            check_access($menu, 'c');
            if (isset($_FILES['file']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->upload_aturan($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/aturan'));
            }
            $this->db->where('id', $this->input->get_post('e'));
            $this->db->select('file');
            $data['jumlah'] = $this->db->get('support.aturan')->num_rows();
            $data['user'] = $this->user->get_aturan_by_id($this->input->get_post('e'));
            $this->template->user('updateaturan', $data, array('title' => $data['user']['kode'], 'breadcrumbs' => array('Master', 'Aturan', 'Perbarui')));
            return;
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_aturan_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.jenis.ket)', $data['search']);
            $this->db->or_like('lower(master.sasaran.ket)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('support.aturan.status', $data['closed']);
        }
        $data['aturan'] = $this->user->get_list_aturan();
        $this->template->user('aturan', $data, array('title' => 'Aturan Audit', 'breadcrumbs' => array('Master')));
    }

    // Nama Satker E-Audit
    public function satker()
    {
        $menu = '8a1dfd71-6c85-412f-91de-970ca913dc60';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.satker', array($this->input->get_post('name') => $value));
            return;
        }
        else if($this->input->get_post('satker')){
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_resiko_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/satker'));
        }
        elseif ($this->input->get_post('kode')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/satker'));
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_satker_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.satker.kode)', $data['search']);
            $this->db->or_like('lower(master.satker.ket)', $data['search']);
            $this->db->or_like('lower(master.satker.nama)', $data['search']);
            $this->db->or_like('lower(master.satker.alamat)', $data['search']);
            $this->db->or_like('lower(master.satker.email)', $data['search']);
        }
        if($this->input->get_post('p')){
            $this->db->where('status','t');
            $this->db->where('resiko','0');
            $data['satker']=$this->db->get('master.satker')->result_array();
            $data['periode']=$this->db->get('periode')->result_array();
            $this->template->user('resiko_satker', $data, array('title' => 'Resiko Satker', 'breadcrumbs' => array('Master')));
            return;
        }
        else if($this->input->get_post('e')){
            $data['satker']=$this->db->get('master.satker')->result_array();
            $data['id_satker']=$this->input->get_post('e');

            $this->db->where('nama',date('Y'));
            $thn=$this->db->get('periode')->row_array();
            $data['per']=$thn['id'];

            $this->db->where('periode',$thn['id']);
            $this->db->where('id_satker',$this->input->get_post('e'));
            $data['edit']=$this->db->get('master.resiko_satker')->row_array();

            $data['periode']=$this->db->get('periode')->result_array();
            $this->template->user('resiko_satker', $data, array('title' => 'Resiko Satker', 'breadcrumbs' => array('Master')));
            return;
        }
        else if($this->input->get_post('cek_periode')){
            $data['satker']=$this->db->get('master.satker')->result_array();
            $data['id_satker']=$this->input->get_post('id_h');

            $data['per']=$this->input->get_post('periode');

            $this->db->where('periode',$this->input->get_post('periode'));
            $this->db->where('id_satker',$this->input->get_post('id_h'));
            $data['edit']=$this->db->get('master.resiko_satker')->row_array();

            $data['periode']=$this->db->get('periode')->result_array();
            $this->template->user('resiko_satker', $data, array('title' => 'Resiko Satker', 'breadcrumbs' => array('Master')));
            return;
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.satker.status', $data['closed']);
        }
        $data['satker'] = $this->user->get_list_satker();
        $this->template->user('satker', $data, array('title' => 'Satker', 'breadcrumbs' => array('Master')));
    }
    public function kode_temuan($page=0){
        $menu = 'c6fabac7-e2b3-4245-a69a-84992efa4bc1';
        $data['access'] = list_access($menu);
        $this->load->model('user');

        $perpage=1000;
        if($this->input->get_post('i')){
            check_access($menu,'c');
            if($this->input->get_post('status')){
                $this->akuntansi->set_coa_status($this->input->get_post('status'));
            }else{
                if($this->input->get_post('parent')){
                    $data['form']=$this->input->get_post(null);
                    $data['status_update']=$this->user->add_kode_temuan($data['form']);
                    if($data['status_update']){
                        $this->session->set_flashdata('status_update', true);
                        redirect(base_url('master/kode_temuan'));
                    }
                }
                $this->db->order_by('k1,k2,k3,k4','asc');
                $this->db->where('level <> ',4);
                $this->db->where('status',true);
                $this->db->where('deleted',false);
                $this->db->select("support.kode_temuan.id,support.kode_temuan.nama,(k1||k2||k3||k4) as kode");
                $data['parent']=$this->db->get('support.kode_temuan')->result_array();
                $this->template->user('addkt',$data,array('title'=>'Tambah Data','breadcrumbs'=>array('Master','Kode Temuan')));
//           echo json_encode($data['status_update']);
//                die();
            }
            return;
        }elseif($this->input->get_post('delete')){
            check_access($menu,'d');
            echo $this->user->set_coa_delete($this->input->get_post('delete'));
            return;
        }
        elseif($this->input->get_post('e')){
            check_access($menu,'u');
            if($this->input->get_post('status')){
                $this->user->set_coa_status($this->input->get_post('status'));
            }else{
                if($this->input->get_post('level')){
                    $data['status_update']=$this->user->update_coa($this->input->get_post(null));
                }
                $this->db->where('id',$this->input->get_post('e'));
                $data['data']=$this->db->get('support.kode_temuan')->row_array();
                $this->template->user('updatecoa',$data,array('title'=>'Perbarui Data','breadcrumbs'=>array('Master','Chart Of Account')));
            }
            return;
        }
        elseif($this->input->get_post('status')){
            check_access($menu,'d');
            echo $this->user->set_kode_delete($this->input->get_post('delete'));
            return;
        }
        $data['search']=$this->input->get_post('search');
        $data['type']=$this->input->get_post('type');
        $data['status']=$this->input->get_post('status')?$this->input->get_post('status'):(isset($_GET['status'])?'':'t');
        if($this->input->get_post('search')){
            $this->db->like('lower(nama)',strtolower($data['search']));
            $this->db->or_where('lower(k1||k2||k3||k4)',strtolower(str_replace(".","",$data['search'])));
        }
        if($data['status']){
            $this->db->where('status',$data['status']);
        }
        $this->db->where('deleted',false);
        $total=$this->db->get('support.kode_temuan')->num_rows();
        $this->db->limit($perpage,$page);
        $this->db->order_by('k1,k2,k3,k4','asc');
        if($this->input->get_post('search')){
            $this->db->like('lower(nama)',strtolower($data['search']));
            $this->db->or_where('lower(k1||k2||k3||k4)',strtolower(str_replace(".","",$data['search'])));
        }
        if($data['status']){
            $this->db->where('status',$data['status']);
        }
        $this->db->where('deleted',false);
        $this->db->select("kode_temuan.*,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
        $data['data']=$this->db->get('support.kode_temuan')->result_array();
        $data['pagination']=$this->template->pagination('master/kode_temuan',$total,$perpage);
        $data['page']=$page;

        $this->template->user('kode_temuan', $data, array('title' => 'Kode Temuan', 'breadcrumbs' => array('Master')));
        return;
    }
    // Periode E-Audit
    public function periode()
    {
        $menu = 'b0014449-4706-48e1-b81d-d758057ef0d0';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if($this->input->get_post('id')=='tambah'){
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_periode($this->input->get_post(null));
            $status_update=explode(',',$data['status_update'][0]);
//            echo $status_update[0];die();
            if ($status_update[0]=='true') {
                $this->session->set_flashdata('status_update', true);
                $this->db->where('nama',$this->input->get_post('nama'));
                $this->db->where('start',$this->input->get_post('sd'));
                $id=$this->db->get('periode')->row_array();
                redirect(base_url("master/periode?e=$id[id]"));
            }
            else{
                $this->session->set_flashdata('status_update', $data['status_update']);
                redirect(base_url('master/periode'));
            }
        }
        else if($this->input->get_post('holidayadd')){
            check_access($menu, 'c');
            $this->db->where('id',$this->input->get_post('id'));
            $this->db->where('holiday',$this->input->get_post('hari'));
            $num=$this->db->get('periode_holiday');
            if($num->num_rows()>0){
                $this->db->where('id',$this->input->get_post('id'));
                $this->db->where('holiday',$this->input->get_post('hari'));
                $this->db->update('periode_holiday',array('ket'=>$this->input->get_post('ket')));
            }
            else{
                $this->db->insert('periode_holiday',array('ket'=>$this->input->get_post('ket'),'id'=>$this->input->get_post('id'),'holiday'=>$this->input->get_post('hari')));
            }
            $data=array('id'=>$this->input->get_post('id'),'start'=>$this->input->get_post('hari'),'end'=>$this->input->get_post('hari'),'ket'=>$this->input->get_post('ket'));
            echo json_encode($data);
            return;
        }else if($this->input->get_post('deletes')){
            check_access($menu, 'c');
            $this->db->where('id',$this->input->get_post('id'));
            $this->db->where('holiday',$this->input->get_post('hari'));
            $this->db->delete('periode_holiday');
            return;
        }else if($this->input->get_post('id')!='tambah'&&$this->input->get_post('id')!=''){
            check_access($menu, 'u');
            $data['status_update'] = $this->user->edit_periode($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/periode'));
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_periode_status($this->input->get_post('status'));
            return;
        }
        elseif ($this->input->get_post('cek')) {
            check_access($menu, 'r');
            $this->db->where('holiday',$this->input->get_post('hari'));
            $this->db->where('id',$this->input->get_post('edit'));
            $holiday=$this->db->get('periode_holiday')->row_array();
            echo json_encode($holiday);
            return;
        }
        else if($this->input->get_post('e')){
            $lh=$this->db->get('periode_holiday')->result_array();
            $data['listholiday']=array();
            foreach($lh as $l){
                $data['listholiday'][]=$l['holiday'];
            }
            $this->db->where('id',$this->input->get_post('e'));
            $data['list_h']=$this->db->get('periode_holiday')->result_array();
            $this->db->where('id',$this->input->get_post('e'));
            $data['edit']=$this->db->get('periode')->row_array();
            $start_date = $data['edit']['start'];
            $end_date = $data['edit']['end'];

            $data['range_date'] = array();
            for ($i = strtotime($start_date); $i <= strtotime($end_date); $i = strtotime('+1 day', $i))
            {
                $data['range_date'][]=$i;
            }
            $data['starts'] = $data['edit']['start']?$data['edit']['start']:date('Y-m-d');
            $data['ends'] =  $data['edit']['end']?$data['edit']['end']:date('Y-m-d');
            $s = explode('-', $data['starts']);
            $d = explode('-', $data['ends']);
            $data['daterange'] = $s[2] . '/' . $s[1] . '/' . $s[0] . ' - ' . $d[2] . '/' . $d[1 ] . '/' . $d[0];

            $this->template->user('addperiode', $data, array('title' => 'Edit Periode', 'breadcrumbs' => array('Master')));
            return;
        }
        $data['starts'] =date('Y').'-'.'01'.'-'.'01';
        $data['ends'] =  date('Y').'-'.'12'.'-'.'31';
        $s = explode('-', $data['starts']);
        $d = explode('-', $data['ends']);
        $data['daterange'] = $s[2] . '/' . $s[1] . '/' . $s[0] . ' - ' . $d[2] . '/' . $d[1 ] . '/' . $d[0];

        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        if ($data['closed'] <> '') {
            $this->db->where('periode.status', $data['closed']);
        }
        $data['periode'] = $this->user->get_list_periode();
        $this->template->user('periode', $data, array('title' => 'Periode', 'breadcrumbs' => array('Master')));
    }

    // Sasaran E-Audit
    public function sasaran()
    {
        $menu = 'd66ab356-6792-451d-8f42-84f07e48b9e7';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.sasaran', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_sasaran($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/sasaran'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_sasaran_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.sasaran.kode)', $data['search']);
            $this->db->or_like('lower(master.sasaran.ket)', $data['search']);

        }
        if ($data['closed'] <> '') {
            $this->db->where('master.sasaran.status', $data['closed']);
        }
        $data['sasaran'] = $this->user->get_list_sasaran();
        $this->template->user('sasaran', $data, array('title' => 'Sasaran Audit', 'breadcrumbs' => array('Master')));
    }

    // Tujuan E-Audit
    public function tujuan()
    {
        $menu = '9312a5c1-7991-4eb1-be6d-05fef767935b';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.tujuan', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_tujuan($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/tujuan'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_tujuan_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('master.tujuan.kode', $data['search']);
            $this->db->or_like('lower(master.tujuan.ket)', $data['search']);

        }
        if ($data['closed'] <> '') {
            $this->db->where('master.tujuan.status', $data['closed']);
        }
        $data['tujuan'] = $this->user->get_list_tujuan();
        $this->template->user('tujuan', $data, array('title' => 'Tujuan Audit', 'breadcrumbs' => array('Master')));
    }

    // Peran E-Audit
    public function peran()
    {
        $menu = 'd66ab356-6792-451d-8f42-84f07e48b9e7';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.peran', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_peran($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/peran'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_peran_status($this->input->get_post('status'));
            return;
        }
        $data['peran'] = $this->user->get_list_peran();
        $this->template->user('peran', $data, array('title' => 'Peran Audit', 'breadcrumbs' => array('Master')));
    }

    // Golongan E-Audit
    public function golongan()
    {
        $menu = 'e89cdedb-32f8-47fd-bbfc-960c894dae01';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.golongan', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_golongan($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/golongan'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_golongan_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.golongan.ket)', $data['search']);

        }
        if ($data['closed'] <> '') {
            $this->db->where('master.golongan.status', $data['closed']);
        }
        $data['golongan'] = $this->user->get_list_golongan();
        $this->template->user('golongan', $data, array('title' => 'Golongan', 'breadcrumbs' => array('Master')));
    }

    public function session()
    {
        $menu = 'd66ab356-6792-451d-8f42-84f07e48b9e7';
        check_access($menu, 'd');
        if ($this->input->get_post('clear')) {
            $this->db->truncate('sessions');
            redirect(base_url('master/session'));
        } elseif ($this->input->get_post('c')) {
            $this->db->where('id', $this->input->get_post('c'));
            $this->db->delete('sessions');
            redirect(base_url('master/session'));
        }
        $this->db->order_by('timestamp');
        $this->db->where('timestamp >= ', (time() - 7200));
        $data['session'] = $this->db->get('sessions')->result_array();
        $this->template->user('session', $data, array('title' => 'Session Active', 'breadcrumbs' => array('Master')));
    }
}
