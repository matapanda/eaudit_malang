<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class import extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        check_access('f10293a6-79c7-41e2-840e-a597e5ace426');
        $this->template->set_view('import');
    }

    public function index()
    {
        redirect(base_url('welcome'));
    }
    public function coa(){
        if(isset($_FILES['file'])){
            $this->load->library('PHPExcel');
            $config['upload_path'] = './img/';
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload("file")) {
                $data['status_update'] = 'error';
                $data['status_message'] =  $this->upload->display_errors('', '');
                echo json_encode($data);
            } else {
                $file = $this->upload->data();
                $full_path=$file["full_path"];
                $objPHPExcel = PHPExcel_IOFactory::load($full_path);
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                    if ($row == 1) {
                        if(strlen($data_value)>0){
                            $data_value=strtolower($data_value);
                            $header[$column] = trim($data_value);
                        }
                    } else {
                        if(isset($header[$column]) && strlen($header[$column])>0){
                            $arr_data[$row][$header[$column]] = trim($data_value);
                        }
                    }
                }
                unlink($full_path);
                $this->db->trans_begin();
                $jml=0;
                foreach ($arr_data as $v){
                    if(isset($v['k4']) && strlen($v['k4'])>0){
                        $v['level']=4;
                    }elseif(isset($v['k3']) && strlen($v['k3'])>0){
                        $v['level']=3;
                    }elseif(isset($v['k2']) && strlen($v['k2'])>0){
                        $v['level']=2;
                    }else{
                        $v['level']=1;
                    }
                    $user=$this->session->userdata('user');
                    $v['created_at']=date('Y-m-d H:i:s+07');
                    $v['created_by']=$user['id'];
                    if(isset($v['k1'])) {
                        $this->load->library('uuid');
                        $v['id'] = $this->uuid->v4();
                        $this->db->insert('akuntansi.coa',$v);
                        $jml++;
                    } else {
                        continue;
                    }
                }
                if ($this->db->trans_status()){
                    $this->db->trans_commit();
                    $data['status_update'] = array('success',"$jml coa inserted :)");
                }else{
                    $data['status_update'] = array('error',"Something wrong!");
                }
            }
        }
        $this->template->user('coa',@$data,array('title'=>'COA','breadcrumbs'=>array('Master','Import')));
    }
}
