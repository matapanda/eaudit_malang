--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.14
-- Dumped by pg_dump version 9.5.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: interpro; Type: SCHEMA; Schema: -; Owner: eauditpr_user
--

CREATE SCHEMA interpro;


ALTER SCHEMA interpro OWNER TO postgres;

--
-- Name: master; Type: SCHEMA; Schema: -; Owner: eauditpr_user
--

CREATE SCHEMA master;


ALTER SCHEMA master OWNER TO postgres;

--
-- Name: support; Type: SCHEMA; Schema: -; Owner: eauditpr_user
--

CREATE SCHEMA support;


ALTER SCHEMA support OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: NewProc(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: interpro; Owner: eauditpr_user
--

CREATE FUNCTION interpro."NewProc"(_start timestamp without time zone, _end timestamp without time zone) RETURNS TABLE(id uuid, spt_date date, judul character varying, no_laporan character varying, thn_laporan character varying, status_tl boolean, proses character varying)
    LANGUAGE plpgsql
    AS $$
	declare "_curs_rencana" REFCURSOR;
	declare "_cur_rencana" RECORD;
	declare "_curs_tl" REFCURSOR;
	declare "_cur_tl" RECORD;
BEGIN 
	CREATE TEMP TABLE _kinerja
	("id" uuid, "spt_date" date, "judul" varchar, "no_laporan" varchar, "thn_laporan" varchar, "status_tl" bool, "proses" varchar) on commit drop;
	OPEN _curs_rencana FOR 
	SELECT r.id,r.spt_date,r.judul,r.no_laporan,r.thn_laporan,r.status_tl, t.proses
	from interpro.perencanaan r left join interpro.tindak_lanjut t on t.id_rencana=r.id
		WHERE r.status=true 
		AND r.proses=4
		AND r.created_at>= _start
		AND r.created_at<= _end
		ORDER BY r.created_at desc;
	LOOP
		FETCH _curs_rencana INTO _cur_rencana;
		EXIT WHEN NOT FOUND;
		INSERT INTO _kinerja VALUES (_cur_rencana.id, _cur_rencana.spt_date, _cur_rencana.judul, _cur_rencana.no_laporan, _cur_rencana.thn_laporan, _cur_rencana.status_tl, _cur_rencana.proses);
	END LOOP;
	CLOSE _curs_rencana;
		
OPEN _curs_tl FOR 
	SELECT interpro.tindak_lanjut.id,skpd,no_lhp,tgl_lhp,status,thn_lhp,interpro.tindak_lanjut.proses
	from interpro.tindak_lanjut
		WHERE pelaksana is not null 
		AND created_at>= _start
		AND created_at<= _end
		ORDER BY created_at desc;
	LOOP
		FETCH _curs_tl INTO _cur_tl;
		EXIT WHEN NOT FOUND;
		INSERT INTO _kinerja VALUES (_cur_tl.id, _cur_tl.tgl_lhp, _cur_tl.skpd, _cur_tl.no_lhp, _cur_tl.thn_lhp, _cur_tl.status, _cur_tl.proses);
	END LOOP;
	CLOSE _curs_tl;
	RETURN QUERY SELECT * FROM _kinerja;
END;
$$;


ALTER FUNCTION interpro."NewProc"(_start timestamp without time zone, _end timestamp without time zone) OWNER TO postgres;

--
-- Name: _navicat_temp_stored_proc(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: interpro; Owner: eauditpr_user
--

CREATE FUNCTION interpro._navicat_temp_stored_proc(_start timestamp without time zone, _end timestamp without time zone) RETURNS TABLE(id uuid, spt_date date, judul character varying, no_laporan character varying, thn_laporan character varying, status_tl boolean, proses character varying)
    LANGUAGE plpgsql
    AS $$
	declare "_curs_rencana" REFCURSOR;
	declare "_cur_rencana" RECORD;
	declare "_curs_tl" REFCURSOR;
	declare "_cur_tl" RECORD;
BEGIN 
	CREATE TEMP TABLE _kinerja
	("id" uuid, "spt_date" date, "judul" varchar, "no_laporan" varchar, "thn_laporan" varchar, "status_tl" bool, "proses" varchar) on commit drop;
	OPEN _curs_rencana FOR 
	SELECT r.id,r.spt_date,r.judul,r.no_laporan,r.thn_laporan,r.status_tl, t.proses
	from interpro.perencanaan r left join interpro.tindak_lanjut t on t.id_rencana=r.id
		WHERE r.status=true 
		AND r.proses=4
		AND r.created_at>= _start
		AND r.created_at<= _end
		ORDER BY r.created_at desc;
	LOOP
		FETCH _curs_rencana INTO _cur_rencana;
		EXIT WHEN NOT FOUND;
		INSERT INTO _kinerja VALUES (_cur_rencana.id, _cur_rencana.spt_date, _cur_rencana.judul, _cur_rencana.no_laporan, _cur_rencana.thn_laporan, _cur_rencana.status_tl, _cur_rencana.proses);
	END LOOP;
	CLOSE _curs_rencana;
		
OPEN _curs_tl FOR 
	SELECT interpro.tindak_lanjut.id,skpd,no_lhp,tgl_lhp,status,thn_lhp,interpro.tindak_lanjut.proses
	from interpro.tindak_lanjut
		WHERE pelaksana is not null 
		AND created_at>= _start
		AND created_at<= _end
		ORDER BY created_at desc;
	LOOP
		FETCH _curs_tl INTO _cur_tl;
		EXIT WHEN NOT FOUND;
		INSERT INTO _kinerja VALUES (_cur_tl.id, _cur_tl.tgl_lhp, _cur_tl.skpd, _cur_tl.no_lhp, _cur_tl.thn_lhp, _cur_tl.status, _cur_tl.proses);
	END LOOP;
	CLOSE _curs_tl;
	RETURN QUERY SELECT * FROM _kinerja;
END;
$$;


ALTER FUNCTION interpro._navicat_temp_stored_proc(_start timestamp without time zone, _end timestamp without time zone) OWNER TO postgres;

--
-- Name: daftar_tim_dan_jabatan(uuid); Type: FUNCTION; Schema: interpro; Owner: eauditpr_user
--

CREATE FUNCTION interpro.daftar_tim_dan_jabatan(_id uuid) RETURNS TABLE(id uuid, nip character varying, nama character varying, jabatan character varying)
    LANGUAGE plpgsql
    AS $$
	declare "_curs_tim" REFCURSOR;
	declare "_cur_tim" RECORD;
	declare "_curs_agt" REFCURSOR;
	declare "_cur_agt" RECORD;
	declare "_uuid" uuid;
	declare "_nip" varchar;
	declare "_nama" varchar;
BEGIN 
	CREATE TEMP TABLE _temp ("id" uuid, "nip" varchar, "nama" varchar, "jabatan" varchar) on commit drop;
	OPEN _curs_tim FOR SELECT pt.* FROM interpro.perencanaan_tim pt WHERE pt.id=_id;
	FETCH _curs_tim INTO _cur_tim;
	SELECT u1.id,u1.nip,u1.name into _uuid,_nip,_nama FROM public.users u1 WHERE u1.id in (SELECT p.app_sv2 FROM interpro.perencanaan p WHERE p.id=_id);
	INSERT INTO _temp VALUES (_uuid,_nip,_nama,'Penanggung Jawab');
	SELECT u2.id,u2.nip,u2.name into _uuid,_nip,_nama FROM public.users u2 WHERE u2.id = _cur_tim.irban;
	INSERT INTO _temp VALUES (_uuid,_nip,_nama,'Pembantu Penanggung Jawab');
	SELECT u3.id,u3.nip,u3.name into _uuid,_nip,_nama FROM public.users u3 WHERE u3.id = _cur_tim.dalnis;
	INSERT INTO _temp VALUES (_uuid,_nip,_nama,'Pengendali Teknis');
	SELECT u4.id,u4.nip,u4.name into _uuid,_nip,_nama FROM public.users u4 WHERE u4.id = _cur_tim.ketua;
	INSERT INTO _temp VALUES (_uuid,_nip,_nama,'Ketua Tim');
	OPEN _curs_agt FOR SELECT u.id,u.nip,u.name FROM public.users u WHERE u.id = ANY(_cur_tim.tim);
	LOOP
		FETCH _curs_agt INTO _cur_agt;
		EXIT WHEN NOT FOUND;
		INSERT INTO _temp VALUES (_cur_agt.id,_cur_agt.nip,_cur_agt.name,'Anggota Tim');
	END LOOP;
	CLOSE _curs_tim;
	CLOSE _curs_agt;
	RETURN QUERY SELECT * FROM _temp;
END;
$$;


ALTER FUNCTION interpro.daftar_tim_dan_jabatan(_id uuid) OWNER TO postgres;

--
-- Name: insert_perencanaan(); Type: FUNCTION; Schema: interpro; Owner: eauditpr_user
--

CREATE FUNCTION interpro.insert_perencanaan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
		DECLARE _irban uuid;
		DECLARE _dalnis uuid;
		DECLARE _ketua uuid;
    DECLARE _tim uuid[];
		BEGIN
			IF (NEW.pkpt_no='')
			THEN
				NEW.pkpt_no:=null;
			END IF;
			IF (NEW.draft<>TRUE)
			THEN
				NEW.app_sv2:=(select id from public.inspektur);
				_tim:=(select array_agg(dt.id) from (select replace(json_array_elements(tim::json)::VARCHAR,'"','')::uuid as id from interpro.perencanaan WHERE id=NEW.id) dt);
				_irban:=(SELECT w.irban FROM support.wilayah w WHERE w.irban = ANY(_tim) LIMIT 1);
				_tim:=(SELECT array_remove(_tim,_irban));
				_dalnis:=(SELECT t.dalnis FROM support.tim t WHERE t.dalnis = ANY(_tim) LIMIT 1);
				_tim:=(SELECT array_remove(_tim,_dalnis));
				_ketua:=(SELECT t.ketua FROM support.tim t WHERE t.ketua = ANY(_tim) LIMIT 1);
				_tim:=(SELECT array_remove(_tim,_ketua));
				INSERT into interpro.perencanaan_tim VALUES (NEW.id,_irban,_dalnis,_ketua,_tim);
			END IF;
		RETURN NEW;
    END;
$$;


ALTER FUNCTION interpro.insert_perencanaan() OWNER TO postgres;

--
-- Name: insert_perencanaan_detail(); Type: FUNCTION; Schema: interpro; Owner: eauditpr_user
--

CREATE FUNCTION interpro.insert_perencanaan_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	DECLARE _langkah varchar;
	DECLARE _spt varchar;
    BEGIN
			IF NOT EXISTS(SELECT "id" from interpro.perencanaan WHERE "id"=NEW."id" and draft=true)
			THEN
					_spt:=(select spt_no from interpro.perencanaan WHERE id=NEW.id);
					_langkah:=(select langkah from support.tao_detail WHERE id=NEW.tao);
					INSERT into support.notif VALUES (uuid_generate_v4(),NEW.rencana_by,'rencana','Anda ditugaskan pada '||_spt||' - '||_langkah,NEW.id,NEW.tao,NOW(),NULL);
			END IF;
		RETURN NEW;
    END;
$$;


ALTER FUNCTION interpro.insert_perencanaan_detail() OWNER TO postgres;

--
-- Name: kinerja(character varying); Type: FUNCTION; Schema: interpro; Owner: eaudit
--

CREATE FUNCTION interpro.kinerja(_tahun character varying) RETURNS TABLE(id uuid, nip character varying, nama character varying, jumlah numeric, realisasi numeric, selesai numeric)
    LANGUAGE plpgsql
    AS $$
	declare "_curs_tim" REFCURSOR;
	declare "_cur_tim" RECORD;
	declare "_jumlah" NUMERIC;
	declare "_realisasi" NUMERIC;
	declare "_selesai" NUMERIC;
BEGIN 
	CREATE TEMP TABLE _kinerja
	("id" uuid, "nip" varchar, "nama" varchar, jumlah NUMERIC, realisasi NUMERIC, selesai NUMERIC) on commit drop;
	OPEN _curs_tim FOR 
	SELECT u.id,u.nip,u.name 
	from public.users u
		WHERE u.status=true 
		AND u.username is not null
		ORDER BY u.name,u.nip;
	LOOP
		FETCH _curs_tim INTO _cur_tim;
		EXIT WHEN NOT FOUND;
			_jumlah:=(SELECT COUNT(*) FROM interpro.perencanaan_detail pd 
			JOIN interpro.perencanaan p ON p.id=pd.id AND p.pkpt_no!=''
			JOIN master.pkpt ON (pkpt_no)::uuid=pkpt.id AND SUBSTRING(pkpt.no,0,5)=_tahun
			WHERE pd.rencana_by=_cur_tim.id);
			_realisasi:=(SELECT COUNT(*) FROM interpro.perencanaan_detail pd 
			JOIN interpro.perencanaan p ON p.id=pd.id AND p.pkpt_no!=''
			JOIN master.pkpt ON (pkpt_no)::uuid=pkpt.id AND SUBSTRING(pkpt.no,0,5)=_tahun
			WHERE pd.rencana_by=_cur_tim.id AND pd.realisasi_by is not null);
			_selesai:=(SELECT COUNT(*) FROM interpro.perencanaan_detail pd 
			JOIN interpro.perencanaan p ON p.id=pd.id AND p.pkpt_no!=''
			JOIN master.pkpt ON (pkpt_no)::uuid=pkpt.id AND SUBSTRING(pkpt.no,0,5)=_tahun
			WHERE pd.rencana_by=_cur_tim.id AND pd.app_sv3_status=true);
		INSERT INTO _kinerja VALUES (_cur_tim.id,_cur_tim.nip,_cur_tim.name,_jumlah,_realisasi,_selesai);
	END LOOP;
	CLOSE _curs_tim;
	RETURN QUERY SELECT * FROM _kinerja;
END;
$$;


ALTER FUNCTION interpro.kinerja(_tahun character varying) OWNER TO postgres;

--
-- Name: range_tahapan(uuid, character varying); Type: FUNCTION; Schema: interpro; Owner: eauditpr_user
--

CREATE FUNCTION interpro.range_tahapan(_id uuid, _tahapan character varying) RETURNS TABLE(awal date, akhir date)
    LANGUAGE plpgsql
    AS $$
BEGIN
	if(_tahapan='0')
	THEN
		RETURN QUERY SELECT MIN(d.realisasi_date),MAX(d.realisasi_date)
		FROM interpro.perencanaan_detail d
		JOIN support.tao_detail td ON td."id"=d."tao"
		JOIN support.tao t ON t."id"=td."id_tao"
		WHERE d."id"=_id;
	ELSE
		RETURN QUERY SELECT MIN(d.realisasi_date),MAX(d.realisasi_date)
		FROM interpro.perencanaan_detail d
		JOIN support.tao_detail td ON td."id"=d."tao"
		JOIN support.tao t ON t."id"=td."id_tao" AND t.tahapan=_tahapan
		WHERE d."id"=_id;
	END IF;
END;
$$;


ALTER FUNCTION interpro.range_tahapan(_id uuid, _tahapan character varying) OWNER TO postgres;

--
-- Name: rekap_tl(character varying); Type: FUNCTION; Schema: interpro; Owner: eauditpr_user
--

CREATE FUNCTION interpro.rekap_tl(_tahun character varying) RETURNS TABLE(tahun character varying, id_kode uuid, kode character varying, temuan character varying, jml_1 numeric, saran_1 numeric, nilai_1 numeric, jml_2 numeric, saran_2 numeric, nilai_2 numeric, jml_3 numeric, saran_3 numeric, nilai_3 numeric, jml_4 numeric, saran_4 numeric, nilai_4 numeric, keterangan character varying)
    LANGUAGE plpgsql
    AS $$
	declare "_curs_kt" REFCURSOR;
	declare "_cur_kt" RECORD;
	declare "_curs_kr" REFCURSOR;
	declare "_cur_kr" RECORD;
	declare "_jml1" numeric;
	declare "_nilai1" numeric;
	declare "_jml2" numeric;
	declare "_nilai2" numeric;
	declare "_jml3" numeric;
	declare "_nilai3" numeric;
	declare "_jml4" numeric;
	declare "_nilai4" numeric;
	declare "_total" numeric;
BEGIN 
_total:=0;
	CREATE TEMP TABLE _rekap
	("tahun" varchar,"id_kode" uuid,"kode" varchar, "temuan" varchar, "jml_1" numeric, "saran_1" numeric, "nilai_1" numeric, "jml_2" numeric, "saran_2" numeric, "nilai_2" numeric, "jml_3" numeric, "saran_3" numeric, "nilai_3" numeric, "jml_4" numeric, "saran_4" numeric, "nilai_4" numeric, "keterangan" varchar) on commit drop;
	OPEN _curs_kt FOR 
	SELECT kt.id,(kt.k1||'.'||kt.k2||'.'||kt.k3||'.'||kt.k4) as kode_t,kt.nama as temuan
	from support.kode_temuan kt
		left JOIN interpro.tindak_lanjutd tld 
		ON kt.id=tld.kode_temuan
		left JOIN interpro.tindak_lanjut tl 
		ON tl.id=tld.id
		WHERE (EXTRACT(year FROM (tl.created_at)::date))::varchar = _tahun 
		ORDER BY kode_t;
	LOOP
		FETCH _curs_kt INTO _cur_kt;
		EXIT WHEN NOT FOUND;
			_jml1:=(SELECT count(tld.hasil_rekom) from support.kode_temuan kt join interpro.tindak_lanjutd tld ON kt.id=tld.kode_temuan WHERE kt.id=_cur_kt.id and tld.hasil_rekom=1 );
			_nilai1:=(SELECT sum(tld.nilai_rekom) from support.kode_temuan kt join interpro.tindak_lanjutd tld ON kt.id=tld.kode_temuan WHERE kt.id=_cur_kt.id and tld.hasil_rekom=1 );
			_jml2:=(SELECT count(tld.hasil_rekom) from support.kode_temuan kt join interpro.tindak_lanjutd tld ON kt.id=tld.kode_temuan WHERE kt.id=_cur_kt.id and tld.hasil_rekom=2 );
			_nilai2:=(SELECT sum(tld.nilai_rekom) from support.kode_temuan kt join interpro.tindak_lanjutd tld ON kt.id=tld.kode_temuan WHERE kt.id=_cur_kt.id and tld.hasil_rekom=2 );
			_jml3:=(SELECT count(tld.hasil_rekom) from support.kode_temuan kt join interpro.tindak_lanjutd tld ON kt.id=tld.kode_temuan WHERE kt.id=_cur_kt.id and tld.hasil_rekom=3 );
			_nilai4:=(SELECT sum(tld.nilai_rekom) from support.kode_temuan kt join interpro.tindak_lanjutd tld ON kt.id=tld.kode_temuan WHERE kt.id=_cur_kt.id and tld.hasil_rekom=3 );
			_jml4:=(SELECT count(tld.hasil_rekom) from support.kode_temuan kt join interpro.tindak_lanjutd tld ON kt.id=tld.kode_temuan WHERE kt.id=_cur_kt.id and tld.hasil_rekom=4 );
			_nilai4:=(SELECT sum(tld.nilai_rekom) from support.kode_temuan kt join interpro.tindak_lanjutd tld ON kt.id=tld.kode_temuan WHERE kt.id=_cur_kt.id and tld.hasil_rekom=4 );
			_total:=_total+1;
			INSERT INTO _rekap VALUES (_tahun,_cur_kt.id,_cur_kt.kode_t,_cur_kt.temuan,_jml1,_jml1,_nilai1,_jml2,_jml2,_nilai2,_jml3,_jml3,_nilai3,_jml4,_jml4,_nilai4,'-');
	END LOOP;
	CLOSE _curs_kt;
	RETURN QUERY SELECT * FROM _rekap;
END;
$$;


ALTER FUNCTION interpro.rekap_tl(_tahun character varying) OWNER TO postgres;

--
-- Name: tindak_lanjut_v(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: interpro; Owner: eauditpr_user
--

CREATE FUNCTION interpro.tindak_lanjut_v(_start timestamp without time zone, _end timestamp without time zone) RETURNS TABLE(id uuid, spt_date date, judul character varying, no_laporan character varying, thn_laporan character varying, status_tl boolean)
    LANGUAGE plpgsql
    AS $$
	declare "_curs_rencana" REFCURSOR;
	declare "_cur_rencana" RECORD;
	declare "_curs_tl" REFCURSOR;
	declare "_cur_tl" RECORD;
BEGIN 
	CREATE TEMP TABLE _kinerja
	("id" uuid, "spt_date" date, "judul" varchar, "no_laporan" varchar, "thn_laporan" varchar, "status_tl" bool) on commit drop;
	OPEN _curs_rencana FOR 
	SELECT r.id,r.spt_date,r.judul,r.no_laporan,r.thn_laporan,r.status_tl 
	from interpro.perencanaan r
		WHERE r.status=true 
		AND r.proses=4
		AND r.created_at>= _start
		AND r.created_at<= _end
		ORDER BY r.created_at desc;
	LOOP
		FETCH _curs_rencana INTO _cur_rencana;
		EXIT WHEN NOT FOUND;
		INSERT INTO _kinerja VALUES (_cur_rencana.id, _cur_rencana.spt_date, _cur_rencana.judul, _cur_rencana.no_laporan, _cur_rencana.thn_laporan, _cur_rencana.status_tl);
	END LOOP;
	CLOSE _curs_rencana;
		
OPEN _curs_tl FOR 
	SELECT interpro.tindak_lanjut.id,skpd,no_lhp,tgl_lhp,status,thn_lhp
	from interpro.tindak_lanjut
		WHERE pelaksana is not null 
		AND created_at>= _start
		AND created_at<= _end
		ORDER BY created_at desc;
	LOOP
		FETCH _curs_tl INTO _cur_tl;
		EXIT WHEN NOT FOUND;
		INSERT INTO _kinerja VALUES (_cur_tl.id, _cur_tl.tgl_lhp, _cur_tl.skpd, _cur_tl.no_lhp, _cur_tl.thn_lhp, _cur_tl.status);
	END LOOP;
	CLOSE _curs_tl;
	RETURN QUERY SELECT * FROM _kinerja;
END;
$$;


ALTER FUNCTION interpro.tindak_lanjut_v(_start timestamp without time zone, _end timestamp without time zone) OWNER TO postgres;

--
-- Name: tindak_lanjut_v2(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: interpro; Owner: eauditpr_user
--

CREATE FUNCTION interpro.tindak_lanjut_v2(_start timestamp without time zone, _end timestamp without time zone) RETURNS TABLE(id uuid, spt_date date, judul character varying, no_laporan character varying, thn_laporan character varying, status_tl boolean)
    LANGUAGE plpgsql
    AS $$
	declare "_curs_rencana" REFCURSOR;
	declare "_cur_rencana" RECORD;
	declare "_curs_tl" REFCURSOR;
	declare "_cur_tl" RECORD;
BEGIN 
	CREATE TEMP TABLE _kinerja
	("id" uuid, "spt_date" date, "judul" varchar, "no_laporan" varchar, "thn_laporan" varchar, "status_tl" bool) on commit drop;
	OPEN _curs_rencana FOR 
	SELECT r.id,r.spt_date,r.judul,r.no_laporan,r.thn_laporan,r.status_tl 
	from interpro.perencanaan r
		WHERE r.status=true 
		AND r.proses=4
		AND r.created_at>= _start
		AND r.created_at<= _end
		ORDER BY r.created_at desc;
	LOOP
		FETCH _curs_rencana INTO _cur_rencana;
		EXIT WHEN NOT FOUND;
		INSERT INTO _kinerja VALUES (_cur_rencana.id, _cur_rencana.spt_date, _cur_rencana.judul, _cur_rencana.no_laporan, _cur_rencana.thn_laporan, _cur_rencana.status_tl);
	END LOOP;
	CLOSE _curs_rencana;
		
OPEN _curs_tl FOR 
	SELECT interpro.tindak_lanjut.id,skpd,no_lhp,tgl_lhp,status,thn_lhp
	from interpro.tindak_lanjut
		WHERE pelaksana is not null 
		AND created_at>= _start
		AND created_at<= _end
		ORDER BY created_at desc;
	LOOP
		FETCH _curs_tl INTO _cur_tl;
		EXIT WHEN NOT FOUND;
		INSERT INTO _kinerja VALUES (_cur_tl.id, _cur_tl.tgl_lhp, _cur_tl.skpd, _cur_tl.no_lhp, _cur_tl.thn_lhp, _cur_tl.status);
	END LOOP;
	CLOSE _curs_tl;
	RETURN QUERY SELECT * FROM _kinerja;
END;
$$;


ALTER FUNCTION interpro.tindak_lanjut_v2(_start timestamp without time zone, _end timestamp without time zone) OWNER TO postgres;

--
-- Name: tindak_lanjut_v3(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: interpro; Owner: eauditpr_user
--

CREATE FUNCTION interpro.tindak_lanjut_v3(_start timestamp without time zone, _end timestamp without time zone) RETURNS TABLE(id uuid, spt_date date, judul character varying, no_laporan character varying, thn_laporan character varying, status_tl boolean, proses character varying)
    LANGUAGE plpgsql
    AS $$
	declare "_curs_rencana" REFCURSOR;
	declare "_cur_rencana" RECORD;
	declare "_curs_tl" REFCURSOR;
	declare "_cur_tl" RECORD;
BEGIN 
	CREATE TEMP TABLE _kinerja
	("id" uuid, "spt_date" date, "judul" varchar, "no_laporan" varchar, "thn_laporan" varchar, "status_tl" bool, "proses" varchar) on commit drop;
	OPEN _curs_rencana FOR 
	SELECT r.id,r.spt_date,r.judul,r.no_laporan,r.thn_laporan,r.status_tl, t.proses
	from interpro.perencanaan r left join interpro.tindak_lanjut t on t.id_rencana=r.id
		WHERE r.status=true 
		AND r.proses=4
		AND r.created_at>= _start
		AND r.created_at<= _end
		ORDER BY r.created_at desc;
	LOOP
		FETCH _curs_rencana INTO _cur_rencana;
		EXIT WHEN NOT FOUND;
		INSERT INTO _kinerja VALUES (_cur_rencana.id, _cur_rencana.spt_date, _cur_rencana.judul, _cur_rencana.no_laporan, _cur_rencana.thn_laporan, _cur_rencana.status_tl, _cur_rencana.proses);
	END LOOP;
	CLOSE _curs_rencana;
		
OPEN _curs_tl FOR 
	SELECT interpro.tindak_lanjut.id,skpd,no_lhp,tgl_lhp,status,thn_lhp,interpro.tindak_lanjut.proses
	from interpro.tindak_lanjut
		WHERE pelaksana is not null 
		AND created_at>= _start
		AND created_at<= _end
		ORDER BY created_at desc;
	LOOP
		FETCH _curs_tl INTO _cur_tl;
		EXIT WHEN NOT FOUND;
		INSERT INTO _kinerja VALUES (_cur_tl.id, _cur_tl.tgl_lhp, _cur_tl.skpd, _cur_tl.no_lhp, _cur_tl.thn_lhp, _cur_tl.status, _cur_tl.proses);
	END LOOP;
	CLOSE _curs_tl;
	RETURN QUERY SELECT * FROM _kinerja;
END;
$$;


ALTER FUNCTION interpro.tindak_lanjut_v3(_start timestamp without time zone, _end timestamp without time zone) OWNER TO postgres;

--
-- Name: tindak_lanjut_v4(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: interpro; Owner: eauditpr_user
--

CREATE FUNCTION interpro.tindak_lanjut_v4(_start timestamp without time zone, _end timestamp without time zone) RETURNS TABLE(id uuid, spt_date date, judul character varying, no_laporan character varying, thn_laporan character varying, status_tl boolean, proses character varying, lap_akir character varying)
    LANGUAGE plpgsql
    AS $$
	declare "_curs_rencana" REFCURSOR;
	declare "_cur_rencana" RECORD;
	declare "_curs_tl" REFCURSOR;
	declare "_cur_tl" RECORD;
BEGIN 
	CREATE TEMP TABLE _kinerja
	("id" uuid, "spt_date" date, "judul" varchar, "no_laporan" varchar, "thn_laporan" varchar, "status_tl" bool, "proses" varchar, "lap_akir" varchar) on commit drop;
	OPEN _curs_rencana FOR 
	SELECT r.id,r.spt_date,r.judul,r.no_laporan,r.thn_laporan,r.status_tl, t.proses,t.lap_akir
	from interpro.perencanaan r left join interpro.tindak_lanjut t on t.id_rencana=r.id
		WHERE r.status=true 
		AND r.proses=4
		AND r.created_at>= _start
		AND r.created_at<= _end
		ORDER BY r.created_at desc;
	LOOP
		FETCH _curs_rencana INTO _cur_rencana;
		EXIT WHEN NOT FOUND;
		INSERT INTO _kinerja VALUES (_cur_rencana.id, _cur_rencana.spt_date, _cur_rencana.judul, _cur_rencana.no_laporan, _cur_rencana.thn_laporan, _cur_rencana.status_tl, _cur_rencana.proses, _cur_rencana.lap_akir);
	END LOOP;
	CLOSE _curs_rencana;
		
OPEN _curs_tl FOR 
	SELECT interpro.tindak_lanjut.id,skpd,no_lhp,tgl_lhp,status,thn_lhp,interpro.tindak_lanjut.proses,interpro.tindak_lanjut.lap_akir
	from interpro.tindak_lanjut
		WHERE pelaksana is not null 
		AND created_at>= _start
		AND created_at<= _end
		ORDER BY created_at desc;
	LOOP
		FETCH _curs_tl INTO _cur_tl;
		EXIT WHEN NOT FOUND;
		INSERT INTO _kinerja VALUES (_cur_tl.id, _cur_tl.tgl_lhp, _cur_tl.skpd, _cur_tl.no_lhp, _cur_tl.thn_lhp, _cur_tl.status, _cur_tl.proses, _cur_tl.lap_akir);
	END LOOP;
	CLOSE _curs_tl;
	RETURN QUERY SELECT * FROM _kinerja;
END;
$$;


ALTER FUNCTION interpro.tindak_lanjut_v4(_start timestamp without time zone, _end timestamp without time zone) OWNER TO postgres;

--
-- Name: update_perencanaan(); Type: FUNCTION; Schema: interpro; Owner: eauditpr_user
--

CREATE FUNCTION interpro.update_perencanaan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
		DECLARE _irban uuid;
		DECLARE _dalnis uuid;
		DECLARE _ketua uuid;
    DECLARE _tim uuid[];
		BEGIN
			IF (NEW.pkpt_no='')
			THEN
				NEW.pkpt_no:=null;
			END IF;
			IF (NEW.tim<>OLD.tim)
			THEN
				_tim:=(select array_agg(dt.id) from (select replace(json_array_elements(tim::json)::VARCHAR,'"','')::uuid as id from interpro.perencanaan WHERE id=NEW.id) dt);
				_irban:=(SELECT w.irban FROM support.wilayah w WHERE w.irban = ANY(_tim) LIMIT 1);
				_tim:=(SELECT array_remove(_tim,_irban));
				_dalnis:=(SELECT t.dalnis FROM support.tim t WHERE t.dalnis = ANY(_tim) LIMIT 1);
				_tim:=(SELECT array_remove(_tim,_dalnis));
				_ketua:=(SELECT t.ketua FROM support.tim t WHERE t.ketua = ANY(_tim) LIMIT 1);
				_tim:=(SELECT array_remove(_tim,_ketua));
				DELETE FROM interpro.perencanaan_tim WHERE id=NEW.id;
				INSERT into interpro.perencanaan_tim VALUES (NEW.id,_irban,_dalnis,_ketua,_tim);
			END IF;
		RETURN NEW;
    END;
$$;


ALTER FUNCTION interpro.update_perencanaan() OWNER TO postgres;

--
-- Name: update_perencanaan_detail(); Type: FUNCTION; Schema: interpro; Owner: eauditpr_user
--

CREATE FUNCTION interpro.update_perencanaan_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE _spt VARCHAR;
    DECLARE _langkah VARCHAR;
			BEGIN
			IF NOT EXISTS(SELECT "id" from interpro.perencanaan_detail WHERE "id"=NEW."id" and progres<4)
			THEN
				update interpro.perencanaan set proses=4, modified_by=NEW.modified_by,modified_at=NEW.modified_at WHERE "id"=NEW."id" AND closed=false;
			END IF;
			IF NOT EXISTS(SELECT "id" from interpro.perencanaan WHERE "id"=NEW."id" and draft=true)
			THEN 
 		 		IF NEW.rencana_by<>OLD.rencana_by
				THEN
					DELETE FROM support.notif WHERE type='rencana' AND reff_id1=NEW.id AND reff_id2=NEW.tao;
					_spt:=(select spt_no from interpro.perencanaan WHERE id=NEW.id);
					_langkah:=(select langkah from support.tao_detail WHERE id=NEW.tao);
					INSERT into support.notif VALUES (uuid_generate_v4(),NEW.rencana_by,'rencana','Anda ditugaskan pada '||_spt||' - '||_langkah,NEW.id,NEW.tao,NOW(),NULL);
				END IF;
			END IF;
		RETURN NEW;
    END;
$$;


ALTER FUNCTION interpro.update_perencanaan_detail() OWNER TO postgres;

--
-- Name: insert_roleusers(); Type: FUNCTION; Schema: public; Owner: eauditpr_user
--

CREATE FUNCTION public.insert_roleusers() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
		BEGIN
			IF (NEW.role='6a45efd2-7007-4a43-8bda-09e26135e1ac'::uuid)
			THEN
				DELETE FROM public.roleusers r WHERE r.role='6a45efd2-7007-4a43-8bda-09e26135e1ac' AND r.user<>NEW.user;
			END IF;
		RETURN NEW;
    END;
$$;


ALTER FUNCTION public.insert_roleusers() OWNER TO postgres;

--
-- Name: uuid_generate_v1(); Type: FUNCTION; Schema: public; Owner: eauditpr_user
--

CREATE FUNCTION public.uuid_generate_v1() RETURNS uuid
    LANGUAGE c STRICT
    AS '$libdir/uuid-ossp', 'uuid_generate_v1';


ALTER FUNCTION public.uuid_generate_v1() OWNER TO postgres;

--
-- Name: uuid_generate_v1mc(); Type: FUNCTION; Schema: public; Owner: eauditpr_user
--

CREATE FUNCTION public.uuid_generate_v1mc() RETURNS uuid
    LANGUAGE c STRICT
    AS '$libdir/uuid-ossp', 'uuid_generate_v1mc';


ALTER FUNCTION public.uuid_generate_v1mc() OWNER TO postgres;

--
-- Name: uuid_generate_v3(uuid, text); Type: FUNCTION; Schema: public; Owner: eauditpr_user
--

CREATE FUNCTION public.uuid_generate_v3(namespace uuid, name text) RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_generate_v3';


ALTER FUNCTION public.uuid_generate_v3(namespace uuid, name text) OWNER TO postgres;

--
-- Name: uuid_generate_v4(); Type: FUNCTION; Schema: public; Owner: eauditpr_user
--

CREATE FUNCTION public.uuid_generate_v4() RETURNS uuid
    LANGUAGE c STRICT
    AS '$libdir/uuid-ossp', 'uuid_generate_v4';


ALTER FUNCTION public.uuid_generate_v4() OWNER TO postgres;

--
-- Name: uuid_generate_v5(uuid, text); Type: FUNCTION; Schema: public; Owner: eauditpr_user
--

CREATE FUNCTION public.uuid_generate_v5(namespace uuid, name text) RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_generate_v5';


ALTER FUNCTION public.uuid_generate_v5(namespace uuid, name text) OWNER TO postgres;

--
-- Name: uuid_nil(); Type: FUNCTION; Schema: public; Owner: eauditpr_user
--

CREATE FUNCTION public.uuid_nil() RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_nil';


ALTER FUNCTION public.uuid_nil() OWNER TO postgres;

--
-- Name: uuid_ns_dns(); Type: FUNCTION; Schema: public; Owner: eauditpr_user
--

CREATE FUNCTION public.uuid_ns_dns() RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_ns_dns';


ALTER FUNCTION public.uuid_ns_dns() OWNER TO postgres;

--
-- Name: uuid_ns_oid(); Type: FUNCTION; Schema: public; Owner: eauditpr_user
--

CREATE FUNCTION public.uuid_ns_oid() RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_ns_oid';


ALTER FUNCTION public.uuid_ns_oid() OWNER TO postgres;

--
-- Name: uuid_ns_url(); Type: FUNCTION; Schema: public; Owner: eauditpr_user
--

CREATE FUNCTION public.uuid_ns_url() RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_ns_url';


ALTER FUNCTION public.uuid_ns_url() OWNER TO postgres;

--
-- Name: uuid_ns_x500(); Type: FUNCTION; Schema: public; Owner: eauditpr_user
--

CREATE FUNCTION public.uuid_ns_x500() RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_ns_x500';


ALTER FUNCTION public.uuid_ns_x500() OWNER TO postgres;

--
-- Name: tim(); Type: FUNCTION; Schema: support; Owner: eauditpr_user
--

CREATE FUNCTION support.tim() RETURNS TABLE(id uuid, id_wilayah uuid, id_tim uuid, wilayah character varying, tim character varying, jabatan character varying, nama character varying)
    LANGUAGE plpgsql
    AS $$
	declare "_curs_tim" REFCURSOR;
	declare "_cur_tim" RECORD;
	declare "_curs_anggota" REFCURSOR;
	declare "_cur_anggota" RECORD;
	declare "_user" varchar;
	declare "_jabatan" varchar;
BEGIN 
	CREATE TEMP TABLE _tim
	("id" uuid,"id_wilayah" uuid,"id_tim" uuid, "wilayah" varchar, "tim" varchar, "jabatan" varchar, "nama" varchar) on commit drop;
	OPEN _curs_tim FOR 
	SELECT w."id" as id_wilayah,w."wilayah",w."irban",t.id as id_tim,t.tim,t.dalnis,t.ketua 
	from support.wilayah w 
		JOIN support.tim t 
		ON t.wilayah=w.id
		AND w.status=true 
		AND t.status=true 
		ORDER BY w.kode,t.tim;
	LOOP
		FETCH _curs_tim INTO _cur_tim;
		EXIT WHEN NOT FOUND;
			_user:=(SELECT u.name from public.users u WHERE u.id=_cur_tim.irban);
			_jabatan:='Inspektur Pembantu';
			INSERT INTO _tim VALUES (_cur_tim.irban,_cur_tim.id_wilayah,_cur_tim.id_tim,_cur_tim.wilayah,_cur_tim.tim,_jabatan,_user);

			_jabatan:='Pengendali Teknis';
			_user:=(SELECT u.name from public.users u WHERE u.id=_cur_tim.dalnis);
			INSERT INTO _tim VALUES (_cur_tim.dalnis,_cur_tim.id_wilayah,_cur_tim.id_tim,_cur_tim.wilayah,_cur_tim.tim,_jabatan,_user);

			_jabatan:='Ketua Tim';
			_user:=(SELECT u.name from public.users u WHERE u.id=_cur_tim.ketua);
			INSERT INTO _tim VALUES (_cur_tim.ketua,_cur_tim.id_wilayah,_cur_tim.id_tim,_cur_tim.wilayah,_cur_tim.tim,_jabatan,_user);
			OPEN _curs_anggota FOR SELECT a.anggota FROM support.anggota a WHERE a.tim=_cur_tim.id_tim;
			LOOP
				FETCH _curs_anggota INTO _cur_anggota;
				EXIT WHEN NOT FOUND;
					_jabatan:='Anggota Tim';
					_user:=(SELECT u.name from public.users u WHERE u.id=_cur_anggota.anggota);
					INSERT INTO _tim VALUES (_cur_anggota.anggota,_cur_tim.id_wilayah,_cur_tim.id_tim,_cur_tim.wilayah,_cur_tim.tim,_jabatan,_user);
			END LOOP;		
			CLOSE _curs_anggota;
	END LOOP;
	CLOSE _curs_tim;
	RETURN QUERY SELECT * FROM _tim;
END;
$$;


ALTER FUNCTION support.tim() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: indepensi; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.indepensi (
    id uuid NOT NULL,
    tanggal date,
    created_at timestamp(0) without time zone,
    created_by uuid,
    file character varying,
    app_sv3 uuid,
    app_sv3_timestamp timestamp(0) without time zone,
    app_sv3_status boolean,
    id_rencana uuid
);


ALTER TABLE interpro.indepensi OWNER TO postgres;

--
-- Name: lhp; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.lhp (
    id uuid NOT NULL,
    no character varying,
    tgl date,
    id_rencana uuid
);


ALTER TABLE interpro.lhp OWNER TO postgres;

--
-- Name: lhp_detail; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.lhp_detail (
    id uuid NOT NULL,
    kode_temuan uuid,
    kode_rekom uuid,
    negara smallint,
    daerah smallint,
    tindak_lanjut character varying,
    hasil_rekom smallint,
    nilai_rekom numeric,
    keterangan character varying,
    nd smallint
);


ALTER TABLE interpro.lhp_detail OWNER TO postgres;

--
-- Name: perencanaan; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.perencanaan (
    id uuid NOT NULL,
    dasar uuid,
    dasar_penugasan character varying,
    jenis uuid,
    satker uuid,
    pkpt_no character varying,
    judul character varying,
    start_date date,
    end_date date,
    spt_no character varying,
    spt_date date NOT NULL,
    closed boolean,
    created_by uuid,
    created_at timestamp(6) with time zone DEFAULT NULL::timestamp with time zone,
    modified_by uuid,
    modified_at timestamp(6) with time zone DEFAULT NULL::timestamp with time zone,
    status boolean,
    draft boolean,
    tim character varying,
    app_sv1 uuid,
    app_sv1_status boolean,
    app_sv1_tstamp timestamp(0) with time zone,
    app_sv2 uuid,
    app_sv2_status boolean,
    app_sv2_tstamp timestamp(0) with time zone,
    proses smallint DEFAULT 1 NOT NULL,
    app_sv1_note character varying,
    app_sv2_note character varying,
    no_laporan character varying,
    thn_laporan character varying,
    kesimpulan character varying,
    kp_no character varying,
    kp_date date,
    status_tl boolean,
    dalnis uuid,
    irban uuid,
    ketuatim uuid,
    jml_hari double precision,
    periode uuid
);


ALTER TABLE interpro.perencanaan OWNER TO postgres;

--
-- Name: COLUMN perencanaan.dasar; Type: COMMENT; Schema: interpro; Owner: eauditpr_user
--

COMMENT ON COLUMN interpro.perencanaan.dasar IS 'dasar penugasan';


--
-- Name: COLUMN perencanaan.jenis; Type: COMMENT; Schema: interpro; Owner: eauditpr_user
--

COMMENT ON COLUMN interpro.perencanaan.jenis IS 'jenis penugasan';


--
-- Name: COLUMN perencanaan.pkpt_no; Type: COMMENT; Schema: interpro; Owner: eauditpr_user
--

COMMENT ON COLUMN interpro.perencanaan.pkpt_no IS 'nomor pkpt';


--
-- Name: COLUMN perencanaan.judul; Type: COMMENT; Schema: interpro; Owner: eauditpr_user
--

COMMENT ON COLUMN interpro.perencanaan.judul IS 'judul surat perintah tugas';


--
-- Name: COLUMN perencanaan.spt_no; Type: COMMENT; Schema: interpro; Owner: eauditpr_user
--

COMMENT ON COLUMN interpro.perencanaan.spt_no IS 'nomor spt';


--
-- Name: perencanaan_aturan; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.perencanaan_aturan (
    id uuid NOT NULL,
    aturan uuid NOT NULL
);


ALTER TABLE interpro.perencanaan_aturan OWNER TO postgres;

--
-- Name: perencanaan_detail; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.perencanaan_detail (
    id uuid NOT NULL,
    tao uuid NOT NULL,
    rencana_by uuid,
    rencana_date date,
    realisasi_by uuid,
    realisasi_date date,
    file character varying,
    app_sv1 uuid,
    app_sv1_status boolean,
    app_sv1_tstamp timestamp(6) with time zone DEFAULT NULL::timestamp with time zone,
    app_sv2 uuid,
    app_sv2_status boolean,
    app_sv2_tstamp timestamp(6) with time zone DEFAULT NULL::timestamp with time zone,
    created_by uuid,
    created_at timestamp(6) with time zone DEFAULT NULL::timestamp with time zone,
    modified_by uuid,
    modified_at timestamp(6) with time zone DEFAULT NULL::timestamp with time zone,
    status boolean,
    app_sv3 uuid,
    app_sv3_status boolean,
    app_sv3_tstamp timestamp(6) with time zone,
    progres smallint DEFAULT 0 NOT NULL,
    app_sv1_note text,
    app_sv2_note text,
    app_sv3_note text,
    kesimpulan text,
    jumlah_hari double precision
);


ALTER TABLE interpro.perencanaan_detail OWNER TO postgres;

--
-- Name: perencanaan_hasil; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.perencanaan_hasil (
    id uuid NOT NULL,
    file character varying,
    created_at timestamp(0) with time zone,
    created_by uuid,
    app_sv1 uuid,
    app_sv1_status boolean,
    app_sv1_tstamp timestamp(0) with time zone,
    app_sv1_note character varying,
    app_sv2 uuid,
    app_sv2_status boolean,
    app_sv2_tstamp timestamp(0) with time zone,
    app_sv2_note character varying,
    app_sv3 uuid,
    app_sv3_status boolean,
    app_sv3_tstamp timestamp(0) with time zone,
    app_sv3_note character varying,
    progres smallint,
    modified_at timestamp(6) with time zone,
    modified_by uuid,
    thn_laporan character varying,
    no_laporan character varying,
    id_tao uuid,
    kesimpulan character varying
);


ALTER TABLE interpro.perencanaan_hasil OWNER TO postgres;

--
-- Name: perencanaan_pedoman; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.perencanaan_pedoman (
    id uuid NOT NULL,
    pedoman uuid NOT NULL
);


ALTER TABLE interpro.perencanaan_pedoman OWNER TO postgres;

--
-- Name: perencanaan_sasaran; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.perencanaan_sasaran (
    id uuid NOT NULL,
    sasaran uuid NOT NULL
);


ALTER TABLE interpro.perencanaan_sasaran OWNER TO postgres;

--
-- Name: perencanaan_tim; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.perencanaan_tim (
    id uuid NOT NULL,
    irban uuid,
    dalnis uuid,
    ketua uuid,
    tim uuid[]
);


ALTER TABLE interpro.perencanaan_tim OWNER TO postgres;

--
-- Name: perencanaan_tujuan; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.perencanaan_tujuan (
    id uuid NOT NULL,
    tujuan uuid NOT NULL
);


ALTER TABLE interpro.perencanaan_tujuan OWNER TO postgres;

--
-- Name: tindak_lanjut; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.tindak_lanjut (
    id uuid NOT NULL,
    id_rencana uuid,
    created_at timestamp(0) without time zone DEFAULT now(),
    created_by uuid,
    modified_at timestamp(0) without time zone,
    modified_by uuid,
    status boolean,
    judul character varying,
    spt_no character varying,
    spt_date date,
    pkpt_no uuid,
    app_sv1 uuid,
    app_sv1_tstamp timestamp(0) without time zone,
    app_sv1_note character varying,
    app_sv2 uuid,
    app_sv2_tstamp timestamp(0) without time zone,
    app_sv2_note character varying,
    app_sv1_file character varying,
    app_sv2_file character varying,
    pelaksana character varying,
    thn_lhp character varying,
    no_lhp character varying,
    skpd character varying,
    app_sv1_status boolean,
    app_sv2_status boolean,
    file_akhir character varying,
    tgl_lhp date,
    proses smallint DEFAULT 0,
    lap_akir character varying,
    jml_temuan double precision
);


ALTER TABLE interpro.tindak_lanjut OWNER TO postgres;

--
-- Name: tindak_lanjut_penyebab; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.tindak_lanjut_penyebab (
    id uuid NOT NULL,
    kode_penyebab uuid,
    uraian_penyebab character varying,
    urutan double precision
);


ALTER TABLE interpro.tindak_lanjut_penyebab OWNER TO postgres;

--
-- Name: tindak_lanjut_rekom; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.tindak_lanjut_rekom (
    id uuid NOT NULL,
    kode_rekom uuid,
    uraian_rekom character varying,
    no_tl uuid,
    uraian_tl character varying,
    urutan double precision
);


ALTER TABLE interpro.tindak_lanjut_rekom OWNER TO postgres;

--
-- Name: tindak_lanjutd; Type: TABLE; Schema: interpro; Owner: eauditpr_user
--

CREATE TABLE interpro.tindak_lanjutd (
    id uuid NOT NULL,
    kode_temuan uuid,
    kode_rekom uuid,
    negara smallint,
    daerah smallint,
    tindak_lanjut character varying,
    hasil_rekom smallint,
    nilai_rekom numeric,
    keterangan character varying,
    obrik character varying,
    akibat character varying,
    penyebab character varying,
    kondisi character varying,
    kriteria character varying,
    tujuan_tl character varying,
    sasaran_tl character varying,
    evaluasi character varying,
    ruang character varying,
    tanggapan character varying,
    tindak_lanjut_s character varying,
    nd smallint,
    judul_tl character varying,
    uraian_rekom character varying,
    judul_temuan character varying,
    uraian_temuan character varying,
    nilai_temuan double precision
);


ALTER TABLE interpro.tindak_lanjutd OWNER TO postgres;

--
-- Name: pkpt; Type: TABLE; Schema: master; Owner: eaudit
--

CREATE TABLE master.pkpt (
    id uuid NOT NULL,
    no character varying,
    tema character varying,
    nama character varying,
    kegiatan character varying,
    hp character varying,
    rmp character varying,
    rpl character varying,
    dana character varying,
    risiko character varying,
    keterangan character varying,
    status boolean,
    jenis uuid,
    sasaran uuid,
    tujuan uuid
);


ALTER TABLE master.pkpt OWNER TO postgres;

--
-- Name: v_pelaksanaan; Type: VIEW; Schema: interpro; Owner: eauditpr_user
--

CREATE VIEW interpro.v_pelaksanaan AS
 SELECT p.id,
    p.dasar,
    p.dasar_penugasan,
    p.jenis,
    p.satker,
    pkpt.no AS pkpt_no,
    p.judul,
    p.start_date,
    p.end_date,
    p.spt_no,
    p.spt_date,
    p.closed,
    p.created_by,
    p.created_at,
    p.modified_by,
    p.modified_at,
    p.status,
    p.app_sv1,
    p.app_sv1_status,
    p.app_sv1_tstamp,
    p.app_sv2,
    p.app_sv2_status,
    p.app_sv2_tstamp,
    p.draft,
    p.proses,
    p.tim,
    count(*) AS total,
    count(*) FILTER (WHERE (pd.realisasi_by IS NOT NULL)) AS realisasi,
    count(*) FILTER (WHERE (pd.app_sv3_status = true)) AS disetujui
   FROM ((interpro.perencanaan p
     JOIN master.pkpt ON ((((pkpt.id)::character varying)::text = (p.pkpt_no)::text)))
     JOIN interpro.perencanaan_detail pd ON ((pd.id = p.id)))
  WHERE ((p.app_sv1_status = true) AND (p.app_sv2_status = true) AND (p.proses >= 3))
  GROUP BY p.id, p.dasar, p.dasar_penugasan, p.jenis, p.satker, pkpt.no, p.judul, p.start_date, p.end_date, p.spt_no, p.spt_date, p.closed, p.created_by, p.created_at, p.modified_by, p.modified_at, p.status, p.app_sv1, p.app_sv1_status, p.app_sv1_tstamp, p.app_sv2, p.app_sv2_status, p.app_sv2_tstamp, p.draft, p.proses, p.tim;


ALTER TABLE interpro.v_pelaksanaan OWNER TO postgres;

--
-- Name: jenis; Type: TABLE; Schema: master; Owner: eauditpr_user
--

CREATE TABLE master.jenis (
    id uuid NOT NULL,
    kode character varying,
    ket character varying,
    "order" smallint,
    status boolean
);


ALTER TABLE master.jenis OWNER TO postgres;

--
-- Name: satker; Type: TABLE; Schema: master; Owner: eauditpr_user
--

CREATE TABLE master.satker (
    id uuid NOT NULL,
    kode character varying,
    ket character varying,
    "order" smallint,
    status boolean,
    nama character varying,
    alamat text,
    email character varying,
    resiko character varying,
    ket_resiko character varying,
    jumlah_anggaran smallint,
    nilai_aset smallint,
    jumlah_pegawai smallint,
    jumlah_unit smallint,
    jumlah_jenis_keg smallint,
    jumlah_pemeriksaan smallint,
    nilai_sakip smallint,
    nilai_spip smallint,
    tertib_adm smallint,
    jumlah_pengaduan smallint,
    jumlah_jabatan smallint,
    hasil_monitoring smallint
);


ALTER TABLE master.satker OWNER TO postgres;

--
-- Name: v_perencanaan; Type: VIEW; Schema: interpro; Owner: eauditpr_user
--

CREATE VIEW interpro.v_perencanaan AS
 SELECT p.id,
    p.dasar,
    p.dasar_penugasan,
    p.jenis,
    p.satker,
    pkpt.no AS pkpt_no,
    p.judul,
    p.start_date,
    p.end_date,
    p.spt_no,
    p.spt_date,
    p.closed,
    p.created_by,
    p.created_at,
    p.modified_by,
    p.modified_at,
    p.status,
    p.app_sv1,
    p.app_sv1_status,
    p.app_sv1_tstamp,
    p.app_sv2,
    p.app_sv2_status,
    p.app_sv2_tstamp,
    p.draft,
    p.proses,
    p.tim,
    p.no_laporan,
    (((s.kode)::text || ' '::text) || (s.ket)::text) AS nama_satker,
    (((j.kode)::text || ' '::text) || (j.ket)::text) AS nama_jenis,
    p.periode
   FROM (((interpro.perencanaan p
     LEFT JOIN master.satker s ON ((s.id = p.satker)))
     LEFT JOIN master.jenis j ON ((j.id = p.jenis)))
     LEFT JOIN master.pkpt ON ((((pkpt.id)::character varying)::text = (p.pkpt_no)::text)));


ALTER TABLE interpro.v_perencanaan OWNER TO postgres;

--
-- Name: conf_spt; Type: TABLE; Schema: master; Owner: eauditpr_user
--

CREATE TABLE master.conf_spt (
    id uuid NOT NULL,
    ket character varying,
    "order" smallint,
    status boolean
);


ALTER TABLE master.conf_spt OWNER TO postgres;

--
-- Name: dasar_penugasan; Type: TABLE; Schema: master; Owner: eaudit
--

CREATE TABLE master.dasar_penugasan (
    id uuid NOT NULL,
    kode character varying,
    dasar character varying,
    "order" smallint,
    show boolean DEFAULT true,
    other boolean DEFAULT false
);


ALTER TABLE master.dasar_penugasan OWNER TO postgres;

--
-- Name: COLUMN dasar_penugasan.show; Type: COMMENT; Schema: master; Owner: eaudit
--

COMMENT ON COLUMN master.dasar_penugasan.show IS 'tampil di pilihan';


--
-- Name: COLUMN dasar_penugasan.other; Type: COMMENT; Schema: master; Owner: eaudit
--

COMMENT ON COLUMN master.dasar_penugasan.other IS 'jika true isi textfield';


--
-- Name: golongan; Type: TABLE; Schema: master; Owner: eaudit
--

CREATE TABLE master.golongan (
    id uuid NOT NULL,
    ket character varying,
    "order" smallint,
    status boolean
);


ALTER TABLE master.golongan OWNER TO postgres;

--
-- Name: jabatan; Type: TABLE; Schema: master; Owner: eaudit
--

CREATE TABLE master.jabatan (
    id uuid NOT NULL,
    ket character varying,
    "order" smallint,
    status boolean
);


ALTER TABLE master.jabatan OWNER TO postgres;

--
-- Name: kendali; Type: TABLE; Schema: master; Owner: eauditpr_user
--

CREATE TABLE master.kendali (
    id uuid NOT NULL,
    nama character varying,
    status boolean
);


ALTER TABLE master.kendali OWNER TO postgres;

--
-- Name: penyebab; Type: TABLE; Schema: master; Owner: eauditpr_user
--

CREATE TABLE master.penyebab (
    id uuid NOT NULL,
    nama character varying,
    status boolean,
    kode character varying
);


ALTER TABLE master.penyebab OWNER TO postgres;

--
-- Name: profile; Type: TABLE; Schema: master; Owner: eaudit
--

CREATE TABLE master.profile (
    id uuid NOT NULL,
    nama character varying,
    alamat character varying,
    telp character varying,
    status boolean
);


ALTER TABLE master.profile OWNER TO postgres;

--
-- Name: prosedur_audit; Type: TABLE; Schema: master; Owner: eauditpr_user
--

CREATE TABLE master.prosedur_audit (
    id uuid NOT NULL,
    nama character varying,
    status boolean
);


ALTER TABLE master.prosedur_audit OWNER TO postgres;

--
-- Name: resiko_satker; Type: TABLE; Schema: master; Owner: eauditpr_user
--

CREATE TABLE master.resiko_satker (
    id uuid NOT NULL,
    periode uuid,
    resiko character varying,
    ket_resiko character varying,
    jumlah_anggaran smallint,
    nilai_aset smallint,
    jumlah_pegawai smallint,
    jumlah_unit smallint,
    jumlah_jenis_keg smallint,
    jumlah_pemeriksaan smallint,
    nilai_sakip smallint,
    nilai_spip smallint,
    tertib_adm smallint,
    jumlah_pengaduan smallint,
    jumlah_jabatan smallint,
    hasil_monitoring smallint,
    id_satker uuid,
    lp smallint,
    lp2 smallint,
    lp3 smallint,
    lp4 smallint,
    lp5 smallint,
    tot_lp smallint,
    km smallint,
    km2 smallint,
    km3 smallint,
    km4 smallint,
    km5 smallint,
    tot_km smallint,
    im smallint,
    im2 smallint,
    im3 smallint,
    im4 smallint,
    im5 smallint,
    tot_im smallint,
    ko smallint,
    ko2 smallint,
    ko3 smallint,
    ko4 smallint,
    ko5 smallint,
    tot_ko smallint,
    si smallint,
    si2 smallint,
    si3 smallint,
    si4 smallint,
    si5 smallint,
    tot_si smallint
);


ALTER TABLE master.resiko_satker OWNER TO postgres;

--
-- Name: sasaran; Type: TABLE; Schema: master; Owner: eaudit
--

CREATE TABLE master.sasaran (
    id uuid NOT NULL,
    kode character varying,
    ket character varying,
    "order" smallint,
    status boolean
);


ALTER TABLE master.sasaran OWNER TO postgres;

--
-- Name: tahapan_audit; Type: TABLE; Schema: master; Owner: eaudit
--

CREATE TABLE master.tahapan_audit (
    kode character varying NOT NULL,
    tahapan character varying
);


ALTER TABLE master.tahapan_audit OWNER TO postgres;

--
-- Name: tindak_lanjut; Type: TABLE; Schema: master; Owner: eauditpr_user
--

CREATE TABLE master.tindak_lanjut (
    id uuid NOT NULL,
    nama character varying,
    kode character varying,
    status boolean
);


ALTER TABLE master.tindak_lanjut OWNER TO postgres;

--
-- Name: COLUMN tindak_lanjut.id; Type: COMMENT; Schema: master; Owner: eauditpr_user
--

COMMENT ON COLUMN master.tindak_lanjut.id IS ' ';


--
-- Name: tipe_resiko; Type: TABLE; Schema: master; Owner: eauditpr_user
--

CREATE TABLE master.tipe_resiko (
    id uuid NOT NULL,
    nama character varying,
    status boolean
);


ALTER TABLE master.tipe_resiko OWNER TO postgres;

--
-- Name: tujuan; Type: TABLE; Schema: master; Owner: eaudit
--

CREATE TABLE master.tujuan (
    id uuid NOT NULL,
    kode character varying,
    ket character varying,
    "order" smallint,
    status boolean
);


ALTER TABLE master.tujuan OWNER TO postgres;

--
-- Name: v_pkpt; Type: VIEW; Schema: master; Owner: eauditpr_user
--

CREATE VIEW master.v_pkpt AS
 SELECT "substring"((pkpt.no)::text, 0, 5) AS tahun
   FROM master.pkpt
  GROUP BY ("substring"((pkpt.no)::text, 0, 5));


ALTER TABLE master.v_pkpt OWNER TO postgres;

--
-- Name: roleusers; Type: TABLE; Schema: public; Owner: eaudit
--

CREATE TABLE public.roleusers (
    role uuid NOT NULL,
    "user" uuid NOT NULL
);


ALTER TABLE public.roleusers OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: eaudit
--

CREATE TABLE public.users (
    id uuid NOT NULL,
    name character varying(255) DEFAULT NULL::character varying NOT NULL,
    email character varying(255) DEFAULT NULL::character varying NOT NULL,
    username character varying(20) DEFAULT NULL::character varying NOT NULL,
    password character varying(32) DEFAULT NULL::character varying NOT NULL,
    created_at timestamp(6) with time zone DEFAULT NULL::timestamp with time zone,
    modified_at timestamp(6) with time zone DEFAULT NULL::timestamp with time zone,
    status boolean NOT NULL,
    avatar character varying,
    nip character varying,
    jabatan uuid,
    golongan uuid,
    telp character varying,
    alamat character varying,
    ttd character varying
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: inspektur; Type: VIEW; Schema: public; Owner: eauditpr_user
--

CREATE VIEW public.inspektur AS
 SELECT u.id,
    u.name AS nama,
    u.email,
    u.nip,
    g.ket AS golongan,
    u.ttd
   FROM ((public.users u
     JOIN public.roleusers r ON (((u.id = r."user") AND (r.role = '6a45efd2-7007-4a43-8bda-09e26135e1ac'::uuid))))
     LEFT JOIN master.golongan g ON ((u.golongan = g.id)));


ALTER TABLE public.inspektur OWNER TO postgres;

--
-- Name: menu; Type: TABLE; Schema: public; Owner: eaudit
--

CREATE TABLE public.menu (
    id uuid NOT NULL,
    name character varying,
    parent uuid,
    level smallint,
    report boolean
);


ALTER TABLE public.menu OWNER TO postgres;

--
-- Name: periode; Type: TABLE; Schema: public; Owner: eauditpr_user
--

CREATE TABLE public.periode (
    id uuid NOT NULL,
    nama character varying,
    start date,
    "end" date,
    status boolean
);


ALTER TABLE public.periode OWNER TO postgres;

--
-- Name: periode_holiday; Type: TABLE; Schema: public; Owner: eauditpr_user
--

CREATE TABLE public.periode_holiday (
    id uuid NOT NULL,
    holiday date,
    ket character varying
);


ALTER TABLE public.periode_holiday OWNER TO postgres;

--
-- Name: role; Type: TABLE; Schema: public; Owner: eaudit
--

CREATE TABLE public.role (
    id uuid NOT NULL,
    name character varying(255) DEFAULT NULL::character varying NOT NULL,
    status boolean NOT NULL
);


ALTER TABLE public.role OWNER TO postgres;

--
-- Name: rolemenu; Type: TABLE; Schema: public; Owner: eaudit
--

CREATE TABLE public.rolemenu (
    role uuid NOT NULL,
    menu uuid NOT NULL,
    c boolean,
    r boolean,
    u boolean,
    d boolean
);


ALTER TABLE public.rolemenu OWNER TO postgres;

--
-- Name: sessions; Type: TABLE; Schema: public; Owner: eaudit
--

CREATE TABLE public.sessions (
    id character varying(128) DEFAULT NULL::character varying NOT NULL,
    ip_address character varying(45) DEFAULT NULL::character varying NOT NULL,
    "timestamp" bigint DEFAULT 0 NOT NULL,
    data text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.sessions OWNER TO postgres;

--
-- Name: anggota; Type: TABLE; Schema: support; Owner: eaudit
--

CREATE TABLE support.anggota (
    tim uuid NOT NULL,
    anggota uuid NOT NULL
);


ALTER TABLE support.anggota OWNER TO postgres;

--
-- Name: aturan; Type: TABLE; Schema: support; Owner: eaudit
--

CREATE TABLE support.aturan (
    id uuid NOT NULL,
    kode character varying,
    jenis uuid,
    sasaran uuid,
    file character varying,
    status boolean
);


ALTER TABLE support.aturan OWNER TO postgres;

--
-- Name: kegiatan_kr; Type: TABLE; Schema: support; Owner: eauditpr_user
--

CREATE TABLE support.kegiatan_kr (
    id uuid NOT NULL,
    resiko uuid,
    kendali_s uuid,
    kendali_a uuid,
    urutan character varying
);


ALTER TABLE support.kegiatan_kr OWNER TO postgres;

--
-- Name: kegiatan_satker; Type: TABLE; Schema: support; Owner: eauditpr_user
--

CREATE TABLE support.kegiatan_satker (
    id uuid NOT NULL,
    nama character varying,
    program_satker uuid,
    resiko uuid,
    penyebab character varying,
    jumlah double precision,
    kendali_s uuid,
    kendali_a uuid,
    prosedur_audit uuid NOT NULL,
    status boolean,
    periode uuid
);


ALTER TABLE support.kegiatan_satker OWNER TO postgres;

--
-- Name: kejadian_satker; Type: TABLE; Schema: support; Owner: eauditpr_user
--

CREATE TABLE support.kejadian_satker (
    id uuid NOT NULL,
    nama character varying,
    pk uuid,
    resiko uuid,
    penyebab character varying,
    jumlah double precision,
    kendali_s uuid,
    kendali_a uuid,
    prosedur_audit uuid,
    status boolean,
    program_kejadian character varying,
    hitung boolean,
    periode uuid
);


ALTER TABLE support.kejadian_satker OWNER TO postgres;

--
-- Name: kertas_kerja; Type: TABLE; Schema: support; Owner: eaudit
--

CREATE TABLE support.kertas_kerja (
    id uuid NOT NULL,
    kode character varying,
    jenis uuid,
    sasaran uuid,
    file character varying,
    status boolean
);


ALTER TABLE support.kertas_kerja OWNER TO postgres;

--
-- Name: kode_temuan; Type: TABLE; Schema: support; Owner: eauditpr_user
--

CREATE TABLE support.kode_temuan (
    id uuid NOT NULL,
    k1 character varying(10) DEFAULT NULL::character varying NOT NULL,
    k2 character varying(10) DEFAULT NULL::character varying,
    k3 character varying(10) DEFAULT NULL::character varying,
    k4 character varying(10) DEFAULT NULL::character varying,
    nama character varying(255) DEFAULT NULL::character varying NOT NULL,
    level smallint NOT NULL,
    status boolean DEFAULT true,
    deleted boolean DEFAULT false,
    created_at timestamp(6) with time zone DEFAULT NULL::timestamp with time zone,
    created_by uuid,
    modified_at timestamp(6) with time zone DEFAULT NULL::timestamp with time zone,
    modified_by uuid
);


ALTER TABLE support.kode_temuan OWNER TO postgres;

--
-- Name: notif; Type: TABLE; Schema: support; Owner: eaudit
--

CREATE TABLE support.notif (
    id uuid NOT NULL,
    id_user uuid,
    type character varying,
    message character varying,
    reff_id1 uuid,
    reff_id2 uuid,
    created_at timestamp(0) with time zone,
    readed_at timestamp(0) with time zone
);


ALTER TABLE support.notif OWNER TO postgres;

--
-- Name: pedoman; Type: TABLE; Schema: support; Owner: eaudit
--

CREATE TABLE support.pedoman (
    id uuid NOT NULL,
    kode character varying,
    jenis uuid,
    sasaran uuid,
    file character varying,
    status boolean
);


ALTER TABLE support.pedoman OWNER TO postgres;

--
-- Name: program_kr; Type: TABLE; Schema: support; Owner: eauditpr_user
--

CREATE TABLE support.program_kr (
    id uuid NOT NULL,
    resiko uuid,
    kendali_s uuid,
    kendali_a uuid,
    urutan character varying
);


ALTER TABLE support.program_kr OWNER TO postgres;

--
-- Name: program_satker; Type: TABLE; Schema: support; Owner: eauditpr_user
--

CREATE TABLE support.program_satker (
    id uuid NOT NULL,
    nama character varying,
    resiko uuid,
    penyebab character varying,
    jumlah double precision,
    kendali_s uuid,
    kendali_a uuid,
    prosedur_audit uuid,
    status boolean,
    periode uuid
);


ALTER TABLE support.program_satker OWNER TO postgres;

--
-- Name: COLUMN program_satker.id; Type: COMMENT; Schema: support; Owner: eauditpr_user
--

COMMENT ON COLUMN support.program_satker.id IS ' ';


--
-- Name: tao; Type: TABLE; Schema: support; Owner: eaudit
--

CREATE TABLE support.tao (
    id uuid NOT NULL,
    kode character varying,
    jenis uuid,
    tujuan uuid,
    status boolean,
    sasaran uuid,
    tahapan character varying,
    langkah character varying
);


ALTER TABLE support.tao OWNER TO postgres;

--
-- Name: tao_detail; Type: TABLE; Schema: support; Owner: eauditpr_user
--

CREATE TABLE support.tao_detail (
    id uuid NOT NULL,
    id_tao uuid,
    langkah character varying,
    kertas_kerja character varying,
    file_download character varying,
    status boolean
);


ALTER TABLE support.tao_detail OWNER TO postgres;

--
-- Name: tim; Type: TABLE; Schema: support; Owner: eaudit
--

CREATE TABLE support.tim (
    id uuid NOT NULL,
    wilayah uuid,
    tim character varying,
    dalnis uuid,
    ketua uuid,
    status boolean
);


ALTER TABLE support.tim OWNER TO postgres;

--
-- Name: v_tao; Type: VIEW; Schema: support; Owner: eauditpr_user
--

CREATE VIEW support.v_tao AS
 SELECT t1.id AS id_tao,
    t2.id,
    t1.tahapan,
    t5.tahapan AS ket_tahapan,
    (((t4.kode)::text || (t3.kode)::text) || (t1.kode)::text) AS kode_tao,
    ((((('KK'::text || (t4.kode)::text) || (t3.kode)::text) || (t1.kode)::text) || '-'::text) || (t2.kertas_kerja)::text) AS kode_kk,
    t1.langkah AS tao,
    t2.langkah,
    t1.status AS status_tao,
    t2.status AS status_kk,
    t1.tujuan,
    t1.sasaran,
    t1.jenis
   FROM ((((support.tao t1
     JOIN support.tao_detail t2 ON ((t2.id_tao = t1.id)))
     JOIN master.tujuan t3 ON ((t1.tujuan = t3.id)))
     JOIN master.sasaran t4 ON ((t1.sasaran = t4.id)))
     JOIN master.tahapan_audit t5 ON (((t1.tahapan)::text = (t5.kode)::text)));


ALTER TABLE support.v_tao OWNER TO postgres;

--
-- Name: v_tao_header; Type: VIEW; Schema: support; Owner: eauditpr_user
--

CREATE VIEW support.v_tao_header AS
 SELECT t1.id,
    t1.tahapan,
    t5.tahapan AS ket_tahapan,
    (((t4.kode)::text || (t3.kode)::text) || (t1.kode)::text) AS kode,
    t1.langkah,
    t1.status,
    t1.tujuan,
    t1.sasaran,
    t1.jenis
   FROM (((support.tao t1
     JOIN master.tujuan t3 ON ((t1.tujuan = t3.id)))
     JOIN master.sasaran t4 ON ((t1.sasaran = t4.id)))
     JOIN master.tahapan_audit t5 ON (((t1.tahapan)::text = (t5.kode)::text)));


ALTER TABLE support.v_tao_header OWNER TO postgres;

--
-- Name: v_tao_pelaksanaan; Type: VIEW; Schema: support; Owner: eauditpr_user
--

CREATE VIEW support.v_tao_pelaksanaan AS
 SELECT t1.id AS id_tao,
    t2.id,
    t1.tahapan,
    t5.tahapan AS ket_tahapan,
    (((t4.kode)::text || (t3.kode)::text) || (t1.kode)::text) AS kode_tao,
    ((((('KK'::text || (t4.kode)::text) || (t3.kode)::text) || (t1.kode)::text) || '-'::text) || (t2.kertas_kerja)::text) AS kode_kk,
    t1.langkah AS tao,
    t2.langkah,
    t1.status AS status_tao,
    t2.status AS status_kk,
    t1.tujuan,
    t1.sasaran,
    t1.jenis,
    t2.file_download AS template
   FROM ((((support.tao t1
     JOIN support.tao_detail t2 ON ((t2.id_tao = t1.id)))
     JOIN master.tujuan t3 ON ((t1.tujuan = t3.id)))
     JOIN master.sasaran t4 ON ((t1.sasaran = t4.id)))
     JOIN master.tahapan_audit t5 ON (((t1.tahapan)::text = (t5.kode)::text)))
  WHERE ((t1.status = true) AND (t2.status = true));


ALTER TABLE support.v_tao_pelaksanaan OWNER TO postgres;

--
-- Name: wilayah; Type: TABLE; Schema: support; Owner: eaudit
--

CREATE TABLE support.wilayah (
    id uuid NOT NULL,
    kode character varying,
    wilayah character varying,
    irban uuid,
    status boolean DEFAULT true
);


ALTER TABLE support.wilayah OWNER TO postgres;

--
-- Data for Name: indepensi; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.indepensi (id, tanggal, created_at, created_by, file, app_sv3, app_sv3_timestamp, app_sv3_status, id_rencana) FROM stdin;
\.


--
-- Data for Name: lhp; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.lhp (id, no, tgl, id_rencana) FROM stdin;
daba39f5-5ec2-473a-b618-328e84ba1e90	1234	2018-10-22	9375e424-6ea6-4841-a55f-1c1f79c8c6de
2bb1d5e2-cb27-4fb6-8c67-73156e72a74f	1235	2018-10-22	9375e424-6ea6-4841-a55f-1c1f79c8c6de
\.


--
-- Data for Name: lhp_detail; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.lhp_detail (id, kode_temuan, kode_rekom, negara, daerah, tindak_lanjut, hasil_rekom, nilai_rekom, keterangan, nd) FROM stdin;
\.


--
-- Data for Name: perencanaan; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.perencanaan (id, dasar, dasar_penugasan, jenis, satker, pkpt_no, judul, start_date, end_date, spt_no, spt_date, closed, created_by, created_at, modified_by, modified_at, status, draft, tim, app_sv1, app_sv1_status, app_sv1_tstamp, app_sv2, app_sv2_status, app_sv2_tstamp, proses, app_sv1_note, app_sv2_note, no_laporan, thn_laporan, kesimpulan, kp_no, kp_date, status_tl, dalnis, irban, ketuatim, jml_hari, periode) FROM stdin;
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1		audit bos	2018-09-03	2018-09-04	0003	2018-08-31	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 04:08:53+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	ae3234c8-ea30-45d2-a48e-367b8c32567d	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	222cead4-f3bf-48d7-9541-84f60c66cac5	11	7ae49e60-c139-4744-820f-71939ed71eae
b90a05f4-e77d-44b7-b657-a2fefd13c8fb	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1		DEVI	2018-09-03	2018-09-04	5577	2018-08-31	f	3cd6fe67-4266-465f-ae5a-d1e60dfd2709	2018-08-31 06:59:15+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6","89488dc0-00e0-48c8-9e47-1f143b877878"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	ae3234c8-ea30-45d2-a48e-367b8c32567d	7a766876-545a-42a4-bb81-ca6fa59efff6	2	7ae49e60-c139-4744-820f-71939ed71eae
7a0547b1-b065-42bf-a898-6228ef56fe5a	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1	ee358350-0710-4d77-9a63-bd0f55e511ca	audit operasional dana BOS	2018-09-03	2018-09-06	70089	2018-08-31	f	acbc412d-f29a-4931-b599-a37cee3aed69	2018-08-31 07:23:12+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","f741041f-732a-481f-bc78-461bc55ada9b","d016fde5-c284-4455-a44a-723ab77fa99f","6f9cfc1d-dc27-4d78-8e51-51dcbf30117b"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	f741041f-732a-481f-bc78-461bc55ada9b	ae3234c8-ea30-45d2-a48e-367b8c32567d	d016fde5-c284-4455-a44a-723ab77fa99f	2	7ae49e60-c139-4744-820f-71939ed71eae
b1477309-9e65-4c43-aefa-1219ef447c56	\N	\N	519bbf54-6048-4a14-b184-898e521d9094	9839aa5d-3197-4ba3-9180-6713d08571b1		firly	2018-09-03	2018-09-28	1984	2018-09-03	f	5efd92f0-d96e-4037-ab17-467310a76cd0	2018-08-31 07:36:28+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	ae3234c8-ea30-45d2-a48e-367b8c32567d	7a766876-545a-42a4-bb81-ca6fa59efff6	7	7ae49e60-c139-4744-820f-71939ed71eae
185a85fd-c805-4697-9719-21b017164891	\N	\N	47610b8d-0476-4174-9128-996685a51354	a2f700a6-7343-41cd-b70f-122ee937b9f4	caed4da2-ca46-4c41-9914-c8ec60604409	Audit operasional atas pengelolaan keuangan dan aset SMP Negeri	2017-09-07	2017-09-29	0917	2017-09-06	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-08 12:28:42+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-09-12 08:23:39.922056+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6","9d5f96cb-8d85-4216-b60e-809f39620ac6"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-09-12 08:21:55+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-12 08:23:40+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1	119afc90-7dac-4158-adbb-549e6b2ab0c8	Audit operasional pengelolaan keuangan dan aset SMP Negeri (Tim 3)	2017-09-13	2017-09-13	09173	2017-09-06	f	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-09-13 11:47:59.607871+00	t	f	["ac513f02-a71e-4764-b78c-216b62ea590f","70bf9ad1-3775-4be7-abab-9128837416ad","3d1d59ae-5115-44ee-8d8c-7db38a5e5200","dd7ae175-271f-467e-979e-8d2d709b1f0e","ae3234c8-ea30-45d2-a48e-367b8c32567d"]	dd7ae175-271f-467e-979e-8d2d709b1f0e	t	2017-09-13 11:47:06+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-13 11:48:00+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
01950624-d000-47ca-ab1f-11837e731ed1	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	697b7869-d11a-4dc3-81d8-eb3082f6db84	8118d9a2-069e-4987-8b4c-3c6bf9aca7c8	Melakukan Monitoring dan Evaluasi Pengadaan Barang dan Jasa  bersumber DAK pada DispendukCapil	2017-10-20	2017-10-20		2017-10-20	f	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:22:31+00	\N	\N	t	t	[]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	9839aa5d-3197-4ba3-9180-6713d08571b1	cdca94d0-2297-41ee-a5eb-7522b679bd1d	Monitoring Evaluasi Pengadaan Barang dan Jasa	2017-10-09	2017-10-18	10237	2017-10-05	f	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	ed485c96-003c-4d05-b7ac-ac38a883257d	2017-10-20 03:04:07.292086+00	t	f	["ed485c96-003c-4d05-b7ac-ac38a883257d","1b162438-55b1-4b5c-ac17-5699e9dfb1ed","ebe58226-6813-4081-8377-b11d002e2be8","986b958b-1a98-44e4-a59b-c02a15620381"]	1b162438-55b1-4b5c-ac17-5699e9dfb1ed	t	2017-10-20 01:45:46+00	ed485c96-003c-4d05-b7ac-ac38a883257d	t	2017-10-20 03:04:07+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
a109ef9e-68e4-48c0-8d25-3318e4154d07	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	70bc7a06-9b14-42c6-aaa3-f4c9e13a7303	1206eaa7-ffbc-451b-ab3e-ac0e69c53bd6	test ahhahha	2018-04-12	2018-04-26	110011	2018-04-12	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-12 10:02:19+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6","89488dc0-00e0-48c8-9e47-1f143b877878"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
145af5e3-abff-4780-b9cd-5b8bba415a09	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	70bc7a06-9b14-42c6-aaa3-f4c9e13a7303	4da7ce19-6326-4c7c-a974-a0b9938974e9	Testing 2	2018-04-08	2018-04-16	8909811	2018-04-16	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-16 05:29:18+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	ae3234c8-ea30-45d2-a48e-367b8c32567d	7a766876-545a-42a4-bb81-ca6fa59efff6	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1		DANA BOS SMP 12 KOTA PROBOLINGGO	2018-09-01	2018-09-28	8888	2018-09-01	f	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-09-01 00:37:21.236102+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2018-09-01 00:35:21+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2018-09-01 00:37:21+00	3	Penugasan daat dilanjutkan, jumlah HP sudah sama dengan SPT	Penugasan dapat dilanjutkan, terbitkan surat perintah tugas	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	ae3234c8-ea30-45d2-a48e-367b8c32567d	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	7.25	0be2e5bf-1089-4332-bfc0-b6b34009be60
4ac7b90f-d4ad-4bba-bad1-94493366203d	\N	\N	519bbf54-6048-4a14-b184-898e521d9094	9839aa5d-3197-4ba3-9180-6713d08571b1	caed4da2-ca46-4c41-9914-c8ec60604409	AA	2017-08-10	2017-08-25	12345	2017-08-25	f	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-09-24 00:27:27.224908+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	2017-09-06 11:40:38.471817+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6","9d5f96cb-8d85-4216-b60e-809f39620ac6","f741041f-732a-481f-bc78-461bc55ada9b"]	fe251bf2-8d4f-4205-af0b-51160cd31e63	t	2017-08-25 16:49:19+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-08-25 15:48:05+00	4	\N	\N	9900	2018	Mencoba hal2 baru	\N	\N	t	\N	\N	\N	\N	\N
89dfef89-b49a-4ca2-9aa8-d7edc8f9e2b9	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	2ecc8748-a413-48f8-bb13-e82a4a38b7e6	fe5c13fb-aecd-4cc3-9efb-45e6db6b35f3	Fas	2018-04-03	2018-04-16	G-0192	2018-04-16	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-16 06:32:42+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-04-17 13:09:53.432239+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2018-04-17 13:09:29+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2018-04-17 13:09:53+00	3			\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	ae3234c8-ea30-45d2-a48e-367b8c32567d	ae3234c8-ea30-45d2-a48e-367b8c32567d	7	7ae49e60-c139-4744-820f-71939ed71eae
095fe615-c827-4a01-93e0-de6076d09ad2	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1		AUDIT DANA BOS (Coba Admin)	2018-09-03	2018-09-07	123123	2018-08-31	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 06:40:29+00	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-08-31 06:44:55.632432+00	t	f	["222cead4-f3bf-48d7-9541-84f60c66cac5","7a766876-545a-42a4-bb81-ca6fa59efff6","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","89488dc0-00e0-48c8-9e47-1f143b877878"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2018-08-31 06:44:56+00	\N	\N	\N	2		\N	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	7a766876-545a-42a4-bb81-ca6fa59efff6	3	7ae49e60-c139-4744-820f-71939ed71eae
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1	ee358350-0710-4d77-9a63-bd0f55e511ca	audit bos retno	2018-08-31	2018-09-03	3388	2018-08-31	f	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6","3d1d59ae-5115-44ee-8d8c-7db38a5e5200"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	7a766876-545a-42a4-bb81-ca6fa59efff6	ae3234c8-ea30-45d2-a48e-367b8c32567d	24	7ae49e60-c139-4744-820f-71939ed71eae
7ee14d19-aa59-4403-b0c1-85a7a0560da4	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1	17db2c9c-1dcc-41c2-8bac-56f71819af60	Audit SDN 09	2017-10-17	2017-10-19	9999	2017-10-17	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:52+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-10-17 02:21:45.73257+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","f741041f-732a-481f-bc78-461bc55ada9b","7a766876-545a-42a4-bb81-ca6fa59efff6","222cead4-f3bf-48d7-9541-84f60c66cac5"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-10-17 02:21:06+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-10-17 02:21:46+00	3	Setuju		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
81318580-6a2e-4c70-9b37-0662a9898536	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	70bc7a06-9b14-42c6-aaa3-f4c9e13a7303	18eae684-2980-42d8-a96b-466de5c8d52a	6789	2018-04-27	2018-04-29	77777	2018-04-12	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-12 10:04:27+00	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-08-31 07:16:16.308526+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2018-08-31 07:16:16+00	\N	\N	\N	2		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
f1ee1810-7e79-4695-8440-6c4ac296a5c7	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1		audit bos	2018-08-31	2018-09-03	332	2018-08-31	f	46a6c7ed-f739-4b61-a893-79646f60549b	2018-08-31 07:26:44+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	ae3234c8-ea30-45d2-a48e-367b8c32567d	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	3	7ae49e60-c139-4744-820f-71939ed71eae
43114d29-0a14-4b78-bb61-1c119d4b3f88	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1		Nadia	2018-08-31	2018-09-03	700/12/425.302/2018	2018-08-31	f	acbc412d-f29a-4931-b599-a37cee3aed69	2018-08-31 07:37:39+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","f741041f-732a-481f-bc78-461bc55ada9b","d016fde5-c284-4455-a44a-723ab77fa99f","9db069b6-02be-46d9-9d45-a89f2d741ec1"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	f741041f-732a-481f-bc78-461bc55ada9b	ae3234c8-ea30-45d2-a48e-367b8c32567d	d016fde5-c284-4455-a44a-723ab77fa99f	1	7ae49e60-c139-4744-820f-71939ed71eae
34e4a86b-0bb3-4d9b-b5a7-4ee047258887	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1		audit bos	2018-08-31	2018-09-02	5577	2018-08-31	f	896bb850-eca4-42f7-b75f-98cb8b67e0a6	2018-08-31 07:26:55+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6","89488dc0-00e0-48c8-9e47-1f143b877878","f741041f-732a-481f-bc78-461bc55ada9b"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	ae3234c8-ea30-45d2-a48e-367b8c32567d	f741041f-732a-481f-bc78-461bc55ada9b	7a766876-545a-42a4-bb81-ca6fa59efff6	8	7ae49e60-c139-4744-820f-71939ed71eae
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	697b7869-d11a-4dc3-81d8-eb3082f6db84	cf9dc3aa-d1aa-4a1a-ba66-fdda4c76eee6	Melaksanakan Monev DAK 	2017-10-09	2017-10-18	10238	2017-10-06	f	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	ed485c96-003c-4d05-b7ac-ac38a883257d	2017-10-20 03:04:28.025716+00	t	f	["ed485c96-003c-4d05-b7ac-ac38a883257d","ee570f4f-46de-4761-ae5c-b205c92d5c3e","bf5300ba-29bf-424c-bb09-e97e0e3f07c8","9d5f96cb-8d85-4216-b60e-809f39620ac6","bac8355c-d3e9-434b-82cf-9aaaadc4b9ae"]	ee570f4f-46de-4761-ae5c-b205c92d5c3e	t	2017-10-20 01:46:15+00	ed485c96-003c-4d05-b7ac-ac38a883257d	t	2017-10-20 03:04:28+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
9973f691-2564-44d0-b825-52b55884dd9c	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	6d341942-f365-43f0-a3a4-2ccaf7407854	5a7505ae-23e2-4048-a751-3ff7bfdc8606	Melakukan monitoring dan evaluasi PBJ yang bersumber dari DAK tahun anggaran 2017	2017-10-09	2017-10-18	10235	2017-10-05	f	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-10-20 01:31:36+00	95542b83-a537-4794-a103-b768408db707	2017-10-20 03:08:50.170383+00	t	f	["95542b83-a537-4794-a103-b768408db707","7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8","e54dd636-fb29-4ada-b95c-848c39ed6d03","998cdf4a-6b17-468e-9c07-186e1a724f6d","b5d26e6b-30f7-46a3-b7fe-03b66a235d86"]	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	t	2017-10-20 01:42:51+00	95542b83-a537-4794-a103-b768408db707	t	2017-10-20 03:08:50+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
20836be9-c3ac-46a2-a33e-932405df0b8b	\N	\N	519bbf54-6048-4a14-b184-898e521d9094	9839aa5d-3197-4ba3-9180-6713d08571b1		murtojo	2018-09-03	2018-09-10	77778	2018-09-03	f	6a962dbe-16da-4609-bb7c-77bdb9e10f1d	2018-08-31 07:39:41+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	ae3234c8-ea30-45d2-a48e-367b8c32567d	7a766876-545a-42a4-bb81-ca6fa59efff6	5	7ae49e60-c139-4744-820f-71939ed71eae
f21cff92-6997-48e3-a453-69bb316c132d	\N	\N	519bbf54-6048-4a14-b184-898e521d9094	9839aa5d-3197-4ba3-9180-6713d08571b1	caed4da2-ca46-4c41-9914-c8ec60604409	Judul	2017-08-15	2017-08-25	0002	2017-08-25	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-08-25 17:37:15+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-08-26 00:00:31.114364+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-08-25 19:06:52+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-08-26 00:00:31+00	3	\N	\N	000234	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
1c14b647-870a-43bd-aa06-f06ac1c8f56e	\N	\N	b48074ba-9e37-4e4a-be7a-0d7edc4c69a8	9839aa5d-3197-4ba3-9180-6713d08571b1	caed4da2-ca46-4c41-9914-c8ec60604409	test2	2017-08-27	2017-09-07	111	2017-08-27	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-08-26 22:55:56+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-08-27 08:13:40.41551+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-08-27 08:12:45+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-08-27 08:13:40+00	3	okg lg 2 bro	gas2	11100	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
359ea449-f314-4b68-99a0-ac42f4ba25d2	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	0296e63d-811f-42e8-aa03-feafaa36d547	caed4da2-ca46-4c41-9914-c8ec60604409	blabla	2017-08-29	2017-08-31	1200	2017-08-29	f	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-08-29 05:38:23+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-09-12 09:44:26.40579+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","7a766876-545a-42a4-bb81-ca6fa59efff6"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-09-01 06:40:52+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-12 09:44:26+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1	1043e161-abc3-4786-9e5d-fdaefbadeff1	Audit operasional pengelolaan keuangan dan aset SMP Negeri (Tim 5)	2017-09-06	2017-09-29	09715	2017-09-06	f	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	95542b83-a537-4794-a103-b768408db707	2017-09-26 11:23:49.088262+00	t	f	["b5d26e6b-30f7-46a3-b7fe-03b66a235d86","998cdf4a-6b17-468e-9c07-186e1a724f6d","e54dd636-fb29-4ada-b95c-848c39ed6d03","7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8","95542b83-a537-4794-a103-b768408db707"]	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	t	2017-09-26 11:17:47+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-26 11:23:49+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1	750ead79-11f7-49f1-bb17-1204e94501db	Audit operasional atas pengelolaan keuangan dan aset SMP Negeri (Tim 8)	2017-09-06	2017-09-29	09178	2017-09-06	f	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	ed485c96-003c-4d05-b7ac-ac38a883257d	2017-09-26 11:07:11.533751+00	t	f	["bf5300ba-29bf-424c-bb09-e97e0e3f07c8","bac8355c-d3e9-434b-82cf-9aaaadc4b9ae","ee570f4f-46de-4761-ae5c-b205c92d5c3e","ed485c96-003c-4d05-b7ac-ac38a883257d"]	ee570f4f-46de-4761-ae5c-b205c92d5c3e	t	2017-09-26 10:59:50+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-26 11:07:12+00	3	Setuju\r\nttd\r\nNengah		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
cec3e6f8-2a37-4514-a252-82968f15f2e3	\N	\N	a950e68a-4c7c-41ee-860b-a82878ead394	af36a328-a371-4095-b15e-a6c76143632c	8bb129e6-fcf4-4dbb-a155-b9edc2f5582e	PKS AUDIT DINAS SOSIAL	2017-09-12	2017-09-12	145612	2017-09-12	f	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-09-12 10:16:12+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-09-29 02:21:52.998663+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","9d5f96cb-8d85-4216-b60e-809f39620ac6","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-09-29 02:14:56+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-29 02:21:53+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1	51d0ae15-c1f4-4f1c-ac33-4a22b63f7d73	Audit operasional atas pengelolaan keuangan dan aset SMP Negeri (Tim 7)	2017-09-06	2017-09-29	09177	2017-09-06	f	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	ed485c96-003c-4d05-b7ac-ac38a883257d	2017-09-26 11:22:16.780076+00	t	f	["986b958b-1a98-44e4-a59b-c02a15620381","ebe58226-6813-4081-8377-b11d002e2be8","1b162438-55b1-4b5c-ac17-5699e9dfb1ed","ed485c96-003c-4d05-b7ac-ac38a883257d"]	1b162438-55b1-4b5c-ac17-5699e9dfb1ed	t	2017-09-26 11:20:22+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-26 11:22:17+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1	1410330d-ab77-43c8-ae25-976f63ef6525	Audit operasional atas pengelolaan keuangan dan aset SMP Negeri (Tim 4)	2017-09-06	2017-09-29	09174	2017-09-06	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	95542b83-a537-4794-a103-b768408db707	2017-09-26 11:23:35.315854+00	t	f	["2a9c364b-035d-4323-a042-7f6c3a5bedf4","29f41daa-38fd-43c3-8376-0942617e0fff","e003b944-5cc5-4c82-8d9b-1f4ad707e7dd","55fc1778-b710-47c8-a860-b519a03e23bf","95542b83-a537-4794-a103-b768408db707"]	55fc1778-b710-47c8-a860-b519a03e23bf	t	2017-09-26 11:15:22+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-26 11:23:35+00	3	Setuju\r\nttd \r\nSlamet Subowo		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
db0ffebb-9dbe-4772-87e8-f9444b28268e	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	70bc7a06-9b14-42c6-aaa3-f4c9e13a7303	67e632d6-0e25-46c9-84e1-1bf949e1d696	99991111	2018-04-12	2018-04-26	119922	2018-04-12	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-12 10:11:40+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-04-13 12:19:22.806744+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2018-04-13 12:19:03+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2018-04-13 12:19:23+00	3			119922-1	2018	\N	\N	2018-04-16	t	ae3234c8-ea30-45d2-a48e-367b8c32567d	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	222cead4-f3bf-48d7-9541-84f60c66cac5	\N	\N
274a4cfd-b67d-46e1-b695-afb76eb7975c	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	\N			2018-04-25	2018-04-25		2018-04-25	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-25 13:03:53+00	\N	\N	t	t	[]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2.5	7ae49e60-c139-4744-820f-71939ed71eae
c59ea1f7-e9cc-4e6f-9206-f90d21fe40f8	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1	ee358350-0710-4d77-9a63-bd0f55e511ca	audit bos ( silvy )	2018-09-03	2018-09-07	1119	2018-08-31	f	21ec1744-ea60-4bb3-b8ed-fc9ffc9918e6	2018-08-31 06:46:54+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	ae3234c8-ea30-45d2-a48e-367b8c32567d	1	7ae49e60-c139-4744-820f-71939ed71eae
58c39523-485e-4456-aadd-47cd90614072	\N	\N	351c7ddd-6659-48ff-a8d0-af6848770361	5c846f8d-4bc3-4b6c-9a29-b30b9e61d1e5	ee358350-0710-4d77-9a63-bd0f55e511ca	Funki Bener	2018-09-03	2018-09-28	198307132011 1 007	2018-08-30	f	b41048a4-1ff0-4ca1-949a-c8275e10ade8	2018-08-31 07:05:49+00	\N	\N	t	f	["bac8355c-d3e9-434b-82cf-9aaaadc4b9ae"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	5	7ae49e60-c139-4744-820f-71939ed71eae
6c8cd73f-7443-4bea-9ae3-184f42f07320	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	70bc7a06-9b14-42c6-aaa3-f4c9e13a7303	3da74478-2f78-462e-b581-a1184db27bf1	Monev PBJ DAK pada Dinas Pertanian	2017-10-09	2017-10-18	10232	2017-10-09	f	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-10-20 02:05:37.504869+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","f741041f-732a-481f-bc78-461bc55ada9b","d016fde5-c284-4455-a44a-723ab77fa99f","6f9cfc1d-dc27-4d78-8e51-51dcbf30117b","9db069b6-02be-46d9-9d45-a89f2d741ec1"]	f741041f-732a-481f-bc78-461bc55ada9b	t	2017-10-20 01:47:03+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2017-10-20 02:05:38+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
97ab28b5-8568-4f6f-94c5-837c3c2adb9e	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1	ee358350-0710-4d77-9a63-bd0f55e511ca	FRISCA	2018-08-31	2018-09-05	6677	2018-08-31	f	e03281a2-712d-497a-a86e-7c7e0a425052	2018-08-31 06:50:02+00	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-08-31 06:53:12.281348+00	t	f	["4ae755a7-3733-4f95-9bbe-02ce3a481fb7","222cead4-f3bf-48d7-9541-84f60c66cac5","ae3234c8-ea30-45d2-a48e-367b8c32567d","7a766876-545a-42a4-bb81-ca6fa59efff6","89488dc0-00e0-48c8-9e47-1f143b877878"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2018-08-31 06:53:12+00	\N	\N	\N	2		\N	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	89488dc0-00e0-48c8-9e47-1f143b877878	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	3	7ae49e60-c139-4744-820f-71939ed71eae
f79b7985-23bb-4156-a074-bc23365ded79	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	56d52b87-5bf4-4efb-a5fa-4262c228399f	29c0bf6a-4831-4aea-a634-9745dbaa3a26	Melaksanakan Monitoring dan Evaluasi  PBJ Sumber dana DAK TA 2017	2017-10-09	2017-10-18	10236	2017-10-05	f	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 01:31:41+00	ed485c96-003c-4d05-b7ac-ac38a883257d	2017-10-20 03:03:48.832617+00	t	f	["ed485c96-003c-4d05-b7ac-ac38a883257d","a30f21b6-918e-421d-835c-fac8228feed5","709e3413-c729-4435-ae26-eff4cb99f23b","29fe44b6-4bc3-4de2-b917-2b15713b546d","0188ba46-865b-43b4-9adf-44f6e4fe0cae"]	a30f21b6-918e-421d-835c-fac8228feed5	t	2017-10-20 02:51:52+00	ed485c96-003c-4d05-b7ac-ac38a883257d	t	2017-10-20 03:03:49+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
63216689-ab95-4628-bcec-8fe56198caee	\N	\N	519bbf54-6048-4a14-b184-898e521d9094	9839aa5d-3197-4ba3-9180-6713d08571b1	caed4da2-ca46-4c41-9914-c8ec60604409	Dinas Pendidikan	2017-10-11	2017-10-12	0192	2017-10-11	f	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-10-11 16:03:39+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-10-11 16:06:36.164967+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-10-11 16:06:02+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-10-11 16:06:36+00	3			123456	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
aa955757-a10f-4e35-915c-c08ad42c9ae3	\N	\N	d49b3332-1b95-43c0-8c8f-25842cf7f537	45c9cdfe-47e2-4a79-96da-7fcc10349c15	d962f7c4-3ce4-4c0a-8e48-4aa1a80209b3	Kinerja Per Orangan 2	2017-10-11	2017-10-27	9000	2017-10-11	f	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-10-11 16:19:52+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-10-11 16:20:45.577044+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6","9d5f96cb-8d85-4216-b60e-809f39620ac6"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-10-11 16:20:36+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-10-11 16:20:46+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
568a9e86-8b62-4125-a959-781ee7382863	\N	\N	d49b3332-1b95-43c0-8c8f-25842cf7f537	6d341942-f365-43f0-a3a4-2ccaf7407854	caed4da2-ca46-4c41-9914-c8ec60604409	Kinerja	2017-08-27	2017-08-31	12345678	2017-08-27	f	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-08-27 07:50:18+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-08-27 07:58:45.313495+00	t	f	["4ae755a7-3733-4f95-9bbe-02ce3a481fb7","ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","7a766876-545a-42a4-bb81-ca6fa59efff6","9d5f96cb-8d85-4216-b60e-809f39620ac6"]	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2017-08-27 07:56:11+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-08-27 07:58:45+00	3	sip bro	Sip bor	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
c7f2b0c5-f32b-443c-ab9e-bebaf0a701ad	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1		Bos Widi Pujo	2018-08-31	2018-09-03	212	2018-08-31	f	46a6c7ed-f739-4b61-a893-79646f60549b	2018-08-31 07:31:29+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","f741041f-732a-481f-bc78-461bc55ada9b","6f9cfc1d-dc27-4d78-8e51-51dcbf30117b"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	f741041f-732a-481f-bc78-461bc55ada9b	ae3234c8-ea30-45d2-a48e-367b8c32567d	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	3	7ae49e60-c139-4744-820f-71939ed71eae
07b42dd0-4867-4d42-b506-dc241d85c262	\N	\N	519bbf54-6048-4a14-b184-898e521d9094	9839aa5d-3197-4ba3-9180-6713d08571b1		andre	2018-09-01	2018-09-30	003/andre/425.302/2018	2018-08-31	f	0b78c0a2-0548-4ece-b97a-e557bdc882bf	2018-08-31 07:41:35+00	\N	\N	t	f	["4ae755a7-3733-4f95-9bbe-02ce3a481fb7","222cead4-f3bf-48d7-9541-84f60c66cac5","ae3234c8-ea30-45d2-a48e-367b8c32567d","7a766876-545a-42a4-bb81-ca6fa59efff6"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	ae3234c8-ea30-45d2-a48e-367b8c32567d	7a766876-545a-42a4-bb81-ca6fa59efff6	3	7ae49e60-c139-4744-820f-71939ed71eae
5ffb7fc3-7839-497f-b5e8-e936665e819f	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	70bc7a06-9b14-42c6-aaa3-f4c9e13a7303	88c87396-c02b-47d9-ba58-e5b87ad07939	123456	2018-03-21	2018-03-28	123456	2018-03-21	f	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-03-21 12:48:18+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	2018-03-21 12:57:18.427445+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6","89488dc0-00e0-48c8-9e47-1f143b877878"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2018-03-21 12:51:15+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2018-03-21 12:51:33+00	4			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
0334a10d-2102-4261-b84c-056720515d2c	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	70bc7a06-9b14-42c6-aaa3-f4c9e13a7303	9e67be82-a43a-4249-822f-0efd85095596	123	2018-04-01	2018-04-13	123112121212	2018-04-13	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-13 12:32:23+00	\N	\N	t	f	["95542b83-a537-4794-a103-b768408db707","a30f21b6-918e-421d-835c-fac8228feed5","e003b944-5cc5-4c82-8d9b-1f4ad707e7dd","2a9c364b-035d-4323-a042-7f6c3a5bedf4"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	2a9c364b-035d-4323-a042-7f6c3a5bedf4	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	95542b83-a537-4794-a103-b768408db707	\N	\N
d1856679-933f-4801-acd2-7d08f130161e	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	9839aa5d-3197-4ba3-9180-6713d08571b1	7bad5bff-f505-4b20-b574-249132e5d116	melakukan monev DAK	2017-10-09	2017-10-18	700/10231/429.060/2017	2017-10-05	f	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-10-20 01:33:21+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-10-20 02:05:24.520754+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6","89488dc0-00e0-48c8-9e47-1f143b877878"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-10-20 01:43:45+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2017-10-20 02:05:25+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
84759213-de14-4bea-8154-647cedb74bcf	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	56d52b87-5bf4-4efb-a5fa-4262c228399f	3d9c98a9-57f3-40a4-9568-d9d5690a2328	Teest irban dalnis	2018-04-12	2018-04-26	0000091	2018-04-12	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-12 10:00:39+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-04-13 12:16:23+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6","89488dc0-00e0-48c8-9e47-1f143b877878"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
bf117ea8-6a7b-40c3-9d6c-1fc9761e85f5	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1		audit bos (desy)	2018-08-31	2018-09-21	7009	2018-08-31	f	d2c224f6-d2d9-49b9-8285-cc953b2ebc5f	2018-08-31 06:55:40+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","d016fde5-c284-4455-a44a-723ab77fa99f","222cead4-f3bf-48d7-9541-84f60c66cac5","6f9cfc1d-dc27-4d78-8e51-51dcbf30117b"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	d016fde5-c284-4455-a44a-723ab77fa99f	2	7ae49e60-c139-4744-820f-71939ed71eae
8d671fbc-19f7-43ae-8e33-044a666d0ab3	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1		rencana ulfa	2018-08-31	2018-09-03	1234	2018-08-31	f	ac776057-ba95-4b14-b37a-ba2a9ff8c2b9	2018-08-31 07:21:44+00	\N	\N	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	ae3234c8-ea30-45d2-a48e-367b8c32567d	7a766876-545a-42a4-bb81-ca6fa59efff6	4	7ae49e60-c139-4744-820f-71939ed71eae
a1272441-cb74-427f-a423-dcb2c1bf5c13	\N	\N	519bbf54-6048-4a14-b184-898e521d9094	9839aa5d-3197-4ba3-9180-6713d08571b1		Bos reza	2018-09-03	2018-09-14	6789	2018-09-03	f	fc903300-d0fd-4fbd-a09e-2e6646bbc89f	2018-08-31 07:43:00+00	\N	\N	t	f	["4ae755a7-3733-4f95-9bbe-02ce3a481fb7","222cead4-f3bf-48d7-9541-84f60c66cac5","ae3234c8-ea30-45d2-a48e-367b8c32567d","7a766876-545a-42a4-bb81-ca6fa59efff6"]	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	ae3234c8-ea30-45d2-a48e-367b8c32567d	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	9	7ae49e60-c139-4744-820f-71939ed71eae
9375e424-6ea6-4841-a55f-1c1f79c8c6de	\N	\N	72f15fac-60fd-4108-9e82-e08357d8ecc0	70bc7a06-9b14-42c6-aaa3-f4c9e13a7303	1cb169bd-485d-430d-9a5b-e2ef077c2ed3	Kamu adalah auraku	2018-04-01	2018-04-13	88888	2018-04-14	f	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-04-13 12:40:03+00	95542b83-a537-4794-a103-b768408db707	2018-04-13 12:42:49.641889+00	t	f	["95542b83-a537-4794-a103-b768408db707","7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8","e54dd636-fb29-4ada-b95c-848c39ed6d03","998cdf4a-6b17-468e-9c07-186e1a724f6d"]	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	t	2018-04-13 12:42:18+00	95542b83-a537-4794-a103-b768408db707	t	2018-04-13 12:42:50+00	3			\N	\N	\N	\N	\N	t	95542b83-a537-4794-a103-b768408db707	e54dd636-fb29-4ada-b95c-848c39ed6d03	998cdf4a-6b17-468e-9c07-186e1a724f6d	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1	bb0d243b-5a3a-4fa8-ab6c-613b1c316218	Audit operasional pengelolaan keuangan dan aset SMP Negeri (Tim 2)	2017-09-06	2017-09-29	09172	2017-09-06	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-20 07:12:47+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-09-26 11:05:29.8181+00	t	f	["9db069b6-02be-46d9-9d45-a89f2d741ec1","6f9cfc1d-dc27-4d78-8e51-51dcbf30117b","d016fde5-c284-4455-a44a-723ab77fa99f","f741041f-732a-481f-bc78-461bc55ada9b"]	f741041f-732a-481f-bc78-461bc55ada9b	t	2017-09-26 11:03:39+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-26 11:05:30+00	3	Setuju\r\nttd \r\nChoir E	Setuju\r\nttd\r\nMarwoto	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
bb7b542c-24ec-493b-80b9-ebbbad3ebd94	\N	\N	d49b3332-1b95-43c0-8c8f-25842cf7f537	6e6d44de-7d44-4857-b01f-07f92644e9ef	caed4da2-ca46-4c41-9914-c8ec60604409	Kinerja	2017-08-13	2017-09-07	123456	2017-08-27	f	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-08-27 04:15:39+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-08-29 01:58:47.746174+00	t	f	["ae3234c8-ea30-45d2-a48e-367b8c32567d","222cead4-f3bf-48d7-9541-84f60c66cac5","4ae755a7-3733-4f95-9bbe-02ce3a481fb7","7a766876-545a-42a4-bb81-ca6fa59efff6","9d5f96cb-8d85-4216-b60e-809f39620ac6"]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-08-29 01:54:29+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	f	2017-08-29 01:58:48+00	0			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	\N	\N	47610b8d-0476-4174-9128-996685a51354	9839aa5d-3197-4ba3-9180-6713d08571b1	f0040b9d-dfc9-45e9-8e58-d34e79c02616	Audit operasional pengelolaan keuangan dan aset SMP Negeri (Tim 6)	2017-09-06	2017-09-29	09176	2017-09-06	f	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	ed485c96-003c-4d05-b7ac-ac38a883257d	2017-09-26 11:21:56.027498+00	t	f	["29fe44b6-4bc3-4de2-b917-2b15713b546d","709e3413-c729-4435-ae26-eff4cb99f23b","a30f21b6-918e-421d-835c-fac8228feed5","ed485c96-003c-4d05-b7ac-ac38a883257d"]	a30f21b6-918e-421d-835c-fac8228feed5	t	2017-09-26 11:19:18+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-26 11:21:56+00	3			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: perencanaan_aturan; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.perencanaan_aturan (id, aturan) FROM stdin;
\.


--
-- Data for Name: perencanaan_detail; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.perencanaan_detail (id, tao, rencana_by, rencana_date, realisasi_by, realisasi_date, file, app_sv1, app_sv1_status, app_sv1_tstamp, app_sv2, app_sv2_status, app_sv2_tstamp, created_by, created_at, modified_by, modified_at, status, app_sv3, app_sv3_status, app_sv3_tstamp, progres, app_sv1_note, app_sv2_note, app_sv3_note, kesimpulan, jumlah_hari) FROM stdin;
05275cf2-b282-4653-8c37-a0d7f8923a62	c5c56164-40c4-43fa-8495-dc5165718ebf	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	8a6bbfba-798e-4593-9d57-d0d5a88b2c4a	986b958b-1a98-44e4-a59b-c02a15620381	2017-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	30ef6912-451f-4c64-ac71-92a4bf5736c9	986b958b-1a98-44e4-a59b-c02a15620381	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	823a2b84-ca0e-42a6-9792-1c1b19d3dfb6	1b162438-55b1-4b5c-ac17-5699e9dfb1ed	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	61c0607b-868f-47af-a90d-dde0c42a3af4	986b958b-1a98-44e4-a59b-c02a15620381	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	64ecb1a9-91f0-4b16-8360-11da3e655574	986b958b-1a98-44e4-a59b-c02a15620381	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	8d53ae6e-9502-4103-8603-ecd615b32d94	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	6b69d2bd-631b-423a-a5c4-15d7ba2adb89	986b958b-1a98-44e4-a59b-c02a15620381	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	fd3066d2-c8c0-460e-a52c-790cd50b7469	986b958b-1a98-44e4-a59b-c02a15620381	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	505f7604-ccda-49df-83e7-6f5ef6c9bbb0	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	5db3d0f4-1803-444c-97de-d121100a62dc	986b958b-1a98-44e4-a59b-c02a15620381	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	e32bf68b-6224-4d54-8162-cb96836a27d7	986b958b-1a98-44e4-a59b-c02a15620381	2017-10-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	5250d9b9-8156-4a02-ad3b-dde51a608188	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	47ea222d-05f8-4b95-a130-3e7bb6453ff0	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	57ba24b4-bde1-4749-88ce-ad7e5e658c34	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
20836be9-c3ac-46a2-a33e-932405df0b8b	2dc5180a-6ecb-40db-a6e9-742109c4e82e	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	6a962dbe-16da-4609-bb7c-77bdb9e10f1d	2018-08-31 07:39:41+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	2
20836be9-c3ac-46a2-a33e-932405df0b8b	28d7e52a-59e8-4a1e-b43f-4a48cdca3e22	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	6a962dbe-16da-4609-bb7c-77bdb9e10f1d	2018-08-31 07:39:41+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	3
05275cf2-b282-4653-8c37-a0d7f8923a62	1b72e119-165e-401c-908c-83b0438780e9	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-10	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-10	[{"nama":"KK0813007-1.xlsx","file":"282c9e044a5988b480e0208c174b64c7.xlsx"}]	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	Terdapat kekurangan volume pekerjaan.	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	80636594-116a-44e8-b4be-e8b064986a06	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-09	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-09	[{"nama":"KKA PERMINTAAN DATA.xlsx","file":"6540980afd2820077602c4f9d0a477ab.xlsx"}]	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	DATA TELAH DITERIMA	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	493f29c3-5db5-470d-88a1-d50c04a963a6	986b958b-1a98-44e4-a59b-c02a15620381	2017-10-09	986b958b-1a98-44e4-a59b-c02a15620381	2017-10-09	[{"nama":"KKA SMPN 2 Gambiran 2017.xlsx","file":"dee5caf3455d779ddf800449445b7b49.xlsx"}]	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	\N	\N
05275cf2-b282-4653-8c37-a0d7f8923a62	2e0ee088-c298-4238-9d84-1bcac8f05b4e	1b162438-55b1-4b5c-ac17-5699e9dfb1ed	2017-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-10-20 01:36:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	53983413-54bf-4a94-815e-82ccceecacc3	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-08-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2018-09-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	51dd4682-183d-4989-84c2-18baefbd4539	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	b7689c74-5a86-4711-b170-6deb99f7137b	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	e7a5563b-215a-44a1-a523-a1e2f47cb694	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	62fc3064-8e6a-4e97-a185-fb1156c0c16a	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	28ca9f0f-b7c0-442d-9e59-cf89b711af61	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	acd1709e-5bb3-436c-b956-d1cfebc17b65	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	4453d1e6-05ed-4374-8344-a23915bf425b	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	c7aa0a46-3ae1-4c52-a708-98c17cec316e	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	5cbb9528-fa19-449b-a05f-ed0abfc36a00	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	49f6c09c-485d-458b-aae9-c3ff883f4542	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	64722e98-772e-4a28-94c3-03d3cb483320	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	04b0f150-6dca-4d93-b29f-3eef3a302281	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	72eed5d9-87a3-424d-abbf-e74972466494	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	06709067-0f75-4b63-96b3-47b501e4674c	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	90ef9c22-9163-4459-8dde-319cb03dd221	2018-08-31 07:01:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	4
f21cff92-6997-48e3-a453-69bb316c132d	1b6aec97-c1c4-460e-ba0d-ebf406c3ceeb	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-08-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-08-25 17:37:15+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	fb1b3834-b5d2-4599-8273-a49881db8d36	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	313a30e0-7f5b-4b15-ac3f-708e03a3c990	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	72eed5d9-87a3-424d-abbf-e74972466494	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	06709067-0f75-4b63-96b3-47b501e4674c	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	5d03a7f2-fbdc-4dcb-becd-75c08489febd	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	52cc2b91-fe8b-4097-9d9e-3f81a889153f	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	470396d6-b06a-4147-b47b-7d55859ce577	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	008749c7-efb0-47c7-b54d-6a2503ba2ebd	998cdf4a-6b17-468e-9c07-186e1a724f6d	2017-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	53449178-793e-47f9-892d-ac58f1375831	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	2017-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	37bab70a-0dd2-4786-9c24-9542445e738d	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	49f6c09c-485d-458b-aae9-c3ff883f4542	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	64722e98-772e-4a28-94c3-03d3cb483320	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	04b0f150-6dca-4d93-b29f-3eef3a302281	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	45d56475-668b-41e4-bdc0-3634c76f35a8	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	fb1b3834-b5d2-4599-8273-a49881db8d36	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	313a30e0-7f5b-4b15-ac3f-708e03a3c990	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	72eed5d9-87a3-424d-abbf-e74972466494	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	06709067-0f75-4b63-96b3-47b501e4674c	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	53983413-54bf-4a94-815e-82ccceecacc3	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	51dd4682-183d-4989-84c2-18baefbd4539	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	b7689c74-5a86-4711-b170-6deb99f7137b	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	e7a5563b-215a-44a1-a523-a1e2f47cb694	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	62fc3064-8e6a-4e97-a185-fb1156c0c16a	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	28ca9f0f-b7c0-442d-9e59-cf89b711af61	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	acd1709e-5bb3-436c-b956-d1cfebc17b65	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	4453d1e6-05ed-4374-8344-a23915bf425b	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	c7aa0a46-3ae1-4c52-a708-98c17cec316e	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	5cbb9528-fa19-449b-a05f-ed0abfc36a00	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	c90bd32b-1b3f-4952-ad96-d498e6b56364	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	336014ad-e1cd-41c4-bf97-b8438b78b12f	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	149643dc-f2b1-4426-8f69-013f48e4d7bd	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	dc315220-3e80-4d7a-841e-dda52b0a1f84	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	4cdccf23-dc54-4583-b560-8ae93efcd3e8	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	5d03a7f2-fbdc-4dcb-becd-75c08489febd	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	52cc2b91-fe8b-4097-9d9e-3f81a889153f	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	470396d6-b06a-4147-b47b-7d55859ce577	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	008749c7-efb0-47c7-b54d-6a2503ba2ebd	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	53449178-793e-47f9-892d-ac58f1375831	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	37bab70a-0dd2-4786-9c24-9542445e738d	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	49f6c09c-485d-458b-aae9-c3ff883f4542	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	64722e98-772e-4a28-94c3-03d3cb483320	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	04b0f150-6dca-4d93-b29f-3eef3a302281	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	45d56475-668b-41e4-bdc0-3634c76f35a8	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	fb1b3834-b5d2-4599-8273-a49881db8d36	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	80636594-116a-44e8-b4be-e8b064986a06	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	c5c56164-40c4-43fa-8495-dc5165718ebf	ee570f4f-46de-4761-ae5c-b205c92d5c3e	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	493f29c3-5db5-470d-88a1-d50c04a963a6	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	8a6bbfba-798e-4593-9d57-d0d5a88b2c4a	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	30ef6912-451f-4c64-ac71-92a4bf5736c9	ee570f4f-46de-4761-ae5c-b205c92d5c3e	2017-10-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	823a2b84-ca0e-42a6-9792-1c1b19d3dfb6	ee570f4f-46de-4761-ae5c-b205c92d5c3e	2017-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	53983413-54bf-4a94-815e-82ccceecacc3	f741041f-732a-481f-bc78-461bc55ada9b	2017-10-19	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-10-18	[{"nama":"64a5db3c913aaf8316ed3fac980d12e2.xlsx","file":"ce2226a0c24454e35fd5c586b6547064.xlsx"}]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-10-17 02:26:35.476668+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2017-10-17 02:27:01.742875+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	2017-10-17 02:27:58.504937+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-10-17 02:27:58.504937+00	4		\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	fb1b3834-b5d2-4599-8273-a49881db8d36	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	313a30e0-7f5b-4b15-ac3f-708e03a3c990	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	313a30e0-7f5b-4b15-ac3f-708e03a3c990	709e3413-c729-4435-ae26-eff4cb99f23b	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	72eed5d9-87a3-424d-abbf-e74972466494	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
9cb18ed2-4658-481f-9f74-e40105dc188b	06709067-0f75-4b63-96b3-47b501e4674c	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	a30f21b6-918e-421d-835c-fac8228feed5	2017-09-26 10:19:44+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	53983413-54bf-4a94-815e-82ccceecacc3	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	51dd4682-183d-4989-84c2-18baefbd4539	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	b7689c74-5a86-4711-b170-6deb99f7137b	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	e7a5563b-215a-44a1-a523-a1e2f47cb694	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	62fc3064-8e6a-4e97-a185-fb1156c0c16a	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	28ca9f0f-b7c0-442d-9e59-cf89b711af61	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	acd1709e-5bb3-436c-b956-d1cfebc17b65	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	4453d1e6-05ed-4374-8344-a23915bf425b	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	c7aa0a46-3ae1-4c52-a708-98c17cec316e	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	5cbb9528-fa19-449b-a05f-ed0abfc36a00	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	c90bd32b-1b3f-4952-ad96-d498e6b56364	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	336014ad-e1cd-41c4-bf97-b8438b78b12f	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	149643dc-f2b1-4426-8f69-013f48e4d7bd	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	dc315220-3e80-4d7a-841e-dda52b0a1f84	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	4cdccf23-dc54-4583-b560-8ae93efcd3e8	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	5d03a7f2-fbdc-4dcb-becd-75c08489febd	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	52cc2b91-fe8b-4097-9d9e-3f81a889153f	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	470396d6-b06a-4147-b47b-7d55859ce577	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	008749c7-efb0-47c7-b54d-6a2503ba2ebd	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	53449178-793e-47f9-892d-ac58f1375831	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	37bab70a-0dd2-4786-9c24-9542445e738d	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	49f6c09c-485d-458b-aae9-c3ff883f4542	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	64722e98-772e-4a28-94c3-03d3cb483320	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	04b0f150-6dca-4d93-b29f-3eef3a302281	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	45d56475-668b-41e4-bdc0-3634c76f35a8	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	fb1b3834-b5d2-4599-8273-a49881db8d36	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	313a30e0-7f5b-4b15-ac3f-708e03a3c990	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	72eed5d9-87a3-424d-abbf-e74972466494	1b162438-55b1-4b5c-ac17-5699e9dfb1ed	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4114359b-983c-4020-a06f-65a7c7913b15	06709067-0f75-4b63-96b3-47b501e4674c	986b958b-1a98-44e4-a59b-c02a15620381	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	ebe58226-6813-4081-8377-b11d002e2be8	2017-09-26 10:32:42+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	53983413-54bf-4a94-815e-82ccceecacc3	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	61c0607b-868f-47af-a90d-dde0c42a3af4	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	1b72e119-165e-401c-908c-83b0438780e9	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	64ecb1a9-91f0-4b16-8360-11da3e655574	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-10-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	8d53ae6e-9502-4103-8603-ecd615b32d94	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-10-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	6b69d2bd-631b-423a-a5c4-15d7ba2adb89	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	fd3066d2-c8c0-460e-a52c-790cd50b7469	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	505f7604-ccda-49df-83e7-6f5ef6c9bbb0	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-10-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	5db3d0f4-1803-444c-97de-d121100a62dc	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	e32bf68b-6224-4d54-8162-cb96836a27d7	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-10-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-10-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	51dd4682-183d-4989-84c2-18baefbd4539	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	b7689c74-5a86-4711-b170-6deb99f7137b	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-10-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-10-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-11-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	e7a5563b-215a-44a1-a523-a1e2f47cb694	f741041f-732a-481f-bc78-461bc55ada9b	2017-10-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	62fc3064-8e6a-4e97-a185-fb1156c0c16a	f741041f-732a-481f-bc78-461bc55ada9b	2017-10-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	28ca9f0f-b7c0-442d-9e59-cf89b711af61	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	acd1709e-5bb3-436c-b956-d1cfebc17b65	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-10-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	4453d1e6-05ed-4374-8344-a23915bf425b	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-10-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	c7aa0a46-3ae1-4c52-a708-98c17cec316e	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-11-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	5cbb9528-fa19-449b-a05f-ed0abfc36a00	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	c90bd32b-1b3f-4952-ad96-d498e6b56364	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-10-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-10-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	336014ad-e1cd-41c4-bf97-b8438b78b12f	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-10-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	149643dc-f2b1-4426-8f69-013f48e4d7bd	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-10-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	dc315220-3e80-4d7a-841e-dda52b0a1f84	f741041f-732a-481f-bc78-461bc55ada9b	2017-10-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	53983413-54bf-4a94-815e-82ccceecacc3	70bf9ad1-3775-4be7-abab-9128837416ad	2017-09-07	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13	[{"nama":"ISIAN","file":"85a70e48843afb8e4f04cde744e2a8d5.xlsx"}]	dd7ae175-271f-467e-979e-8d2d709b1f0e	t	2017-09-13 12:01:51.195301+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2017-10-11 06:57:05.799129+00	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-10-11 06:57:05.799129+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	3		\N	\N	Telah dilakukan entry meeting, dan Pimpinan Obrik telah bersedia menerima Tim	\N
f21cff92-6997-48e3-a453-69bb316c132d	3e1c2107-b0da-450d-8afa-b9380f375942	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-08-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-08-25 17:37:15+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
f21cff92-6997-48e3-a453-69bb316c132d	73639b26-c3bc-4978-85e7-8797454f1dc7	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-08-30	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-08-25 17:37:15+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
4ac7b90f-d4ad-4bba-bad1-94493366203d	1036dac7-ccb6-47fd-a958-5a5c19f3df62	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-08-08	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-08-30	[{"nama":"ISIAN","file":"229d5484898b412c37f07ada1a2a0784.jpg"}]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-09-01 10:32:53.729189+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2017-09-01 11:46:33.13469+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-08-25 11:52:16+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	2017-09-01 12:10:29.492446+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-01 12:10:29.492446+00	4	Deal	Deal Juga	deal OK	\N	\N
4ac7b90f-d4ad-4bba-bad1-94493366203d	a5d15d37-4b03-4f9c-95b0-a01f970891f7	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-08-01	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-08-30	[{"nama":"ISIAN","file":"679acc999d674eb24788c609f1810b49.jpg"}]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-09-06 11:23:40.990791+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2017-09-06 11:24:36.100043+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-08-25 11:52:16+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	2017-09-06 11:25:12.624828+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-06 11:25:12.624828+00	4	gg	\N	\N	\N	\N
4ac7b90f-d4ad-4bba-bad1-94493366203d	3e1c2107-b0da-450d-8afa-b9380f375942	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-08-21	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-08-29	[{"nama":"ISIAN","file":"d945e715d23051b51ab03fd8342a5306.pdf"}]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-09-06 11:35:18.801719+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2017-09-06 11:39:02.505688+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-08-25 11:52:16+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	2017-09-06 11:40:07.331902+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-06 11:40:07.331902+00	4	asd	\N	\N	\N	\N
4ac7b90f-d4ad-4bba-bad1-94493366203d	73639b26-c3bc-4978-85e7-8797454f1dc7	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-08-10	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-09-06	[{"nama":"ISIAN","file":"37b9ae2f60dddd6ee16c2eca734921cd.jpg"}]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-09-06 11:38:08.68569+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2017-09-06 11:39:15.408707+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-08-25 11:52:16+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	2017-09-06 11:40:19.540493+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-06 11:40:19.540493+00	4		\N	\N	\N	\N
4ac7b90f-d4ad-4bba-bad1-94493366203d	1b6aec97-c1c4-460e-ba0d-ebf406c3ceeb	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-08-31	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-08-31	[{"nama":"ISIAN","file":"210cc9cdc2b81f1b2178a3d7fca601e2.jpg"}]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-09-06 11:38:18.222218+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2017-09-06 11:39:30.682853+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-08-25 11:52:16+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	2017-09-06 11:40:38.471817+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2017-09-06 11:40:38.471817+00	4		\N	\N	\N	\N
f21cff92-6997-48e3-a453-69bb316c132d	a5d15d37-4b03-4f9c-95b0-a01f970891f7	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-08-28	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-09-11	[{"nama":"ISIAN","file":"012c401101e3824a0f7b68465904331d.pdf"}]	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-08-25 17:37:15+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	1	\N	\N	\N	KK01010088-2 2\r\nkesimpulannya	\N
185a85fd-c805-4697-9719-21b017164891	53983413-54bf-4a94-815e-82ccceecacc3	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	51dd4682-183d-4989-84c2-18baefbd4539	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	b7689c74-5a86-4711-b170-6deb99f7137b	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	f741041f-732a-481f-bc78-461bc55ada9b	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	4cdccf23-dc54-4583-b560-8ae93efcd3e8	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-23	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	5d03a7f2-fbdc-4dcb-becd-75c08489febd	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-10-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	52cc2b91-fe8b-4097-9d9e-3f81a889153f	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	470396d6-b06a-4147-b47b-7d55859ce577	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	008749c7-efb0-47c7-b54d-6a2503ba2ebd	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	53449178-793e-47f9-892d-ac58f1375831	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	37bab70a-0dd2-4786-9c24-9542445e738d	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	49f6c09c-485d-458b-aae9-c3ff883f4542	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	64722e98-772e-4a28-94c3-03d3cb483320	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	51dd4682-183d-4989-84c2-18baefbd4539	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-06	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-09-06	[{"nama":"ISIAN","file":"34d0a77701e71a30633b9c1087dcd181.pdf"}]	f741041f-732a-481f-bc78-461bc55ada9b	t	2018-10-30 21:09:54.306305+00	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	f741041f-732a-481f-bc78-461bc55ada9b	2018-10-30 21:09:54.306305+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	2		\N	\N	--	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	04b0f150-6dca-4d93-b29f-3eef3a302281	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
7ee14d19-aa59-4403-b0c1-85a7a0560da4	45d56475-668b-41e4-bdc0-3634c76f35a8	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-10-24	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	72eed5d9-87a3-424d-abbf-e74972466494	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	06709067-0f75-4b63-96b3-47b501e4674c	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
a109ef9e-68e4-48c0-8d25-3318e4154d07	2e0ee088-c298-4238-9d84-1bcac8f05b4e	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-04-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-12 10:02:19+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	3
d26350c7-bc66-4098-ab11-202d883e3c3d	51dd4682-183d-4989-84c2-18baefbd4539	ac513f02-a71e-4764-b78c-216b62ea590f	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	b7689c74-5a86-4711-b170-6deb99f7137b	ac513f02-a71e-4764-b78c-216b62ea590f	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	ac513f02-a71e-4764-b78c-216b62ea590f	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	70bf9ad1-3775-4be7-abab-9128837416ad	2017-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	313a30e0-7f5b-4b15-ac3f-708e03a3c990	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	72eed5d9-87a3-424d-abbf-e74972466494	dd7ae175-271f-467e-979e-8d2d709b1f0e	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	06709067-0f75-4b63-96b3-47b501e4674c	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	53983413-54bf-4a94-815e-82ccceecacc3	2a9c364b-035d-4323-a042-7f6c3a5bedf4	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	b7689c74-5a86-4711-b170-6deb99f7137b	29f41daa-38fd-43c3-8376-0942617e0fff	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	2a9c364b-035d-4323-a042-7f6c3a5bedf4	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	2a9c364b-035d-4323-a042-7f6c3a5bedf4	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	29f41daa-38fd-43c3-8376-0942617e0fff	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	e7a5563b-215a-44a1-a523-a1e2f47cb694	2a9c364b-035d-4323-a042-7f6c3a5bedf4	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	acd1709e-5bb3-436c-b956-d1cfebc17b65	29f41daa-38fd-43c3-8376-0942617e0fff	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	4453d1e6-05ed-4374-8344-a23915bf425b	29f41daa-38fd-43c3-8376-0942617e0fff	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	c7aa0a46-3ae1-4c52-a708-98c17cec316e	29f41daa-38fd-43c3-8376-0942617e0fff	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	5cbb9528-fa19-449b-a05f-ed0abfc36a00	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	c90bd32b-1b3f-4952-ad96-d498e6b56364	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	336014ad-e1cd-41c4-bf97-b8438b78b12f	2a9c364b-035d-4323-a042-7f6c3a5bedf4	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	149643dc-f2b1-4426-8f69-013f48e4d7bd	29f41daa-38fd-43c3-8376-0942617e0fff	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	dc315220-3e80-4d7a-841e-dda52b0a1f84	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	2a9c364b-035d-4323-a042-7f6c3a5bedf4	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	2a9c364b-035d-4323-a042-7f6c3a5bedf4	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	52cc2b91-fe8b-4097-9d9e-3f81a889153f	29f41daa-38fd-43c3-8376-0942617e0fff	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	470396d6-b06a-4147-b47b-7d55859ce577	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	008749c7-efb0-47c7-b54d-6a2503ba2ebd	2a9c364b-035d-4323-a042-7f6c3a5bedf4	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	53449178-793e-47f9-892d-ac58f1375831	29f41daa-38fd-43c3-8376-0942617e0fff	2017-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	37bab70a-0dd2-4786-9c24-9542445e738d	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	49f6c09c-485d-458b-aae9-c3ff883f4542	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	64722e98-772e-4a28-94c3-03d3cb483320	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	04b0f150-6dca-4d93-b29f-3eef3a302281	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	62fc3064-8e6a-4e97-a185-fb1156c0c16a	2a9c364b-035d-4323-a042-7f6c3a5bedf4	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	28ca9f0f-b7c0-442d-9e59-cf89b711af61	2a9c364b-035d-4323-a042-7f6c3a5bedf4	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	45d56475-668b-41e4-bdc0-3634c76f35a8	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	fb1b3834-b5d2-4599-8273-a49881db8d36	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	313a30e0-7f5b-4b15-ac3f-708e03a3c990	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	72eed5d9-87a3-424d-abbf-e74972466494	55fc1778-b710-47c8-a860-b519a03e23bf	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	06709067-0f75-4b63-96b3-47b501e4674c	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	53983413-54bf-4a94-815e-82ccceecacc3	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	5250d9b9-8156-4a02-ad3b-dde51a608188	ee570f4f-46de-4761-ae5c-b205c92d5c3e	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	47ea222d-05f8-4b95-a130-3e7bb6453ff0	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	57ba24b4-bde1-4749-88ce-ad7e5e658c34	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	70bf9ad1-3775-4be7-abab-9128837416ad	2017-09-08	70bf9ad1-3775-4be7-abab-9128837416ad	2017-10-20	[{"nama":"mahfud.xlsx","file":"b18bd711e51346a5004895ffd7c81c7e.xlsx"}]	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	1	\N	\N	\N	zero	\N
58c39523-485e-4456-aadd-47cd90614072	040c3a08-6eb5-46e8-9ce2-5ce3abbd68cd	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	b41048a4-1ff0-4ca1-949a-c8275e10ade8	2018-08-31 07:05:49+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	2
58c39523-485e-4456-aadd-47cd90614072	2ab1af68-84e0-4535-a4c7-4812c8a4473b	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2018-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	b41048a4-1ff0-4ca1-949a-c8275e10ade8	2018-08-31 07:05:49+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	3
58c39523-485e-4456-aadd-47cd90614072	33a3fba6-c0be-4095-bf13-e2e193d298c3	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2018-09-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	b41048a4-1ff0-4ca1-949a-c8275e10ade8	2018-08-31 07:05:49+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	e7a5563b-215a-44a1-a523-a1e2f47cb694	70bf9ad1-3775-4be7-abab-9128837416ad	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	62fc3064-8e6a-4e97-a185-fb1156c0c16a	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	28ca9f0f-b7c0-442d-9e59-cf89b711af61	ac513f02-a71e-4764-b78c-216b62ea590f	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	acd1709e-5bb3-436c-b956-d1cfebc17b65	ac513f02-a71e-4764-b78c-216b62ea590f	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	4453d1e6-05ed-4374-8344-a23915bf425b	ac513f02-a71e-4764-b78c-216b62ea590f	2017-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	c7aa0a46-3ae1-4c52-a708-98c17cec316e	70bf9ad1-3775-4be7-abab-9128837416ad	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	70bf9ad1-3775-4be7-abab-9128837416ad	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	70bf9ad1-3775-4be7-abab-9128837416ad	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	5cbb9528-fa19-449b-a05f-ed0abfc36a00	ac513f02-a71e-4764-b78c-216b62ea590f	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	c90bd32b-1b3f-4952-ad96-d498e6b56364	ac513f02-a71e-4764-b78c-216b62ea590f	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	ac513f02-a71e-4764-b78c-216b62ea590f	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	336014ad-e1cd-41c4-bf97-b8438b78b12f	70bf9ad1-3775-4be7-abab-9128837416ad	2017-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	149643dc-f2b1-4426-8f69-013f48e4d7bd	70bf9ad1-3775-4be7-abab-9128837416ad	2017-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	dc315220-3e80-4d7a-841e-dda52b0a1f84	70bf9ad1-3775-4be7-abab-9128837416ad	2017-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	4cdccf23-dc54-4583-b560-8ae93efcd3e8	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	5d03a7f2-fbdc-4dcb-becd-75c08489febd	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	52cc2b91-fe8b-4097-9d9e-3f81a889153f	ac513f02-a71e-4764-b78c-216b62ea590f	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	470396d6-b06a-4147-b47b-7d55859ce577	ac513f02-a71e-4764-b78c-216b62ea590f	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	008749c7-efb0-47c7-b54d-6a2503ba2ebd	70bf9ad1-3775-4be7-abab-9128837416ad	2017-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	53449178-793e-47f9-892d-ac58f1375831	70bf9ad1-3775-4be7-abab-9128837416ad	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	37bab70a-0dd2-4786-9c24-9542445e738d	70bf9ad1-3775-4be7-abab-9128837416ad	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	49f6c09c-485d-458b-aae9-c3ff883f4542	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	64722e98-772e-4a28-94c3-03d3cb483320	dd7ae175-271f-467e-979e-8d2d709b1f0e	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	04b0f150-6dca-4d93-b29f-3eef3a302281	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	45d56475-668b-41e4-bdc0-3634c76f35a8	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
d26350c7-bc66-4098-ab11-202d883e3c3d	fb1b3834-b5d2-4599-8273-a49881db8d36	dd7ae175-271f-467e-979e-8d2d709b1f0e	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	2017-09-13 11:45:26+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	2e0ee088-c298-4238-9d84-1bcac8f05b4e	f741041f-732a-481f-bc78-461bc55ada9b	2017-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	80636594-116a-44e8-b4be-e8b064986a06	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	493f29c3-5db5-470d-88a1-d50c04a963a6	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	8a6bbfba-798e-4593-9d57-d0d5a88b2c4a	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	30ef6912-451f-4c64-ac71-92a4bf5736c9	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	823a2b84-ca0e-42a6-9792-1c1b19d3dfb6	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	61c0607b-868f-47af-a90d-dde0c42a3af4	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	1b72e119-165e-401c-908c-83b0438780e9	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-10-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	64ecb1a9-91f0-4b16-8360-11da3e655574	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	8d53ae6e-9502-4103-8603-ecd615b32d94	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	6b69d2bd-631b-423a-a5c4-15d7ba2adb89	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-10-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	fd3066d2-c8c0-460e-a52c-790cd50b7469	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	505f7604-ccda-49df-83e7-6f5ef6c9bbb0	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	5db3d0f4-1803-444c-97de-d121100a62dc	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	e32bf68b-6224-4d54-8162-cb96836a27d7	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	5250d9b9-8156-4a02-ad3b-dde51a608188	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	47ea222d-05f8-4b95-a130-3e7bb6453ff0	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
6c8cd73f-7443-4bea-9ae3-184f42f07320	57ba24b4-bde1-4749-88ce-ad7e5e658c34	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-10-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
07b42dd0-4867-4d42-b506-dc241d85c262	2dc5180a-6ecb-40db-a6e9-742109c4e82e	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	0b78c0a2-0548-4ece-b97a-e557bdc882bf	2018-08-31 07:41:35+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
6c8cd73f-7443-4bea-9ae3-184f42f07320	c5c56164-40c4-43fa-8495-dc5165718ebf	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-10	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20	[{"nama":"kka-kelengkapan kontrak.xls","file":"43b8faef70fdc82c1f590e5e4d8eb7e7.xls"}]	f741041f-732a-481f-bc78-461bc55ada9b	t	2017-10-20 03:16:17.023897+00	\N	\N	\N	d016fde5-c284-4455-a44a-723ab77fa99f	2017-10-20 01:46:08+00	f741041f-732a-481f-bc78-461bc55ada9b	2017-10-20 03:16:17.023897+00	t	\N	\N	\N	2		\N	\N	\N	\N
81318580-6a2e-4c70-9b37-0662a9898536	2e0ee088-c298-4238-9d84-1bcac8f05b4e	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-04-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-12 10:04:27+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	4
8d671fbc-19f7-43ae-8e33-044a666d0ab3	53983413-54bf-4a94-815e-82ccceecacc3	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-08-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	ac776057-ba95-4b14-b37a-ba2a9ff8c2b9	2018-08-31 07:21:44+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
8d671fbc-19f7-43ae-8e33-044a666d0ab3	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-09-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	ac776057-ba95-4b14-b37a-ba2a9ff8c2b9	2018-08-31 07:21:44+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
8d671fbc-19f7-43ae-8e33-044a666d0ab3	51dd4682-183d-4989-84c2-18baefbd4539	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	ac776057-ba95-4b14-b37a-ba2a9ff8c2b9	2018-08-31 07:21:44+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
8d671fbc-19f7-43ae-8e33-044a666d0ab3	b7689c74-5a86-4711-b170-6deb99f7137b	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	ac776057-ba95-4b14-b37a-ba2a9ff8c2b9	2018-08-31 07:21:44+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
07b42dd0-4867-4d42-b506-dc241d85c262	28d7e52a-59e8-4a1e-b43f-4a48cdca3e22	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	0b78c0a2-0548-4ece-b97a-e557bdc882bf	2018-08-31 07:41:35+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
07b42dd0-4867-4d42-b506-dc241d85c262	45d56475-668b-41e4-bdc0-3634c76f35a8	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	0b78c0a2-0548-4ece-b97a-e557bdc882bf	2018-08-31 07:41:35+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2a9c364b-035d-4323-a042-7f6c3a5bedf4	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	51dd4682-183d-4989-84c2-18baefbd4539	29f41daa-38fd-43c3-8376-0942617e0fff	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
f21cff92-6997-48e3-a453-69bb316c132d	1036dac7-ccb6-47fd-a958-5a5c19f3df62	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-08-28	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-12-14	[{"nama":"ISIAN","file":"eb21b8da1d94f2acc381ee1b6609de4a.pdf"}]	222cead4-f3bf-48d7-9541-84f60c66cac5	f	2017-09-07 03:13:07.130655+00	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-08-25 17:37:15+00	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-09-07 03:13:07.130655+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	1	Kurang sesuai	\N	\N	kesimpulan dari\r\nKK01010088-1 1	\N
db0ffebb-9dbe-4772-87e8-f9444b28268e	2e0ee088-c298-4238-9d84-1bcac8f05b4e	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-04-12	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-04-16	[{"nama":"lap_pemantauan (11).xlsx","file":"e8bfa1533392298c630e8cd9a5a0fda7.xlsx"}]	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-12 10:11:40+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	\N	4
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	53983413-54bf-4a94-815e-82ccceecacc3	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 04:08:53+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 04:08:53+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	51dd4682-183d-4989-84c2-18baefbd4539	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 04:08:53+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	b7689c74-5a86-4711-b170-6deb99f7137b	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 04:08:53+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 04:08:53+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 04:08:53+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 04:08:53+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	e7a5563b-215a-44a1-a523-a1e2f47cb694	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 04:08:53+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	62fc3064-8e6a-4e97-a185-fb1156c0c16a	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 04:08:53+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	28ca9f0f-b7c0-442d-9e59-cf89b711af61	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 04:08:53+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	acd1709e-5bb3-436c-b956-d1cfebc17b65	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 04:08:53+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
7a0547b1-b065-42bf-a898-6228ef56fe5a	53983413-54bf-4a94-815e-82ccceecacc3	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	acbc412d-f29a-4931-b599-a37cee3aed69	2018-08-31 07:23:12+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
7a0547b1-b065-42bf-a898-6228ef56fe5a	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	d016fde5-c284-4455-a44a-723ab77fa99f	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	acbc412d-f29a-4931-b599-a37cee3aed69	2018-08-31 07:23:12+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
a1272441-cb74-427f-a423-dcb2c1bf5c13	2dc5180a-6ecb-40db-a6e9-742109c4e82e	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	fc903300-d0fd-4fbd-a09e-2e6646bbc89f	2018-08-31 07:43:00+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	5
a1272441-cb74-427f-a423-dcb2c1bf5c13	28d7e52a-59e8-4a1e-b43f-4a48cdca3e22	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-09-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	fc903300-d0fd-4fbd-a09e-2e6646bbc89f	2018-08-31 07:43:00+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	4
628aabdb-1589-46bc-923b-bf2b57c3fdd6	4453d1e6-05ed-4374-8344-a23915bf425b	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	2e0ee088-c298-4238-9d84-1bcac8f05b4e	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	80636594-116a-44e8-b4be-e8b064986a06	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-10-09	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	c5c56164-40c4-43fa-8495-dc5165718ebf	0188ba46-865b-43b4-9adf-44f6e4fe0cae	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	8a6bbfba-798e-4593-9d57-d0d5a88b2c4a	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	30ef6912-451f-4c64-ac71-92a4bf5736c9	0188ba46-865b-43b4-9adf-44f6e4fe0cae	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	823a2b84-ca0e-42a6-9792-1c1b19d3dfb6	0188ba46-865b-43b4-9adf-44f6e4fe0cae	2017-10-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	61c0607b-868f-47af-a90d-dde0c42a3af4	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	1b72e119-165e-401c-908c-83b0438780e9	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-10	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	64ecb1a9-91f0-4b16-8360-11da3e655574	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	8d53ae6e-9502-4103-8603-ecd615b32d94	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	6b69d2bd-631b-423a-a5c4-15d7ba2adb89	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-10-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	fd3066d2-c8c0-460e-a52c-790cd50b7469	0188ba46-865b-43b4-9adf-44f6e4fe0cae	2017-10-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	505f7604-ccda-49df-83e7-6f5ef6c9bbb0	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-10-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	5db3d0f4-1803-444c-97de-d121100a62dc	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-10-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	e32bf68b-6224-4d54-8162-cb96836a27d7	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	5250d9b9-8156-4a02-ad3b-dde51a608188	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-16	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	47ea222d-05f8-4b95-a130-3e7bb6453ff0	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
f79b7985-23bb-4156-a074-bc23365ded79	57ba24b4-bde1-4749-88ce-ad7e5e658c34	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	53983413-54bf-4a94-815e-82ccceecacc3	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-03	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-01	[{"nama":"00100801.xlsx","file":"8d5f3bb9e24653c4b75fa2579503bc64.xlsx"}]	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	Telah dilakukan entry meeting	0.5
28f5ba79-6c18-45e1-9d23-75f047783203	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-03	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-01	[{"nama":"00100802.xlsx","file":"6f353489c21caa22cd14105548b402d5.xlsx"}]	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	Hasil survey, pengendalian intern lemah	0.5
28f5ba79-6c18-45e1-9d23-75f047783203	51dd4682-183d-4989-84c2-18baefbd4539	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-03	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-01	[{"nama":"00100803.xlsx","file":"baf6c1afe914aa8315012c3c31c87461.xlsx"}]	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	---	0.5
28f5ba79-6c18-45e1-9d23-75f047783203	b7689c74-5a86-4711-b170-6deb99f7137b	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-03	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-01	[{"nama":"00100804.xlsx","file":"09cd06aef2ed616d30a7d6a1d41b654e.xlsx"}]	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	\N	0.25
f79b7985-23bb-4156-a074-bc23365ded79	493f29c3-5db5-470d-88a1-d50c04a963a6	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-10-10	29fe44b6-4bc3-4de2-b917-2b15713b546d	2017-10-20	[{"nama":"kka rina.xlsx","file":"549f70d549e9a43213da0af0d8db257d.xlsx"},{"nama":"kka rina.xlsx","file":"918dcc886d73f86e8918f86ef2e3b23a.xlsx"}]	a30f21b6-918e-421d-835c-fac8228feed5	t	2017-10-20 03:20:08.833633+00	ed485c96-003c-4d05-b7ac-ac38a883257d	t	2017-10-20 03:23:24.700148+00	709e3413-c729-4435-ae26-eff4cb99f23b	2017-10-20 02:49:56+00	ed485c96-003c-4d05-b7ac-ac38a883257d	2017-10-20 03:23:24.700148+00	t	\N	\N	\N	3		\N	\N	SPMK sudah sesuai	\N
84759213-de14-4bea-8154-647cedb74bcf	2e0ee088-c298-4238-9d84-1bcac8f05b4e	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-04-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-04-13 12:16:23+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	3
095fe615-c827-4a01-93e0-de6076d09ad2	53983413-54bf-4a94-815e-82ccceecacc3	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 06:40:29+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
095fe615-c827-4a01-93e0-de6076d09ad2	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-08-31 06:40:29+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	2
f1ee1810-7e79-4695-8440-6c4ac296a5c7	53983413-54bf-4a94-815e-82ccceecacc3	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-08-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	46a6c7ed-f739-4b61-a893-79646f60549b	2018-08-31 07:26:44+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
f1ee1810-7e79-4695-8440-6c4ac296a5c7	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-09-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	46a6c7ed-f739-4b61-a893-79646f60549b	2018-08-31 07:26:44+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
f1ee1810-7e79-4695-8440-6c4ac296a5c7	51dd4682-183d-4989-84c2-18baefbd4539	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	46a6c7ed-f739-4b61-a893-79646f60549b	2018-08-31 07:26:44+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
28f5ba79-6c18-45e1-9d23-75f047783203	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	0.25
28f5ba79-6c18-45e1-9d23-75f047783203	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	0.25
28f5ba79-6c18-45e1-9d23-75f047783203	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	e7a5563b-215a-44a1-a523-a1e2f47cb694	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	62fc3064-8e6a-4e97-a185-fb1156c0c16a	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	28ca9f0f-b7c0-442d-9e59-cf89b711af61	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	acd1709e-5bb3-436c-b956-d1cfebc17b65	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	4453d1e6-05ed-4374-8344-a23915bf425b	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	c7aa0a46-3ae1-4c52-a708-98c17cec316e	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	5cbb9528-fa19-449b-a05f-ed0abfc36a00	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	c90bd32b-1b3f-4952-ad96-d498e6b56364	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	336014ad-e1cd-41c4-bf97-b8438b78b12f	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	149643dc-f2b1-4426-8f69-013f48e4d7bd	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	\N
28f5ba79-6c18-45e1-9d23-75f047783203	49f6c09c-485d-458b-aae9-c3ff883f4542	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
28f5ba79-6c18-45e1-9d23-75f047783203	64722e98-772e-4a28-94c3-03d3cb483320	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
28f5ba79-6c18-45e1-9d23-75f047783203	72eed5d9-87a3-424d-abbf-e74972466494	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
28f5ba79-6c18-45e1-9d23-75f047783203	04b0f150-6dca-4d93-b29f-3eef3a302281	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-13	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01	[{"nama":"LHP PENGADUAN AN. KADES WRINGINANGUNG.kasus.docx","file":"9e69942c3892efc6ab68098e204959e8.docx"}]	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	\N	1
7ee14d19-aa59-4403-b0c1-85a7a0560da4	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-10-05	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-10-20	[{"nama":"LKE NILAI ASLI 80.61.xlsx","file":"e9ee8dc1daeb4a48325886caf593dbac.xlsx"}]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2017-12-22 07:40:49.882212+00	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-10-17 02:16:53+00	222cead4-f3bf-48d7-9541-84f60c66cac5	2017-12-22 07:40:49.882212+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	2	SETUJU	\N	\N	kkkkk	\N
0334a10d-2102-4261-b84c-056720515d2c	2e0ee088-c298-4238-9d84-1bcac8f05b4e	95542b83-a537-4794-a103-b768408db707	2018-04-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-13 12:32:23+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	33
c59ea1f7-e9cc-4e6f-9206-f90d21fe40f8	53983413-54bf-4a94-815e-82ccceecacc3	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	21ec1744-ea60-4bb3-b8ed-fc9ffc9918e6	2018-08-31 06:46:54+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
34e4a86b-0bb3-4d9b-b5a7-4ee047258887	53983413-54bf-4a94-815e-82ccceecacc3	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-08-21	\N	\N	\N	\N	\N	\N	\N	\N	\N	896bb850-eca4-42f7-b75f-98cb8b67e0a6	2018-08-31 07:26:55+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
34e4a86b-0bb3-4d9b-b5a7-4ee047258887	51dd4682-183d-4989-84c2-18baefbd4539	89488dc0-00e0-48c8-9e47-1f143b877878	2018-08-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	896bb850-eca4-42f7-b75f-98cb8b67e0a6	2018-08-31 07:26:55+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	3
34e4a86b-0bb3-4d9b-b5a7-4ee047258887	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-08-08	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-08-31	[{"nama":"AHSP 2017 cipta karya.xlsx","file":"f2d91b19af44445fabde5451a1e21558.xlsx"}]	\N	\N	\N	\N	\N	\N	896bb850-eca4-42f7-b75f-98cb8b67e0a6	2018-08-31 07:26:55+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	coba	4
28f5ba79-6c18-45e1-9d23-75f047783203	06709067-0f75-4b63-96b3-47b501e4674c	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-01 00:32:59+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	2e0ee088-c298-4238-9d84-1bcac8f05b4e	ee570f4f-46de-4761-ae5c-b205c92d5c3e	2017-10-09	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-10	[{"nama":"95410547cf3685563908c6ba0209498f.xlsx","file":"c967fc815e009eea2a75ad72c3f6c34e.xlsx"}]	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-10-20 01:44:47+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	Telah dilaksanakan entry meeting dengan Kepala SKPD tanggal 10-10-2017	\N
9375e424-6ea6-4841-a55f-1c1f79c8c6de	2e0ee088-c298-4238-9d84-1bcac8f05b4e	998cdf4a-6b17-468e-9c07-186e1a724f6d	2018-04-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-04-13 12:40:03+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	3
97ab28b5-8568-4f6f-94c5-837c3c2adb9e	53983413-54bf-4a94-815e-82ccceecacc3	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-03	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-08-31	[{"nama":"CEKLIST BPK30Jan2018.xlsx","file":"1e019854b40ac3051f62c31011a222f1.xlsx"}]	\N	\N	\N	\N	\N	\N	e03281a2-712d-497a-a86e-7c7e0a425052	2018-08-31 06:50:02+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	\N	1
97ab28b5-8568-4f6f-94c5-837c3c2adb9e	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-04	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-08-31	[{"nama":"NAMA-NAMA UNDANGAN GAGAS.xlsx","file":"58f6462108532c42e8eea1e405ceac35.xlsx"}]	\N	\N	\N	\N	\N	\N	e03281a2-712d-497a-a86e-7c7e0a425052	2018-08-31 06:50:02+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	coba	1
97ab28b5-8568-4f6f-94c5-837c3c2adb9e	51dd4682-183d-4989-84c2-18baefbd4539	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-03	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-08-31	[{"nama":"Rekap PAK Tahun anggaran 2017 Inspektorat Kabupaten bwi.xlsx","file":"eb0c26a9aee90f5857098bd9711704ea.xlsx"}]	\N	\N	\N	\N	\N	\N	e03281a2-712d-497a-a86e-7c7e0a425052	2018-08-31 06:50:02+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	coba	1
c7f2b0c5-f32b-443c-ab9e-bebaf0a701ad	53983413-54bf-4a94-815e-82ccceecacc3	f741041f-732a-481f-bc78-461bc55ada9b	2018-08-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	46a6c7ed-f739-4b61-a893-79646f60549b	2018-08-31 07:31:30+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
c7f2b0c5-f32b-443c-ab9e-bebaf0a701ad	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2018-09-01	\N	\N	\N	\N	\N	\N	\N	\N	\N	46a6c7ed-f739-4b61-a893-79646f60549b	2018-08-31 07:31:30+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
c7f2b0c5-f32b-443c-ab9e-bebaf0a701ad	51dd4682-183d-4989-84c2-18baefbd4539	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-09-02	\N	\N	\N	\N	\N	\N	\N	\N	\N	46a6c7ed-f739-4b61-a893-79646f60549b	2018-08-31 07:31:30+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
628aabdb-1589-46bc-923b-bf2b57c3fdd6	acd1709e-5bb3-436c-b956-d1cfebc17b65	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-09-13	d016fde5-c284-4455-a44a-723ab77fa99f	2018-09-01	[{"nama":"000000001.xlsx","file":"a6d56eecc3d5ead4b0da78e6a5c402ff.xlsx"}]	f741041f-732a-481f-bc78-461bc55ada9b	t	2018-10-30 21:11:02.804853+00	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	f741041f-732a-481f-bc78-461bc55ada9b	2018-10-30 21:11:02.804853+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	2		\N	\N	Seluruh penerimaan telah didukung dengan aturan	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	c7aa0a46-3ae1-4c52-a708-98c17cec316e	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	5cbb9528-fa19-449b-a05f-ed0abfc36a00	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	c90bd32b-1b3f-4952-ad96-d498e6b56364	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	336014ad-e1cd-41c4-bf97-b8438b78b12f	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
145af5e3-abff-4780-b9cd-5b8bba415a09	2e0ee088-c298-4238-9d84-1bcac8f05b4e	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-04-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-16 05:29:18+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	3
145af5e3-abff-4780-b9cd-5b8bba415a09	80636594-116a-44e8-b4be-e8b064986a06	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-04-17	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-16 05:29:18+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	4
bf117ea8-6a7b-40c3-9d6c-1fc9761e85f5	53983413-54bf-4a94-815e-82ccceecacc3	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-08-31	\N	\N	\N	\N	\N	\N	\N	\N	\N	d2c224f6-d2d9-49b9-8285-cc953b2ebc5f	2018-08-31 06:55:40+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	2
b1477309-9e65-4c43-aefa-1219ef447c56	2dc5180a-6ecb-40db-a6e9-742109c4e82e	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	5efd92f0-d96e-4037-ab17-467310a76cd0	2018-08-31 07:36:28+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	3
b1477309-9e65-4c43-aefa-1219ef447c56	28d7e52a-59e8-4a1e-b43f-4a48cdca3e22	7a766876-545a-42a4-bb81-ca6fa59efff6	2018-09-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	5efd92f0-d96e-4037-ab17-467310a76cd0	2018-08-31 07:36:28+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	4
628aabdb-1589-46bc-923b-bf2b57c3fdd6	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-09-07	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-09-06	[{"nama":"ISIAN","file":"50128ece7018b1a0976401ca0d8e0e2e.pdf"}]	f741041f-732a-481f-bc78-461bc55ada9b	t	2018-10-30 21:10:44.484397+00	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	f741041f-732a-481f-bc78-461bc55ada9b	2018-10-30 21:10:44.484397+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	2		\N	\N	--	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	04b0f150-6dca-4d93-b29f-3eef3a302281	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-28	d016fde5-c284-4455-a44a-723ab77fa99f	2018-09-01	[{"nama":"LHP SEKOLAH.docx","file":"ae19405135e56d31a11bf3c43285b100.docx"}]	f741041f-732a-481f-bc78-461bc55ada9b	t	2018-10-30 21:11:30.816001+00	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	f741041f-732a-481f-bc78-461bc55ada9b	2018-10-30 21:11:30.816001+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	2		\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	149643dc-f2b1-4426-8f69-013f48e4d7bd	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	dc315220-3e80-4d7a-841e-dda52b0a1f84	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	4cdccf23-dc54-4583-b560-8ae93efcd3e8	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	5d03a7f2-fbdc-4dcb-becd-75c08489febd	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	52cc2b91-fe8b-4097-9d9e-3f81a889153f	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	470396d6-b06a-4147-b47b-7d55859ce577	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	008749c7-efb0-47c7-b54d-6a2503ba2ebd	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	53449178-793e-47f9-892d-ac58f1375831	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	37bab70a-0dd2-4786-9c24-9542445e738d	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	49f6c09c-485d-458b-aae9-c3ff883f4542	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	64722e98-772e-4a28-94c3-03d3cb483320	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	45d56475-668b-41e4-bdc0-3634c76f35a8	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	fb1b3834-b5d2-4599-8273-a49881db8d36	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	e7a5563b-215a-44a1-a523-a1e2f47cb694	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-09-08	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-09-07	[{"nama":"ISIAN","file":"78afabbb50ba2edda4bd085e5ef744c0.pdf"}]	f741041f-732a-481f-bc78-461bc55ada9b	t	2017-09-29 02:52:53.861346+00	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	f741041f-732a-481f-bc78-461bc55ada9b	2017-09-29 02:52:53.861346+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	2		\N	\N	--	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	313a30e0-7f5b-4b15-ac3f-708e03a3c990	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	72eed5d9-87a3-424d-abbf-e74972466494	f741041f-732a-481f-bc78-461bc55ada9b	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	06709067-0f75-4b63-96b3-47b501e4674c	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	53983413-54bf-4a94-815e-82ccceecacc3	998cdf4a-6b17-468e-9c07-186e1a724f6d	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	998cdf4a-6b17-468e-9c07-186e1a724f6d	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	51dd4682-183d-4989-84c2-18baefbd4539	998cdf4a-6b17-468e-9c07-186e1a724f6d	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	b7689c74-5a86-4711-b170-6deb99f7137b	998cdf4a-6b17-468e-9c07-186e1a724f6d	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	998cdf4a-6b17-468e-9c07-186e1a724f6d	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	998cdf4a-6b17-468e-9c07-186e1a724f6d	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	998cdf4a-6b17-468e-9c07-186e1a724f6d	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	e7a5563b-215a-44a1-a523-a1e2f47cb694	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	62fc3064-8e6a-4e97-a185-fb1156c0c16a	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	28ca9f0f-b7c0-442d-9e59-cf89b711af61	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	acd1709e-5bb3-436c-b956-d1cfebc17b65	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	4453d1e6-05ed-4374-8344-a23915bf425b	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	c7aa0a46-3ae1-4c52-a708-98c17cec316e	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	2017-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	5cbb9528-fa19-449b-a05f-ed0abfc36a00	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	c90bd32b-1b3f-4952-ad96-d498e6b56364	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	336014ad-e1cd-41c4-bf97-b8438b78b12f	e54dd636-fb29-4ada-b95c-848c39ed6d03	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	149643dc-f2b1-4426-8f69-013f48e4d7bd	998cdf4a-6b17-468e-9c07-186e1a724f6d	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	dc315220-3e80-4d7a-841e-dda52b0a1f84	998cdf4a-6b17-468e-9c07-186e1a724f6d	2017-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	4cdccf23-dc54-4583-b560-8ae93efcd3e8	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	2017-09-26 06:56:49+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	51dd4682-183d-4989-84c2-18baefbd4539	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	b7689c74-5a86-4711-b170-6deb99f7137b	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-06	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	e7a5563b-215a-44a1-a523-a1e2f47cb694	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	62fc3064-8e6a-4e97-a185-fb1156c0c16a	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	28ca9f0f-b7c0-442d-9e59-cf89b711af61	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	acd1709e-5bb3-436c-b956-d1cfebc17b65	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	4453d1e6-05ed-4374-8344-a23915bf425b	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	c7aa0a46-3ae1-4c52-a708-98c17cec316e	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	336014ad-e1cd-41c4-bf97-b8438b78b12f	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	149643dc-f2b1-4426-8f69-013f48e4d7bd	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	dc315220-3e80-4d7a-841e-dda52b0a1f84	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	4cdccf23-dc54-4583-b560-8ae93efcd3e8	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	5d03a7f2-fbdc-4dcb-becd-75c08489febd	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	52cc2b91-fe8b-4097-9d9e-3f81a889153f	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	470396d6-b06a-4147-b47b-7d55859ce577	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	008749c7-efb0-47c7-b54d-6a2503ba2ebd	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	53449178-793e-47f9-892d-ac58f1375831	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	37bab70a-0dd2-4786-9c24-9542445e738d	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	49f6c09c-485d-458b-aae9-c3ff883f4542	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	64722e98-772e-4a28-94c3-03d3cb483320	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-27	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	04b0f150-6dca-4d93-b29f-3eef3a302281	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	e7a5563b-215a-44a1-a523-a1e2f47cb694	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-09-07	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	62fc3064-8e6a-4e97-a185-fb1156c0c16a	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	28ca9f0f-b7c0-442d-9e59-cf89b711af61	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	acd1709e-5bb3-436c-b956-d1cfebc17b65	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	4453d1e6-05ed-4374-8344-a23915bf425b	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	c7aa0a46-3ae1-4c52-a708-98c17cec316e	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-09-08	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-09-11	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	5cbb9528-fa19-449b-a05f-ed0abfc36a00	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	c90bd32b-1b3f-4952-ad96-d498e6b56364	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	336014ad-e1cd-41c4-bf97-b8438b78b12f	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	149643dc-f2b1-4426-8f69-013f48e4d7bd	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	dc315220-3e80-4d7a-841e-dda52b0a1f84	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-09-14	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	4cdccf23-dc54-4583-b560-8ae93efcd3e8	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-09-15	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	5d03a7f2-fbdc-4dcb-becd-75c08489febd	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	52cc2b91-fe8b-4097-9d9e-3f81a889153f	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	470396d6-b06a-4147-b47b-7d55859ce577	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-20	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	008749c7-efb0-47c7-b54d-6a2503ba2ebd	7a766876-545a-42a4-bb81-ca6fa59efff6	2017-09-22	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	53449178-793e-47f9-892d-ac58f1375831	9d5f96cb-8d85-4216-b60e-809f39620ac6	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	37bab70a-0dd2-4786-9c24-9542445e738d	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-25	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	49f6c09c-485d-458b-aae9-c3ff883f4542	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	64722e98-772e-4a28-94c3-03d3cb483320	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-12	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	5cbb9528-fa19-449b-a05f-ed0abfc36a00	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	c90bd32b-1b3f-4952-ad96-d498e6b56364	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	2017-09-13	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	04b0f150-6dca-4d93-b29f-3eef3a302281	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
185a85fd-c805-4697-9719-21b017164891	45d56475-668b-41e4-bdc0-3634c76f35a8	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2017-09-12 08:15:36+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	4cdccf23-dc54-4583-b560-8ae93efcd3e8	29f41daa-38fd-43c3-8376-0942617e0fff	2017-09-18	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	5d03a7f2-fbdc-4dcb-becd-75c08489febd	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	2017-09-19	\N	\N	\N	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 02:49:09+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	45d56475-668b-41e4-bdc0-3634c76f35a8	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	fb1b3834-b5d2-4599-8273-a49881db8d36	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	313a30e0-7f5b-4b15-ac3f-708e03a3c990	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	72eed5d9-87a3-424d-abbf-e74972466494	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-28	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
2a6993c2-d863-4fbc-8b75-40f005f7190e	06709067-0f75-4b63-96b3-47b501e4674c	ee570f4f-46de-4761-ae5c-b205c92d5c3e	2017-09-29	\N	\N	\N	\N	\N	\N	\N	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	2017-09-26 10:55:37+00	\N	\N	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	0	\N	\N	\N	\N	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	28ca9f0f-b7c0-442d-9e59-cf89b711af61	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-12	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-07	[{"nama":"ISIAN","file":"45dc0d834bf38f2c7eead1ae6d670180.pdf"}]	f741041f-732a-481f-bc78-461bc55ada9b	t	2017-09-28 04:27:59.58606+00	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	f741041f-732a-481f-bc78-461bc55ada9b	2017-09-28 04:27:59.58606+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	2		\N	\N	Terdapat keterlambatan transfer dana BOS antara 1-2 bulan	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	62fc3064-8e6a-4e97-a185-fb1156c0c16a	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-09-11	d016fde5-c284-4455-a44a-723ab77fa99f	2017-09-07	[{"nama":"ISIAN","file":"815d7084ee64d83664f3d1b5c5b710a9.pdf"}]	f741041f-732a-481f-bc78-461bc55ada9b	t	2017-09-29 02:37:28.028779+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2017-09-29 02:51:22.59674+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2017-09-29 02:51:22.59674+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	3	Perlu dibuat KKA yang lebih detail	\N	\N	Terdapat kekurangan penyaluran semester I sebanyak 4.800.000	\N
5ffb7fc3-7839-497f-b5e8-e936665e819f	2e0ee088-c298-4238-9d84-1bcac8f05b4e	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-03-21	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-03-21	[{"nama":"2273-4638-1-SM.pdf","file":"f5e0777c2c2733d3e3674ae485a16a45.pdf"}]	222cead4-f3bf-48d7-9541-84f60c66cac5	t	2018-03-21 12:56:14.032234+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	t	2018-03-21 12:56:55.128288+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-03-21 12:48:18+00	6953ba33-d2b4-45a9-9c9b-0faffe456de6	2018-03-21 12:57:18.427445+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	t	2018-03-21 12:57:18.427445+00	4		\N	\N	\N	1
89dfef89-b49a-4ca2-9aa8-d7edc8f9e2b9	2e0ee088-c298-4238-9d84-1bcac8f05b4e	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-04-17	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-04-17	[{"nama":"KM4 (7).xlsx","file":"d4c10b36108985d8ae7a3c5a7171f7a3.xlsx"}]	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-16 06:32:42+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	\N	3
89dfef89-b49a-4ca2-9aa8-d7edc8f9e2b9	80636594-116a-44e8-b4be-e8b064986a06	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-04-16	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-04-17	[{"nama":"KM3 (9).xlsx","file":"24b510e54c17d8409ed57f712e95a8d1.xlsx"}]	\N	\N	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-16 06:32:42+00	\N	\N	t	\N	\N	\N	1	\N	\N	\N	\N	4
b90a05f4-e77d-44b7-b657-a2fefd13c8fb	53983413-54bf-4a94-815e-82ccceecacc3	89488dc0-00e0-48c8-9e47-1f143b877878	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	3cd6fe67-4266-465f-ae5a-d1e60dfd2709	2018-08-31 06:59:15+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
b90a05f4-e77d-44b7-b657-a2fefd13c8fb	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	2018-09-04	\N	\N	\N	\N	\N	\N	\N	\N	\N	3cd6fe67-4266-465f-ae5a-d1e60dfd2709	2018-08-31 06:59:15+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
43114d29-0a14-4b78-bb61-1c119d4b3f88	53983413-54bf-4a94-815e-82ccceecacc3	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-09-03	\N	\N	\N	\N	\N	\N	\N	\N	\N	acbc412d-f29a-4931-b599-a37cee3aed69	2018-08-31 07:37:39+00	\N	\N	t	\N	\N	\N	0	\N	\N	\N	\N	1
628aabdb-1589-46bc-923b-bf2b57c3fdd6	b7689c74-5a86-4711-b170-6deb99f7137b	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-09-06	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-09-06	[{"nama":"ISIAN","file":"3cd37b2b4442ae09b5e8a28e2a53bf70.pdf"}]	f741041f-732a-481f-bc78-461bc55ada9b	t	2018-10-30 21:10:23.169348+00	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	f741041f-732a-481f-bc78-461bc55ada9b	2018-10-30 21:10:23.169348+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	2		\N	\N	--	\N
628aabdb-1589-46bc-923b-bf2b57c3fdd6	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	9db069b6-02be-46d9-9d45-a89f2d741ec1	2017-09-06	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	2017-09-06	[{"nama":"ISIAN","file":"1bb8d89581d15a7e3d6a75dbcf8ed0cf.pdf"}]	f741041f-732a-481f-bc78-461bc55ada9b	t	2018-10-30 21:10:35.889445+00	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2017-09-26 03:33:41+00	f741041f-732a-481f-bc78-461bc55ada9b	2018-10-30 21:10:35.889445+00	t	6953ba33-d2b4-45a9-9c9b-0faffe456de6	\N	\N	2		\N	\N	--	\N
\.


--
-- Data for Name: perencanaan_hasil; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.perencanaan_hasil (id, file, created_at, created_by, app_sv1, app_sv1_status, app_sv1_tstamp, app_sv1_note, app_sv2, app_sv2_status, app_sv2_tstamp, app_sv2_note, app_sv3, app_sv3_status, app_sv3_tstamp, app_sv3_note, progres, modified_at, modified_by, thn_laporan, no_laporan, id_tao, kesimpulan) FROM stdin;
6fc1040e-7ee5-4f5b-9ba0-ef4e85919d50	\N	2017-09-07 00:43:13+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2017-09-07 00:43:12.802139+00	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018	900	4ac7b90f-d4ad-4bba-bad1-94493366203d	Good JOB!
\.


--
-- Data for Name: perencanaan_pedoman; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.perencanaan_pedoman (id, pedoman) FROM stdin;
\.


--
-- Data for Name: perencanaan_sasaran; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.perencanaan_sasaran (id, sasaran) FROM stdin;
2d0dfd47-3664-463b-83df-050b7c010aac	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d
2d0dfd47-3664-463b-83df-050b7c010aac	bf44df83-9bbf-452e-ab81-11bff7f66f32
2d0dfd47-3664-463b-83df-050b7c010aac	fad97553-5c17-4caa-b8a3-01ff6ea39ff0
31c1272e-dbde-4382-ae95-3ee17fbe59d7	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d
4ac7b90f-d4ad-4bba-bad1-94493366203d	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d
4ac7b90f-d4ad-4bba-bad1-94493366203d	bf44df83-9bbf-452e-ab81-11bff7f66f32
4ac7b90f-d4ad-4bba-bad1-94493366203d	f019a1a1-b1b2-472c-9f4e-7aadcb31e220
eba05609-cdd3-4c38-a91b-5306eee4a6b2	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d
f21cff92-6997-48e3-a453-69bb316c132d	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d
1c14b647-870a-43bd-aa06-f06ac1c8f56e	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d
1c14b647-870a-43bd-aa06-f06ac1c8f56e	f019a1a1-b1b2-472c-9f4e-7aadcb31e220
1c14b647-870a-43bd-aa06-f06ac1c8f56e	2ed85714-d1fe-444d-8f20-d821b58b8423
bb7b542c-24ec-493b-80b9-ebbbad3ebd94	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d
bb7b542c-24ec-493b-80b9-ebbbad3ebd94	bf44df83-9bbf-452e-ab81-11bff7f66f32
bb7b542c-24ec-493b-80b9-ebbbad3ebd94	fad97553-5c17-4caa-b8a3-01ff6ea39ff0
568a9e86-8b62-4125-a959-781ee7382863	bf44df83-9bbf-452e-ab81-11bff7f66f32
359ea449-f314-4b68-99a0-ac42f4ba25d2	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d
359ea449-f314-4b68-99a0-ac42f4ba25d2	f019a1a1-b1b2-472c-9f4e-7aadcb31e220
359ea449-f314-4b68-99a0-ac42f4ba25d2	2ed85714-d1fe-444d-8f20-d821b58b8423
185a85fd-c805-4697-9719-21b017164891	a863e538-ff0c-4df6-a710-ae75058c750f
d26350c7-bc66-4098-ab11-202d883e3c3d	a863e538-ff0c-4df6-a710-ae75058c750f
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	a863e538-ff0c-4df6-a710-ae75058c750f
628aabdb-1589-46bc-923b-bf2b57c3fdd6	a863e538-ff0c-4df6-a710-ae75058c750f
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	a863e538-ff0c-4df6-a710-ae75058c750f
9cb18ed2-4658-481f-9f74-e40105dc188b	a863e538-ff0c-4df6-a710-ae75058c750f
4114359b-983c-4020-a06f-65a7c7913b15	a863e538-ff0c-4df6-a710-ae75058c750f
2a6993c2-d863-4fbc-8b75-40f005f7190e	a863e538-ff0c-4df6-a710-ae75058c750f
cec3e6f8-2a37-4514-a252-82968f15f2e3	f019a1a1-b1b2-472c-9f4e-7aadcb31e220
63216689-ab95-4628-bcec-8fe56198caee	bf44df83-9bbf-452e-ab81-11bff7f66f32
aa955757-a10f-4e35-915c-c08ad42c9ae3	bf44df83-9bbf-452e-ab81-11bff7f66f32
aa955757-a10f-4e35-915c-c08ad42c9ae3	fad97553-5c17-4caa-b8a3-01ff6ea39ff0
aa955757-a10f-4e35-915c-c08ad42c9ae3	2ed85714-d1fe-444d-8f20-d821b58b8423
aa955757-a10f-4e35-915c-c08ad42c9ae3	69063990-533f-4fd1-9a00-6fef0b273bb2
7ee14d19-aa59-4403-b0c1-85a7a0560da4	f019a1a1-b1b2-472c-9f4e-7aadcb31e220
7ee14d19-aa59-4403-b0c1-85a7a0560da4	fad97553-5c17-4caa-b8a3-01ff6ea39ff0
7ee14d19-aa59-4403-b0c1-85a7a0560da4	b02fb5b6-a19e-4d2e-92c9-a83feeb2189c
7ee14d19-aa59-4403-b0c1-85a7a0560da4	a863e538-ff0c-4df6-a710-ae75058c750f
01950624-d000-47ca-ab1f-11837e731ed1	e6100dce-b40c-4381-9592-9fcdb90bdf05
9973f691-2564-44d0-b825-52b55884dd9c	e6100dce-b40c-4381-9592-9fcdb90bdf05
d1856679-933f-4801-acd2-7d08f130161e	e6100dce-b40c-4381-9592-9fcdb90bdf05
05275cf2-b282-4653-8c37-a0d7f8923a62	e6100dce-b40c-4381-9592-9fcdb90bdf05
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	e6100dce-b40c-4381-9592-9fcdb90bdf05
6c8cd73f-7443-4bea-9ae3-184f42f07320	e6100dce-b40c-4381-9592-9fcdb90bdf05
f79b7985-23bb-4156-a074-bc23365ded79	e6100dce-b40c-4381-9592-9fcdb90bdf05
e7d913c6-fed5-465d-a619-7808378b81ff	e6100dce-b40c-4381-9592-9fcdb90bdf05
08fa0711-fce9-419e-b46d-bdc38b61b644	e6100dce-b40c-4381-9592-9fcdb90bdf05
6dd905b2-147a-4db1-aa8a-6f56f2bf4cbe	e6100dce-b40c-4381-9592-9fcdb90bdf05
5ffb7fc3-7839-497f-b5e8-e936665e819f	e6100dce-b40c-4381-9592-9fcdb90bdf05
a109ef9e-68e4-48c0-8d25-3318e4154d07	e6100dce-b40c-4381-9592-9fcdb90bdf05
81318580-6a2e-4c70-9b37-0662a9898536	e6100dce-b40c-4381-9592-9fcdb90bdf05
db0ffebb-9dbe-4772-87e8-f9444b28268e	e6100dce-b40c-4381-9592-9fcdb90bdf05
84759213-de14-4bea-8154-647cedb74bcf	e6100dce-b40c-4381-9592-9fcdb90bdf05
0334a10d-2102-4261-b84c-056720515d2c	bf44df83-9bbf-452e-ab81-11bff7f66f32
0334a10d-2102-4261-b84c-056720515d2c	e6100dce-b40c-4381-9592-9fcdb90bdf05
9375e424-6ea6-4841-a55f-1c1f79c8c6de	e6100dce-b40c-4381-9592-9fcdb90bdf05
145af5e3-abff-4780-b9cd-5b8bba415a09	e6100dce-b40c-4381-9592-9fcdb90bdf05
89dfef89-b49a-4ca2-9aa8-d7edc8f9e2b9	e6100dce-b40c-4381-9592-9fcdb90bdf05
274a4cfd-b67d-46e1-b695-afb76eb7975c	e6100dce-b40c-4381-9592-9fcdb90bdf05
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	a863e538-ff0c-4df6-a710-ae75058c750f
095fe615-c827-4a01-93e0-de6076d09ad2	a863e538-ff0c-4df6-a710-ae75058c750f
c59ea1f7-e9cc-4e6f-9206-f90d21fe40f8	a863e538-ff0c-4df6-a710-ae75058c750f
97ab28b5-8568-4f6f-94c5-837c3c2adb9e	a863e538-ff0c-4df6-a710-ae75058c750f
bf117ea8-6a7b-40c3-9d6c-1fc9761e85f5	a863e538-ff0c-4df6-a710-ae75058c750f
b90a05f4-e77d-44b7-b657-a2fefd13c8fb	a863e538-ff0c-4df6-a710-ae75058c750f
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	a863e538-ff0c-4df6-a710-ae75058c750f
58c39523-485e-4456-aadd-47cd90614072	2ed85714-d1fe-444d-8f20-d821b58b8423
2ef9e777-3102-411a-8911-cce857ec0330	e6100dce-b40c-4381-9592-9fcdb90bdf05
8d671fbc-19f7-43ae-8e33-044a666d0ab3	a863e538-ff0c-4df6-a710-ae75058c750f
7a0547b1-b065-42bf-a898-6228ef56fe5a	a863e538-ff0c-4df6-a710-ae75058c750f
f1ee1810-7e79-4695-8440-6c4ac296a5c7	a863e538-ff0c-4df6-a710-ae75058c750f
34e4a86b-0bb3-4d9b-b5a7-4ee047258887	a863e538-ff0c-4df6-a710-ae75058c750f
c7f2b0c5-f32b-443c-ab9e-bebaf0a701ad	a863e538-ff0c-4df6-a710-ae75058c750f
b1477309-9e65-4c43-aefa-1219ef447c56	a863e538-ff0c-4df6-a710-ae75058c750f
43114d29-0a14-4b78-bb61-1c119d4b3f88	a863e538-ff0c-4df6-a710-ae75058c750f
20836be9-c3ac-46a2-a33e-932405df0b8b	a863e538-ff0c-4df6-a710-ae75058c750f
07b42dd0-4867-4d42-b506-dc241d85c262	a863e538-ff0c-4df6-a710-ae75058c750f
a1272441-cb74-427f-a423-dcb2c1bf5c13	a863e538-ff0c-4df6-a710-ae75058c750f
28f5ba79-6c18-45e1-9d23-75f047783203	a863e538-ff0c-4df6-a710-ae75058c750f
\.


--
-- Data for Name: perencanaan_tim; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.perencanaan_tim (id, irban, dalnis, ketua, tim) FROM stdin;
185a85fd-c805-4697-9719-21b017164891	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6,9d5f96cb-8d85-4216-b60e-809f39620ac6}
d26350c7-bc66-4098-ab11-202d883e3c3d	ae3234c8-ea30-45d2-a48e-367b8c32567d	dd7ae175-271f-467e-979e-8d2d709b1f0e	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	{ac513f02-a71e-4764-b78c-216b62ea590f,70bf9ad1-3775-4be7-abab-9128837416ad}
4ac7b90f-d4ad-4bba-bad1-94493366203d	ae3234c8-ea30-45d2-a48e-367b8c32567d	f741041f-732a-481f-bc78-461bc55ada9b	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{222cead4-f3bf-48d7-9541-84f60c66cac5,7a766876-545a-42a4-bb81-ca6fa59efff6,9d5f96cb-8d85-4216-b60e-809f39620ac6}
f21cff92-6997-48e3-a453-69bb316c132d	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	\N	{}
1c14b647-870a-43bd-aa06-f06ac1c8f56e	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	\N	{}
359ea449-f314-4b68-99a0-ac42f4ba25d2	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	\N	{7a766876-545a-42a4-bb81-ca6fa59efff6}
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	95542b83-a537-4794-a103-b768408db707	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	e54dd636-fb29-4ada-b95c-848c39ed6d03	{b5d26e6b-30f7-46a3-b7fe-03b66a235d86,998cdf4a-6b17-468e-9c07-186e1a724f6d}
2a6993c2-d863-4fbc-8b75-40f005f7190e	ed485c96-003c-4d05-b7ac-ac38a883257d	ee570f4f-46de-4761-ae5c-b205c92d5c3e	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	{bf5300ba-29bf-424c-bb09-e97e0e3f07c8}
cec3e6f8-2a37-4514-a252-82968f15f2e3	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{9d5f96cb-8d85-4216-b60e-809f39620ac6,7a766876-545a-42a4-bb81-ca6fa59efff6}
4114359b-983c-4020-a06f-65a7c7913b15	ed485c96-003c-4d05-b7ac-ac38a883257d	1b162438-55b1-4b5c-ac17-5699e9dfb1ed	ebe58226-6813-4081-8377-b11d002e2be8	{986b958b-1a98-44e4-a59b-c02a15620381}
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	95542b83-a537-4794-a103-b768408db707	55fc1778-b710-47c8-a860-b519a03e23bf	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	{2a9c364b-035d-4323-a042-7f6c3a5bedf4,29f41daa-38fd-43c3-8376-0942617e0fff}
63216689-ab95-4628-bcec-8fe56198caee	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{}
aa955757-a10f-4e35-915c-c08ad42c9ae3	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6,9d5f96cb-8d85-4216-b60e-809f39620ac6}
568a9e86-8b62-4125-a959-781ee7382863	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6,9d5f96cb-8d85-4216-b60e-809f39620ac6}
628aabdb-1589-46bc-923b-bf2b57c3fdd6	\N	f741041f-732a-481f-bc78-461bc55ada9b	d016fde5-c284-4455-a44a-723ab77fa99f	{9db069b6-02be-46d9-9d45-a89f2d741ec1,6f9cfc1d-dc27-4d78-8e51-51dcbf30117b}
bb7b542c-24ec-493b-80b9-ebbbad3ebd94	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6,9d5f96cb-8d85-4216-b60e-809f39620ac6}
9cb18ed2-4658-481f-9f74-e40105dc188b	ed485c96-003c-4d05-b7ac-ac38a883257d	a30f21b6-918e-421d-835c-fac8228feed5	709e3413-c729-4435-ae26-eff4cb99f23b	{29fe44b6-4bc3-4de2-b917-2b15713b546d}
7ee14d19-aa59-4403-b0c1-85a7a0560da4	ae3234c8-ea30-45d2-a48e-367b8c32567d	f741041f-732a-481f-bc78-461bc55ada9b	\N	{7a766876-545a-42a4-bb81-ca6fa59efff6,222cead4-f3bf-48d7-9541-84f60c66cac5}
83c9ad85-4045-446e-81ca-c1e83bf7fee2	95542b83-a537-4794-a103-b768408db707	\N	\N	{}
9973f691-2564-44d0-b825-52b55884dd9c	95542b83-a537-4794-a103-b768408db707	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	e54dd636-fb29-4ada-b95c-848c39ed6d03	{998cdf4a-6b17-468e-9c07-186e1a724f6d,b5d26e6b-30f7-46a3-b7fe-03b66a235d86}
d1856679-933f-4801-acd2-7d08f130161e	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6,89488dc0-00e0-48c8-9e47-1f143b877878}
05275cf2-b282-4653-8c37-a0d7f8923a62	ed485c96-003c-4d05-b7ac-ac38a883257d	1b162438-55b1-4b5c-ac17-5699e9dfb1ed	ebe58226-6813-4081-8377-b11d002e2be8	{986b958b-1a98-44e4-a59b-c02a15620381}
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	ed485c96-003c-4d05-b7ac-ac38a883257d	ee570f4f-46de-4761-ae5c-b205c92d5c3e	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	{bf5300ba-29bf-424c-bb09-e97e0e3f07c8,9d5f96cb-8d85-4216-b60e-809f39620ac6}
6c8cd73f-7443-4bea-9ae3-184f42f07320	ae3234c8-ea30-45d2-a48e-367b8c32567d	f741041f-732a-481f-bc78-461bc55ada9b	d016fde5-c284-4455-a44a-723ab77fa99f	{6f9cfc1d-dc27-4d78-8e51-51dcbf30117b,9db069b6-02be-46d9-9d45-a89f2d741ec1}
e7d913c6-fed5-465d-a619-7808378b81ff	ae3234c8-ea30-45d2-a48e-367b8c32567d	\N	\N	{}
08fa0711-fce9-419e-b46d-bdc38b61b644	ae3234c8-ea30-45d2-a48e-367b8c32567d	\N	\N	{}
6dd905b2-147a-4db1-aa8a-6f56f2bf4cbe	ae3234c8-ea30-45d2-a48e-367b8c32567d	\N	\N	{}
5ffb7fc3-7839-497f-b5e8-e936665e819f	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6,89488dc0-00e0-48c8-9e47-1f143b877878}
84759213-de14-4bea-8154-647cedb74bcf	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6,89488dc0-00e0-48c8-9e47-1f143b877878}
a109ef9e-68e4-48c0-8d25-3318e4154d07	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6,89488dc0-00e0-48c8-9e47-1f143b877878}
81318580-6a2e-4c70-9b37-0662a9898536	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6}
db0ffebb-9dbe-4772-87e8-f9444b28268e	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{}
0334a10d-2102-4261-b84c-056720515d2c	95542b83-a537-4794-a103-b768408db707	a30f21b6-918e-421d-835c-fac8228feed5	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	{2a9c364b-035d-4323-a042-7f6c3a5bedf4}
9375e424-6ea6-4841-a55f-1c1f79c8c6de	95542b83-a537-4794-a103-b768408db707	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	e54dd636-fb29-4ada-b95c-848c39ed6d03	{998cdf4a-6b17-468e-9c07-186e1a724f6d}
145af5e3-abff-4780-b9cd-5b8bba415a09	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6}
89dfef89-b49a-4ca2-9aa8-d7edc8f9e2b9	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6}
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{}
095fe615-c827-4a01-93e0-de6076d09ad2	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6,89488dc0-00e0-48c8-9e47-1f143b877878}
c59ea1f7-e9cc-4e6f-9206-f90d21fe40f8	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6}
97ab28b5-8568-4f6f-94c5-837c3c2adb9e	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6,89488dc0-00e0-48c8-9e47-1f143b877878}
bf117ea8-6a7b-40c3-9d6c-1fc9761e85f5	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	d016fde5-c284-4455-a44a-723ab77fa99f	{6f9cfc1d-dc27-4d78-8e51-51dcbf30117b}
b90a05f4-e77d-44b7-b657-a2fefd13c8fb	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6,89488dc0-00e0-48c8-9e47-1f143b877878}
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	{4ae755a7-3733-4f95-9bbe-02ce3a481fb7,7a766876-545a-42a4-bb81-ca6fa59efff6}
58c39523-485e-4456-aadd-47cd90614072	\N	\N	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	{}
8d671fbc-19f7-43ae-8e33-044a666d0ab3	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6}
7a0547b1-b065-42bf-a898-6228ef56fe5a	ae3234c8-ea30-45d2-a48e-367b8c32567d	f741041f-732a-481f-bc78-461bc55ada9b	d016fde5-c284-4455-a44a-723ab77fa99f	{6f9cfc1d-dc27-4d78-8e51-51dcbf30117b}
f1ee1810-7e79-4695-8440-6c4ac296a5c7	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{}
34e4a86b-0bb3-4d9b-b5a7-4ee047258887	ae3234c8-ea30-45d2-a48e-367b8c32567d	f741041f-732a-481f-bc78-461bc55ada9b	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{222cead4-f3bf-48d7-9541-84f60c66cac5,7a766876-545a-42a4-bb81-ca6fa59efff6,89488dc0-00e0-48c8-9e47-1f143b877878}
c7f2b0c5-f32b-443c-ab9e-bebaf0a701ad	ae3234c8-ea30-45d2-a48e-367b8c32567d	f741041f-732a-481f-bc78-461bc55ada9b	\N	{6f9cfc1d-dc27-4d78-8e51-51dcbf30117b}
b1477309-9e65-4c43-aefa-1219ef447c56	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6}
43114d29-0a14-4b78-bb61-1c119d4b3f88	ae3234c8-ea30-45d2-a48e-367b8c32567d	f741041f-732a-481f-bc78-461bc55ada9b	d016fde5-c284-4455-a44a-723ab77fa99f	{9db069b6-02be-46d9-9d45-a89f2d741ec1}
20836be9-c3ac-46a2-a33e-932405df0b8b	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6}
07b42dd0-4867-4d42-b506-dc241d85c262	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6}
a1272441-cb74-427f-a423-dcb2c1bf5c13	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6}
28f5ba79-6c18-45e1-9d23-75f047783203	ae3234c8-ea30-45d2-a48e-367b8c32567d	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	{7a766876-545a-42a4-bb81-ca6fa59efff6}
\.


--
-- Data for Name: perencanaan_tujuan; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.perencanaan_tujuan (id, tujuan) FROM stdin;
2d0dfd47-3664-463b-83df-050b7c010aac	602fa656-5eb7-42eb-b33d-27ae2bac110a
2d0dfd47-3664-463b-83df-050b7c010aac	7b46be23-f857-4cfb-bb52-1834f8bf72b9
31c1272e-dbde-4382-ae95-3ee17fbe59d7	404b5b30-1273-4e2c-a4de-a8847cb7e5c7
31c1272e-dbde-4382-ae95-3ee17fbe59d7	602fa656-5eb7-42eb-b33d-27ae2bac110a
4ac7b90f-d4ad-4bba-bad1-94493366203d	744b5300-4427-439a-a729-ab70a6a908a1
4ac7b90f-d4ad-4bba-bad1-94493366203d	e918fec9-e4a5-4903-8591-3e8b9e6f166a
eba05609-cdd3-4c38-a91b-5306eee4a6b2	602fa656-5eb7-42eb-b33d-27ae2bac110a
f21cff92-6997-48e3-a453-69bb316c132d	e918fec9-e4a5-4903-8591-3e8b9e6f166a
1c14b647-870a-43bd-aa06-f06ac1c8f56e	e918fec9-e4a5-4903-8591-3e8b9e6f166a
1c14b647-870a-43bd-aa06-f06ac1c8f56e	602fa656-5eb7-42eb-b33d-27ae2bac110a
bb7b542c-24ec-493b-80b9-ebbbad3ebd94	e918fec9-e4a5-4903-8591-3e8b9e6f166a
bb7b542c-24ec-493b-80b9-ebbbad3ebd94	602fa656-5eb7-42eb-b33d-27ae2bac110a
bb7b542c-24ec-493b-80b9-ebbbad3ebd94	fc6ced79-8c2c-4a5d-8b8c-aa75b125b31f
568a9e86-8b62-4125-a959-781ee7382863	7b46be23-f857-4cfb-bb52-1834f8bf72b9
359ea449-f314-4b68-99a0-ac42f4ba25d2	744b5300-4427-439a-a729-ab70a6a908a1
359ea449-f314-4b68-99a0-ac42f4ba25d2	404b5b30-1273-4e2c-a4de-a8847cb7e5c7
185a85fd-c805-4697-9719-21b017164891	e918fec9-e4a5-4903-8591-3e8b9e6f166a
d26350c7-bc66-4098-ab11-202d883e3c3d	e918fec9-e4a5-4903-8591-3e8b9e6f166a
8465eb58-4cb1-4e88-85cc-1ccfac8afffd	e918fec9-e4a5-4903-8591-3e8b9e6f166a
628aabdb-1589-46bc-923b-bf2b57c3fdd6	e918fec9-e4a5-4903-8591-3e8b9e6f166a
e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	e918fec9-e4a5-4903-8591-3e8b9e6f166a
9cb18ed2-4658-481f-9f74-e40105dc188b	e918fec9-e4a5-4903-8591-3e8b9e6f166a
4114359b-983c-4020-a06f-65a7c7913b15	e918fec9-e4a5-4903-8591-3e8b9e6f166a
2a6993c2-d863-4fbc-8b75-40f005f7190e	e918fec9-e4a5-4903-8591-3e8b9e6f166a
cec3e6f8-2a37-4514-a252-82968f15f2e3	404b5b30-1273-4e2c-a4de-a8847cb7e5c7
63216689-ab95-4628-bcec-8fe56198caee	744b5300-4427-439a-a729-ab70a6a908a1
aa955757-a10f-4e35-915c-c08ad42c9ae3	744b5300-4427-439a-a729-ab70a6a908a1
7ee14d19-aa59-4403-b0c1-85a7a0560da4	e918fec9-e4a5-4903-8591-3e8b9e6f166a
7ee14d19-aa59-4403-b0c1-85a7a0560da4	602fa656-5eb7-42eb-b33d-27ae2bac110a
7ee14d19-aa59-4403-b0c1-85a7a0560da4	404b5b30-1273-4e2c-a4de-a8847cb7e5c7
9973f691-2564-44d0-b825-52b55884dd9c	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
d1856679-933f-4801-acd2-7d08f130161e	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
05275cf2-b282-4653-8c37-a0d7f8923a62	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
6c8cd73f-7443-4bea-9ae3-184f42f07320	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
f79b7985-23bb-4156-a074-bc23365ded79	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
e7d913c6-fed5-465d-a619-7808378b81ff	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
08fa0711-fce9-419e-b46d-bdc38b61b644	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
6dd905b2-147a-4db1-aa8a-6f56f2bf4cbe	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
5ffb7fc3-7839-497f-b5e8-e936665e819f	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
a109ef9e-68e4-48c0-8d25-3318e4154d07	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
81318580-6a2e-4c70-9b37-0662a9898536	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
db0ffebb-9dbe-4772-87e8-f9444b28268e	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
84759213-de14-4bea-8154-647cedb74bcf	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
0334a10d-2102-4261-b84c-056720515d2c	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
9375e424-6ea6-4841-a55f-1c1f79c8c6de	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
145af5e3-abff-4780-b9cd-5b8bba415a09	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
89dfef89-b49a-4ca2-9aa8-d7edc8f9e2b9	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
274a4cfd-b67d-46e1-b695-afb76eb7975c	c1db9e90-f731-4a62-9fb8-4fd07fa0f844
c12b0e44-7d6b-406d-ae54-d3bfc4046f17	e918fec9-e4a5-4903-8591-3e8b9e6f166a
095fe615-c827-4a01-93e0-de6076d09ad2	e918fec9-e4a5-4903-8591-3e8b9e6f166a
c59ea1f7-e9cc-4e6f-9206-f90d21fe40f8	e918fec9-e4a5-4903-8591-3e8b9e6f166a
97ab28b5-8568-4f6f-94c5-837c3c2adb9e	e918fec9-e4a5-4903-8591-3e8b9e6f166a
bf117ea8-6a7b-40c3-9d6c-1fc9761e85f5	e918fec9-e4a5-4903-8591-3e8b9e6f166a
b90a05f4-e77d-44b7-b657-a2fefd13c8fb	e918fec9-e4a5-4903-8591-3e8b9e6f166a
b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	e918fec9-e4a5-4903-8591-3e8b9e6f166a
58c39523-485e-4456-aadd-47cd90614072	e918fec9-e4a5-4903-8591-3e8b9e6f166a
2ef9e777-3102-411a-8911-cce857ec0330	602fa656-5eb7-42eb-b33d-27ae2bac110a
8d671fbc-19f7-43ae-8e33-044a666d0ab3	e918fec9-e4a5-4903-8591-3e8b9e6f166a
7a0547b1-b065-42bf-a898-6228ef56fe5a	e918fec9-e4a5-4903-8591-3e8b9e6f166a
f1ee1810-7e79-4695-8440-6c4ac296a5c7	e918fec9-e4a5-4903-8591-3e8b9e6f166a
34e4a86b-0bb3-4d9b-b5a7-4ee047258887	e918fec9-e4a5-4903-8591-3e8b9e6f166a
c7f2b0c5-f32b-443c-ab9e-bebaf0a701ad	e918fec9-e4a5-4903-8591-3e8b9e6f166a
b1477309-9e65-4c43-aefa-1219ef447c56	e918fec9-e4a5-4903-8591-3e8b9e6f166a
43114d29-0a14-4b78-bb61-1c119d4b3f88	e918fec9-e4a5-4903-8591-3e8b9e6f166a
20836be9-c3ac-46a2-a33e-932405df0b8b	e918fec9-e4a5-4903-8591-3e8b9e6f166a
07b42dd0-4867-4d42-b506-dc241d85c262	e918fec9-e4a5-4903-8591-3e8b9e6f166a
a1272441-cb74-427f-a423-dcb2c1bf5c13	e918fec9-e4a5-4903-8591-3e8b9e6f166a
28f5ba79-6c18-45e1-9d23-75f047783203	e918fec9-e4a5-4903-8591-3e8b9e6f166a
\.


--
-- Data for Name: tindak_lanjut; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.tindak_lanjut (id, id_rencana, created_at, created_by, modified_at, modified_by, status, judul, spt_no, spt_date, pkpt_no, app_sv1, app_sv1_tstamp, app_sv1_note, app_sv2, app_sv2_tstamp, app_sv2_note, app_sv1_file, app_sv2_file, pelaksana, thn_lhp, no_lhp, skpd, app_sv1_status, app_sv2_status, file_akhir, tgl_lhp, proses, lap_akir, jml_temuan) FROM stdin;
d6b36ddd-c633-4973-8c16-e4d213e954e2	9375e424-6ea6-4841-a55f-1c1f79c8c6de	2018-10-02 16:29:30	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-10-02 22:03:13	222cead4-f3bf-48d7-9541-84f60c66cac5	t	Kamu adalah auraku	88888	2018-04-14	1cb169bd-485d-430d-9a5b-e2ef077c2ed3	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-10-02 22:37:49	123	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-10-02 22:50:05	123123	[{"nama":"Capture.PNG","file":"6c2080c3a5048175a51796706d07139c.PNG"}]	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
765645cf-922b-4e6e-8b89-644fa86f27c2	db0ffebb-9dbe-4772-87e8-f9444b28268e	2018-04-15 23:35:45	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-17 17:20:23	fe251bf2-8d4f-4205-af0b-51160cd31e63	t	99991111	119922	2018-04-12	67e632d6-0e25-46c9-84e1-1bf949e1d696	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-10-02 22:56:22	11111	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N
a7c7513a-8e50-4018-93b3-8f08d11c147c	\N	2018-10-03 01:07:16	ae3234c8-ea30-45d2-a48e-367b8c32567d	\N	\N	t	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-10-03 01:10:05	hefs	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-10-03 01:12:13	hehehe	\N	[{"nama":"Capture.PNG","file":"1fea6ecb4e37f1631a81031a595da222.PNG"}]	141	14114	14141	14141	\N	\N	\N	\N	0	\N	\N
0387e964-a884-48b4-9ce9-02500df0375f	\N	2018-10-04 10:54:16	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	bbkpd	2018	01288	judul	\N	\N	\N	2018-04-10	0	\N	\N
d4cfda65-260e-4162-80a0-1a7d8f53f892	\N	2018-10-04 11:00:18	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	12	12	12	12	\N	\N	\N	2018-10-22	0	\N	\N
e478b5e6-07c9-48e2-8efb-02a33efc8337	\N	2018-10-02 23:39:01	ae3234c8-ea30-45d2-a48e-367b8c32567d	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	inspektorat	2018	2819289128	hhahahahahahaahh	\N	\N	\N	\N	0	\N	\N
27481acf-857b-4c0d-a191-4fe92cf7f474	\N	2018-10-03 00:48:40	ae3234c8-ea30-45d2-a48e-367b8c32567d	\N	\N	t	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-10-09 12:52:30	tyesaaaa	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-10-09 13:03:36	oke lah bole	\N	\N	11	1111	11	111	t	t	\N	\N	3	[{"nama":"db stok.jpg","file":"57e62d6bdcf779e1a211ffe75fad9498.jpg"}]	\N
9f1f28c9-fe55-4c59-b70c-05a8dd36eb8d	\N	2018-10-27 00:03:53	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	BPKB	2018	012930	Judul SKPD	\N	\N	\N	2018-10-26	0	\N	\N
68ca798b-ba6d-4bca-9bc8-34d9a831b233	\N	2018-10-27 00:11:46	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N	t	\N	\N	\N	\N	222cead4-f3bf-48d7-9541-84f60c66cac5	2018-10-27 00:13:11	gas	ae3234c8-ea30-45d2-a48e-367b8c32567d	2018-10-27 00:13:27	gas	\N	\N	BPKBP	2018	0001	judul skpd	t	t	\N	2018-10-27	2	\N	\N
3d0ddb70-6f7f-4271-a6d7-9ae2944ba209	\N	2018-11-16 01:09:12	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	BPK	2018	9021080	Temuan & Tindak Lanjut Korupsi Dana BOS	\N	\N	\N	2018-11-16	0	\N	1
dc8f007f-7ef6-49e9-a35b-d6f40c1d48c0	4ac7b90f-d4ad-4bba-bad1-94493366203d	2018-11-17 15:20:42	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N	t	\N	\N	\N	\N	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-11-17 16:00:19	Teeesss	\N	\N	\N	\N	\N	\N	2018	9900	Temuan Tindak Korupsi RT 1 Muncar	t	\N	\N	2018-11-17	1	\N	1
\.


--
-- Data for Name: tindak_lanjut_penyebab; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.tindak_lanjut_penyebab (id, kode_penyebab, uraian_penyebab, urutan) FROM stdin;
3d0ddb70-6f7f-4271-a6d7-9ae2944ba209	94929f69-2a70-43a4-ba9d-8629aa200c49	Teledor?	1
3d0ddb70-6f7f-4271-a6d7-9ae2944ba209	94929f69-2a70-43a4-ba9d-8629aa200c49	Kurang bertanggung jaab	2
dc8f007f-7ef6-49e9-a35b-d6f40c1d48c0	94929f69-2a70-43a4-ba9d-8629aa200c49	Terjadinya kelalaian	1
\.


--
-- Data for Name: tindak_lanjut_rekom; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.tindak_lanjut_rekom (id, kode_rekom, uraian_rekom, no_tl, uraian_tl, urutan) FROM stdin;
3d0ddb70-6f7f-4271-a6d7-9ae2944ba209	f251d5dd-f896-4892-b5b0-d61fec46d7fd	Pengunaan TL 1	3ae6a4bc-73df-415b-b3be-48d9a80fbf01	Memberikan Peringatan kepada Bendahara 1A	1
3d0ddb70-6f7f-4271-a6d7-9ae2944ba209	af19cc09-3175-48ec-a11f-52a6527e31cc	Penggunaan TL2	3ae6a4bc-73df-415b-b3be-48d9a80fbf01	Memberikan Peringatan kepada kepala sekolah	1
dc8f007f-7ef6-49e9-a35b-d6f40c1d48c0	f251d5dd-f896-4892-b5b0-d61fec46d7fd	Melakukan penyidikan dan auditing di RT 1 Muncar	3ae6a4bc-73df-415b-b3be-48d9a80fbf01	Memberikan peringatan kepada pihak yang bertanggung jaab	1
\.


--
-- Data for Name: tindak_lanjutd; Type: TABLE DATA; Schema: interpro; Owner: eauditpr_user
--

COPY interpro.tindak_lanjutd (id, kode_temuan, kode_rekom, negara, daerah, tindak_lanjut, hasil_rekom, nilai_rekom, keterangan, obrik, akibat, penyebab, kondisi, kriteria, tujuan_tl, sasaran_tl, evaluasi, ruang, tanggapan, tindak_lanjut_s, nd, judul_tl, uraian_rekom, judul_temuan, uraian_temuan, nilai_temuan) FROM stdin;
765645cf-922b-4e6e-8b89-644fa86f27c3	5ec40809-3eba-4239-bfb3-bffd0fa4b8ff	f251d5dd-f896-4892-b5b0-d61fec46d7fd	44	0	TEST	1	45	HAHA	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
765645cf-922b-4e6e-8b89-644fa86f27c4	5ec40809-3eba-4239-bfb3-bffd0fa4b8ff	f251d5dd-f896-4892-b5b0-d61fec46d7fd	43	0	0	2	412	0	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
765645cf-922b-4e6e-8b89-644fa86f27c2	5ec40809-3eba-4239-bfb3-bffd0fa4b8ff	f251d5dd-f896-4892-b5b0-d61fec46d7fd	5	0	Hahahahaha	1	23	2	1	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
3d0ddb70-6f7f-4271-a6d7-9ae2944ba209	5ec40809-3eba-4239-bfb3-bffd0fa4b8ff	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Dana BOS tidak diberikan	Dana BOS yang seharusnya sudah cair namun tidak di berikan kepada SMK 1 Malang	\N	\N	\N	\N	\N	\N	\N	\N	\N	Korupsi Dana BOS 2018	Telah Terjadi tindak Pidana Korupsi dana BOS sekolah SMK 1 Malang	56200000
f7a4aca1-6e17-4dbf-bf56-e0c6d4a023f2	5ec40809-3eba-4239-bfb3-bffd0fa4b8ff	f251d5dd-f896-4892-b5b0-d61fec46d7fd	2	0	2	1	2	2	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N	\N
a7c7513a-8e50-4018-93b3-8f08d11c147c	5ec40809-3eba-4239-bfb3-bffd0fa4b8ff	f251d5dd-f896-4892-b5b0-d61fec46d7fd	12	0	12	1	12	121212	12	12121	1212	1212	212	12	12	1212	12	12	122	1	121	\N	\N	\N	\N
0387e964-a884-48b4-9ce9-02500df0375f	5ec40809-3eba-4239-bfb3-bffd0fa4b8ff	f251d5dd-f896-4892-b5b0-d61fec46d7fd	12	0	tl	1	12	jke	yda	aju	oenya	knd	kru	tunb	sasa	eva	raa	ta	ytls	1	hd	\N	\N	\N	\N
dc8f007f-7ef6-49e9-a35b-d6f40c1d48c0	5ec40809-3eba-4239-bfb3-bffd0fa4b8ff	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	 tindak korupsi > 200 JT	Telah Terjadi  tindak korupsi > 200 JT	\N	\N	\N	\N	\N	\N	\N	\N	\N	Temuan Korupsi 	Temuan tindak korupsi > 200 JT	200000000
d4cfda65-260e-4162-80a0-1a7d8f53f892	5ec40809-3eba-4239-bfb3-bffd0fa4b8ff	f251d5dd-f896-4892-b5b0-d61fec46d7fd	2	0	2	1	2	2	2	2	2	2	2	2	2	2	2	2	2	1	2	\N	\N	\N	\N
d4cfda65-260e-4162-80a0-1a7d8f53f892	5ec40809-3eba-4239-bfb3-bffd0fa4b8ff	f251d5dd-f896-4892-b5b0-d61fec46d7fd	1	0	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	\N	\N	\N	\N
9f1f28c9-fe55-4c59-b70c-05a8dd36eb8d	5ec40809-3eba-4239-bfb3-bffd0fa4b8ff	f251d5dd-f896-4892-b5b0-d61fec46d7fd	123	0	tl	1	123	keterangan	umu obrik	akb	penyebab	kondisi	kriteria	tuj	sasa	eva	rl	tang	tls	1	Judul Temuan	\N	\N	\N	\N
68ca798b-ba6d-4bca-9bc8-34d9a831b233	5ec40809-3eba-4239-bfb3-bffd0fa4b8ff	f251d5dd-f896-4892-b5b0-d61fec46d7fd	123	0	ti	3	12312	1231	o	a	p	k	k	t	s	e	r	t	tis	1	j	Uraian Rekom	\N	\N	\N
\.


--
-- Data for Name: conf_spt; Type: TABLE DATA; Schema: master; Owner: eauditpr_user
--

COPY master.conf_spt (id, ket, "order", status) FROM stdin;
cc341d9e-d423-43f3-8bb4-97074dd2dfe0	Peraturan Menteri Dalam Negeri Nomor 76 Tahun 2016 tentang Kebijakan Pengawasan di\r\n            Lingkungan Kementerian Dalam Negeri dan Penyelenggaraan Pemerintahan Daerah Tahun 2017	1	t
523d6e11-069a-40e2-9585-e4ee40041968	Peraturan Daerah Kabupaten Banyuwangi Nomor 8 Tahun 2016 tentang Pembentukan dan Susunan Perangkat Daerah             Kabupaten Banyuwangi	2	t
995b78d9-4b3e-4dcc-837d-c6640a8b74a6	Peraturan Bupati Banyuwangi Nomor 71 Tahun 2016 tentang Kedudukan, Susunan Organisasi, Tugas dan Fungsi             serta Tata Kerja Inspektorat Kabupaten Banyuwangi;	3	t
3d6fb3d0-802e-4b40-8c94-3e1082d839bf	Dokumen Pelaksanaan Anggaran (DPA-SKPD) Tahun 2017 Nomor : 1.20.07.20.01.5.2.	4	t
\.


--
-- Data for Name: dasar_penugasan; Type: TABLE DATA; Schema: master; Owner: eaudit
--

COPY master.dasar_penugasan (id, kode, dasar, "order", show, other) FROM stdin;
\.


--
-- Data for Name: golongan; Type: TABLE DATA; Schema: master; Owner: eaudit
--

COPY master.golongan (id, ket, "order", status) FROM stdin;
0a6b9dce-f9ef-4fee-a95e-e5fd328b27b9	Penata Muda / III a	14	t
2298e57d-292b-4ce0-8374-5b2e4bb3dea4	Penata Muda Tingkat I / III b	13	t
2a5587b8-96ed-428b-bda5-07ec4b434b80	Penata / III c	12	t
3f8f7984-099c-4bde-bdc2-ac8e1f12122c	Juru Muda Tingkat I / I b	33	t
4e6e19e1-0a5d-47dd-8704-2d0972260254	Pembina Madya / IV d	2	t
7b2e4be7-9d87-4145-b1c4-ed362401ff6d	Pembina Tingkat I / IV b	4	t
89acc71d-d12c-4d4b-b33f-15b64e7b57e2	Juru / I c	32	t
89f98bab-43b3-4867-8159-392e61ad4772	Pembina / IV a	5	t
942202d3-9fbf-420b-927e-e37f9465e8d1	Penata Tingkat I / III d	11	t
99332cbf-f6f0-4e9c-9b0a-269251b94ce8	Juru Tingkat I / I d	31	t
9beef4e6-b046-41b6-bf2f-f7d59e0e288a	Juru Muda / I a	34	t
9fc0f6a6-0fcc-4065-9b89-854ff3b4ce5b	Pembina Utama / IV e	1	t
a94a2941-bf79-44a5-b4c6-4fada8d43d5f	Pengatur Muda / II a	24	t
af1c7bcf-0cb4-4e55-8244-131146db1022	Pengatur Tingkat I / II d	21	t
b6fde387-9e09-401d-a97f-fa1c1b8aee6f	Pengatur Muda Tingkat I / II b	23	t
ebf8827d-3189-4ce1-b192-a3615234ef51	Pengatur / II c	22	t
2d3504c0-44cf-481f-8324-82f4b48e1fcb	Pembina Utama Muda / IV c	3	t
\.


--
-- Data for Name: jabatan; Type: TABLE DATA; Schema: master; Owner: eaudit
--

COPY master.jabatan (id, ket, "order", status) FROM stdin;
7f7ccbdb-950a-4c3c-9107-3e2045a0d892	Pengawas Pemerintahan Madya	8	t
e3b6c364-9335-4257-8ed1-95c309d8fc78	Inspektur Pembantu Wil. II	9	t
69d50c86-8a8e-4fd0-a702-214b23b6666a	Auditor Pertama	5	f
41fa4cd8-d1ff-4bf9-b120-cf91d48f5e2e	Audutor Penyelia	6	f
637cfb84-1b29-46a8-9c36-ccf92f35253d	Auditor Muda	7	f
a5786e3b-2486-496a-8186-eaadb919a47b	Inspektur Pembantu Wil. III	10	f
93597754-576c-4830-845b-6536adaa21f4	Inspektur Pembantu Wil. IV	11	f
277a6cdf-fb68-4f86-ae11-36095327438e	Inspektur Pembantu Wil. V	12	f
7a363d52-3c85-4097-82db-af977ecd03c3	Inspektur	2	t
821d7135-b8d2-4945-8c03-7e3b1d2614cd	Ketua Umum	1	t
966f66dd-aaaf-464f-a0e9-b6e6fe5182db	Inspektur Pembantu Wil. I	3	t
2e179b51-5924-400f-a6f8-4505f5f900e6	Auditor Ahli Madya	4	t
\.


--
-- Data for Name: jenis; Type: TABLE DATA; Schema: master; Owner: eauditpr_user
--

COPY master.jenis (id, kode, ket, "order", status) FROM stdin;
0126ec40-142d-4969-aa61-bf4415a93841	12	Bimbingan Teknis	13	t
04f2f62f-5f1f-4fe7-a181-6d2a27b14a54	16	Rapat Evaluasi	17	t
20072f05-5b06-4b1e-8544-cde9c6b78c18	11	Pendampingan	12	t
33488044-6912-4253-99fd-e7a86b1b3f96	13	Workshop	14	t
351c7ddd-6659-48ff-a8d0-af6848770361	03	Audit Operasional	0	t
38aff685-4ebc-4794-865d-92981026d1f3	14	Pendidikan dan Pelatihan	15	t
45e84c28-77d7-4a74-92c9-98087cbee550	99	Penugasan Lainnya	0	t
4f5f1807-d852-4b2d-9e09-2d06dc601055	06	Audit Investigasi	0	t
519bbf54-6048-4a14-b184-898e521d9094	01	Audit Ketaatan	0	t
71e5982e-2cc9-48c5-91d4-c750046bbc95	09	Klarifikasi	0	t
8f4380a5-b984-4848-8f99-ff1d92abb004	10	Pembinaan	0	t
9138adc4-cc48-4ef9-af93-66734f97a39c	05	Penugasan Reviu	0	t
a950e68a-4c7c-41ee-860b-a82878ead394	15	Pelatihan di Kantor Sendiri	16	t
b48074ba-9e37-4e4a-be7a-0d7edc4c69a8	02	Audit Keuangan	0	t
d49b3332-1b95-43c0-8c8f-25842cf7f537	04	Audit Kinerja	0	t
47610b8d-0476-4174-9128-996685a51354	17	Audit Operasional Sekolah Negeri	18	t
ab481ec4-8498-4404-ba87-d0955bb05287	98	PKS	19	t
72f15fac-60fd-4108-9e82-e08357d8ecc0	08	Monitoring dan Evaluasi	0	t
f310107e-0a0c-4971-8a34-57e761145aaa	07	Audit Tujuan tertentu	0	t
\.


--
-- Data for Name: kendali; Type: TABLE DATA; Schema: master; Owner: eauditpr_user
--

COPY master.kendali (id, nama, status) FROM stdin;
be7c8577-7b34-4f86-a256-b014f0ba84aa	kendali1	t
\.


--
-- Data for Name: penyebab; Type: TABLE DATA; Schema: master; Owner: eauditpr_user
--

COPY master.penyebab (id, nama, status, kode) FROM stdin;
94929f69-2a70-43a4-ba9d-8629aa200c49	penyebab1	t	3.0.0.1
e72f10eb-d970-42f4-a582-c0a872aac9eb	Dana tidak memadai	f	3.0.0.0
\.


--
-- Data for Name: pkpt; Type: TABLE DATA; Schema: master; Owner: eaudit
--

COPY master.pkpt (id, no, tema, nama, kegiatan, hp, rmp, rpl, dana, risiko, keterangan, status, jenis, sasaran, tujuan) FROM stdin;
1206eaa7-ffbc-451b-ab3e-ac0e69c53bd6	20170002	Audit ketaatan pertanggungjawaban keuangan	Dinas Perindustrian dan Perdagangan  (Pengelolaan Keuangan dan Asset TA. 2016)	Assurance	48	201701	201702	33276000	Tinggi	Tinggi	t	\N	\N	\N
9e67be82-a43a-4249-822f-0efd85095596	20170003	Audit ketaatan pertanggungjawaban keuangan	Dinas PU Pengairan	Assurance	48	201701	201702	33276000	Sangat Tinggi	Sangat Tinggi	t	\N	\N	\N
cee57bf1-73a0-43ee-8b6f-9898b2002b48	20170004	Audit ketaatan pertanggungjawaban keuangan	Dinas PU Cipta Karya dan Penataan Ruang	Assurance	48	201701	201702	33276000	Sangat Tinggi	Sangat Tinggi	t	\N	\N	\N
4da7ce19-6326-4c7c-a974-a0b9938974e9	20170005	Audit ketaatan pertanggungjawaban keuangan	Dinas Kesehatan	Assurance	48	201701	201702	33276000	Sangat Tinggi	Sangat Tinggi	t	\N	\N	\N
fe5c13fb-aecd-4cc3-9efb-45e6db6b35f3	20170006	Audit ketaatan pertanggungjawaban keuangan	Dinas Perhubungan	Assurance	48	201701	201702	33276000	Tinggi	Tinggi	t	\N	\N	\N
58898776-1c53-4343-ac20-be25daafbda9	20170007	Audit ketaatan pertanggungjawaban keuangan	Badan Pendapatan Daerah	Assurance	48	201701	201702	33276000	Tinggi	Tinggi	t	\N	\N	\N
4464d4dd-5627-418d-bc23-3704d77f24bd	20170008	Audit ketaatan pertanggungjawaban keuangan	Badan Lingkungan Hidup	Assurance	48	201701	201702	33276000	Sedang	Sedang	t	\N	\N	\N
ee2bcc00-f9dd-410b-93f4-ea711ddbfed7	20170009	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Kalibaru	Assurance	48	201702	201703	33276000	Sedang	Sedang	t	\N	\N	\N
8ff1b526-2a18-4792-9ca2-2e45bc1da4db	20170010	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Siliragung	Assurance	48	201702	201703	33276000	Sedang	Sedang	t	\N	\N	\N
df850095-86b4-4990-a956-e9fea685e34d	20170011	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Glenmore	Assurance	48	201702	201703	33276000	Sedang	Sedang	t	\N	\N	\N
fd88d330-f9e6-4dde-a355-0d027aa4e231	20170012	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Songgon	Assurance	48	201702	201703	33276000	Sedang	Sedang	t	\N	\N	\N
c46eeb42-e9e6-42b0-8071-43dce00bbf6b	20170013	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Wongsorejo	Assurance	48	201702	201703	33276000	Sedang	Sedang	t	\N	\N	\N
a3382bc2-76ff-48f7-8c74-e1cd4cdadbfe	20170014	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Muncar	Assurance	48	201702	201703	33276000	Sedang	Sedang	t	\N	\N	\N
985abad1-7977-4c57-87aa-6217c4815fa2	20170015	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Pesanggaran	Assurance	48	201702	201703	33276000	Sedang	Sedang	t	\N	\N	\N
892ab6c5-a9c1-4d02-8cd6-6191c9f5c57e	20170016	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Purwoharjo	Assurance	48	201702	201703	33276000	Sedang	Sedang	t	\N	\N	\N
c3f9db98-8a38-4faa-8203-fcb3e17fb3a1	20170017	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Cluring	Assurance	48	201703	201704	33276000	Sedang	Sedang	t	\N	\N	\N
8bf970da-243c-4f5f-9c50-33d38e68f39d	20170018	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Kabat	Assurance	48	201703	201704	33276000	Sedang	Sedang	t	\N	\N	\N
75ecbad0-0b12-4b5d-87d3-7b358f29fe78	20170019	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Rogojampi	Assurance	48	201703	201704	33276000	Sedang	Sedang	t	\N	\N	\N
67e632d6-0e25-46c9-84e1-1bf949e1d696	20170020	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Singojuruh	Assurance	48	201703	201704	33276000	Sedang	Sedang	t	\N	\N	\N
b60832b9-94a3-437c-be24-fb68b312a1bb	20170021	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Tegaldlimo	Assurance	48	201703	201704	33276000	Sedang	Sedang	t	\N	\N	\N
b27fb448-b888-46e4-acfa-121e894e0ea5	20170022	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Genteng	Assurance	48	201703	201704	33276000	Sedang	Sedang	t	\N	\N	\N
c48114ac-2c35-46cf-b376-b36561286d32	20170023	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Gambiran	Assurance	48	201703	201704	33276000	Sedang	Sedang	t	\N	\N	\N
d962f7c4-3ce4-4c0a-8e48-4aa1a80209b3	20170024	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Bangorejo	Assurance	48	201703	201704	33276000	Sedang	Sedang	t	\N	\N	\N
17db2c9c-1dcc-41c2-8bac-56f71819af60	20170025	Audit ketaatan pertanggungjawaban keuangan	Dinas Pendidikan	Assurance	48	201704	201705	33276000	Sangat Tinggi	Sangat Tinggi	t	\N	\N	\N
3da74478-2f78-462e-b581-a1184db27bf1	20170026	Audit ketaatan pertanggungjawaban keuangan	Dinas Pertanian	Assurance	48	201704	201705	33276000	Tinggi	Tinggi	t	\N	\N	\N
02b02e6a-bd31-4b40-a4a8-b410f2ab3b92	20170027	Audit ketaatan pertanggungjawaban keuangan	Dinas PU Pengairan	Assurance	48	201704	201705	33276000	Sangat Tinggi	Sangat Tinggi	t	\N	\N	\N
b8be7293-9abf-4aea-8672-ac78762cb330	20170028	Audit ketaatan pertanggungjawaban keuangan	Dinas PU Cipta Karya dan Penataan Ruang 	Assurance	48	201704	201705	33276000	Sangat Tinggi	Sangat Tinggi	t	\N	\N	\N
e0b3c426-6f2d-421b-880c-e1e89b700976	20170029	Audit ketaatan pertanggungjawaban keuangan	Dinas Kesehatan	Assurance	48	201704	201705	33276000	Sangat Tinggi	Sangat Tinggi	t	\N	\N	\N
69f9fdc2-c149-4188-a58d-a5b3deb35bc0	20170030	Audit ketaatan pertanggungjawaban keuangan	Dinas Kesehatan	Assurance	48	201704	201705	33276000	Sangat Tinggi	Sangat Tinggi	t	\N	\N	\N
18eae684-2980-42d8-a96b-466de5c8d52a	20170031	Audit ketaatan pertanggungjawaban keuangan	Dinas PU Pengairan (2)	Assurance	48	201704	201705	33276000	Tinggi	Tinggi	t	\N	\N	\N
3ca4bfb0-f594-414e-b984-4fa417a7f6de	20170032	Audit ketaatan pertanggungjawaban keuangan	Dinas Pendidikan	Assurance	48	201704	201705	33276000	Tinggi	Tinggi	t	\N	\N	\N
08282a76-9f1c-4647-bd83-da685ab2b6da	20170033	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Blimbingsari 	Assurance	48	201705	201706	33276000	Sedang	Sedang	t	\N	\N	\N
d1f5b002-c8c5-4e69-8aea-675d1621eeb0	20170034	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Srono	Assurance	48	201705	201706	33276000	Sedang	Sedang	t	\N	\N	\N
9769652a-4d4a-4e5b-adb4-2935fee478f7	20170035	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Tegalsari	Assurance	48	201705	201706	33276000	Sedang	Sedang	t	\N	\N	\N
6d574dab-076e-4ba4-9008-4c38bb15db37	20170036	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Glagah	Assurance	48	201705	201706	33276000	Sedang	Sedang	t	\N	\N	\N
ebe69ba9-ce68-4295-b996-87c12e04cb55	20170037	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Sempu	Assurance	48	201705	201706	33276000	Sedang	Sedang	t	\N	\N	\N
4bec5c1c-e190-4f28-abf0-3b692ce84799	20170038	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Giri	Assurance	48	201705	201706	33276000	Sedang	Sedang	t	\N	\N	\N
80838b00-4330-4b99-a2e5-586b2d4f503e	20170039	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Kalipuro	Assurance	48	201705	201706	33276000	Sedang	Sedang	t	\N	\N	\N
ef7b15dd-2ede-4b6d-b66f-d920d7efa8dd	20170040	Audit ketaatan pertanggungjawaban keuangan	Kecamatan Licin	Assurance	48	201705	201706	33276000	Sedang	Sedang	t	\N	\N	\N
243dfd57-053e-41d0-8cef-e9309eba6cc7	20170041	Evaluasi SAKIP Kecamatan	Evaluasi dan Implementasi Sakip pada Kecamatan Kabat	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
3d1e53c7-3d12-4981-9d03-54707ed171a6	20170042	Evaluasi SAKIP Kecamatan	Evaluasi dan Implementasi Sakip pada Kecamatan Srono	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
a231a768-ae6b-4db1-8547-7578de4dc690	20170043	Evaluasi SAKIP Kecamatan	Evaluasi dan Implementasi Sakip pada Kecamatan Bangorejo	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
010ad47c-2df2-4abf-bd9a-df00e57ed742	20170044	Evaluasi SAKIP Kecamatan	Evaluasi dan Implementasi Sakip pada Kecamatan Singojuruh	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
ef7265d5-9564-4bc1-8268-8f230b417274	20170045	Evaluasi SAKIP Kecamatan	Evaluasi dan Implementasi Sakip pada Kecamatan Sempu	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
03c791be-7c8f-4332-8499-464f33b93785	20170046	Evaluasi SAKIP Kecamatan	Evaluasi dan Implementasi Sakip pada Kecamatan Giri	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
b674b1a4-ff26-40bd-8401-8c36bf9902c6	20170047	Evaluasi SAKIP Kecamatan	Evaluasi dan Implementasi Sakip pada Kecamatan Pesanggaran	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
ab9952a4-50ea-4255-8ad2-e6dc50a874ce	20170048	Evaluasi SAKIP Kecamatan	Evaluasi dan Implementasi Sakip pada Kecamatan Licin	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
6a0cfefe-0121-4848-8ed8-a04c6cb7278c	20170049	Evaluasi SPIP SKPD	Evaluasi SPIP pada Dinas Pendidikan Kab. Banyuwangi	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
a97a7f14-08de-40fa-8c66-a62650c11058	20170050	Evaluasi SPIP SKPD	Evaluasi SPIP pada Dinas Perindustrian dan Perda-gangan Kab. Banyuwangi	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
dacfe617-b343-4142-8d80-e37fd9fa1f8a	20170051	Evaluasi SPIP SKPD	Evaluasi SPIP pada Dinas PU Pengairan Kab. Banyuwangi	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
b87cd8dc-c699-45af-bbff-4120ab081361	20170052	Evaluasi SPIP SKPD	Evaluasi SPIP pada Dinas Kesehatan Kab. Banyuwangi	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
0d98fe6a-6093-40ff-b512-41f6708bce48	20170053	Evaluasi SPIP SKPD	Evaluasi SPIP pada Dinas Kesehatan Kab. Banyuwangi	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
ed469292-aa6f-4a9b-8a43-150d7330e847	20170054	Evaluasi SPIP SKPD	Evaluasi SPIP pada Dinas Perhubungan Kab. Banyuwangi	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
4d6a7d6f-5aef-4bc6-be59-0b83780ebc53	20170055	Evaluasi SPIP SKPD	Evaluasi SPIP pada Dinas Pertanian Kab. Banyuwangi	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
8bb129e6-fcf4-4dbb-a155-b9edc2f5582e	20170056	Evaluasi SPIP SKPD	Evaluasi SPIP pada Dinas Pendidikan  Kab. Banyuwangi	Assurance	18	201705	201706	7759500	Sedang	Sedang	t	\N	\N	\N
0c529637-0ad7-47af-98e1-f88087e7e7c0	20170057	Evaluasi Implementasi SAKIP	 Evaluasi Implementasi Sakip pada Kecamatan Kalibaru	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
21323cbf-c1ab-43cf-8a36-286f20c65eaf	20170058	Evaluasi Implementasi SAKIP	 Evaluasi Implementasi Sakip pada Kecamatan Siliragung	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
5b1e9e7e-6e96-404a-a865-ca7aad4ced23	20170059	Evaluasi Implementasi SAKIP	 Evaluasi Implementasi Sakip pada Kecamatan Rogojampi	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
87a785fc-f94e-49d4-b8f3-30fb3933b2e8	20170060	Evaluasi Implementasi SAKIP	 Evaluasi Implementasi Sakip pada Kecamatan Songgon	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
bd97de0b-b60d-47e6-8ceb-e8aeb8bb7527	20170061	Evaluasi Implementasi SAKIP	 Evaluasi Implementasi Sakip pada Kecamatan Wongsorejo	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
fd81d82d-097b-4708-9b4a-df80d465982d	20170062	Evaluasi Implementasi SAKIP	 Evaluasi Implementasi Sakip pada Kecamatan Muncar	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
e0d4fa42-31d7-4dc3-9a68-0fe4e8a9cff3	20170063	Evaluasi Implementasi SAKIP	 Evaluasi Implementasi Sakip pada Kecamatan Kalipuro	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
3980565b-bef8-4e9c-95bd-744af1242389	20170064	Evaluasi Implementasi SAKIP	 Evaluasi Implementasi Sakip pada Kecamatan Purwoharjo	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
beaa5cd2-2be9-412d-86da-8f5462b7c58d	20170065	Evaluasi Implementasi SAKIP	Evaluasi Implementasi Sakip pada Satuan SKPD (1)	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
891f3545-c468-4a9d-adb9-87ba7df9d1ea	20170066	Evaluasi Implementasi SAKIP	Evaluasi Implementasi Sakip pada Satuan SKPD (2)	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
543ab089-e720-4b26-b37a-fb408ebc4264	20170067	Evaluasi Implementasi SAKIP	Evaluasi Implementasi Sakip pada Satuan SKPD (3)	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
a4277c6c-7901-477f-aa61-0fe5f2dc80e9	20170068	Evaluasi Implementasi SAKIP	Evaluasi Implementasi Sakip pada Satuan SKPD (4)	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
db925112-05da-4676-b210-0bac04a54e26	20170069	Evaluasi Implementasi SAKIP	Evaluasi Implementasi Sakip pada Satuan SKPD (5)	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
1875bfe7-68cc-4c58-b014-a950053532b1	20170070	Evaluasi Implementasi SAKIP	Evaluasi Implementasi Sakip pada Satuan SKPD (6)	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
1a44fe90-fe9d-4306-87fc-c12dd725ae19	20170071	Evaluasi Implementasi SAKIP	Evaluasi Implementasi Sakip pada Satuan SKPD (7)	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
7f2fe589-5ded-40da-8c51-5d2e4fca5c5c	20170072	Evaluasi Implementasi SAKIP	Evaluasi Implementasi Sakip pada Satuan SKPD (8)	Assurance	18	201706	201707	7759500	Sedang	Sedang	t	\N	\N	\N
32ec20ce-c4b4-47e1-8c2d-ec198bae3dfe	20170073	Audit Operasional 	Pengelolaan Keuangan dan Aset Tahun 2017 pada Dinas Pemberdayaan Perempuan dan KB Kab. Banyuwangi.	Assurance	48	201707	201708	33276000	Tinggi	Tinggi	t	\N	\N	\N
a52d8b1d-575b-48af-9b7a-8c217faa6bb2	20170074	Audit Operasional 	Pengelolaan Keuangan dan Aset Tahun 2017 pada Badan Kesatuan Bangsa dan Politik Kab. Banyuwangi.	Assurance	48	201707	201708	33276000	Tinggi	Tinggi	t	\N	\N	\N
45992980-e5b5-4f18-96f6-47b4aecef38c	20170075	Audit Operasional 	Pengelolaan Keuangan dan Aset Tahun 2017 pada Dinas Koperasi dan Usaha Mikro Kab. Banyuwangi.	Assurance	48	201707	201708	33276000	Tinggi	Tinggi	t	\N	\N	\N
4bb85d50-9794-430b-97bf-8ff1103c43e4	20170076	Audit Operasional 	Pengelolaan Keuangan dan Aset Tahun 2017 pada RSUD Genteng Kab. Banyuwangi.	Assurance	48	201707	201708	33276000	Tinggi	Tinggi	t	\N	\N	\N
ee628eef-4c79-4e13-80be-a3aa06587906	20170077	Audit Operasional 	Pengelolaan Keuangan dan Aset Tahun 2017 pada Dinas Pariwisata dan Kebudayaan Kabupaten  Banyuwangi.	Assurance	48	201707	201708	33276000	Tinggi	Tinggi	t	\N	\N	\N
3682a531-9c99-46ef-8a41-0aede5fecaf9	20170078	Audit Operasional 	Pengelolaan Keuangan dan Aset Tahun 2017 pada Dinas Pemberdayaan Masyarakat dan Desa Kab. Banyuwangi.	Assurance	48	201707	201708	33276000	Tinggi	Tinggi	t	\N	\N	\N
44a5b224-2d69-4d16-83fb-dcdf32d2a012	20170079	Audit Operasional 	Pengelolaan Keuangan dan Aset Tahun 2017 pada Dinas Kependudukan dan Pencatatan Sipil Kabupaten Banyuwangi.	Assurance	48	201707	201708	33276000	Tinggi	Tinggi	t	\N	\N	\N
afbf97ab-e26e-4ece-9e2e-2af3cc0f9ef6	20170080	Audit Operasional 	Pengelolaan Keuangan dan Aset Tahun 2017 pada Dinas Perikanan  Kab. Banyuwangi.	Assurance	48	201707	201708	33276000	Tinggi	Tinggi	t	\N	\N	\N
d5da7230-a8fc-4416-b91d-8086055637b0	20170081	Reviu Renja SKPD	Reviu Renja pada  Dinas Pendidikan	Assurance	18	201707	201708	7759500	Rendah	Rendah	t	\N	\N	\N
ec8260de-4370-4456-b488-57909acb93ed	20170082	Reviu Renja SKPD	Reviu Renja pada  Dinas Pertanian	Assurance	18	201707	201708	7759500	Rendah	Rendah	t	\N	\N	\N
32a3fc75-ffbe-4ce8-bcbf-96f3f6c89acd	20170083	Reviu Renja SKPD	Reviu Renja pada  Dinas PU Pengairan	Assurance	18	201707	201708	7759500	Rendah	Rendah	t	\N	\N	\N
b72436eb-e86a-46e2-b636-f9b58e2b23bb	20170084	Reviu Renja SKPD	Reviu Renja pada  Dinas PU Cipta Karya dan 	Assurance	18	201707	201708	7759500	Rendah	Rendah	t	\N	\N	\N
168735ad-db50-4b06-8905-157b8bf89813	20170085	Reviu Renja SKPD	Reviu Renja pada  Dinas Kesehatan	Assurance	18	201707	201708	7759500	Rendah	Rendah	t	\N	\N	\N
fc029623-f08d-4d43-b5ac-c74041317376	20170086	Reviu Renja SKPD	Reviu Renja pada  Dinas Perhubungan	Assurance	18	201707	201708	7759500	Rendah	Rendah	t	\N	\N	\N
f750f332-f0f6-407c-aba8-d23fb7520b89	20170087	Reviu Renja SKPD	Reviu Renja pada  Sekretariat Daerah	Assurance	18	201707	201708	7759500	Rendah	Rendah	t	\N	\N	\N
0dda3929-4776-4d33-bc13-cf2a8b37965b	20170088	Reviu Renja SKPD	Reviu Renja pada  Dinas Lingkungan Hidup	Assurance	18	201707	201708	7759500	Rendah	Rendah	t	\N	\N	\N
4b13eb07-26f1-4059-a4a8-1d900b01080b	20170089	Audit Operasional pendapatan	Audit Operasional Pendapatan PBB pada  Kecamatan Kalibaru	Assurance	18	201707	201708	7759500	Sedang	Sedang	t	\N	\N	\N
1cb169bd-485d-430d-9a5b-e2ef077c2ed3	20170090	Audit Operasional pendapatan	Audit Operasional Pendapatan PBB pada   Kecamatan Kabat	Assurance	18	201707	201708	7759500	Sedang	Sedang	t	\N	\N	\N
d26f5315-ec4e-49e3-be03-7538a1ffae3f	20170091	Audit Operasional pendapatan	Audit Operasional Pendapatan PBB pada    Kecamatan Rogojampi	Assurance	18	201707	201708	7759500	Sedang	Sedang	t	\N	\N	\N
7e916896-c0f6-45aa-91ee-361ba3afbf5b	20170092	Audit Operasional pendapatan	Audit Operasional Pendapatan PBB pada    Kecamatan Singojuruh	Assurance	18	201707	201708	7759500	Sedang	Sedang	t	\N	\N	\N
258ddcd9-7b42-4ea2-a1fc-36da7f34e16e	20170093	Audit Operasional pendapatan	Audit Operasional Pendapatan PBB pada    Kecamatan Wongsorejo	Assurance	18	201707	201708	7759500	Sedang	Sedang	t	\N	\N	\N
5241e414-cba0-4a62-845c-a9d6b7de7259	20170094	Audit Operasional pendapatan	Audit Operasional Pendapatan PBB pada    Kecamatan Genteng	Assurance	18	201707	201708	7759500	Sedang	Sedang	t	\N	\N	\N
bb52d44b-bcf8-4f9d-8419-e72b99d765fa	20170095	Audit Operasional pendapatan	Audit Operasional Pendapatan PBB pada    Kecamatan Kalipuro	Assurance	18	201707	201708	7759500	Sedang	Sedang	t	\N	\N	\N
af69f5eb-7d74-45a1-93b1-e5ae6db1a76d	20170096	Audit Operasional pendapatan	Audit Operasional Pendapatan PBB pada     Kecamatan Licin	Assurance	18	201707	201708	7759500	Sedang	Sedang	t	\N	\N	\N
a4a5b9cf-6141-47c6-a074-bf40d8fe374b	20170097	Audit Operasional pendapatan	Audit Operasional pendapatan pada Dinas Perindustrian dan Perdagangan  dengan ruang lingkup Audit Operasional Atas Penerimaan Asli Daerah Tahun 2016 dan tahun berjalan pada Pasar Daerah Kalibaru, Pasar Daerah Benculuk, Pasar Daerah Banyuwangi dan Pasar Daerah Pujasera.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
90739d88-aa3a-4e59-8cba-2bbde5df34ba	20170098	Audit Operasional pendapatan	Audit Operasional pendapatan pada Dinas Perindustrian dan Perdagangan . dengan ruang lingkup Audit Operasional Atas Penerimaan Asli Daerah Tahun 2016 dan tahun berjalan Pasar Hewan Rogojampi, Pasar Hewan Kebondalem, Pasar Hewan Genteng I dan Pasar Daerah Mojopanggung.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
9575e339-b5aa-439d-8a21-d3a124f9e010	20170099	Audit Operasional pendapatan	Audit Operasional pendapatan pada Dinas Perindustrian dan Perdagangan dengan ruang lingkup Audit Operasional Atas Penerimaan Asli Daerah Tahun 2016 dan tahun berjalan Pasar Daerah Rogojampi, Pasar Daerah  Glenmore, Pasar Hewan Glenmore dan Pasar Daerah  Segitiga Berlian.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
8e0c9696-dc33-439e-bc54-5d4ca7b9fdd3	20170100	Audit Operasional pendapatan	Audit Operasional pendapatan pada Dinas Kebudayaan dan Pariwisata dengan ruang lingkup Audit Operasional Atas Penerimaan Asli Daerah Tahun 2017 pada hotel dan restoran.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
ab272f54-d197-40fd-8ebb-754adba08578	20170101	Audit Operasional pendapatan	Audit Operasional pendapatan pada Dinas Kebudayaan dan Pariwisata dengan ruang lingkup Audit Operasional Atas Penerimaan Asli Daerah Tahun 2017 pada hotel dan restoran.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
94e3f03b-d367-4b83-8df4-b04e114d668a	20170102	Audit Operasional pendapatan	Audit Operasional pendapatan pada Dinas Perindustrian dan Perdagangan dengan ruang lingkup Audit Operasional Atas Penerimaan Asli Daerah Tahun 2016 dan tahun berjalan Pasar Daerah Genteng I, Pasar Daerah Genteng II, Pasar Daerah Muncar dan Pasar Daerah Dam Buntung.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
0c4fea48-632a-4124-9c94-c82bd0bad363	20170103	Audit Operasional pendapatan	Audit Operasional pendapatan pada Dinas Perindustrian dan Perdagangan dengan ruang lingkup Audit Operasional Atas Penerimaan Asli Daerah Tahun 2016 dan tahun berjalan Pasar Daerah Gendoh, Pasar Daerah Jajag/Gambiran, Pasar Daerah Blambangan dan Pasar Daerah Jatirejo.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
48bd93c5-dcad-4fff-bd20-f5a362d6509b	20170104	Audit Operasional pendapatan	Dinas Perindustrian dan Perdagangan dengan ruang lingkup Audit Operasional Atas Penerimaan Asli Daerah Tahun 2016 dan tahun berjalan Pasar Daerah Kebondalem, Pasar Daerah Srono, Pasar Daerah Sambirejo dan Pasar Daerah Songgon.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
405ba578-e58b-4a11-8e4c-ecca0fe4f6c0	20170105	Pendampingan	Pendampingan Validasidan tata kelola asset desa tahun 2017 pada Desa Blimbingsari dan Desa Gintangan Kec. Blimbingsari	Consulting	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
bd117de2-f765-486b-98e0-949f9918be8d	20170106	Pendampingan	Pendampingan Validasidan tata kelola asset desa tahun 2017 pada Desa Sukonatar Kecamatan Srono dan Desa Kedayunan Kec. Kabat	Consulting	18	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
f24d2eb3-ad1c-4d5b-abaa-a8370dd34a5a	20170107	Pendampingan	Pendampingan Validasidan tata kelola asset desa tahun 2017 pada Desa Mangir dan Desa Gladag Kec. Rogojampi	Consulting	18	201708	201709	7759500	Sedang	Sedang	t	\N	\N	\N
868e825b-ca48-4f8b-beeb-d2b6f9e20fed	20170108	Pendampingan	Pendampingan Validasidan tata kelola asset desa tahun 2017 pada Desa Lemahbangdewo  dan Desa Gumirih Kec. Singojuruh	Consulting	18	201708	201709	7759500	Sedang	Sedang	t	\N	\N	\N
f5bf0bc4-991e-488b-93a8-2770834fe409	20170109	Pendampingan	Pendampingan Validasidan tata kelola asset desa tahun 2017 pada Desa Tegaldlimo Kecamatan Tegaldlimo dan Desa Jambewangi Kec. Sempu	Consulting	18	201708	201709	7759500	Sedang	Sedang	t	\N	\N	\N
52d820df-97c5-43cb-a856-02f809899529	20170110	Pendampingan	Pendampingan Validasidan tata kelola asset desa tahun 2017 pada Desa Tembokrejo dan Desa Blambangan Kec. Muncar	Consulting	18	201708	201709	7759500	Sedang	Sedang	t	\N	\N	\N
d0673e08-7dac-488b-94d4-beb19c734851	20170111	Pendampingan	Pendampingan Validasidan tata kelola asset desa tahun 2017 pada Desa Wringinrejo dan Desa Purwodadi Kec. Gambiran	Consulting	18	201708	201709	7759500	Sedang	Sedang	t	\N	\N	\N
e543df46-7007-4126-9267-abdbffe70c87	20170112	Pendampingan	Pendampingan Validasidan tata kelola asset desa tahun 2017 pada Desa Glagah Agung dan Desa Karetan Kec. Purwoharjo	Consulting	18	201708	201709	7759500	Sedang	Sedang	t	\N	\N	\N
30eee8d2-8afe-4728-bbf9-7afe0b5dba3f	20170113	Reviu RKA	Reviu RKA SKPD dan RKA PPKD APBD Perubahan Tahun 2017 pada  Dinas Pendidikan, Dinas Pemuda dan Olahraga, Dinas Perumahan dan Kawasan Permukiman, Dinas Perpustakaan dan Kearsipan, Kecamatan Blimbingsari, Kecamatan Cluring dan Kecamatan Kalibaru.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
ca8367ef-3cf1-4f4e-a699-705b7a2f005f	20170114	Reviu RKA	Reviu RKA SKPD dan RKA PPKD APBD Perubahan Tahun 2017 pada Dinas Pertanian, Dinas Perindustrian dan Perdagangan, Badan Kesatuan Bangsa dan Politik, Dinas Komunikasi, Informatika dan Persandian, Kecamatan Kabat, Kecamatan Srono dan Kecamatan Siliragung.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
c8d65c2e-dc56-40ee-80ce-3aa389ce7b73	20170115	Reviu RKA	Reviu RKA SKPD dan RKA PPKD APBD Perubahan Tahun 2017 pada Dinas PU Pengairan, Sekretariat DPRD, Dinas Koperasi dan Usaha Mikro, Dinas Pemberdayaan Perempuan dan KB, Badan Penanggulangan Bencana Daerah, Kecamatan Rogojampi, Kecamatan Tegalsari dan Kecamatan Glenmore.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
c71a0771-3a39-49d9-a83b-ba1f4fd9eb9c	20170116	Reviu RKA	Reviu RKA SKPD dan RKA PPKD APBD Perubahan Tahun 2017 pada Dinas PU Cipta Karya dan Penataan Ruang, Bappeda, Satpol PP, RSUD Genteng, Kecamatan Glagah, Kecamatan Songgon dan Kecamatan Singojuruh.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
e2e1a3a0-b3b6-4585-b7a7-093585173418	20170117	Reviu RKA	Reviu RKA SKPD dan RKA PPKD APBD Perubahan Tahun 2017 pada Dinas Kesehatan, Dinas Kebudayaan dan Pariwisata, Dinas Sosial, Dinas Tenaga Kerja dan Transmigrasi, Kecamatan Wongsorejo, Kecamatan Sempu dan Kecamatan Tegaldlimo.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
65717591-9079-42a4-a32f-1a81cdc3c638	20170118	Reviu RKA	Reviu RKA SKPD dan RKA PPKD APBD Perubahan Tahun 2017 pada Dinas Perhubungan, BPKAD, Dinas Pemberdayaan Masyarakat dan Desa, Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu, Kecamatan Giri, Kecamatan Banyuwangi, Kecamatan Genteng dan Kecamatan Muncar.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
fc9d3fa1-6b69-4e82-9cf7-caf93dcd5dfd	20170119	Reviu RKA	Reviu RKA SKPD dan RKA PPKD APBD Perubahan Tahun 2017 pada Dinas Lingkungan Hidup, Dinas Perikanan dan Pangan, Dinas Kependudukan dan Pencatatan Sipil, RSUD Blambangan, Kecamatan Licin, Kecamatan Purwoharjo dan Kecamatan Bangorejo.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
274a905a-f771-41ad-8dfc-475ca3c0b2f2	20170120	Reviu RKA	Reviu RKA SKPD dan RKA PPKD APBD Perubahan Tahun 2017 pada Sekretariat Daerah, Badan Pendapatan daerah, BKD, Inspektorat, Kecamatan Kalipuro, Kecamatan Gambiran, dan  Kecamatan Pesanggaran.	Assurance	48	201708	201709	33276000	Sedang	Sedang	t	\N	\N	\N
1e7b1b7f-3dac-481f-ae0f-69735e8ab441	20170121	Evaluasi SAKIP	Evaluasi SAKIP pada Dinas Pendidikan, Dinas Perumahan dan Kawasan Permukiman, Kecamatan Tegaldlimo dan , Kecamatan Blimbingsari.	Assurance	36	201708	201709	24957000	Sedang	Sedang	t	\N	\N	\N
640bd05e-ba47-4231-8694-7c3d21f5d1c2	20170122	Evaluasi SAKIP	Evaluasi SAKIP pada Dinas Perindustrian dan Perdagang-an, Dinas Komunikasi, Infor-matika dan Persandian, Satpol PP.	Assurance	36	201708	201709	24957000	Sedang	Sedang	t	\N	\N	\N
2643caa1-3e22-4feb-aa9f-9148a416ee57	20170123	Evaluasi SAKIP	Evaluasi SAKIP pada Dinas Pemberdayaan Perempuan dan KB, Dinas PU Pengairan dan Dinas Koperasi dan Usaha Mikro.	Assurance	36	201708	201709	24957000	Sedang	Sedang	t	\N	\N	\N
be00e33e-2022-4847-9a86-7a371674ac2b	20170124	Evaluasi SAKIP	Evaluasi SAKIP pada Dinas PU Cipta Karya dan Penataan Ruang, Bappeda dan RSUD Genteng.	Assurance	36	201708	201709	24957000	Sedang	Sedang	t	\N	\N	\N
3d9c98a9-57f3-40a4-9568-d9d5690a2328	20170125	Evaluasi SAKIP	Evaluasi SAKIP pada Dinas Kebudayaan dan Pariwisata, Dinas Kesehatan, Kecamatan Wongsorejo dan Kecamatan Sempu.	Assurance	36	201708	201709	24957000	Sedang	Sedang	t	\N	\N	\N
88c87396-c02b-47d9-ba58-e5b87ad07939	20170126	Evaluasi SAKIP	Evaluasi SAKIP pada Dinas Perhubungan, BPKAD, Kecamatan Genteng dan Kecamatan Muncar.	Assurance	36	201708	201709	24957000	Sedang	Sedang	t	\N	\N	\N
c793a3b9-4bc9-4b1c-9b1b-520416850356	20170127	Evaluasi SAKIP	Evaluasi SAKIP pada Inspektorat, BKD dan Kecamatan Pesanggaran.	Assurance	36	201708	201709	24957000	Sedang	Sedang	t	\N	\N	\N
4e8d77b6-b32e-463f-a87f-8a1c212adf1c	20170128	Evaluasi SAKIP	Evaluasi SAKIP pada Dinas Perikanan dan Pangan, Badan Lingkungan Hidup dan RSUD Blambangan.	Assurance	36	201708	201709	24957000	Sedang	Sedang	t	\N	\N	\N
58db755a-7856-46bc-bc8b-68f52b480980	20170129	Pendampingan	Pembangunan Zona Integritas ( dalam rangka penilaian SKPD menuju Wilayah Bebas Korupsi dan Wilayah Birokrasi Bebas Melayani pada Badan Kepegawaian, Pendidikan dan Pelatihan Kab. Banyuwangi ).	Assurance	6	201708	201709	7759500	Sedang	Sedang	t	\N	\N	\N
501ab827-11b9-465f-9af3-64ae83603f62	20170130	Pendampingan	Pembangunan Zona Integritas ( dalam rangka penilaian SKPD menuju Wilayah Bebas Korupsi dan Wilayah Birokrasi Bebas Melayani pada RSUD Blambangan Kabupaten. Banyuwangi ).	Assurance	6	201708	201709	7759500	Sedang	Sedang	t	\N	\N	\N
410a614f-1da3-45e4-aae5-108192fb29ea	20170131	Pendampingan	Pembangunan Zona Integritas ( dalam rangka penilaian SKPD menuju Wilayah Bebas Korupsi dan Wilayah Birokrasi Bebas Melayani pada Dinas Kependudukan dan Pencatatan Sipil Kabupaten Banyuwangi ).	Assurance	6	201708	201709	7759500	Sedang	Sedang	t	\N	\N	\N
3dd947c4-9829-47a6-94d4-eb7ed638695b	20170132	Audit Operasional 	Audit Operasional Atas Pengelolaan Keuangan dan Aset TA. 2016 dan 2017 pada 3 SMP Negeri (1)	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
bb0d243b-5a3a-4fa8-ab6c-613b1c316218	20170133	Audit Operasional 	Audit Operasional Atas Pengelolaan Keuangan dan Aset TA. 2016 dan 2017 pada 3 SMP Negeri (2)	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
119afc90-7dac-4158-adbb-549e6b2ab0c8	20170134	Audit Operasional 	Audit Operasional Atas Pengelolaan Keuangan dan Aset TA. 2016 dan 2017 pada 3 SMP Negeri (3)	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
1410330d-ab77-43c8-ae25-976f63ef6525	20170135	Audit Operasional 	Audit Operasional Atas Pengelolaan Keuangan dan Aset TA. 2016 dan 2017 pada 3 SMP Negeri (4)	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
1043e161-abc3-4786-9e5d-fdaefbadeff1	20170136	Audit Operasional 	Audit Operasional Atas Pengelolaan Keuangan dan Aset TA. 2016 dan 2017 pada 3 SMP Negeri (5)	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
f0040b9d-dfc9-45e9-8e58-d34e79c02616	20170137	Audit Operasional 	Audit Operasional Atas Pengelolaan Keuangan dan Aset TA. 2016 dan 2017 pada 3 SMP Negeri (6)	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
51d0ae15-c1f4-4f1c-ac33-4a22b63f7d73	20170138	Audit Operasional 	Audit Operasional Atas Pengelolaan Keuangan dan Aset TA. 2016 dan 2017 pada 3 SMP Negeri (7)	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
750ead79-11f7-49f1-bb17-1204e94501db	20170139	Audit Operasional 	Audit Operasional Atas Pengelolaan Keuangan dan Aset TA. 2016 dan 2017 pada 3 SMP Negeri (8)	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
16a753d3-de02-4a39-b3bd-eec8f442d5b7	20170140	Audit Operasional 	SMP NEGERI 2 TEGALDLIMO	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
1fc14630-a52a-48f9-a81a-ad46ab7c0b2d	20170142	Audit Operasional 	UPTD PENDIDIKAN KECAMATAN BANYUWANGI	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
4d17d978-3df6-4bcd-8a72-889356fc2dae	20170143	Audit Operasional 	UPTD PENDIDIKAN KECAMATAN GLENMORE	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
7206eba3-461e-452a-9b5d-500ab2bec47f	20170144	Audit Operasional 	KECAMATAN TEGALSARI	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
1b8fda36-4713-4e34-9539-6c0f815d526b	20170146	Audit Operasional 	DINAS PERINDUSTRIAN, PERDAGANGAN DAN PERTAMBANGAN	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
5f1d19a2-cfde-48ed-8f6f-bfd82dad07a3	20170147	Audit Operasional 	BADAN PERENCANAAN PEMBANGUNAN DAERAH	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
7a3bf6a9-9bbe-4b84-a1f0-12fc3285e697	20170148	Monitoring	MONEV RAD PPK	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
a813fcfd-1f82-49d4-b9e6-93e85e4cb352	20170149	Monitoring	MONEV RAD PPK	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
ebc094dc-ab49-4b5b-b12f-0a06cfcd66ee	20170150	Monitoring	MONEV RAD PPK	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
0a61a3a5-59d5-4073-8f8e-915dd42fb131	20170151	Monitoring	MONEV RAD PPK	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
680554a8-51cc-415b-81f0-810fcfb709ac	20170152	Monitoring	MONEV RAD PPK	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
48625b85-9b85-4345-9144-d96515893d1d	20170153	Monitoring	MONEV RAD PPK	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
0421d57f-7b5a-4207-bb33-944535285c04	20170154	Monitoring	MONEV RAD PPK	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
cd4e228d-9ed7-4a47-9888-f9c42af13fd5	20170155	Monitoring	MONEV RAD PPK	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
fa1cf600-4815-456d-b58b-3b667f5cd67b	20170156	Pendampingan pelaporan	PENDAMPINGAN VALIDASI DAN PELAPORAN ASET DESA DI 2 DESA AB	Consulting	6	201711	201712	7759500	Sedang	Sedang	t	\N	\N	\N
f30b8d09-96a2-4972-b5eb-dc021523d576	20170145	Audit Operasional 	SMP NEGERI 2 MUNCAR	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
e79ff67f-dbe4-46b7-903f-8955a908ca7c	20170141	Audit Operasional 	UPTD PENDIDIKAN KECAMATAN SRONO	Assurance	48	201709	201710	33276000	Sedang	Sedang	t	\N	\N	\N
47c89990-f79d-4305-93fd-240d01d4d3a2	20170157	Pendampingan pelaporan	PENDAMPINGAN VALIDASI DAN PELAPORAN ASET DESA DI 2 DESA CD	Consulting	6	201711	201712	7759500	Sedang	Sedang	t	\N	\N	\N
1eefa564-898a-463a-975b-7674445f2ab4	20170158	Pendampingan pelaporan	PENDAMPINGAN VALIDASI DAN PELAPORAN ASET DESA DI 2 DESA EF	Consulting	6	201711	201712	7759500	Sedang	Sedang	t	\N	\N	\N
4b788550-a759-4da7-bf61-f7e410d14315	20170159	Pendampingan pelaporan	PENDAMPINGAN VALIDASI DAN PELAPORAN ASET DESA DI 2 DESA GH	Consulting	6	201711	201712	7759500	Sedang	Sedang	t	\N	\N	\N
e4058d28-203a-490b-abc3-ae4cfc8d73cc	20170160	Pendampingan pelaporan	PENDAMPINGAN VALIDASI DAN PELAPORAN ASET DESA DI 2 DESA IJ	Consulting	6	201711	201712	7759500	Sedang	Sedang	t	\N	\N	\N
d245bf7c-3d53-4da9-85d4-d869d9570c24	20170161	Pendampingan pelaporan	PENDAMPINGAN VALIDASI DAN PELAPORAN ASET DESA DI 2 DESA KL	Consulting	6	201711	201712	7759500	Sedang	Sedang	t	\N	\N	\N
c237e0e6-3e37-4a4a-b073-73e8812e77e7	20170162	Pendampingan pelaporan	PENDAMPINGAN VALIDASI DAN PELAPORAN ASET DESA DI 2 DESA MN	Consulting	6	201711	201712	7759500	Sedang	Sedang	t	\N	\N	\N
1271b32d-db21-4b40-8f9a-ce967a35e0ac	20170163	Pendampingan pelaporan	PENDAMPINGAN VALIDASI DAN PELAPORAN ASET DESA DI 2 DESA OP	Consulting	6	201711	201712	7759500	Sedang	Sedang	t	\N	\N	\N
7bad5bff-f505-4b20-b574-249132e5d116	20170164	Monitoring hasil PBJ	KEGIATAN MONEV PENGADAAN BARANG DAN JASA PEMERINTAH SKPD A	Assurance	48	201712	201712	33276000	Sedang	Sedang	t	\N	\N	\N
1cac3afd-2407-48dd-863b-09711dbb7575	20170165	Monitoring hasil PBJ	KEGIATAN MONEV PENGADAAN BARANG DAN JASA PEMERINTAH SKPD B	Assurance	48	201712	201712	33276000	Sedang	Sedang	t	\N	\N	\N
cf9dc3aa-d1aa-4a1a-ba66-fdda4c76eee6	20170166	Monitoring hasil PBJ	KEGIATAN MONEV PENGADAAN BARANG DAN JASA PEMERINTAH SKPD C	Assurance	48	201712	201712	33276000	Sedang	Sedang	t	\N	\N	\N
dc6dc348-297f-4c65-9a16-d3a64a8fe4ed	20170167	Monitoring hasil PBJ	KEGIATAN MONEV PENGADAAN BARANG DAN JASA PEMERINTAH SKPD D	Assurance	48	201712	201712	33276000	Sedang	Sedang	t	\N	\N	\N
5a7505ae-23e2-4048-a751-3ff7bfdc8606	20170168	Monitoring hasil PBJ	KEGIATAN MONEV PENGADAAN BARANG DAN JASA PEMERINTAH SKPD E	Assurance	48	201712	201712	33276000	Sedang	Sedang	t	\N	\N	\N
29c0bf6a-4831-4aea-a634-9745dbaa3a26	20170169	Monitoring hasil PBJ	KEGIATAN MONEV PENGADAAN BARANG DAN JASA PEMERINTAH SKPD F	Assurance	48	201712	201712	33276000	Sedang	Sedang	t	\N	\N	\N
cdca94d0-2297-41ee-a5eb-7522b679bd1d	20170170	Monitoring hasil PBJ	KEGIATAN MONEV PENGADAAN BARANG DAN JASA PEMERINTAH SKPD G	Assurance	48	201712	201712	33276000	Sedang	Sedang	t	\N	\N	\N
8118d9a2-069e-4987-8b4c-3c6bf9aca7c8	20170171	Monitoring hasil PBJ	KEGIATAN MONEV PENGADAAN BARANG DAN JASA PEMERINTAH SKPD H	Assurance	48	201712	201712	33276000	Sedang	Sedang	t	\N	\N	\N
caed4da2-ca46-4c41-9914-c8ec60604409	20170001	Audit ketaatan pertanggungjawaban keuangan	Dinas Pemuda dan Olahraga ( Pengelolaan Keuangan dan Asset TA. 2016 )	Assurance	48	201701	201702	33276000	Tinggi	Tinggi	t	\N	\N	\N
9a2bc8a7-347e-4506-becc-76478df24663	20180089	Test	test	test	0841	912381	123	12	12	hhe	t	\N	\N	\N
c43f2d40-0c29-4d0f-a6ff-8da1a10e8a82	201800002	testi	testi	testi	1	2	3	4	5	hh	t	b48074ba-9e37-4e4a-be7a-0d7edc4c69a8	f019a1a1-b1b2-472c-9f4e-7aadcb31e220	404b5b30-1273-4e2c-a4de-a8847cb7e5c7
ee358350-0710-4d77-9a63-bd0f55e511ca	20180000001	Audit Ketaatan	Audit Persediaan per 31 Desember 2017	Audit persediaan	16	201801	201802	10000000	Sedang	--	t	b48074ba-9e37-4e4a-be7a-0d7edc4c69a8	bf44df83-9bbf-452e-ab81-11bff7f66f32	e918fec9-e4a5-4903-8591-3e8b9e6f166a
\.


--
-- Data for Name: profile; Type: TABLE DATA; Schema: master; Owner: eaudit
--

COPY master.profile (id, nama, alamat, telp, status) FROM stdin;
fe251bf2-8d4f-4205-af0b-51160cd31e63	Mimi4	Jl Ayani 30	081335504918	t
ae3234c8-ea30-45d2-a48e-367b8c32567d	Marwoto, S.E	Jl Ayani 30	081929938110	t
\.


--
-- Data for Name: prosedur_audit; Type: TABLE DATA; Schema: master; Owner: eauditpr_user
--

COPY master.prosedur_audit (id, nama, status) FROM stdin;
ce8a1712-cacb-44fc-a152-6970afdf4d03	Test2	t
\.


--
-- Data for Name: resiko_satker; Type: TABLE DATA; Schema: master; Owner: eauditpr_user
--

COPY master.resiko_satker (id, periode, resiko, ket_resiko, jumlah_anggaran, nilai_aset, jumlah_pegawai, jumlah_unit, jumlah_jenis_keg, jumlah_pemeriksaan, nilai_sakip, nilai_spip, tertib_adm, jumlah_pengaduan, jumlah_jabatan, hasil_monitoring, id_satker, lp, lp2, lp3, lp4, lp5, tot_lp, km, km2, km3, km4, km5, tot_km, im, im2, im3, im4, im5, tot_im, ko, ko2, ko3, ko4, ko5, tot_ko, si, si2, si3, si4, si5, tot_si) FROM stdin;
0cd679c4-9149-45bf-a02c-d72949d33514	7ae49e60-c139-4744-820f-71939ed71eae	0		0	0	3	0	0	0	0	0	0	0	0	0	6e6d44de-7d44-4857-b01f-07f92644e9ef	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
42b602ac-5699-499e-aa21-36e64fcfb719	7ae49e60-c139-4744-820f-71939ed71eae	8		0	0	0	4	3	4	4	4	4	4	4	4	af36a328-a371-4095-b15e-a6c76143632c	1	1	1	1	0	4	0	1	0	0	0	1	0	0	0	0	1	1	0	0	0	0	1	1	1	0	0	0	0	1
\.


--
-- Data for Name: sasaran; Type: TABLE DATA; Schema: master; Owner: eaudit
--

COPY master.sasaran (id, kode, ket, "order", status) FROM stdin;
2ed85714-d1fe-444d-8f20-d821b58b8423	05	Persediaan	5	t
69063990-533f-4fd1-9a00-6fef0b273bb2	99	Sasaran lainnya	8	t
95988cfc-aefe-446d-918c-d8ff7828d779	07	Proses Kegiatan	7	t
b02fb5b6-a19e-4d2e-92c9-a83feeb2189c	06	Tugas pokok dan fungsi	6	t
bf44df83-9bbf-452e-ab81-11bff7f66f32	02	Kepegawaian	2	t
f019a1a1-b1b2-472c-9f4e-7aadcb31e220	03	Aset Daerah	3	t
fad97553-5c17-4caa-b8a3-01ff6ea39ff0	04	Pelaporan	4	t
a863e538-ff0c-4df6-a710-ae75058c750f	88	Dana BOS	9	t
6c87e264-ce63-46b4-8e70-ec3e312aa3b1	98	PKS	10	t
e6100dce-b40c-4381-9592-9fcdb90bdf05	08	Pengadaan Barang dan Jasa	11	t
a0f66b9c-6825-4299-89c7-6d4296125b1b	09	Pengelolaan Keuangan Desa	12	t
f0a961a7-6672-4ce1-b8aa-b9279b76bd21	10	Output Kegiatan	13	t
bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d	01	Keuangan	1	t
\.


--
-- Data for Name: satker; Type: TABLE DATA; Schema: master; Owner: eauditpr_user
--

COPY master.satker (id, kode, ket, "order", status, nama, alamat, email, resiko, ket_resiko, jumlah_anggaran, nilai_aset, jumlah_pegawai, jumlah_unit, jumlah_jenis_keg, jumlah_pemeriksaan, nilai_sakip, nilai_spip, tertib_adm, jumlah_pengaduan, jumlah_jabatan, hasil_monitoring) FROM stdin;
9839aa5d-3197-4ba3-9180-6713d08571b1	010101	DINAS PENDIDIKAN	1	t	DINAS PENDIDIKAN			5		4	3	3	2	2	2	0	0	3	0	0	0
6e6d44de-7d44-4857-b01f-07f92644e9ef	010203	BADAN KEPEGAWAIAN DAN DIKLAT	4	t	DINAS KESEHATAN - RSUD GENTENG			0		0	0	0	0	0	0	0	0	0	2	0	2
8adebfb8-1797-4fad-8a7a-b0be3d2f550b	020701	DINAS PEMBERDAYAAN MASYARAKAT DESA	15	t	DINAS PEMBERDAYAAN MASYARAKAT DESA			0		0	0	0	0	0	0	0	0	1	0	2	0
45c9cdfe-47e2-4a79-96da-7fcc10349c15	010202	BADAN PERENCANAAN DAN PEMBANGUNAN DAERAH KABUPATEN BANYUWANGI	3	t	DINAS KESEHATAN - RSUD BLAMBANGAN			0		0	1	2	0	0	0	0	0	0	0	0	0
780fdeac-a0a2-4757-bf4a-7d16e9b2e6c6	020501	DINAS LINGKUNGAN HIDUP	13	f	DINAS LINGKUNGAN HIDUP			14		1	0	1	0	0	0	0	0	0	0	0	0
af36a328-a371-4095-b15e-a6c76143632c	010601	DINAS SOSIAL	10	t	DINAS SOSIAL			6		0	0	0	2	0	0	0	0	0	0	0	0
6d341942-f365-43f0-a3a4-2ccaf7407854	010201	DINAS KESEHATAN	2	f	DINAS KESEHATAN			21		1	2	2	1	2	3	2	2	2	0	1	0
c621314f-841f-4f4d-800a-0cf9fed7c0c0	021301	DINAS PEMUDA DAN OLAH RAGA	20	t	DINAS PEMUDA DAN OLAH RAGA			0	\N	0	0	0	0	0	0	0	0	0	0	0	0
af81795c-9806-4a93-b154-9e5165d82680	010502	BADAN PENANGGULANGAN BENCANA	9	t	BADAN PENANGGULANGAN BENCANA			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
27663555-420e-43da-86bf-9263ad56e04a	119003	RSUD dr. Mohamad Saleh	208	t	CANTUK	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
697b7869-d11a-4dc3-81d8-eb3082f6db84	020601	DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL	14	t	DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL			3		0	0	0	0	0	0	1	2	0	0	1	1
b8b41722-4937-463e-9c5c-fbc48a86f902	119004	Dinas Koperasi, Usaha Mikro, Perdagangan dan Perindustrian	209	t	GAMBOR	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
531190ff-b4bd-446b-8a04-dc75903b0b84	117001	Dinas Kebudayaan dan Pariwisata	194	t	GENDOH	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
e3894c7d-074b-4bff-b251-4722b18517eb	125005	Dinas Pertanian dan Ketahanan Pangan	266	t	BADEAN WONGSOREJO	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
7ef08998-fb13-4507-8979-077b3e4b330e	121001	Dinas Perikanan	225	t	BAGOREJO	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
e71d87c9-d374-4e09-aeae-2adb7c4e2c14	124003	Dinas Perumahan dan Kawasan Permukiman	252	t	BAJULMATI	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
1ff84adc-4d4c-4a7c-8bb4-b95ec5013387	123001	Dinas Penanaman Modal dan PTSP	244	t	DASRI	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
7025ffa6-b232-4ef6-862b-50f43cc8a952	124006	Kecamatan Kanigaran	255	t	BUMIREJO	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
b10e79aa-5841-4451-96c8-27a10776ced7	115001	Bagian Pemerintahan	176	t	BULUREJO	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
1eb53585-90ec-49c8-ac00-3b579a52711d	118002	Bagian Hukum	202	t	BULUAGUNG	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
9ac50b73-924e-4870-ad76-4c4331e55fa6	116002	Bagian Kesejahteraan Rakyat	185	t	BUBUK	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
5fb9f651-065d-4257-b231-d6e561443937	125002	Bagian Administrasi Pembangunan	263	t	BOMO	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
203ea79b-ee80-4534-bd1f-6e82e03afe04	125001	Bagian Umum	262	t	BLIMBINGSARI	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
b09a303e-e019-407c-bdc7-b393c93695ec	124005	Bagian Humas dan Protokol	254	t	BENGKAK	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
1a7cde58-a951-4cf4-9836-9e585b7ddce3	119002	Satuan Polisi Pamong Praja	207	t	BENELAN KIDUL	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
0296e63d-811f-42e8-aa03-feafaa36d547	021201	DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU	19	t	DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
bc314ee6-ed38-4334-b112-9d777eb07023	030701	DINAS PERINDUSTRIAN DAN PERDAGANGAN	25	t	DINAS PERINDUSTRIAN DAN PERDAGANGAN			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
febc01a2-c64f-4017-bafb-73f1ad78bc0e	021101	DINAS KOPERASI DAN USAHA MIKRO	18	t	DINAS KOPERASI DAN USAHA MIKRO			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8308dded-c5af-4a35-83be-cb265f317d89	021001	DINAS KOMUNIKASI, INFORMATIKA DAN PERSANDIAN	17	t	DINAS KOMUNIKASI, INFORMATIKA DAN PERSANDIAN			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
c21367d6-be25-484f-b766-d998ccb95cf7	020901	DINAS PERHUBUNGAN	16	t	DINAS PERHUBUNGAN			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
70bc7a06-9b14-42c6-aaa3-f4c9e13a7303	030301	DINAS PERTANIAN	24	t	DINAS PERTANIAN			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
a2f700a6-7343-41cd-b70f-122ee937b9f4	020201	DINAS PEMBERDAYAAN PEREMPUAN DAN KB	12	t	DINAS PEMBERDAYAAN PEREMPUAN DAN KB			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
d77a287a-67d7-4fe1-805f-6eabbbf4d578	020101	DINAS TENAGA KERJA DAN TRANSMIGRASI	11	t	DINAS TENAGA KERJA DAN TRANSMIGRASI			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
56d52b87-5bf4-4efb-a5fa-4262c228399f	030101	DINAS PERIKANAN DAN PANGAN	23	t	DINAS PERIKANAN DAN PANGAN			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
7e0d6bca-09b5-4607-b2a6-883294baf980	021701	DINAS PERPUSTAKAAN DAN KEARSIPAN	22	t	DINAS PERPUSTAKAAN DAN KEARSIPAN			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
784778f2-a8ed-470c-bb97-e89e4a1e9269	021601	DINAS KEBUDAYAAN DAN PARIWISATA	21	t	DINAS KEBUDAYAAN DAN PARIWISATA			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
bc4daebb-72d7-4519-8025-bd228688e172	010401	DINAS PERUMAHAN DAN KAWASAN PERMUKIMAN	7	t	DINAS PERUMAHAN DAN KAWASAN PERMUKIMAN			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
5c846f8d-4bc3-4b6c-9a29-b30b9e61d1e5	010302	DINAS PU PENGAIRAN	6	t	DINAS PU PENGAIRAN			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
2ecc8748-a413-48f8-bb13-e82a4a38b7e6	010301	DINAS PU CIPTAKARYA DAN PENATAAN RUANG	5	t	DINAS PU CIPTAKARYA DAN PENATAAN RUANG			0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
7a87940d-dabf-4ca4-a572-f0c6ba5f5987	050501	SEKRETARIAT DAERAH	55	t	SEKRETARIAT DAERAH	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
ed8775b3-28c9-478d-a4f0-da776b4fd774	040201	BADAN KESATUAN BANGSA DAN POLITIK	51	t	BADAN KESATUAN BANGSA DAN POLITIK	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
f62eaaba-0f3e-4ba4-a3d0-4649af4e864c	050201	BADAN PENGELOLAAN KEUANGAN DAN ASET DAERAH	52	t	BADAN PENGELOLAAN KEUANGAN DAN ASET DAERAH	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
c48c3481-9450-4826-a9bc-ca1d26cba35c	050202	BADAN PENDAPATAN DAERAH	53	t	BADAN PENDAPATAN DAERAH	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
232cf4b2-a9c1-4221-bf08-6008c962654c	050301	BADAN KEPEGAWAIAN, PENDIDIKAN DAN PELATIHAN	54	t	BADAN KEPEGAWAIAN, PENDIDIKAN DAN PELATIHAN	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
29aac501-822e-45bd-9c3e-355e759475f8	050502	SEKRETARIAT DPRD	56	t	SEKRETARIAT DPRD	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
90447f06-371e-49fe-a54e-bb3c5035dc25	050503	INSPEKTORAT	57	t	INSPEKTORAT	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
9e34bdc8-cbf5-46d7-bfde-eb342091da9b	050505	KEPALA DAERAH DAN WAKIL KEPALA DAERAH	59	t	KEPALA DAERAH DAN WAKIL KEPALA DAERAH	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
5540ae2c-ccac-427e-a3f2-2758ea6c6533	103002	Dinas Pemberdayaan Perempuan, Perlindungan Anak dan KB	79	t	CLURING	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
4009856d-cb1a-4f8d-816a-30fd34e22ef7	109001	Badan Pendapatan, Pengelolaan Keuangan dan Aset Daerah	122	t	BADEAN	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
b2702994-e63a-43dc-b9a2-8c4934f90587	112002	Badan Perencanaan Pembangunan Daerah dan Litbang	154	t	GUMUK	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
2646383e-3881-4de6-af27-ca1d3765379a	109005	Badan Kepegawaian dan Pengembangan SDM	126	t	DADAPAN	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
b2656636-2458-4e48-940e-73dd3570bfa4	109004	Kecamatan Mayangan	125	t	BUNDER	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
29c68f1c-173b-4d02-b7ba-0e0323f11e06	108001	Kecamatan Kedopok	115	t	BUMIHARJO	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
7ff6ea58-a7d8-4ebd-89dc-1829a38d6fb2	111002	Kecamatan Wonoasih	145	t	BULUSAN	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
70638430-6c9d-42e8-8623-e9160a19a5fc	106003	Bagian Administrasi Perekonomian	101	t	BOYOLANGU	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
e6418558-c3c3-47bb-9fe2-ac1e3c062433	113001	Bagian Organisasi	161	t	BLAMBANGAN	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
f8d56fdd-af90-4bfd-9f38-2e54b3252859	109003	Badan Penanggulangan Bencana Daerah	124	t	BENELAN LOR	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
c7a3f5a9-b663-46a2-ac58-4469b5161a65	050504	DEWAN PERWAKILAN RAKYAT DAERAH	58	t	DEWAN PERWAKILAN RAKYAT DAERAH	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
5fa647f2-1fbf-4143-9f85-a466774cd571	104001	Dinas Pekerjaan Umum dan Penataan Ruang	87	t	GAMBIRAN	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
204414e1-3309-4f8f-952a-2244a5bf1169	111003	Kecamatan Kademangan	146	t	BULUSARI	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: tahapan_audit; Type: TABLE DATA; Schema: master; Owner: eaudit
--

COPY master.tahapan_audit (kode, tahapan) FROM stdin;
1	Pendahuluan
2	Lanjutan
3	Penyelesaian
\.


--
-- Data for Name: tindak_lanjut; Type: TABLE DATA; Schema: master; Owner: eauditpr_user
--

COPY master.tindak_lanjut (id, nama, kode, status) FROM stdin;
3ae6a4bc-73df-415b-b3be-48d9a80fbf01	Memberikan peringatan kepada bendahara1	3.0.0.12	t
\.


--
-- Data for Name: tipe_resiko; Type: TABLE DATA; Schema: master; Owner: eauditpr_user
--

COPY master.tipe_resiko (id, nama, status) FROM stdin;
5966c178-dfb8-4433-be4f-ba473231059d	Resiko Lokal	t
\.


--
-- Data for Name: tujuan; Type: TABLE DATA; Schema: master; Owner: eaudit
--

COPY master.tujuan (id, kode, ket, "order", status) FROM stdin;
404b5b30-1273-4e2c-a4de-a8847cb7e5c7	04	Menilai keekonomisan kegiatan	4	t
43590641-a16b-4dc7-954b-85ca90dd87e3	99	Tujuan Penugasan Lainnya	12	t
52b32848-48ef-4c83-86b6-67810dd696af	11	Melakukan klarifikasi atas suatu masalah	11	t
602fa656-5eb7-42eb-b33d-27ae2bac110a	03	Menilai efisiensi kegiatan	3	t
7b46be23-f857-4cfb-bb52-1834f8bf72b9	05	Menilai tertib administrasi keuangan	5	t
7e5efe38-4cdf-4e04-8dd2-36d0f4f66979	09	Melakukan pendampingan atas kegiatan	9	t
91f23bee-0b30-4ea2-9841-b4ddff6a979e	07	Menilai capaian tujuan	7	t
999342da-ac0f-496e-ae6c-bc00cef49c2b	08	Meyakini kesesuaian angka	8	t
c869623b-f11a-45bd-8bc7-d462e62648fd	10	Menghitung kerugian negara/daerah	10	t
fc6ced79-8c2c-4a5d-8b8c-aa75b125b31f	06	Menilai kinerja satuan kerja	6	t
00c36151-82df-4682-9520-2b9aa16a69d6	12	Menilai ketepatan waktu penyelesaian laporan/kegiatan	13	t
28127248-496a-4150-ad5b-48a1c9ec56bc	21	Menilai operasional pendapatan	15	t
ec11c465-c966-4bb3-baa8-0c38d20ff872	31	Pemenuhan kewajiban perpajakan	16	t
62117d42-9252-46ef-9eb9-f20d40012f3f	98	Meningkatkan kemampuan auditor dalam melaksanakan penugasan	17	t
c1db9e90-f731-4a62-9fb8-4fd07fa0f844	13	Monitoring dan evaluasi capaian target	18	t
ae34b855-ce4f-472b-8672-15e28682166c	14	Menilai Pengamanan Aset	19	t
d5735834-9b22-45c1-86cf-4f2909bc110d	15	Administrasi Aset Desa	20	t
82d51b4d-157a-4d69-a814-fa15c3f98b41	00	Pengumpulan data Umum	14	t
e918fec9-e4a5-4903-8591-3e8b9e6f166a	01	Menilai ketaatan pada aturan	1	t
744b5300-4427-439a-a729-ab70a6a908a1	02	Menilai efektifitas kegiatan	2	t
\.


--
-- Data for Name: menu; Type: TABLE DATA; Schema: public; Owner: eaudit
--

COPY public.menu (id, name, parent, level, report) FROM stdin;
1541696d-a165-4a06-9181-88472bd61293	Aturan	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
2a5a511b-7bd4-424f-8b38-252f6115640f	PKPT	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
2f4e5764-01c0-49f5-b178-555f814a015a	TAO	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
36fbf7b0-d4ba-49d1-af67-1e0af4a05766	Tim	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
50492ec8-26b3-4178-9d66-501ccef6ef8b	Jenis Audit	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
6ea5af6e-aeb0-4bde-9e9b-8dd58f322c75	Sasaran Audit	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
74fa394a-0fee-4b9d-827c-91a09f5c68f5	Rencana Audit	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	2	\N
7a2eee70-8883-4c74-9a9b-e820f2249b4b	Pedoman	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
8a1dfd71-6c85-412f-91de-970ca913dc60	Satuan Kerja	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
9312a5c1-7991-4eb1-be6d-05fef767935b	Tujuan Audit	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
acda06a9-1a27-45f0-bceb-1b77c81b8749	Jabatan	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
b4be4621-3e9e-4e3d-a47b-df549ae100f4	Wilayah	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
b7d62610-7f21-43e4-a95b-43f1856411b4	Persetujuan Pelaksanaan	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	2	\N
ca530176-384e-4c7d-8377-1171ec3a8d05	Pelaksanaan Audit	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	2	\N
d66ab356-6792-451d-8f42-84f07e48b9e7	User	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
da53f223-2170-4c58-b561-aa1af6721541	Tema & Tindak Lanjut	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	2	\N
e3033e60-42d8-47fd-8c8a-6e51cebaba33	Persetujuan Rencana	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	2	\N
e89cdedb-32f8-47fd-bbfc-960c894dae01	Golongan Audit	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
e964618e-29bd-4e17-91f2-1f42c0fa0488	Laporan	\N	1	\N
ec657ac4-bfb1-453c-b3c0-e409f3ed27b9	Rencana Audit	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
f10293a6-79c7-41e2-840e-a597e5ace426	Master	\N	1	\N
f577d9b2-3ac6-43af-8c55-81f0652ffd72	Program Audit	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	Internal Proses	\N	1	\N
fe012b79-adf5-42f4-bdbd-5ffd5417289c	Pelaporan Hasil Audit	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	2	\N
a40138cb-39c5-4625-9203-0de3ba0a9df1	Persetujuan Pelaporan Hasil Audit	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	2	\N
5fc41cc2-2391-4ba9-a1d5-d04d0aeef216	KM3	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	t
388deab8-36ff-4a8b-a1d7-b88eab8b4c26	Program Satker	923143fd-ca98-489d-88d7-642d2001b6ef	2	\N
d54451d1-3a31-442a-8279-1cdc0ed2b221	KM8	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	t
340671b5-d101-46ec-b86f-6dc06e9d5668	Tipe Resiko	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
bfa6447a-7dc4-4495-9397-db2e5865ade1	Kendali	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
e741e54c-9c5f-454e-a2f9-a38b8806091b	Monitoring Penugasan	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	\N
304c528b-aa08-466d-a766-45d3c102d733	Tracking Proses	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	t
c43ac70d-a037-4148-827d-d5c0a8b00b97	Penyebab	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
97c15763-d7b1-4846-9fc9-f5a36dfa5468	Monitoring LHP	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	\N
5281099c-88c4-417d-9e0d-285fcc8ffc41	Cetak SPT	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	t
17c15763-d7b1-4846-9fc9-f5a36dfa5468	Dashboard Pimpinan	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	\N
92559c74-927e-4edf-85c8-203447141840	Data Role	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
93391995-8d3d-447c-a50f-1cec916d3c27	Laporan Kejadian	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	\N
c6fabac7-e2b3-4245-a69a-84992efa4bc1	Kode Temuan & Kode Rekomendasi	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
b0014449-4706-48e1-b81d-d758057ef0d0	Periode	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
c34d2968-b466-4482-9803-d85a69a73d06	Tindak Lanjut	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	2	\N
e36e779a-e2aa-468e-a11c-972f75c1c052	Kamus Tindak Lanjut	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
e7dadcd0-c8ce-41e1-af4b-4fdc566a8868	Temuan TL	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	t
b280071d-87e7-44be-99f3-64cd75883e63	KM4	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	t
82f810bf-86ce-4c88-927e-5e9a4ea82587	Monitoring SPT	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	f
59b034e9-a1ed-4267-afba-606ead1b9500	File Manager	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	\N
08f6f5e6-0933-48e2-88ce-ce52f2709040	Rekap TL	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	f
292e5ff3-1095-49b3-aafd-565794ee545e	Config SPT	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
23ca0398-00b3-4547-9312-63633b0e4a09	KM9	e964618e-29bd-4e17-91f2-1f42c0fa0488	2	t
72e9e5f5-9b6c-4213-9c23-85588627d997	Prosedur Audit	f10293a6-79c7-41e2-840e-a597e5ace426	2	\N
923143fd-ca98-489d-88d7-642d2001b6ef	Manajemen Resiko	\N	1	\N
cb7ac7ac-9d00-4acd-a1fd-2afefb0c8a81	Kejadian Satker	923143fd-ca98-489d-88d7-642d2001b6ef	2	\N
0d59e730-4d54-4ef2-a892-2bdb88f4e732	Kegiatan Satker	923143fd-ca98-489d-88d7-642d2001b6ef	2	\N
\.


--
-- Data for Name: periode; Type: TABLE DATA; Schema: public; Owner: eauditpr_user
--

COPY public.periode (id, nama, start, "end", status) FROM stdin;
7ae49e60-c139-4744-820f-71939ed71eae	2018	2018-01-01	2018-10-18	t
0be2e5bf-1089-4332-bfc0-b6b34009be60	2017	2017-01-01	2017-12-31	f
4e4a0015-c2e4-496f-91b2-71f167a33c70	2016	2016-01-01	2016-12-31	f
422db048-d18e-4c71-9cc6-ac23a9896110	2015	2015-01-01	2015-12-31	f
\.


--
-- Data for Name: periode_holiday; Type: TABLE DATA; Schema: public; Owner: eauditpr_user
--

COPY public.periode_holiday (id, holiday, ket) FROM stdin;
7ae49e60-c139-4744-820f-71939ed71eae	2018-01-06	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-01-07	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-01-13	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-01-14	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-01-20	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-01-21	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-01-27	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-01-28	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-02-03	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-02-04	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-02-10	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-02-11	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-02-17	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-02-18	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-02-24	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-02-25	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-03-03	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-03-04	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-03-10	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-03-11	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-03-17	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-03-18	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-03-24	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-03-25	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-03-31	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-04-01	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-04-07	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-04-08	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-04-14	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-04-15	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-04-21	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-04-22	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-04-28	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-04-29	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-05-05	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-05-06	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-05-12	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-05-13	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-05-19	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-05-20	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-05-26	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-05-27	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-06-02	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-06-03	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-06-09	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-06-10	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-06-16	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-06-17	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-06-23	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-06-24	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-06-30	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-07-01	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-07-07	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-07-08	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-07-14	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-07-15	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-07-21	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-07-22	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-07-28	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-07-29	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-08-04	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-08-05	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-08-11	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-08-12	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-08-18	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-08-19	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-08-25	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-08-26	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-09-01	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-09-02	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-09-08	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-09-09	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-09-15	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-09-16	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-09-22	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-09-23	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-09-29	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-09-30	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-10-06	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-10-07	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-10-13	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-10-14	Libur Sabtu - Minggu
7ae49e60-c139-4744-820f-71939ed71eae	2018-02-01	hari nasional
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-01-06	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-01-07	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-01-13	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-01-14	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-01-20	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-01-21	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-01-27	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-01-28	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-02-03	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-02-04	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-02-10	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-02-11	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-02-17	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-02-18	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-02-24	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-02-25	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-03-03	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-03-04	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-03-10	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-03-11	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-03-17	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-03-18	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-03-24	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-03-25	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-03-31	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-04-01	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-04-07	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-04-08	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-04-14	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-04-15	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-04-21	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-04-22	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-04-28	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-04-29	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-05-05	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-05-06	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-05-12	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-05-13	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-05-19	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-05-20	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-05-26	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-05-27	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-06-02	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-06-03	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-06-09	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-06-10	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-06-16	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-06-17	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-06-23	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-06-24	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-06-30	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-07-01	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-07-07	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-07-08	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-07-14	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-07-15	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-07-21	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-07-22	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-07-28	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-07-29	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-08-04	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-08-05	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-08-11	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-08-12	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-08-18	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-08-19	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-08-25	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-08-26	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-09-01	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-09-02	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-09-08	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-09-09	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-09-15	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-09-16	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-09-22	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-09-23	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-09-29	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-09-30	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-10-06	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-10-07	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-10-13	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-10-14	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-10-20	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-10-21	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-10-27	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-10-28	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-11-03	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-11-04	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-11-10	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-11-11	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-11-17	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-11-18	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-11-24	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-11-25	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-12-01	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-12-02	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-12-08	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-12-09	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-12-15	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-12-16	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-12-22	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-12-23	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-12-29	Libur Sabtu - Minggu
0be2e5bf-1089-4332-bfc0-b6b34009be60	2018-12-30	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-01-06	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-01-07	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-01-13	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-01-14	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-01-20	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-01-21	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-01-27	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-01-28	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-02-03	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-02-04	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-02-10	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-02-11	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-02-17	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-02-18	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-02-24	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-02-25	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-03-03	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-03-04	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-03-10	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-03-11	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-03-17	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-03-18	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-03-24	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-03-25	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-03-31	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-04-01	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-04-07	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-04-08	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-04-14	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-04-15	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-04-21	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-04-22	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-04-28	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-04-29	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-05-05	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-05-06	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-05-12	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-05-13	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-05-19	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-05-20	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-05-26	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-05-27	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-06-02	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-06-03	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-06-09	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-06-10	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-06-16	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-06-17	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-06-23	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-06-24	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-06-30	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-07-01	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-07-07	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-07-08	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-07-14	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-07-15	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-07-21	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-07-22	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-07-28	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-07-29	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-08-04	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-08-05	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-08-11	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-08-12	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-08-18	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-08-19	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-08-25	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-08-26	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-09-01	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-09-02	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-09-08	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-09-09	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-09-15	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-09-16	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-09-22	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-09-23	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-09-29	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-09-30	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-10-06	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-10-07	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-10-13	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-10-14	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-10-20	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-10-21	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-10-27	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-10-28	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-11-03	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-11-04	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-11-10	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-11-11	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-11-17	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-11-18	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-11-24	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-11-25	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-12-01	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-12-02	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-12-08	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-12-09	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-12-15	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-12-16	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-12-22	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-12-23	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-12-29	Libur Sabtu - Minggu
4e4a0015-c2e4-496f-91b2-71f167a33c70	2018-12-30	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-01-03	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-01-04	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-01-10	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-01-11	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-01-17	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-01-18	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-01-24	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-01-25	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-01-31	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-02-01	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-02-07	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-02-08	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-02-14	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-02-15	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-02-21	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-02-22	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-02-28	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-03-01	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-03-07	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-03-08	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-03-14	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-03-15	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-03-21	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-03-22	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-03-28	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-03-29	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-04-04	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-04-05	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-04-11	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-04-12	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-04-18	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-04-19	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-04-25	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-04-26	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-05-02	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-05-03	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-05-09	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-05-10	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-05-16	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-05-17	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-05-23	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-05-24	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-05-30	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-05-31	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-06-06	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-06-07	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-06-13	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-06-14	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-06-20	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-06-21	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-06-27	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-06-28	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-07-04	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-07-05	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-07-11	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-07-12	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-07-18	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-07-19	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-07-25	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-07-26	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-08-01	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-08-02	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-08-08	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-08-09	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-08-15	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-08-16	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-08-22	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-08-23	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-08-29	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-08-30	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-09-05	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-09-06	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-09-12	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-09-13	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-09-19	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-09-20	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-09-26	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-09-27	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-10-03	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-10-04	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-10-10	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-10-11	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-10-17	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-10-18	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-10-24	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-10-25	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-10-31	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-11-01	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-11-07	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-11-08	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-11-14	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-11-15	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-11-21	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-11-22	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-11-28	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-11-29	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-12-05	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-12-06	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-12-12	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-12-13	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-12-19	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-12-20	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-12-26	Libur Sabtu - Minggu
422db048-d18e-4c71-9cc6-ac23a9896110	2015-12-27	Libur Sabtu - Minggu
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: eaudit
--

COPY public.role (id, name, status) FROM stdin;
108ce450-d740-4bcb-9aaa-be9504bf94d8	Pengendali Teknis	t
2332800e-edb0-49cd-92d7-1abc3d981207	Inspektur Pembantu	t
52e68b85-90c1-4910-b9b0-df9a3b3574c4	Administrator	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	Inspektur	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	Super Administrator	t
97cc7404-490c-4827-beba-425c85acef5d	Ketua	t
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	Anggota	t
\.


--
-- Data for Name: rolemenu; Type: TABLE DATA; Schema: public; Owner: eaudit
--

COPY public.rolemenu (role, menu, c, r, u, d) FROM stdin;
108ce450-d740-4bcb-9aaa-be9504bf94d8	102f762a-5afe-44ba-9715-1ee21b5dd5f6	f	f	\N	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	1541696d-a165-4a06-9181-88472bd61293	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	17eebe14-ed40-4a30-a689-c141c2d149fc	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	2f4e5764-01c0-49f5-b178-555f814a015a	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	2f6c338e-1d6a-467c-a8a1-5db563bb66c8	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	30e5a675-aac0-40f0-b139-721fc6affd7c	t	\N	\N	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	36fbf7b0-d4ba-49d1-af67-1e0af4a05766	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	3d4ca5e7-8f17-4537-8d2a-4b0f02c3edf7	\N	t	\N	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	4938d59a-801c-44b9-adea-20ebdc9e6343	t	t	\N	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	50492ec8-26b3-4178-9d66-501ccef6ef8b	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	50eba0ee-9760-4355-80c7-27e2f970bfc0	\N	f	\N	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	5fc41cc2-2391-4ba9-a1d5-d04d0aeef216	t	t	\N	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	6ea5af6e-aeb0-4bde-9e9b-8dd58f322c75	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	7a2eee70-8883-4c74-9a9b-e820f2249b4b	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	92559c74-927e-4edf-85c8-203447141840	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	946653d4-231f-4805-9c7a-5a104bcd46aa	\N	t	\N	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	9be5fdc8-3b57-4822-8840-3e2a9944385c	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	acda06a9-1a27-45f0-bceb-1b77c81b8749	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	b280071d-87e7-44be-99f3-64cd75883e63	t	t	\N	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	b4be4621-3e9e-4e3d-a47b-df549ae100f4	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	b7d62610-7f21-43e4-a95b-43f1856411b4	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	d54451d1-3a31-442a-8279-1cdc0ed2b221	t	t	f	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	d669b3d2-f9e1-4ad6-97b5-403cf7da0fdd	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	e3033e60-42d8-47fd-8c8a-6e51cebaba33	t	t	t	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	e964618e-29bd-4e17-91f2-1f42c0fa0488	\N	t	\N	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	f10293a6-79c7-41e2-840e-a597e5ace426	\N	t	\N	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	f577d9b2-3ac6-43af-8c55-81f0652ffd72	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	fb2045aa-bcc6-4494-9760-7f2f28ff6cf6	t	t	t	f
108ce450-d740-4bcb-9aaa-be9504bf94d8	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	\N	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	1541696d-a165-4a06-9181-88472bd61293	\N	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	2f4e5764-01c0-49f5-b178-555f814a015a	\N	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	36fbf7b0-d4ba-49d1-af67-1e0af4a05766	\N	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	3d4ca5e7-8f17-4537-8d2a-4b0f02c3edf7	\N	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	5fc41cc2-2391-4ba9-a1d5-d04d0aeef216	t	t	t	t
2332800e-edb0-49cd-92d7-1abc3d981207	74fa394a-0fee-4b9d-827c-91a09f5c68f5	t	t	t	t
2332800e-edb0-49cd-92d7-1abc3d981207	7a2eee70-8883-4c74-9a9b-e820f2249b4b	\N	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	9be5fdc8-3b57-4822-8840-3e2a9944385c	\N	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	b280071d-87e7-44be-99f3-64cd75883e63	t	t	t	t
2332800e-edb0-49cd-92d7-1abc3d981207	b7d62610-7f21-43e4-a95b-43f1856411b4	t	t	t	t
2332800e-edb0-49cd-92d7-1abc3d981207	ca530176-384e-4c7d-8377-1171ec3a8d05	t	t	t	t
2332800e-edb0-49cd-92d7-1abc3d981207	e3033e60-42d8-47fd-8c8a-6e51cebaba33	t	t	t	t
2332800e-edb0-49cd-92d7-1abc3d981207	f577d9b2-3ac6-43af-8c55-81f0652ffd72	\N	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	\N	t	\N	\N
52e68b85-90c1-4910-b9b0-df9a3b3574c4	30e5a675-aac0-40f0-b139-721fc6affd7c	\N	t	t	t
52e68b85-90c1-4910-b9b0-df9a3b3574c4	4938d59a-801c-44b9-adea-20ebdc9e6343	f	\N	t	t
52e68b85-90c1-4910-b9b0-df9a3b3574c4	50492ec8-26b3-4178-9d66-501ccef6ef8b	f	t	t	t
52e68b85-90c1-4910-b9b0-df9a3b3574c4	5fc41cc2-2391-4ba9-a1d5-d04d0aeef216	\N	\N	t	t
52e68b85-90c1-4910-b9b0-df9a3b3574c4	6ea5af6e-aeb0-4bde-9e9b-8dd58f322c75	f	t	t	f
52e68b85-90c1-4910-b9b0-df9a3b3574c4	74fa394a-0fee-4b9d-827c-91a09f5c68f5	f	\N	t	t
52e68b85-90c1-4910-b9b0-df9a3b3574c4	8a1dfd71-6c85-412f-91de-970ca913dc60	\N	t	t	t
52e68b85-90c1-4910-b9b0-df9a3b3574c4	acda06a9-1a27-45f0-bceb-1b77c81b8749	f	t	f	t
52e68b85-90c1-4910-b9b0-df9a3b3574c4	b280071d-87e7-44be-99f3-64cd75883e63	\N	\N	t	t
52e68b85-90c1-4910-b9b0-df9a3b3574c4	ca530176-384e-4c7d-8377-1171ec3a8d05	f	f	t	t
52e68b85-90c1-4910-b9b0-df9a3b3574c4	d669b3d2-f9e1-4ad6-97b5-403cf7da0fdd	f	f	t	t
52e68b85-90c1-4910-b9b0-df9a3b3574c4	d66ab356-6792-451d-8f42-84f07e48b9e7	t	t	t	\N
52e68b85-90c1-4910-b9b0-df9a3b3574c4	da53f223-2170-4c58-b561-aa1af6721541	t	t	t	t
52e68b85-90c1-4910-b9b0-df9a3b3574c4	e964618e-29bd-4e17-91f2-1f42c0fa0488	\N	t	\N	\N
52e68b85-90c1-4910-b9b0-df9a3b3574c4	ec657ac4-bfb1-453c-b3c0-e409f3ed27b9	\N	f	f	\N
52e68b85-90c1-4910-b9b0-df9a3b3574c4	f10293a6-79c7-41e2-840e-a597e5ace426	\N	t	\N	\N
52e68b85-90c1-4910-b9b0-df9a3b3574c4	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	\N	t	\N	\N
52e68b85-90c1-4910-b9b0-df9a3b3574c4	fe012b79-adf5-42f4-bdbd-5ffd5417289c	f	f	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	2f4e5764-01c0-49f5-b178-555f814a015a	t	t	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	36fbf7b0-d4ba-49d1-af67-1e0af4a05766	\N	f	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	3d4ca5e7-8f17-4537-8d2a-4b0f02c3edf7	\N	t	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	50492ec8-26b3-4178-9d66-501ccef6ef8b	\N	\N	f	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	50eba0ee-9760-4355-80c7-27e2f970bfc0	\N	t	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	946653d4-231f-4805-9c7a-5a104bcd46aa	\N	t	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	9be5fdc8-3b57-4822-8840-3e2a9944385c	t	t	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	acda06a9-1a27-45f0-bceb-1b77c81b8749	f	t	t	f
6a45efd2-7007-4a43-8bda-09e26135e1ac	b4be4621-3e9e-4e3d-a47b-df549ae100f4	t	t	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	d66ab356-6792-451d-8f42-84f07e48b9e7	\N	t	f	f
6a45efd2-7007-4a43-8bda-09e26135e1ac	f10293a6-79c7-41e2-840e-a597e5ace426	\N	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	1541696d-a165-4a06-9181-88472bd61293	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	2a5a511b-7bd4-424f-8b38-252f6115640f	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	2f4e5764-01c0-49f5-b178-555f814a015a	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	30e5a675-aac0-40f0-b139-721fc6affd7c	t	t	t	f
81dd7d3d-8092-49e3-b63d-a70b978984c5	36fbf7b0-d4ba-49d1-af67-1e0af4a05766	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	3d4ca5e7-8f17-4537-8d2a-4b0f02c3edf7	\N	f	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	50492ec8-26b3-4178-9d66-501ccef6ef8b	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	6e85a669-7fad-4377-bbec-0c6b16cfd516	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	6ea5af6e-aeb0-4bde-9e9b-8dd58f322c75	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	7a2eee70-8883-4c74-9a9b-e820f2249b4b	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	8a1dfd71-6c85-412f-91de-970ca913dc60	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	9312a5c1-7991-4eb1-be6d-05fef767935b	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	9be5fdc8-3b57-4822-8840-3e2a9944385c	\N	\N	\N	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	acda06a9-1a27-45f0-bceb-1b77c81b8749	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	b4be4621-3e9e-4e3d-a47b-df549ae100f4	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	b7d62610-7f21-43e4-a95b-43f1856411b4	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	d669b3d2-f9e1-4ad6-97b5-403cf7da0fdd	f	f	f	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	d66ab356-6792-451d-8f42-84f07e48b9e7	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	e3033e60-42d8-47fd-8c8a-6e51cebaba33	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	e7bec1b5-8c3b-447f-8921-2734e0350c07	\N	f	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	e89cdedb-32f8-47fd-bbfc-960c894dae01	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	ec657ac4-bfb1-453c-b3c0-e409f3ed27b9	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	f10293a6-79c7-41e2-840e-a597e5ace426	\N	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	f577d9b2-3ac6-43af-8c55-81f0652ffd72	t	t	t	t
97cc7404-490c-4827-beba-425c85acef5d	2a5a511b-7bd4-424f-8b38-252f6115640f	\N	t	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	2f4e5764-01c0-49f5-b178-555f814a015a	f	t	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	6ea5af6e-aeb0-4bde-9e9b-8dd58f322c75	\N	t	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	74fa394a-0fee-4b9d-827c-91a09f5c68f5	t	t	t	t
97cc7404-490c-4827-beba-425c85acef5d	8a1dfd71-6c85-412f-91de-970ca913dc60	\N	t	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	acda06a9-1a27-45f0-bceb-1b77c81b8749	\N	f	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	5fc41cc2-2391-4ba9-a1d5-d04d0aeef216	f	t	\N	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	b280071d-87e7-44be-99f3-64cd75883e63	f	t	\N	t
97cc7404-490c-4827-beba-425c85acef5d	50492ec8-26b3-4178-9d66-501ccef6ef8b	\N	f	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	b4be4621-3e9e-4e3d-a47b-df549ae100f4	\N	f	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	7a2eee70-8883-4c74-9a9b-e820f2249b4b	\N	t	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	9312a5c1-7991-4eb1-be6d-05fef767935b	\N	f	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	36fbf7b0-d4ba-49d1-af67-1e0af4a05766	\N	f	\N	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	74fa394a-0fee-4b9d-827c-91a09f5c68f5	t	t	t	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	fe012b79-adf5-42f4-bdbd-5ffd5417289c	t	t	t	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	ca530176-384e-4c7d-8377-1171ec3a8d05	t	t	t	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	\N	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	ca530176-384e-4c7d-8377-1171ec3a8d05	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	fe012b79-adf5-42f4-bdbd-5ffd5417289c	t	t	t	t
97cc7404-490c-4827-beba-425c85acef5d	ec657ac4-bfb1-453c-b3c0-e409f3ed27b9	\N	t	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	f10293a6-79c7-41e2-840e-a597e5ace426	\N	t	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	f577d9b2-3ac6-43af-8c55-81f0652ffd72	\N	t	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	\N	t	\N	\N
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	4938d59a-801c-44b9-adea-20ebdc9e6343	f	\N	\N	\N
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	\N	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	da53f223-2170-4c58-b561-aa1af6721541	t	t	t	t
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	7a2eee70-8883-4c74-9a9b-e820f2249b4b	\N	t	\N	\N
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	2a5a511b-7bd4-424f-8b38-252f6115640f	\N	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	fe012b79-adf5-42f4-bdbd-5ffd5417289c	t	t	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	b7d62610-7f21-43e4-a95b-43f1856411b4	t	t	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	e3033e60-42d8-47fd-8c8a-6e51cebaba33	t	t	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	74fa394a-0fee-4b9d-827c-91a09f5c68f5	t	t	t	t
97cc7404-490c-4827-beba-425c85acef5d	e741e54c-9c5f-454e-a2f9-a38b8806091b	\N	f	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	c7396230-d400-4629-abb3-b29a920febb3	\N	f	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	304c528b-aa08-466d-a766-45d3c102d733	\N	t	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	fb7ca00c-ef46-4dc1-bf9f-5d00358213a0	\N	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	b0014449-4706-48e1-b81d-d758057ef0d0	t	t	t	t
97cc7404-490c-4827-beba-425c85acef5d	1541696d-a165-4a06-9181-88472bd61293	\N	t	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	d66ab356-6792-451d-8f42-84f07e48b9e7	\N	f	\N	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	c7396230-d400-4629-abb3-b29a920febb3	\N	t	\N	\N
108ce450-d740-4bcb-9aaa-be9504bf94d8	e741e54c-9c5f-454e-a2f9-a38b8806091b	\N	t	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	da53f223-2170-4c58-b561-aa1af6721541	t	t	t	t
108ce450-d740-4bcb-9aaa-be9504bf94d8	43799900-fc23-4bc6-a8cc-40d3cf820640	\N	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	a40138cb-39c5-4625-9203-0de3ba0a9df1	\N	t	t	t
108ce450-d740-4bcb-9aaa-be9504bf94d8	304c528b-aa08-466d-a766-45d3c102d733	\N	t	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	ca530176-384e-4c7d-8377-1171ec3a8d05	t	t	t	f
2332800e-edb0-49cd-92d7-1abc3d981207	a40138cb-39c5-4625-9203-0de3ba0a9df1	t	t	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	fe012b79-adf5-42f4-bdbd-5ffd5417289c	t	t	t	f
81dd7d3d-8092-49e3-b63d-a70b978984c5	292e5ff3-1095-49b3-aafd-565794ee545e	t	t	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	e741e54c-9c5f-454e-a2f9-a38b8806091b	t	t	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	304c528b-aa08-466d-a766-45d3c102d733	t	t	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	a40138cb-39c5-4625-9203-0de3ba0a9df1	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	e964618e-29bd-4e17-91f2-1f42c0fa0488	\N	t	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	ca530176-384e-4c7d-8377-1171ec3a8d05	t	t	t	t
2332800e-edb0-49cd-92d7-1abc3d981207	b0014449-4706-48e1-b81d-d758057ef0d0	t	t	t	t
97cc7404-490c-4827-beba-425c85acef5d	fe012b79-adf5-42f4-bdbd-5ffd5417289c	t	t	t	t
97cc7404-490c-4827-beba-425c85acef5d	a40138cb-39c5-4625-9203-0de3ba0a9df1	\N	f	f	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	97c15763-d7b1-4846-9fc9-f5a36dfa5468	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	923143fd-ca98-489d-88d7-642d2001b6ef	\N	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	d54451d1-3a31-442a-8279-1cdc0ed2b221	f	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	59b034e9-a1ed-4267-afba-606ead1b9500	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	c7396230-d400-4629-abb3-b29a920febb3	f	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	18382929-d59a-4b7f-afb9-9c90d71b438f	f	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	a2f03c97-68e6-426b-9ab9-bf99ca5ffff0	f	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	ec25002a-1567-4093-bd9b-34e7be06a9ce	f	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	43799900-fc23-4bc6-a8cc-40d3cf820640	f	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	7e730a7e-10b8-4708-ac93-0cef3a2ca1e9	f	t	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	d54451d1-3a31-442a-8279-1cdc0ed2b221	f	t	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	e964618e-29bd-4e17-91f2-1f42c0fa0488	\N	t	\N	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	c34d2968-b466-4482-9803-d85a69a73d06	t	t	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	5fc41cc2-2391-4ba9-a1d5-d04d0aeef216	f	t	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	b280071d-87e7-44be-99f3-64cd75883e63	f	t	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	d54451d1-3a31-442a-8279-1cdc0ed2b221	\N	t	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	c7396230-d400-4629-abb3-b29a920febb3	\N	t	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	43799900-fc23-4bc6-a8cc-40d3cf820640	\N	t	\N	\N
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	74fa394a-0fee-4b9d-827c-91a09f5c68f5	\N	t	\N	\N
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	1541696d-a165-4a06-9181-88472bd61293	\N	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	e964618e-29bd-4e17-91f2-1f42c0fa0488	\N	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	97c15763-d7b1-4846-9fc9-f5a36dfa5468	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	82f810bf-86ce-4c88-927e-5e9a4ea82587	\N	t	t	t
2332800e-edb0-49cd-92d7-1abc3d981207	e741e54c-9c5f-454e-a2f9-a38b8806091b	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	97c15763-d7b1-4846-9fc9-f5a36dfa5468	\N	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	e741e54c-9c5f-454e-a2f9-a38b8806091b	f	t	t	t
2332800e-edb0-49cd-92d7-1abc3d981207	d54451d1-3a31-442a-8279-1cdc0ed2b221	\N	t	\N	\N
6a45efd2-7007-4a43-8bda-09e26135e1ac	5281099c-88c4-417d-9e0d-285fcc8ffc41	f	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	304c528b-aa08-466d-a766-45d3c102d733	t	t	t	t
2332800e-edb0-49cd-92d7-1abc3d981207	5281099c-88c4-417d-9e0d-285fcc8ffc41	\N	t	\N	\N
97cc7404-490c-4827-beba-425c85acef5d	5281099c-88c4-417d-9e0d-285fcc8ffc41	\N	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	82f810bf-86ce-4c88-927e-5e9a4ea82587	t	t	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	c34d2968-b466-4482-9803-d85a69a73d06	t	t	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	82f810bf-86ce-4c88-927e-5e9a4ea82587	t	t	t	t
97cc7404-490c-4827-beba-425c85acef5d	5fc41cc2-2391-4ba9-a1d5-d04d0aeef216	f	t	f	\N
97cc7404-490c-4827-beba-425c85acef5d	b280071d-87e7-44be-99f3-64cd75883e63	f	t	f	\N
81dd7d3d-8092-49e3-b63d-a70b978984c5	c6fabac7-e2b3-4245-a69a-84992efa4bc1	t	t	t	t
6a45efd2-7007-4a43-8bda-09e26135e1ac	17c15763-d7b1-4846-9fc9-f5a36dfa5468	f	t	\N	\N
2332800e-edb0-49cd-92d7-1abc3d981207	17c15763-d7b1-4846-9fc9-f5a36dfa5468	\N	t	\N	\N
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	5281099c-88c4-417d-9e0d-285fcc8ffc41	\N	t	\N	\N
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	e964618e-29bd-4e17-91f2-1f42c0fa0488	\N	t	\N	\N
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	5fc41cc2-2391-4ba9-a1d5-d04d0aeef216	\N	f	\N	\N
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	b280071d-87e7-44be-99f3-64cd75883e63	\N	f	\N	\N
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	d54451d1-3a31-442a-8279-1cdc0ed2b221	\N	f	\N	\N
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	ca530176-384e-4c7d-8377-1171ec3a8d05	t	t	t	\N
2332800e-edb0-49cd-92d7-1abc3d981207	59b034e9-a1ed-4267-afba-606ead1b9500	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	23ca0398-00b3-4547-9312-63633b0e4a09	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	304c528b-aa08-466d-a766-45d3c102d733	t	t	t	t
108ce450-d740-4bcb-9aaa-be9504bf94d8	a40138cb-39c5-4625-9203-0de3ba0a9df1	t	t	t	f
2332800e-edb0-49cd-92d7-1abc3d981207	e7dadcd0-c8ce-41e1-af4b-4fdc566a8868	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	e7dadcd0-c8ce-41e1-af4b-4fdc566a8868	t	t	t	t
108ce450-d740-4bcb-9aaa-be9504bf94d8	c34d2968-b466-4482-9803-d85a69a73d06	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	08f6f5e6-0933-48e2-88ce-ce52f2709040	t	t	t	t
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	c34d2968-b466-4482-9803-d85a69a73d06	t	t	t	t
2332800e-edb0-49cd-92d7-1abc3d981207	c34d2968-b466-4482-9803-d85a69a73d06	t	t	t	t
108ce450-d740-4bcb-9aaa-be9504bf94d8	da53f223-2170-4c58-b561-aa1af6721541	t	t	t	\N
97cc7404-490c-4827-beba-425c85acef5d	c34d2968-b466-4482-9803-d85a69a73d06	t	t	t	t
97cc7404-490c-4827-beba-425c85acef5d	da53f223-2170-4c58-b561-aa1af6721541	t	t	t	t
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	da53f223-2170-4c58-b561-aa1af6721541	t	t	t	t
52e68b85-90c1-4910-b9b0-df9a3b3574c4	c34d2968-b466-4482-9803-d85a69a73d06	t	t	t	t
2332800e-edb0-49cd-92d7-1abc3d981207	da53f223-2170-4c58-b561-aa1af6721541	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	388deab8-36ff-4a8b-a1d7-b88eab8b4c26	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	0d59e730-4d54-4ef2-a892-2bdb88f4e732	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	cb7ac7ac-9d00-4acd-a1fd-2afefb0c8a81	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	92559c74-927e-4edf-85c8-203447141840	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	72e9e5f5-9b6c-4213-9c23-85588627d997	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	bfa6447a-7dc4-4495-9397-db2e5865ade1	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	c43ac70d-a037-4148-827d-d5c0a8b00b97	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	340671b5-d101-46ec-b86f-6dc06e9d5668	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	74fa394a-0fee-4b9d-827c-91a09f5c68f5	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	93391995-8d3d-447c-a50f-1cec916d3c27	t	t	t	t
81dd7d3d-8092-49e3-b63d-a70b978984c5	e36e779a-e2aa-468e-a11c-972f75c1c052	t	t	t	t
\.


--
-- Data for Name: roleusers; Type: TABLE DATA; Schema: public; Owner: eaudit
--

COPY public.roleusers (role, "user") FROM stdin;
108ce450-d740-4bcb-9aaa-be9504bf94d8	1ed280a7-f742-431b-9193-ab9a82f1be04
2332800e-edb0-49cd-92d7-1abc3d981207	366cad9c-e266-4907-8a6a-d9a18a9424c9
108ce450-d740-4bcb-9aaa-be9504bf94d8	2b67dbfc-97e8-45f1-8627-be6cd2b55629
97cc7404-490c-4827-beba-425c85acef5d	366cad9c-e266-4907-8a6a-d9a18a9424c9
2332800e-edb0-49cd-92d7-1abc3d981207	0b78c0a2-0548-4ece-b97a-e557bdc882bf
108ce450-d740-4bcb-9aaa-be9504bf94d8	748a008e-4ad4-4586-826b-fa4c9886cb61
97cc7404-490c-4827-beba-425c85acef5d	0b78c0a2-0548-4ece-b97a-e557bdc882bf
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	33744954-9c74-4c0f-89b7-3e1b65fb61d2
97cc7404-490c-4827-beba-425c85acef5d	33744954-9c74-4c0f-89b7-3e1b65fb61d2
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	b6ffd03c-a5ef-4c89-99a7-446d2545fff8
97cc7404-490c-4827-beba-425c85acef5d	b6ffd03c-a5ef-4c89-99a7-446d2545fff8
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	3cd6fe67-4266-465f-ae5a-d1e60dfd2709
97cc7404-490c-4827-beba-425c85acef5d	3cd6fe67-4266-465f-ae5a-d1e60dfd2709
2332800e-edb0-49cd-92d7-1abc3d981207	4d519e67-3c64-4b5c-8c62-2c9099eb109e
97cc7404-490c-4827-beba-425c85acef5d	4d519e67-3c64-4b5c-8c62-2c9099eb109e
2332800e-edb0-49cd-92d7-1abc3d981207	dbb60522-1516-4c19-a95d-5b70a0382498
6a45efd2-7007-4a43-8bda-09e26135e1ac	3e7b5d34-9467-4386-8891-d9cd72692094
97cc7404-490c-4827-beba-425c85acef5d	3e7b5d34-9467-4386-8891-d9cd72692094
52e68b85-90c1-4910-b9b0-df9a3b3574c4	12ab6597-03be-48eb-b6e3-57d830989ed8
52e68b85-90c1-4910-b9b0-df9a3b3574c4	2329389f-555c-42fe-ba49-8703a09a6314
52e68b85-90c1-4910-b9b0-df9a3b3574c4	2b67dbfc-97e8-45f1-8627-be6cd2b55629
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	b41048a4-1ff0-4ca1-949a-c8275e10ade8
97cc7404-490c-4827-beba-425c85acef5d	b41048a4-1ff0-4ca1-949a-c8275e10ade8
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	5efd92f0-d96e-4037-ab17-467310a76cd0
97cc7404-490c-4827-beba-425c85acef5d	5efd92f0-d96e-4037-ab17-467310a76cd0
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	e03281a2-712d-497a-a86e-7c7e0a425052
97cc7404-490c-4827-beba-425c85acef5d	e03281a2-712d-497a-a86e-7c7e0a425052
97cc7404-490c-4827-beba-425c85acef5d	90ef9c22-9163-4459-8dde-319cb03dd221
108ce450-d740-4bcb-9aaa-be9504bf94d8	90ef9c22-9163-4459-8dde-319cb03dd221
81dd7d3d-8092-49e3-b63d-a70b978984c5	2329389f-555c-42fe-ba49-8703a09a6314
81dd7d3d-8092-49e3-b63d-a70b978984c5	2b67dbfc-97e8-45f1-8627-be6cd2b55629
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	46a6c7ed-f739-4b61-a893-79646f60549b
97cc7404-490c-4827-beba-425c85acef5d	46a6c7ed-f739-4b61-a893-79646f60549b
97cc7404-490c-4827-beba-425c85acef5d	1ed280a7-f742-431b-9193-ab9a82f1be04
97cc7404-490c-4827-beba-425c85acef5d	75fe446d-ee00-41ef-9144-9af013c8a02b
108ce450-d740-4bcb-9aaa-be9504bf94d8	75fe446d-ee00-41ef-9144-9af013c8a02b
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	21ec1744-ea60-4bb3-b8ed-fc9ffc9918e6
97cc7404-490c-4827-beba-425c85acef5d	21ec1744-ea60-4bb3-b8ed-fc9ffc9918e6
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	fc903300-d0fd-4fbd-a09e-2e6646bbc89f
97cc7404-490c-4827-beba-425c85acef5d	fc903300-d0fd-4fbd-a09e-2e6646bbc89f
97cc7404-490c-4827-beba-425c85acef5d	6a962dbe-16da-4609-bb7c-77bdb9e10f1d
108ce450-d740-4bcb-9aaa-be9504bf94d8	6a962dbe-16da-4609-bb7c-77bdb9e10f1d
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	ac776057-ba95-4b14-b37a-ba2a9ff8c2b9
97cc7404-490c-4827-beba-425c85acef5d	ac776057-ba95-4b14-b37a-ba2a9ff8c2b9
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	d2c224f6-d2d9-49b9-8285-cc953b2ebc5f
97cc7404-490c-4827-beba-425c85acef5d	d2c224f6-d2d9-49b9-8285-cc953b2ebc5f
97cc7404-490c-4827-beba-425c85acef5d	80cc7a7a-186a-4156-9d57-58d0682e3d36
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	b633f299-a183-4681-b351-c9d133c58e71
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	dbb60522-1516-4c19-a95d-5b70a0382498
2332800e-edb0-49cd-92d7-1abc3d981207	22f106d2-756e-4c26-ac69-923e4ea75a6f
108ce450-d740-4bcb-9aaa-be9504bf94d8	22f106d2-756e-4c26-ac69-923e4ea75a6f
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	ac513f02-a71e-4764-b78c-216b62ea590f
97cc7404-490c-4827-beba-425c85acef5d	4ae755a7-3733-4f95-9bbe-02ce3a481fb7
97cc7404-490c-4827-beba-425c85acef5d	d016fde5-c284-4455-a44a-723ab77fa99f
108ce450-d740-4bcb-9aaa-be9504bf94d8	f741041f-732a-481f-bc78-461bc55ada9b
97cc7404-490c-4827-beba-425c85acef5d	709e3413-c729-4435-ae26-eff4cb99f23b
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	bf5300ba-29bf-424c-bb09-e97e0e3f07c8
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	29f41daa-38fd-43c3-8376-0942617e0fff
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	2a9c364b-035d-4323-a042-7f6c3a5bedf4
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b
97cc7404-490c-4827-beba-425c85acef5d	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	7a766876-545a-42a4-bb81-ca6fa59efff6
108ce450-d740-4bcb-9aaa-be9504bf94d8	ee570f4f-46de-4761-ae5c-b205c92d5c3e
97cc7404-490c-4827-beba-425c85acef5d	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae
108ce450-d740-4bcb-9aaa-be9504bf94d8	dd7ae175-271f-467e-979e-8d2d709b1f0e
108ce450-d740-4bcb-9aaa-be9504bf94d8	222cead4-f3bf-48d7-9541-84f60c66cac5
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	70bf9ad1-3775-4be7-abab-9128837416ad
108ce450-d740-4bcb-9aaa-be9504bf94d8	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	9db069b6-02be-46d9-9d45-a89f2d741ec1
2332800e-edb0-49cd-92d7-1abc3d981207	ae3234c8-ea30-45d2-a48e-367b8c32567d
97cc7404-490c-4827-beba-425c85acef5d	e54dd636-fb29-4ada-b95c-848c39ed6d03
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	29fe44b6-4bc3-4de2-b917-2b15713b546d
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	b5d26e6b-30f7-46a3-b7fe-03b66a235d86
108ce450-d740-4bcb-9aaa-be9504bf94d8	1b162438-55b1-4b5c-ac17-5699e9dfb1ed
108ce450-d740-4bcb-9aaa-be9504bf94d8	55fc1778-b710-47c8-a860-b519a03e23bf
108ce450-d740-4bcb-9aaa-be9504bf94d8	a30f21b6-918e-421d-835c-fac8228feed5
97cc7404-490c-4827-beba-425c85acef5d	3d1d59ae-5115-44ee-8d8c-7db38a5e5200
2332800e-edb0-49cd-92d7-1abc3d981207	95542b83-a537-4794-a103-b768408db707
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	986b958b-1a98-44e4-a59b-c02a15620381
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	9d5f96cb-8d85-4216-b60e-809f39620ac6
52e68b85-90c1-4910-b9b0-df9a3b3574c4	fe251bf2-8d4f-4205-af0b-51160cd31e63
2332800e-edb0-49cd-92d7-1abc3d981207	fe251bf2-8d4f-4205-af0b-51160cd31e63
97cc7404-490c-4827-beba-425c85acef5d	fe251bf2-8d4f-4205-af0b-51160cd31e63
108ce450-d740-4bcb-9aaa-be9504bf94d8	fe251bf2-8d4f-4205-af0b-51160cd31e63
81dd7d3d-8092-49e3-b63d-a70b978984c5	fe251bf2-8d4f-4205-af0b-51160cd31e63
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	998cdf4a-6b17-468e-9c07-186e1a724f6d
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	89488dc0-00e0-48c8-9e47-1f143b877878
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	0188ba46-865b-43b4-9adf-44f6e4fe0cae
52e68b85-90c1-4910-b9b0-df9a3b3574c4	2fed6324-e046-41ce-b7ce-3789033105eb
2332800e-edb0-49cd-92d7-1abc3d981207	2fed6324-e046-41ce-b7ce-3789033105eb
97cc7404-490c-4827-beba-425c85acef5d	2fed6324-e046-41ce-b7ce-3789033105eb
108ce450-d740-4bcb-9aaa-be9504bf94d8	2fed6324-e046-41ce-b7ce-3789033105eb
81dd7d3d-8092-49e3-b63d-a70b978984c5	2fed6324-e046-41ce-b7ce-3789033105eb
52e68b85-90c1-4910-b9b0-df9a3b3574c4	71843774-648d-4439-8bfb-f215cf5ec803
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	71843774-648d-4439-8bfb-f215cf5ec803
2332800e-edb0-49cd-92d7-1abc3d981207	71843774-648d-4439-8bfb-f215cf5ec803
97cc7404-490c-4827-beba-425c85acef5d	71843774-648d-4439-8bfb-f215cf5ec803
108ce450-d740-4bcb-9aaa-be9504bf94d8	71843774-648d-4439-8bfb-f215cf5ec803
81dd7d3d-8092-49e3-b63d-a70b978984c5	71843774-648d-4439-8bfb-f215cf5ec803
2332800e-edb0-49cd-92d7-1abc3d981207	e024d7dc-a9ed-47b7-919e-1edb6036c3cb
108ce450-d740-4bcb-9aaa-be9504bf94d8	ebe58226-6813-4081-8377-b11d002e2be8
97cc7404-490c-4827-beba-425c85acef5d	d194de79-57b6-40a9-b216-5e5f63b65712
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	7afb8c60-b1c5-4dd4-b1e8-5bb98ea44085
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	55b3d390-5c20-4738-a061-e9435606c883
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	64ffa694-7d59-4a61-a781-87708f8c40fe
2332800e-edb0-49cd-92d7-1abc3d981207	e571a6b9-809f-4a61-910d-c765dbd55b6a
97cc7404-490c-4827-beba-425c85acef5d	24a0fcd4-1c0d-4359-aa3b-f2f0b69ff66d
97cc7404-490c-4827-beba-425c85acef5d	4bd27afd-8b72-4c03-b2a0-6d35b42180a3
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	e3740c89-1a9c-4f9f-b9ac-4307df48b2dc
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	4d4faea1-a44b-46bd-b628-17213e34145f
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	3430e73c-7fd6-4750-9401-1f69886c331d
2332800e-edb0-49cd-92d7-1abc3d981207	560270b9-a2fe-4d9a-8484-96d723432f37
108ce450-d740-4bcb-9aaa-be9504bf94d8	3e051b0c-61d9-4de4-b772-fdadb7fa9af9
108ce450-d740-4bcb-9aaa-be9504bf94d8	60c1ffee-4ffd-4ff3-a925-7c3503d341df
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	c93ca7d5-c5c5-46f9-b341-3fde3cbaca23
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	650c3754-dbc6-462d-929b-4a73c57ae400
9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26	48ba7890-f05c-4d83-9b17-de6aab28db0c
108ce450-d740-4bcb-9aaa-be9504bf94d8	ed485c96-003c-4d05-b7ac-ac38a883257d
97cc7404-490c-4827-beba-425c85acef5d	a178026b-4e6f-43e7-941e-7559426f2757
97cc7404-490c-4827-beba-425c85acef5d	557b5e62-6819-48f1-8b45-af36c4a986f1
97cc7404-490c-4827-beba-425c85acef5d	e99a0c6e-c0e0-4064-bcfd-69cd08786b6a
97cc7404-490c-4827-beba-425c85acef5d	429d2712-60dc-49a3-a3fa-362126f124df
97cc7404-490c-4827-beba-425c85acef5d	896bb850-eca4-42f7-b75f-98cb8b67e0a6
97cc7404-490c-4827-beba-425c85acef5d	acbc412d-f29a-4931-b599-a37cee3aed69
\.


--
-- Data for Name: sessions; Type: TABLE DATA; Schema: public; Owner: eaudit
--

COPY public.sessions (id, ip_address, "timestamp", data) FROM stdin;
34932f14af4514ae51abeff43dd4c515704cdbd4	36.81.143.5	1543911710	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzEwOw==
89aec7f4dfb698ca0eeb0dd787255c0c646ed5d1	36.81.143.5	1543911710	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzEwOw==
1df63c337dd30050708c41ed684f2468bfb7e6db	36.81.143.5	1543911711	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzExOw==
c6eb0a3b45d85fd9563bf75c8712d4a25219d8a6	36.81.143.5	1543911711	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzExOw==
ee3f58539d0a092fd1f881cd019e88d2a3fa693f	103.84.117.100	1543913549	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTQ5O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
5d87c6d5f19430565b08475f84b72bc1c05533b4	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
f6209c05934d4e3b46eebda90fa1e4e0f9b8df76	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
62228599204530e697545062cc4adc72ea6c5687	103.84.117.100	1543913551	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUxO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
54c21413b8e73f150eac17f6934c0b1f02e6f2e2	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1Ow==
5440e154c6f46cc047bd1b8c7a84475377913334	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1Ow==
c53a0cc8aafae8fc94270bcdefa81e6d4baae257	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1Ow==
2ca9e3bfc89ff3a6d4df4cdcae2c2fbcb441ff04	54.245.60.54	1543925755	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI1NzU1O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
ab8c255771bfd29f903c9bf2da4ac1f89c1cdbf9	54.245.60.54	1543925759	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI1NzU5O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
6b625b5626c8a1e96cb4986bd8bf0f43f7d892ed	54.202.169.83	1543926092	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MDkyOw==
3c5b79fb291b4ee2455cfe479c61a67e3ab8c0f1	54.202.169.83	1543926096	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MDk2Ow==
9c2d4716ee8b1b5c80cb5303551c205f883dc817	54.202.169.83	1543926099	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MDk5Ow==
7904e4ab8f7921f95b8d23e128747342bb2d430b	66.249.71.41	1543966841	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTY2ODQxOw==
8bb337612f8f49ebc3a5367c11baff3f2cd6b125	66.249.71.41	1544000059	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQ0MDAwMDU3Ow==
5b388f360638141d0bc59d0bb7067f6d5774aa69	36.81.143.5	1543908180	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgwOw==
83841e37616f716626ba005633b9f5a695b98d0d	36.81.143.5	1543908182	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgyOw==
a848fe233542ce066a4b6ed426a71e8f1efb113c	36.81.143.5	1543908183	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgzOw==
4b9fe58fc5d34781994987d46591006938c6adf6	36.81.143.5	1543908184	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTg0Ow==
d3813ef2aebae230ea840e1e16db98027af89640	36.81.143.5	1543911599	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNTk5O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
3ca29cd33d30d5308fe3f2e289a384c1043be20a	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
118f2874c3aa7fe27650bd720d684ccc0b5dd907	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
4ab4c66ff991a8c7453594ba913a075c4cfa5d70	66.249.71.35	1544000063	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQ0MDAwMDYyO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
654eab9a06912c456adbb6cdfb5648b8d66493cf	36.81.143.5	1543911710	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzEwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
d4384b3aa02754fe2d9c943e64d7bda5ad15475b	36.81.143.5	1543911710	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzEwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
8041bef81e027ac8da172b7f1afa342c1c53663c	36.81.143.5	1543911711	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzExO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
fccd50eebb573e535d9e7b321a1783b0321b6a3a	36.81.143.5	1543911711	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzExO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
c9dceed26e0799417d5174c17073f3e6a6a35d08	103.84.117.100	1543913549	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTQ5Ow==
e1cdc5560405a51aec5c699454d075431dbc8f10	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwOw==
5f58e7834d9a8fab7c09443b77c7a7f77cfdbd5b	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwOw==
5f9b258c6af056fce372360ae7af95fa25dd644c	103.84.117.100	1543913551	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUxOw==
c095855432420142a270bfce51aa4455647392cc	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
77bd590008556307ca614b4f6d64c43a46f0fff8	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
e8d65900d1e0f5da3f78881f106a34fec828c1de	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
8f75febf4a6a10f82a16b832cf3c6a80a20896c3	54.245.60.54	1543925756	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI1NzU2Ow==
20c9f51a42785cefbbbb31d652390feb83e40a80	54.245.60.54	1543925759	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI1NzU5Ow==
1856d32658d9794c38f9b1c2346d4ce97ce0d1c4	54.202.169.83	1543926093	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MDkzO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
230ba063d957e8d7c7bd64d1794ed26c0cf20ca9	54.202.169.83	1543926096	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MDk2O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
0fe6d196fa9ceea2e2ea41fd2b669e1fef626f71	54.202.169.83	1543926100	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MTAwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
5532bb2db1c4db3179ca8ba9ffdf64d942999338	66.249.71.35	1543966841	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTY2ODQxOw==
fd907927d8f7c67c634656c93c41faef039edc0b	36.81.143.5	1543908180	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
2001e4a5142a62ede566f4ab5701c823afc040d2	36.81.143.5	1543908182	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgyO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
951aa677f3ec27e13dbfe94b036977290ad5b38f	36.81.143.5	1543908183	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgzO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
55f83cf240ab77f60967be7189ca538f6b6dae35	36.81.143.5	1543908184	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTg0O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
4fe128267014f2bb24287b1d59fc9c69bc1e337a	36.81.143.5	1543911599	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNTk5Ow==
dd95cf0dda340d832003e4bf498183fe181ea834	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwOw==
fff718fb687fa914fe56c79ecf08342969d33fc8	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwOw==
abb7953eb246132a660a7d0c8dac8b0bdf51ec15	66.249.71.39	1544000058	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQ0MDAwMDU4O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
e0196540821e62fe79d0934d606add08a1805719	36.81.143.5	1543911710	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzEwOw==
0b9738dd66ef4c456c2557d5fb40954544a938ac	36.81.143.5	1543911710	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzEwOw==
f4cf03c18a0cf85ea8f588578af1bb72bce95b0a	36.81.143.5	1543911711	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzExOw==
29f904d74dcd725397b522f83def9709135a8db1	36.81.143.5	1543911711	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzExOw==
205521434842c188a8b9af5b4089755a70b30652	103.84.117.100	1543913549	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTQ5O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
912057d6cb60640869b4e146cee9b66ddb218503	36.81.143.5	1543911599	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNTk5O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
159ab0e40610b62a1447aa89f74dc113ded4242b	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
09f4bafb940765b89c86812a745e0f1b07b28de9	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
41524b34e2d0a06935094a689c357ed5851324f3	103.202.227.200	1543914824	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI0Ow==
901b480db96e4d8084f02a8ba802417fed5985d7	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1Ow==
992b018c92076c4ee9954717aa3b5af486d6f5a4	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1Ow==
7dce603ba64414434cecd4b2259a2c33b0bed46c	103.202.227.200	1543914826	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI2Ow==
f00136631fc18d9cc51977c4e2edb294b948884e	54.245.60.54	1543925756	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI1NzU2O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
1b6c3b4e910083755226afb3cff6fb7f6e338774	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
3e7a319e3338bdef94ebdd3a6521f4344ba59298	54.245.60.54	1543925760	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI1NzYwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
c793a8cff7ac77b9ce51f31e94d02c473c5d90c5	36.81.143.5	1543908182	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgyOw==
34e171e81abcea1da838fd781f44147dcfd1bfa3	36.81.143.5	1543908182	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgyOw==
d5157c4d60af50084010e255f099e1a55fbf3156	36.81.143.5	1543908184	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTg0Ow==
2d12d811f395968cac2e2b2a59c2fdbb1bca1d51	36.81.143.5	1543908184	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTg0Ow==
1a49e7de7e55ca1ea7ad3d1e43cc1a0c7a3ed32f	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
e92a665fe0cf3700233e43d1ed0715ea19a61ae7	54.202.169.83	1543926093	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MDkzOw==
c0bf24db19533a8e728d3c5c538eb331648abc5c	54.202.169.83	1543926097	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MDk3Ow==
69c69c1399b4180207bec52fdf309107b5eb48ce	54.202.169.83	1543926101	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MTAxOw==
a6e0ce92c721acebdab95fee403fe1ea6fcfae1a	66.249.71.39	1543966843	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTY2ODQzOw==
8bb337612f8f49ebc3a5367c11baff3f2cd6b125	66.249.71.35	1544000058	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQ0MDAwMDU4O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
ee695894dcfcbfbfb13c87759d0a0776b17f83d1	36.81.143.5	1543911710	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzEwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
d2cbec6af9c2e47c72d20c08c99eea43f1c11a18	36.81.143.5	1543911710	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzEwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
9a087a66d94408320b4907f5366fb530d5e4bf88	36.81.143.5	1543911711	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzExO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
7a8abefea4b003d3849e2ea63de45e8842c976f3	66.249.66.22	1543912439	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEyNDM4O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
5b2d9db37603c609a3a94693573e8ddc85701597	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwOw==
18ad36760062447b5ffda45b3be21cf03e127b86	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwOw==
3e3c8554d67d1409df73f2eaf6f64ce14a932e24	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwOw==
fe2fbe1c5dc65590caba4745ec34308294576015	103.202.227.200	1543914824	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI0O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
373cb7e15d06ed58d58289998bca78340205bd52	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
e3f8a590e8392b9ea25effaf5d182104363f626b	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
181e476ac74ad4033496b52c4444665d13e362b8	103.202.227.200	1543914826	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI2O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
01c6a9d626ae4df0d0c254b2a4bcc15386d9dfbb	54.245.60.54	1543925757	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI1NzU3Ow==
2b096ad1019bb0f9a04a571b2b4462027f9edac6	54.245.60.54	1543925761	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI1NzYxOw==
5511caeffc0e960822cf02adc0c21cb9daf6ff89	54.202.169.83	1543926094	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MDk0O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
aea25d9d0e47b9e9034cc430953530f6119011ad	54.202.169.83	1543926098	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MDk4O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
b76839df4d0c5fe93eda1ae2183a9664363d8232	36.81.143.5	1543908182	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgyO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
2e26df4af3c46ad8bf0f32f3ade060c651a7a66b	36.81.143.5	1543908183	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgzO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
ac028687a3a18d6e71cc909b11091e4c2b50d764	36.81.143.5	1543908184	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTg0O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
92c9e35d2c5f76a9143f9db801dfc96ee8be5ebb	36.81.143.5	1543911599	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNTk5Ow==
1f34e938830ddb60bee6f55a089615e9112d0af8	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwOw==
17553a5cc72f81c5993a3702c9471e109d4d7a27	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwOw==
5f2a6da2b27a58af09019f05fbae8b41b553d201	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwOw==
40a17d6db49a0a92289547d137f1f7f3345fc004	36.85.30.197	1543926674	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2NjczO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
a6e0ce92c721acebdab95fee403fe1ea6fcfae1a	66.249.71.35	1543966844	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTY2ODQzO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
abb7953eb246132a660a7d0c8dac8b0bdf51ec15	66.249.71.35	1544000059	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQ0MDAwMDU5Ow==
1d682aa231570e616ed207cb766be54b4d98efb5	36.81.143.5	1543911710	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzEwOw==
9ee1ac6f4b1779a4fff0c45f37de1fe0912d4b18	36.81.143.5	1543911711	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzExOw==
eb2a8eaee185e579166fa0c574bcb7d2e04b20f7	36.81.143.5	1543911711	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzExOw==
7a8abefea4b003d3849e2ea63de45e8842c976f3	66.249.66.21	1543912440	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEyNDQwOw==
699e9b6349e7233303c7a656b2b8f80440cf6752	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
8bd68330796ea4dabbd42053d08187d759d9c9ac	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
6166e45cd42add3a819d1a1eef3b3f243ff2e18b	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
de884987b146a277e4d1abd5eccdc6c5a00c5032	103.202.227.200	1543914824	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI0Ow==
b900378f4a9d9a4d465f65f01da73d417876c8e4	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1Ow==
9aca085c5581ed907ef66c83360d6a76fae795e0	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1Ow==
175357560dfe79ae3826f26570a9c4dbd7df9eb1	103.202.227.200	1543914826	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI2Ow==
b2bc5a13cf34ed34bc3d3be3dd1e366ab6de9105	54.245.60.54	1543925758	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI1NzU4O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
fd52b4832f7cd1ef7866c63fcef92a4de6d7d04c	54.245.60.54	1543925761	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI1NzYxO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
4dbcb8acda6d03745d9237201fded358fc94b6b2	54.202.169.83	1543926095	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MDk1Ow==
4af1e68e7a2613a3d93e37787026e4152361f652	54.202.169.83	1543926098	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MDk4Ow==
e635e33090f9edf2eb044d628fc8a36295a28444	36.81.143.5	1543908182	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgyOw==
8b130730b956341c012a763f2d090058fbd34f44	36.81.143.5	1543908183	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgzOw==
57025cfb6df155d8ddc76fc2e62ce5a2e8936d7f	36.81.143.5	1543908184	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTg0Ow==
33423d8ae393c3fce02d766c5129a1d4087ca9d2	36.81.143.5	1543911599	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNTk5O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
55bed101072d2e3c5b3af88a9d5f8226d5264c62	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
33d101d68fbdc0a9fea13202b2d2f200e1e9fdad	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
3151b273d933c54d3c8f49ebfbfc8c60abd10222	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
7904e4ab8f7921f95b8d23e128747342bb2d430b	66.249.71.35	1543966840	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTY2ODM4O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
34de335fdfb798b34ef1cd21997d6e843593bd2e	66.249.71.39	1543966845	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTY2ODQ0Ow==
adb480fe35e86dc767314d1ca0b05e40e070f476	66.249.71.35	1544000061	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQ0MDAwMDYxOw==
eaf2414bce6382127579845ea438573b2828d7e2	36.81.143.5	1543911710	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzEwO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
03127ae93e8489ac74d1c2fd1c0f2213fafaaffc	36.81.143.5	1543911711	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzExO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
13336521945352da78d035308c3d4a38e969062e	36.81.143.5	1543911711	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNzExO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
a4b19d1c56491baa493ca46f3ac9616b03f70cdd	103.84.117.100	1543913549	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTQ5Ow==
46649b7c0180ce9f8c2bc47ce2b7d2d0508ea609	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwOw==
9e883dd049cd641e6fb61101e1cca9b38c52b163	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwOw==
914d7bc188de8d365e193788952e0a3781dfdc87	103.84.117.100	1543913550	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTEzNTUwOw==
fa2ea9c817579bf9599a36c6feed4ca5e378c329	103.202.227.200	1543914824	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI0O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
fff3481b500d5f582a7662126e922b5a7fbaa90b	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
191fd9d9ecb298658b58fbac66bfdc9037a2e9a0	103.202.227.200	1543914825	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTE0ODI1O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
7a394f5faae9c5f716430bf8cc9d70f75c1c491a	54.245.60.54	1543925754	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI1NzU0Ow==
5e0df3baf4d5c4dd173c16dcfaaa928cec019e54	54.245.60.54	1543925758	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI1NzU4Ow==
732cbe8eebd87e5a876491a353d82c2e5731ecbe	54.245.60.54	1543925762	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI1NzYyOw==
cc18f13022072bea359083104f23e33c3c0a47f2	54.202.169.83	1543926095	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MDk1O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
dff0c189e77d7466f985fbc2f365e33cfcc8148e	54.202.169.83	1543926099	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTI2MDk5O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
5532bb2db1c4db3179ca8ba9ffdf64d942999338	66.249.71.41	1543966840	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTY2ODM5O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
34de335fdfb798b34ef1cd21997d6e843593bd2e	66.249.71.35	1543966844	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTY2ODQ0O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
adb480fe35e86dc767314d1ca0b05e40e070f476	66.249.71.39	1544000062	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQ0MDAwMDYxO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
c6ef2a4df97cf8ae79e03bdb949775c5d8ba750d	36.81.143.5	1543908182	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgyO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
c3ff4e8df03b909e0bbcd0242c425493283dac78	36.81.143.5	1543908183	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTgzO2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
e5c46230cf0548039ca994c3957dee42b349a0c8	36.81.143.5	1543908184	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTA4MTg0O2NvbmZpZ3xhOjY6e3M6MjoiaWQiO2k6MTtzOjE6ImEiO3M6MTg6Ik1lZ2FoIEpheWEgU2VudG9zYSI7czoxOiJkIjtzOjE4OiJNZWdhaCBKYXlhIFNlbnRvc2EiO3M6NzoiY29tcGFueSI7czoxODoiRS1BdWRpdCBCYW55dXdhbmdpIjtzOjg6ImxvZ290ZXh0IjtzOjc6IkUtQXVkaXQiO3M6MTE6Im1haW50ZW5hbmNlIjtzOjE6ImYiO30=
f535e378bef9591c926554e4a4c7f751c70caa20	36.81.143.5	1543911599	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNTk5Ow==
33153b3c7f01cf498ac0161fd867ea1592be348c	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwOw==
f49d4e9d0faabc6dc72908a6a8ef24604758963e	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwOw==
4f3ed98f9062b621ccae0c55e89aa262a6e03540	36.81.143.5	1543911600	X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTQzOTExNjAwOw==
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: eaudit
--

COPY public.users (id, name, email, username, password, created_at, modified_at, status, avatar, nip, jabatan, golongan, telp, alamat, ttd) FROM stdin;
bf5300ba-29bf-424c-bb09-e97e0e3f07c8	MULYADI, ST  	mulyadi@gmail.com	mulyadi2	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:04:26.733082+00	t	\N	19701121 199803 1 008	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	\N	\N	\N
bfb3be2e-bf5e-40c1-b11f-61bb0b36bc39	ABD. RASYID, S.Pd   	abd@mail.com	abd	c4ca4238a0b923820dcc509a6f75849b	2016-11-07 04:07:38.047093+00	2016-11-07 04:07:38.047093+00	t		19620818 198603 1 021	\N	\N	\N	\N	\N
e024d7dc-a9ed-47b7-919e-1edb6036c3cb	GAMAL AGUS SASONGKO, S.Sos.	agus@gmail.com	agus	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2018-02-20 09:48:30.861095+00	t	\N	19630817 198903 1 017	e3b6c364-9335-4257-8ed1-95c309d8fc78	7b2e4be7-9d87-4145-b1c4-ed362401ff6d	--	--	\N
29f41daa-38fd-43c3-8376-0942617e0fff	WIDI PUJO WICAKSONO, SE	WIDI@gmail.com	WIDI	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:05:02.260866+00	t	\N	19810818 201001 1 024	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	\N	\N	\N
6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	DANNY PRADANA, SE	dannyp@gmail.com	dannyp	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:05:50.309327+00	t	\N	19860811 201001 1 007	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	\N	\N	\N
2a9c364b-035d-4323-a042-7f6c3a5bedf4	FIRLY WULANDARI, SE    	firly@gmail.com	firly	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:05:19.963874+00	t	\N	19840420 201001 2 024	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	\N	\N	\N
e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	FANI WIRASWASTA, SE	fani@gmail.com	fani	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:06:07.055584+00	t	\N	19830713 201101 1 007	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	\N	\N	\N
bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	SYURIAH, A.Md	syuriah@gmail.com	syuriah	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:07:08.41287+00	t	\N	19840130 201001 2 020	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	082331046273	Tapanrejo 2/3, Muncar, Banyuwangi	\N
222cead4-f3bf-48d7-9541-84f60c66cac5	ANIS NURUL LAYLY	anisnurul@gmail.com	anisnurul	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:08:08.384107+00	t	\N	19780112 200901 2 004	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	082 131 513 656	Jl.Ikan Lele no.23 Banyuwangi	\N
70bf9ad1-3775-4be7-abab-9128837416ad	ERFAN ARIFANDI	erfan@gmail.com	erfan	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:08:27.951912+00	t	\N	19820907 201001 1 001	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	\N	\N	\N
ae3234c8-ea30-45d2-a48e-367b8c32567d	DODIK HERMANTO	dodik@gmail.com	dodik	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:10:28.241969+00	t		19751216 201001 100 3	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	0819299381101	Jl Ayani 302	\N
f741041f-732a-481f-bc78-461bc55ada9b	FAIDA NORMAWATI, A.KS, MM	faida@gmail.com	faida	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:03:38.301115+00	t	\N	19741113 200502 2 002	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	\N	\N	\N
6953ba33-d2b4-45a9-9c9b-0faffe456de6	Drs. SHOFWAN THOHARI , M. Si	tohari@gmail.com	tohari	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2018-07-09 23:20:01.640626+00	t	\N	19590425 198303 1 015	\N	2d3504c0-44cf-481f-8324-82f4b48e1fcb	\N	Banyuwangi	015cfc3911c53440b87fdbbedd6a9fa8.jpg
d016fde5-c284-4455-a44a-723ab77fa99f	MURTOJO, S.Sos, MM  	murtojo@gmail.com	murtojo2	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:03:15.729077+00	t	\N	19650511 198903 1 013	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	\N	\N	\N
7a766876-545a-42a4-bb81-ca6fa59efff6	TITIN DESY NOVIANTI, S.Sos	titin@gmail.com	titind	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:06:29.482686+00	t	\N	19831201 201101 2 008	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	\N	\N	\N
ee570f4f-46de-4761-ae5c-b205c92d5c3e	SILVY LUQITASARI, SE	silvy@gmail.com	silvylu	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:06:51.493112+00	t	\N	19850903 201101 2 009	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	\N	\N	\N
dd7ae175-271f-467e-979e-8d2d709b1f0e	YULIATI	yuliati@gmail.com	yuliati	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:07:29.959204+00	t	\N	19780725 200801 2 015	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	\N	\N	\N
0b78c0a2-0548-4ece-b97a-e557bdc882bf	ANDRI PURWANTO, S.Sos, MM	andri@gmail.com	andri	202cb962ac59075b964b07152d234b70	2018-02-20 10:30:05.305595+00	2018-08-31 06:10:09.225998+00	t	\N	\N	\N	\N	\N	\N	\N
a178026b-4e6f-43e7-941e-7559426f2757	SUWIGNYO WIDODO, ST  	suigno@gmail.com	suigno	81dc9bdb52d04dc20036dbd8313ed055	2018-02-20 10:26:44.21904+00	2018-02-22 03:56:22.108315+00	t	\N	19630525 198910 1 002	\N	\N	\N	\N	\N
fe251bf2-8d4f-4205-af0b-51160cd31e63	Super Admin	admin@eauditpr_user.id	sa	c4ca4238a0b923820dcc509a6f75849b	2016-11-11 07:42:59.705111+00	2017-10-04 13:00:28.296292+00	t	8d9c8514b83fabd4c17c8ec276883828.png		\N	\N	\N	\N	09b4dfe67a4a77a516a3787257f23bd1.jpg
55b3d390-5c20-4738-a061-e9435606c883	Merisitiamin	merisitiamin@gmail.com	meri	202cb962ac59075b964b07152d234b70	2018-02-20 10:08:43.996586+00	\N	t	\N	\N	\N	\N	\N	\N	\N
3e7b5d34-9467-4386-8891-d9cd72692094	GATOT SUPRIADI, S.Sos.	gatot@gmail.com	gatot	202cb962ac59075b964b07152d234b70	2018-08-31 06:00:21.895997+00	2018-08-31 06:11:39.9217+00	t	\N	19640721 199403 1 010	\N	\N	\N	\N	\N
e99a0c6e-c0e0-4064-bcfd-69cd08786b6a	Maya Dian	maya@gmail.com	maya	202cb962ac59075b964b07152d234b70	2018-02-20 10:07:03.936729+00	2018-02-22 04:02:32.288523+00	t	\N	\N	\N	\N	\N	\N	\N
b6ffd03c-a5ef-4c89-99a7-446d2545fff8	DANNY PRADANA, SE	a@a.com	danny	202cb962ac59075b964b07152d234b70	2018-08-31 05:58:36.966841+00	2018-08-31 06:10:44.770088+00	t	\N	\N	\N	\N	\N	\N	\N
3cd6fe67-4266-465f-ae5a-d1e60dfd2709	DEVI KURNIAWATI, ST	a@a.com	devi	202cb962ac59075b964b07152d234b70	2018-08-31 05:59:06.792689+00	2018-08-31 06:10:59.584773+00	t	\N	\N	\N	\N	\N	\N	\N
4d519e67-3c64-4b5c-8c62-2c9099eb109e	DRS ANWAR FANANI, M.Pd	anwarfanani@gmail.com	agung	202cb962ac59075b964b07152d234b70	2018-08-31 05:59:49.067832+00	2018-08-31 06:11:24.960354+00	t	\N	\N	\N	\N	\N	\N	\N
709e3413-c729-4435-ae26-eff4cb99f23b	RAHMAD KUSADHANI AMIRUDIN, S.AB, MM	rahmad@gmail.com	rahmad	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:03:59.665224+00	t	\N	19790512 201001 1 018	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	\N	\N	\N
5efd92f0-d96e-4037-ab17-467310a76cd0	REZA PERDANA KUSUMA, SE  	rezaperdana@gmail.com	rezap	202cb962ac59075b964b07152d234b70	2018-08-31 06:01:19.373176+00	2018-08-31 06:12:11.016222+00	t	f4df017d4a4c1d870a43963654f0282d.jpg	19790414 201001 1 016	\N	\N	\N	\N	\N
e03281a2-712d-497a-a86e-7c7e0a425052	FRISCA BUDY ANGGRAENY, SE	friscabudy@gmail.com	frisca	202cb962ac59075b964b07152d234b70	2018-08-31 06:01:48.884205+00	2018-08-31 06:12:22.513509+00	t	\N	19831106 201001 2 013	\N	\N	\N	\N	\N
c93ca7d5-c5c5-46f9-b341-3fde3cbaca23	DAVID HENDRA WIJAYA	davidhendra@gmail.com	davidh	202cb962ac59075b964b07152d234b70	2018-02-20 10:27:38.248541+00	\N	t	\N	19820414 201001 1 001	\N	\N	\N	\N	\N
80cc7a7a-186a-4156-9d57-58d0682e3d36	DEVI KURNIAWATI, ST 	devik@gmail.com	devik	202cb962ac59075b964b07152d234b70	2018-11-12 23:44:44.119776+00	2018-11-12 23:47:51.532304+00	t	\N	19800920 201101 2 003	\N	\N	\N	\N	\N
4ae755a7-3733-4f95-9bbe-02ce3a481fb7	Ir. RETNO FADJAR WINARTI	retno@gmail.com	retno2	202cb962ac59075b964b07152d234b70	2017-07-18 08:20:26.20211+00	2017-09-26 02:02:58.371039+00	t	\N	19691127 199403 2 008	821d7135-b8d2-4945-8c03-7e3b1d2614cd	81dd7d3d-8092-49e3-b63d-a70b978984c5	\N	\N	\N
7afb8c60-b1c5-4dd4-b1e8-5bb98ea44085	DWI NUR RAKHMADI, SE, MM	rakhmadi@gmail.com	rakhmadi	202cb962ac59075b964b07152d234b70	2018-02-20 10:06:16.806863+00	\N	t	\N	19790529 2008001 1 005	\N	\N	\N	\N	\N
4bd27afd-8b72-4c03-b2a0-6d35b42180a3	 NADIA ROSYIDAH, SE   \r\nNADIA ROSYIDAH, SE  	nadia@gmail.com	nadia2	202cb962ac59075b964b07152d234b70	2018-02-20 10:17:45.537738+00	\N	t	\N	19840213 200903 2 002	\N	\N	\N	\N	\N
e571a6b9-809f-4a61-910d-c765dbd55b6a	MARIYA ULFA, ST 	mariya@gmail.com	mariya	202cb962ac59075b964b07152d234b70	2018-02-20 10:11:22.682013+00	\N	t	\N	19830208 201101 2 008	\N	\N	\N	\N	\N
429d2712-60dc-49a3-a3fa-362126f124df	ARI WIBISONO, S.AP	ari@gmail.com	ari2	202cb962ac59075b964b07152d234b70	2018-08-31 06:02:12.287398+00	\N	t	\N	19870707 201101 1 008	\N	\N	\N	\N	\N
90ef9c22-9163-4459-8dde-319cb03dd221	SUPARMAN	suparman@gmail.com	suparman	202cb962ac59075b964b07152d234b70	2018-08-31 06:02:47.412424+00	2018-08-31 06:13:34.820429+00	t	\N	19680815 200604 1 011	\N	\N	\N	\N	\N
ac776057-ba95-4b14-b37a-ba2a9ff8c2b9	IWAN PURWANTO	IWAN@gmail.com	puranto	202cb962ac59075b964b07152d234b70	2018-08-31 06:03:25.120104+00	2018-08-31 06:16:05.83295+00	t	\N	19751216 201001 100 3	\N	\N	\N	\N	\N
24a0fcd4-1c0d-4359-aa3b-f2f0b69ff66d	SETYA AGUS WURYANINGRUM, S.Sos   	setya@gmail.com	setya	202cb962ac59075b964b07152d234b70	2018-02-20 10:14:44.071007+00	\N	t	\N	19630811 199012 2 001	\N	\N	\N	\N	\N
b41048a4-1ff0-4ca1-949a-c8275e10ade8	IMAM AHMADI, S.Sos  	imam@gmail.com	imam3	202cb962ac59075b964b07152d234b70	2018-08-31 06:00:44.832286+00	2018-08-31 06:11:57.408589+00	t	e7af26df0061e03ac2c1dc3011503d4e.jpg	19630525 199003 1 008	\N	\N	081231476800	Jl. Brantas Perum De' Amarylis Garden	b47ce79a7fbba982bd6e268e9129fbf2.jpg
\.


--
-- Data for Name: anggota; Type: TABLE DATA; Schema: support; Owner: eaudit
--

COPY support.anggota (tim, anggota) FROM stdin;
088299c6-bb63-4492-91df-25b3bce47e44	986b958b-1a98-44e4-a59b-c02a15620381
1556c471-4eaf-447e-842c-ba5e46b48392	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b
1556c471-4eaf-447e-842c-ba5e46b48392	9db069b6-02be-46d9-9d45-a89f2d741ec1
ed5f4f37-38e9-4446-8267-cb538d37b881	70bf9ad1-3775-4be7-abab-9128837416ad
ed5f4f37-38e9-4446-8267-cb538d37b881	ac513f02-a71e-4764-b78c-216b62ea590f
f40b930f-b7b3-491e-bc9e-9b1d417ecceb	998cdf4a-6b17-468e-9c07-186e1a724f6d
f40b930f-b7b3-491e-bc9e-9b1d417ecceb	b5d26e6b-30f7-46a3-b7fe-03b66a235d86
570fca4b-20b2-4fff-85c0-0acd63df0722	7a766876-545a-42a4-bb81-ca6fa59efff6
570fca4b-20b2-4fff-85c0-0acd63df0722	89488dc0-00e0-48c8-9e47-1f143b877878
6938edea-22b5-4ded-94dd-298065bb67d4	29f41daa-38fd-43c3-8376-0942617e0fff
6938edea-22b5-4ded-94dd-298065bb67d4	2a9c364b-035d-4323-a042-7f6c3a5bedf4
34bde133-f669-4443-b083-7426b6ed793a	bf5300ba-29bf-424c-bb09-e97e0e3f07c8
34bde133-f669-4443-b083-7426b6ed793a	9d5f96cb-8d85-4216-b60e-809f39620ac6
a9df2aa7-24a6-442b-bb4a-0fd109bfeb16	29fe44b6-4bc3-4de2-b917-2b15713b546d
a9df2aa7-24a6-442b-bb4a-0fd109bfeb16	0188ba46-865b-43b4-9adf-44f6e4fe0cae
\.


--
-- Data for Name: aturan; Type: TABLE DATA; Schema: support; Owner: eaudit
--

COPY support.aturan (id, kode, jenis, sasaran, file, status) FROM stdin;
d06049a9-6ea8-4dd1-ae9c-d6e2237ced21	2	b48074ba-9e37-4e4a-be7a-0d7edc4c69a8	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d	809be96aaeeac0980927c99728d31e78.pdf	t
d63c18d6-0503-474a-8210-29e43dec7d37	12	45e84c28-77d7-4a74-92c9-98087cbee550	fad97553-5c17-4caa-b8a3-01ff6ea39ff0	08c08228c7d9fdc4f25396dd55d3f4e6.pdf	t
83fceaf8-5743-476b-87a5-1133cdc8a235	990001	351c7ddd-6659-48ff-a8d0-af6848770361	69063990-533f-4fd1-9a00-6fef0b273bb2	8040fa910bc1a6afa37c4ac073e4a1e0.pdf	t
0e260ca7-07ee-49dd-8142-1fe01b75daaf	880117	47610b8d-0476-4174-9128-996685a51354	a863e538-ff0c-4df6-a710-ae75058c750f	f9fc9a073249c3c9a02cb0ad1852a007.pdf	t
\.


--
-- Data for Name: kegiatan_kr; Type: TABLE DATA; Schema: support; Owner: eauditpr_user
--

COPY support.kegiatan_kr (id, resiko, kendali_s, kendali_a, urutan) FROM stdin;
24780b97-54b5-485b-bfa7-18459eb72c57	5966c178-dfb8-4433-be4f-ba473231059d	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	1
24780b97-54b5-485b-bfa7-18459eb72c57	5966c178-dfb8-4433-be4f-ba473231059d	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	2
\.


--
-- Data for Name: kegiatan_satker; Type: TABLE DATA; Schema: support; Owner: eauditpr_user
--

COPY support.kegiatan_satker (id, nama, program_satker, resiko, penyebab, jumlah, kendali_s, kendali_a, prosedur_audit, status, periode) FROM stdin;
0680b01d-0b0b-456d-a55a-c62cfbf115d7	kegiatan1	a9f21b87-8af9-463b-9af2-2e19fbeb05bf	5966c178-dfb8-4433-be4f-ba473231059d	94929f69-2a70-43a4-ba9d-8629aa200c49	1000	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	ce8a1712-cacb-44fc-a152-6970afdf4d03	t	\N
d1f55a80-2304-4964-9c7a-7b2be1485ba7	Kegiatan 2	820e63ca-b3a2-4860-86ae-2ad3f7648908	5966c178-dfb8-4433-be4f-ba473231059d	94929f69-2a70-43a4-ba9d-8629aa200c49	13	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	ce8a1712-cacb-44fc-a152-6970afdf4d03	t	7ae49e60-c139-4744-820f-71939ed71eae
24780b97-54b5-485b-bfa7-18459eb72c57	Kegiatan 3.1	820e63ca-b3a2-4860-86ae-2ad3f7648908	\N	\N	2	\N	\N	ce8a1712-cacb-44fc-a152-6970afdf4d03	t	7ae49e60-c139-4744-820f-71939ed71eae
\.


--
-- Data for Name: kejadian_satker; Type: TABLE DATA; Schema: support; Owner: eauditpr_user
--

COPY support.kejadian_satker (id, nama, pk, resiko, penyebab, jumlah, kendali_s, kendali_a, prosedur_audit, status, program_kejadian, hitung, periode) FROM stdin;
5ba927c7-3cae-48c1-b568-40553026ed50	Kejadian Program 1	820e63ca-b3a2-4860-86ae-2ad3f7648908	5966c178-dfb8-4433-be4f-ba473231059d	94929f69-2a70-43a4-ba9d-8629aa200c49	10	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	ce8a1712-cacb-44fc-a152-6970afdf4d03	t	P	t	7ae49e60-c139-4744-820f-71939ed71eae
cace07a9-874e-48d8-a94e-b5aacd0c2e57	Kejadian Kegiatan	d1f55a80-2304-4964-9c7a-7b2be1485ba7	5966c178-dfb8-4433-be4f-ba473231059d	94929f69-2a70-43a4-ba9d-8629aa200c49	22	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	ce8a1712-cacb-44fc-a152-6970afdf4d03	t	K	t	7ae49e60-c139-4744-820f-71939ed71eae
b60dcc23-6ba6-4efe-bf73-1fd6735d4270	Kejadian Tindak Korupsi	\N	5966c178-dfb8-4433-be4f-ba473231059d	\N	3	\N	\N	\N	t	\N	t	7ae49e60-c139-4744-820f-71939ed71eae
393b4c2c-dcbf-4d1f-91c5-d87b3a0fb2d7	Kejadian Program	a9f21b87-8af9-463b-9af2-2e19fbeb05bf	5966c178-dfb8-4433-be4f-ba473231059d	94929f69-2a70-43a4-ba9d-8629aa200c49	21	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	ce8a1712-cacb-44fc-a152-6970afdf4d03	t	P	t	\N
\.


--
-- Data for Name: kertas_kerja; Type: TABLE DATA; Schema: support; Owner: eaudit
--

COPY support.kertas_kerja (id, kode, jenis, sasaran, file, status) FROM stdin;
83fceaf8-5743-476b-87a5-1133cdc8a235	1	45e84c28-77d7-4a74-92c9-98087cbee550	2ed85714-d1fe-444d-8f20-d821b58b8423	c0b2afd6497d0ce18418a95f025b97d1.pdf	t
c297c98f-47fb-4eab-8d23-a81d94461111	18920	8f4380a5-b984-4848-8f99-ff1d92abb004	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d	1fce658e01a397e6c0d0bb9fb1625900.pdf	t
d06049a9-6ea8-4dd1-ae9c-d6e2237ced21	2	45e84c28-77d7-4a74-92c9-98087cbee550	2ed85714-d1fe-444d-8f20-d821b58b8423	809be96aaeeac0980927c99728d31e78.pdf	t
\.


--
-- Data for Name: kode_temuan; Type: TABLE DATA; Schema: support; Owner: eauditpr_user
--

COPY support.kode_temuan (id, k1, k2, k3, k4, nama, level, status, deleted, created_at, created_by, modified_at, modified_by) FROM stdin;
1158d7a2-2b87-4325-ad4e-f24a50430c33	5				Temuan 5	1	t	f	2017-02-11 03:49:54+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N
0923aaae-6502-4812-917b-383cd341830e	6				Temuan 6	1	t	f	2017-02-11 03:49:54+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N
5ec40809-3eba-4239-bfb3-bffd0fa4b8ff	1	31	01	01	testing	4	t	f	2018-04-12 05:58:55+00	\N	2018-04-12 06:15:12+00	fe251bf2-8d4f-4205-af0b-51160cd31e63
a6b741c4-4a3a-49c4-a782-e5b6f51b5bc4	9				Tindak Lanjut	1	t	f	2017-02-11 03:49:54+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N
de403c88-93fb-4073-aea9-38a76216796f	9	01	01		TLL1	3	t	f	2018-04-15 15:07:28+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N
ac6d6bbe-64f2-42fd-a91c-ea0063fab660	1	101			Belanja dan/atau pengadaan barang/jasa fiktif	2	t	f	2018-11-09 01:11:14+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N
df68c876-84e0-4a64-93c6-2123f48e4fba	1	31	01	02	kmk	4	t	t	2018-04-12 06:00:08+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-12 06:19:05+00	fe251bf2-8d4f-4205-af0b-51160cd31e63
f251d5dd-f896-4892-b5b0-d61fec46d7fd	9	01	01	01	TL1	4	t	f	2018-04-15 15:07:38+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N
af19cc09-3175-48ec-a11f-52a6527e31cc	9	01	01	02	TLL2	4	t	f	2018-11-11 10:25:12+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-11-11 10:30:20+00	fe251bf2-8d4f-4205-af0b-51160cd31e63
47b23b0c-b5bb-4557-859a-e85a192f3cda	1	31			Test2	2	t	f	2018-04-12 05:56:31+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-12 06:15:12+00	fe251bf2-8d4f-4205-af0b-51160cd31e63
e8804d00-b5a0-46f0-9752-c21afff8a3e9	1	31	01	02	kimak	4	t	t	2018-04-12 06:00:55+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-04-12 06:18:57+00	fe251bf2-8d4f-4205-af0b-51160cd31e63
8c7c7d72-37c0-4bf0-8406-26129e472712	2				Temuan Kelemahan Sistem Pengendali Intern	1	t	t	2017-02-11 03:49:54+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-11-25 02:49:18+00	fe251bf2-8d4f-4205-af0b-51160cd31e63
df907609-7134-438c-a4fb-e2ca10f716d8	10				test	1	t	f	2018-11-25 03:22:38+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N
937f5207-6f01-4f22-9ed2-b187fa12561f	7				Temuan 7	1	t	t	2017-02-11 03:49:54+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-11-25 04:23:40+00	fe251bf2-8d4f-4205-af0b-51160cd31e63
a9404508-879c-40fd-aaff-6375b33d2a59	8				Temuan 8	1	t	t	2017-02-11 03:49:54+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	2018-11-25 04:23:52+00	fe251bf2-8d4f-4205-af0b-51160cd31e63
c60072ab-94f8-4485-883f-c6116ea8cabd	1				Kode Temuan	1	t	f	2017-02-11 03:49:54+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N
e0fec2f1-0e74-465b-b73d-f3e0e0b3182d	3				Temuan 3E	1	t	f	2017-02-11 03:49:54+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N
daf4be51-36b9-4eeb-ae80-6af795a165ab	4				Temuan 4	1	t	f	2017-02-11 03:49:54+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N
fe58ea62-631b-4266-aa30-78113c93ac3f	1	31	01		test3	3	t	f	2018-04-12 05:57:53+00	\N	2018-04-12 06:15:12+00	fe251bf2-8d4f-4205-af0b-51160cd31e63
887c8185-fc33-4521-b9bd-9aab1ac56ec8	9	01			TL1	2	t	f	2018-04-15 15:07:07+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N
92c7f6f9-bbe8-4baf-ab57-41f2213344d2	11				test	1	t	f	2018-11-25 03:25:21+00	fe251bf2-8d4f-4205-af0b-51160cd31e63	\N	\N
\.


--
-- Data for Name: notif; Type: TABLE DATA; Schema: support; Owner: eaudit
--

COPY support.notif (id, id_user, type, message, reff_id1, reff_id2, created_at, readed_at) FROM stdin;
375dafaa-3d96-461d-a8ee-6d18d411d50d	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Lakukan entry meeting dengan pimpinan obyek	628aabdb-1589-46bc-923b-bf2b57c3fdd6	53983413-54bf-4a94-815e-82ccceecacc3	2017-10-16 10:42:20+00	2017-10-16 22:58:40+00
9425bc3a-5376-487c-ace7-0667e298631b	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 10237 - Apakah Penyerahan akhir pekerjaan dilaksanakan berdasarkan permintaan tertulis dari penyedia; Permintaan penyerahan akhir setelah masa pemeliharaan berakhir; Masa pemeliharaan telah ditentukan dalam syarat syarat khusus kontrak; Apakah Panitia/Pejabat Penerima Hasil Pekerjaan telah melakukan pemeriksaan/pengujian sebelum menerima hasil pekerjaan konstruksi; Apakah Penyedia telah memberikan petunjuk kepada PPK tentang pedoman pengoperasian dan perawatan sesuai dengan SSKK;Apakah PPK menerima penyerahan akhir pekerjaan setelah penyedia melaksanakan semua kewajibannya selama masa pemeliharaan dengan baik;  Tuangkan dalam KKA Kuisioner; Buat Kesimpulan	05275cf2-b282-4653-8c37-a0d7f8923a62	505f7604-ccda-49df-83e7-6f5ef6c9bbb0	2017-10-20 01:36:04+00	\N
f02fd179-5b8f-4c40-87aa-3bcdcf31676e	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 10237 - Apabila penyedia tidak melaksanakan kewajiban pemeliharaan sebagaimana mestinya, maka apakah PPK  menggunakan uang retensi untuk membiayai perbaikan/pemeliharaan atau mencairkan Jaminan Pemeliharaan; Apakah PPK  melakukan pembayaran sisa nilai kontrak yang belum dibayar setelah PPK menrima penyerahan akhir pekerjaan; Apakah PPK  mengembalikan Jaminan Pemeliharaan setelah PPK menrima penyerahan akhir pekerjaan. "Tuangkan dalam KKA Kuisioner; Buat kesimpulan	05275cf2-b282-4653-8c37-a0d7f8923a62	5db3d0f4-1803-444c-97de-d121100a62dc	2017-10-20 01:36:04+00	\N
f2576831-26e5-4597-9401-005b3dcf29d7	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 10237 - Apakah Penyedia Barang/Jasa memberitahukan tentang terjadinya Keadaan Kahar kepada PPK secara tertulis; Apakah Surat pemberitahuan didibuat dalam waktu paling lambat 14 (empat belas) hari kalender sejak terjadinya Keadaan Kahar; Apakah Surat pemberitahuan keadaan kahar disertai salinan pernyataan keadaan kahar yang dikeluarkan oleh pihak/instansi yang berwenang sesuai ketentuan peraturan perundang-undangan; Apakah Keadaan kahar telah sesuai dengan kesepakatan yang dituangkan dalam kontrak; Apakah Pengaruh keadaan kahar, telah dituangkan dalam perubahan Kontrak ; Apakah Penggantian biaya telah diatur dalam suatu adendum Kontrak ; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	05275cf2-b282-4653-8c37-a0d7f8923a62	e32bf68b-6224-4d54-8162-cb96836a27d7	2017-10-20 01:36:04+00	\N
27200db4-f0a9-4cec-8b01-c4815e11a3c0	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 10237 - Susun P2HP, dan bahas dengan obrik	05275cf2-b282-4653-8c37-a0d7f8923a62	5250d9b9-8156-4a02-ad3b-dde51a608188	2017-10-20 01:36:04+00	\N
57943fb4-2a7c-4806-ba7d-9a021fec603b	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 10237 - Susun draft laporan, dan bahas dengan internal Tim	05275cf2-b282-4653-8c37-a0d7f8923a62	47ea222d-05f8-4b95-a130-3e7bb6453ff0	2017-10-20 01:36:04+00	\N
96575aa4-cb46-4fd8-869a-73f203599320	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 10237 - Lakukan scan laporan final, dan upload dalam aplikasi	05275cf2-b282-4653-8c37-a0d7f8923a62	57ba24b4-bde1-4749-88ce-ad7e5e658c34	2017-10-20 01:36:04+00	\N
7421e6e0-e3f4-4582-9c54-97cb7e3b1135	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 10236 - Teliti apakah ada perubahan pekerjaan; Jika ada perubahan apakah telah didukung dengan dokumen yang lengkap; Tuangkan dalam KKA kuisioner; Buat kesimpulan	f79b7985-23bb-4156-a074-bc23365ded79	61c0607b-868f-47af-a90d-dde0c42a3af4	2017-10-20 02:49:56+00	\N
7ea5302f-b347-4d46-971b-070eaf563394	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 10236 - Teliti apakah Penyedia telah menyampaikan laporan kemajuan hasil pekerjaan; Apakah Laporan kemajuan hasil pekerjaan telah diverifikasi PPK; Teliti apakah Kemajuan hasil pekerjaan yang dilaporkan telah sesuai dengan kemajuan pekerjaan fisik dilapangan; Teliti apakah Spesifikasi teknis hasil pekerjaan yang terpasang telah sesuai dengan kontrak;  Tuangkan dalam KKA Kuisioner; Buat kesimpulan	f79b7985-23bb-4156-a074-bc23365ded79	1b72e119-165e-401c-908c-83b0438780e9	2017-10-20 02:49:56+00	\N
f03bc943-65a7-427f-827a-1359f61bf2d6	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 10236 - Apakah Penyedia telah mengajukan tagihan disertai laporan kemajuan hasil pekerjaan; Untuk Kontrak yang mempunyai subkontrak, apakah permintaan pembayaran telah dilengkapi bukti pembayaran kepada seluruh sub penyedia sesuai dengan prestasi pekerjaan;  Apakah Pembayaran penyedia kepada sub kontraktor telah sesuai dengan pekerjaan yang terpasang; Apakah Pembayaran dilakukan dengan sistem termin sesuai ketentuan dalam Dokumen Kontrak; Apakah Pembayaran dilakukan senilai pekerjaan yang telah terpasang, tidak termasuk bahan/material dan peralatan yang ada di lokasi pekerjaan; Apakah Pembayaran termin telah dipotong angsuran uang muka, denda(bila ada); Tuangkan dalam KKA Kuisioner; Buat kesimpulan 	f79b7985-23bb-4156-a074-bc23365ded79	64ecb1a9-91f0-4b16-8360-11da3e655574	2017-10-20 02:49:56+00	\N
bea83333-c1fc-4726-9b02-0c3275f8f6dc	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 10236 - Apakah PPK melakukan penelitian usulan tertulis perpanjangan waktu penyelesaian kontrak oleh Penyedia; Apakah Perpanjangan waktu pelaksanaan kontrak telah berdasarkan pertimbangan yang layak dan wajar oleh PPK, karena :( pekerjaan tambah;  - perubahan disain;  - keterlambatan yang disebabkan oleh PPK;  "- masalah yang timbul diluar kendali penyedia; dan/atau " - Keadaan Kahar) ; Apakah Penyedia telah memberikan peringatan dini akan terjadi keterlambatan dan telah bekerja sama untuk bekerja sama untuk mencegah keterlambatan; Apakah PPK telah menetapkan ada tidaknya perpanjangan, dalam jangka waktu 21 (dua puluh satu) hari setelah penyedia meminta perpanjangan; Apakah Penetapan ada/tidak perpanjangan pelaksanaan dinyatakan secara tertulis; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	f79b7985-23bb-4156-a074-bc23365ded79	8d53ae6e-9502-4103-8603-ecd615b32d94	2017-10-20 02:49:56+00	\N
2f062a52-c3da-4c5c-acc9-c1a6486a3f72	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 10236 - Apakah Penyerahan hasil pekerjaan berdasarkan permintaan secara tertulis dari Penyedia Pekerjaan Konstruksi; Apakah Permintaan tersebut setelah pekerjaan selesai 100 % ; Apakah Panitia/Pejabat Penerima Hasil Pekerjaan melakukan pemeriksaan hasil pekerjaan Pengadaan Barang/Jasa; Apakah Melakukan pemeriksaan sesuai dengan pedoman/prosedur yang telah ditetapkan; Apakah Penyedia telah melaksanakan pekerjaan konstruksi sesuai dengan ketentuan yang tercantum dalam Kontrak dan gambar; Apakah Apabila terdapat ketidaksesuaian dengan kontrak (kekurangan-kekurangan dan/atau cacat hasil pekerjaan), penyedia telah memperbaiki/menyelesaikannya; Apakah panitia/Pejabat Penerima Hasil Pekerjaan menerima hasil Pengadaan Barang/Jasa setelah melalui pemeriksaan/pengujian; Apakah PPK menerima penyerahan pertama pekerjaan setelah seluruh hasil pekerjaan dilaksanakan sesuai dengan ketentuan Kontrak dan diterima oleh Panitia/Pejabat Penerima Hasil Pekerjaan; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	f79b7985-23bb-4156-a074-bc23365ded79	6b69d2bd-631b-423a-a5c4-15d7ba2adb89	2017-10-20 02:49:56+00	\N
b03fb1a1-7843-4b5c-9dd2-49748e0dda54	0188ba46-865b-43b4-9adf-44f6e4fe0cae	rencana	Anda ditugaskan pada 10236 - Teliti apakah telah dilakukan pengawasan mutu pelaksanaan kegiatan; Tuangkan dalam KKA kuisioner ; Buat kesimpulan	f79b7985-23bb-4156-a074-bc23365ded79	823a2b84-ca0e-42a6-9792-1c1b19d3dfb6	2017-10-20 02:49:56+00	2017-10-20 02:55:11+00
ced8c4a2-25a6-4e39-b330-454665e3ad18	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 10236 - Teliti pembayaran Uang muka apakah sesuai klausul dan rencana penggunaan penggunaan; isikan dalam KKA kuisioner; Buat kesimpulan	f79b7985-23bb-4156-a074-bc23365ded79	8a6bbfba-798e-4593-9d57-d0d5a88b2c4a	2017-10-20 02:49:56+00	2017-10-20 03:16:18+00
ca15e850-f62b-43c5-931b-20876d82b204	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 9999 - Dapatkan data pendapatan dan belanja sekolah	7ee14d19-aa59-4403-b0c1-85a7a0560da4	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	2017-10-17 02:16:53+00	\N
ffcaefd2-f54f-4b24-9b8d-a905f2d31657	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 9999 - Dapatkan data realisasi penerimaan selain BOS; Dapatkan dasar penerimaan tersebut; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	acd1709e-5bb3-436c-b956-d1cfebc17b65	2017-10-17 02:16:53+00	\N
d228e0e1-3592-4de9-985a-418991e21c88	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 9999 - Teliti catatan dalam buku kas, apakah item pengeluaran telah tercantum dalam RKAS 	7ee14d19-aa59-4403-b0c1-85a7a0560da4	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	2017-10-17 02:16:53+00	\N
c2e6c1f7-f272-47c0-a279-9d593546d41f	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 9999 - Dapatkan data analisis kebutuhan; Dapatkan rencana pengadaan tahun berjalan; buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	2017-10-17 02:16:53+00	\N
7ea0d5a3-1931-4ede-8764-7cfd88e9b0ed	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 9999 - Dapatkan data pengadaan aset sekolah; Lakukan pengujian terhadap proses pengadaan; Bandingkan dengan peraturan pengadaan barang/jasa pemerintah; Buat Kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	336014ad-e1cd-41c4-bf97-b8438b78b12f	2017-10-17 02:16:53+00	\N
dc17e2bb-33b3-497d-8140-ada68fb3ffa2	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 9999 - Dapatkan daftar barang yang diadakan sekolah beserta harganya; Bandingkan dengan harga pasar (katalog LKPP); Lakukan klarifikasi jika ada perbedaan; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	149643dc-f2b1-4426-8f69-013f48e4d7bd	2017-10-17 02:16:53+00	\N
df2a4fe2-ecc1-48a0-a474-376de9300b6b	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 9999 - Dapatkan data hasil opname fisik aset; Jika tidak diperoleh data, lakukan klarifikasi kepada petugas barang; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	2017-10-17 02:16:53+00	\N
3c5d530d-9f5b-44e3-b811-4aa635e96905	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Dapatkan data aset; lakukan cek fisik secara sampling terhadap keberadaan barang; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	470396d6-b06a-4147-b47b-7d55859ce577	2017-10-17 02:16:53+00	\N
0e7f98a1-e234-4082-a841-eba71ead0eb7	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Teliti pengeluaran yang menjadi obyek pajak; lakukan pengecekan apakah sudah dikenakan pajak sesuai tarif; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	008749c7-efb0-47c7-b54d-6a2503ba2ebd	2017-10-17 02:16:53+00	\N
afa1a4d2-3698-4ca9-a857-4480eb1e5c54	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Dapatkan data pajak yang dipungut; Lakukan pengecekan terhadap pajak yang telah dipungut seluruhnya telah disetor; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	53449178-793e-47f9-892d-ac58f1375831	2017-10-17 02:16:53+00	\N
e186dd94-ea33-478c-9ecc-b72a10251c31	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 9999 - Dapatkan data penyetoran pajak; Lakukan pengujian apakah ada pajak yang terlambat disetor sesuai ketentuan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	37bab70a-0dd2-4786-9c24-9542445e738d	2017-10-17 02:16:53+00	\N
433129c0-f4c2-4066-ae28-d2fe68ddb01f	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 9999 - Buat Daftar Temuan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	49f6c09c-485d-458b-aae9-c3ff883f4542	2017-10-17 02:16:53+00	\N
3959107a-325d-46ff-92e7-8c8bdc46c17c	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Buat Pemberitahuan Hasil Pengawasan/pemeriksaan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	04b0f150-6dca-4d93-b29f-3eef3a302281	2017-10-17 02:16:53+00	\N
3dee8341-c239-4a51-9045-8beffdc5dcb4	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Buat kuisioner kepuasan steakholder dan pernyataan independensi auditor	7ee14d19-aa59-4403-b0c1-85a7a0560da4	45d56475-668b-41e4-bdc0-3634c76f35a8	2017-10-17 02:16:53+00	\N
340c4774-0bb4-4837-bd57-ac79769453b2	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Dapatkan RKAS; Teliti apakah RKAS telah disusun sesuai pedoman; Buat Kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	c7aa0a46-3ae1-4c52-a708-98c17cec316e	2017-10-17 02:16:53+00	2017-10-19 01:12:54+00
7e5df593-76d0-448d-9ddd-7ead7b6cd9cf	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada  - 1	4ac7b90f-d4ad-4bba-bad1-94493366203d	1036dac7-ccb6-47fd-a958-5a5c19f3df62	2017-10-16 10:42:20+00	\N
cb8a9298-4c60-4b3c-9696-6f190e88e4b3	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Lakukan pengujian terhadap kegiatan; Dapatkan standar harga kegiatan (SBU);bandingkan biaya kegiatan dengan SBU yang berlaku; Buat simpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	2017-10-17 02:16:53+00	2017-10-19 01:33:07+00
c38b8289-e40f-4e0d-878b-ad784526f40c	f741041f-732a-481f-bc78-461bc55ada9b	rencana	Anda ditugaskan pada 9999 - Lakukan entry meeting dengan pimpinan obyek	7ee14d19-aa59-4403-b0c1-85a7a0560da4	53983413-54bf-4a94-815e-82ccceecacc3	2017-10-17 02:16:53+00	2017-10-20 01:29:32+00
cb1bcc45-6143-4139-8333-8f3b9eaed33d	f741041f-732a-481f-bc78-461bc55ada9b	rencana	Anda ditugaskan pada 9999 - Dapatkan data total penerimaan BOS tiap tahun ajaran	7ee14d19-aa59-4403-b0c1-85a7a0560da4	e7a5563b-215a-44a1-a523-a1e2f47cb694	2017-10-17 02:16:53+00	2017-10-20 01:30:14+00
be98c331-1f3a-4189-a08c-a73a99f26fb6	f741041f-732a-481f-bc78-461bc55ada9b	rencana	Anda ditugaskan pada 9999 - Dapatkan data perhitungan penerimaan BOS dan bandingkan dengan jumlah siswa yang terdaftar	7ee14d19-aa59-4403-b0c1-85a7a0560da4	62fc3064-8e6a-4e97-a185-fb1156c0c16a	2017-10-17 02:16:53+00	2017-10-20 01:30:37+00
99805b44-eb09-4add-8ec0-082f373441b1	f741041f-732a-481f-bc78-461bc55ada9b	rencana	Anda ditugaskan pada 9999 - Dapatkan data pengadaan barang; lakukan cek fisik apakah telah sesuai spesifikasi dan telah dimanfaatkan; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	dc315220-3e80-4d7a-841e-dda52b0a1f84	2017-10-17 02:16:53+00	2017-10-20 01:30:45+00
3a3f371c-35e9-44f3-aad6-5964d1124a0a	f741041f-732a-481f-bc78-461bc55ada9b	rencana	Anda ditugaskan pada 9999 - Dapatkan dokumen pengeluaran/belanja; lakukan pengecekan terhadap barang yang dibeli; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	2017-10-17 02:16:53+00	2017-10-20 01:30:57+00
c67d83f7-36a0-467a-8b72-8d8b1c439232	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 9999 - Lakukan survey terbatas IC	7ee14d19-aa59-4403-b0c1-85a7a0560da4	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2017-10-17 02:16:53+00	2017-10-20 03:20:13+00
3b059b5f-cb34-4a0d-b2d7-3331fabb059b	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 9999 - Dapatkan data aset sekolah	7ee14d19-aa59-4403-b0c1-85a7a0560da4	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	2017-10-17 02:16:53+00	2017-10-20 03:28:53+00
5e25516e-60e3-4c32-b4d7-115dcc6357d8	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 9999 - Dapatkan data penerimaan ; Bandingkan dengan jumlah angka di laporan; buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	4453d1e6-05ed-4374-8344-a23915bf425b	2017-10-17 02:16:53+00	2017-10-20 03:30:55+00
3db69ce1-c25b-442b-8b70-475e578d0342	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 9999 - Dapatkan catatan pengeluaran; lakukan pengecekan terhadap bukti pendukung, apakah pengeluaran didukung oleh bukti yang sah ; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	c90bd32b-1b3f-4952-ad96-d498e6b56364	2017-10-17 02:16:53+00	2017-10-20 03:34:01+00
c91f5654-5240-4dcd-b3ff-b63bfe6189f9	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 9999 - Dapatkan data siswa tiap tahun ajaran	7ee14d19-aa59-4403-b0c1-85a7a0560da4	b7689c74-5a86-4711-b170-6deb99f7137b	2017-10-17 02:16:53+00	2017-10-25 00:55:53+00
8af85bb5-385a-43da-9ea6-3aa26a6eddc5	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Dapatkan data pengeluaran kas; Lakukan konfirmasi atas hasil kegiatan, dokumen dan data pendukung pengeluaran; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	5cbb9528-fa19-449b-a05f-ed0abfc36a00	2017-10-17 02:16:53+00	2017-12-22 06:44:44+00
e097b85c-609e-49e9-9ff9-194a999efe93	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Dapatkan daftar aset; Lakukan pengujian terhadap mutasi tambah dan kurang; Cek apakah seluruh mutasi telah dicatat; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	5d03a7f2-fbdc-4dcb-becd-75c08489febd	2017-10-17 02:16:53+00	2017-12-22 07:16:32+00
f237efc5-86ff-4167-9994-83c27d3469bd	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Lakukan pembahasan internal tim	7ee14d19-aa59-4403-b0c1-85a7a0560da4	64722e98-772e-4a28-94c3-03d3cb483320	2017-10-17 02:16:53+00	2017-12-22 07:38:42+00
04d53916-f39b-4c93-bf62-dc6f2cb62537	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Dapatkan daftar aset; Lakukan pengujian apakah seluruh pengadaan telah dicatat dalam daftar aset; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	4cdccf23-dc54-4583-b560-8ae93efcd3e8	2017-10-17 02:16:53+00	2018-01-18 03:40:46+00
f8ef17dd-7384-4309-ac28-a07e35639a54	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Dapatkan data aset yang digunakan untuk operasional sekolah; Lakukan konfirmasi atau wawancara apakah terdapat aset hibah yang tidak dicatat dalam aset sekolah; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	52cc2b91-fe8b-4097-9d9e-3f81a889153f	2017-10-17 02:16:53+00	2018-02-06 13:07:16+00
21706805-dc11-467c-826c-a920442b2a25	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 9999 - Dapatkan data laporan periodik yang dibuat sekolah	7ee14d19-aa59-4403-b0c1-85a7a0560da4	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	2017-10-17 02:16:53+00	2018-06-25 04:15:14+00
d8d1548d-5d87-42d9-9548-626cfc364340	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Lakukan pembahasan Intern Pemberitahuan Hasil Pemeriksaan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	fb1b3834-b5d2-4599-8273-a49881db8d36	2017-10-17 02:16:53+00	\N
89619624-b41e-4e90-990e-2594f3307e7e	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Susun draft laporan hasil pengawasan/pemeriksaan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	313a30e0-7f5b-4b15-ac3f-708e03a3c990	2017-10-17 02:16:53+00	\N
4fa686ca-0f1f-4443-a555-5bc6a69b06e1	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Lakukan reviu terhadap konsep Laporan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	72eed5d9-87a3-424d-abbf-e74972466494	2017-10-17 02:16:53+00	\N
9b4dbb3f-f08f-4501-9d2f-fdbdbf81f5ff	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Lakukan scan laporan final dalam bentuk pdf, dan upload di aplikasi	7ee14d19-aa59-4403-b0c1-85a7a0560da4	06709067-0f75-4b63-96b3-47b501e4674c	2017-10-17 02:16:53+00	\N
73727200-ddba-4c82-a916-7e739ad36d02	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Lakukan pengujian terhadap kegiatan; Dapatkan standar harga kegiatan (SBU);bandingkan biaya kegiatan dengan SBU yang berlaku; Buat simpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	2017-10-16 10:42:20+00	2017-10-20 01:43:11+00
97878934-0aa0-4a47-a3d6-e1d3122fae17	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 10236 - Apakah Penyerahan akhir pekerjaan dilaksanakan berdasarkan permintaan tertulis dari penyedia; Permintaan penyerahan akhir setelah masa pemeliharaan berakhir; Masa pemeliharaan telah ditentukan dalam syarat syarat khusus kontrak; Apakah Panitia/Pejabat Penerima Hasil Pekerjaan telah melakukan pemeriksaan/pengujian sebelum menerima hasil pekerjaan konstruksi; Apakah Penyedia telah memberikan petunjuk kepada PPK tentang pedoman pengoperasian dan perawatan sesuai dengan SSKK;Apakah PPK menerima penyerahan akhir pekerjaan setelah penyedia melaksanakan semua kewajibannya selama masa pemeliharaan dengan baik;  Tuangkan dalam KKA Kuisioner; Buat Kesimpulan	f79b7985-23bb-4156-a074-bc23365ded79	505f7604-ccda-49df-83e7-6f5ef6c9bbb0	2017-10-20 02:49:56+00	\N
7a79a8f2-59ba-496e-a5a8-3ade7191aea5	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 10236 - Apabila penyedia tidak melaksanakan kewajiban pemeliharaan sebagaimana mestinya, maka apakah PPK  menggunakan uang retensi untuk membiayai perbaikan/pemeliharaan atau mencairkan Jaminan Pemeliharaan; Apakah PPK  melakukan pembayaran sisa nilai kontrak yang belum dibayar setelah PPK menrima penyerahan akhir pekerjaan; Apakah PPK  mengembalikan Jaminan Pemeliharaan setelah PPK menrima penyerahan akhir pekerjaan. "Tuangkan dalam KKA Kuisioner; Buat kesimpulan	f79b7985-23bb-4156-a074-bc23365ded79	5db3d0f4-1803-444c-97de-d121100a62dc	2017-10-20 02:49:56+00	\N
0c07d5e6-04a4-4a9c-9053-da679a048bad	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 10236 - Apakah Penyedia Barang/Jasa memberitahukan tentang terjadinya Keadaan Kahar kepada PPK secara tertulis; Apakah Surat pemberitahuan didibuat dalam waktu paling lambat 14 (empat belas) hari kalender sejak terjadinya Keadaan Kahar; Apakah Surat pemberitahuan keadaan kahar disertai salinan pernyataan keadaan kahar yang dikeluarkan oleh pihak/instansi yang berwenang sesuai ketentuan peraturan perundang-undangan; Apakah Keadaan kahar telah sesuai dengan kesepakatan yang dituangkan dalam kontrak; Apakah Pengaruh keadaan kahar, telah dituangkan dalam perubahan Kontrak ; Apakah Penggantian biaya telah diatur dalam suatu adendum Kontrak ; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	f79b7985-23bb-4156-a074-bc23365ded79	e32bf68b-6224-4d54-8162-cb96836a27d7	2017-10-20 02:49:56+00	\N
b7e069e9-592a-4b32-ba2a-fe3664e2b917	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 10236 - Susun draft laporan, dan bahas dengan internal Tim	f79b7985-23bb-4156-a074-bc23365ded79	47ea222d-05f8-4b95-a130-3e7bb6453ff0	2017-10-20 02:49:56+00	\N
2d1978d1-114e-436c-bf11-8fcfe6ce43af	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 10236 - Lakukan scan laporan final, dan upload dalam aplikasi	f79b7985-23bb-4156-a074-bc23365ded79	57ba24b4-bde1-4749-88ce-ad7e5e658c34	2017-10-20 02:49:56+00	\N
e7804986-69d9-4a8d-acc8-b48a8ee87f37	0188ba46-865b-43b4-9adf-44f6e4fe0cae	rencana	Anda ditugaskan pada 10236 - Apakah Pembayaran terakhir dilakukan setelah : - Pekerjaan selesai 100% (seratus perseratus)  "- Berita acara penyerahan pertama pekerjaan diterbitkan; Apakah Pembayaran yang dilakukan sebesar 95% (sembilan puluh lima perseratus) dari nilai kontrak, sedangkan yang 5% (lima perseratus) merupakan retensi selama masa pemeliharaan," "Pembayaran yang dilakukan sebesar 100% (seratus perseratus) dari nilai kontrak dan penyedia harus menyerahkan Jaminan Pemeliharaan sebesar 5% (lima perseratus) dari nilai kontrak" "Masa berlaku jaminan pemeliharaan sejak tanggal serah terima pertama sampai dengan tanggal penyerahan akhir pekerjaan," "Telah dipotong denda (jika ada), pajak,uang muka dan ganti rugi (jika ada), " "Pembayaran ganti rugi kepada penyedia ( jika ada) sesuai kontrak, " "Ganti rugi dibayarkan berdasarkan data penunjang dan perhitungan kompensasi yang diajukan oleh penyedia kepada PPK, telah dapat dibuktikan kerugian nyata akibat Peristiwa Kompensasi" "Penyedia telah memberikan peringatan dini akan terjadi pengeluaran biaya tambahan dan telah bekerja sama untuk mencegah pengeluaran biaya tambahan," "Besarnya denda, pajak, uang muka sesuai dengan kontrak, " "Jika pekerjaan tidak selesai karena kesalahan atau kelalaian penyedia maka penyedia dikenakan denda. " "Besarnya denda 1/1000 dari sisa harga bagian kontrak yang belum dikerjakan, bila pekrjaan yang telah dilaksanakan dapat berfungsi," Jika pekerjaan yang telah dilaksanakan belum  berfungsi, besarnya denda 1/1000 dari harga kontrak; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	f79b7985-23bb-4156-a074-bc23365ded79	fd3066d2-c8c0-460e-a52c-790cd50b7469	2017-10-20 02:49:56+00	2017-10-20 02:54:50+00
95b8ad78-19bf-4dd1-81ae-a54b4a112925	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada  - 2	4ac7b90f-d4ad-4bba-bad1-94493366203d	a5d15d37-4b03-4f9c-95b0-a01f970891f7	2017-10-16 10:42:20+00	2017-10-25 01:39:24+00
104e5691-f600-4f4d-b57c-b6c182f395ac	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 77777 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	81318580-6a2e-4c70-9b37-0662a9898536	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2018-04-12 10:04:28+00	\N
e4430dc6-0665-4e31-861b-3d9e70d7de17	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada G-0192 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	89dfef89-b49a-4ca2-9aa8-d7edc8f9e2b9	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2018-04-16 06:32:42+00	\N
68806441-b157-445d-a4dd-2849bbd7166b	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada G-0192 - Buat permintaan data yang terkait dengan penugasan	89dfef89-b49a-4ca2-9aa8-d7edc8f9e2b9	80636594-116a-44e8-b4be-e8b064986a06	2018-04-16 06:32:42+00	\N
223e9a65-9061-4699-9f79-ffb087bc5acb	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 7009 - Lakukan entry meeting dengan pimpinan obyek	bf117ea8-6a7b-40c3-9d6c-1fc9761e85f5	53983413-54bf-4a94-815e-82ccceecacc3	2018-08-31 06:55:40+00	2018-08-31 07:11:44+00
9c9f0359-4474-434b-98c1-ce7f84b54618	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 70089 - Lakukan survey terbatas IC	7a0547b1-b065-42bf-a898-6228ef56fe5a	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2018-08-31 07:23:12+00	2018-08-31 07:25:24+00
8fb0432e-f8bd-429e-9515-622b50e382b5	f741041f-732a-481f-bc78-461bc55ada9b	rencana	Anda ditugaskan pada 212 - Lakukan entry meeting dengan pimpinan obyek	c7f2b0c5-f32b-443c-ab9e-bebaf0a701ad	53983413-54bf-4a94-815e-82ccceecacc3	2018-08-31 07:31:30+00	\N
84bf5d7d-075b-4764-96bc-a51a041a665a	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 212 - Lakukan survey terbatas IC	c7f2b0c5-f32b-443c-ab9e-bebaf0a701ad	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2018-08-31 07:31:30+00	\N
7f6c3780-7b2e-4076-8881-4850394817dd	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 212 - Buat permintaan data yang terkait dengan penugasan	c7f2b0c5-f32b-443c-ab9e-bebaf0a701ad	51dd4682-183d-4989-84c2-18baefbd4539	2018-08-31 07:31:30+00	\N
2b8c88f6-8457-4bdf-8e6a-eb56ffb3551c	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 003/andre/425.302/2018 - Dapatkan SK Kepala Sekolah tentang Tim BOS Sekolah dan periksa apakah susunan Tim telah sesuai dengan pedoman	07b42dd0-4867-4d42-b506-dc241d85c262	2dc5180a-6ecb-40db-a6e9-742109c4e82e	2018-08-31 07:41:36+00	\N
85e00a0a-597d-4895-8cf3-e35d050cc969	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 003/andre/425.302/2018 - Periksa apakah susunan tim sesuai dengan pedoman	07b42dd0-4867-4d42-b506-dc241d85c262	28d7e52a-59e8-4a1e-b43f-4a48cdca3e22	2018-08-31 07:41:36+00	\N
5a95b726-8a73-4d76-a208-72df0de16fd0	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 003/andre/425.302/2018 - Buat kuisioner kepuasan steakholder dan pernyataan independensi auditor	07b42dd0-4867-4d42-b506-dc241d85c262	45d56475-668b-41e4-bdc0-3634c76f35a8	2018-08-31 07:41:36+00	\N
6f5ca48e-c4db-45d1-932c-78b391d22433	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Buat permintaan data yang terkait dengan penugasan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	51dd4682-183d-4989-84c2-18baefbd4539	2017-10-17 02:16:53+00	2017-10-17 02:18:02+00
fb6df38b-088a-4c0e-b4f6-a91734e9ce72	ee570f4f-46de-4761-ae5c-b205c92d5c3e	rencana	Anda ditugaskan pada 10238 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2017-10-20 01:44:48+00	\N
25a4d331-3c87-4d34-8235-8d7275167c26	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 10238 - Buat permintaan data yang terkait dengan penugasan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	80636594-116a-44e8-b4be-e8b064986a06	2017-10-20 01:44:48+00	\N
347f556a-5ed4-45cf-9d76-266f2154815b	ee570f4f-46de-4761-ae5c-b205c92d5c3e	rencana	Anda ditugaskan pada 10238 - Teliti uraian, isi dan kelengkapan formal kontrak; Isikan dalam kuisioner di Form KKA; Buat kesimpulan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	c5c56164-40c4-43fa-8495-dc5165718ebf	2017-10-20 01:44:48+00	\N
b090b07a-d628-4371-b317-c1e686d8f749	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 10238 - Lakukan pengujian terhadap Jadwal mobilisasi, Pemeriksaan bersama lokasi kerja; Teliti kegiatan Penyerahan lokasi ; Teliti Program mutu dari penyedia ; Teliti ketepatan waktu penerbitan SPMK; Isikan dalam formulir kuisioner; Buat kesimpulan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	493f29c3-5db5-470d-88a1-d50c04a963a6	2017-10-20 01:44:48+00	\N
662c033e-966e-4f5f-b3f4-0847cd7c8ae3	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 10238 - Teliti pembayaran Uang muka apakah sesuai klausul dan rencana penggunaan penggunaan; isikan dalam KKA kuisioner; Buat kesimpulan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	8a6bbfba-798e-4593-9d57-d0d5a88b2c4a	2017-10-20 01:44:48+00	\N
2b214287-efc2-45d8-b9ab-f5b221bf24ad	ee570f4f-46de-4761-ae5c-b205c92d5c3e	rencana	Anda ditugaskan pada 10238 - Teliti apakah telah dilakukan pemantauan pelaksanaan pekerjaan; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	30ef6912-451f-4c64-ac71-92a4bf5736c9	2017-10-20 01:44:48+00	\N
99f6bde5-d21b-4093-8b70-56794b966a5d	ee570f4f-46de-4761-ae5c-b205c92d5c3e	rencana	Anda ditugaskan pada 10238 - Teliti apakah telah dilakukan pengawasan mutu pelaksanaan kegiatan; Tuangkan dalam KKA kuisioner ; Buat kesimpulan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	823a2b84-ca0e-42a6-9792-1c1b19d3dfb6	2017-10-20 01:44:48+00	\N
6e731c2d-f746-4be5-8a97-2e92648242b4	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 10238 - Teliti apakah Penyedia telah menyampaikan laporan kemajuan hasil pekerjaan; Apakah Laporan kemajuan hasil pekerjaan telah diverifikasi PPK; Teliti apakah Kemajuan hasil pekerjaan yang dilaporkan telah sesuai dengan kemajuan pekerjaan fisik dilapangan; Teliti apakah Spesifikasi teknis hasil pekerjaan yang terpasang telah sesuai dengan kontrak;  Tuangkan dalam KKA Kuisioner; Buat kesimpulan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	1b72e119-165e-401c-908c-83b0438780e9	2017-10-20 01:44:48+00	\N
4472ac76-6d91-4b9d-86b0-3680f41f0ad0	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 10238 - Apakah Penyedia telah mengajukan tagihan disertai laporan kemajuan hasil pekerjaan; Untuk Kontrak yang mempunyai subkontrak, apakah permintaan pembayaran telah dilengkapi bukti pembayaran kepada seluruh sub penyedia sesuai dengan prestasi pekerjaan;  Apakah Pembayaran penyedia kepada sub kontraktor telah sesuai dengan pekerjaan yang terpasang; Apakah Pembayaran dilakukan dengan sistem termin sesuai ketentuan dalam Dokumen Kontrak; Apakah Pembayaran dilakukan senilai pekerjaan yang telah terpasang, tidak termasuk bahan/material dan peralatan yang ada di lokasi pekerjaan; Apakah Pembayaran termin telah dipotong angsuran uang muka, denda(bila ada); Tuangkan dalam KKA Kuisioner; Buat kesimpulan 	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	64ecb1a9-91f0-4b16-8360-11da3e655574	2017-10-20 01:44:48+00	\N
1f2f558d-011a-40c7-990d-fc3e392865ef	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 10238 - Apakah PPK melakukan penelitian usulan tertulis perpanjangan waktu penyelesaian kontrak oleh Penyedia; Apakah Perpanjangan waktu pelaksanaan kontrak telah berdasarkan pertimbangan yang layak dan wajar oleh PPK, karena :( pekerjaan tambah;  - perubahan disain;  - keterlambatan yang disebabkan oleh PPK;  "- masalah yang timbul diluar kendali penyedia; dan/atau " - Keadaan Kahar) ; Apakah Penyedia telah memberikan peringatan dini akan terjadi keterlambatan dan telah bekerja sama untuk bekerja sama untuk mencegah keterlambatan; Apakah PPK telah menetapkan ada tidaknya perpanjangan, dalam jangka waktu 21 (dua puluh satu) hari setelah penyedia meminta perpanjangan; Apakah Penetapan ada/tidak perpanjangan pelaksanaan dinyatakan secara tertulis; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	8d53ae6e-9502-4103-8603-ecd615b32d94	2017-10-20 01:44:48+00	\N
ae296cb4-e1ea-4883-973b-f20deaf7aff6	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 10238 - Apakah Penyerahan hasil pekerjaan berdasarkan permintaan secara tertulis dari Penyedia Pekerjaan Konstruksi; Apakah Permintaan tersebut setelah pekerjaan selesai 100 % ; Apakah Panitia/Pejabat Penerima Hasil Pekerjaan melakukan pemeriksaan hasil pekerjaan Pengadaan Barang/Jasa; Apakah Melakukan pemeriksaan sesuai dengan pedoman/prosedur yang telah ditetapkan; Apakah Penyedia telah melaksanakan pekerjaan konstruksi sesuai dengan ketentuan yang tercantum dalam Kontrak dan gambar; Apakah Apabila terdapat ketidaksesuaian dengan kontrak (kekurangan-kekurangan dan/atau cacat hasil pekerjaan), penyedia telah memperbaiki/menyelesaikannya; Apakah panitia/Pejabat Penerima Hasil Pekerjaan menerima hasil Pengadaan Barang/Jasa setelah melalui pemeriksaan/pengujian; Apakah PPK menerima penyerahan pertama pekerjaan setelah seluruh hasil pekerjaan dilaksanakan sesuai dengan ketentuan Kontrak dan diterima oleh Panitia/Pejabat Penerima Hasil Pekerjaan; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	6b69d2bd-631b-423a-a5c4-15d7ba2adb89	2017-10-20 01:44:48+00	\N
55d679ab-005c-4188-8fb0-e051e72ee2fa	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 10238 - Apakah Pembayaran terakhir dilakukan setelah : - Pekerjaan selesai 100% (seratus perseratus)  "- Berita acara penyerahan pertama pekerjaan diterbitkan; Apakah Pembayaran yang dilakukan sebesar 95% (sembilan puluh lima perseratus) dari nilai kontrak, sedangkan yang 5% (lima perseratus) merupakan retensi selama masa pemeliharaan," "Pembayaran yang dilakukan sebesar 100% (seratus perseratus) dari nilai kontrak dan penyedia harus menyerahkan Jaminan Pemeliharaan sebesar 5% (lima perseratus) dari nilai kontrak" "Masa berlaku jaminan pemeliharaan sejak tanggal serah terima pertama sampai dengan tanggal penyerahan akhir pekerjaan," "Telah dipotong denda (jika ada), pajak,uang muka dan ganti rugi (jika ada), " "Pembayaran ganti rugi kepada penyedia ( jika ada) sesuai kontrak, " "Ganti rugi dibayarkan berdasarkan data penunjang dan perhitungan kompensasi yang diajukan oleh penyedia kepada PPK, telah dapat dibuktikan kerugian nyata akibat Peristiwa Kompensasi" "Penyedia telah memberikan peringatan dini akan terjadi pengeluaran biaya tambahan dan telah bekerja sama untuk mencegah pengeluaran biaya tambahan," "Besarnya denda, pajak, uang muka sesuai dengan kontrak, " "Jika pekerjaan tidak selesai karena kesalahan atau kelalaian penyedia maka penyedia dikenakan denda. " "Besarnya denda 1/1000 dari sisa harga bagian kontrak yang belum dikerjakan, bila pekrjaan yang telah dilaksanakan dapat berfungsi," Jika pekerjaan yang telah dilaksanakan belum  berfungsi, besarnya denda 1/1000 dari harga kontrak; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	fd3066d2-c8c0-460e-a52c-790cd50b7469	2017-10-20 01:44:48+00	\N
587eaedf-2514-4035-ac37-dc1a7ffeea3b	0188ba46-865b-43b4-9adf-44f6e4fe0cae	rencana	Anda ditugaskan pada 10236 - Teliti apakah telah dilakukan pemantauan pelaksanaan pekerjaan; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	f79b7985-23bb-4156-a074-bc23365ded79	30ef6912-451f-4c64-ac71-92a4bf5736c9	2017-10-20 02:49:56+00	2017-10-20 02:53:09+00
71fd0f4f-9786-4b8b-81ba-870177d9248e	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 10238 - Teliti apakah ada perubahan pekerjaan; Jika ada perubahan apakah telah didukung dengan dokumen yang lengkap; Tuangkan dalam KKA kuisioner; Buat kesimpulan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	61c0607b-868f-47af-a90d-dde0c42a3af4	2017-10-20 01:44:48+00	2017-10-20 03:03:20+00
4358b341-3ccb-426b-9443-663da3506e32	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 345 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	08fa0711-fce9-419e-b46d-bdc38b61b644	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2018-03-21 03:11:41+00	\N
1650de87-024b-4701-b93d-959c9dd985a6	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 9999 - Catat tanggal penerimaan dana BOS; bandingkan dengan ketentuan batas waktu penyaluran dana BOS ke Rekening sekolah; Buat kesimpulan	7ee14d19-aa59-4403-b0c1-85a7a0560da4	28ca9f0f-b7c0-442d-9e59-cf89b711af61	2017-10-17 02:16:53+00	2017-10-19 01:31:01+00
17363d68-d2b0-4352-b6fc-ebceab5b06fb	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 10238 - Apakah Penyerahan akhir pekerjaan dilaksanakan berdasarkan permintaan tertulis dari penyedia; Permintaan penyerahan akhir setelah masa pemeliharaan berakhir; Masa pemeliharaan telah ditentukan dalam syarat syarat khusus kontrak; Apakah Panitia/Pejabat Penerima Hasil Pekerjaan telah melakukan pemeriksaan/pengujian sebelum menerima hasil pekerjaan konstruksi; Apakah Penyedia telah memberikan petunjuk kepada PPK tentang pedoman pengoperasian dan perawatan sesuai dengan SSKK;Apakah PPK menerima penyerahan akhir pekerjaan setelah penyedia melaksanakan semua kewajibannya selama masa pemeliharaan dengan baik;  Tuangkan dalam KKA Kuisioner; Buat Kesimpulan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	505f7604-ccda-49df-83e7-6f5ef6c9bbb0	2017-10-20 01:44:48+00	\N
25fade72-c24d-47dd-9e41-4c53d4e0ba3c	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 10238 - Apabila penyedia tidak melaksanakan kewajiban pemeliharaan sebagaimana mestinya, maka apakah PPK  menggunakan uang retensi untuk membiayai perbaikan/pemeliharaan atau mencairkan Jaminan Pemeliharaan; Apakah PPK  melakukan pembayaran sisa nilai kontrak yang belum dibayar setelah PPK menrima penyerahan akhir pekerjaan; Apakah PPK  mengembalikan Jaminan Pemeliharaan setelah PPK menrima penyerahan akhir pekerjaan. "Tuangkan dalam KKA Kuisioner; Buat kesimpulan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	5db3d0f4-1803-444c-97de-d121100a62dc	2017-10-20 01:44:48+00	\N
c17c03b1-c5c2-4854-94c7-281e85e1763d	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 10238 - Apakah Penyedia Barang/Jasa memberitahukan tentang terjadinya Keadaan Kahar kepada PPK secara tertulis; Apakah Surat pemberitahuan didibuat dalam waktu paling lambat 14 (empat belas) hari kalender sejak terjadinya Keadaan Kahar; Apakah Surat pemberitahuan keadaan kahar disertai salinan pernyataan keadaan kahar yang dikeluarkan oleh pihak/instansi yang berwenang sesuai ketentuan peraturan perundang-undangan; Apakah Keadaan kahar telah sesuai dengan kesepakatan yang dituangkan dalam kontrak; Apakah Pengaruh keadaan kahar, telah dituangkan dalam perubahan Kontrak ; Apakah Penggantian biaya telah diatur dalam suatu adendum Kontrak ; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	e32bf68b-6224-4d54-8162-cb96836a27d7	2017-10-20 01:44:48+00	\N
a90b2932-fcde-43d2-9442-abc8ee384767	ee570f4f-46de-4761-ae5c-b205c92d5c3e	rencana	Anda ditugaskan pada 10238 - Susun P2HP, dan bahas dengan obrik	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	5250d9b9-8156-4a02-ad3b-dde51a608188	2017-10-20 01:44:48+00	\N
037de246-d5ca-48d6-9930-43d90e2b5dff	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 10238 - Susun draft laporan, dan bahas dengan internal Tim	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	47ea222d-05f8-4b95-a130-3e7bb6453ff0	2017-10-20 01:44:48+00	\N
479c657c-b8d7-4e07-ba17-24d931328d23	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 10238 - Lakukan scan laporan final, dan upload dalam aplikasi	3ac78c9e-9bbe-4348-9bd0-e0a8f0870e6d	57ba24b4-bde1-4749-88ce-ad7e5e658c34	2017-10-20 01:44:48+00	\N
838e53e2-d9a4-422c-a0c0-21cc3917238e	0188ba46-865b-43b4-9adf-44f6e4fe0cae	rencana	Anda ditugaskan pada 10236 - Teliti uraian, isi dan kelengkapan formal kontrak; Isikan dalam kuisioner di Form KKA; Buat kesimpulan	f79b7985-23bb-4156-a074-bc23365ded79	c5c56164-40c4-43fa-8495-dc5165718ebf	2017-10-20 02:49:56+00	2017-10-20 02:55:28+00
43e34967-80c5-4cb6-9b8b-82611fd4982c	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 987 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	6dd905b2-147a-4db1-aa8a-6f56f2bf4cbe	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2018-03-21 03:15:07+00	\N
9b334b3b-8f80-4b50-a2cc-5ae930fe84b5	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 119922 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	db0ffebb-9dbe-4772-87e8-f9444b28268e	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2018-04-12 10:11:41+00	\N
eacaac29-501f-42ee-ba58-c1ff9a102cc9	89488dc0-00e0-48c8-9e47-1f143b877878	rencana	Anda ditugaskan pada 5577 - Lakukan entry meeting dengan pimpinan obyek	b90a05f4-e77d-44b7-b657-a2fefd13c8fb	53983413-54bf-4a94-815e-82ccceecacc3	2018-08-31 06:59:16+00	\N
11458e55-695e-47e6-8b92-80890f2dd342	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 5577 - Lakukan survey terbatas IC	b90a05f4-e77d-44b7-b657-a2fefd13c8fb	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2018-08-31 06:59:16+00	\N
073c1502-126e-46c9-a715-90aa5bf1ce90	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 332 - Lakukan entry meeting dengan pimpinan obyek	f1ee1810-7e79-4695-8440-6c4ac296a5c7	53983413-54bf-4a94-815e-82ccceecacc3	2018-08-31 07:26:44+00	\N
20a1276d-30a8-4ab6-898a-66e700e01fe6	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 332 - Lakukan survey terbatas IC	f1ee1810-7e79-4695-8440-6c4ac296a5c7	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2018-08-31 07:26:44+00	\N
1a44c4c9-0ab9-4fc0-8259-57cc4ea16b41	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 332 - Buat permintaan data yang terkait dengan penugasan	f1ee1810-7e79-4695-8440-6c4ac296a5c7	51dd4682-183d-4989-84c2-18baefbd4539	2018-08-31 07:26:44+00	\N
0f9ac9f7-7c28-4e09-954a-78f8f6acd157	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 1984 - Dapatkan SK Kepala Sekolah tentang Tim BOS Sekolah dan periksa apakah susunan Tim telah sesuai dengan pedoman	b1477309-9e65-4c43-aefa-1219ef447c56	2dc5180a-6ecb-40db-a6e9-742109c4e82e	2018-08-31 07:36:28+00	\N
f72fc0e8-71a8-48b0-b2ca-4d14c4c814dd	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 1984 - Periksa apakah susunan tim sesuai dengan pedoman	b1477309-9e65-4c43-aefa-1219ef447c56	28d7e52a-59e8-4a1e-b43f-4a48cdca3e22	2018-08-31 07:36:28+00	\N
b5638da9-dc29-481a-9a9b-8b555dc770f0	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 6789 - Dapatkan SK Kepala Sekolah tentang Tim BOS Sekolah dan periksa apakah susunan Tim telah sesuai dengan pedoman	a1272441-cb74-427f-a423-dcb2c1bf5c13	2dc5180a-6ecb-40db-a6e9-742109c4e82e	2018-08-31 07:43:00+00	\N
56159252-ff16-4ead-9236-b52bcb7d2e88	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 6789 - Periksa apakah susunan tim sesuai dengan pedoman	a1272441-cb74-427f-a423-dcb2c1bf5c13	28d7e52a-59e8-4a1e-b43f-4a48cdca3e22	2018-08-31 07:43:00+00	\N
ed8f48e5-420d-4580-8be8-9cf5a7924aac	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 8888 - Catat tanggal penerimaan dana BOS; bandingkan dengan ketentuan batas waktu penyaluran dana BOS ke Rekening sekolah; Buat kesimpulan	28f5ba79-6c18-45e1-9d23-75f047783203	28ca9f0f-b7c0-442d-9e59-cf89b711af61	2018-09-01 00:32:59+00	\N
d319beeb-6694-4d59-9ad1-3e67129c1de6	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Dapatkan data realisasi penerimaan selain BOS; Dapatkan dasar penerimaan tersebut; Buat kesimpulan	28f5ba79-6c18-45e1-9d23-75f047783203	acd1709e-5bb3-436c-b956-d1cfebc17b65	2018-09-01 00:32:59+00	\N
43cff134-1ac4-47ff-9c86-b4da1783c669	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Dapatkan data penerimaan ; Bandingkan dengan jumlah angka di laporan; buat kesimpulan	28f5ba79-6c18-45e1-9d23-75f047783203	4453d1e6-05ed-4374-8344-a23915bf425b	2018-09-01 00:32:59+00	\N
6761a9cf-fcae-451f-b5e9-c696ad0c595f	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Dapatkan RKAS; Teliti apakah RKAS telah disusun sesuai pedoman; Buat Kesimpulan	28f5ba79-6c18-45e1-9d23-75f047783203	c7aa0a46-3ae1-4c52-a708-98c17cec316e	2018-09-01 00:32:59+00	\N
48432382-cc43-472d-9a68-a90eb5c786af	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 8888 - Teliti catatan dalam buku kas, apakah item pengeluaran telah tercantum dalam RKAS 	28f5ba79-6c18-45e1-9d23-75f047783203	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	2018-09-01 00:32:59+00	\N
b95dbaf9-bbd8-4c60-8adc-8d79a6f1af9b	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 8888 - Lakukan pengujian terhadap kegiatan; Dapatkan standar harga kegiatan (SBU);bandingkan biaya kegiatan dengan SBU yang berlaku; Buat simpulan	28f5ba79-6c18-45e1-9d23-75f047783203	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	2018-09-01 00:32:59+00	\N
71b54275-bc0a-4451-913a-d208558c9fa3	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 8888 - Dapatkan data pengeluaran kas; Lakukan konfirmasi atas hasil kegiatan, dokumen dan data pendukung pengeluaran; Buat kesimpulan	28f5ba79-6c18-45e1-9d23-75f047783203	5cbb9528-fa19-449b-a05f-ed0abfc36a00	2018-09-01 00:32:59+00	\N
5624216b-3584-4b26-9d1a-49a05c9055b8	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 8888 - Dapatkan catatan pengeluaran; lakukan pengecekan terhadap bukti pendukung, apakah pengeluaran didukung oleh bukti yang sah ; Buat kesimpulan	28f5ba79-6c18-45e1-9d23-75f047783203	c90bd32b-1b3f-4952-ad96-d498e6b56364	2018-09-01 00:32:59+00	\N
270bd039-3594-4b1e-a4df-feea74ed2f46	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 10236 - Susun P2HP, dan bahas dengan obrik	f79b7985-23bb-4156-a074-bc23365ded79	5250d9b9-8156-4a02-ad3b-dde51a608188	2017-10-20 02:49:56+00	2018-09-14 01:31:24+00
2af883fc-0b06-490b-b6d1-d6a7634f2179	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada  - 3	4ac7b90f-d4ad-4bba-bad1-94493366203d	3e1c2107-b0da-450d-8afa-b9380f375942	2017-10-16 10:42:20+00	\N
7c2ccab0-5fa9-4034-84cc-69341e92ad59	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Dapatkan data laporan periodik yang dibuat sekolah	9cb18ed2-4658-481f-9f74-e40105dc188b	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	2017-10-16 10:42:20+00	2017-10-20 00:54:42+00
2786572d-72e2-43cf-a30b-b4b417157702	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Lakukan reviu terhadap konsep Laporan	185a85fd-c805-4697-9719-21b017164891	72eed5d9-87a3-424d-abbf-e74972466494	2017-10-16 10:42:20+00	2017-10-20 01:45:08+00
cb6b5753-0e02-4f72-be41-407b919edbd1	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 10236 - Lakukan pengujian terhadap Jadwal mobilisasi, Pemeriksaan bersama lokasi kerja; Teliti kegiatan Penyerahan lokasi ; Teliti Program mutu dari penyedia ; Teliti ketepatan waktu penerbitan SPMK; Isikan dalam formulir kuisioner; Buat kesimpulan	f79b7985-23bb-4156-a074-bc23365ded79	493f29c3-5db5-470d-88a1-d50c04a963a6	2017-10-20 02:49:56+00	2017-10-20 03:05:45+00
12631764-a401-4126-922a-d30220dc6c00	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 987 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	6dd905b2-147a-4db1-aa8a-6f56f2bf4cbe	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2018-03-21 03:24:55+00	\N
d9de8764-395f-4fbd-a07d-4b41fe57144f	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 0000091 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	84759213-de14-4bea-8154-647cedb74bcf	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2018-04-13 12:16:24+00	\N
3e8679f5-7362-42fe-be0c-a2e48eb2a1ae	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 0003 - Lakukan entry meeting dengan pimpinan obyek	c12b0e44-7d6b-406d-ae54-d3bfc4046f17	53983413-54bf-4a94-815e-82ccceecacc3	2018-08-31 04:08:54+00	\N
6755866e-f604-4b8f-89a6-8632df94ddf8	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 0003 - Lakukan survey terbatas IC	c12b0e44-7d6b-406d-ae54-d3bfc4046f17	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2018-08-31 04:08:54+00	\N
fa98563f-3e9f-475e-a2ff-751c58ec276e	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 0003 - Buat permintaan data yang terkait dengan penugasan	c12b0e44-7d6b-406d-ae54-d3bfc4046f17	51dd4682-183d-4989-84c2-18baefbd4539	2018-08-31 04:08:54+00	\N
d6607f31-8c50-4999-8b3b-50120bd9bec6	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 0003 - Dapatkan data siswa tiap tahun ajaran	c12b0e44-7d6b-406d-ae54-d3bfc4046f17	b7689c74-5a86-4711-b170-6deb99f7137b	2018-08-31 04:08:54+00	\N
1d16dbae-f7b8-43c6-802c-b847db40172b	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0003 - Dapatkan data pendapatan dan belanja sekolah	c12b0e44-7d6b-406d-ae54-d3bfc4046f17	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	2018-08-31 04:08:54+00	\N
5ffdbb66-9b63-43e0-96e0-37737796ad25	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 0003 - Dapatkan data aset sekolah	c12b0e44-7d6b-406d-ae54-d3bfc4046f17	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	2018-08-31 04:08:54+00	\N
306af65c-64c3-473c-9245-ba66ccb0d13c	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0003 - Dapatkan data laporan periodik yang dibuat sekolah	c12b0e44-7d6b-406d-ae54-d3bfc4046f17	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	2018-08-31 04:08:54+00	\N
edda2699-01a4-48be-9de4-3d3cab792d60	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 0003 - Dapatkan data total penerimaan BOS tiap tahun ajaran	c12b0e44-7d6b-406d-ae54-d3bfc4046f17	e7a5563b-215a-44a1-a523-a1e2f47cb694	2018-08-31 04:08:54+00	\N
95a3f8e2-a394-499a-b4f2-d3eac642beea	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0003 - Dapatkan data perhitungan penerimaan BOS dan bandingkan dengan jumlah siswa yang terdaftar	c12b0e44-7d6b-406d-ae54-d3bfc4046f17	62fc3064-8e6a-4e97-a185-fb1156c0c16a	2018-08-31 04:08:54+00	\N
617b7791-4566-4ee1-8a64-38906f112c4d	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0003 - Catat tanggal penerimaan dana BOS; bandingkan dengan ketentuan batas waktu penyaluran dana BOS ke Rekening sekolah; Buat kesimpulan	c12b0e44-7d6b-406d-ae54-d3bfc4046f17	28ca9f0f-b7c0-442d-9e59-cf89b711af61	2018-08-31 04:08:54+00	\N
7b011772-5a93-46b1-96b2-3d3c10e22f1f	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 0003 - Dapatkan data realisasi penerimaan selain BOS; Dapatkan dasar penerimaan tersebut; Buat kesimpulan	c12b0e44-7d6b-406d-ae54-d3bfc4046f17	acd1709e-5bb3-436c-b956-d1cfebc17b65	2018-08-31 04:08:54+00	\N
a3259726-0b76-4afd-8e55-bf8c009eb597	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 3388 - Lakukan entry meeting dengan pimpinan obyek	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	53983413-54bf-4a94-815e-82ccceecacc3	2018-08-31 07:01:48+00	\N
1c035eb4-a17d-4105-85b1-0aa5061229cb	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 3388 - Lakukan survey terbatas IC	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2018-08-31 07:01:48+00	\N
d213c722-0972-4eb7-9a9e-152a46f90735	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 3388 - Buat permintaan data yang terkait dengan penugasan	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	51dd4682-183d-4989-84c2-18baefbd4539	2018-08-31 07:01:48+00	\N
7c8e09cb-f9c8-4ab8-bff6-108716ae4b2d	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 3388 - Dapatkan data siswa tiap tahun ajaran	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	b7689c74-5a86-4711-b170-6deb99f7137b	2018-08-31 07:01:48+00	\N
7ab93c76-a0f1-4833-b3c5-48b87b777acb	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 3388 - Dapatkan data pendapatan dan belanja sekolah	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	2018-08-31 07:01:48+00	\N
e0c54d73-9fa2-47e2-b186-35358a8b878d	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 3388 - Dapatkan data aset sekolah	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	2018-08-31 07:01:48+00	\N
8d296119-6ef4-48c1-b88c-a3941471975c	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 3388 - Dapatkan data laporan periodik yang dibuat sekolah	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	2018-08-31 07:01:48+00	\N
82af1029-94da-4e6b-b704-30d4cf883fe5	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 3388 - Dapatkan data total penerimaan BOS tiap tahun ajaran	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	e7a5563b-215a-44a1-a523-a1e2f47cb694	2018-08-31 07:01:48+00	\N
61be05bd-7dbd-4def-a9a4-1851086d0a96	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 3388 - Dapatkan data perhitungan penerimaan BOS dan bandingkan dengan jumlah siswa yang terdaftar	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	62fc3064-8e6a-4e97-a185-fb1156c0c16a	2018-08-31 07:01:48+00	\N
98304715-f59e-49f7-b63d-aa2edb92f5ea	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 3388 - Catat tanggal penerimaan dana BOS; bandingkan dengan ketentuan batas waktu penyaluran dana BOS ke Rekening sekolah; Buat kesimpulan	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	28ca9f0f-b7c0-442d-9e59-cf89b711af61	2018-08-31 07:01:48+00	\N
fc12fa25-516d-4f81-a473-4febe04cad7b	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 3388 - Dapatkan data realisasi penerimaan selain BOS; Dapatkan dasar penerimaan tersebut; Buat kesimpulan	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	acd1709e-5bb3-436c-b956-d1cfebc17b65	2018-08-31 07:01:48+00	\N
4c4eca17-752f-4c3d-a8a6-18670d7f6b24	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 3388 - Dapatkan data penerimaan ; Bandingkan dengan jumlah angka di laporan; buat kesimpulan	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	4453d1e6-05ed-4374-8344-a23915bf425b	2018-08-31 07:01:48+00	\N
7d18fd70-e842-4b42-a173-27bea467f817	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 3388 - Dapatkan RKAS; Teliti apakah RKAS telah disusun sesuai pedoman; Buat Kesimpulan	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	c7aa0a46-3ae1-4c52-a708-98c17cec316e	2018-08-31 07:01:48+00	\N
29b1eaa5-b455-4ac5-8d22-90aca6b2057a	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 3388 - Teliti catatan dalam buku kas, apakah item pengeluaran telah tercantum dalam RKAS 	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	2018-08-31 07:01:48+00	\N
0b17919e-e8a0-4679-b805-29c4f24a9d03	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 3388 - Lakukan pengujian terhadap kegiatan; Dapatkan standar harga kegiatan (SBU);bandingkan biaya kegiatan dengan SBU yang berlaku; Buat simpulan	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	2018-08-31 07:01:48+00	\N
cad3cf03-29d1-4a28-bf64-135af2b3244b	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 3388 - Dapatkan data pengeluaran kas; Lakukan konfirmasi atas hasil kegiatan, dokumen dan data pendukung pengeluaran; Buat kesimpulan	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	5cbb9528-fa19-449b-a05f-ed0abfc36a00	2018-08-31 07:01:48+00	\N
95aa3b78-d98e-4ab2-bbe2-0656bfe1f489	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 3388 - Buat Daftar Temuan	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	49f6c09c-485d-458b-aae9-c3ff883f4542	2018-08-31 07:01:48+00	\N
65d57e57-944a-448e-b84e-e4dadafa5506	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 3388 - Lakukan pembahasan internal tim	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	64722e98-772e-4a28-94c3-03d3cb483320	2018-08-31 07:01:48+00	\N
fe7d94a4-613d-41d8-a407-55ce117aa5d1	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 3388 - Buat Pemberitahuan Hasil Pengawasan/pemeriksaan	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	04b0f150-6dca-4d93-b29f-3eef3a302281	2018-08-31 07:01:48+00	\N
c8320851-27e7-4829-a300-a3eb4996cf3e	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 3388 - Lakukan reviu terhadap konsep Laporan	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	72eed5d9-87a3-424d-abbf-e74972466494	2018-08-31 07:01:48+00	\N
64940127-e374-45db-9a88-680af3a91b63	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 3388 - Lakukan scan laporan final dalam bentuk pdf, dan upload di aplikasi	b2cb7106-fd00-400b-bb10-4fb1e5a7a6f8	06709067-0f75-4b63-96b3-47b501e4674c	2018-08-31 07:01:48+00	\N
c630f204-a522-4ed0-88a1-3b1433dbfe91	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 5577 - Lakukan entry meeting dengan pimpinan obyek	34e4a86b-0bb3-4d9b-b5a7-4ee047258887	53983413-54bf-4a94-815e-82ccceecacc3	2018-08-31 07:26:55+00	\N
22eed6db-a3d5-4c4c-b238-a81bab8d2794	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Dapatkan data laporan periodik yang dibuat sekolah	4114359b-983c-4020-a06f-65a7c7913b15	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	2017-10-16 10:42:20+00	2017-10-20 01:22:44+00
c171dadc-6268-4d11-83fd-c9fd43484f6d	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 10232 - Lakukan pengujian terhadap Jadwal mobilisasi, Pemeriksaan bersama lokasi kerja; Teliti kegiatan Penyerahan lokasi ; Teliti Program mutu dari penyedia ; Teliti ketepatan waktu penerbitan SPMK; Isikan dalam formulir kuisioner; Buat kesimpulan	6c8cd73f-7443-4bea-9ae3-184f42f07320	493f29c3-5db5-470d-88a1-d50c04a963a6	2017-10-20 01:46:09+00	\N
5794efef-f96b-465f-a40c-5d0ba5210437	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 10232 - Teliti pembayaran Uang muka apakah sesuai klausul dan rencana penggunaan penggunaan; isikan dalam KKA kuisioner; Buat kesimpulan	6c8cd73f-7443-4bea-9ae3-184f42f07320	8a6bbfba-798e-4593-9d57-d0d5a88b2c4a	2017-10-20 01:46:09+00	\N
7b156475-d7d1-47b6-8521-648e36b845d5	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 10232 - Teliti apakah telah dilakukan pengawasan mutu pelaksanaan kegiatan; Tuangkan dalam KKA kuisioner ; Buat kesimpulan	6c8cd73f-7443-4bea-9ae3-184f42f07320	823a2b84-ca0e-42a6-9792-1c1b19d3dfb6	2017-10-20 01:46:09+00	\N
afe5a903-d94b-49be-9917-92aa39a218cf	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 10232 - Teliti apakah ada perubahan pekerjaan; Jika ada perubahan apakah telah didukung dengan dokumen yang lengkap; Tuangkan dalam KKA kuisioner; Buat kesimpulan	6c8cd73f-7443-4bea-9ae3-184f42f07320	61c0607b-868f-47af-a90d-dde0c42a3af4	2017-10-20 01:46:09+00	\N
4b89739f-3cb0-4464-be32-20425abe811b	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 10232 - Teliti apakah Penyedia telah menyampaikan laporan kemajuan hasil pekerjaan; Apakah Laporan kemajuan hasil pekerjaan telah diverifikasi PPK; Teliti apakah Kemajuan hasil pekerjaan yang dilaporkan telah sesuai dengan kemajuan pekerjaan fisik dilapangan; Teliti apakah Spesifikasi teknis hasil pekerjaan yang terpasang telah sesuai dengan kontrak;  Tuangkan dalam KKA Kuisioner; Buat kesimpulan	6c8cd73f-7443-4bea-9ae3-184f42f07320	1b72e119-165e-401c-908c-83b0438780e9	2017-10-20 01:46:09+00	\N
c4ea11cf-cff5-4f76-b44c-a3a71b7dfe27	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 10232 - Apakah Penyedia telah mengajukan tagihan disertai laporan kemajuan hasil pekerjaan; Untuk Kontrak yang mempunyai subkontrak, apakah permintaan pembayaran telah dilengkapi bukti pembayaran kepada seluruh sub penyedia sesuai dengan prestasi pekerjaan;  Apakah Pembayaran penyedia kepada sub kontraktor telah sesuai dengan pekerjaan yang terpasang; Apakah Pembayaran dilakukan dengan sistem termin sesuai ketentuan dalam Dokumen Kontrak; Apakah Pembayaran dilakukan senilai pekerjaan yang telah terpasang, tidak termasuk bahan/material dan peralatan yang ada di lokasi pekerjaan; Apakah Pembayaran termin telah dipotong angsuran uang muka, denda(bila ada); Tuangkan dalam KKA Kuisioner; Buat kesimpulan 	6c8cd73f-7443-4bea-9ae3-184f42f07320	64ecb1a9-91f0-4b16-8360-11da3e655574	2017-10-20 01:46:09+00	\N
c567c7bd-f904-409d-957f-4af1a02ee3d0	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 10232 - Apakah PPK melakukan penelitian usulan tertulis perpanjangan waktu penyelesaian kontrak oleh Penyedia; Apakah Perpanjangan waktu pelaksanaan kontrak telah berdasarkan pertimbangan yang layak dan wajar oleh PPK, karena :( pekerjaan tambah;  - perubahan disain;  - keterlambatan yang disebabkan oleh PPK;  "- masalah yang timbul diluar kendali penyedia; dan/atau " - Keadaan Kahar) ; Apakah Penyedia telah memberikan peringatan dini akan terjadi keterlambatan dan telah bekerja sama untuk bekerja sama untuk mencegah keterlambatan; Apakah PPK telah menetapkan ada tidaknya perpanjangan, dalam jangka waktu 21 (dua puluh satu) hari setelah penyedia meminta perpanjangan; Apakah Penetapan ada/tidak perpanjangan pelaksanaan dinyatakan secara tertulis; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	6c8cd73f-7443-4bea-9ae3-184f42f07320	8d53ae6e-9502-4103-8603-ecd615b32d94	2017-10-20 01:46:09+00	\N
1fe30c13-e45f-48db-acf7-90669da4e4d5	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 10232 - Apakah Penyerahan hasil pekerjaan berdasarkan permintaan secara tertulis dari Penyedia Pekerjaan Konstruksi; Apakah Permintaan tersebut setelah pekerjaan selesai 100 % ; Apakah Panitia/Pejabat Penerima Hasil Pekerjaan melakukan pemeriksaan hasil pekerjaan Pengadaan Barang/Jasa; Apakah Melakukan pemeriksaan sesuai dengan pedoman/prosedur yang telah ditetapkan; Apakah Penyedia telah melaksanakan pekerjaan konstruksi sesuai dengan ketentuan yang tercantum dalam Kontrak dan gambar; Apakah Apabila terdapat ketidaksesuaian dengan kontrak (kekurangan-kekurangan dan/atau cacat hasil pekerjaan), penyedia telah memperbaiki/menyelesaikannya; Apakah panitia/Pejabat Penerima Hasil Pekerjaan menerima hasil Pengadaan Barang/Jasa setelah melalui pemeriksaan/pengujian; Apakah PPK menerima penyerahan pertama pekerjaan setelah seluruh hasil pekerjaan dilaksanakan sesuai dengan ketentuan Kontrak dan diterima oleh Panitia/Pejabat Penerima Hasil Pekerjaan; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	6c8cd73f-7443-4bea-9ae3-184f42f07320	6b69d2bd-631b-423a-a5c4-15d7ba2adb89	2017-10-20 01:46:09+00	\N
2728be8c-3954-440c-9eb9-e4c1c38e9fa3	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 10232 - Teliti apakah telah dilakukan pemantauan pelaksanaan pekerjaan; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	6c8cd73f-7443-4bea-9ae3-184f42f07320	30ef6912-451f-4c64-ac71-92a4bf5736c9	2017-10-20 01:46:09+00	2017-10-20 01:51:13+00
46388d80-bc99-4a86-910a-efa8a3b08dc1	f741041f-732a-481f-bc78-461bc55ada9b	rencana	Anda ditugaskan pada 10232 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	6c8cd73f-7443-4bea-9ae3-184f42f07320	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2017-10-20 01:46:09+00	2017-10-20 01:51:19+00
a14b2c37-3089-4b53-b9f6-49484eecd4a1	1b162438-55b1-4b5c-ac17-5699e9dfb1ed	rencana	Anda ditugaskan pada 09177 - Lakukan reviu terhadap konsep Laporan	4114359b-983c-4020-a06f-65a7c7913b15	72eed5d9-87a3-424d-abbf-e74972466494	2017-10-16 10:42:20+00	2017-10-20 11:07:24+00
eea71aa3-549b-4566-8aa8-8eb33bd98865	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 10232 - Buat permintaan data yang terkait dengan penugasan	6c8cd73f-7443-4bea-9ae3-184f42f07320	80636594-116a-44e8-b4be-e8b064986a06	2017-10-20 01:46:09+00	2017-10-21 05:31:34+00
a3ec7c57-c200-45d6-9f99-b62859175868	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 123456 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	5ffb7fc3-7839-497f-b5e8-e936665e819f	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2018-03-21 12:48:18+00	\N
4f8ed1b9-4a8c-433e-a58d-ed60e09217a0	95542b83-a537-4794-a103-b768408db707	rencana	Anda ditugaskan pada 123112121212 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	0334a10d-2102-4261-b84c-056720515d2c	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2018-04-13 12:32:23+00	\N
04e4fd30-e980-4ad6-bdb3-d17e28ae013a	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 10232 - Teliti uraian, isi dan kelengkapan formal kontrak; Isikan dalam kuisioner di Form KKA; Buat kesimpulan	6c8cd73f-7443-4bea-9ae3-184f42f07320	c5c56164-40c4-43fa-8495-dc5165718ebf	2017-10-20 01:46:09+00	2018-07-10 03:04:54+00
21d0c0cf-ace1-47f9-9dab-08f5ac2b70ae	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 123123 - Lakukan entry meeting dengan pimpinan obyek	095fe615-c827-4a01-93e0-de6076d09ad2	53983413-54bf-4a94-815e-82ccceecacc3	2018-08-31 06:40:29+00	\N
a79b5444-c3e6-49e9-8db4-c63f36ff4e14	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 123123 - Lakukan survey terbatas IC	095fe615-c827-4a01-93e0-de6076d09ad2	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2018-08-31 06:40:29+00	2018-08-31 06:59:00+00
aebf57b1-89f0-45fc-99b9-414a57a3b260	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 198307132011 1 007 - Dapatkan data Laporan penerimaan barang dan LRA 	58c39523-485e-4456-aadd-47cd90614072	040c3a08-6eb5-46e8-9ce2-5ce3abbd68cd	2018-08-31 07:05:49+00	\N
a3b64f0f-39b8-49b2-97b1-fa91d0bd7c8c	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 198307132011 1 007 - Lakukan penghitungan jumlah transaksi (record) untuk m,enentukan jumlah sample yang akan dilakukan ujipetik	58c39523-485e-4456-aadd-47cd90614072	2ab1af68-84e0-4535-a4c7-4812c8a4473b	2018-08-31 07:05:49+00	\N
0314e6f8-ad7d-492e-a606-41bb56a82fbe	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 198307132011 1 007 - Lakukan pengujian atas penginputan data transaksi yang tidak lengkapp	58c39523-485e-4456-aadd-47cd90614072	33a3fba6-c0be-4095-bf13-e2e193d298c3	2018-08-31 07:05:49+00	\N
1f515ba9-ec9e-4ee2-b360-c9b7b76f7f06	89488dc0-00e0-48c8-9e47-1f143b877878	rencana	Anda ditugaskan pada 5577 - Buat permintaan data yang terkait dengan penugasan	34e4a86b-0bb3-4d9b-b5a7-4ee047258887	51dd4682-183d-4989-84c2-18baefbd4539	2018-08-31 07:26:55+00	\N
48ee2333-7858-44c0-bf97-09bf3f548492	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 700/12/425.302/2018 - Lakukan entry meeting dengan pimpinan obyek	43114d29-0a14-4b78-bb61-1c119d4b3f88	53983413-54bf-4a94-815e-82ccceecacc3	2018-08-31 07:37:39+00	\N
1c390724-5c09-4cd8-b56c-0f78ffb6bb85	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 8888 - Lakukan entry meeting dengan pimpinan obyek	28f5ba79-6c18-45e1-9d23-75f047783203	53983413-54bf-4a94-815e-82ccceecacc3	2018-09-01 00:32:59+00	\N
7e70cdc0-9c0d-4428-947e-82787b3c8e48	f741041f-732a-481f-bc78-461bc55ada9b	rencana	Anda ditugaskan pada 09172 - Lakukan reviu terhadap konsep Laporan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	72eed5d9-87a3-424d-abbf-e74972466494	2017-10-16 10:42:20+00	2017-10-20 01:31:06+00
d0399747-146e-4ce7-b498-dfde6c3f8a33	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 10232 - Apakah Penyedia Barang/Jasa memberitahukan tentang terjadinya Keadaan Kahar kepada PPK secara tertulis; Apakah Surat pemberitahuan didibuat dalam waktu paling lambat 14 (empat belas) hari kalender sejak terjadinya Keadaan Kahar; Apakah Surat pemberitahuan keadaan kahar disertai salinan pernyataan keadaan kahar yang dikeluarkan oleh pihak/instansi yang berwenang sesuai ketentuan peraturan perundang-undangan; Apakah Keadaan kahar telah sesuai dengan kesepakatan yang dituangkan dalam kontrak; Apakah Pengaruh keadaan kahar, telah dituangkan dalam perubahan Kontrak ; Apakah Penggantian biaya telah diatur dalam suatu adendum Kontrak ; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	6c8cd73f-7443-4bea-9ae3-184f42f07320	e32bf68b-6224-4d54-8162-cb96836a27d7	2017-10-20 01:46:09+00	\N
6c9bae53-0d3e-42d0-a778-5f1cfc1fdeff	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 10232 - Susun P2HP, dan bahas dengan obrik	6c8cd73f-7443-4bea-9ae3-184f42f07320	5250d9b9-8156-4a02-ad3b-dde51a608188	2017-10-20 01:46:09+00	\N
f9e433c0-d713-4455-895c-d511f6bad659	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 10232 - Susun draft laporan, dan bahas dengan internal Tim	6c8cd73f-7443-4bea-9ae3-184f42f07320	47ea222d-05f8-4b95-a130-3e7bb6453ff0	2017-10-20 01:46:09+00	\N
245b61f1-3033-4955-ae55-7c045c1ec29a	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 10232 - Lakukan scan laporan final, dan upload dalam aplikasi	6c8cd73f-7443-4bea-9ae3-184f42f07320	57ba24b4-bde1-4749-88ce-ad7e5e658c34	2017-10-20 01:46:09+00	\N
886c8764-59f6-4f69-a09a-3ce89e4901f3	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 10232 - Apakah Pembayaran terakhir dilakukan setelah : - Pekerjaan selesai 100% (seratus perseratus)  "- Berita acara penyerahan pertama pekerjaan diterbitkan; Apakah Pembayaran yang dilakukan sebesar 95% (sembilan puluh lima perseratus) dari nilai kontrak, sedangkan yang 5% (lima perseratus) merupakan retensi selama masa pemeliharaan," "Pembayaran yang dilakukan sebesar 100% (seratus perseratus) dari nilai kontrak dan penyedia harus menyerahkan Jaminan Pemeliharaan sebesar 5% (lima perseratus) dari nilai kontrak" "Masa berlaku jaminan pemeliharaan sejak tanggal serah terima pertama sampai dengan tanggal penyerahan akhir pekerjaan," "Telah dipotong denda (jika ada), pajak,uang muka dan ganti rugi (jika ada), " "Pembayaran ganti rugi kepada penyedia ( jika ada) sesuai kontrak, " "Ganti rugi dibayarkan berdasarkan data penunjang dan perhitungan kompensasi yang diajukan oleh penyedia kepada PPK, telah dapat dibuktikan kerugian nyata akibat Peristiwa Kompensasi" "Penyedia telah memberikan peringatan dini akan terjadi pengeluaran biaya tambahan dan telah bekerja sama untuk mencegah pengeluaran biaya tambahan," "Besarnya denda, pajak, uang muka sesuai dengan kontrak, " "Jika pekerjaan tidak selesai karena kesalahan atau kelalaian penyedia maka penyedia dikenakan denda. " "Besarnya denda 1/1000 dari sisa harga bagian kontrak yang belum dikerjakan, bila pekrjaan yang telah dilaksanakan dapat berfungsi," Jika pekerjaan yang telah dilaksanakan belum  berfungsi, besarnya denda 1/1000 dari harga kontrak; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	6c8cd73f-7443-4bea-9ae3-184f42f07320	fd3066d2-c8c0-460e-a52c-790cd50b7469	2017-10-20 01:46:09+00	2017-10-21 05:29:46+00
77292f3c-3a98-496d-9c1f-07faf3ca089f	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 10232 - Apakah Penyerahan akhir pekerjaan dilaksanakan berdasarkan permintaan tertulis dari penyedia; Permintaan penyerahan akhir setelah masa pemeliharaan berakhir; Masa pemeliharaan telah ditentukan dalam syarat syarat khusus kontrak; Apakah Panitia/Pejabat Penerima Hasil Pekerjaan telah melakukan pemeriksaan/pengujian sebelum menerima hasil pekerjaan konstruksi; Apakah Penyedia telah memberikan petunjuk kepada PPK tentang pedoman pengoperasian dan perawatan sesuai dengan SSKK;Apakah PPK menerima penyerahan akhir pekerjaan setelah penyedia melaksanakan semua kewajibannya selama masa pemeliharaan dengan baik;  Tuangkan dalam KKA Kuisioner; Buat Kesimpulan	6c8cd73f-7443-4bea-9ae3-184f42f07320	505f7604-ccda-49df-83e7-6f5ef6c9bbb0	2017-10-20 01:46:09+00	2017-10-21 05:31:05+00
d4a778e3-5a54-477b-a00b-da27b1cff66b	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 0000091 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	84759213-de14-4bea-8154-647cedb74bcf	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2018-04-12 10:00:39+00	\N
32e7eb9e-c1db-4580-9788-ab216a22ee95	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 10232 - Apabila penyedia tidak melaksanakan kewajiban pemeliharaan sebagaimana mestinya, maka apakah PPK  menggunakan uang retensi untuk membiayai perbaikan/pemeliharaan atau mencairkan Jaminan Pemeliharaan; Apakah PPK  melakukan pembayaran sisa nilai kontrak yang belum dibayar setelah PPK menrima penyerahan akhir pekerjaan; Apakah PPK  mengembalikan Jaminan Pemeliharaan setelah PPK menrima penyerahan akhir pekerjaan. "Tuangkan dalam KKA Kuisioner; Buat kesimpulan	6c8cd73f-7443-4bea-9ae3-184f42f07320	5db3d0f4-1803-444c-97de-d121100a62dc	2017-10-20 01:46:09+00	2018-04-26 08:28:44+00
979def4a-a698-40ef-8ed0-f061c849e1d8	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 1119 - Lakukan entry meeting dengan pimpinan obyek	c59ea1f7-e9cc-4e6f-9206-f90d21fe40f8	53983413-54bf-4a94-815e-82ccceecacc3	2018-08-31 06:46:55+00	2018-08-31 06:47:28+00
1854d362-8cda-4db7-93ed-860e72633fff	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 1234 - Lakukan entry meeting dengan pimpinan obyek	8d671fbc-19f7-43ae-8e33-044a666d0ab3	53983413-54bf-4a94-815e-82ccceecacc3	2018-08-31 07:21:44+00	\N
a87decf4-e2fb-4d90-864a-7db605757e1d	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 1234 - Lakukan survey terbatas IC	8d671fbc-19f7-43ae-8e33-044a666d0ab3	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2018-08-31 07:21:44+00	\N
e88a0aba-19ef-4137-9348-3f41e503db6a	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 1234 - Buat permintaan data yang terkait dengan penugasan	8d671fbc-19f7-43ae-8e33-044a666d0ab3	51dd4682-183d-4989-84c2-18baefbd4539	2018-08-31 07:21:44+00	\N
0060171d-59cd-4744-9746-76c910bf4812	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 1234 - Dapatkan data siswa tiap tahun ajaran	8d671fbc-19f7-43ae-8e33-044a666d0ab3	b7689c74-5a86-4711-b170-6deb99f7137b	2018-08-31 07:21:44+00	\N
e5f722bb-b142-421c-9342-7f0b58f63227	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 5577 - Lakukan survey terbatas IC	34e4a86b-0bb3-4d9b-b5a7-4ee047258887	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2018-08-31 07:26:55+00	2018-08-31 07:29:12+00
917e1364-244a-40b7-b044-5562008f21a8	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 77778 - Dapatkan SK Kepala Sekolah tentang Tim BOS Sekolah dan periksa apakah susunan Tim telah sesuai dengan pedoman	20836be9-c3ac-46a2-a33e-932405df0b8b	2dc5180a-6ecb-40db-a6e9-742109c4e82e	2018-08-31 07:39:42+00	\N
46ce9256-b483-4cdb-b498-ee0416cefe17	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 77778 - Periksa apakah susunan tim sesuai dengan pedoman	20836be9-c3ac-46a2-a33e-932405df0b8b	28d7e52a-59e8-4a1e-b43f-4a48cdca3e22	2018-08-31 07:39:42+00	\N
d4a4aa4b-e8c4-43af-9061-90c1fe7cbb95	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 8888 - Lakukan survey terbatas IC	28f5ba79-6c18-45e1-9d23-75f047783203	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2018-09-01 00:32:59+00	\N
7841ee83-d7a3-499b-a2e4-8bb5893141fa	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 8888 - Buat permintaan data yang terkait dengan penugasan	28f5ba79-6c18-45e1-9d23-75f047783203	51dd4682-183d-4989-84c2-18baefbd4539	2018-09-01 00:32:59+00	\N
276ffaf1-7af3-4296-b895-1b79ee0767ca	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 8888 - Dapatkan data siswa tiap tahun ajaran	28f5ba79-6c18-45e1-9d23-75f047783203	b7689c74-5a86-4711-b170-6deb99f7137b	2018-09-01 00:32:59+00	\N
69352df7-5159-4edc-8a59-44113d361346	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Dapatkan data pendapatan dan belanja sekolah	28f5ba79-6c18-45e1-9d23-75f047783203	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	2018-09-01 00:32:59+00	\N
ea03f7a1-0750-4c4d-bab5-7cbc3c09a87b	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Dapatkan data aset sekolah	28f5ba79-6c18-45e1-9d23-75f047783203	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	2018-09-01 00:32:59+00	\N
de34e5bb-1869-415b-8ee7-e633ee7132e1	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Dapatkan data laporan periodik yang dibuat sekolah	28f5ba79-6c18-45e1-9d23-75f047783203	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	2018-09-01 00:32:59+00	\N
466e357e-80d6-4bd5-bad7-392b9e3583b5	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 8888 - Dapatkan data total penerimaan BOS tiap tahun ajaran	28f5ba79-6c18-45e1-9d23-75f047783203	e7a5563b-215a-44a1-a523-a1e2f47cb694	2018-09-01 00:32:59+00	\N
1a72bc5a-ccec-4897-abbd-aacf2b3f9ef4	fe251bf2-8d4f-4205-af0b-51160cd31e63	rencana	Anda ditugaskan pada 09172 - Buat permintaan data yang terkait dengan penugasan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	51dd4682-183d-4989-84c2-18baefbd4539	2017-10-16 10:42:20+00	2017-10-19 12:05:40+00
9d9d9bc6-e925-44ca-8c01-0fdf1bfea105	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 10237 - Buat permintaan data yang terkait dengan penugasan	05275cf2-b282-4653-8c37-a0d7f8923a62	80636594-116a-44e8-b4be-e8b064986a06	2017-10-20 01:36:04+00	\N
0987b8b0-dced-4811-b2c7-5805314fd4fa	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 10237 - Teliti uraian, isi dan kelengkapan formal kontrak; Isikan dalam kuisioner di Form KKA; Buat kesimpulan	05275cf2-b282-4653-8c37-a0d7f8923a62	c5c56164-40c4-43fa-8495-dc5165718ebf	2017-10-20 01:36:04+00	\N
77ea56b3-2bfa-4167-ac7d-96a3f1335bd0	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 10237 - Lakukan pengujian terhadap Jadwal mobilisasi, Pemeriksaan bersama lokasi kerja; Teliti kegiatan Penyerahan lokasi ; Teliti Program mutu dari penyedia ; Teliti ketepatan waktu penerbitan SPMK; Isikan dalam formulir kuisioner; Buat kesimpulan	05275cf2-b282-4653-8c37-a0d7f8923a62	493f29c3-5db5-470d-88a1-d50c04a963a6	2017-10-20 01:36:04+00	\N
9beb1d39-3f2f-4f6a-a139-f0eb43b627b5	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 10237 - Teliti pembayaran Uang muka apakah sesuai klausul dan rencana penggunaan penggunaan; isikan dalam KKA kuisioner; Buat kesimpulan	05275cf2-b282-4653-8c37-a0d7f8923a62	8a6bbfba-798e-4593-9d57-d0d5a88b2c4a	2017-10-20 01:36:04+00	\N
a460600d-de71-483a-923c-ad702f6b6791	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 10237 - Teliti apakah telah dilakukan pemantauan pelaksanaan pekerjaan; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	05275cf2-b282-4653-8c37-a0d7f8923a62	30ef6912-451f-4c64-ac71-92a4bf5736c9	2017-10-20 01:36:04+00	\N
88843813-fdc0-437d-aba3-2dfe11437ad1	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 10237 - Teliti apakah ada perubahan pekerjaan; Jika ada perubahan apakah telah didukung dengan dokumen yang lengkap; Tuangkan dalam KKA kuisioner; Buat kesimpulan	05275cf2-b282-4653-8c37-a0d7f8923a62	61c0607b-868f-47af-a90d-dde0c42a3af4	2017-10-20 01:36:04+00	\N
a0591902-4ec5-4341-9f3a-84c379ad8478	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 10237 - Teliti apakah Penyedia telah menyampaikan laporan kemajuan hasil pekerjaan; Apakah Laporan kemajuan hasil pekerjaan telah diverifikasi PPK; Teliti apakah Kemajuan hasil pekerjaan yang dilaporkan telah sesuai dengan kemajuan pekerjaan fisik dilapangan; Teliti apakah Spesifikasi teknis hasil pekerjaan yang terpasang telah sesuai dengan kontrak;  Tuangkan dalam KKA Kuisioner; Buat kesimpulan	05275cf2-b282-4653-8c37-a0d7f8923a62	1b72e119-165e-401c-908c-83b0438780e9	2017-10-20 01:36:04+00	\N
98082143-656f-4190-9389-45de93ee12ba	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 10237 - Apakah Penyedia telah mengajukan tagihan disertai laporan kemajuan hasil pekerjaan; Untuk Kontrak yang mempunyai subkontrak, apakah permintaan pembayaran telah dilengkapi bukti pembayaran kepada seluruh sub penyedia sesuai dengan prestasi pekerjaan;  Apakah Pembayaran penyedia kepada sub kontraktor telah sesuai dengan pekerjaan yang terpasang; Apakah Pembayaran dilakukan dengan sistem termin sesuai ketentuan dalam Dokumen Kontrak; Apakah Pembayaran dilakukan senilai pekerjaan yang telah terpasang, tidak termasuk bahan/material dan peralatan yang ada di lokasi pekerjaan; Apakah Pembayaran termin telah dipotong angsuran uang muka, denda(bila ada); Tuangkan dalam KKA Kuisioner; Buat kesimpulan 	05275cf2-b282-4653-8c37-a0d7f8923a62	64ecb1a9-91f0-4b16-8360-11da3e655574	2017-10-20 01:36:04+00	\N
605742cd-951c-43aa-9b05-7b6587954f91	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 10237 - Apakah PPK melakukan penelitian usulan tertulis perpanjangan waktu penyelesaian kontrak oleh Penyedia; Apakah Perpanjangan waktu pelaksanaan kontrak telah berdasarkan pertimbangan yang layak dan wajar oleh PPK, karena :( pekerjaan tambah;  - perubahan disain;  - keterlambatan yang disebabkan oleh PPK;  "- masalah yang timbul diluar kendali penyedia; dan/atau " - Keadaan Kahar) ; Apakah Penyedia telah memberikan peringatan dini akan terjadi keterlambatan dan telah bekerja sama untuk bekerja sama untuk mencegah keterlambatan; Apakah PPK telah menetapkan ada tidaknya perpanjangan, dalam jangka waktu 21 (dua puluh satu) hari setelah penyedia meminta perpanjangan; Apakah Penetapan ada/tidak perpanjangan pelaksanaan dinyatakan secara tertulis; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	05275cf2-b282-4653-8c37-a0d7f8923a62	8d53ae6e-9502-4103-8603-ecd615b32d94	2017-10-20 01:36:04+00	\N
9b69a10b-c42f-4ebb-8fd4-4b83bc245d5f	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 10237 - Apakah Penyerahan hasil pekerjaan berdasarkan permintaan secara tertulis dari Penyedia Pekerjaan Konstruksi; Apakah Permintaan tersebut setelah pekerjaan selesai 100 % ; Apakah Panitia/Pejabat Penerima Hasil Pekerjaan melakukan pemeriksaan hasil pekerjaan Pengadaan Barang/Jasa; Apakah Melakukan pemeriksaan sesuai dengan pedoman/prosedur yang telah ditetapkan; Apakah Penyedia telah melaksanakan pekerjaan konstruksi sesuai dengan ketentuan yang tercantum dalam Kontrak dan gambar; Apakah Apabila terdapat ketidaksesuaian dengan kontrak (kekurangan-kekurangan dan/atau cacat hasil pekerjaan), penyedia telah memperbaiki/menyelesaikannya; Apakah panitia/Pejabat Penerima Hasil Pekerjaan menerima hasil Pengadaan Barang/Jasa setelah melalui pemeriksaan/pengujian; Apakah PPK menerima penyerahan pertama pekerjaan setelah seluruh hasil pekerjaan dilaksanakan sesuai dengan ketentuan Kontrak dan diterima oleh Panitia/Pejabat Penerima Hasil Pekerjaan; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	05275cf2-b282-4653-8c37-a0d7f8923a62	6b69d2bd-631b-423a-a5c4-15d7ba2adb89	2017-10-20 01:36:04+00	\N
dde48030-f9fa-4bb9-bf47-4a020aad8a1e	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Dapatkan data analisis kebutuhan; Dapatkan rencana pengadaan tahun berjalan; buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	2017-10-16 10:42:20+00	2017-10-20 01:52:36+00
6af8fe1c-3268-4f41-8f80-05c5f2f0bdac	1b162438-55b1-4b5c-ac17-5699e9dfb1ed	rencana	Anda ditugaskan pada 10237 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	05275cf2-b282-4653-8c37-a0d7f8923a62	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2017-10-20 01:36:04+00	2017-10-20 11:02:55+00
f6e8d097-8443-4036-a0f4-40a087d68975	70bf9ad1-3775-4be7-abab-9128837416ad	rencana	Anda ditugaskan pada 09173 - Lakukan entry meeting dengan pimpinan obyek	d26350c7-bc66-4098-ab11-202d883e3c3d	53983413-54bf-4a94-815e-82ccceecacc3	2017-10-16 10:42:20+00	2017-10-24 01:57:31+00
7adb0bee-c3b4-40d0-ab09-8406dcfe2d02	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 110011 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	a109ef9e-68e4-48c0-8d25-3318e4154d07	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2018-04-12 10:02:20+00	\N
5f5d1d04-1d46-4893-bb5a-017baf664751	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 8909811 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	145af5e3-abff-4780-b9cd-5b8bba415a09	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2018-04-16 05:29:19+00	\N
94cdcf6c-f869-427e-8ea4-c50e03ed239a	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 8909811 - Buat permintaan data yang terkait dengan penugasan	145af5e3-abff-4780-b9cd-5b8bba415a09	80636594-116a-44e8-b4be-e8b064986a06	2018-04-16 05:29:19+00	\N
5f5f945b-3849-451f-9e81-1f7c2123564e	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 6677 - Buat permintaan data yang terkait dengan penugasan	97ab28b5-8568-4f6f-94c5-837c3c2adb9e	51dd4682-183d-4989-84c2-18baefbd4539	2018-08-31 06:50:03+00	2018-08-31 06:56:41+00
0f947ac2-3c0e-417f-b3c8-fdd1008e9507	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 6677 - Lakukan survey terbatas IC	97ab28b5-8568-4f6f-94c5-837c3c2adb9e	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2018-08-31 06:50:03+00	2018-08-31 07:04:35+00
c185f39f-0ad6-4f5a-9816-51f66b01462f	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 70089 - Lakukan entry meeting dengan pimpinan obyek	7a0547b1-b065-42bf-a898-6228ef56fe5a	53983413-54bf-4a94-815e-82ccceecacc3	2018-08-31 07:23:12+00	\N
bbbc6bf3-531e-40d3-93d8-aa54ba423fd7	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 0002 - Dapatkan aturan yg mendasari	f21cff92-6997-48e3-a453-69bb316c132d	1b6aec97-c1c4-460e-ba0d-ebf406c3ceeb	2017-10-16 10:42:20+00	\N
0e4d5671-b555-4bf7-8da3-f34eb560405a	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 0002 - 3	f21cff92-6997-48e3-a453-69bb316c132d	3e1c2107-b0da-450d-8afa-b9380f375942	2017-10-16 10:42:20+00	\N
6b7b43fa-5897-4a19-a7e6-fda4792d178d	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 0002 - 4	f21cff92-6997-48e3-a453-69bb316c132d	73639b26-c3bc-4978-85e7-8797454f1dc7	2017-10-16 10:42:20+00	\N
93124bb3-76d9-4c1d-89d5-6110087b097e	ae3234c8-ea30-45d2-a48e-367b8c32567d	rencana	Anda ditugaskan pada 0002 - 1	f21cff92-6997-48e3-a453-69bb316c132d	1036dac7-ccb6-47fd-a958-5a5c19f3df62	2017-10-16 10:42:20+00	\N
877b4710-698a-4868-9425-d2d1ba46b418	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada  - 4	4ac7b90f-d4ad-4bba-bad1-94493366203d	73639b26-c3bc-4978-85e7-8797454f1dc7	2017-10-16 10:42:20+00	\N
72ef106c-ebf7-4e27-b33c-1c4e433c9231	222cead4-f3bf-48d7-9541-84f60c66cac5	rencana	Anda ditugaskan pada 0002 - 2	f21cff92-6997-48e3-a453-69bb316c132d	a5d15d37-4b03-4f9c-95b0-a01f970891f7	2017-10-16 10:42:20+00	\N
de4eaaab-7a9c-4f33-9414-6cf46f61cd0b	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 0917 - Lakukan entry meeting dengan pimpinan obyek	185a85fd-c805-4697-9719-21b017164891	53983413-54bf-4a94-815e-82ccceecacc3	2017-10-16 10:42:20+00	\N
a25b4380-68b9-4693-b235-0517003f7642	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 0917 - Buat permintaan data yang terkait dengan penugasan	185a85fd-c805-4697-9719-21b017164891	51dd4682-183d-4989-84c2-18baefbd4539	2017-10-16 10:42:20+00	\N
bcb613df-6f39-4f38-be12-617a396ad3ce	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 0917 - Dapatkan data siswa tiap tahun ajaran	185a85fd-c805-4697-9719-21b017164891	b7689c74-5a86-4711-b170-6deb99f7137b	2017-10-16 10:42:20+00	\N
c1efc1ce-ec2c-4283-bb26-3a12db59ee12	70bf9ad1-3775-4be7-abab-9128837416ad	rencana	Anda ditugaskan pada 09173 - Lakukan survey terbatas IC	d26350c7-bc66-4098-ab11-202d883e3c3d	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2017-10-16 10:42:20+00	\N
6863fe13-cee0-423a-b642-4255230032d1	ac513f02-a71e-4764-b78c-216b62ea590f	rencana	Anda ditugaskan pada 09173 - Buat permintaan data yang terkait dengan penugasan	d26350c7-bc66-4098-ab11-202d883e3c3d	51dd4682-183d-4989-84c2-18baefbd4539	2017-10-16 10:42:20+00	\N
c88b0075-abb9-422f-ad7c-2d4ed2fa1bd2	ac513f02-a71e-4764-b78c-216b62ea590f	rencana	Anda ditugaskan pada 09173 - Dapatkan data siswa tiap tahun ajaran	d26350c7-bc66-4098-ab11-202d883e3c3d	b7689c74-5a86-4711-b170-6deb99f7137b	2017-10-16 10:42:20+00	\N
306ce2c0-8976-478f-8659-b7b843d42965	ac513f02-a71e-4764-b78c-216b62ea590f	rencana	Anda ditugaskan pada 09173 - Dapatkan data pendapatan dan belanja sekolah	d26350c7-bc66-4098-ab11-202d883e3c3d	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	2017-10-16 10:42:20+00	\N
a5a724ea-acab-4af4-95e2-5f4ced5085e1	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 09173 - Dapatkan data aset sekolah	d26350c7-bc66-4098-ab11-202d883e3c3d	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	2017-10-16 10:42:20+00	\N
1375dfdc-af9b-4f08-b193-d2f2d43e7ca3	70bf9ad1-3775-4be7-abab-9128837416ad	rencana	Anda ditugaskan pada 09173 - Dapatkan data laporan periodik yang dibuat sekolah	d26350c7-bc66-4098-ab11-202d883e3c3d	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	2017-10-16 10:42:20+00	\N
57066981-75c4-4bdc-bb71-7dfd65c50864	70bf9ad1-3775-4be7-abab-9128837416ad	rencana	Anda ditugaskan pada 09173 - Dapatkan data total penerimaan BOS tiap tahun ajaran	d26350c7-bc66-4098-ab11-202d883e3c3d	e7a5563b-215a-44a1-a523-a1e2f47cb694	2017-10-16 10:42:20+00	\N
b1933675-6839-490c-9aaa-9e88111c7c60	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 09173 - Dapatkan data perhitungan penerimaan BOS dan bandingkan dengan jumlah siswa yang terdaftar	d26350c7-bc66-4098-ab11-202d883e3c3d	62fc3064-8e6a-4e97-a185-fb1156c0c16a	2017-10-16 10:42:20+00	\N
0e8954ab-37ef-425e-a8d2-a5b95f7ce76b	ac513f02-a71e-4764-b78c-216b62ea590f	rencana	Anda ditugaskan pada 09173 - Catat tanggal penerimaan dana BOS; bandingkan dengan ketentuan batas waktu penyaluran dana BOS ke Rekening sekolah; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	28ca9f0f-b7c0-442d-9e59-cf89b711af61	2017-10-16 10:42:20+00	\N
f2857fae-178b-4ba5-88cc-b7be093c03e3	ac513f02-a71e-4764-b78c-216b62ea590f	rencana	Anda ditugaskan pada 09173 - Dapatkan data realisasi penerimaan selain BOS; Dapatkan dasar penerimaan tersebut; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	acd1709e-5bb3-436c-b956-d1cfebc17b65	2017-10-16 10:42:20+00	\N
eee831d3-9ab4-46b2-8275-b5c6dd83a565	ac513f02-a71e-4764-b78c-216b62ea590f	rencana	Anda ditugaskan pada 09173 - Dapatkan data penerimaan ; Bandingkan dengan jumlah angka di laporan; buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	4453d1e6-05ed-4374-8344-a23915bf425b	2017-10-16 10:42:20+00	\N
4e5c3cf2-6a72-4d3c-8757-d5a630d4f551	70bf9ad1-3775-4be7-abab-9128837416ad	rencana	Anda ditugaskan pada 09173 - Dapatkan RKAS; Teliti apakah RKAS telah disusun sesuai pedoman; Buat Kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	c7aa0a46-3ae1-4c52-a708-98c17cec316e	2017-10-16 10:42:20+00	\N
bc611241-8ae7-4a74-8ffd-cd2c1a86f4f7	70bf9ad1-3775-4be7-abab-9128837416ad	rencana	Anda ditugaskan pada 09173 - Teliti catatan dalam buku kas, apakah item pengeluaran telah tercantum dalam RKAS 	d26350c7-bc66-4098-ab11-202d883e3c3d	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	2017-10-16 10:42:20+00	\N
49ca7c64-d2c5-4a75-9664-5ebfc0d971de	70bf9ad1-3775-4be7-abab-9128837416ad	rencana	Anda ditugaskan pada 09173 - Lakukan pengujian terhadap kegiatan; Dapatkan standar harga kegiatan (SBU);bandingkan biaya kegiatan dengan SBU yang berlaku; Buat simpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	2017-10-16 10:42:20+00	\N
f8dd5aca-376e-4e67-9b9a-912d5c0f9551	ac513f02-a71e-4764-b78c-216b62ea590f	rencana	Anda ditugaskan pada 09173 - Dapatkan data pengeluaran kas; Lakukan konfirmasi atas hasil kegiatan, dokumen dan data pendukung pengeluaran; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	5cbb9528-fa19-449b-a05f-ed0abfc36a00	2017-10-16 10:42:20+00	\N
3db3661c-ebf6-461c-a892-a42d6ba33d3e	ac513f02-a71e-4764-b78c-216b62ea590f	rencana	Anda ditugaskan pada 09173 - Dapatkan catatan pengeluaran; lakukan pengecekan terhadap bukti pendukung, apakah pengeluaran didukung oleh bukti yang sah ; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	c90bd32b-1b3f-4952-ad96-d498e6b56364	2017-10-16 10:42:20+00	\N
c7c198e0-5b41-4e4a-8492-b35bd97275f6	ac513f02-a71e-4764-b78c-216b62ea590f	rencana	Anda ditugaskan pada 09173 - Dapatkan data analisis kebutuhan; Dapatkan rencana pengadaan tahun berjalan; buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	2017-10-16 10:42:20+00	\N
d6b1d40b-ba5e-4f8f-ba36-8e85e503b3b4	70bf9ad1-3775-4be7-abab-9128837416ad	rencana	Anda ditugaskan pada 09173 - Dapatkan data pengadaan aset sekolah; Lakukan pengujian terhadap proses pengadaan; Bandingkan dengan peraturan pengadaan barang/jasa pemerintah; Buat Kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	336014ad-e1cd-41c4-bf97-b8438b78b12f	2017-10-16 10:42:20+00	\N
7e35cd29-5644-4c45-a173-b8f309f542e2	70bf9ad1-3775-4be7-abab-9128837416ad	rencana	Anda ditugaskan pada 09173 - Dapatkan daftar barang yang diadakan sekolah beserta harganya; Bandingkan dengan harga pasar (katalog LKPP); Lakukan klarifikasi jika ada perbedaan; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	149643dc-f2b1-4426-8f69-013f48e4d7bd	2017-10-16 10:42:20+00	\N
6b03cbd7-df66-47bf-9a4a-23f133f0297b	70bf9ad1-3775-4be7-abab-9128837416ad	rencana	Anda ditugaskan pada 09173 - Dapatkan data pengadaan barang; lakukan cek fisik apakah telah sesuai spesifikasi dan telah dimanfaatkan; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	dc315220-3e80-4d7a-841e-dda52b0a1f84	2017-10-16 10:42:20+00	\N
81614fae-b969-42c6-abe7-0a56edd99e42	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 09173 - Dapatkan dokumen pengeluaran/belanja; lakukan pengecekan terhadap barang yang dibeli; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	2017-10-16 10:42:20+00	\N
de3be073-9d43-434a-abba-4042b920c9f3	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 09173 - Dapatkan daftar aset; Lakukan pengujian apakah seluruh pengadaan telah dicatat dalam daftar aset; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	4cdccf23-dc54-4583-b560-8ae93efcd3e8	2017-10-16 10:42:20+00	\N
8eeb63e1-3d34-4c9d-8a6f-387c982daa69	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 09173 - Dapatkan daftar aset; Lakukan pengujian terhadap mutasi tambah dan kurang; Cek apakah seluruh mutasi telah dicatat; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	5d03a7f2-fbdc-4dcb-becd-75c08489febd	2017-10-16 10:42:20+00	\N
6c727196-b347-4248-a7ea-c329b6177a96	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 09173 - Dapatkan data hasil opname fisik aset; Jika tidak diperoleh data, lakukan klarifikasi kepada petugas barang; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	2017-10-16 10:42:20+00	\N
cb1c8ff9-20a4-49c6-b4ed-03f7942e1ef6	ac513f02-a71e-4764-b78c-216b62ea590f	rencana	Anda ditugaskan pada 09173 - Dapatkan data aset yang digunakan untuk operasional sekolah; Lakukan konfirmasi atau wawancara apakah terdapat aset hibah yang tidak dicatat dalam aset sekolah; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	52cc2b91-fe8b-4097-9d9e-3f81a889153f	2017-10-16 10:42:20+00	\N
7e22f31a-7f80-4961-862b-7f0696424cd5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada  - Dapatkan aturan yg mendasari	4ac7b90f-d4ad-4bba-bad1-94493366203d	1b6aec97-c1c4-460e-ba0d-ebf406c3ceeb	2017-10-16 10:42:20+00	2017-10-17 04:39:28+00
d74912a1-0f28-426d-b7b7-2f3a2dbe65f1	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Lakukan survey terbatas IC	185a85fd-c805-4697-9719-21b017164891	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2017-10-16 10:42:20+00	2017-10-20 01:16:35+00
312afbfc-965d-4b33-b08f-e98cf05dedf1	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Lakukan pembahasan Intern Pemberitahuan Hasil Pemeriksaan	185a85fd-c805-4697-9719-21b017164891	fb1b3834-b5d2-4599-8273-a49881db8d36	2017-10-16 10:42:20+00	2017-10-20 01:44:58+00
54acf144-1bc5-4ee9-a3df-17dd215c1995	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Susun draft laporan hasil pengawasan/pemeriksaan	185a85fd-c805-4697-9719-21b017164891	313a30e0-7f5b-4b15-ac3f-708e03a3c990	2017-10-16 10:42:20+00	2017-10-20 01:45:04+00
171c47db-2dc3-4f2e-b38e-fab7b4dc42ea	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Lakukan scan laporan final dalam bentuk pdf, dan upload di aplikasi	185a85fd-c805-4697-9719-21b017164891	06709067-0f75-4b63-96b3-47b501e4674c	2017-10-16 10:42:20+00	2017-10-20 01:50:21+00
659f3537-651e-4c23-a0af-c11cbaf879bd	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 10236 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	f79b7985-23bb-4156-a074-bc23365ded79	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2017-10-20 02:49:56+00	\N
ac036406-80b4-480c-9625-3a8d5f5a94f1	ac513f02-a71e-4764-b78c-216b62ea590f	rencana	Anda ditugaskan pada 09173 - Dapatkan data aset; lakukan cek fisik secara sampling terhadap keberadaan barang; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	470396d6-b06a-4147-b47b-7d55859ce577	2017-10-16 10:42:20+00	\N
2584afc4-9f4c-441f-9a4d-5de346d48988	70bf9ad1-3775-4be7-abab-9128837416ad	rencana	Anda ditugaskan pada 09173 - Teliti pengeluaran yang menjadi obyek pajak; lakukan pengecekan apakah sudah dikenakan pajak sesuai tarif; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	008749c7-efb0-47c7-b54d-6a2503ba2ebd	2017-10-16 10:42:20+00	\N
a6a1a706-d30b-4089-b509-c39ec2284783	70bf9ad1-3775-4be7-abab-9128837416ad	rencana	Anda ditugaskan pada 09173 - Dapatkan data pajak yang dipungut; Lakukan pengecekan terhadap pajak yang telah dipungut seluruhnya telah disetor; Buat kesimpulan	d26350c7-bc66-4098-ab11-202d883e3c3d	53449178-793e-47f9-892d-ac58f1375831	2017-10-16 10:42:20+00	\N
4240b27e-b13d-42c0-b6cd-4c4903ef9269	70bf9ad1-3775-4be7-abab-9128837416ad	rencana	Anda ditugaskan pada 09173 - Dapatkan data penyetoran pajak; Lakukan pengujian apakah ada pajak yang terlambat disetor sesuai ketentuan	d26350c7-bc66-4098-ab11-202d883e3c3d	37bab70a-0dd2-4786-9c24-9542445e738d	2017-10-16 10:42:20+00	\N
188fcc04-edf5-41d1-9d90-2bd1d439796f	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 09173 - Buat Daftar Temuan	d26350c7-bc66-4098-ab11-202d883e3c3d	49f6c09c-485d-458b-aae9-c3ff883f4542	2017-10-16 10:42:20+00	\N
ee882d5d-394b-4210-ba87-bce291c56fb6	dd7ae175-271f-467e-979e-8d2d709b1f0e	rencana	Anda ditugaskan pada 09173 - Lakukan pembahasan internal tim	d26350c7-bc66-4098-ab11-202d883e3c3d	64722e98-772e-4a28-94c3-03d3cb483320	2017-10-16 10:42:20+00	\N
7e1b25f5-c08f-4ec5-a81e-3a5f9d57a3cb	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 09173 - Buat Pemberitahuan Hasil Pengawasan/pemeriksaan	d26350c7-bc66-4098-ab11-202d883e3c3d	04b0f150-6dca-4d93-b29f-3eef3a302281	2017-10-16 10:42:20+00	\N
0744d34e-1cda-48ac-9332-f569e19308ba	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 09173 - Buat kuisioner kepuasan steakholder dan pernyataan independensi auditor	d26350c7-bc66-4098-ab11-202d883e3c3d	45d56475-668b-41e4-bdc0-3634c76f35a8	2017-10-16 10:42:20+00	\N
a0550022-adb8-4bcc-81b3-0cf6be33a6e6	dd7ae175-271f-467e-979e-8d2d709b1f0e	rencana	Anda ditugaskan pada 09173 - Lakukan pembahasan Intern Pemberitahuan Hasil Pemeriksaan	d26350c7-bc66-4098-ab11-202d883e3c3d	fb1b3834-b5d2-4599-8273-a49881db8d36	2017-10-16 10:42:20+00	\N
6caa440d-90c0-4ddd-a899-94b0ea53ea3e	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 09173 - Susun draft laporan hasil pengawasan/pemeriksaan	d26350c7-bc66-4098-ab11-202d883e3c3d	313a30e0-7f5b-4b15-ac3f-708e03a3c990	2017-10-16 10:42:20+00	\N
10135117-ce95-4de8-a524-fff2c96955bb	dd7ae175-271f-467e-979e-8d2d709b1f0e	rencana	Anda ditugaskan pada 09173 - Lakukan reviu terhadap konsep Laporan	d26350c7-bc66-4098-ab11-202d883e3c3d	72eed5d9-87a3-424d-abbf-e74972466494	2017-10-16 10:42:20+00	\N
51731ec0-71b0-4335-9372-d891c2965009	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	rencana	Anda ditugaskan pada 09173 - Lakukan scan laporan final dalam bentuk pdf, dan upload di aplikasi	d26350c7-bc66-4098-ab11-202d883e3c3d	06709067-0f75-4b63-96b3-47b501e4674c	2017-10-16 10:42:20+00	\N
1ca58def-0fe6-4a0d-ab39-faab3327e1b6	2a9c364b-035d-4323-a042-7f6c3a5bedf4	rencana	Anda ditugaskan pada 09174 - Lakukan survey terbatas IC	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2017-10-16 10:42:20+00	\N
32c0e9bc-af5d-49c5-8eea-1f6ea4b55ca5	29f41daa-38fd-43c3-8376-0942617e0fff	rencana	Anda ditugaskan pada 09174 - Buat permintaan data yang terkait dengan penugasan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	51dd4682-183d-4989-84c2-18baefbd4539	2017-10-16 10:42:20+00	\N
2c7f9e0c-da94-4387-b91a-baf6e21c9f32	29f41daa-38fd-43c3-8376-0942617e0fff	rencana	Anda ditugaskan pada 09174 - Dapatkan data siswa tiap tahun ajaran	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	b7689c74-5a86-4711-b170-6deb99f7137b	2017-10-16 10:42:20+00	\N
ce1b0602-6079-47b4-9f73-e5e58974f909	29f41daa-38fd-43c3-8376-0942617e0fff	rencana	Anda ditugaskan pada 09174 - Dapatkan data laporan periodik yang dibuat sekolah	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	2017-10-16 10:42:20+00	\N
59c4293d-d09b-4c73-8476-566b19886334	29f41daa-38fd-43c3-8376-0942617e0fff	rencana	Anda ditugaskan pada 09174 - Dapatkan data realisasi penerimaan selain BOS; Dapatkan dasar penerimaan tersebut; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	acd1709e-5bb3-436c-b956-d1cfebc17b65	2017-10-16 10:42:20+00	\N
e1ea7442-bb99-4d4b-8b08-b4d3da52486a	29f41daa-38fd-43c3-8376-0942617e0fff	rencana	Anda ditugaskan pada 09174 - Dapatkan data penerimaan ; Bandingkan dengan jumlah angka di laporan; buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	4453d1e6-05ed-4374-8344-a23915bf425b	2017-10-16 10:42:20+00	\N
08c757fd-f870-4212-8931-3bf6764ab85b	29f41daa-38fd-43c3-8376-0942617e0fff	rencana	Anda ditugaskan pada 09174 - Dapatkan RKAS; Teliti apakah RKAS telah disusun sesuai pedoman; Buat Kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	c7aa0a46-3ae1-4c52-a708-98c17cec316e	2017-10-16 10:42:20+00	\N
06826539-0d05-4622-94e3-523da827e546	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Teliti catatan dalam buku kas, apakah item pengeluaran telah tercantum dalam RKAS 	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	2017-10-16 10:42:20+00	\N
16c64616-8d38-4e1f-8e2d-ae89ec9d4653	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Dapatkan data pengeluaran kas; Lakukan konfirmasi atas hasil kegiatan, dokumen dan data pendukung pengeluaran; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	5cbb9528-fa19-449b-a05f-ed0abfc36a00	2017-10-16 10:42:20+00	\N
4b8e9523-ec99-4096-b4c9-300afef22d8c	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Dapatkan catatan pengeluaran; lakukan pengecekan terhadap bukti pendukung, apakah pengeluaran didukung oleh bukti yang sah ; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	c90bd32b-1b3f-4952-ad96-d498e6b56364	2017-10-16 10:42:20+00	\N
b2b16738-d111-4881-ac12-529cb92109ff	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Dapatkan data analisis kebutuhan; Dapatkan rencana pengadaan tahun berjalan; buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	2017-10-16 10:42:20+00	\N
6d3e3f33-0004-4866-89a5-04fb01791f8f	2a9c364b-035d-4323-a042-7f6c3a5bedf4	rencana	Anda ditugaskan pada 09174 - Dapatkan data pengadaan aset sekolah; Lakukan pengujian terhadap proses pengadaan; Bandingkan dengan peraturan pengadaan barang/jasa pemerintah; Buat Kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	336014ad-e1cd-41c4-bf97-b8438b78b12f	2017-10-16 10:42:20+00	\N
153f5dd0-2855-49dd-adb9-1a466efae4ed	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Dapatkan data pengadaan barang; lakukan cek fisik apakah telah sesuai spesifikasi dan telah dimanfaatkan; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	dc315220-3e80-4d7a-841e-dda52b0a1f84	2017-10-16 10:42:20+00	\N
40a3c4b1-ab2c-4425-8d32-d75de35303f5	2a9c364b-035d-4323-a042-7f6c3a5bedf4	rencana	Anda ditugaskan pada 09174 - Dapatkan dokumen pengeluaran/belanja; lakukan pengecekan terhadap barang yang dibeli; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	2017-10-16 10:42:20+00	\N
d009e382-5a4b-4f32-8952-3e18af405808	2a9c364b-035d-4323-a042-7f6c3a5bedf4	rencana	Anda ditugaskan pada 09174 - Dapatkan data hasil opname fisik aset; Jika tidak diperoleh data, lakukan klarifikasi kepada petugas barang; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	2017-10-16 10:42:20+00	\N
567ade8f-379d-4ddf-8b57-46b72c658296	29f41daa-38fd-43c3-8376-0942617e0fff	rencana	Anda ditugaskan pada 09174 - Dapatkan data aset yang digunakan untuk operasional sekolah; Lakukan konfirmasi atau wawancara apakah terdapat aset hibah yang tidak dicatat dalam aset sekolah; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	52cc2b91-fe8b-4097-9d9e-3f81a889153f	2017-10-16 10:42:20+00	\N
09dd5ea5-39e4-4d9c-8ca2-80d74e2a50a5	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Dapatkan data aset; lakukan cek fisik secara sampling terhadap keberadaan barang; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	470396d6-b06a-4147-b47b-7d55859ce577	2017-10-16 10:42:20+00	\N
e4643a39-b404-494e-9e77-8bccb4740010	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 10236 - Buat permintaan data yang terkait dengan penugasan	f79b7985-23bb-4156-a074-bc23365ded79	80636594-116a-44e8-b4be-e8b064986a06	2017-10-20 02:49:56+00	\N
182afd2a-1233-417e-9813-30ec8f4da145	29f41daa-38fd-43c3-8376-0942617e0fff	rencana	Anda ditugaskan pada 09174 - Dapatkan daftar barang yang diadakan sekolah beserta harganya; Bandingkan dengan harga pasar (katalog LKPP); Lakukan klarifikasi jika ada perbedaan; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	149643dc-f2b1-4426-8f69-013f48e4d7bd	2017-10-16 10:42:20+00	2017-10-20 03:08:11+00
eaf36cfe-288f-48a7-b755-6e3f7802c3c8	2a9c364b-035d-4323-a042-7f6c3a5bedf4	rencana	Anda ditugaskan pada 09174 - Dapatkan data pendapatan dan belanja sekolah	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	2017-10-16 10:42:20+00	2017-10-20 03:33:09+00
68a98ddb-635b-48b3-8699-eb98be70f800	2a9c364b-035d-4323-a042-7f6c3a5bedf4	rencana	Anda ditugaskan pada 09174 - Lakukan entry meeting dengan pimpinan obyek	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	53983413-54bf-4a94-815e-82ccceecacc3	2017-10-16 10:42:20+00	2017-10-20 14:06:24+00
8f189c4a-b6fe-4e48-a6db-0ab39cef3c51	2a9c364b-035d-4323-a042-7f6c3a5bedf4	rencana	Anda ditugaskan pada 09174 - Dapatkan data aset sekolah	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	2017-10-16 10:42:20+00	2017-10-20 14:08:04+00
dd5cf668-c8cf-4d39-835b-da0187379b8c	2a9c364b-035d-4323-a042-7f6c3a5bedf4	rencana	Anda ditugaskan pada 09174 - Dapatkan data total penerimaan BOS tiap tahun ajaran	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	e7a5563b-215a-44a1-a523-a1e2f47cb694	2017-10-16 10:42:20+00	2017-10-24 01:24:33+00
90232bbb-0746-4b63-a808-5f60f37237ac	2a9c364b-035d-4323-a042-7f6c3a5bedf4	rencana	Anda ditugaskan pada 09174 - Teliti pengeluaran yang menjadi obyek pajak; lakukan pengecekan apakah sudah dikenakan pajak sesuai tarif; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	008749c7-efb0-47c7-b54d-6a2503ba2ebd	2017-10-16 10:42:20+00	\N
4aa5f76a-b0bb-4ee8-9928-18522e0636c2	29f41daa-38fd-43c3-8376-0942617e0fff	rencana	Anda ditugaskan pada 09174 - Dapatkan data pajak yang dipungut; Lakukan pengecekan terhadap pajak yang telah dipungut seluruhnya telah disetor; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	53449178-793e-47f9-892d-ac58f1375831	2017-10-16 10:42:20+00	\N
b2f710d2-5dad-4b6e-bd64-37638b6aac97	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Dapatkan data penyetoran pajak; Lakukan pengujian apakah ada pajak yang terlambat disetor sesuai ketentuan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	37bab70a-0dd2-4786-9c24-9542445e738d	2017-10-16 10:42:20+00	\N
d8141306-e04f-4b32-88c8-27148938ef93	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Buat Daftar Temuan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	49f6c09c-485d-458b-aae9-c3ff883f4542	2017-10-16 10:42:20+00	\N
63e82f7b-bc3c-4311-a808-ecd1c4ad5825	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Lakukan pembahasan internal tim	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	64722e98-772e-4a28-94c3-03d3cb483320	2017-10-16 10:42:20+00	\N
bcf72949-ce6b-4b64-a161-16545bbf6141	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Buat Pemberitahuan Hasil Pengawasan/pemeriksaan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	04b0f150-6dca-4d93-b29f-3eef3a302281	2017-10-16 10:42:20+00	\N
291c7ef5-b485-44ab-8027-db708d3dc107	2a9c364b-035d-4323-a042-7f6c3a5bedf4	rencana	Anda ditugaskan pada 09174 - Dapatkan data perhitungan penerimaan BOS dan bandingkan dengan jumlah siswa yang terdaftar	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	62fc3064-8e6a-4e97-a185-fb1156c0c16a	2017-10-16 10:42:20+00	\N
cb4bce03-d980-4101-b3bd-ae1990f30d5d	2a9c364b-035d-4323-a042-7f6c3a5bedf4	rencana	Anda ditugaskan pada 09174 - Catat tanggal penerimaan dana BOS; bandingkan dengan ketentuan batas waktu penyaluran dana BOS ke Rekening sekolah; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	28ca9f0f-b7c0-442d-9e59-cf89b711af61	2017-10-16 10:42:20+00	\N
c67a2f0c-c9eb-4a8d-b398-6aad8625dc24	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Buat kuisioner kepuasan steakholder dan pernyataan independensi auditor	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	45d56475-668b-41e4-bdc0-3634c76f35a8	2017-10-16 10:42:20+00	\N
5bd6e363-aef2-4d06-8c23-d6fdaa84d1c6	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Lakukan pembahasan Intern Pemberitahuan Hasil Pemeriksaan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	fb1b3834-b5d2-4599-8273-a49881db8d36	2017-10-16 10:42:20+00	\N
4ca08643-b554-41da-a11c-60cf3239ded7	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Susun draft laporan hasil pengawasan/pemeriksaan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	313a30e0-7f5b-4b15-ac3f-708e03a3c990	2017-10-16 10:42:20+00	\N
2ee2410f-31ea-461c-82d4-2ef6b523a811	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Lakukan scan laporan final dalam bentuk pdf, dan upload di aplikasi	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	06709067-0f75-4b63-96b3-47b501e4674c	2017-10-16 10:42:20+00	\N
e6c034a6-b82a-47df-8aab-2903d123d3c6	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 09172 - Lakukan survey terbatas IC	628aabdb-1589-46bc-923b-bf2b57c3fdd6	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2017-10-16 10:42:20+00	\N
955e5d37-31c2-4c50-a2b0-9170b2096996	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 09172 - Dapatkan data aset sekolah	628aabdb-1589-46bc-923b-bf2b57c3fdd6	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	2017-10-16 10:42:20+00	\N
dce1fb01-27c7-48c4-bbf6-23bb3d8bf0ed	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 09172 - Dapatkan data realisasi penerimaan selain BOS; Dapatkan dasar penerimaan tersebut; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	acd1709e-5bb3-436c-b956-d1cfebc17b65	2017-10-16 10:42:20+00	\N
919a8df6-b80c-4ea1-b90f-74b599961f19	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 09172 - Dapatkan data penerimaan ; Bandingkan dengan jumlah angka di laporan; buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	4453d1e6-05ed-4374-8344-a23915bf425b	2017-10-16 10:42:20+00	\N
33bce1f3-3b43-448e-b310-5c543a45121a	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 09172 - Teliti catatan dalam buku kas, apakah item pengeluaran telah tercantum dalam RKAS 	628aabdb-1589-46bc-923b-bf2b57c3fdd6	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	2017-10-16 10:42:20+00	\N
0bab5ad8-de94-4b65-9e46-b4c6b3c04360	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 09172 - Lakukan pengujian terhadap kegiatan; Dapatkan standar harga kegiatan (SBU);bandingkan biaya kegiatan dengan SBU yang berlaku; Buat simpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	2017-10-16 10:42:20+00	\N
b2c899d7-95d0-41fc-b72e-2c61842ec819	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Dapatkan data pengeluaran kas; Lakukan konfirmasi atas hasil kegiatan, dokumen dan data pendukung pengeluaran; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	5cbb9528-fa19-449b-a05f-ed0abfc36a00	2017-10-16 10:42:20+00	\N
b1ee87fc-61db-49d3-abc9-7ba922a09b20	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 09172 - Dapatkan catatan pengeluaran; lakukan pengecekan terhadap bukti pendukung, apakah pengeluaran didukung oleh bukti yang sah ; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	c90bd32b-1b3f-4952-ad96-d498e6b56364	2017-10-16 10:42:20+00	\N
19c9d322-1736-4756-9f2e-d1bb7a6a1b5e	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 09172 - Dapatkan data analisis kebutuhan; Dapatkan rencana pengadaan tahun berjalan; buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	2017-10-16 10:42:20+00	\N
6586c178-4d70-408e-8cfd-629ada02ee42	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Dapatkan data pengadaan aset sekolah; Lakukan pengujian terhadap proses pengadaan; Bandingkan dengan peraturan pengadaan barang/jasa pemerintah; Buat Kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	336014ad-e1cd-41c4-bf97-b8438b78b12f	2017-10-16 10:42:20+00	\N
8e5e6d63-0bbb-4206-a0a0-803a1cdd1fd7	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 09172 - Dapatkan daftar barang yang diadakan sekolah beserta harganya; Bandingkan dengan harga pasar (katalog LKPP); Lakukan klarifikasi jika ada perbedaan; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	149643dc-f2b1-4426-8f69-013f48e4d7bd	2017-10-16 10:42:20+00	\N
cb47c3f1-fe54-4f30-9f85-3b6a64727151	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 09172 - Dapatkan data pengadaan barang; lakukan cek fisik apakah telah sesuai spesifikasi dan telah dimanfaatkan; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	dc315220-3e80-4d7a-841e-dda52b0a1f84	2017-10-16 10:42:20+00	\N
b84a1bcf-dd5e-47b8-8f27-fc0734e6afc0	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Dapatkan dokumen pengeluaran/belanja; lakukan pengecekan terhadap barang yang dibeli; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	2017-10-16 10:42:20+00	\N
04926cb9-4e8d-41e7-b12a-7009fa5daa80	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 09172 - Dapatkan daftar aset; Lakukan pengujian apakah seluruh pengadaan telah dicatat dalam daftar aset; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	4cdccf23-dc54-4583-b560-8ae93efcd3e8	2017-10-16 10:42:20+00	\N
16ea4789-00bf-4afc-9123-389280b356b9	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 09172 - Dapatkan daftar aset; Lakukan pengujian terhadap mutasi tambah dan kurang; Cek apakah seluruh mutasi telah dicatat; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	5d03a7f2-fbdc-4dcb-becd-75c08489febd	2017-10-16 10:42:20+00	\N
cfcf8e2e-95d2-4e8c-a619-03145794554c	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Dapatkan data hasil opname fisik aset; Jika tidak diperoleh data, lakukan klarifikasi kepada petugas barang; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	2017-10-16 10:42:20+00	\N
3bab5787-7f02-46b8-9ca6-aa2189308e35	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 09172 - Dapatkan data aset yang digunakan untuk operasional sekolah; Lakukan konfirmasi atau wawancara apakah terdapat aset hibah yang tidak dicatat dalam aset sekolah; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	52cc2b91-fe8b-4097-9d9e-3f81a889153f	2017-10-16 10:42:20+00	\N
9200c12d-58e0-4328-a2f6-4b70e32c23e0	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Dapatkan data aset; lakukan cek fisik secara sampling terhadap keberadaan barang; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	470396d6-b06a-4147-b47b-7d55859ce577	2017-10-16 10:42:20+00	\N
511a0277-9381-40de-b76b-332bf9afbaeb	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 09172 - Teliti pengeluaran yang menjadi obyek pajak; lakukan pengecekan apakah sudah dikenakan pajak sesuai tarif; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	008749c7-efb0-47c7-b54d-6a2503ba2ebd	2017-10-16 10:42:20+00	\N
67436f6d-4101-4351-a446-104b9de61c35	55fc1778-b710-47c8-a860-b519a03e23bf	rencana	Anda ditugaskan pada 09174 - Lakukan reviu terhadap konsep Laporan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	72eed5d9-87a3-424d-abbf-e74972466494	2017-10-16 10:42:20+00	2017-10-20 03:11:40+00
91b9af07-3cc6-4aa6-a0ff-86f4646d619a	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Dapatkan RKAS; Teliti apakah RKAS telah disusun sesuai pedoman; Buat Kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	c7aa0a46-3ae1-4c52-a708-98c17cec316e	2017-10-16 10:42:20+00	2018-11-03 05:16:38+00
181b389d-138e-4e79-8ccc-a70232548970	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 09172 - Dapatkan data pajak yang dipungut; Lakukan pengecekan terhadap pajak yang telah dipungut seluruhnya telah disetor; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	53449178-793e-47f9-892d-ac58f1375831	2017-10-16 10:42:20+00	\N
dfdcb114-0b3b-422c-9af3-cc1c22e1ffaf	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 09172 - Dapatkan data penyetoran pajak; Lakukan pengujian apakah ada pajak yang terlambat disetor sesuai ketentuan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	37bab70a-0dd2-4786-9c24-9542445e738d	2017-10-16 10:42:20+00	\N
b0348c53-1eef-4f97-befe-cc1ffdd4de75	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Buat Daftar Temuan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	49f6c09c-485d-458b-aae9-c3ff883f4542	2017-10-16 10:42:20+00	\N
2a6fa541-b4b8-403a-af1e-190e69404ae3	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Lakukan pembahasan internal tim	628aabdb-1589-46bc-923b-bf2b57c3fdd6	64722e98-772e-4a28-94c3-03d3cb483320	2017-10-16 10:42:20+00	\N
7df09911-c386-4422-aa30-ada90e2b9d38	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Buat Pemberitahuan Hasil Pengawasan/pemeriksaan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	04b0f150-6dca-4d93-b29f-3eef3a302281	2017-10-16 10:42:20+00	\N
e60158f8-0d2e-45d0-83ba-e05b92d9f557	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Buat kuisioner kepuasan steakholder dan pernyataan independensi auditor	628aabdb-1589-46bc-923b-bf2b57c3fdd6	45d56475-668b-41e4-bdc0-3634c76f35a8	2017-10-16 10:42:20+00	\N
df76415a-dab5-4347-b2e4-b6bdcdec6aef	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 09172 - Dapatkan data laporan periodik yang dibuat sekolah	628aabdb-1589-46bc-923b-bf2b57c3fdd6	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	2017-10-16 10:42:20+00	\N
f70ec87a-0402-41db-b202-9b5fb3a782b5	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 09172 - Dapatkan data total penerimaan BOS tiap tahun ajaran	628aabdb-1589-46bc-923b-bf2b57c3fdd6	e7a5563b-215a-44a1-a523-a1e2f47cb694	2017-10-16 10:42:20+00	\N
2aad770c-fa48-4abe-a016-126315e19ea2	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Susun draft laporan hasil pengawasan/pemeriksaan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	313a30e0-7f5b-4b15-ac3f-708e03a3c990	2017-10-16 10:42:20+00	\N
c6b26198-785f-4ca8-b585-b85c61c75f5f	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Lakukan scan laporan final dalam bentuk pdf, dan upload di aplikasi	628aabdb-1589-46bc-923b-bf2b57c3fdd6	06709067-0f75-4b63-96b3-47b501e4674c	2017-10-16 10:42:20+00	\N
28a6c704-8385-4ea0-9334-9d47b5a22d9b	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	rencana	Anda ditugaskan pada 09715 - Dapatkan data total penerimaan BOS tiap tahun ajaran	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	e7a5563b-215a-44a1-a523-a1e2f47cb694	2017-10-16 10:42:20+00	\N
5e20ccbb-96b8-40f8-9ebe-7ef61af1fa76	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	rencana	Anda ditugaskan pada 09715 - Dapatkan data perhitungan penerimaan BOS dan bandingkan dengan jumlah siswa yang terdaftar	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	62fc3064-8e6a-4e97-a185-fb1156c0c16a	2017-10-16 10:42:20+00	\N
651f546f-5d72-4daf-8c1e-4c87cc77bb57	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	rencana	Anda ditugaskan pada 09715 - Catat tanggal penerimaan dana BOS; bandingkan dengan ketentuan batas waktu penyaluran dana BOS ke Rekening sekolah; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	28ca9f0f-b7c0-442d-9e59-cf89b711af61	2017-10-16 10:42:20+00	\N
b214314f-b923-458a-8b1c-0a1f7d4ab572	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	rencana	Anda ditugaskan pada 09715 - Dapatkan data realisasi penerimaan selain BOS; Dapatkan dasar penerimaan tersebut; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	acd1709e-5bb3-436c-b956-d1cfebc17b65	2017-10-16 10:42:20+00	\N
612b4b54-f46a-4c24-9d5c-920a8796a316	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	rencana	Anda ditugaskan pada 09715 - Dapatkan data penerimaan ; Bandingkan dengan jumlah angka di laporan; buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	4453d1e6-05ed-4374-8344-a23915bf425b	2017-10-16 10:42:20+00	\N
ae857cd3-a03a-4256-9821-d85d943cff0e	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	rencana	Anda ditugaskan pada 09715 - Dapatkan RKAS; Teliti apakah RKAS telah disusun sesuai pedoman; Buat Kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	c7aa0a46-3ae1-4c52-a708-98c17cec316e	2017-10-16 10:42:20+00	\N
a8cf4775-9b8b-482e-b71e-fb0f71fa54a6	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	rencana	Anda ditugaskan pada 09715 - Teliti catatan dalam buku kas, apakah item pengeluaran telah tercantum dalam RKAS 	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	2017-10-16 10:42:20+00	\N
6df1245a-a130-42e2-82f6-97ea9f0a64b8	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	rencana	Anda ditugaskan pada 09715 - Lakukan pengujian terhadap kegiatan; Dapatkan standar harga kegiatan (SBU);bandingkan biaya kegiatan dengan SBU yang berlaku; Buat simpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	2017-10-16 10:42:20+00	\N
1a8627b4-d30f-4b0c-a671-6f2d372e950c	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	rencana	Anda ditugaskan pada 09715 - Dapatkan data pengeluaran kas; Lakukan konfirmasi atas hasil kegiatan, dokumen dan data pendukung pengeluaran; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	5cbb9528-fa19-449b-a05f-ed0abfc36a00	2017-10-16 10:42:20+00	\N
accc8200-4b9a-4005-aeec-49eb440ca518	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Dapatkan catatan pengeluaran; lakukan pengecekan terhadap bukti pendukung, apakah pengeluaran didukung oleh bukti yang sah ; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	c90bd32b-1b3f-4952-ad96-d498e6b56364	2017-10-16 10:42:20+00	\N
a9188ce5-10dc-40b4-8038-4a400ded7677	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Dapatkan data analisis kebutuhan; Dapatkan rencana pengadaan tahun berjalan; buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	2017-10-16 10:42:20+00	\N
4c5a741a-d54f-40a9-9d66-cfd8e5be969b	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Dapatkan data pengadaan aset sekolah; Lakukan pengujian terhadap proses pengadaan; Bandingkan dengan peraturan pengadaan barang/jasa pemerintah; Buat Kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	336014ad-e1cd-41c4-bf97-b8438b78b12f	2017-10-16 10:42:20+00	\N
8a4a557f-5ee3-4c67-858e-e0aeabab1160	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	rencana	Anda ditugaskan pada 09715 - Dapatkan dokumen pengeluaran/belanja; lakukan pengecekan terhadap barang yang dibeli; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	2017-10-16 10:42:20+00	\N
d4647656-b2df-4455-b796-fdb471e3e0ef	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	rencana	Anda ditugaskan pada 09715 - Dapatkan daftar aset; Lakukan pengujian apakah seluruh pengadaan telah dicatat dalam daftar aset; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	4cdccf23-dc54-4583-b560-8ae93efcd3e8	2017-10-16 10:42:20+00	\N
7008d06f-8712-4213-af1a-f93d30af5ae3	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Dapatkan daftar aset; Lakukan pengujian terhadap mutasi tambah dan kurang; Cek apakah seluruh mutasi telah dicatat; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	5d03a7f2-fbdc-4dcb-becd-75c08489febd	2017-10-16 10:42:20+00	\N
73db3d77-d555-4225-b2d7-28665cd22529	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Lakukan pembahasan Intern Pemberitahuan Hasil Pemeriksaan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	fb1b3834-b5d2-4599-8273-a49881db8d36	2017-10-16 10:42:20+00	2018-09-01 02:20:55+00
7ff08ad0-e539-4f7c-8f9f-c68879daab98	998cdf4a-6b17-468e-9c07-186e1a724f6d	rencana	Anda ditugaskan pada 09715 - Lakukan survey terbatas IC	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2017-10-16 10:42:20+00	2018-09-14 01:15:15+00
3922b887-2aee-41b9-ad3e-8eb61e43e736	998cdf4a-6b17-468e-9c07-186e1a724f6d	rencana	Anda ditugaskan pada 09715 - Buat permintaan data yang terkait dengan penugasan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	51dd4682-183d-4989-84c2-18baefbd4539	2017-10-16 10:42:20+00	2018-09-14 01:30:15+00
4b00724c-7dca-4d19-9517-4d9bbf9686ab	998cdf4a-6b17-468e-9c07-186e1a724f6d	rencana	Anda ditugaskan pada 09715 - Dapatkan data siswa tiap tahun ajaran	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	b7689c74-5a86-4711-b170-6deb99f7137b	2017-10-16 10:42:20+00	2018-09-14 01:38:31+00
80725cb7-4380-4219-b96f-8dae495ae974	998cdf4a-6b17-468e-9c07-186e1a724f6d	rencana	Anda ditugaskan pada 09715 - Dapatkan data aset sekolah	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	2017-10-16 10:42:20+00	2018-09-14 01:38:49+00
b70b28ee-46d6-41f0-8c58-a6b97761750d	998cdf4a-6b17-468e-9c07-186e1a724f6d	rencana	Anda ditugaskan pada 09715 - Dapatkan data laporan periodik yang dibuat sekolah	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	2017-10-16 10:42:20+00	2018-09-14 01:39:01+00
10299bda-ae00-41db-922c-0c589a753171	998cdf4a-6b17-468e-9c07-186e1a724f6d	rencana	Anda ditugaskan pada 09715 - Lakukan entry meeting dengan pimpinan obyek	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	53983413-54bf-4a94-815e-82ccceecacc3	2017-10-16 10:42:20+00	2018-09-14 01:39:20+00
f4f9c972-0f5b-4dcf-bf03-612b7d8f8f06	998cdf4a-6b17-468e-9c07-186e1a724f6d	rencana	Anda ditugaskan pada 09715 - Dapatkan data pengadaan barang; lakukan cek fisik apakah telah sesuai spesifikasi dan telah dimanfaatkan; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	dc315220-3e80-4d7a-841e-dda52b0a1f84	2017-10-16 10:42:20+00	2018-09-14 01:39:31+00
b29d201f-d7f5-4b83-970a-8526ef9f4902	998cdf4a-6b17-468e-9c07-186e1a724f6d	rencana	Anda ditugaskan pada 09715 - Dapatkan data pendapatan dan belanja sekolah	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	2017-10-16 10:42:20+00	2018-09-14 01:39:47+00
48750eb3-5a71-40c7-b95d-74778a13d0ee	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Dapatkan data hasil opname fisik aset; Jika tidak diperoleh data, lakukan klarifikasi kepada petugas barang; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	2017-10-16 10:42:20+00	\N
f4165f2e-e3cb-44ad-8065-670bdec499de	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Dapatkan data aset yang digunakan untuk operasional sekolah; Lakukan konfirmasi atau wawancara apakah terdapat aset hibah yang tidak dicatat dalam aset sekolah; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	52cc2b91-fe8b-4097-9d9e-3f81a889153f	2017-10-16 10:42:20+00	\N
72c19edb-c411-4e5a-acfe-4553164c203c	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Dapatkan data aset; lakukan cek fisik secara sampling terhadap keberadaan barang; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	470396d6-b06a-4147-b47b-7d55859ce577	2017-10-16 10:42:20+00	\N
3c4a2048-f27c-49f4-b4c6-dc109647ea2c	b5d26e6b-30f7-46a3-b7fe-03b66a235d86	rencana	Anda ditugaskan pada 09715 - Dapatkan data pajak yang dipungut; Lakukan pengecekan terhadap pajak yang telah dipungut seluruhnya telah disetor; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	53449178-793e-47f9-892d-ac58f1375831	2017-10-16 10:42:20+00	\N
5e7baf23-1ab4-412d-8177-847155766599	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Dapatkan data penyetoran pajak; Lakukan pengujian apakah ada pajak yang terlambat disetor sesuai ketentuan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	37bab70a-0dd2-4786-9c24-9542445e738d	2017-10-16 10:42:20+00	\N
6552957e-5f9a-4a88-9f9e-9df595689a12	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Buat Daftar Temuan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	49f6c09c-485d-458b-aae9-c3ff883f4542	2017-10-16 10:42:20+00	\N
c2252de9-94df-417b-ba38-b809d8cef8d3	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Lakukan pembahasan internal tim	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	64722e98-772e-4a28-94c3-03d3cb483320	2017-10-16 10:42:20+00	\N
1b96068c-ecd8-480c-b090-ff566656ec26	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Buat Pemberitahuan Hasil Pengawasan/pemeriksaan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	04b0f150-6dca-4d93-b29f-3eef3a302281	2017-10-16 10:42:20+00	\N
171c224c-4355-400d-91f1-699db167fec5	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Buat kuisioner kepuasan steakholder dan pernyataan independensi auditor	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	45d56475-668b-41e4-bdc0-3634c76f35a8	2017-10-16 10:42:20+00	\N
a9113c45-1fb7-4e9d-8199-e32a10b7a71f	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Lakukan pembahasan Intern Pemberitahuan Hasil Pemeriksaan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	fb1b3834-b5d2-4599-8273-a49881db8d36	2017-10-16 10:42:20+00	\N
62cc146c-c9d7-49df-9723-33fbf2b87a47	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Susun draft laporan hasil pengawasan/pemeriksaan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	313a30e0-7f5b-4b15-ac3f-708e03a3c990	2017-10-16 10:42:20+00	\N
1110fc4c-6b27-49f9-b2be-81ea84bd864c	e54dd636-fb29-4ada-b95c-848c39ed6d03	rencana	Anda ditugaskan pada 09715 - Lakukan scan laporan final dalam bentuk pdf, dan upload di aplikasi	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	06709067-0f75-4b63-96b3-47b501e4674c	2017-10-16 10:42:20+00	\N
7150901c-b63c-4845-883d-0f921aed324c	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Lakukan survey terbatas IC	9cb18ed2-4658-481f-9f74-e40105dc188b	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2017-10-16 10:42:20+00	\N
6fc9498a-bf42-41e3-b7a2-5e956c99813e	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Buat permintaan data yang terkait dengan penugasan	9cb18ed2-4658-481f-9f74-e40105dc188b	51dd4682-183d-4989-84c2-18baefbd4539	2017-10-16 10:42:20+00	\N
26b1aaa8-6a1e-499c-9b3b-db9afacf1b50	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Dapatkan data siswa tiap tahun ajaran	9cb18ed2-4658-481f-9f74-e40105dc188b	b7689c74-5a86-4711-b170-6deb99f7137b	2017-10-16 10:42:20+00	\N
6a2e9ccd-f8b2-4db6-bd32-14c539f9dbf8	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Dapatkan data aset sekolah	9cb18ed2-4658-481f-9f74-e40105dc188b	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	2017-10-16 10:42:20+00	\N
6cb5ad2d-621d-410f-9a72-75c7c88dc4a1	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Dapatkan data total penerimaan BOS tiap tahun ajaran	9cb18ed2-4658-481f-9f74-e40105dc188b	e7a5563b-215a-44a1-a523-a1e2f47cb694	2017-10-16 10:42:20+00	\N
f9ab7b3d-1954-46c6-8bd2-e9ef8c776d9a	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Dapatkan data perhitungan penerimaan BOS dan bandingkan dengan jumlah siswa yang terdaftar	9cb18ed2-4658-481f-9f74-e40105dc188b	62fc3064-8e6a-4e97-a185-fb1156c0c16a	2017-10-16 10:42:20+00	\N
b73cfb9f-52a6-4e64-8b4b-2cd3e7c7dec3	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Catat tanggal penerimaan dana BOS; bandingkan dengan ketentuan batas waktu penyaluran dana BOS ke Rekening sekolah; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	28ca9f0f-b7c0-442d-9e59-cf89b711af61	2017-10-16 10:42:20+00	\N
234d51d2-c44d-449b-bc40-5a4f031a9c7b	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Dapatkan data realisasi penerimaan selain BOS; Dapatkan dasar penerimaan tersebut; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	acd1709e-5bb3-436c-b956-d1cfebc17b65	2017-10-16 10:42:20+00	\N
4573036d-e94b-42e0-afcb-e2f91f0624da	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Dapatkan data penerimaan ; Bandingkan dengan jumlah angka di laporan; buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	4453d1e6-05ed-4374-8344-a23915bf425b	2017-10-16 10:42:20+00	\N
189ca916-1226-457e-bbd9-22fa36ed2a5d	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Dapatkan RKAS; Teliti apakah RKAS telah disusun sesuai pedoman; Buat Kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	c7aa0a46-3ae1-4c52-a708-98c17cec316e	2017-10-16 10:42:20+00	\N
2aad7cb0-2648-46ef-85ee-b0d988f2e8df	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Teliti catatan dalam buku kas, apakah item pengeluaran telah tercantum dalam RKAS 	9cb18ed2-4658-481f-9f74-e40105dc188b	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	2017-10-16 10:42:20+00	\N
0111d85e-ad76-4ff1-9011-1f6bad90b1c8	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Lakukan pengujian terhadap kegiatan; Dapatkan standar harga kegiatan (SBU);bandingkan biaya kegiatan dengan SBU yang berlaku; Buat simpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	2017-10-16 10:42:20+00	\N
b4c72a27-eac9-428c-966f-a04a4da943c5	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Dapatkan data pengeluaran kas; Lakukan konfirmasi atas hasil kegiatan, dokumen dan data pendukung pengeluaran; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	5cbb9528-fa19-449b-a05f-ed0abfc36a00	2017-10-16 10:42:20+00	\N
5558f9ed-f19a-423c-8d00-42d449c34ca2	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Dapatkan catatan pengeluaran; lakukan pengecekan terhadap bukti pendukung, apakah pengeluaran didukung oleh bukti yang sah ; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	c90bd32b-1b3f-4952-ad96-d498e6b56364	2017-10-16 10:42:20+00	\N
914743c4-580f-41e0-a099-f0fc83a466b0	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Dapatkan data analisis kebutuhan; Dapatkan rencana pengadaan tahun berjalan; buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	2017-10-16 10:42:20+00	\N
2e8d1498-df65-4374-99d0-95df6678e2d9	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Dapatkan data pengadaan aset sekolah; Lakukan pengujian terhadap proses pengadaan; Bandingkan dengan peraturan pengadaan barang/jasa pemerintah; Buat Kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	336014ad-e1cd-41c4-bf97-b8438b78b12f	2017-10-16 10:42:20+00	\N
d9100f90-1582-4b97-ad17-1232bbf7b788	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Dapatkan daftar barang yang diadakan sekolah beserta harganya; Bandingkan dengan harga pasar (katalog LKPP); Lakukan klarifikasi jika ada perbedaan; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	149643dc-f2b1-4426-8f69-013f48e4d7bd	2017-10-16 10:42:20+00	\N
0e1a0433-b608-4796-b42a-2d097f592a32	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Dapatkan data pengadaan barang; lakukan cek fisik apakah telah sesuai spesifikasi dan telah dimanfaatkan; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	dc315220-3e80-4d7a-841e-dda52b0a1f84	2017-10-16 10:42:20+00	\N
348c7c75-7c2d-40ed-bae2-3dd65ea427be	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Lakukan entry meeting dengan pimpinan obyek	9cb18ed2-4658-481f-9f74-e40105dc188b	53983413-54bf-4a94-815e-82ccceecacc3	2017-10-16 10:42:20+00	2017-10-17 02:45:48+00
2684d692-1a68-46ee-8c2b-0add221a2490	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Dapatkan data pendapatan dan belanja sekolah	9cb18ed2-4658-481f-9f74-e40105dc188b	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	2017-10-16 10:42:20+00	2017-10-20 00:52:04+00
ad2765db-304b-4796-801e-5b191e9094fd	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	rencana	Anda ditugaskan pada 09715 - Lakukan reviu terhadap konsep Laporan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	72eed5d9-87a3-424d-abbf-e74972466494	2017-10-16 10:42:20+00	2018-01-16 06:46:46+00
1e5d4b66-594a-4ef3-87db-e88536ee8af6	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 6677 - Lakukan entry meeting dengan pimpinan obyek	97ab28b5-8568-4f6f-94c5-837c3c2adb9e	53983413-54bf-4a94-815e-82ccceecacc3	2018-08-31 06:50:03+00	2018-08-31 06:50:45+00
762096b3-e0af-467a-9165-ce346c8b909c	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Dapatkan dokumen pengeluaran/belanja; lakukan pengecekan terhadap barang yang dibeli; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	2017-10-16 10:42:20+00	\N
e4baedc7-e3a7-4da0-81b3-87be49d3e63c	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Dapatkan daftar aset; Lakukan pengujian apakah seluruh pengadaan telah dicatat dalam daftar aset; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	4cdccf23-dc54-4583-b560-8ae93efcd3e8	2017-10-16 10:42:20+00	\N
0d6e894c-428d-4925-a1a1-e7f9b0453fe5	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Dapatkan daftar aset; Lakukan pengujian terhadap mutasi tambah dan kurang; Cek apakah seluruh mutasi telah dicatat; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	5d03a7f2-fbdc-4dcb-becd-75c08489febd	2017-10-16 10:42:20+00	\N
8a5be181-d1ce-48ef-9415-2e895a90f5fa	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Dapatkan data hasil opname fisik aset; Jika tidak diperoleh data, lakukan klarifikasi kepada petugas barang; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	2017-10-16 10:42:20+00	\N
3f893880-ee30-4430-aed0-9ce975c43d92	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Dapatkan data aset yang digunakan untuk operasional sekolah; Lakukan konfirmasi atau wawancara apakah terdapat aset hibah yang tidak dicatat dalam aset sekolah; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	52cc2b91-fe8b-4097-9d9e-3f81a889153f	2017-10-16 10:42:20+00	\N
60e9bfd0-45b0-47ad-a79c-cc1928ecd69d	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Dapatkan data aset; lakukan cek fisik secara sampling terhadap keberadaan barang; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	470396d6-b06a-4147-b47b-7d55859ce577	2017-10-16 10:42:20+00	\N
f3993369-19af-42ed-b284-7ddbaa98b48b	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Teliti pengeluaran yang menjadi obyek pajak; lakukan pengecekan apakah sudah dikenakan pajak sesuai tarif; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	008749c7-efb0-47c7-b54d-6a2503ba2ebd	2017-10-16 10:42:20+00	\N
1fc67cd4-7d72-42f4-b874-d7e0a018bb5f	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Dapatkan data pajak yang dipungut; Lakukan pengecekan terhadap pajak yang telah dipungut seluruhnya telah disetor; Buat kesimpulan	9cb18ed2-4658-481f-9f74-e40105dc188b	53449178-793e-47f9-892d-ac58f1375831	2017-10-16 10:42:20+00	\N
365b19b4-a5d3-4533-b1e9-4f9dbfe96381	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Dapatkan data penyetoran pajak; Lakukan pengujian apakah ada pajak yang terlambat disetor sesuai ketentuan	9cb18ed2-4658-481f-9f74-e40105dc188b	37bab70a-0dd2-4786-9c24-9542445e738d	2017-10-16 10:42:20+00	\N
2e865168-60a3-403f-96d3-7c0aeb4a99a9	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Buat Daftar Temuan	9cb18ed2-4658-481f-9f74-e40105dc188b	49f6c09c-485d-458b-aae9-c3ff883f4542	2017-10-16 10:42:20+00	\N
f88ac9b4-9da1-4573-a36b-dea59484ab44	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Lakukan pembahasan internal tim	9cb18ed2-4658-481f-9f74-e40105dc188b	64722e98-772e-4a28-94c3-03d3cb483320	2017-10-16 10:42:20+00	\N
8ba5fb43-6125-4123-85ec-114aec34fc3c	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Buat Pemberitahuan Hasil Pengawasan/pemeriksaan	9cb18ed2-4658-481f-9f74-e40105dc188b	04b0f150-6dca-4d93-b29f-3eef3a302281	2017-10-16 10:42:20+00	\N
be9c63c0-a4c9-4de8-a2f3-14e98ddc5219	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Buat kuisioner kepuasan steakholder dan pernyataan independensi auditor	9cb18ed2-4658-481f-9f74-e40105dc188b	45d56475-668b-41e4-bdc0-3634c76f35a8	2017-10-16 10:42:20+00	\N
cb9190ff-6174-4731-9637-bbb12eb01704	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Lakukan pembahasan Intern Pemberitahuan Hasil Pemeriksaan	9cb18ed2-4658-481f-9f74-e40105dc188b	fb1b3834-b5d2-4599-8273-a49881db8d36	2017-10-16 10:42:20+00	\N
b3d27aff-7951-4895-bfcf-7ed748883a52	709e3413-c729-4435-ae26-eff4cb99f23b	rencana	Anda ditugaskan pada 09176 - Susun draft laporan hasil pengawasan/pemeriksaan	9cb18ed2-4658-481f-9f74-e40105dc188b	313a30e0-7f5b-4b15-ac3f-708e03a3c990	2017-10-16 10:42:20+00	\N
52ee6fe9-4d3f-4139-831c-73e7752045ca	a30f21b6-918e-421d-835c-fac8228feed5	rencana	Anda ditugaskan pada 09176 - Lakukan reviu terhadap konsep Laporan	9cb18ed2-4658-481f-9f74-e40105dc188b	72eed5d9-87a3-424d-abbf-e74972466494	2017-10-16 10:42:20+00	\N
11acf966-82be-4b36-9b30-bf59345f7232	29fe44b6-4bc3-4de2-b917-2b15713b546d	rencana	Anda ditugaskan pada 09176 - Lakukan scan laporan final dalam bentuk pdf, dan upload di aplikasi	9cb18ed2-4658-481f-9f74-e40105dc188b	06709067-0f75-4b63-96b3-47b501e4674c	2017-10-16 10:42:20+00	\N
a1972c39-c505-4665-84dd-858fb3219d2c	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Lakukan entry meeting dengan pimpinan obyek	4114359b-983c-4020-a06f-65a7c7913b15	53983413-54bf-4a94-815e-82ccceecacc3	2017-10-16 10:42:20+00	\N
2935b538-163e-4fba-b579-a39fe1e563ca	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Lakukan survey terbatas IC	4114359b-983c-4020-a06f-65a7c7913b15	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2017-10-16 10:42:20+00	\N
b480598f-8d8b-47b4-a48e-1ff1792113e7	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Buat permintaan data yang terkait dengan penugasan	4114359b-983c-4020-a06f-65a7c7913b15	51dd4682-183d-4989-84c2-18baefbd4539	2017-10-16 10:42:20+00	\N
f2492e1d-51ac-4024-849d-b74d90ade671	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Dapatkan data siswa tiap tahun ajaran	4114359b-983c-4020-a06f-65a7c7913b15	b7689c74-5a86-4711-b170-6deb99f7137b	2017-10-16 10:42:20+00	\N
bbc1bc93-1e24-464d-852b-6d0f3ffa4b8e	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Dapatkan data pendapatan dan belanja sekolah	4114359b-983c-4020-a06f-65a7c7913b15	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	2017-10-16 10:42:20+00	\N
840df661-401e-47e5-b4b0-9a12f02f8b3c	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Dapatkan data aset sekolah	4114359b-983c-4020-a06f-65a7c7913b15	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	2017-10-16 10:42:20+00	\N
7b0a7c44-ae60-4344-b53e-b87625c5a73e	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Dapatkan data total penerimaan BOS tiap tahun ajaran	4114359b-983c-4020-a06f-65a7c7913b15	e7a5563b-215a-44a1-a523-a1e2f47cb694	2017-10-16 10:42:20+00	\N
6a2321c3-035b-4339-bbec-be371433a6ef	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Dapatkan data perhitungan penerimaan BOS dan bandingkan dengan jumlah siswa yang terdaftar	4114359b-983c-4020-a06f-65a7c7913b15	62fc3064-8e6a-4e97-a185-fb1156c0c16a	2017-10-16 10:42:20+00	\N
40dae8f1-db8e-4fb0-a83f-5ce968ba0137	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Catat tanggal penerimaan dana BOS; bandingkan dengan ketentuan batas waktu penyaluran dana BOS ke Rekening sekolah; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	28ca9f0f-b7c0-442d-9e59-cf89b711af61	2017-10-16 10:42:20+00	\N
0789c29d-f9b6-45ad-98bd-66d43cbd9684	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Dapatkan data realisasi penerimaan selain BOS; Dapatkan dasar penerimaan tersebut; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	acd1709e-5bb3-436c-b956-d1cfebc17b65	2017-10-16 10:42:20+00	\N
dad28c90-95d8-4694-9f6d-aa6fa7de8a86	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Dapatkan data penerimaan ; Bandingkan dengan jumlah angka di laporan; buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	4453d1e6-05ed-4374-8344-a23915bf425b	2017-10-16 10:42:20+00	\N
a27a5955-2272-4ecb-a859-3ffe5ebb8c5f	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Dapatkan RKAS; Teliti apakah RKAS telah disusun sesuai pedoman; Buat Kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	c7aa0a46-3ae1-4c52-a708-98c17cec316e	2017-10-16 10:42:20+00	\N
891f332d-6ac8-431e-8057-f48016f4d2bb	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Teliti catatan dalam buku kas, apakah item pengeluaran telah tercantum dalam RKAS 	4114359b-983c-4020-a06f-65a7c7913b15	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	2017-10-16 10:42:20+00	\N
1c375634-d3ad-44d1-a49d-ec19a8010d45	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Lakukan pengujian terhadap kegiatan; Dapatkan standar harga kegiatan (SBU);bandingkan biaya kegiatan dengan SBU yang berlaku; Buat simpulan	4114359b-983c-4020-a06f-65a7c7913b15	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	2017-10-16 10:42:20+00	\N
df0fe189-d5aa-41eb-8105-b8525da7e32d	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Dapatkan data pengeluaran kas; Lakukan konfirmasi atas hasil kegiatan, dokumen dan data pendukung pengeluaran; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	5cbb9528-fa19-449b-a05f-ed0abfc36a00	2017-10-16 10:42:20+00	\N
0245b08c-f3f1-46b2-9a37-aae04f442dd3	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Dapatkan catatan pengeluaran; lakukan pengecekan terhadap bukti pendukung, apakah pengeluaran didukung oleh bukti yang sah ; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	c90bd32b-1b3f-4952-ad96-d498e6b56364	2017-10-16 10:42:20+00	\N
10257bfc-3592-4e4a-af07-0583c6143513	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Dapatkan data analisis kebutuhan; Dapatkan rencana pengadaan tahun berjalan; buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	2017-10-16 10:42:20+00	\N
3dd5f1ee-87d7-4237-b424-9f834affe68e	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Dapatkan data pengadaan aset sekolah; Lakukan pengujian terhadap proses pengadaan; Bandingkan dengan peraturan pengadaan barang/jasa pemerintah; Buat Kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	336014ad-e1cd-41c4-bf97-b8438b78b12f	2017-10-16 10:42:20+00	\N
2a55bc0a-a007-4b56-abb8-ed62266ef169	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Dapatkan daftar barang yang diadakan sekolah beserta harganya; Bandingkan dengan harga pasar (katalog LKPP); Lakukan klarifikasi jika ada perbedaan; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	149643dc-f2b1-4426-8f69-013f48e4d7bd	2017-10-16 10:42:20+00	\N
744fd8b5-8724-4e1b-b559-3e2f96191862	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Dapatkan data pengadaan barang; lakukan cek fisik apakah telah sesuai spesifikasi dan telah dimanfaatkan; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	dc315220-3e80-4d7a-841e-dda52b0a1f84	2017-10-16 10:42:20+00	\N
c007a7f8-44cb-4e90-85e9-197443eba5c0	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Dapatkan dokumen pengeluaran/belanja; lakukan pengecekan terhadap barang yang dibeli; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	2017-10-16 10:42:20+00	\N
822839cc-3449-4cf0-ba5f-924d3fdaa454	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Dapatkan daftar aset; Lakukan pengujian apakah seluruh pengadaan telah dicatat dalam daftar aset; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	4cdccf23-dc54-4583-b560-8ae93efcd3e8	2017-10-16 10:42:20+00	\N
23a7ed83-9c97-4c02-b674-058fd92429ac	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Dapatkan daftar aset; Lakukan pengujian terhadap mutasi tambah dan kurang; Cek apakah seluruh mutasi telah dicatat; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	5d03a7f2-fbdc-4dcb-becd-75c08489febd	2017-10-16 10:42:20+00	\N
a14cfa12-5687-496d-b243-241b5aba1d2a	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Dapatkan data hasil opname fisik aset; Jika tidak diperoleh data, lakukan klarifikasi kepada petugas barang; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	2017-10-16 10:42:20+00	\N
77b3960c-43a4-4b34-a727-c9b358cc4986	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Dapatkan data aset yang digunakan untuk operasional sekolah; Lakukan konfirmasi atau wawancara apakah terdapat aset hibah yang tidak dicatat dalam aset sekolah; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	52cc2b91-fe8b-4097-9d9e-3f81a889153f	2017-10-16 10:42:20+00	\N
8f5b8bd7-934a-4dd1-9afb-65262c8a1cf4	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Dapatkan data aset; lakukan cek fisik secara sampling terhadap keberadaan barang; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	470396d6-b06a-4147-b47b-7d55859ce577	2017-10-16 10:42:20+00	\N
2c5905e3-b7b0-4259-9d86-1b3e8b7191ec	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Teliti pengeluaran yang menjadi obyek pajak; lakukan pengecekan apakah sudah dikenakan pajak sesuai tarif; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	008749c7-efb0-47c7-b54d-6a2503ba2ebd	2017-10-16 10:42:20+00	\N
1e0b511c-d5af-4186-863e-254c43803aab	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Dapatkan data pajak yang dipungut; Lakukan pengecekan terhadap pajak yang telah dipungut seluruhnya telah disetor; Buat kesimpulan	4114359b-983c-4020-a06f-65a7c7913b15	53449178-793e-47f9-892d-ac58f1375831	2017-10-16 10:42:20+00	\N
f9685298-e0c9-49eb-8fb4-d06e1519e397	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Dapatkan data penyetoran pajak; Lakukan pengujian apakah ada pajak yang terlambat disetor sesuai ketentuan	4114359b-983c-4020-a06f-65a7c7913b15	37bab70a-0dd2-4786-9c24-9542445e738d	2017-10-16 10:42:20+00	\N
666a137a-6ea9-4f7e-b229-26abe95426f9	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Buat Daftar Temuan	4114359b-983c-4020-a06f-65a7c7913b15	49f6c09c-485d-458b-aae9-c3ff883f4542	2017-10-16 10:42:20+00	\N
91a8ef0e-476a-429f-84ec-7532da5d652b	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Lakukan pembahasan internal tim	4114359b-983c-4020-a06f-65a7c7913b15	64722e98-772e-4a28-94c3-03d3cb483320	2017-10-16 10:42:20+00	\N
8610a4ce-e31d-4341-929f-e8895ae80ed9	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Buat Pemberitahuan Hasil Pengawasan/pemeriksaan	4114359b-983c-4020-a06f-65a7c7913b15	04b0f150-6dca-4d93-b29f-3eef3a302281	2017-10-16 10:42:20+00	\N
904fba8a-caeb-4967-800e-dbc100bf4f8d	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Buat kuisioner kepuasan steakholder dan pernyataan independensi auditor	4114359b-983c-4020-a06f-65a7c7913b15	45d56475-668b-41e4-bdc0-3634c76f35a8	2017-10-16 10:42:20+00	\N
81f7ff8f-731d-4e3b-99b7-ccbcb6b7b245	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Lakukan pembahasan Intern Pemberitahuan Hasil Pemeriksaan	4114359b-983c-4020-a06f-65a7c7913b15	fb1b3834-b5d2-4599-8273-a49881db8d36	2017-10-16 10:42:20+00	\N
34260405-7346-4dba-abfc-557534927b31	ebe58226-6813-4081-8377-b11d002e2be8	rencana	Anda ditugaskan pada 09177 - Susun draft laporan hasil pengawasan/pemeriksaan	4114359b-983c-4020-a06f-65a7c7913b15	313a30e0-7f5b-4b15-ac3f-708e03a3c990	2017-10-16 10:42:20+00	\N
cf0fa7f4-8e22-4d62-85d7-86a8baf15ba6	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 09177 - Lakukan scan laporan final dalam bentuk pdf, dan upload di aplikasi	4114359b-983c-4020-a06f-65a7c7913b15	06709067-0f75-4b63-96b3-47b501e4674c	2017-10-16 10:42:20+00	\N
4329d407-9b8f-4615-aaa6-58d91a38b873	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Lakukan entry meeting dengan pimpinan obyek	2a6993c2-d863-4fbc-8b75-40f005f7190e	53983413-54bf-4a94-815e-82ccceecacc3	2017-10-16 10:42:20+00	\N
ecbaf994-232c-486e-9699-267dacef0066	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Lakukan survey terbatas IC	2a6993c2-d863-4fbc-8b75-40f005f7190e	66b469d7-da4b-4c89-9b40-6fe3cec49a6b	2017-10-16 10:42:20+00	\N
717dfb4e-bd0f-4280-886d-6563320974c4	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Buat permintaan data yang terkait dengan penugasan	2a6993c2-d863-4fbc-8b75-40f005f7190e	51dd4682-183d-4989-84c2-18baefbd4539	2017-10-16 10:42:20+00	\N
d069154b-3a34-45e2-ba22-ef36c34ae499	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Dapatkan data siswa tiap tahun ajaran	2a6993c2-d863-4fbc-8b75-40f005f7190e	b7689c74-5a86-4711-b170-6deb99f7137b	2017-10-16 10:42:20+00	\N
b4de52cc-83ca-41bd-9d72-cec44aebd69d	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Dapatkan data pendapatan dan belanja sekolah	2a6993c2-d863-4fbc-8b75-40f005f7190e	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	2017-10-16 10:42:20+00	\N
2dc9feb3-c07e-4879-bc75-09d861501159	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Dapatkan data aset sekolah	2a6993c2-d863-4fbc-8b75-40f005f7190e	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	2017-10-16 10:42:20+00	\N
b3144af9-0f7b-4397-9970-66f90cfc0cdc	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Dapatkan data laporan periodik yang dibuat sekolah	2a6993c2-d863-4fbc-8b75-40f005f7190e	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	2017-10-16 10:42:20+00	\N
1e7aa4b3-b19d-4b0c-b1eb-bab5edf18091	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Dapatkan data total penerimaan BOS tiap tahun ajaran	2a6993c2-d863-4fbc-8b75-40f005f7190e	e7a5563b-215a-44a1-a523-a1e2f47cb694	2017-10-16 10:42:20+00	\N
26122eec-ab63-48b3-84f4-54e650a27ffb	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Dapatkan data perhitungan penerimaan BOS dan bandingkan dengan jumlah siswa yang terdaftar	2a6993c2-d863-4fbc-8b75-40f005f7190e	62fc3064-8e6a-4e97-a185-fb1156c0c16a	2017-10-16 10:42:20+00	\N
7bf6bf09-6d9a-4644-84eb-a8d31faee3ba	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Catat tanggal penerimaan dana BOS; bandingkan dengan ketentuan batas waktu penyaluran dana BOS ke Rekening sekolah; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	28ca9f0f-b7c0-442d-9e59-cf89b711af61	2017-10-16 10:42:20+00	\N
16d95640-b767-4635-93ac-5997df01de25	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Dapatkan data realisasi penerimaan selain BOS; Dapatkan dasar penerimaan tersebut; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	acd1709e-5bb3-436c-b956-d1cfebc17b65	2017-10-16 10:42:20+00	\N
70e3ebc5-43cc-42e1-b90d-24d987fed11c	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Dapatkan data penerimaan ; Bandingkan dengan jumlah angka di laporan; buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	4453d1e6-05ed-4374-8344-a23915bf425b	2017-10-16 10:42:20+00	\N
27e03603-ad2b-4652-9672-7f0fef955b5a	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Dapatkan RKAS; Teliti apakah RKAS telah disusun sesuai pedoman; Buat Kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	c7aa0a46-3ae1-4c52-a708-98c17cec316e	2017-10-16 10:42:20+00	\N
3fe0093d-0d33-4105-8c14-a300bbe9ddac	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Dapatkan data analisis kebutuhan; Dapatkan rencana pengadaan tahun berjalan; buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	2017-10-16 10:42:20+00	\N
ddb29244-b06d-4c10-abc5-aed31c574dff	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Dapatkan data pengadaan aset sekolah; Lakukan pengujian terhadap proses pengadaan; Bandingkan dengan peraturan pengadaan barang/jasa pemerintah; Buat Kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	336014ad-e1cd-41c4-bf97-b8438b78b12f	2017-10-16 10:42:20+00	\N
6f4db35d-3014-4e07-b538-be7772fc4222	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Dapatkan daftar barang yang diadakan sekolah beserta harganya; Bandingkan dengan harga pasar (katalog LKPP); Lakukan klarifikasi jika ada perbedaan; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	149643dc-f2b1-4426-8f69-013f48e4d7bd	2017-10-16 10:42:20+00	\N
bebc68b6-fd5c-48cb-8978-758995dd057f	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Dapatkan data pengadaan barang; lakukan cek fisik apakah telah sesuai spesifikasi dan telah dimanfaatkan; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	dc315220-3e80-4d7a-841e-dda52b0a1f84	2017-10-16 10:42:20+00	\N
86637d8d-7687-47eb-a2ab-7689be3dba03	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Dapatkan dokumen pengeluaran/belanja; lakukan pengecekan terhadap barang yang dibeli; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	2017-10-16 10:42:20+00	\N
d2ba6a28-9e52-4fe5-a5c7-4cf87b5843a5	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Dapatkan daftar aset; Lakukan pengujian apakah seluruh pengadaan telah dicatat dalam daftar aset; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	4cdccf23-dc54-4583-b560-8ae93efcd3e8	2017-10-16 10:42:20+00	\N
ee5f34f0-66fd-4207-be78-00a45446ebee	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Dapatkan daftar aset; Lakukan pengujian terhadap mutasi tambah dan kurang; Cek apakah seluruh mutasi telah dicatat; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	5d03a7f2-fbdc-4dcb-becd-75c08489febd	2017-10-16 10:42:20+00	\N
44907697-a933-4e22-ab07-b6befa47b99d	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Dapatkan data hasil opname fisik aset; Jika tidak diperoleh data, lakukan klarifikasi kepada petugas barang; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	2017-10-16 10:42:20+00	\N
2d8eccab-52d7-45d8-9299-168eb48d6df1	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Dapatkan data aset yang digunakan untuk operasional sekolah; Lakukan konfirmasi atau wawancara apakah terdapat aset hibah yang tidak dicatat dalam aset sekolah; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	52cc2b91-fe8b-4097-9d9e-3f81a889153f	2017-10-16 10:42:20+00	\N
410eb3d7-5b80-4f66-a2fd-e550b28f96ba	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Dapatkan data aset; lakukan cek fisik secara sampling terhadap keberadaan barang; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	470396d6-b06a-4147-b47b-7d55859ce577	2017-10-16 10:42:20+00	\N
24068f96-02ed-447e-acb8-ac5261b1a688	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Teliti pengeluaran yang menjadi obyek pajak; lakukan pengecekan apakah sudah dikenakan pajak sesuai tarif; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	008749c7-efb0-47c7-b54d-6a2503ba2ebd	2017-10-16 10:42:20+00	\N
a23aa870-d637-42aa-9749-8f9f421ca4fd	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Dapatkan data pajak yang dipungut; Lakukan pengecekan terhadap pajak yang telah dipungut seluruhnya telah disetor; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	53449178-793e-47f9-892d-ac58f1375831	2017-10-16 10:42:20+00	\N
c4dec7d3-9b1b-4106-a48a-f4e05f153df9	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Dapatkan data penyetoran pajak; Lakukan pengujian apakah ada pajak yang terlambat disetor sesuai ketentuan	2a6993c2-d863-4fbc-8b75-40f005f7190e	37bab70a-0dd2-4786-9c24-9542445e738d	2017-10-16 10:42:20+00	\N
c8056822-bba5-4390-97f9-e75c59ad4e31	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Buat Daftar Temuan	2a6993c2-d863-4fbc-8b75-40f005f7190e	49f6c09c-485d-458b-aae9-c3ff883f4542	2017-10-16 10:42:20+00	\N
2434310b-ca5a-4dc1-a273-0c5b066b43b7	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Lakukan pembahasan internal tim	2a6993c2-d863-4fbc-8b75-40f005f7190e	64722e98-772e-4a28-94c3-03d3cb483320	2017-10-16 10:42:20+00	\N
49339255-35d4-4625-86e0-d2c9d7a70f9d	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Buat Pemberitahuan Hasil Pengawasan/pemeriksaan	2a6993c2-d863-4fbc-8b75-40f005f7190e	04b0f150-6dca-4d93-b29f-3eef3a302281	2017-10-16 10:42:20+00	\N
9fc510f7-5cc0-416d-b48c-f8f890477167	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Teliti catatan dalam buku kas, apakah item pengeluaran telah tercantum dalam RKAS 	2a6993c2-d863-4fbc-8b75-40f005f7190e	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	2017-10-16 10:42:20+00	\N
6b43d5c2-047f-401a-bd80-1c7b2028d922	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 0917 - Dapatkan data pendapatan dan belanja sekolah	185a85fd-c805-4697-9719-21b017164891	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	2017-10-16 10:42:20+00	\N
e6f6e274-919c-4ab3-b44f-21f885ac246d	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 0917 - Dapatkan data aset sekolah	185a85fd-c805-4697-9719-21b017164891	62ba2b57-a6ff-4ada-ae74-9a197a94f20e	2017-10-16 10:42:20+00	\N
644d5582-648f-4afb-aa53-e753978565c4	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 0917 - Dapatkan data laporan periodik yang dibuat sekolah	185a85fd-c805-4697-9719-21b017164891	5ebea223-1051-4c3b-8518-bd4eff2f8c4b	2017-10-16 10:42:20+00	\N
b6c06ed7-0d18-483d-a242-cb3e57006c00	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 0917 - Dapatkan data total penerimaan BOS tiap tahun ajaran	185a85fd-c805-4697-9719-21b017164891	e7a5563b-215a-44a1-a523-a1e2f47cb694	2017-10-16 10:42:20+00	\N
488c0bac-7238-4e3e-a042-d3bd0f767e66	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 0917 - Dapatkan data perhitungan penerimaan BOS dan bandingkan dengan jumlah siswa yang terdaftar	185a85fd-c805-4697-9719-21b017164891	62fc3064-8e6a-4e97-a185-fb1156c0c16a	2017-10-16 10:42:20+00	\N
bcd4f588-6124-451c-9171-3006d3389bf6	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 0917 - Catat tanggal penerimaan dana BOS; bandingkan dengan ketentuan batas waktu penyaluran dana BOS ke Rekening sekolah; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	28ca9f0f-b7c0-442d-9e59-cf89b711af61	2017-10-16 10:42:20+00	\N
57dfca28-a8ad-492d-8f62-16ba5b64382e	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 0917 - Dapatkan data realisasi penerimaan selain BOS; Dapatkan dasar penerimaan tersebut; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	acd1709e-5bb3-436c-b956-d1cfebc17b65	2017-10-16 10:42:20+00	\N
d0a0d229-3645-4a90-9ad0-7fae9353f328	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 0917 - Dapatkan data penerimaan ; Bandingkan dengan jumlah angka di laporan; buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	4453d1e6-05ed-4374-8344-a23915bf425b	2017-10-16 10:42:20+00	\N
6d857990-cac1-4753-b36b-840ebae0916c	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 0917 - Dapatkan RKAS; Teliti apakah RKAS telah disusun sesuai pedoman; Buat Kesimpulan	185a85fd-c805-4697-9719-21b017164891	c7aa0a46-3ae1-4c52-a708-98c17cec316e	2017-10-16 10:42:20+00	\N
85d7c80e-bbca-4b73-bd6a-384720461cd6	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 0917 - Teliti catatan dalam buku kas, apakah item pengeluaran telah tercantum dalam RKAS 	185a85fd-c805-4697-9719-21b017164891	c3d33bb2-fde4-4953-a53e-8f490b6ab43d	2017-10-16 10:42:20+00	\N
88b6488e-9713-4bc1-954b-757b8a093fdb	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 0917 - Lakukan pengujian terhadap kegiatan; Dapatkan standar harga kegiatan (SBU);bandingkan biaya kegiatan dengan SBU yang berlaku; Buat simpulan	185a85fd-c805-4697-9719-21b017164891	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	2017-10-16 10:42:20+00	\N
d238ea35-6043-4128-b81c-0dafe9cb7585	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 0917 - Dapatkan data pengeluaran kas; Lakukan konfirmasi atas hasil kegiatan, dokumen dan data pendukung pengeluaran; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	5cbb9528-fa19-449b-a05f-ed0abfc36a00	2017-10-16 10:42:20+00	\N
5800f364-27c1-4ba2-a176-be3cca9ad173	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 0917 - Dapatkan catatan pengeluaran; lakukan pengecekan terhadap bukti pendukung, apakah pengeluaran didukung oleh bukti yang sah ; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	c90bd32b-1b3f-4952-ad96-d498e6b56364	2017-10-16 10:42:20+00	\N
7465e00a-9bcd-4fcf-821d-6a2f0a4c6a88	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 0917 - Dapatkan data pengadaan barang; lakukan cek fisik apakah telah sesuai spesifikasi dan telah dimanfaatkan; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	dc315220-3e80-4d7a-841e-dda52b0a1f84	2017-10-16 10:42:20+00	\N
77e71b45-f484-4f09-a4be-1519d96074d0	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 0917 - Dapatkan dokumen pengeluaran/belanja; lakukan pengecekan terhadap barang yang dibeli; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	2017-10-16 10:42:20+00	\N
f8e30809-08f1-46e1-9c4c-01c56a3548e1	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 0917 - Dapatkan daftar aset; Lakukan pengujian apakah seluruh pengadaan telah dicatat dalam daftar aset; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	4cdccf23-dc54-4583-b560-8ae93efcd3e8	2017-10-16 10:42:20+00	\N
fa45e6ac-4eab-400b-acce-5afd4e9cd005	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Dapatkan data pengadaan aset sekolah; Lakukan pengujian terhadap proses pengadaan; Bandingkan dengan peraturan pengadaan barang/jasa pemerintah; Buat Kesimpulan	185a85fd-c805-4697-9719-21b017164891	336014ad-e1cd-41c4-bf97-b8438b78b12f	2017-10-16 10:42:20+00	2017-10-20 01:52:45+00
ef3a6f2d-3b56-4f1b-9aa1-510c614a1f3e	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Dapatkan daftar barang yang diadakan sekolah beserta harganya; Bandingkan dengan harga pasar (katalog LKPP); Lakukan klarifikasi jika ada perbedaan; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	149643dc-f2b1-4426-8f69-013f48e4d7bd	2017-10-16 10:42:20+00	2017-10-20 01:52:54+00
80672175-6c26-4e47-b8d4-3ed92d0f653c	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 0917 - Dapatkan daftar aset; Lakukan pengujian terhadap mutasi tambah dan kurang; Cek apakah seluruh mutasi telah dicatat; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	5d03a7f2-fbdc-4dcb-becd-75c08489febd	2017-10-16 10:42:20+00	\N
5058f2de-0450-4a41-9aa5-611db61070d2	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 0917 - Teliti pengeluaran yang menjadi obyek pajak; lakukan pengecekan apakah sudah dikenakan pajak sesuai tarif; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	008749c7-efb0-47c7-b54d-6a2503ba2ebd	2017-10-16 10:42:20+00	\N
a373d8e4-0866-405a-b9d5-942b43f446ad	9d5f96cb-8d85-4216-b60e-809f39620ac6	rencana	Anda ditugaskan pada 0917 - Dapatkan data pajak yang dipungut; Lakukan pengecekan terhadap pajak yang telah dipungut seluruhnya telah disetor; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	53449178-793e-47f9-892d-ac58f1375831	2017-10-16 10:42:20+00	\N
b72701ee-fe07-4f55-8fd4-291523ce7fed	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Lakukan pengujian terhadap kegiatan; Dapatkan standar harga kegiatan (SBU);bandingkan biaya kegiatan dengan SBU yang berlaku; Buat simpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	2017-10-16 10:42:20+00	\N
9704523d-9541-4ddf-ac70-3dd332381b29	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Dapatkan data pengeluaran kas; Lakukan konfirmasi atas hasil kegiatan, dokumen dan data pendukung pengeluaran; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	5cbb9528-fa19-449b-a05f-ed0abfc36a00	2017-10-16 10:42:20+00	\N
37d4e27e-a58c-4263-aecf-25131cddf533	bf5300ba-29bf-424c-bb09-e97e0e3f07c8	rencana	Anda ditugaskan pada 09178 - Dapatkan catatan pengeluaran; lakukan pengecekan terhadap bukti pendukung, apakah pengeluaran didukung oleh bukti yang sah ; Buat kesimpulan	2a6993c2-d863-4fbc-8b75-40f005f7190e	c90bd32b-1b3f-4952-ad96-d498e6b56364	2017-10-16 10:42:20+00	\N
e36aef2e-d036-43aa-8736-a627e801d2ca	29f41daa-38fd-43c3-8376-0942617e0fff	rencana	Anda ditugaskan pada 09174 - Dapatkan daftar aset; Lakukan pengujian apakah seluruh pengadaan telah dicatat dalam daftar aset; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	4cdccf23-dc54-4583-b560-8ae93efcd3e8	2017-10-16 10:42:20+00	\N
a860b4d2-756a-47a3-841a-94feae1e4103	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	rencana	Anda ditugaskan pada 09174 - Dapatkan daftar aset; Lakukan pengujian terhadap mutasi tambah dan kurang; Cek apakah seluruh mutasi telah dicatat; Buat kesimpulan	8465eb58-4cb1-4e88-85cc-1ccfac8afffd	5d03a7f2-fbdc-4dcb-becd-75c08489febd	2017-10-16 10:42:20+00	\N
3fa0d7cf-1f76-4523-afe4-5444658a958a	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Buat kuisioner kepuasan steakholder dan pernyataan independensi auditor	2a6993c2-d863-4fbc-8b75-40f005f7190e	45d56475-668b-41e4-bdc0-3634c76f35a8	2017-10-16 10:42:20+00	\N
761de935-070b-4f01-9e68-767e73c5b281	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Lakukan pembahasan Intern Pemberitahuan Hasil Pemeriksaan	2a6993c2-d863-4fbc-8b75-40f005f7190e	fb1b3834-b5d2-4599-8273-a49881db8d36	2017-10-16 10:42:20+00	\N
e9d2755f-0881-49a2-9e66-030190b92006	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Susun draft laporan hasil pengawasan/pemeriksaan	2a6993c2-d863-4fbc-8b75-40f005f7190e	313a30e0-7f5b-4b15-ac3f-708e03a3c990	2017-10-16 10:42:20+00	\N
d105c6d2-8260-4142-aedc-fbb8bcb370fa	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	rencana	Anda ditugaskan pada 09178 - Lakukan reviu terhadap konsep Laporan	2a6993c2-d863-4fbc-8b75-40f005f7190e	72eed5d9-87a3-424d-abbf-e74972466494	2017-10-16 10:42:20+00	\N
cca13272-9ece-476d-8de9-33bd712dbd62	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 09172 - Dapatkan data siswa tiap tahun ajaran	628aabdb-1589-46bc-923b-bf2b57c3fdd6	b7689c74-5a86-4711-b170-6deb99f7137b	2017-10-16 10:42:20+00	\N
cea4adb7-f4fc-48f8-b09c-65ac06adddfb	9db069b6-02be-46d9-9d45-a89f2d741ec1	rencana	Anda ditugaskan pada 09172 - Dapatkan data pendapatan dan belanja sekolah	628aabdb-1589-46bc-923b-bf2b57c3fdd6	da8ab89d-8e7c-49d9-a4a7-362bcf83a072	2017-10-16 10:42:20+00	\N
b6777c29-a7f0-478b-b832-f8420ab18070	6f9cfc1d-dc27-4d78-8e51-51dcbf30117b	rencana	Anda ditugaskan pada 09172 - Dapatkan data perhitungan penerimaan BOS dan bandingkan dengan jumlah siswa yang terdaftar	628aabdb-1589-46bc-923b-bf2b57c3fdd6	62fc3064-8e6a-4e97-a185-fb1156c0c16a	2017-10-16 10:42:20+00	\N
f1a3d3d0-9113-4b30-bc47-279c9ab5d9c4	d016fde5-c284-4455-a44a-723ab77fa99f	rencana	Anda ditugaskan pada 09172 - Catat tanggal penerimaan dana BOS; bandingkan dengan ketentuan batas waktu penyaluran dana BOS ke Rekening sekolah; Buat kesimpulan	628aabdb-1589-46bc-923b-bf2b57c3fdd6	28ca9f0f-b7c0-442d-9e59-cf89b711af61	2017-10-16 10:42:20+00	2017-10-16 22:59:01+00
896c9c27-bc4a-4c0e-aea4-6610752e1b0b	ee570f4f-46de-4761-ae5c-b205c92d5c3e	rencana	Anda ditugaskan pada 09178 - Lakukan scan laporan final dalam bentuk pdf, dan upload di aplikasi	2a6993c2-d863-4fbc-8b75-40f005f7190e	06709067-0f75-4b63-96b3-47b501e4674c	2017-10-16 10:42:20+00	2017-10-20 01:23:04+00
b716e72d-54a5-498a-a32e-88127d93cd5e	986b958b-1a98-44e4-a59b-c02a15620381	rencana	Anda ditugaskan pada 10237 - Apakah Pembayaran terakhir dilakukan setelah : - Pekerjaan selesai 100% (seratus perseratus)  "- Berita acara penyerahan pertama pekerjaan diterbitkan; Apakah Pembayaran yang dilakukan sebesar 95% (sembilan puluh lima perseratus) dari nilai kontrak, sedangkan yang 5% (lima perseratus) merupakan retensi selama masa pemeliharaan," "Pembayaran yang dilakukan sebesar 100% (seratus perseratus) dari nilai kontrak dan penyedia harus menyerahkan Jaminan Pemeliharaan sebesar 5% (lima perseratus) dari nilai kontrak" "Masa berlaku jaminan pemeliharaan sejak tanggal serah terima pertama sampai dengan tanggal penyerahan akhir pekerjaan," "Telah dipotong denda (jika ada), pajak,uang muka dan ganti rugi (jika ada), " "Pembayaran ganti rugi kepada penyedia ( jika ada) sesuai kontrak, " "Ganti rugi dibayarkan berdasarkan data penunjang dan perhitungan kompensasi yang diajukan oleh penyedia kepada PPK, telah dapat dibuktikan kerugian nyata akibat Peristiwa Kompensasi" "Penyedia telah memberikan peringatan dini akan terjadi pengeluaran biaya tambahan dan telah bekerja sama untuk mencegah pengeluaran biaya tambahan," "Besarnya denda, pajak, uang muka sesuai dengan kontrak, " "Jika pekerjaan tidak selesai karena kesalahan atau kelalaian penyedia maka penyedia dikenakan denda. " "Besarnya denda 1/1000 dari sisa harga bagian kontrak yang belum dikerjakan, bila pekrjaan yang telah dilaksanakan dapat berfungsi," Jika pekerjaan yang telah dilaksanakan belum  berfungsi, besarnya denda 1/1000 dari harga kontrak; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	05275cf2-b282-4653-8c37-a0d7f8923a62	fd3066d2-c8c0-460e-a52c-790cd50b7469	2017-10-20 01:36:04+00	\N
10a55e42-3ef4-4aaa-a34c-352f473ff427	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Buat kuisioner kepuasan steakholder dan pernyataan independensi auditor	185a85fd-c805-4697-9719-21b017164891	45d56475-668b-41e4-bdc0-3634c76f35a8	2017-10-16 10:42:20+00	2017-10-20 01:51:04+00
2fdef91a-5d43-4d44-8b38-1ef2fd15a28d	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Buat Pemberitahuan Hasil Pengawasan/pemeriksaan	185a85fd-c805-4697-9719-21b017164891	04b0f150-6dca-4d93-b29f-3eef3a302281	2017-10-16 10:42:20+00	2017-10-20 01:51:21+00
bb871ca4-3983-4121-a287-4e21b1cfccee	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Lakukan pembahasan internal tim	185a85fd-c805-4697-9719-21b017164891	64722e98-772e-4a28-94c3-03d3cb483320	2017-10-16 10:42:20+00	2017-10-20 01:52:00+00
b25c5ee2-46b5-485f-8652-6036ff35bfb9	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Buat Daftar Temuan	185a85fd-c805-4697-9719-21b017164891	49f6c09c-485d-458b-aae9-c3ff883f4542	2017-10-16 10:42:20+00	2017-10-20 01:52:12+00
003717d5-fbf7-4e62-9b66-7a64a3a60173	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Dapatkan data penyetoran pajak; Lakukan pengujian apakah ada pajak yang terlambat disetor sesuai ketentuan	185a85fd-c805-4697-9719-21b017164891	37bab70a-0dd2-4786-9c24-9542445e738d	2017-10-16 10:42:20+00	2017-10-20 01:52:24+00
131dc100-763b-4a69-961d-43a5f0767aba	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Dapatkan data hasil opname fisik aset; Jika tidak diperoleh data, lakukan klarifikasi kepada petugas barang; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	2017-10-16 10:42:20+00	2017-10-20 01:53:04+00
4f8c2606-3f1a-417b-a889-50a70e0d1b0a	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Dapatkan data aset yang digunakan untuk operasional sekolah; Lakukan konfirmasi atau wawancara apakah terdapat aset hibah yang tidak dicatat dalam aset sekolah; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	52cc2b91-fe8b-4097-9d9e-3f81a889153f	2017-10-16 10:42:20+00	2017-10-20 01:53:14+00
b0adfa4a-c424-4ee1-ab59-e7a641ca6433	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 0917 - Dapatkan data aset; lakukan cek fisik secara sampling terhadap keberadaan barang; Buat kesimpulan	185a85fd-c805-4697-9719-21b017164891	470396d6-b06a-4147-b47b-7d55859ce577	2017-10-16 10:42:20+00	2017-10-20 01:53:33+00
c807e33d-0db1-4c4a-abb9-5fd1d2c1e620	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Dapatkan data analisis kebutuhan; Dapatkan rencana pengadaan tahun berjalan; buat kesimpulan	28f5ba79-6c18-45e1-9d23-75f047783203	58a6c1cc-590a-43cb-b914-f6dc3807b1a1	2018-09-01 00:32:59+00	\N
b33f785d-cae8-4cdf-89df-07179782f29c	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Dapatkan data pengadaan aset sekolah; Lakukan pengujian terhadap proses pengadaan; Bandingkan dengan peraturan pengadaan barang/jasa pemerintah; Buat Kesimpulan	28f5ba79-6c18-45e1-9d23-75f047783203	336014ad-e1cd-41c4-bf97-b8438b78b12f	2018-09-01 00:32:59+00	\N
c7b41968-1155-49de-ab1f-b1a41550845c	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Dapatkan daftar barang yang diadakan sekolah beserta harganya; Bandingkan dengan harga pasar (katalog LKPP); Lakukan klarifikasi jika ada perbedaan; Buat kesimpulan	28f5ba79-6c18-45e1-9d23-75f047783203	149643dc-f2b1-4426-8f69-013f48e4d7bd	2018-09-01 00:32:59+00	\N
c4de03ae-3f73-4ae5-8a99-6fa45c87febb	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Lakukan pembahasan internal tim	28f5ba79-6c18-45e1-9d23-75f047783203	64722e98-772e-4a28-94c3-03d3cb483320	2018-09-01 00:32:59+00	\N
38965346-f49b-43af-9630-05ca0b1ed338	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Buat Pemberitahuan Hasil Pengawasan/pemeriksaan	28f5ba79-6c18-45e1-9d23-75f047783203	04b0f150-6dca-4d93-b29f-3eef3a302281	2018-09-01 00:32:59+00	\N
ab818aca-4fb6-47a8-94ff-db76ebb4d7f8	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Lakukan reviu terhadap konsep Laporan	28f5ba79-6c18-45e1-9d23-75f047783203	72eed5d9-87a3-424d-abbf-e74972466494	2018-09-01 00:32:59+00	\N
8010c185-528a-483b-a1eb-6ff6022cdea5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Lakukan scan laporan final dalam bentuk pdf, dan upload di aplikasi	28f5ba79-6c18-45e1-9d23-75f047783203	06709067-0f75-4b63-96b3-47b501e4674c	2018-09-01 00:32:59+00	\N
36c79286-00ae-4584-9482-b4dcf1890e81	7a766876-545a-42a4-bb81-ca6fa59efff6	rencana	Anda ditugaskan pada 8888 - Dapatkan data perhitungan penerimaan BOS dan bandingkan dengan jumlah siswa yang terdaftar	28f5ba79-6c18-45e1-9d23-75f047783203	62fc3064-8e6a-4e97-a185-fb1156c0c16a	2018-09-01 00:32:59+00	2018-09-01 00:38:03+00
1918f6f9-aa9d-4f5e-bf5a-203445539fd3	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	rencana	Anda ditugaskan pada 8888 - Buat Daftar Temuan	28f5ba79-6c18-45e1-9d23-75f047783203	49f6c09c-485d-458b-aae9-c3ff883f4542	2018-09-01 00:32:59+00	2018-09-01 00:51:30+00
f0299226-b0a1-496f-9e12-b0cf55984886	998cdf4a-6b17-468e-9c07-186e1a724f6d	rencana	Anda ditugaskan pada 88888 - Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	9375e424-6ea6-4841-a55f-1c1f79c8c6de	2e0ee088-c298-4238-9d84-1bcac8f05b4e	2018-04-13 12:40:03+00	2018-09-14 01:21:07+00
690da9b7-14ce-4983-878c-afea685d31f2	1b162438-55b1-4b5c-ac17-5699e9dfb1ed	rencana	Anda ditugaskan pada 10237 - Teliti apakah telah dilakukan pengawasan mutu pelaksanaan kegiatan; Tuangkan dalam KKA kuisioner ; Buat kesimpulan	05275cf2-b282-4653-8c37-a0d7f8923a62	823a2b84-ca0e-42a6-9792-1c1b19d3dfb6	2017-10-20 01:36:04+00	2018-09-14 01:21:12+00
4850cbd5-d17b-4cb0-819e-07537545479f	998cdf4a-6b17-468e-9c07-186e1a724f6d	rencana	Anda ditugaskan pada 09715 - Teliti pengeluaran yang menjadi obyek pajak; lakukan pengecekan apakah sudah dikenakan pajak sesuai tarif; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	008749c7-efb0-47c7-b54d-6a2503ba2ebd	2017-10-16 10:42:20+00	2018-09-14 01:24:45+00
087a313f-6354-485e-9a67-57c794ad35a4	998cdf4a-6b17-468e-9c07-186e1a724f6d	rencana	Anda ditugaskan pada 09715 - Dapatkan daftar barang yang diadakan sekolah beserta harganya; Bandingkan dengan harga pasar (katalog LKPP); Lakukan klarifikasi jika ada perbedaan; Buat kesimpulan	e5a1cbae-fa6b-4b33-86dd-2406a4c0f4fd	149643dc-f2b1-4426-8f69-013f48e4d7bd	2017-10-16 10:42:20+00	2018-09-14 01:39:09+00
\.


--
-- Data for Name: pedoman; Type: TABLE DATA; Schema: support; Owner: eaudit
--

COPY support.pedoman (id, kode, jenis, sasaran, file, status) FROM stdin;
09b84f7c-78ec-41ff-965d-a9b9bc5b0d95	1	351c7ddd-6659-48ff-a8d0-af6848770361	bf44df83-9bbf-452e-ab81-11bff7f66f32	148c8766a5409e3befe42f7511e8f63d.pdf	t
00edc129-3e48-4da9-9c8d-69adc6b5546a	8801171	47610b8d-0476-4174-9128-996685a51354	a863e538-ff0c-4df6-a710-ae75058c750f	ba78be8292bdea6e7198e852890e3a0a.pdf	t
\.


--
-- Data for Name: program_kr; Type: TABLE DATA; Schema: support; Owner: eauditpr_user
--

COPY support.program_kr (id, resiko, kendali_s, kendali_a, urutan) FROM stdin;
56fe73e7-e3b6-4e61-a8e0-7391e92766e8	5966c178-dfb8-4433-be4f-ba473231059d	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	1
56fe73e7-e3b6-4e61-a8e0-7391e92766e8	5966c178-dfb8-4433-be4f-ba473231059d	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	1
56fe73e7-e3b6-4e61-a8e0-7391e92766e8	5966c178-dfb8-4433-be4f-ba473231059d	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	1
56fe73e7-e3b6-4e61-a8e0-7391e92766e8	5966c178-dfb8-4433-be4f-ba473231059d	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	1
56fe73e7-e3b6-4e61-a8e0-7391e92766e8	5966c178-dfb8-4433-be4f-ba473231059d	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	2
\.


--
-- Data for Name: program_satker; Type: TABLE DATA; Schema: support; Owner: eauditpr_user
--

COPY support.program_satker (id, nama, resiko, penyebab, jumlah, kendali_s, kendali_a, prosedur_audit, status, periode) FROM stdin;
a9f21b87-8af9-463b-9af2-2e19fbeb05bf	Test	5966c178-dfb8-4433-be4f-ba473231059d	94929f69-2a70-43a4-ba9d-8629aa200c49	123	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	ce8a1712-cacb-44fc-a152-6970afdf4d03	t	\N
820e63ca-b3a2-4860-86ae-2ad3f7648908	Program 2	5966c178-dfb8-4433-be4f-ba473231059d	94929f69-2a70-43a4-ba9d-8629aa200c49	12	be7c8577-7b34-4f86-a256-b014f0ba84aa	be7c8577-7b34-4f86-a256-b014f0ba84aa	ce8a1712-cacb-44fc-a152-6970afdf4d03	t	7ae49e60-c139-4744-820f-71939ed71eae
56fe73e7-e3b6-4e61-a8e0-7391e92766e8	Program Buatan 1	\N	\N	2	\N	\N	\N	t	7ae49e60-c139-4744-820f-71939ed71eae
\.


--
-- Data for Name: tao; Type: TABLE DATA; Schema: support; Owner: eaudit
--

COPY support.tao (id, kode, jenis, tujuan, status, sasaran, tahapan, langkah) FROM stdin;
737defc4-f70c-4446-b9e0-7f86c4e564ef	0095	519bbf54-6048-4a14-b184-898e521d9094	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	3	Pembahasan P2HP Dengan Obrik
a882d581-3e6c-4817-8cdb-09a7af208310	0082	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d	1	Peraturan yang berlaku tidak terfile dengan lengkap
6aa93d98-8ae8-43a2-81e8-02d4a34a2e38	0083	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d	1	Struktur organisasi tidak mendukung Tupoksi
e45456ed-9fdf-4b98-942e-727b4fc894c1	0084	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d	1	Data anggaran dan realisasi tidak terfile dengan rapi
de8c7cc5-c048-4b32-bd66-7084fee423d8	0085	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d	1	Aset tidak didata 
47a410bc-02a2-4f80-ae44-9b7d757de2c8	0086	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d	1	Permintaan data tidak dibuat
01336ab8-aa72-4f92-9849-9c607106da3b	0087	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d	1	Pembiacaraan awal tidak didokumentasikan
c6630c3c-0efb-41e0-bd96-b8b9ec0cbac6	0088	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d	1	Data pendapatan tidak akurat
6eb362ab-a8ee-4d1a-bcf7-39ac104c277c	0089	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d	1	Survey terbatas internal control
f81b0d55-9936-4f86-83c5-d1ad212e6d89	0097	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	3	Pembahasan Draft LHP
91dc6e78-7fc7-4bf9-b6ac-1b19347bfc2b	04	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	1	Data Siswa
40c90e32-c3c2-436c-9e7c-802fddbd7c80	0034	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pengadaan barang tidak efektif
a439622f-4c50-4be0-b762-b4bea0781118	0053	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pajak terlambat disetor
e82290e6-3748-4a66-b9d0-0a08a1889da7	0035	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pengadaan barang fiktif
9ad69d88-3c4e-4830-8d7d-0701cf6cf496	07	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	1	Data laporan
697e5dfb-3707-42fe-98bd-76819b2e661f	0021	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	RKAS disusun tidak sesuai ketentuan
13c67239-e6df-4c48-981d-c2ea31c1d9e5	0022	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pengeluaran tidak sesuai RKAS
65c6376e-e024-4c1f-8117-f86f64cbd090	0031	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pengadaanbarang tidak ada perencanaan
c6a82074-58ea-43e1-8214-e6c0fa82a0d6	0032	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Proses pengadaan tidak sesuai ketentuan
27cfbc71-3058-4ca8-9df0-3e3c38b20146	0041	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pengadaan barang tidak dicatat di daftar aset
ab59b878-c234-4eb8-86e3-e17c3c91c4a5	0042	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Mutasi barang tidak dicatat
dc0ff3ab-4441-4047-8d42-bb79233d3df1	0043	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Tidak pernah dilakukan opname fisik
6ed30202-3be3-417a-ac5d-53c80e43ff07	0044	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Barang hibah tidak dicatat
4546f5e4-34fc-4664-83a9-7f47a1f91d90	0045	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Barang  tidak ada fisiknya, masih tercatat dalam infentaris
45e5d931-74f5-45fc-844f-e3f6679e40ca	02	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	1	Survey terbatas internal control
a48222b7-f231-4634-b56a-6f69e0f4c18c	03	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	1	Permintaan data
61a84c91-2068-449a-9ded-4c1c4439a7cc	0033	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pengadaan barang tidak ekonomis
f7c4a805-775d-4bbe-af98-c35f732dd974	0002	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Penyaluran dana BOS ke sekolah tidak tepat waktu
34710954-64b0-430b-8779-f0c5eb306043	0003	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Penerimaan selain BOS tidak ada dasarnya
8a347078-3a23-491d-b4fd-7c6ce099dbbd	01	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	1	Entry meeting
2b6d6cd2-d4f5-44a8-99ee-5ba08dff3e99	0004	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Penerimaan tidak dicatat dan tidak dilaporkan
47ac6f23-f671-466d-9340-f5cb6f686f42	0098	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	3	laporan final
9a2b9aa1-8e6b-4557-813c-daf247521e33	0023	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pengeluaran tidak ekonomis
5336be63-e553-4d88-8b34-8f50e8b113e7	0091	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	3	Pembuatan Daftar temuan 
d0b85463-6c07-4906-aab8-3ec38dc6423a	0092	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	3	Pembahasan Internal Daftar temuan
41d52537-bd80-4226-95b9-0a641ad25b0a	0093	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	3	Penyusunan Draft P2HP 
da7856e4-eb1d-48f0-9000-284b41ab184b	0096	519bbf54-6048-4a14-b184-898e521d9094	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	3	Penyusunan Draft LHP
ad9daeb1-9c67-4560-a7f0-cd69264dd2eb	0094	519bbf54-6048-4a14-b184-898e521d9094	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	3	Kuisioner kepuasan steakholder dan pernyataan independensi
41744813-c4ed-4b02-90ac-01b744f46285	06	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	1	Data aset
890ae4ef-a151-4751-994a-b492e35a66a8	05	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	1	Data Pendapatan dan Belanja Sekolah
178730ce-8475-4419-84e2-d6d05e9e9ddc	11	ab481ec4-8498-4404-ba87-d0955bb05287	62117d42-9252-46ef-9eb9-f20d40012f3f	t	6c87e264-ce63-46b4-8e70-ec3e312aa3b1	1	Persiapan pelaksanaan Pelatihan di Kantor Sendiri (PKS)
ecad0f81-c4a9-478d-9b20-81d8f5552c8b	0024	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pengeluaran fiktif
d63907ed-677f-4b62-b82d-a556fe2b491d	0025	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pengeluaran tidak sesuai bukti yang sah
b4cad1d8-e418-47c0-b18f-a735ac36907f	0001	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Dasar Perhitungan Penerimaan BOS tidak sesuai jumlah siswa
8a67ed8c-e839-411c-9f63-2165384b4cb7	0051	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pajak tidak/kurang dipungut
c84bebc0-7b80-436d-8a63-63fb0dfba36c	0052	47610b8d-0476-4174-9128-996685a51354	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pajak tudak /kurang disetor
7d8c1621-e48a-4fe7-ab50-d1180aff679a	21	ab481ec4-8498-4404-ba87-d0955bb05287	62117d42-9252-46ef-9eb9-f20d40012f3f	t	6c87e264-ce63-46b4-8e70-ec3e312aa3b1	2	Pelaksanaan Pelatihan di Kantor Sendiri(PKS)
19dca7ad-d329-46e8-b057-7a822b746ca1	31	ab481ec4-8498-4404-ba87-d0955bb05287	62117d42-9252-46ef-9eb9-f20d40012f3f	t	6c87e264-ce63-46b4-8e70-ec3e312aa3b1	3	Laporan hasil pelaksanaan PKS
f330670d-c334-48a8-9bf0-35514fe3d1cf	20	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	1	Permintaan Data
ae5660f2-1dba-49ed-8b6c-b1935be051bf	002	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Tahapan persiapan kontrak tidak dilakukan
1b0f9300-53c5-46ab-9c66-24a969adcb94	004	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Tidak dilakukan pemantauan pelaksanaan pekerjaan
2668c7f7-3137-4d7e-ab64-80121f97175e	005	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Tidak dilakukan pengawasan mutu
e27d21f5-0b31-487e-9c5b-1560c457d551	006	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Perubahan pekerjaan tidak didukung dokumen
adeb046a-9825-44e4-b61d-8b4354004640	003	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Pemberian uang muka tdk sesuai klausul kontrak
2840a020-f00c-416b-ba28-0a91b99e63ba	001	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Isi kontrak tidak standar
a21923df-178b-4fb1-8935-f6dfa12a96bc	008	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Pembayaran prestasi kerja tidak didukung dokumen lengkap
05871b6d-b242-4f25-bbe3-c53ed23ac360	009	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Perpanjangan waktu tidak didasarkan pertimbangan yang layak dan wajar
4feddf7b-0b5e-4e62-94dc-eb4d227dc9a8	010	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Serah terima pekerjaan tidak dilengkapi data pendukung
a0017e63-252b-4733-afbf-1e3ef061414e	011	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Pembayaran prestasi pekerjaan tidak memperhitungkan prestasi, uang muka dan denda
e36f3a90-27a6-4623-84d6-0e47b4656190	012	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Serah terima akhir tidak mempertimbangkan surat permintaan, akhir masa pemeliharaan, pemenuhan syarat dan kewajiban
8be6a3b9-3f92-4e27-8c4d-e2d251b9a1d1	013	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Pengembalian retensi tidak sesuai data
827276ee-af78-4672-8f75-ccf63198aa83	014	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Keadaan kahar tidak memenuhi kriteria
8b0eb456-eb2d-4f7e-bd66-331d25fd36d7	10	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	1	Entry meeting
5985f921-9091-4fd1-9b9c-665a7839ed40	007	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Kemajuan pelaksanaan pekerjaan tidak diverifikasi
60ceabc3-35b2-49fe-99dc-7da0e99d369f	15	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Dasar pemutusan kontrak tidak memadai
0f945d17-bdb8-4ca0-9533-ad10237e20cc	16	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Penyerahan hasil pekerjaan
eff7bbb1-e049-4ee0-a980-826d354274df	17	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Pencatatan hasil pekerjaan belum dilakukan
08ce083b-a160-48b6-badc-6ac76b4a2533	18	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	2	Pemanfaatan hasil pekerjaan
a1ed94dc-3457-4803-a396-f84fb8f01dfe	19	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	3	Kesimpulan hasil monitoring dan evaluasi
73af2673-e957-4009-9347-b3378fd6461e	020	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	3	Penyusunan P2HP
14cac531-b4f0-4f60-a89f-c18e2597d64c	021	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	3	Penyusunan Draft Laporan
07dd3bbd-7528-4127-821d-2cb28c21e4b8	022	72f15fac-60fd-4108-9e82-e08357d8ecc0	c1db9e90-f731-4a62-9fb8-4fd07fa0f844	t	e6100dce-b40c-4381-9592-9fcdb90bdf05	3	Laporan final
fb38cd83-fdb2-462c-ae6e-e8f6cd5ede1f	0302	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	2ed85714-d1fe-444d-8f20-d821b58b8423	2	Jumlah transaksi
b98786e8-2537-4bdf-a56d-e7f6f8bb29a6	0303	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	2ed85714-d1fe-444d-8f20-d821b58b8423	2	Isian data transaksi tidak lengkap
8afbfffb-e3a9-484a-9a1f-5a24a9ddd5e5	0304	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	2ed85714-d1fe-444d-8f20-d821b58b8423	2	Perkalian harga dan jumlah barang salah
ef723534-297e-4ddb-97a8-f7b54f37a0a2	0305	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	2ed85714-d1fe-444d-8f20-d821b58b8423	2	Perbandingan realisasi anggaran dengan LRA tidak benar
acb8a983-45f5-4f2f-96d7-bcb9aafe0fbd	0306	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	2ed85714-d1fe-444d-8f20-d821b58b8423	2	Tanggal kontrak diluar tanggal hari kerja
f202a518-7f40-434a-b35c-cad62552fd66	0307	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	2ed85714-d1fe-444d-8f20-d821b58b8423	2	Penerimaan barang pada hari libur 
82ee9af5-f19a-42fe-b32d-e76f52939717	0308	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	2ed85714-d1fe-444d-8f20-d821b58b8423	2	Penerimaan barang mendahului tanggal kontrak
2a95fbbe-ca72-495e-b72d-6143d2c7c53f	0309	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	2ed85714-d1fe-444d-8f20-d821b58b8423	2	Resume nilai total per kontrak
2f36fc36-1fe5-49b3-b5fc-eb6230d0f711	0310	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	2ed85714-d1fe-444d-8f20-d821b58b8423	2	Salah menghitung PPN dan PPh 22 
6b05fac6-0ef9-4179-91cf-0d013c91a67f	0301	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	2ed85714-d1fe-444d-8f20-d821b58b8423	1	Pengumpulan data audit
56229775-a1c3-461d-86aa-63fa6749becc	99999	351c7ddd-6659-48ff-a8d0-af6848770361	602fa656-5eb7-42eb-b33d-27ae2bac110a	t	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d	1	kehilangan aset
53a1a73a-5275-472f-8153-b0f502321a20	0081	351c7ddd-6659-48ff-a8d0-af6848770361	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	bc3654ad-73c9-48ea-a8b7-b7c5540d7d6d	1	Kegiatan Utama Satker Tidak Terdata
1f4ee3b6-1a9d-4bf5-bbf9-2281434e6f7d	111	d49b3332-1b95-43c0-8c8f-25842cf7f537	404b5b30-1273-4e2c-a4de-a8847cb7e5c7	t	a0f66b9c-6825-4299-89c7-6d4296125b1b	1	Pengumpulan data DD/ADD/PADes
513b8fcc-2c52-4482-81d4-f55958885c6e	110	d49b3332-1b95-43c0-8c8f-25842cf7f537	404b5b30-1273-4e2c-a4de-a8847cb7e5c7	t	a0f66b9c-6825-4299-89c7-6d4296125b1b	1	Entry Meeting Desa
36f7a519-b3b6-42bf-baf2-bf05d03b2daa	210	d49b3332-1b95-43c0-8c8f-25842cf7f537	404b5b30-1273-4e2c-a4de-a8847cb7e5c7	t	a0f66b9c-6825-4299-89c7-6d4296125b1b	2	Belanja Barang Tidak ekonomis
0b659aed-9e3a-468f-9fe5-fc812fcaa6af	0201	d49b3332-1b95-43c0-8c8f-25842cf7f537	404b5b30-1273-4e2c-a4de-a8847cb7e5c7	t	a0f66b9c-6825-4299-89c7-6d4296125b1b	2	Pekerjaan/kegiatan DD/ADD tidak ekonomis
6f760f26-482f-4e2d-a84b-d9aa719878c6	0202	d49b3332-1b95-43c0-8c8f-25842cf7f537	602fa656-5eb7-42eb-b33d-27ae2bac110a	t	a0f66b9c-6825-4299-89c7-6d4296125b1b	2	Pekerjaan/kegiatan DD/ADD tidak efisien
1a88efe8-4e81-4e26-a39c-923c5b283a87	0203	d49b3332-1b95-43c0-8c8f-25842cf7f537	744b5300-4427-439a-a729-ab70a6a908a1	t	a0f66b9c-6825-4299-89c7-6d4296125b1b	2	Hasil pekerjaan/kegiatan DD/ADD tidak/kurang bermanfaat
e9de9c4b-61ad-4c14-acc1-500e4baab60f	0101	d49b3332-1b95-43c0-8c8f-25842cf7f537	744b5300-4427-439a-a729-ab70a6a908a1	t	a0f66b9c-6825-4299-89c7-6d4296125b1b	1	Data Keuangan, Aset dan Output kegiatan desa
434fb404-f3a2-4b50-955e-ff39e6a535a4	0204	d49b3332-1b95-43c0-8c8f-25842cf7f537	ae34b855-ce4f-472b-8672-15e28682166c	t	a0f66b9c-6825-4299-89c7-6d4296125b1b	2	Pengamanan/pengendalian aset di desda masih lemah
c41f7b1e-dc07-4566-8561-3a45b8963a0d	0205	d49b3332-1b95-43c0-8c8f-25842cf7f537	ae34b855-ce4f-472b-8672-15e28682166c	t	a0f66b9c-6825-4299-89c7-6d4296125b1b	2	Mekanisme pemanfaatan aset desa belum tertib
e5a56055-0def-4ec5-bd6f-245dc8015698	0207	d49b3332-1b95-43c0-8c8f-25842cf7f537	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a0f66b9c-6825-4299-89c7-6d4296125b1b	2	Kewajiban perpajakan belum dipenuhi
ffb5d332-b07b-42b3-b771-8f9d3a6af23f	0206	d49b3332-1b95-43c0-8c8f-25842cf7f537	7b46be23-f857-4cfb-bb52-1834f8bf72b9	t	a0f66b9c-6825-4299-89c7-6d4296125b1b	2	Administrasi dan pelaporan belum tertib
011a8af3-73b8-4a8f-a97f-c960523a5014	0208	d49b3332-1b95-43c0-8c8f-25842cf7f537	744b5300-4427-439a-a729-ab70a6a908a1	t	a0f66b9c-6825-4299-89c7-6d4296125b1b	3	Penyelesaian Penugasan dan reviu berjenjang
4631cde7-6bea-4998-bc83-5fd219b4698c	901	d49b3332-1b95-43c0-8c8f-25842cf7f537	82d51b4d-157a-4d69-a814-fa15c3f98b41	t	a0f66b9c-6825-4299-89c7-6d4296125b1b	1	Entry Meeting dan Data Umum Pengelolaan Keuangan Desa
b608b9a5-295b-4c44-9c47-94e50de02bda	60	519bbf54-6048-4a14-b184-898e521d9094	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pembentukan Tim BOS Sekolah belum sesuai ketentuan
2f609172-c0f0-42bc-9457-cf4fb57fda2b	61	519bbf54-6048-4a14-b184-898e521d9094	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pengsisan Dapodik sekolah belum sesuai data 
5246fb49-496f-4a9a-857c-23cda5365bf2	62	519bbf54-6048-4a14-b184-898e521d9094	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Jumlah dana BOS yang diterima tidak sesuai jumlah data peserta didik
39273dd4-314c-4434-92f9-b47616157df7	63	519bbf54-6048-4a14-b184-898e521d9094	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Pemotongan dana BOS oleh pihak tertentu
09814a69-9c41-4920-a2ac-eee09546c300	64	519bbf54-6048-4a14-b184-898e521d9094	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Penatausahaan dana BOS di sekolah belum tertib
3708edfd-dd9d-411b-9969-bee70e0536d1	65	519bbf54-6048-4a14-b184-898e521d9094	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Administrasi dan pelaporan BOS belum tertib
1c567347-9179-46e8-b6b9-0c27051fd19c	66	519bbf54-6048-4a14-b184-898e521d9094	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Tim BOS Sekolah belum memenuhi ketentuan transparansi
a2de942d-68c2-4ecc-bd62-c3e999e026f1	67	519bbf54-6048-4a14-b184-898e521d9094	e918fec9-e4a5-4903-8591-3e8b9e6f166a	t	a863e538-ff0c-4df6-a710-ae75058c750f	2	Penggunaan dana BOS diluar RAPBS
d4691f2f-6915-4e04-b360-8f1cd589f87e	9999	d49b3332-1b95-43c0-8c8f-25842cf7f537	404b5b30-1273-4e2c-a4de-a8847cb7e5c7	t	2ed85714-d1fe-444d-8f20-d821b58b8423	2	Kemahalan Harga
\.


--
-- Data for Name: tao_detail; Type: TABLE DATA; Schema: support; Owner: eauditpr_user
--

COPY support.tao_detail (id, id_tao, langkah, kertas_kerja, file_download, status) FROM stdin;
1036dac7-ccb6-47fd-a958-5a5c19f3df62	c6630c3c-0efb-41e0-bd96-b8b9ec0cbac6	1	1	bdd33adcef02bfeb793c509ac231e87d.png	t
18b9e17e-61bf-48d9-8013-b1f1a36ad704	6eb362ab-a8ee-4d1a-bcf7-39ac104c277c	Dapatkan aturan yg mendasari	1	a6d8cd313e42a199585651e23ccbaaf2.png	t
1b6aec97-c1c4-460e-ba0d-ebf406c3ceeb	c6630c3c-0efb-41e0-bd96-b8b9ec0cbac6	Dapatkan aturan yg mendasari	6	76adc2f97877687fdbab4e6a8bdae75b.pdf	t
3e1c2107-b0da-450d-8afa-b9380f375942	c6630c3c-0efb-41e0-bd96-b8b9ec0cbac6	3	3	b91daf477eef0c96670fec2917cfb5b0.pdf	t
445638e9-2e0b-4327-8d40-a7f35e5ad7a9	6eb362ab-a8ee-4d1a-bcf7-39ac104c277c	Pelajari aturan yang mendasari kegiatan	2	9aba7f9301ea5a8c9cbe0de3c1fc3044.png	t
73639b26-c3bc-4978-85e7-8797454f1dc7	c6630c3c-0efb-41e0-bd96-b8b9ec0cbac6	4	4	90a15ac58c4a0853394ac161157dcef7.png	t
9dd50f8a-d057-4ffe-a36f-fb23b4698a52	6eb362ab-a8ee-4d1a-bcf7-39ac104c277c	Buat data pegawai	010101	b652a6722a91aaa16639326c0abb5c98.doc	t
a5d15d37-4b03-4f9c-95b0-a01f970891f7	c6630c3c-0efb-41e0-bd96-b8b9ec0cbac6	2	2	c70bcb8c8350c478010880ae6f3d3ef4.png	t
cdc2393a-7c7e-454b-b568-bc2dc1434c8b	53a1a73a-5275-472f-8153-b0f502321a20	Dapatkan data kegiatan utama Obrik	01	7439434b1e7b9af2d512c0a9f03fc8dc.xlsx	t
d8ee7903-f852-4b5b-8195-469e5ba3eec1	a882d581-3e6c-4817-8cdb-09a7af208310	Dapatkan Peraturan perundang-undangan yang berlaku	01	b76a85d60200ec6a3fbd3c06e2603b38.xlsx	t
292c3c23-6972-4595-b058-ed73dc4b2486	6aa93d98-8ae8-43a2-81e8-02d4a34a2e38	Dapatkan struktur organisasi dan data pegawai	01	30a22584295d9e11845c04fef5c5b723.xlsx	t
3c3798d5-4d30-4e44-a5da-9a24f40740c2	e45456ed-9fdf-4b98-942e-727b4fc894c1	Dapatkan data anggaran dan realisasi	01	eafe8c29654d7dce714092041e5311cd.xlsx	t
13f6eb59-4e29-4059-be0b-3c9458446882	de8c7cc5-c048-4b32-bd66-7084fee423d8	Dapatkan daftar aset dan mutasi barang	01	f4121e0775dc21891aa279a68a22ae80.xlsx	t
af9e2f57-54e5-49de-97e5-49d24f998f8f	47a410bc-02a2-4f80-ae44-9b7d757de2c8	Buat daftar permintaan data	01	5c4831ce581f8e334d13ea92c5f95065.xlsx	t
b1897b15-8992-47dc-abf3-1b8200e5665a	01336ab8-aa72-4f92-9849-9c607106da3b	Buat berita acara pembicaraan pendahuluan	01	3de8f988bd4d54465b9005fcbba7f0ad.xlsx	t
49f6c09c-485d-458b-aae9-c3ff883f4542	5336be63-e553-4d88-8b34-8f50e8b113e7	Buat Daftar Temuan	01	c2c8c45c74036652224ecd5bf74fa7f4.xlsx	t
64722e98-772e-4a28-94c3-03d3cb483320	d0b85463-6c07-4906-aab8-3ec38dc6423a	Lakukan pembahasan internal tim	01	9b955182d527870fa254fd2330feb84c.xlsx	t
04b0f150-6dca-4d93-b29f-3eef3a302281	41d52537-bd80-4226-95b9-0a641ad25b0a	Buat Pemberitahuan Hasil Pengawasan/pemeriksaan	01	f9d6d5df83507408c5beb1cb6420fb0c.xlsx	t
fb1b3834-b5d2-4599-8273-a49881db8d36	737defc4-f70c-4446-b9e0-7f86c4e564ef	Lakukan pembahasan Intern Pemberitahuan Hasil Pemeriksaan	01	6ad65bc83228d3fb3d65a4d67625b7d4.xlsx	t
313a30e0-7f5b-4b15-ac3f-708e03a3c990	da7856e4-eb1d-48f0-9000-284b41ab184b	Susun draft laporan hasil pengawasan/pemeriksaan	01	270ecf5bf0d889d4fc69d666bb4206f2.xlsx	t
72eed5d9-87a3-424d-abbf-e74972466494	f81b0d55-9936-4f86-83c5-d1ad212e6d89	Lakukan reviu terhadap konsep Laporan	01	d89bd12fb56971589d2e04e61978eadf.xlsx	t
58a6c1cc-590a-43cb-b914-f6dc3807b1a1	65c6376e-e024-4c1f-8117-f86f64cbd090	Dapatkan data analisis kebutuhan; Dapatkan rencana pengadaan tahun berjalan; buat kesimpulan	1	99d30076ce1ed81162ace678b0fe18a0.xlsx	t
336014ad-e1cd-41c4-bf97-b8438b78b12f	c6a82074-58ea-43e1-8214-e6c0fa82a0d6	Dapatkan data pengadaan aset sekolah; Lakukan pengujian terhadap proses pengadaan; Bandingkan dengan peraturan pengadaan barang/jasa pemerintah; Buat Kesimpulan	1	d6cf29594fd48c8394734f52b36394ff.xlsx	t
a90ddbdb-4e88-4010-9bb2-9d4960ec05cc	e82290e6-3748-4a66-b9d0-0a08a1889da7	Dapatkan dokumen pengeluaran/belanja; lakukan pengecekan terhadap barang yang dibeli; Buat kesimpulan	1	d2246d420176c639ce61803fd917ff6e.xlsx	t
45d56475-668b-41e4-bdc0-3634c76f35a8	ad9daeb1-9c67-4560-a7f0-cd69264dd2eb	Buat kuisioner kepuasan steakholder dan pernyataan independensi auditor	01	69eec4ccca1eebe7a4f6e4e3e4c10a39.docx	t
53983413-54bf-4a94-815e-82ccceecacc3	8a347078-3a23-491d-b4fd-7c6ce099dbbd	Lakukan entry meeting dengan pimpinan obyek	1	eb4d0a5a417631c437d1e6fce4a1805f.xlsx	t
66b469d7-da4b-4c89-9b40-6fe3cec49a6b	45e5d931-74f5-45fc-844f-e3f6679e40ca	Lakukan survey terbatas IC	1	480698b9f87ac33596db99694e3aab4d.xlsx	t
51dd4682-183d-4989-84c2-18baefbd4539	a48222b7-f231-4634-b56a-6f69e0f4c18c	Buat permintaan data yang terkait dengan penugasan	1	64a5db3c913aaf8316ed3fac980d12e2.xlsx	t
b7689c74-5a86-4711-b170-6deb99f7137b	91dc6e78-7fc7-4bf9-b6ac-1b19347bfc2b	Dapatkan data siswa tiap tahun ajaran	1	f2993c81c686c25a7a594a2f5ef755e9.xlsx	t
da8ab89d-8e7c-49d9-a4a7-362bcf83a072	890ae4ef-a151-4751-994a-b492e35a66a8	Dapatkan data pendapatan dan belanja sekolah	1	0c388b6aeec2c1fc7c481aa5525e4480.xlsx	t
62ba2b57-a6ff-4ada-ae74-9a197a94f20e	41744813-c4ed-4b02-90ac-01b744f46285	Dapatkan data aset sekolah	1	4fba06dd25f3accf41cba4a7691c2326.xlsx	t
5ebea223-1051-4c3b-8518-bd4eff2f8c4b	9ad69d88-3c4e-4830-8d7d-0701cf6cf496	Dapatkan data laporan periodik yang dibuat sekolah	1	645c2c64b2de7aa4acfb7fa21e44621f.xlsx	t
62fc3064-8e6a-4e97-a185-fb1156c0c16a	b4cad1d8-e418-47c0-b18f-a735ac36907f	Dapatkan data perhitungan penerimaan BOS dan bandingkan dengan jumlah siswa yang terdaftar	2	c10a17aae0e143777a9a00316d036a13.xlsx	t
e7a5563b-215a-44a1-a523-a1e2f47cb694	b4cad1d8-e418-47c0-b18f-a735ac36907f	Dapatkan data total penerimaan BOS tiap tahun ajaran	1	c1a1cee71e6aedce3e08f61a2cb9c846.xlsx	t
28ca9f0f-b7c0-442d-9e59-cf89b711af61	f7c4a805-775d-4bbe-af98-c35f732dd974	Catat tanggal penerimaan dana BOS; bandingkan dengan ketentuan batas waktu penyaluran dana BOS ke Rekening sekolah; Buat kesimpulan	1	e03049f2285ec2cb745ff9fc7b933c2f.xlsx	t
06709067-0f75-4b63-96b3-47b501e4674c	47ac6f23-f671-466d-9340-f5cb6f686f42	Lakukan scan laporan final dalam bentuk pdf, dan upload di aplikasi	1	bb94f3907e90ed7331c483066beb6d66.xlsx	t
acd1709e-5bb3-436c-b956-d1cfebc17b65	34710954-64b0-430b-8779-f0c5eb306043	Dapatkan data realisasi penerimaan selain BOS; Dapatkan dasar penerimaan tersebut; Buat kesimpulan	1	c92f385c5cb3c8edafa0b389949dfc92.xlsx	t
4453d1e6-05ed-4374-8344-a23915bf425b	2b6d6cd2-d4f5-44a8-99ee-5ba08dff3e99	Dapatkan data penerimaan ; Bandingkan dengan jumlah angka di laporan; buat kesimpulan	1	b59ec1a69f48fce8a4126dfc03e22414.xlsx	t
c7aa0a46-3ae1-4c52-a708-98c17cec316e	697e5dfb-3707-42fe-98bd-76819b2e661f	Dapatkan RKAS; Teliti apakah RKAS telah disusun sesuai pedoman; Buat Kesimpulan	1	bc40557422329d07c615302012550391.xlsx	t
c3d33bb2-fde4-4953-a53e-8f490b6ab43d	13c67239-e6df-4c48-981d-c2ea31c1d9e5	Teliti catatan dalam buku kas, apakah item pengeluaran telah tercantum dalam RKAS 	1	f32257a48d9cbdaa7a4e4f0b43f1fced.xlsx	t
2bfac78e-bf0d-4088-bd50-3f6bbbeba7e9	9a2b9aa1-8e6b-4557-813c-daf247521e33	Lakukan pengujian terhadap kegiatan; Dapatkan standar harga kegiatan (SBU);bandingkan biaya kegiatan dengan SBU yang berlaku; Buat simpulan	1	841a96e7d00bfee71115646ed406413d.xlsx	t
5cbb9528-fa19-449b-a05f-ed0abfc36a00	ecad0f81-c4a9-478d-9b20-81d8f5552c8b	Dapatkan data pengeluaran kas; Lakukan konfirmasi atas hasil kegiatan, dokumen dan data pendukung pengeluaran; Buat kesimpulan	1	7a87fb52c517650f3921ef4f58ee02e1.xlsx	t
c90bd32b-1b3f-4952-ad96-d498e6b56364	d63907ed-677f-4b62-b82d-a556fe2b491d	Dapatkan catatan pengeluaran; lakukan pengecekan terhadap bukti pendukung, apakah pengeluaran didukung oleh bukti yang sah ; Buat kesimpulan	1	e6d482a3b1ecd9974d2d9f8bf7692ca1.xlsx	t
dc315220-3e80-4d7a-841e-dda52b0a1f84	40c90e32-c3c2-436c-9e7c-802fddbd7c80	Dapatkan data pengadaan barang; lakukan cek fisik apakah telah sesuai spesifikasi dan telah dimanfaatkan; Buat kesimpulan	1	c6a3b52acacecab9b8ce89264de6d675.xlsx	t
149643dc-f2b1-4426-8f69-013f48e4d7bd	61a84c91-2068-449a-9ded-4c1c4439a7cc	Dapatkan daftar barang yang diadakan sekolah beserta harganya; Bandingkan dengan harga pasar (katalog LKPP); Lakukan klarifikasi jika ada perbedaan; Buat kesimpulan	1	cc3256ba9ed74242a8ee5d5d09966dcd.xlsx	t
4cdccf23-dc54-4583-b560-8ae93efcd3e8	27cfbc71-3058-4ca8-9df0-3e3c38b20146	Dapatkan daftar aset; Lakukan pengujian apakah seluruh pengadaan telah dicatat dalam daftar aset; Buat kesimpulan	1	dd1a62816cd2c5329d6a5a9c191b9efc.xlsx	t
5d03a7f2-fbdc-4dcb-becd-75c08489febd	ab59b878-c234-4eb8-86e3-e17c3c91c4a5	Dapatkan daftar aset; Lakukan pengujian terhadap mutasi tambah dan kurang; Cek apakah seluruh mutasi telah dicatat; Buat kesimpulan	1	cff0d161bd58f1d78d115f2ce7a864a4.xlsx	t
fcf31fbf-0610-4595-bc7d-f5c41f5f5f9a	dc0ff3ab-4441-4047-8d42-bb79233d3df1	Dapatkan data hasil opname fisik aset; Jika tidak diperoleh data, lakukan klarifikasi kepada petugas barang; Buat kesimpulan	1	8b7dd92600d9f6721699d6ce6a023d16.xlsx	t
52cc2b91-fe8b-4097-9d9e-3f81a889153f	6ed30202-3be3-417a-ac5d-53c80e43ff07	Dapatkan data aset yang digunakan untuk operasional sekolah; Lakukan konfirmasi atau wawancara apakah terdapat aset hibah yang tidak dicatat dalam aset sekolah; Buat kesimpulan	1	9b0f8593bb4d3ab23d1a98903210ee97.xlsx	t
470396d6-b06a-4147-b47b-7d55859ce577	4546f5e4-34fc-4664-83a9-7f47a1f91d90	Dapatkan data aset; lakukan cek fisik secara sampling terhadap keberadaan barang; Buat kesimpulan	1	e49e6bfa982e2fe42961a620f9bb4647.xlsx	t
008749c7-efb0-47c7-b54d-6a2503ba2ebd	8a67ed8c-e839-411c-9f63-2165384b4cb7	Teliti pengeluaran yang menjadi obyek pajak; lakukan pengecekan apakah sudah dikenakan pajak sesuai tarif; Buat kesimpulan	1	8786e8dad1ae0c26cd6ee36a07f700e5.xlsx	t
37bab70a-0dd2-4786-9c24-9542445e738d	a439622f-4c50-4be0-b762-b4bea0781118	Dapatkan data penyetoran pajak; Lakukan pengujian apakah ada pajak yang terlambat disetor sesuai ketentuan	1	7ae50195efd8dddacc400aca7245f507.xlsx	t
53449178-793e-47f9-892d-ac58f1375831	c84bebc0-7b80-436d-8a63-63fb0dfba36c	Dapatkan data pajak yang dipungut; Lakukan pengecekan terhadap pajak yang telah dipungut seluruhnya telah disetor; Buat kesimpulan	1	dca020395f1d9fdebb9d0acc4d473e0c.xlsx	t
2e0ee088-c298-4238-9d84-1bcac8f05b4e	8b0eb456-eb2d-4f7e-bd66-331d25fd36d7	Lakukan entry meeting dengan pimpinan obyek dan buat berita acara pembicaraan pendahuluan	01	95410547cf3685563908c6ba0209498f.xlsx	t
80636594-116a-44e8-b4be-e8b064986a06	f330670d-c334-48a8-9bf0-35514fe3d1cf	Buat permintaan data yang terkait dengan penugasan	02	5aeb5253f3aa223bc020b03fbfd6949e.xlsx	t
c5c56164-40c4-43fa-8495-dc5165718ebf	2840a020-f00c-416b-ba28-0a91b99e63ba	Teliti uraian, isi dan kelengkapan formal kontrak; Isikan dalam kuisioner di Form KKA; Buat kesimpulan	01	a5db7627827fcf37c94bfbe4f255f8c2.xlsx	t
493f29c3-5db5-470d-88a1-d50c04a963a6	ae5660f2-1dba-49ed-8b6c-b1935be051bf	Lakukan pengujian terhadap Jadwal mobilisasi, Pemeriksaan bersama lokasi kerja; Teliti kegiatan Penyerahan lokasi ; Teliti Program mutu dari penyedia ; Teliti ketepatan waktu penerbitan SPMK; Isikan dalam formulir kuisioner; Buat kesimpulan	01	a8b43c091003e42d1c70e490adb3dfdb.xlsx	t
8a6bbfba-798e-4593-9d57-d0d5a88b2c4a	adeb046a-9825-44e4-b61d-8b4354004640	Teliti pembayaran Uang muka apakah sesuai klausul dan rencana penggunaan penggunaan; isikan dalam KKA kuisioner; Buat kesimpulan	1	fda8153319274d62bdf5077446107d2c.xlsx	t
30ef6912-451f-4c64-ac71-92a4bf5736c9	1b0f9300-53c5-46ab-9c66-24a969adcb94	Teliti apakah telah dilakukan pemantauan pelaksanaan pekerjaan; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	1	63495c47390f354de5c5b4f4824545fb.xlsx	t
823a2b84-ca0e-42a6-9792-1c1b19d3dfb6	2668c7f7-3137-4d7e-ab64-80121f97175e	Teliti apakah telah dilakukan pengawasan mutu pelaksanaan kegiatan; Tuangkan dalam KKA kuisioner ; Buat kesimpulan	1	a825b9f602d0f35dc80339eaddccce08.xlsx	t
61c0607b-868f-47af-a90d-dde0c42a3af4	e27d21f5-0b31-487e-9c5b-1560c457d551	Teliti apakah ada perubahan pekerjaan; Jika ada perubahan apakah telah didukung dengan dokumen yang lengkap; Tuangkan dalam KKA kuisioner; Buat kesimpulan	1	747e169a926374e91ad7074c68a04f9c.xlsx	t
1b72e119-165e-401c-908c-83b0438780e9	5985f921-9091-4fd1-9b9c-665a7839ed40	Teliti apakah Penyedia telah menyampaikan laporan kemajuan hasil pekerjaan; Apakah Laporan kemajuan hasil pekerjaan telah diverifikasi PPK; Teliti apakah Kemajuan hasil pekerjaan yang dilaporkan telah sesuai dengan kemajuan pekerjaan fisik dilapangan; Teliti apakah Spesifikasi teknis hasil pekerjaan yang terpasang telah sesuai dengan kontrak;  Tuangkan dalam KKA Kuisioner; Buat kesimpulan	1	12f80c868895028754bb491bc9ccb2ff.xlsx	t
64ecb1a9-91f0-4b16-8360-11da3e655574	a21923df-178b-4fb1-8935-f6dfa12a96bc	Apakah Penyedia telah mengajukan tagihan disertai laporan kemajuan hasil pekerjaan; Untuk Kontrak yang mempunyai subkontrak, apakah permintaan pembayaran telah dilengkapi bukti pembayaran kepada seluruh sub penyedia sesuai dengan prestasi pekerjaan;  Apakah Pembayaran penyedia kepada sub kontraktor telah sesuai dengan pekerjaan yang terpasang; Apakah Pembayaran dilakukan dengan sistem termin sesuai ketentuan dalam Dokumen Kontrak; Apakah Pembayaran dilakukan senilai pekerjaan yang telah terpasang, tidak termasuk bahan/material dan peralatan yang ada di lokasi pekerjaan; Apakah Pembayaran termin telah dipotong angsuran uang muka, denda(bila ada); Tuangkan dalam KKA Kuisioner; Buat kesimpulan 	1	ee7afdf83d36ca89bb823562275de26c.xlsx	t
8d53ae6e-9502-4103-8603-ecd615b32d94	05871b6d-b242-4f25-bbe3-c53ed23ac360	Apakah PPK melakukan penelitian usulan tertulis perpanjangan waktu penyelesaian kontrak oleh Penyedia; Apakah Perpanjangan waktu pelaksanaan kontrak telah berdasarkan pertimbangan yang layak dan wajar oleh PPK, karena :( pekerjaan tambah;  - perubahan disain;  - keterlambatan yang disebabkan oleh PPK;  "- masalah yang timbul diluar kendali penyedia; dan/atau " - Keadaan Kahar) ; Apakah Penyedia telah memberikan peringatan dini akan terjadi keterlambatan dan telah bekerja sama untuk bekerja sama untuk mencegah keterlambatan; Apakah PPK telah menetapkan ada tidaknya perpanjangan, dalam jangka waktu 21 (dua puluh satu) hari setelah penyedia meminta perpanjangan; Apakah Penetapan ada/tidak perpanjangan pelaksanaan dinyatakan secara tertulis; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	1	873b63484acc4e5b7c23f3510028221d.xlsx	t
6b69d2bd-631b-423a-a5c4-15d7ba2adb89	4feddf7b-0b5e-4e62-94dc-eb4d227dc9a8	Apakah Penyerahan hasil pekerjaan berdasarkan permintaan secara tertulis dari Penyedia Pekerjaan Konstruksi; Apakah Permintaan tersebut setelah pekerjaan selesai 100 % ; Apakah Panitia/Pejabat Penerima Hasil Pekerjaan melakukan pemeriksaan hasil pekerjaan Pengadaan Barang/Jasa; Apakah Melakukan pemeriksaan sesuai dengan pedoman/prosedur yang telah ditetapkan; Apakah Penyedia telah melaksanakan pekerjaan konstruksi sesuai dengan ketentuan yang tercantum dalam Kontrak dan gambar; Apakah Apabila terdapat ketidaksesuaian dengan kontrak (kekurangan-kekurangan dan/atau cacat hasil pekerjaan), penyedia telah memperbaiki/menyelesaikannya; Apakah panitia/Pejabat Penerima Hasil Pekerjaan menerima hasil Pengadaan Barang/Jasa setelah melalui pemeriksaan/pengujian; Apakah PPK menerima penyerahan pertama pekerjaan setelah seluruh hasil pekerjaan dilaksanakan sesuai dengan ketentuan Kontrak dan diterima oleh Panitia/Pejabat Penerima Hasil Pekerjaan; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	1	78477ef3a5f504b32cdc750608c4b40f.xlsx	t
505f7604-ccda-49df-83e7-6f5ef6c9bbb0	e36f3a90-27a6-4623-84d6-0e47b4656190	Apakah Penyerahan akhir pekerjaan dilaksanakan berdasarkan permintaan tertulis dari penyedia; Permintaan penyerahan akhir setelah masa pemeliharaan berakhir; Masa pemeliharaan telah ditentukan dalam syarat syarat khusus kontrak; Apakah Panitia/Pejabat Penerima Hasil Pekerjaan telah melakukan pemeriksaan/pengujian sebelum menerima hasil pekerjaan konstruksi; Apakah Penyedia telah memberikan petunjuk kepada PPK tentang pedoman pengoperasian dan perawatan sesuai dengan SSKK;Apakah PPK menerima penyerahan akhir pekerjaan setelah penyedia melaksanakan semua kewajibannya selama masa pemeliharaan dengan baik;  Tuangkan dalam KKA Kuisioner; Buat Kesimpulan	1	a2734b4b0d7fca3373f5586e82be6fcc.xlsx	t
adfa2aaa-1411-4b49-8f24-261523ff01f9	b608b9a5-295b-4c44-9c47-94e50de02bda	Jika terjadi pergantian personil Tim Bos, periksa apakah pergantian tersebut telah dituangkan dalam SK Kepala Sekolah tentang pergantian personil	3	d4ee5fbff34ef7005550dd5b932dc21c.xlsx	t
616b7f1f-f752-4632-bee0-3261dd3150ee	b608b9a5-295b-4c44-9c47-94e50de02bda	Periksa identitas dan metode yang digunakan untuk menunjuk unsur orang tua siswa dalam Tim Bos Sekolah	4	cf90f3d68dd2018410c57d64bdf32472.xlsx	t
fd3066d2-c8c0-460e-a52c-790cd50b7469	a0017e63-252b-4733-afbf-1e3ef061414e	Apakah Pembayaran terakhir dilakukan setelah : - Pekerjaan selesai 100% (seratus perseratus)  "- Berita acara penyerahan pertama pekerjaan diterbitkan; Apakah Pembayaran yang dilakukan sebesar 95% (sembilan puluh lima perseratus) dari nilai kontrak, sedangkan yang 5% (lima perseratus) merupakan retensi selama masa pemeliharaan," "Pembayaran yang dilakukan sebesar 100% (seratus perseratus) dari nilai kontrak dan penyedia harus menyerahkan Jaminan Pemeliharaan sebesar 5% (lima perseratus) dari nilai kontrak" "Masa berlaku jaminan pemeliharaan sejak tanggal serah terima pertama sampai dengan tanggal penyerahan akhir pekerjaan," "Telah dipotong denda (jika ada), pajak,uang muka dan ganti rugi (jika ada), " "Pembayaran ganti rugi kepada penyedia ( jika ada) sesuai kontrak, " "Ganti rugi dibayarkan berdasarkan data penunjang dan perhitungan kompensasi yang diajukan oleh penyedia kepada PPK, telah dapat dibuktikan kerugian nyata akibat Peristiwa Kompensasi" "Penyedia telah memberikan peringatan dini akan terjadi pengeluaran biaya tambahan dan telah bekerja sama untuk mencegah pengeluaran biaya tambahan," "Besarnya denda, pajak, uang muka sesuai dengan kontrak, " "Jika pekerjaan tidak selesai karena kesalahan atau kelalaian penyedia maka penyedia dikenakan denda. " "Besarnya denda 1/1000 dari sisa harga bagian kontrak yang belum dikerjakan, bila pekrjaan yang telah dilaksanakan dapat berfungsi," Jika pekerjaan yang telah dilaksanakan belum  berfungsi, besarnya denda 1/1000 dari harga kontrak; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	1	54776382dbb53d92f11ac6920e139e86.xlsx	t
5db3d0f4-1803-444c-97de-d121100a62dc	8be6a3b9-3f92-4e27-8c4d-e2d251b9a1d1	Apabila penyedia tidak melaksanakan kewajiban pemeliharaan sebagaimana mestinya, maka apakah PPK  menggunakan uang retensi untuk membiayai perbaikan/pemeliharaan atau mencairkan Jaminan Pemeliharaan; Apakah PPK  melakukan pembayaran sisa nilai kontrak yang belum dibayar setelah PPK menrima penyerahan akhir pekerjaan; Apakah PPK  mengembalikan Jaminan Pemeliharaan setelah PPK menrima penyerahan akhir pekerjaan. "Tuangkan dalam KKA Kuisioner; Buat kesimpulan	1	4664370df6dcae7522aa1788dbf82245.xlsx	t
e32bf68b-6224-4d54-8162-cb96836a27d7	827276ee-af78-4672-8f75-ccf63198aa83	Apakah Penyedia Barang/Jasa memberitahukan tentang terjadinya Keadaan Kahar kepada PPK secara tertulis; Apakah Surat pemberitahuan didibuat dalam waktu paling lambat 14 (empat belas) hari kalender sejak terjadinya Keadaan Kahar; Apakah Surat pemberitahuan keadaan kahar disertai salinan pernyataan keadaan kahar yang dikeluarkan oleh pihak/instansi yang berwenang sesuai ketentuan peraturan perundang-undangan; Apakah Keadaan kahar telah sesuai dengan kesepakatan yang dituangkan dalam kontrak; Apakah Pengaruh keadaan kahar, telah dituangkan dalam perubahan Kontrak ; Apakah Penggantian biaya telah diatur dalam suatu adendum Kontrak ; Tuangkan dalam KKA Kuisioner; Buat kesimpulan	1	57cef9e8229131ada43ab524dd1356f7.xlsx	t
5250d9b9-8156-4a02-ad3b-dde51a608188	73af2673-e957-4009-9347-b3378fd6461e	Susun P2HP, dan bahas dengan obrik	1	1e01106db2e67a59ec3a25d52399b7eb.xlsx	t
47ea222d-05f8-4b95-a130-3e7bb6453ff0	14cac531-b4f0-4f60-a89f-c18e2597d64c	Susun draft laporan, dan bahas dengan internal Tim	1	c05772834dc34f6232929dacc3376a3d.xlsx	t
57ba24b4-bde1-4749-88ce-ad7e5e658c34	07dd3bbd-7528-4127-821d-2cb28c21e4b8	Lakukan scan laporan final, dan upload dalam aplikasi	1	95adfafdfe4a484c4799d6ed80b33f51.xlsx	t
040c3a08-6eb5-46e8-9ce2-5ce3abbd68cd	6b05fac6-0ef9-4179-91cf-0d013c91a67f	Dapatkan data Laporan penerimaan barang dan LRA 	1	8ab29b244b1262fcc838daae03fe7840.rar	t
162a7cbb-67c8-476a-877e-98b2767c9cfc	178730ce-8475-4419-84e2-d6d05e9e9ddc	Buat rencana pelaksanaan PKS	1	a80e02343b80e12cd5fc950fd77aa3af.xlsm	t
d01037b7-3314-4989-9ba3-bd8d16608962	36f7a519-b3b6-42bf-baf2-bf05d03b2daa	Dapatkan data target input/anggaran desa (per kegiatan)	01	0976ef528ff16f81efa06d1674299558.xlsx	t
18c2f1a0-ac22-4655-bd0c-70914d7237a6	2a95fbbe-ca72-495e-b72d-6143d2c7c53f	Buat resume jumlah nilai transaksi per kontrak dan bandingkan dengan dokumen kontrak	1	d674a3c4e6dd8a6f23b21a03fe4f301c.xlsm	t
2ab1af68-84e0-4535-a4c7-4812c8a4473b	fb38cd83-fdb2-462c-ae6e-e8f6cd5ede1f	Lakukan penghitungan jumlah transaksi (record) untuk m,enentukan jumlah sample yang akan dilakukan ujipetik	1	5df20e03dbacd9684e2c75ebc682abfb.xlsm	t
33a3fba6-c0be-4095-bf13-e2e193d298c3	b98786e8-2537-4bdf-a56d-e7f6f8bb29a6	Lakukan pengujian atas penginputan data transaksi yang tidak lengkapp	1	46db04e3de08bd842434ccaf157fc071.xlsm	t
ff1e776a-5b34-4af0-b6d3-849eb933629b	8afbfffb-e3a9-484a-9a1f-5a24a9ddd5e5	Lakukan pengujian perkalian harga dengan jumlah barang, dan bandingkan hasilnya 	1	fa0c3e18d946dcd4d9ba9e4aa59b172f.xlsm	t
f13df8c9-30ed-423c-92a9-47beb0fafb8f	ef723534-297e-4ddb-97a8-f7b54f37a0a2	Lakukan pengujian terhadap realisasi anggaran tiap akun/jenis belanja	1	6c9cf992e4422e776e66ce78f9fd2d16.xlsm	t
8cc6439c-6e74-4ad3-ae74-f32ccbfa6099	acb8a983-45f5-4f2f-96d7-bcb9aafe0fbd	Lakukan pengujian apakah terdapat tanggal kontrak 	1	e95b7358c2f1aa165fabbf93e67110ba.xlsm	t
8b8aeb0c-c336-448b-b0a0-01a88d6aa702	f202a518-7f40-434a-b35c-cad62552fd66	Lakukan pengujian apakah terdapat tanggal kontrak pada hari libur 	1	ef46546cbf4871de258163a396bb0d51.xlsm	t
b6482ee8-3b2a-4d0b-b547-bd7b36f85644	82ee9af5-f19a-42fe-b32d-e76f52939717	Lakukan pengujian apakah terdapat tanggal penerimaan barang mendahului tanggal kontrak	1	6c9b4f6863e4eff51b20ed417b9043b5.xlsm	t
50acb856-1072-4401-871a-76a51e4174fe	2f36fc36-1fe5-49b3-b5fc-eb6230d0f711	Lakukan pengujian terhadap perhitungan Pajak (PPN dan PPh)	1	1e08d45818aab5825c9e2e5ede7a9055.xlsm	t
5c1025d7-dcf6-4d29-a857-66ea2dbee831	53a1a73a-5275-472f-8153-b0f502321a20	Buat resume jumlah nilai transaksi per kontrak dan bandingkan dengan dokumen kontrak	1	b86e914354c0ec75d5455df8a74c0d64.xlsm	t
77939c60-eca2-4b1e-95b5-562785b99f90	36f7a519-b3b6-42bf-baf2-bf05d03b2daa	Lakukan wawancara terkait target input kegiatan di desa, tuangkan dalam kertas kerja wawancara	02	661daf137239587f15c9599fb1bcd59c.xlsx	t
3e53c627-a52f-4f9e-8aa4-220bbf111a28	36f7a519-b3b6-42bf-baf2-bf05d03b2daa	Lakukan pengujian hasil wawancara dan tuangkan dalam KKA	03	99e0495d71ebd1d7adc84a7331903827.xlsx	t
5a067575-63d8-480f-a807-cfb1891ef8b8	e9de9c4b-61ad-4c14-acc1-500e4baab60f	Dapatkan APBDes, LRA, BKU, Daftar Aset, Bukti pengeluaran, Bukti pendukung kegiatan, Buku R/K Bank, Data output kegiatan	1	08392e3c24fa3eebc4e73b865c3374e5.xlsx	t
3ea94623-d224-4e7c-a341-a8825e205ffa	e9de9c4b-61ad-4c14-acc1-500e4baab60f	Buat Berita Cara Entry meeting	2	b3451bee96eb03989048bf1ccb80788b.xlsx	t
549f0660-9b92-4d91-bfc4-a605f688d513	0b659aed-9e3a-468f-9fe5-fc812fcaa6af	Dapatkan data target input/anggaran per kegiatan	1	999b285f953284b7dc8bafacad163036.xlsx	t
b2ab6fd4-9ba3-40f6-9fc8-f59a1c8b6e46	0b659aed-9e3a-468f-9fe5-fc812fcaa6af	Lakukan wawancara terkait target input kegiatan desa	2	baf192a79ac0e5800c6da307366556c9.xlsx	t
640a8791-1cfb-45ae-b17b-afbe6bd60a7d	0b659aed-9e3a-468f-9fe5-fc812fcaa6af	Lakukan pengujian hasil wawancara dan tuangkan dalam KKA	3	066ea0438209e16e82185036c8b55b9b.xlsx	t
d5c209bc-2d87-4997-a59b-ed94d0eacab9	1a88efe8-4e81-4e26-a39c-923c5b283a87	Dapatkan data target kinerja desa	1	0470a904f4cb8a66fa856ee282c76ba8.xlsx	t
a1d12769-fc2c-40fd-9654-c22b9760cb57	1a88efe8-4e81-4e26-a39c-923c5b283a87	Lakukan wawancara terkait upaya desa agar hasil kegiatan bermanfaat	2	c36b541d670e78441804a50ddb222d7e.xlsx	t
90e13942-360d-45cb-bc5c-20bf61471b9b	1a88efe8-4e81-4e26-a39c-923c5b283a87	Lakukan pengujian hasil wawancara dan tuangkan dalam KKA	3	d6132e1975358d5f20104d1ef68f1526.xlsx	t
a7081585-1a44-4ee2-b172-987ebf5fdf7f	1a88efe8-4e81-4e26-a39c-923c5b283a87	Lakukan pengujian terhadap upaya yang telah dilakukan desa bahwa hasil yang dicapai telah dimanfaatkan	4	c75413bc142022df6b3e165200b0e03b.xlsx	t
2dc5180a-6ecb-40db-a6e9-742109c4e82e	b608b9a5-295b-4c44-9c47-94e50de02bda	Dapatkan SK Kepala Sekolah tentang Tim BOS Sekolah dan periksa apakah susunan Tim telah sesuai dengan pedoman	1	11f293f96724cb488ffe8357a3cccfc7.xlsx	t
28d7e52a-59e8-4a1e-b43f-4a48cdca3e22	b608b9a5-295b-4c44-9c47-94e50de02bda	Periksa apakah susunan tim sesuai dengan pedoman	2	9cd1c2e4a36aa2af85c9312a4a677164.xlsx	t
566e33e9-4b00-4464-8fdb-b7dd6be36155	b608b9a5-295b-4c44-9c47-94e50de02bda	Buat kesimpulan	6	22fa99cbf98b901ed38e426a8e336831.xlsx	t
8026a18b-3e4f-41c7-80f1-2cfdfd036676	b608b9a5-295b-4c44-9c47-94e50de02bda	Lakukan wawancara kepada personil Tim BOS untuk mengetahui bahwa ybs adalah orang tua siswa, pengetahuan tentang pengelolaan dana BOS, tidak terlibat dalam manajemen sekolah, keterlibatan dalam pengelolaan BOS, telah melakukan fungsi kontrol dengan memberikan saran dan masukan	5	1394228d78385b1efbf20eaf1f7fe3bf.xlsx	t
d5726b6e-4572-4b64-bc6e-f626ae8713e4	d4691f2f-6915-4e04-b360-8f1cd589f87e	Dapatkan Harga Pasar, Dapatkan Daftar Barang, Bandingkan Harga Barang yang di Beli dengan Harga, Buat Kesimpulan	001	929391d8008e611ea568e873f88d0513.xlsx	t
\.


--
-- Data for Name: tim; Type: TABLE DATA; Schema: support; Owner: eaudit
--

COPY support.tim (id, wilayah, tim, dalnis, ketua, status) FROM stdin;
088299c6-bb63-4492-91df-25b3bce47e44	3a0e1cff-be12-471c-9028-2be62ded1ce1	Tim 7	1b162438-55b1-4b5c-ac17-5699e9dfb1ed	ebe58226-6813-4081-8377-b11d002e2be8	t
1556c471-4eaf-447e-842c-ba5e46b48392	fd41d1a5-24de-4dfa-9695-68e6b9910bca	Tim 2	f741041f-732a-481f-bc78-461bc55ada9b	d016fde5-c284-4455-a44a-723ab77fa99f	t
ed5f4f37-38e9-4446-8267-cb538d37b881	fd41d1a5-24de-4dfa-9695-68e6b9910bca	Tim 3	dd7ae175-271f-467e-979e-8d2d709b1f0e	3d1d59ae-5115-44ee-8d8c-7db38a5e5200	t
f40b930f-b7b3-491e-bc9e-9b1d417ecceb	45e5002d-6492-473b-9dc6-48dc85134fec	Tim 5	7aa6fbb3-2ad0-477a-be68-7696e6ac7ba8	e54dd636-fb29-4ada-b95c-848c39ed6d03	t
570fca4b-20b2-4fff-85c0-0acd63df0722	fd41d1a5-24de-4dfa-9695-68e6b9910bca	Tim 1	222cead4-f3bf-48d7-9541-84f60c66cac5	4ae755a7-3733-4f95-9bbe-02ce3a481fb7	t
6938edea-22b5-4ded-94dd-298065bb67d4	45e5002d-6492-473b-9dc6-48dc85134fec	Tim 4	55fc1778-b710-47c8-a860-b519a03e23bf	e003b944-5cc5-4c82-8d9b-1f4ad707e7dd	t
34bde133-f669-4443-b083-7426b6ed793a	3a0e1cff-be12-471c-9028-2be62ded1ce1	Tim 8	ee570f4f-46de-4761-ae5c-b205c92d5c3e	bac8355c-d3e9-434b-82cf-9aaaadc4b9ae	t
a9df2aa7-24a6-442b-bb4a-0fd109bfeb16	3a0e1cff-be12-471c-9028-2be62ded1ce1	Tim 6	a30f21b6-918e-421d-835c-fac8228feed5	709e3413-c729-4435-ae26-eff4cb99f23b	t
f053a273-c823-4586-90e0-4aea5e08e8f3	feda5cf5-563a-4983-8aad-f8bc561c93cf	Gabungan	ee570f4f-46de-4761-ae5c-b205c92d5c3e	80cc7a7a-186a-4156-9d57-58d0682e3d36	t
\.


--
-- Data for Name: wilayah; Type: TABLE DATA; Schema: support; Owner: eaudit
--

COPY support.wilayah (id, kode, wilayah, irban, status) FROM stdin;
45e5002d-6492-473b-9dc6-48dc85134fec	2	Wilayah 2	95542b83-a537-4794-a103-b768408db707	t
fd41d1a5-24de-4dfa-9695-68e6b9910bca	1	Wilayah 1	ae3234c8-ea30-45d2-a48e-367b8c32567d	t
feda5cf5-563a-4983-8aad-f8bc561c93cf	4	Gabungan Irban	ae3234c8-ea30-45d2-a48e-367b8c32567d	t
3a0e1cff-be12-471c-9028-2be62ded1ce1	3	Wilayah 3	95542b83-a537-4794-a103-b768408db707	t
\.


--
-- Name: indepensi_pkey; Type: CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.indepensi
    ADD CONSTRAINT indepensi_pkey PRIMARY KEY (id);


--
-- Name: lhp_detail_pkey; Type: CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.lhp_detail
    ADD CONSTRAINT lhp_detail_pkey PRIMARY KEY (id);


--
-- Name: lhp_pkey; Type: CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.lhp
    ADD CONSTRAINT lhp_pkey PRIMARY KEY (id);


--
-- Name: perencanaan_aturan_pkey; Type: CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan_aturan
    ADD CONSTRAINT perencanaan_aturan_pkey PRIMARY KEY (id, aturan);


--
-- Name: perencanaan_detail_pkey; Type: CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan_detail
    ADD CONSTRAINT perencanaan_detail_pkey PRIMARY KEY (id, tao);


--
-- Name: perencanaan_hasil_pkey; Type: CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan_hasil
    ADD CONSTRAINT perencanaan_hasil_pkey PRIMARY KEY (id);


--
-- Name: perencanaan_pedoman_pkey; Type: CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan_pedoman
    ADD CONSTRAINT perencanaan_pedoman_pkey PRIMARY KEY (id, pedoman);


--
-- Name: perencanaan_pkey; Type: CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan
    ADD CONSTRAINT perencanaan_pkey PRIMARY KEY (id);


--
-- Name: perencanaan_sasaran_pkey; Type: CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan_sasaran
    ADD CONSTRAINT perencanaan_sasaran_pkey PRIMARY KEY (id, sasaran);


--
-- Name: perencanaan_tim_pkey; Type: CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan_tim
    ADD CONSTRAINT perencanaan_tim_pkey PRIMARY KEY (id);


--
-- Name: perencanaan_tujuan_pkey; Type: CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan_tujuan
    ADD CONSTRAINT perencanaan_tujuan_pkey PRIMARY KEY (id, tujuan);


--
-- Name: tindak_lanjut_pkey; Type: CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.tindak_lanjut
    ADD CONSTRAINT tindak_lanjut_pkey PRIMARY KEY (id);


--
-- Name: dasar_penugasan_pkey; Type: CONSTRAINT; Schema: master; Owner: eaudit
--

ALTER TABLE ONLY master.dasar_penugasan
    ADD CONSTRAINT dasar_penugasan_pkey PRIMARY KEY (id);


--
-- Name: golongan_pkey; Type: CONSTRAINT; Schema: master; Owner: eaudit
--

ALTER TABLE ONLY master.golongan
    ADD CONSTRAINT golongan_pkey PRIMARY KEY (id);


--
-- Name: jabatan_copy_pkey; Type: CONSTRAINT; Schema: master; Owner: eauditpr_user
--

ALTER TABLE ONLY master.conf_spt
    ADD CONSTRAINT jabatan_copy_pkey PRIMARY KEY (id);


--
-- Name: jabatan_pkey; Type: CONSTRAINT; Schema: master; Owner: eaudit
--

ALTER TABLE ONLY master.jabatan
    ADD CONSTRAINT jabatan_pkey PRIMARY KEY (id);


--
-- Name: jenis_ket_key; Type: CONSTRAINT; Schema: master; Owner: eauditpr_user
--

ALTER TABLE ONLY master.jenis
    ADD CONSTRAINT jenis_ket_key UNIQUE (ket);


--
-- Name: jenis_pkey; Type: CONSTRAINT; Schema: master; Owner: eauditpr_user
--

ALTER TABLE ONLY master.jenis
    ADD CONSTRAINT jenis_pkey PRIMARY KEY (id);


--
-- Name: pkpt_pkey; Type: CONSTRAINT; Schema: master; Owner: eaudit
--

ALTER TABLE ONLY master.pkpt
    ADD CONSTRAINT pkpt_pkey PRIMARY KEY (id);


--
-- Name: profile_pkey; Type: CONSTRAINT; Schema: master; Owner: eaudit
--

ALTER TABLE ONLY master.profile
    ADD CONSTRAINT profile_pkey PRIMARY KEY (id);


--
-- Name: prosedur_audit_pkey; Type: CONSTRAINT; Schema: master; Owner: eauditpr_user
--

ALTER TABLE ONLY master.prosedur_audit
    ADD CONSTRAINT prosedur_audit_pkey PRIMARY KEY (id);


--
-- Name: resiko_satker_pkey; Type: CONSTRAINT; Schema: master; Owner: eauditpr_user
--

ALTER TABLE ONLY master.resiko_satker
    ADD CONSTRAINT resiko_satker_pkey PRIMARY KEY (id);


--
-- Name: sasaran_ket_key; Type: CONSTRAINT; Schema: master; Owner: eaudit
--

ALTER TABLE ONLY master.sasaran
    ADD CONSTRAINT sasaran_ket_key UNIQUE (ket);


--
-- Name: sasaran_pkey; Type: CONSTRAINT; Schema: master; Owner: eaudit
--

ALTER TABLE ONLY master.sasaran
    ADD CONSTRAINT sasaran_pkey PRIMARY KEY (id);


--
-- Name: satker_ket_key; Type: CONSTRAINT; Schema: master; Owner: eauditpr_user
--

ALTER TABLE ONLY master.satker
    ADD CONSTRAINT satker_ket_key UNIQUE (ket);


--
-- Name: satker_pkey; Type: CONSTRAINT; Schema: master; Owner: eauditpr_user
--

ALTER TABLE ONLY master.satker
    ADD CONSTRAINT satker_pkey PRIMARY KEY (id);


--
-- Name: tahapan_audit_pkey; Type: CONSTRAINT; Schema: master; Owner: eaudit
--

ALTER TABLE ONLY master.tahapan_audit
    ADD CONSTRAINT tahapan_audit_pkey PRIMARY KEY (kode);


--
-- Name: tindak_lanjut_pkey; Type: CONSTRAINT; Schema: master; Owner: eauditpr_user
--

ALTER TABLE ONLY master.tindak_lanjut
    ADD CONSTRAINT tindak_lanjut_pkey PRIMARY KEY (id);


--
-- Name: tujuan_ket_key; Type: CONSTRAINT; Schema: master; Owner: eaudit
--

ALTER TABLE ONLY master.tujuan
    ADD CONSTRAINT tujuan_ket_key UNIQUE (ket);


--
-- Name: tujuan_pkey; Type: CONSTRAINT; Schema: master; Owner: eaudit
--

ALTER TABLE ONLY master.tujuan
    ADD CONSTRAINT tujuan_pkey PRIMARY KEY (id);


--
-- Name: menu_pkey; Type: CONSTRAINT; Schema: public; Owner: eaudit
--

ALTER TABLE ONLY public.menu
    ADD CONSTRAINT menu_pkey PRIMARY KEY (id);


--
-- Name: periode_pkey; Type: CONSTRAINT; Schema: public; Owner: eauditpr_user
--

ALTER TABLE ONLY public.periode
    ADD CONSTRAINT periode_pkey PRIMARY KEY (id);


--
-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner: eaudit
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: rolemenu_pkey; Type: CONSTRAINT; Schema: public; Owner: eaudit
--

ALTER TABLE ONLY public.rolemenu
    ADD CONSTRAINT rolemenu_pkey PRIMARY KEY (role, menu);


--
-- Name: roleusers_pkey; Type: CONSTRAINT; Schema: public; Owner: eaudit
--

ALTER TABLE ONLY public.roleusers
    ADD CONSTRAINT roleusers_pkey PRIMARY KEY (role, "user");


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: eaudit
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: anggota_pkey; Type: CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (tim, anggota);


--
-- Name: aturan_kode_key; Type: CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.aturan
    ADD CONSTRAINT aturan_kode_key UNIQUE (kode);


--
-- Name: aturan_pkey; Type: CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.aturan
    ADD CONSTRAINT aturan_pkey PRIMARY KEY (id);


--
-- Name: coa_pkey; Type: CONSTRAINT; Schema: support; Owner: eauditpr_user
--

ALTER TABLE ONLY support.kode_temuan
    ADD CONSTRAINT coa_pkey PRIMARY KEY (id);


--
-- Name: kertas_kerja_kode_key; Type: CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.kertas_kerja
    ADD CONSTRAINT kertas_kerja_kode_key UNIQUE (kode);


--
-- Name: kertas_kerja_pkey; Type: CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.kertas_kerja
    ADD CONSTRAINT kertas_kerja_pkey PRIMARY KEY (id);


--
-- Name: notif_pkey; Type: CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.notif
    ADD CONSTRAINT notif_pkey PRIMARY KEY (id);


--
-- Name: pedoman_kode_key; Type: CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.pedoman
    ADD CONSTRAINT pedoman_kode_key UNIQUE (kode);


--
-- Name: pedoman_pkey; Type: CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.pedoman
    ADD CONSTRAINT pedoman_pkey PRIMARY KEY (id);


--
-- Name: program_satker_pkey; Type: CONSTRAINT; Schema: support; Owner: eauditpr_user
--

ALTER TABLE ONLY support.program_satker
    ADD CONSTRAINT program_satker_pkey PRIMARY KEY (id);


--
-- Name: tao_detail_pkey; Type: CONSTRAINT; Schema: support; Owner: eauditpr_user
--

ALTER TABLE ONLY support.tao_detail
    ADD CONSTRAINT tao_detail_pkey PRIMARY KEY (id);


--
-- Name: tao_kode_key; Type: CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.tao
    ADD CONSTRAINT tao_kode_key UNIQUE (kode);


--
-- Name: tao_pkey; Type: CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.tao
    ADD CONSTRAINT tao_pkey PRIMARY KEY (id);


--
-- Name: tim_pkey; Type: CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.tim
    ADD CONSTRAINT tim_pkey PRIMARY KEY (id);


--
-- Name: tim_tim_key; Type: CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.tim
    ADD CONSTRAINT tim_tim_key UNIQUE (tim);


--
-- Name: wilayah_pkey; Type: CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.wilayah
    ADD CONSTRAINT wilayah_pkey PRIMARY KEY (id);


--
-- Name: dasar_penugasan_id_key; Type: INDEX; Schema: master; Owner: eaudit
--

CREATE UNIQUE INDEX dasar_penugasan_id_key ON master.dasar_penugasan USING btree (id);


--
-- Name: jenis_id_key; Type: INDEX; Schema: master; Owner: eauditpr_user
--

CREATE UNIQUE INDEX jenis_id_key ON master.jenis USING btree (id);


--
-- Name: pkpt_id; Type: INDEX; Schema: master; Owner: eaudit
--

CREATE UNIQUE INDEX pkpt_id ON master.pkpt USING btree (no);


--
-- Name: sasaran_id_key; Type: INDEX; Schema: master; Owner: eaudit
--

CREATE UNIQUE INDEX sasaran_id_key ON master.sasaran USING btree (id);


--
-- Name: satker_id_key; Type: INDEX; Schema: master; Owner: eauditpr_user
--

CREATE UNIQUE INDEX satker_id_key ON master.satker USING btree (id);


--
-- Name: tujuan_id_key; Type: INDEX; Schema: master; Owner: eaudit
--

CREATE UNIQUE INDEX tujuan_id_key ON master.tujuan USING btree (id);


--
-- Name: sessions_timestamp; Type: INDEX; Schema: public; Owner: eaudit
--

CREATE INDEX sessions_timestamp ON public.sessions USING btree ("timestamp");


--
-- Name: users_id_key; Type: INDEX; Schema: public; Owner: eaudit
--

CREATE UNIQUE INDEX users_id_key ON public.users USING btree (id);


--
-- Name: users_username_idx; Type: INDEX; Schema: public; Owner: eaudit
--

CREATE UNIQUE INDEX users_username_idx ON public.users USING btree (username);


--
-- Name: tim_id_key; Type: INDEX; Schema: support; Owner: eaudit
--

CREATE UNIQUE INDEX tim_id_key ON support.tim USING btree (id);


--
-- Name: wilayah_id_key; Type: INDEX; Schema: support; Owner: eaudit
--

CREATE UNIQUE INDEX wilayah_id_key ON support.wilayah USING btree (id);


--
-- Name: insert_perencanaan; Type: TRIGGER; Schema: interpro; Owner: eauditpr_user
--

CREATE TRIGGER insert_perencanaan AFTER INSERT ON interpro.perencanaan FOR EACH ROW EXECUTE PROCEDURE interpro.insert_perencanaan();


--
-- Name: insert_perencanaan_detail; Type: TRIGGER; Schema: interpro; Owner: eauditpr_user
--

CREATE TRIGGER insert_perencanaan_detail AFTER INSERT ON interpro.perencanaan_detail FOR EACH ROW EXECUTE PROCEDURE interpro.insert_perencanaan_detail();


--
-- Name: update_perencanaan; Type: TRIGGER; Schema: interpro; Owner: eauditpr_user
--

CREATE TRIGGER update_perencanaan AFTER UPDATE ON interpro.perencanaan FOR EACH ROW EXECUTE PROCEDURE interpro.update_perencanaan();


--
-- Name: update_perencanaan_detail; Type: TRIGGER; Schema: interpro; Owner: eauditpr_user
--

CREATE TRIGGER update_perencanaan_detail AFTER UPDATE ON interpro.perencanaan_detail FOR EACH ROW EXECUTE PROCEDURE interpro.update_perencanaan_detail();


--
-- Name: insert_role; Type: TRIGGER; Schema: public; Owner: eaudit
--

CREATE TRIGGER insert_role AFTER INSERT ON public.roleusers FOR EACH ROW EXECUTE PROCEDURE public.insert_roleusers();


--
-- Name: perencanaan_aturan_aturan_fkey; Type: FK CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan_aturan
    ADD CONSTRAINT perencanaan_aturan_aturan_fkey FOREIGN KEY (aturan) REFERENCES support.aturan(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: perencanaan_aturan_id_fkey; Type: FK CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan_aturan
    ADD CONSTRAINT perencanaan_aturan_id_fkey FOREIGN KEY (id) REFERENCES interpro.perencanaan(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: perencanaan_dasar_fkey; Type: FK CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan
    ADD CONSTRAINT perencanaan_dasar_fkey FOREIGN KEY (dasar) REFERENCES master.dasar_penugasan(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: perencanaan_detail_id_fkey; Type: FK CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan_detail
    ADD CONSTRAINT perencanaan_detail_id_fkey FOREIGN KEY (id) REFERENCES interpro.perencanaan(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: perencanaan_detail_tao_fkey; Type: FK CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan_detail
    ADD CONSTRAINT perencanaan_detail_tao_fkey FOREIGN KEY (tao) REFERENCES support.tao_detail(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: perencanaan_jenis_fkey; Type: FK CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan
    ADD CONSTRAINT perencanaan_jenis_fkey FOREIGN KEY (jenis) REFERENCES master.jenis(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: perencanaan_pedoman_id_fkey; Type: FK CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan_pedoman
    ADD CONSTRAINT perencanaan_pedoman_id_fkey FOREIGN KEY (id) REFERENCES interpro.perencanaan(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: perencanaan_pedoman_pedoman_fkey; Type: FK CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan_pedoman
    ADD CONSTRAINT perencanaan_pedoman_pedoman_fkey FOREIGN KEY (pedoman) REFERENCES support.pedoman(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: perencanaan_satker_fkey; Type: FK CONSTRAINT; Schema: interpro; Owner: eauditpr_user
--

ALTER TABLE ONLY interpro.perencanaan
    ADD CONSTRAINT perencanaan_satker_fkey FOREIGN KEY (satker) REFERENCES master.satker(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tim_wilayah_fkey; Type: FK CONSTRAINT; Schema: support; Owner: eaudit
--

ALTER TABLE ONLY support.tim
    ADD CONSTRAINT tim_wilayah_fkey FOREIGN KEY (wilayah) REFERENCES support.wilayah(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

