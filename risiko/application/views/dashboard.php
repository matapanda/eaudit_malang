<div class="row">
    <div class="col-lg-12 col-xs-12">
    <form method="get" class="row hidden-print" action="?">
        <div class="col-md-6">
            <div class="dataTables_wrapper form-inline">
                <select name="tahun" onchange="this.form.submit()" class="form-control input-sm">
                    <?php
                    foreach($list_tahun as $l){
                        echo "<option value='$l[tahun]' ".($l['tahun']==$tahun?'selected=""':'').">$l[tahun]</option>";
                    }
                    ?>
                </select>
            </div>
        </div>


    </form>
    </div>
    <div class="col-lg-6 col-xs-12">
        <div class="card-box">
            <div id="container1" style="width:100%; height:400px;"></div>
        </div>
    </div>

    <div class="col-lg-6 col-xs-12">
        <div class="card-box">
            <div id="container2" style="width:100%; height:400px;"></div>
        </div>
    </div>

    <div class="col-lg-6 col-xs-12">
        <div class="card-box">
            <div id="container3" style="width:100%; height:400px;"></div>
        </div>
    </div>
    <div class="col-lg-6 col-xs-12">
        <div class="card-box">
            <div id="container4" style="width:100%; height:400px;"></div>
        </div>
    </div>
    <div class="col-lg-12 col-xs-12">
        <div class="card-box">
            <div id="container5" style="width:100%; height:400px;"></div>
        </div>
    </div>
    <div class="col-lg-12 col-xs-12">
        <div class="card-box">
            <div id="container6" style="width:100%; height:400px;"></div>
        </div>
    </div>
<!--    <div class="col-lg-12 col-xs-12">-->
<!--        <div class="card-box">-->
<!--            <div id="container6" style="min-width: 100%; height: 600px; margin: 0 auto"></div>-->
<!--        </div>-->
<!--    </div>-->

</div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="<?=base_url()?>assets/plugins/moment/moment.js"></script>
<script>
    $(document).ready(function () {


        //Highcharts.chart('container6', {
        //    chart: {
        //        type: 'scatter'
        //    },
        //    title: {
        //        text: 'Laporan Tipe Resiko'
        //    },
        //    xAxis: {
        //        categories: [
        //            'Tipe Resiko'
        //        ],
        //        crosshair: true
        //    },
        //    yAxis: {
        //        min: 0,
        //    },
        //    plotOptions: {
        //        column: {
        //            pointPadding: 0.2,
        //            borderWidth: 0
        //        }
        //    },
        //    series: [
        //        <?php
        //        foreach ($risk as $s){
        //            echo"{name: '$s[nama]',
        //            data:[[".null_is_nol($s['tot']).",".null_is_nol($s['tot2'])."]]
        //            },
        //            ";
        //        }
        //        ?>//]
        //    //
        //    //     series: [{
        //    //         name: 'Tokyo',
        //    //     data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
        //    //
        //    // }, {
        //    //     name: 'New York',
        //    //         data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
        //    //
        //    // }, {
        //    //     name: 'London',
        //    //         data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
        //    //
        //    // }, {
        //    //     name: 'Berlin',
        //    //         data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
        //    //
        //    // }]
        //});
        Highcharts.chart('container5', {
            chart: {
                type: 'scatter'
            },
            title: {
                text: 'Risk Map'
            },
            xAxis: {
                categories: [
                    'Tipe Resiko'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
                <?php
                foreach ($risk as $s){
                    echo"{name: '$s[nama]',
                    data:[[".null_is_nol($s['tot']).",".null_is_nol($s['tot2'])."]]
                    },
                    ";
                }
                ?>]
            //
            //     series: [{
            //         name: 'Tokyo',
            //     data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
            //
            // }, {
            //     name: 'New York',
            //         data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
            //
            // }, {
            //     name: 'London',
            //         data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
            //
            // }, {
            //     name: 'Berlin',
            //         data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
            //
            // }]
        });

        // Build the chart
        Highcharts.chart('container1', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Monitoring SPT'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },credits: {
                enabled: false
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'SPT Terbit',
                    y: <?=$sesuai['total'];?>
                }, {
                    name: 'SPT Belum Terbit',
                    y: <?=$tidak['total'];?>
                },]
            }]
        });

        // Build the chart
        Highcharts.theme = {
            colors: ['#f45b5b', '#8085e9', '#8d4654', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
                '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],}

        Highcharts.setOptions(Highcharts.theme);

        Highcharts.chart('container2', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Monitoring LHP'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },credits: {
                enabled: false
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'LHP Terbit',
                    y: <?=$lhp_sesuai['total'];?>
                }, {
                    name: 'LHP Belum Terbit',
                    y: <?=$lhp_tidak['total'];?>
                },]
            }]
        });

        Highcharts.chart('container3', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Proses Audit'
            },xAxis: {
                type: 'category'
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },
            legend: {
                enabled: false
            },credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },
            series: [{
                name: 'Proses',
                colorByPoint: true,
                data: [
                    {name: 'Perencanaan',
                        y: <?=$perencanaan['total']/$perencanaan_all['total']*100?>,
                    },
                    {name: 'Pelaksanaan',
                        y: <?=$pelaksanaan['total']/$perencanaan_all['total']*100?>,
                    },{name: 'Pelaporan',
                        y: <?=$pelaporan['total']/$perencanaan_all['total']*100?>,
                    },
                ]
            }]
        });

        Highcharts.chart('container4', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Monitoring Penugasan'
            },xAxis: {
                type: 'category'
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },
            legend: {
                enabled: false
            },credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },
            series: [{
                name: 'Kinerja Telah Selesai',
                colorByPoint: true,
                data: [
                    <?php
                    foreach ($kinerja as $u) {
                            $realisasi=$u['realisasi'];
                            $realisasi2=$u['selesai'];
                            $disetujui = 0;
                            $direalisasi = 0;
                            if ($u['jumlah'] > 0) {
                                if ($u['selesai'] > 0) {
                                    $disetujui = ($u['selesai'] / $u['jumlah'] * 100);
                                }
                                $u['realisasi'] = $u['realisasi'] - $u['selesai'];
                                if ($u['realisasi'] > 0) {
                                    $direalisasi = ($u['realisasi'] / $u['jumlah'] * 100);
                                }
                            }

                    ?>
                    {
                        name: '<?=$u['nama']?>',
                        y: <?= $realisasi2?>,
                    },
                    <?php
                    }
                    ?>
                ]
            }]
        });
    });
</script>
