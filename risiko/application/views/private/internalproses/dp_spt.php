<div class="row">
    <div class="col-sm-12">
        <div class="card-box">

            <?php
            show_alert();
            ?>
            <div class="">
                <h2>List Permintaan Data Terdaftar</h2>
                <table id="simple-table" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-10">Permintaan Data</th>
                        <th class="center col-xs-1"><input type="hidden" id="no" value="0"> </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $n=1;
                    if(@$pengantar){
                        foreach($pengantar as $l){
                            ?>
                            <tr class="<?=$n?>">
                                <td class=""><?=@$l['isian']?></td>
                                <td class="center">
                                    <a class="btn btn-danger btn-xs" href="?iddel=<?=$l['id_p']?>&&pdel=<?=@$l['urutan']?>"><i class="fa fa-remove"></i></a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-box">
            <div class="" style="text-align: right">
                <button onclick="tambah()" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah Data</button>
            </div>
            <hr>

            <div class="">
                <h2>Permintaan Data Tambahan</h2>
                <form method="post" action="?dat=true">
                    <table id="simple-table" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="center col-xs-10">Permintaan Data Pengantar</th>
                            <th class="center col-xs-1">
                                <input type="hidden" id="no" value="0">
                                <input type="hidden" id="id_spt" name="id_spt" value="<?=$id_spt?>">
                            </th>
                        </tr>
                        </thead>
                        <tbody id="materi">

                        </tbody>
                    </table>
                    <input type="submit" value="SAVE" class="btn btn-primary">
                </form>

            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    function tambah() {
        var no=$('#no').val();
        var urutan=parseFloat(no)+1;
        var html='<tr id="'+urutan+'">\n' +
            '                        <td ><textarea name="isian[]" rows="2" style="width: 100%!important;" class="from-control"></textarea> </td>\n' +
            '                        <td class="center"><button class="btn btn-danger btn-xs" onclick="rem('+urutan+')"><i class="fa fa-remove"></i></button></td>\n' +
            '                    </tr>';
        $('#materi').append(html);
        $('#no').val(urutan);
    }
    function rem(_i) {
        $('#'+_i).remove();
    }
</script>
