<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-xs-12">
                <form id="form-data" name="datauserForm" novalidate="" action="?simpan2=true" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Periode</label>
                        <div class="col-md-5">
                            <select class="form-control select2" name="header[periode]" id="periode" onchange="cek_jumlah_hari()" ng-model="periode" required>
                                <option value="">pilih periode</option>
                                <?php
                                foreach ($periode as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nomor PKPT</label>
                        <div class="col-md-9" id="pkpts">
                            <select class="form-control select2" onchange="pkpts()" id="no_pkpt" ng-model="pkpts" name="header[pkpt_no]" required>
                                <option value="">pilih nomor PKPT</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Jenis audit</label>
                        <div class="col-md-5">
                            <select class="form-control select2" name="header[jenis]" id="jenis_a" ng-model="jenis" required>
                                <option value="">pilih jenis audit</option>
                                <?php
                                foreach ($jenis as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[kode] - $v[ket]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Satuan kerja yang diaudit</label>
                        <div class="col-md-7">
                            <select class="form-control select2" name="header[satker]" ng-model="satker" required>
                                <option value="">pilih satuan kerja</option>
                                <?php
                                foreach ($satker as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[kode] - $v[ket]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Pengantar Satker</label>
                        <div class="col-md-7">
                            <select class="form-control select2 select2-multiple" multiple name="header[satker_p][]" required>
                                <option value="">pilih satuan kerja</option>
                                <?php
                                foreach ($satker as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[kode] - $v[ket]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Judul SPT</label>
                        <div class="col-md-6">
                            <input type="text" name="header[judul]" autocomplete="off" ng-model="judul"
                                   placeholder="judul surat perintah tugas" required="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Sasaran audit</label>
                        <div class="col-md-9">
                            <select class="select2 form-control select2-multiple" id="sasaran_a" onchange="init_kk()" name="sasaran[]" ng-model="sasaran"
                                    multiple="multiple" multiple data-placeholder="pilih sasaran audit" required>
                                <option value="">pilih sasaran audit</option>
                                <?php
                                foreach ($sasaran as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[kode] - $v[ket]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tujuan audit</label>
                        <div class="col-md-9">
                            <select class="select2 form-control select2-multiple" id="tujuan_a" name="tujuan[]" ng-model="tujuan"
                                    multiple="multiple" multiple data-placeholder="pilih tujuan audit" required>
                                <option value="">pilih tujuan audit</option>
                                <?php
                                foreach ($tujuan as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[kode] - $v[ket]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Jangka waktu audit</label>
                        <div class="col-md-9">
                            <div class="col-md-4">
                                <div class="input-daterange input-group" id="jangkawaktu">
                                    <input type="text" readonly class="form-control input-sm" onchange="cek_jumlah_hari()" id="start_date" name="header[start_date]"
                                           value="<?= date('d/m/Y') ?>">
                                    <span class="input-group-addon input-sm">~</span>
                                    <input type="text" readonly class="form-control input-sm" onchange="cek_jumlah_hari()" id="end_date" name="header[end_date]"
                                           value="<?= date('d/m/Y') ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <span class="col-md-4 text-muted font-17 pd-025" id="jangkawaktuday"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tim Pelaksana</label>
                        <div class="col-md-7">
                            <input type="hidden" name="tim" id="tim_pelaksana">
                            <select class="form-control select2" id="p_pelaksana" onchange="tambahpelaksana()">
                                <option value="">pilih tim</option>
                                <?php
                                $temp_tim = "";
                                foreach ($tim as $i => $v):
                                    if ($v['nama'] == null)
                                        continue;
                                    if ($v['id_tim'] <> $temp_tim) {
                                        if ($i > 0) {
                                            echo "</optgroup>";
                                        }
                                        echo "<optgroup label='$v[wilayah] - $v[tim]'>";
                                        $temp_tim = $v['id_tim'];
                                    }
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[nama] - $v[jabatan]" ?></option>
                                <?php
                                endforeach;
                                echo "</optgroup>";
                                ?>
                            </select>
                        </div>
                        <div class="col-md-2 hidden">
                            <button type="button" onchange="tambahpelaksana()" class="btn btn-sm btn-inverse"><i
                                        class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-md-offset-3 col-md-9">
                            <ul class="list-group">
                                <div id="tims">

                                </div>
                            </ul>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Ketua Tim</label>
                        <div class="col-md-7" id="kt">
                            <select class="form-control select2" name="header[ketuatim]" ng-model="ketuatim">
                                <option value="">pilih ketua tim</option>
                             </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Dalnis</label>
                        <div class="col-md-7" id="dl">
                            <select class="form-control select2" name="header[dalnis]"  ng-model="dalnis">
                                <option value="">pilih dalnis</option>
                             </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Irban</label>
                        <div class="col-md-7" id="irb">
                            <select class="form-control select2" name="header[irban]" ng-model="irban">
                                <option value="">pilih irban</option>
                             </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Peraturan yang terkait</label>
                        <div class="col-md-9">
<!--                            <span ng-hide="jenis && sasaran" class="col-md-4 text-muted font-17 pd-025">Peraturan yang terkait</span>-->
                            <table class="table table-striped">
                                <tbody id="atur">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Pedoman audit</label>
                        <div class="col-md-9">
<!--                            <span ng-hide="jenis && sasaran" class="col-md-4 text-muted font-17 pd-025">Pedoman audit</span>-->
                            <table class="table table-striped">
                                <tbody id="pedom">

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Audit program</label>
                        <div class="col-md-9">
<!--                            <span ng-hide="jenis && sasaran" class="col-md-4 text-muted font-17 pd-025">Audit program</span>-->
                            <table class="table table-striped" id="tabs">

                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Total Hari</label>
                        <div class="col-md-4">
                            <input type="text" name="header[jml_hari]" id="total_hari" autocomplete="off"
                                   placeholder="Total Hari" readonly required="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nomor SPT</label>
                        <div class="col-md-4">
                            <input type="text" name="header[spt_no]" ng-model="spt_no" autocomplete="off"
                                   placeholder="nomor surat perintah tugas" required="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tanggal SPT</label>
                        <div class="col-md-4">
                            <input type="text" name="header[spt_date]" readonly autocomplete="off"
                                   placeholder="tanggal surat perintah tugas" required="" value="<?= date('d/m/Y') ?>"
                                   class="form-control datepicker">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-4">
                            <input type="hidden" name="header[jml]" id="jml">
                            <input type="submit" class="hidden">
                            <input type="submit" class="btn btn-inverse" value="SUBMIT">
                            <button type="button" ng-click="simpan(1)" class="btn btn-success"><i
                                        class="fa fa-file-text"></i> DRAFT
                            </button>
                            <a href="<?= base_url('internalproses/rencana') ?>" class="btn btn-default"><i
                                        class="fa fa-backward"></i> KEMBALI</a>
                        </div>
                    </div>
                </form>
            </div><!-- end col-->
        </div>
    </div>
</div>
<div class="modal fade" id="taoModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"
      type="text/css"/>
<script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    const component = this;
    var _tim=[];
    // var _tim2=new array();
    var _tao=[];
    var _pedoman=[];
    var _aturan=[];
    function pkpts(){
        var no= $('#no_pkpt').val();
        $.post('?no_pkpt2='+no, function (data, status) {
            if(data.tujuan!=null){
                $("#jenis_a").val(data.jenis);
                $("#jenis_a").trigger("change");
                $("#tujuan_a").val(data.tujuan);
                $("#tujuan_a").trigger("change");
                $("#sasaran_a").val(data.sasaran); //set the value
                $("#sasaran_a").trigger("change");
                // var a=array('[',']');
                _tim=data.list;
                _tim2=data.list;
                $('#tim_pelaksana').val(JSON.stringify(_tim));
                console.log(_tim);

                $.each( _tim, function( key, value ) {
                    var htm=' <li class="list-group-item" id="tim'+key+'">\n' +
                        '                                    <div class="row hand-cursor">\n' +
                        '                                        <div class="col-md-1">\n' +
                        '                                            <a class="btn btn-xs btn-danger" onclick="hapuspelaksana('+key+')">\n' +
                        '                                                <i class="fa fa-remove"></i></a>\n' +
                        '                                        </div>\n' +
                        '                                        <div class="col-md-10">\n' +
                        value.nama+'\n' +
                        '                                        </div>\n' +
                        '                                    </div>\n' +
                        '                                </li>';
                    $('#tims').append(htm);
                });
                init_tim();
                init_kk();
                $('.select2').select2();
            }
        },'json');

        // angular.scope().cek_pkpts();
    }
    function init_kk(){

        var jenis=$("#jenis_a").val();
        var tujuan=$("#tujuan_a").val();
        var sasaran=$("#sasaran_a").val();
        var jml=0;
        $.post('?jenis_a='+jenis+'&tujuan_a='+tujuan+'&sasaran_a='+sasaran, function (data, status) {
            _tao=data.list;
            $('#tim_pelaksana').val(JSON.stringify(_tim));
            _pedoman=data.pedoman;
            _aturan=data.aturan;
            console.log(_tao);
            console.log(_tim);
            var listb='';
            var urutan=1;
            var tahap=[1,2,3];
            for(var i=0;i<tahap.length;i++){
                if(i==0){
                    listb+='<thead>\n'+
                        '                                    <th class="center" colspan="4">PENDAHULUAN</th>\n'+
                        '                                    </thead>\n'+
                        '                                    <tbody>\n';
                }
                else if(i==1){
                    listb+='<thead>\n'+
                        '                                    <th class="center" colspan="4">LANJUTAN</th>\n'+
                        '                                    </thead>\n'+
                        '                                    <tbody>\n';
                }
                else{
                    listb+='<thead>\n'+
                        '                                    <th class="center" colspan="4">PENYELESAIAN</th>\n'+
                        '                                    </thead>\n'+
                        '                                    <tbody>\n';
                }

            $.each( _tao, function( key, value ) {
                var no =parseFloat(value.tahapan)-1;
                if(no==i){
            listb+='<tr class="selection">\n'+
                '<td class="col-xs-4"><input type="hidden" name="tao'+jml+'" value="'+value.id+'">'+value.kode_kk+' '+value.langkah+'</td>\n'+
                '<td class="col-xs-4">\n'+
                '<select class="select2 form-control select2-multiple" multiple="multiple" multiple name="pelaksana'+jml+'[]">\n';
                $.each( _tim, function( key, values ) {
                    listb+='<option value="'+values.id+'">'+values.nama+'</option>\n';
                });
                $('#jml').val(jml);
                listb+='</select>\n'+
                '</td>\n'+
                '<td class="col-xs-2">\n'+
                '<input type="text" name="pelaksanaan'+jml+'" placeholder="tanggal" value="" readonly="" class="form-control datepicker">\n'+
                '</td>\n'+
                '<td class="col-xs-1">\n'+
                '<input type="number" name="jumlah'+jml+'" onkeyup="hitung_hari()" placeholder="jumlah" value="" class="form-control jumlah_hari">\n'+
                '</td>\n'+
                '<td class="right">\n'+
                '<button type="button" onclick="detailaturan('+value.id+')" class="btn btn-sm btn-success"><i class="fa fa-search"></i></button>\n'+
                '</td>\n'+
                '</tr>\n';

                    jml=jml+1;
                }
            });
            listb+='</tbody>';
            }

            $('#tabs').html(listb);

            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            var ped='';
            var atur='';
            $.each( _pedoman, function( key, value ) {
                ped+='   <tr "\n' +
                    '                                    class="selection">\n' +
                    '                                    <td class="">'+value.kode+'</td>\n' +
                    '                                    <td class="right">\n' +
                    '                                        <a href="<?= base_url("img") . "/" ?>'+value.file+'"\n' +
                    '                                           class="btn btn-sm btn-success" target="_blank"><i\n' +
                    '                                                    class="fa fa-file-text"></i></a>\n' +
                    '                                    </td>\n' +
                    '                                </tr>';
            });
            $('#pedom').html(ped);
            $.each( _aturan, function( key, value ) {
                atur+='   <tr "\n' +
                    '                                    class="selection">\n' +
                    '                                    <td class="">'+value.kode+'</td>\n' +
                    '                                    <td class="right">\n' +
                    '                                        <a href="<?= base_url("img") . "/" ?>'+value.file+'"\n' +
                    '                                           class="btn btn-sm btn-success" target="_blank"><i\n' +
                    '                                                    class="fa fa-file-text"></i></a>\n' +
                    '                                    </td>\n' +
                    '                                </tr>';
            });
            $('#atur').html(atur);

            $('.select2').select2();
    },'json');

    }
    function hitung_hari() {
        var total_hari=0;
        $('.jumlah_hari').each(function() {
            var n = parseFloat($(this).val());
            if (!isNaN(n))
                total_hari += n;
        });
        $('#total_hari').val(total_hari);
    }
    function init_tim() {

        var kt='  <select class="form-control select2" name="header[ketuatim]" ng-model="ketuatim">\n' +
            '<option value="">pilih ketua tim</option>\n';

        $.each( _tim, function( key, value ) {
            kt+='                                <option value="'+value.id+'">'+value.nama+'</option>\n';
        });
        kt+='                             </select>';
        $('#kt').html(kt);

        var dl='  <select class="form-control select2" name="header[dalnis]" ng-model="dalnis">\n' +
            '<option value="">pilih dalnis</option>\n';

        $.each( _tim, function( key, value ) {
            dl+='                                <option value="'+value.id+'">'+value.nama+'</option>\n';
        });
        dl+='                             </select>';
        $('#dl').html(dl);

        var irb='  <select class="form-control select2" name="header[irban]" ng-model="irban">\n' +
            '<option value="">pilih irban</option>\n';

        $.each( _tim, function( key, value ) {
            irb+='                                <option value="'+value.id+'">'+value.nama+'</option>\n';
        });
        irb+='                             </select>';
        $('#irb').html(irb);

        $('#tim_pelaksana').val(JSON.stringify(_tim));
        $('.select2').select2();
    }
    function hapuspelaksana(a){
        _tim.splice(a, 1);
        $('#tim'+a).remove();
        init_tim();
        init_kk();
        $('#tim_pelaksana').val(JSON.stringify(_tim));
        console.log(_tim);
    }
    function tambahpelaksana(){
        var pel=$('#p_pelaksana').val();
        var pel_t=$( "#p_pelaksana option:selected" ).text();
        _tim.push({'nama':pel_t,'id': pel });
        $('#tim_pelaksana').val(JSON.stringify(_tim));
        var no =parseFloat(_tim.length)-1;
        var htm=' <li class="list-group-item" id="tim'+no+'">\n' +
            '                                    <div class="row hand-cursor">\n' +
            '                                        <div class="col-md-1">\n' +
            '                                            <a class="btn btn-xs btn-danger" onclick="hapuspelaksana('+no+')">\n' +
            '                                                <i class="fa fa-remove"></i></a>\n' +
            '                                        </div>\n' +
            '                                        <div class="col-md-10">\n' +
            pel_t+'\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                </li>';
        $('#tims').append(htm);
        init_tim();
        init_kk();
        console.log(_tim);
    }
    function jangkawaktuaudit() {
        var _sd = $('input[name="header[start_date]"]').datepicker("getDate");
        var _ed = $('input[name="header[end_date]"]').datepicker("getDate");
        var timeDiff = Math.abs(_ed.getTime() - _sd.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        $('span#jangkawaktuday').html(diffDays + ' Hari');
    }
    function cek_jumlah_hari(){
        var periode= $('#periode').val();
        var periode_nama= $('option:selected','#periode').text();
        var start= $('#start_date').val();
        var end= $('#end_date').val();

        $.post('?periode=' +periode+'&a='+start+'&e='+end, function (data, status) {
            $('#jangkawaktuday').empty();
            $('#jangkawaktuday').html(data);
        });
        var _periode=$('select[name="header[pkpt_no]"]').val();
        $.post('?periodes=' +periode_nama, function (data, status) {
            $('select[name="header[pkpt_no]"]').html(data);
            $('select[name="header[pkpt_no]"]').val(_periode);
            $('.select2').select2();
        });
    }
    $(function () {
        $('.select2').select2();
        cek_jumlah_hari();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('#jangkawaktu').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on("change", function (e) {
            jangkawaktuaudit();
        });
        jangkawaktuaudit();
    });
</script>
