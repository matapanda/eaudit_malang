<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <a href="#new-group" class="<?=is_authority(@$access['c'])?> btn btn-inverse" data-animation="blur" data-plugin="custommodal" data-overlayspeed="100" data-overlaycolor="#36404a"><i class="fa fa-plus"></i> TAMBAH DATA SATKER</a>
            <a href="?p=true" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-edit"></i> PENILAIAN RESIKO</a>
            <a href="?" class="btn btn-inverse"><i class="fa fa-refresh"></i></a>
            <hr>
            <form method="get" class="row" action="<?=base_url('master/satker')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="" <?=$closed==''?'selected':''?>>Semua Status</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status Aktif</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status Non Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <br><table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-1">Kode</th>
                        <th class="center col-xs-2">Nama</th>
                        <th class="center col-xs-2">Alamat</th>
                        <th class="center col-xs-2">Kepala</th>
                        <th class="center col-xs-1">Jml Pegawai</th>
                        <th class="center col-xs-1">Jml Anggaran</th>
                        <th class="center col-xs-1">Jml Satker Dibawahnya</th>
                        <th class="center col-xs-1">Resiko</th>
                        <th class="center col-xs-2">Keterangan Resiko</th>
                        <th class="center col-xs-1">Status</th>
                        <th class="center col-xs-1"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($satker as $g) {
                        $tot=(@$g['s_r']>0?$g['s_r']:-1);
                        if($tot>=60&&$tot<=100){
                            $ket='Setiap Tahun : Perlu  tindakan  segera  untuk  mengatasi  risiko';
                            $resiko='Tidak Diterima';
                        }
                        elseif($tot>=40&&$tot<=59){
                            $ket='Dua Tahun : Perlu  tindakan  untuk  mengatasi  risiko';
                            $resiko='Issue Utama';
                        }
                        elseif($tot>=20&&$tot<=39){
                            $ket='Tiga Tahun : Tindakan  disarankan  dilakukan  jika  sumber daya  tersedia';
                            $resiko='Issue Tambahan';
                        }
                        elseif($tot>=0&&$tot<=19){
                            $ket='Tidak Perlu : Tidak  perlu  ditindaklanjuti';
                            $resiko='Dapat Diterima';
                        }
                        else{
                            $resiko='Belum Dinilai';
                            $ket='Belum Dinilai';
                        }
                        ?>
                        <tr>
                            <td scope="row" class="center"><?=$no?></td>
                            <td class="center">
                                <a href="#" class="emjes-editable" id="<?=isset($access['u'])?"kode$g[id]":''?>"><?=$g['kode']?></a>
                                <script>
                                    $(function () {
                                        $('#kode<?=$g['id']?>').editable({
                                            type: 'text',
                                            name: 'kode',
                                            pk:'<?=$g['id']?>',
                                            url: '?',
                                            title: 'Keterangan Kode'
                                        });
                                    });
                                </script>
                            </td><td class="">
                                <a href="#" class="emjes-editable" id="<?=isset($access['u'])?"nama$g[id]":''?>"><?=$g['nama']?></a>
                                <script>
                                    $(function () {
                                        $('#nama<?=$g['id']?>').editable({
                                            type: 'text',
                                            name: 'nama',
                                            pk:'<?=$g['id']?>',
                                            url: '?',
                                            title: 'Nama Satker'
                                        });
                                    });
                                </script>
                            </td>
                            <td class="">
                                <a href="#" class="emjes-editable" id="<?=isset($access['u'])?"alamat$g[id]":''?>"><?=$g['alamat']?></a>
                                <script>
                                    $(function () {
                                        $('#alamat<?=$g['id']?>').editable({
                                            type: 'textarea',
                                            name: 'alamat',
                                            pk:'<?=$g['id']?>',
                                            url: '?',
                                            title: 'Alamat Satker'
                                        });
                                    });
                                </script>
                            </td>
                            <td class="">
                                <a href="#" class="emjes-editable" id="<?=isset($access['u'])?"kepala$g[id]":''?>"><?=$g['kepala']?></a>
                                <script>
                                    $(function () {
                                        $('#kepala<?=$g['id']?>').editable({
                                            type: 'text',
                                            name: 'kepala',
                                            pk:'<?=$g['id']?>',
                                            url: '?',
                                            title: 'Kepala satker'
                                        });
                                    });
                                </script>
                            </td>
                            <td class="">
                                <a href="#" class="emjes-editable" id="<?=isset($access['u'])?"jumlah_pegawai$g[id]":''?>"><?=$g['jumlah_pegawai']?></a>
                                <script>
                                    $(function () {
                                        $('#jumlah_pegawai<?=$g['id']?>').editable({
                                            type: 'text',
                                            name: 'jumlah_pegawai',
                                            pk:'<?=$g['id']?>',
                                            url: '?',
                                            title: 'Jumlah Pegawai'
                                        });
                                    });
                                </script>
                            </td>
                            <td class="">
                                <a href="#" class="emjes-editable" id="<?=isset($access['u'])?"jumlah_anggaran$g[id]":''?>"><?=$g['jumlah_anggaran']?></a>
                                <script>
                                    $(function () {
                                        $('#jumlah_anggaran<?=$g['id']?>').editable({
                                            type: 'text',
                                            name: 'jumlah_anggaran',
                                            pk:'<?=$g['id']?>',
                                            url: '?',
                                            title: 'Jumlah Aggaran'
                                        });
                                    });
                                </script>
                            </td>
                            <td class="">
                                <a href="#" class="emjes-editable" id="<?=isset($access['u'])?"jumlah_satker_dibawahnya$g[id]":''?>"><?=$g['jumlah_satker_dibawahnya']?></a>
                                <script>
                                    $(function () {
                                        $('#jumlah_satker_dibawahnya<?=$g['id']?>').editable({
                                            type: 'text',
                                            name: 'jumlah_satker_dibawahnya',
                                            pk:'<?=$g['id']?>',
                                            url: '?',
                                            title: 'Jumlah Satker Dibawahnya'
                                        });
                                    });
                                </script>
                            </td>

                            <td class="center"><?=(@$resiko?$resiko:'Belum di nilai')?></td>
                            <td class=""><?=(@$ket?$ket:'')?></td>
                            <td class="center <?= $g['id'] ?> hand-cursor" <?= isset($access['u']) ? "onclick=\"setStatusActive('$g[id]')\"" : '' ?>><?= getLabelStatus($g['status']) ?></td>
                            <td class="center <?= $g['id'] ?> hand-cursor"><a href="?e=<?=$g['id']?>"><label class="hand-cursor btn-block label label-warning">R. Satker</label></a>
                                <br><a href="?ef=<?=$g['id']?>"><label class="hand-cursor btn-block label label-info">F. Resiko</label></a></td>
                        </tr>
                        <?php
                        $no++;}
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>
<div id="new-group" class="modal-gradin">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Tambah Satker</h4>
    <div class="custom-modal-text">
        <form action="?" method="post">
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" name="kode" required class="form-control" placeholder="Kode Satker">
                    <br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="nama" required class="form-control" placeholder="Nama Satker">
                    <br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="alamat" required class="form-control" placeholder="Alamat Satker">
                    <br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="email" required class="form-control" placeholder="Alamat E-Mail">
                    <br>
                </div>
                <div class="col-sm-12">
                    <input type="number" name="jumlah_anggaran" required class="form-control" placeholder="Jumlah Anggaran">
                    <br>
                </div>
                <div class="col-sm-12">
                    <input type="number" name="jumlah_pegawai" required class="form-control" placeholder="Jumlah Pegawai">
                    <br>
                </div>
                <div class="col-sm-12">
                    <input type="number" name="jumlah_satker_dibawahnya" required class="form-control" placeholder="Jumlah Satker Dibawahnya">
                    <br>
                </div>
                <div class="col-sm-12 hidden">
                    <input type="text" name="ket" class="form-control" placeholder="Keterangan Satker">
                    <br>
                </div>
                <div class="col-sm-12 right" style="padding-top: 1em">
                    <button type="button" onclick="Custombox.close();" class="btn btn-default">BATAL</button>
                    <button type="submit" class="btn btn-inverse">SIMPAN</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $.fn.editable.defaults.mode = 'popup';
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
    $(function(){
        <?php
        if(!isset($access['u'])){
        ?>
        $(".emjes-editable").editable('option', 'disabled', true);
        <?php
        }
        ?>
    });
</script>
