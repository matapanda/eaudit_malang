<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-12">
                <form role="form" method="get" action="?cek_periode2=true">
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-1 form-control-label" style="padding-top: 10px">Periode</label>
                        <div class="col-sm-4">
                            <select name="periode" onchange="this.form.submit()" required class="select2 form-control select2-multiple" data-placeholder="Periode">
                                <?php
                                foreach ($periode as $r){
                                    echo "<option value='$r[id]' ".(@$per==$r['id']?'selected':'').">$r[nama]</option>";
                                }
                                ?>
                            </select>
                            <input type="hidden" name="<?=$tipe?>" value="<?=@$id_satker?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-1 form-control-label" style="padding-top: 10px">Pilih Satker</label>
                        <div class="col-sm-4">
                            <select required name="id_h" class="select2 form-control" >
                                <option value="">Pilih Satker</option>
                                <?php
                                foreach ($satker as $r){
                                    echo "<option value='$r[id]' ".(@$id_satker==$r['id']?'selected':'').">$r[ket]</option>";
                                }
                                ?>
                            </select>
                            <input type="hidden" name="periode_h" value="<?=@$per?>">
                            <input type="hidden" name="satker_h" value="<?=@$id_satker?>">
                        </div>
                    </div>
                </form>
                <form role="form" method="post" action="?fk=true">
                    <input type="hidden" name="satker" value="<?=@$id_satker?>">
                    <input type="hidden" name="periode_h" value="<?=@$per?>">
                    <div class="form-group">
                        <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <td class="center" colspan="3"><b>KEPEMIMPINAN</b></td>
                            </tr>
                            <tr>
                                <td class="center">Rapat Pimpinan</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Apakah Rapat Komite Manajemen Risiko dilaksanakan</td>
                                <td class="center"><input type="radio" name="lp1" onclick="cek_lp()" <?=@$edit['lp1']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp1" <?=@$edit['lp1']==0?'checked':''?> onclick="cek_lp()" value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Rapat Komite Manajemen Risiko dipimpin oleh pimpinan tertinggi</td>
                                <td class="center"><input type="radio" name="lp2" onclick="cek_lp()" <?=@$edit['lp2']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp2" onclick="cek_lp()" <?=@$edit['lp2']!=0?'checked':''?> value="-20">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Rapat Komite Manajemen Risiko menghasilkan keputusan  dan Selera risiko sudah ditetapkan</td>
                                <td class="center"><input type="radio" name="lp3" onclick="cek_lp()" <?=@$edit['lp3']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp3" onclick="cek_lp()" <?=@$edit['lp3']!=0?'checked':''?> value="-5">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Rapat Komite Manajemen RisikoDihadiri minimal 2/3 anggota</td>
                                <td class="center"><input type="radio" name="lp4" onclick="cek_lp()" <?=@$edit['lp4']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp4" onclick="cek_lp()" <?=@$edit['lp4']!=0?'checked':''?> value="-20">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Rapat Komite Pelaksana Manajemen Risiko dilaksanakan</td>
                                <td class="center"><input type="radio" name="lp5" onclick="cek_lp()" <?=@$edit['lp5']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp5" <?=@$edit['lp5']==0?'checked':''?> onclick="cek_lp()" value="0">Tidak </td>
                            </tr>
<!--                            <tr>-->
<!--                                <td>Apakah Rapat Komite Pelaksana Manajemen Risiko dilaksanakan</td>-->
<!--                                <td class="center"><input type="radio" name="lp5" onclick="cek_lp()" --><?//=@$edit['lp5']==100?'checked':''?><!-- value="100">Ya </td><td class="center"><input type="radio" name="lp5" onclick="cek_lp()" --><?//=@$edit['lp5']==0?'checked':''?><!--  value="0">Tidak </td>-->
<!--                            </tr>-->
                            <tr>
                                <td>Apakah Rapat Komite Pelaksana Manajemen Risiko dipimpin oleh pimpinan tertinggi</td>
                                <td class="center"><input type="radio" name="lp6" onclick="cek_lp()" <?=@$edit['lp6']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp6" onclick="cek_lp()" <?=@$edit['lp6']!=0?'checked':''?>  value="-25">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Rapat Komite Pelaksana Manajemen Risiko dihadiri minimal 2/3 anggota</td>
                                <td class="center"><input type="radio" name="lp7" onclick="cek_lp()" <?=@$edit['lp7']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp7" onclick="cek_lp()" <?=@$edit['lp7']!=0?'checked':''?>  value="-25">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Rapat Manajemen Risiko OPD dilaksanakan</td>
                                <td class="center"><input type="radio" name="lp8" onclick="cek_lp()" <?=@$edit['lp8']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp8" onclick="cek_lp()" <?=@$edit['lp8']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Rapat Manajemen Risiko OPD dipimpin oleh pimpinan tertinggi</td>
                                <td class="center"><input type="radio" name="lp9" onclick="cek_lp()" <?=@$edit['lp9']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp9" onclick="cek_lp()" <?=@$edit['lp9']!=0?'checked':''?>  value="-25">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Rapat Manajemen Risiko OPD dihadiri seluruh unit organisasi</td>
                                <td class="center"><input type="radio" name="lp10" onclick="cek_lp()" <?=@$edit['lp10']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp10" onclick="cek_lp()" <?=@$edit['lp10']!=0?'checked':''?>  value="-25">Tidak </td>
                            </tr>

                            <tr>
                                <td class="center">Dukungan sumber daya</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>

                            <tr>
                                <td>Apakah Dana Implementasi MR dianggarkan</td>
                                <td class="center"><input type="radio" name="lp11" onclick="cek_lp()" <?=@$edit['lp11']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp11" onclick="cek_lp()" <?=@$edit['lp11']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Pelaksanaan MR dan mitigasi tidak terkendala dana</td>
                                <td class="center"><input type="radio" name="lp12" onclick="cek_lp()" <?=@$edit['lp12']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp12" onclick="cek_lp()" <?=@$edit['lp12']!=0?'checked':''?>  value="-50">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah seluruh pengelola Manajemen Risiko (Pemilik risiko, Komite MR, Komite Pelaksana) telah mengikuti pelatihan MR</td>
                                <td class="center"><input type="radio" name="lp13" onclick="cek_lp()" <?=@$edit['lp13']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp13" onclick="cek_lp()" <?=@$edit['lp13']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah In house training MR dilaksanakan dalam periode berjalan</td>
                                <td class="center"><input type="radio" name="lp14" onclick="cek_lp()" <?=@$edit['lp14']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp14" onclick="cek_lp()" <?=@$edit['lp14']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah seluruh perwakilan unit dibawah OPD terwakili</td>
                                <td class="center"><input type="radio" name="lp15" onclick="cek_lp()" <?=@$edit['lp14']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp15" onclick="cek_lp()" <?=@$edit['lp15']!=0?'checked':''?>  value="-25">Tidak </td>
                            </tr>
                            <tr>
                                <td class="center">Dukungan perangkat penerapan</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Apakah terdapat prinsip, strategi, kerangka, kebijakan</td>
                                <td class="center"><input type="radio" name="lp16" onclick="cek_lp()" <?=@$edit['lp16']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp16" onclick="cek_lp()" <?=@$edit['lp16']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>

                            <tr>
                                <td>Apakah terdapat pengembangan prinsip, strategi, kerangka dalam produk hukum</td>
                                <td class="center"><input type="radio" name="lp17" onclick="cek_lp()" <?=@$edit['lp17']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp17" onclick="cek_lp()" <?=@$edit['lp17']!=0?'checked':''?>  value="-50">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah terdapat organisasi prosedur dan tata kerja</td>
                                <td class="center"><input type="radio" name="lp18" onclick="cek_lp()" <?=@$edit['lp18']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp18" onclick="cek_lp()" <?=@$edit['lp18']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Strukur komite MR ditetapkan secara resmi (SK/ ND)</td>
                                <td class="center"><input type="radio" name="lp19" onclick="cek_lp()" <?=@$edit['lp19']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp19" onclick="cek_lp()" <?=@$edit['lp19']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Susunan keanggotaan MR sesuai aturan</td>
                                <td class="center"><input type="radio" name="lp20" onclick="cek_lp()" <?=@$edit['lp20']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp20" onclick="cek_lp()" <?=@$edit['lp20']!=0?'checked':''?>  value="-15">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Pengambilan keputusan telah mempertimbangkan risiko</td>
                                <td class="center"><input type="radio" name="lp21" onclick="cek_lp()" <?=@$edit['lp21']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp21" onclick="cek_lp()" <?=@$edit['lp21']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Keputusan dibuat pada periode penilaian</td>
                                <td class="center"><input type="radio" name="lp22" onclick="cek_lp()" <?=@$edit['lp22']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp22" onclick="cek_lp()" <?=@$edit['lp22']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Ada rencana kontinjensi atau recovery plan</td>
                                <td class="center"><input type="radio" name="lp23" onclick="cek_lp()" <?=@$edit['lp23']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp23" onclick="cek_lp()" <?=@$edit['lp23']!=0?'checked':''?>  value="-15">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah ada dokumentasi MR</td>
                                <td class="center"><input type="radio" name="lp24" onclick="cek_lp()" <?=@$edit['lp24']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp24" onclick="cek_lp()" <?=@$edit['lp24']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah dokumentasi MR menggunakan formulir yang seragam</td>
                                <td class="center"><input type="radio" name="lp25" onclick="cek_lp()" <?=@$edit['lp25']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp25" onclick="cek_lp()" <?=@$edit['lp25']!=0?'checked':''?>  value="-20">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah ada basis data risk assesment</td>
                                <td class="center"><input type="radio" name="lp26" onclick="cek_lp()" <?=@$edit['lp26']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp26" onclick="cek_lp()" <?=@$edit['lp26']!=0?'checked':''?>  value="-30">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah ada loss event data base</td>
                                <td class="center"><input type="radio" name="lp27" onclick="cek_lp()" <?=@$edit['lp27']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp27" onclick="cek_lp()" <?=@$edit['lp27']!=0?'checked':''?>  value="-20">Tidak </td>
                            </tr>

                            <tr>
                                <td class="center" colspan="3"><b>PROSES MANAJEMEN RISIKO</b></td>
                            </tr>
                            <tr>
                                <td class="center">Penetapan konteks</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Apakah proses Penetapan konteks dijalankan</td>
                                <td class="center"><input type="radio" name="lp28" onclick="cek_lp()" <?=@$edit['lp28']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp28" onclick="cek_lp()" <?=@$edit['lp28']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Seluruh sasaran strategis UPR telah masuk dalam lingkup MR</td>
                                <td class="center"><input type="radio" name="lp29" onclick="cek_lp()" <?=@$edit['lp29']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp29" onclick="cek_lp()" <?=@$edit['lp29']!=0?'checked':''?>  value="-20">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Komposisi tim telah mewakili seluruh unit organisasi dibawah OPD</td>
                                <td class="center"><input type="radio" name="lp30" onclick="cek_lp()" <?=@$edit['lp30']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp30" onclick="cek_lp()" <?=@$edit['lp30']!=0?'checked':''?>  value="-20">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Penetapan profile risiko dilakukan tepat waktu</td>
                                <td class="center"><input type="radio" name="lp31" onclick="cek_lp()" <?=@$edit['lp31']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp31" onclick="cek_lp()" <?=@$edit['lp31']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Penetapan profile risiko dilakukan tepat waktu</td>
                                <td class="center"><input type="radio" name="lp32" onclick="cek_lp()" <?=@$edit['lp32']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp32" onclick="cek_lp()" <?=@$edit['lp32']!=0?'checked':''?>  value="-15">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Penetapan profile risiko dilakukan tepat waktu</td>
                                <td class="center"><input type="radio" name="lp33" onclick="cek_lp()" <?=@$edit['lp33']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp33" onclick="cek_lp()" <?=@$edit['lp33']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Penetapan profile risiko dilakukan tepat waktu</td>
                                <td class="center"><input type="radio" name="lp34" onclick="cek_lp()" <?=@$edit['lp34']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp34" onclick="cek_lp()" <?=@$edit['lp34']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>

                            <tr>
                                <td class="center">Identifikasi risiko</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>

                            <tr>
                                <td>Apakah Proses Identifikasi risiko dijalankan</td>
                                <td class="center"><input type="radio" name="lp35" onclick="cek_lp()" <?=@$edit['lp35']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp35" onclick="cek_lp()" <?=@$edit['lp35']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah telah Mengidentifikasi risiko seluruh sasaran strategis organisasi dalam penetapan konteks</td>
                                <td class="center"><input type="radio" name="lp36" onclick="cek_lp()" <?=@$edit['lp36']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp36" onclick="cek_lp()" <?=@$edit['lp36']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Pengembangan identifikasi risk dengan memperhatikan deskripsi SS dan diupayakan mencakup seluruh risk utamanya yg dihadapi OPD</td>
                                <td class="center"><input type="radio" name="lp37" onclick="cek_lp()" <?=@$edit['lp37']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp37" onclick="cek_lp()" <?=@$edit['lp37']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah telah dilakukan Pemeliharaan berkelanjutan risk periode sebelumnya kecuali terjadi perubahan organisasi tugas dan fungsi dan/atau SS</td>
                                <td class="center"><input type="radio" name="lp38" onclick="cek_lp()" <?=@$edit['lp38']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp38" onclick="cek_lp()" <?=@$edit['lp38']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah telah mengidentifikasi risiko seluruh kategori risk, kecuali risk tersebut tidak mungkin terjadi bagi OPD yang bersangkutan</td>
                                <td class="center"><input type="radio" name="lp39" onclick="cek_lp()" <?=@$edit['lp39']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp39" onclick="cek_lp()" <?=@$edit['lp39']!=0?'checked':''?>  value="-25">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Risiko, penyebab, konsekuensi, dan kategori risk telah dirumuskan dengan tepat</td>
                                <td class="center"><input type="radio" name="lp40" onclick="cek_lp()" <?=@$edit['lp40']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp40" onclick="cek_lp()" <?=@$edit['lp40']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Deskripsi konsekuensi telah memadai (dalam level OPD/mempengaruhi kinerja OPD)</td>
                                <td class="center"><input type="radio" name="lp41" onclick="cek_lp()" <?=@$edit['lp41']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp41" onclick="cek_lp()" <?=@$edit['lp41']!=0?'checked':''?>  value="-5">Tidak </td>
                            </tr>

                            <tr>
                                <td class="center">Analisis risiko</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>

                            <tr>
                                <td>Apakah Analisa risiko dijalankan</td>
                                <td class="center"><input type="radio" name="lp42" onclick="cek_lp()" <?=@$edit['lp42']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp42" onclick="cek_lp()" <?=@$edit['lp42']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td> Apakah Semua risk yang diidentifikasi telah dilakukan analisa</td>
                                <td class="center"><input type="radio" name="lp43" onclick="cek_lp()" <?=@$edit['lp43']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp43" onclick="cek_lp()" <?=@$edit['lp43']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Level risiko ditetapkan sesuai dengan matriks analisis risiko</td>
                                <td class="center"><input type="radio" name="lp44" onclick="cek_lp()" <?=@$edit['lp44']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp44" onclick="cek_lp()" <?=@$edit['lp44']!=0?'checked':''?>  value="-40">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah tren risiko telah dirumuskan secara tepat</td>
                                <td class="center"><input type="radio" name="lp45" onclick="cek_lp()" <?=@$edit['lp45']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp45" onclick="cek_lp()" <?=@$edit['lp45']!=0?'checked':''?>  value="-20">Tidak </td>
                            </tr>
                            <tr>
                                <td class="center">Evaluasi risiko</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Apakah Semua risiko yang dianalisis telah dilakukan evaluasi</td>
                                <td class="center"><input type="radio" name="lp46" onclick="cek_lp()" <?=@$edit['lp46']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp46" onclick="cek_lp()" <?=@$edit['lp46']!=0?'checked':''?>  value="-20">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Prioritas risiko ditetapkan sesuai kaidah</td>
                                <td class="center"><input type="radio" name="lp47" onclick="cek_lp()" <?=@$edit['lp47']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp47" onclick="cek_lp()" <?=@$edit['lp47']!=0?'checked':''?>  value="-50">Tidak </td>
                            </tr>
                            <tr>
                                <td class="center">Mitigasi risiko</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Apakah proses mitigasi risiko dijalankan</td>
                                <td class="center"><input type="radio" name="lp48" onclick="cek_lp()" <?=@$edit['lp48']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp48" onclick="cek_lp()" <?=@$edit['lp48']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah rencana mitigasi diurutkan sesuai urutan prioritas & ditujukan untuk risiko yang berada diluar selera risiko</td>
                                <td class="center"><input type="radio" name="lp49" onclick="cek_lp()" <?=@$edit['lp49']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp49" onclick="cek_lp()" <?=@$edit['lp49']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah opsi yang dipilih selaras dengan opsi mitigasi yang mungkin, level kemungkinan, level konsekuensi serta selera risiko, dan/atau kewenangan dan tanggung jawab OPD</td>
                                <td class="center"><input type="radio" name="lp50" onclick="cek_lp()" <?=@$edit['lp50']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp50" onclick="cek_lp()" <?=@$edit['lp50']!=0?'checked':''?>  value="-5">Tidak </td>
                            </tr>
                            <tr>
                                <td> Apakah rumusan rencana mitigasi sesuai dengan Opsi yang dipilih</td>
                                <td class="center"><input type="radio" name="lp51" onclick="cek_lp()" <?=@$edit['lp51']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp51" onclick="cek_lp()" <?=@$edit['lp51']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Rencana mitigasi merupakan inisiatif baru atau modifikasi atas pengendalian yang ada</td>
                                <td class="center"><input type="radio" name="lp52" onclick="cek_lp()" <?=@$edit['lp52']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp52" onclick="cek_lp()" <?=@$edit['lp52']!=0?'checked':''?>  value="-25">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Rencana mitgasi secara lengkap merumuskan ukuran kinerja, target kinerja, jadwal, risiko residual yang diharapkan, dan penanggungjawabnya</td>
                                <td class="center"><input type="radio" name="lp53" onclick="cek_lp()" <?=@$edit['lp53']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp53" onclick="cek_lp()" <?=@$edit['lp53']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah rumusan ukuran kinerja, target kinerja, jadwal, risikoresidual yg diharapkan, dan penanggungjawabnya</td>
                                <td class="center"><input type="radio" name="lp54" onclick="cek_lp()" <?=@$edit['lp54']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp54" onclick="cek_lp()" <?=@$edit['lp54']!=0?'checked':''?>  value="-15">Tidak </td>
                            </tr>
                            <tr>
                                <td class="center">Monitoring dan reviu</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>

                            <tr>
                                <td>Apakah Proses monitoring dan reviu dijalankan</td>
                                <td class="center"><input type="radio" name="lp55" onclick="cek_lp()" <?=@$edit['lp55']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp55" onclick="cek_lp()" <?=@$edit['lp55']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Seluruh risiko dilakukan monitoring</td>
                                <td class="center"><input type="radio" name="lp56" onclick="cek_lp()" <?=@$edit['lp56']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp56" onclick="cek_lp()" <?=@$edit['lp56']!=0?'checked':''?>  value="-20">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Seluruh rencana penanganan dilakukan monitoring</td>
                                <td class="center"><input type="radio" name="lp57" onclick="cek_lp()" <?=@$edit['lp57']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp57" onclick="cek_lp()" <?=@$edit['lp57']!=0?'checked':''?>  value="-20">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Penetapan form monitoring penanganan dilakukan tepat waktu/akhir periode MR</td>
                                <td class="center"><input type="radio" name="lp58" onclick="cek_lp()" <?=@$edit['lp58']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp58" onclick="cek_lp()" <?=@$edit['lp58']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Level risiko residual aktual, level risiko residual yang diharapkan, kesenjangan/deviasi dan tren risiko sudah dirumuskan dengan tepat</td>
                                <td class="center"><input type="radio" name="lp59" onclick="cek_lp()" <?=@$edit['lp59']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp59" onclick="cek_lp()" <?=@$edit['lp59']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Seluruh deviasi negatif dirumuskan langkah korektif</td>
                                <td class="center"><input type="radio" name="lp60" onclick="cek_lp()" <?=@$edit['lp60']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp60" onclick="cek_lp()" <?=@$edit['lp60']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td class="center">Komunikasi dan Konsultasi</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Apakah Proses komunikasi dan konsultasi MR dijalankan</td>
                                <td class="center"><input type="radio" name="lp61" onclick="cek_lp()" <?=@$edit['lp61']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp61" onclick="cek_lp()" <?=@$edit['lp61']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Prinsip, kerangka, strategi dan kebijakan MR, SOP, Organisasi dan Tata Kerja, serta dokumentasi standar dikomunikasikan ke internal organisasi</td>
                                <td class="center"><input type="radio" name="lp62" onclick="cek_lp()" <?=@$edit['lp62']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp62" onclick="cek_lp()" <?=@$edit['lp62']!=0?'checked':''?>  value="-10">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Profil risiko (PR) dan rencana penanganan (RP) semuanya dikomunikasikan ke ketua KMR dan pejabat/pegawai dibawah OPD</td>
                                <td class="center"><input type="radio" name="lp63" onclick="cek_lp()" <?=@$edit['lp63']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp63" onclick="cek_lp()" <?=@$edit['lp63']!=0?'checked':''?>  value="-35">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Laporan PR disampaiakn ke Ketua KMR</td>
                                <td class="center"><input type="radio" name="lp64" onclick="cek_lp()" <?=@$edit['lp64']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp61" onclick="cek_lp()" <?=@$edit['lp64']!=0?'checked':''?>  value="-25">Tidak </td>
                            </tr>

                            <tr>
                                <td class="center" colspan="3"><b>AKTIVITAS MITIGASI RISIKO</b></td>
                            </tr>
                            <tr>
                                <td class="center">Mitigasi risiko yang dijalankan</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Apakah mitigasi risiko yang dijalankan telah didukung dokumen yang memadai</td>
                                <td class="center"><input type="radio" name="lp65" onclick="cek_lp()" <?=@$edit['lp65']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp65" onclick="cek_lp()" <?=@$edit['lp65']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Penanganan risiko yang dijalankan merupakan inovasi baru/modifikasi sistem pengendalian yang ada</td>
                                <td class="center"><input type="radio" name="lp66" onclick="cek_lp()" <?=@$edit['lp66']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp66" onclick="cek_lp()" <?=@$edit['lp66']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Penanganan risiko yang dijalankan selaras dengan opsi dan apa yang mungkin terjadi, penyebab, dan atau dampak risiko</td>
                                <td class="center"><input type="radio" name="lp67" onclick="cek_lp()" <?=@$edit['lp67']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp67" onclick="cek_lp()" <?=@$edit['lp67']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah sudah  jelas ukuran kinerjanya dan terukur target penanganannya serta sesuai dengan target dan ukuran kinerjaa</td>
                                <td class="center"><input type="radio" name="lp68" onclick="cek_lp()" <?=@$edit['lp68']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp68" onclick="cek_lp()" <?=@$edit['lp68']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>

                            <tr>
                                <td class="center" colspan="3"><b>HASIL PENERAPAN MANAJEMEN RISIKO</b></td>
                            </tr>
                            <tr>
                                <td class="center">Pencapaian kinerja UPR</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Apakah Dokumen penilaian kinerja dalam dokumen perencanaan telah ditetapkan dan dilaporkan</td>
                                <td class="center"><input type="radio" name="lp69" onclick="cek_lp()" <?=@$edit['lp69']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp69" onclick="cek_lp()" <?=@$edit['lp69']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Semua kejadian berdampak negatif tinggi telah dikelola risikonya atau telah diidentifikasi dalam profil risiko OPD</td>
                                <td class="center"><input type="radio" name="lp70" onclick="cek_lp()" <?=@$edit['lp70']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp70" onclick="cek_lp()" <?=@$edit['lp70']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td>Apakah Seluruh IKU pada SS OPD telah mencapai target</td>
                                <td class="center"><input type="radio" name="lp71" onclick="cek_lp()" <?=@$edit['lp71']==100?'checked':''?> value="100">Ya </td><td class="center"><input type="radio" name="lp71" onclick="cek_lp()" <?=@$edit['lp71']==0?'checked':''?>  value="0">Tidak </td>
                            </tr>
                            <tr>
                                <td class="center"><b>Total</b></td>
                                <td class="center" colspan="2"><label class="tot_lp"><b><?=@$edit['tot']?$edit['tot']:'0'?> </b></label><input type="hidden" name="tot_lp" value="<?=@$edit['tot_lp']?$edit['tot_lp']:'0'?>"></td>
                            </tr>

                        </table>
                    </div>

                    <!--                    --><?//=json_encode($edit)?>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    function cek_lp(){
        var lp=$('input[name=lp1]:checked').val();
        var lp3=$('input[name=lp3]:checked').val();
        var lp4=$('input[name=lp4]:checked').val();
        var lp5=$('input[name=lp5]:checked').val();
        var lp6=$('input[name=lp6]:checked').val();
        var lp7=$('input[name=lp7]:checked').val();
        var lp8=$('input[name=lp8]:checked').val();
        var lp9=$('input[name=lp9]:checked').val();
        var lp10=$('input[name=lp10]:checked').val();
        var lp11=$('input[name=lp11]:checked').val();
        var lp12=$('input[name=lp12]:checked').val();
        var lp13=$('input[name=lp13]:checked').val();
        var lp14=$('input[name=lp14]:checked').val();
        var lp15=$('input[name=lp15]:checked').val();
        var lp16=$('input[name=lp16]:checked').val();
        var lp17=$('input[name=lp17]:checked').val();
        var lp18=$('input[name=lp18]:checked').val();
        var lp19=$('input[name=lp19]:checked').val();
        var lp20=$('input[name=lp20]:checked').val();
        var i;

        var tot=0;
        for (i = 1; i < 70; i++) {
            console.log($('input[name=lp'+i+']:checked').val());
            tot +=parseFloat($('input[name=lp'+i+']:checked').val());
        }
        // var tot=parseFloat(lp)+parseFloat(lp2)+parseFloat(lp3)+parseFloat(lp4)+parseFloat(lp5);
        $('input[name=tot_lp]').val(tot);
        $('.tot_lp').html('<b>'+tot+'</b>');
    }
    $(function () {
        $('.select2').select2();
        $('form').parsley();
        $("form").submit(function(event) {
            $(this).parsley().validate();
            if (!$(this).parsley().isValid()){
                event.preventDefault();
            }else if($('#hori-pass1').val()!=$('#hori-pass2').val()){
                event.preventDefault();
                $('#hori-pass2').focus();
            }
        });

    });
</script>