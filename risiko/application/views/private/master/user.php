<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <a href="?i=true" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> ADD NEW USER</a>
            <a href="?" class="btn btn-primary"><i class="fa fa-refresh"></i> RELOAD</a>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-2">Username</th>
                        <th class="col-xs-3">Name</th>
                        <th class="center col-xs-2">E-Mail</th>
                        <th class="center col-xs-2">Role</th>
                         <th class="center <?=is_authority(@$access['u'])?> col-xs-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($user as $u) {
                        ?>
                        <tr>
                            <td class="center"><?=$u['username']?></td>
                            <td><?=$u['name']?></td>
                            <td class="center"><?=$u['email']?></td>
                            <td class=""><?php
                                $role=explode(";",$u['role']);
                                echo "<ol>";
                                foreach ($role as $r){
                                    if(strlen($r)>0){
                                        echo "<li>$r</li>";
                                    }
                                }
                                echo "</ol>";
                                ?></td>
                             <td class="<?=is_authority(@$access['u'])?> center"><a href="?u=<?=$u['id']?>" class="btn btn-xs btn-block btn-inverse">UPDATE DATA</a></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>