<ul>
    <li><a href="<?=base_url('welcome')?>" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span> Dashboard </span></a></li>
    <?php
    if(isset($role['f10293a6-79c7-41e2-840e-a597e5ace426']['r'])){
    ?>
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-archive"></i> <span> Master</span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <?php
            if(isset($role['340671b5-d101-46ec-b86f-6dc06e9d5668']['r'])){
                ?>
                <li><a href="<?=base_url('master/tipe_r')?>"><span>Data Tipe Resiko</span></a></li>
                <?php
            }
            if(isset($role['8a1dfd71-6c85-412f-91de-970ca913dc60']['r'])){
                ?>
                <li><a href="<?=base_url('master/satker')?>"><span>Data Satker</span></a></li>
                <?php
            }$menuid='92559c74-927e-4edf-85c8-203447141840';
            if(isset($role[$menuid]['u'])){
                ?>
                <li><a href="<?=base_url('master/group')?>">Data Role</a></li>
                <?php
            }
            $menuid='d66ab356-6792-451d-8f42-84f07e48b9e7';
            if(isset($role[$menuid]['r'])){
                ?>
                <li><a href="<?=base_url('master/user')?>">Data User</a></li>
                <?php
            }
            $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e743';
            if(isset($role[$menuid]['r'])){
                ?>
                <li><a href="<?=base_url('master/kegiatan')?>">Data Kegiatan</a></li>
                <?php
            }
            $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e743';
            if(isset($role[$menuid]['r'])){
                ?>
                <li><a href="<?=base_url('master/program')?>">Data Program</a></li>
                <?php
            }
            $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e743';
            if(isset($role[$menuid]['r'])){
                ?>
                <li><a href="<?=base_url('master/kejadian')?>">Data Kejadian</a></li>
                <?php
            }
            $menuid='23ca0398-00b3-4547-9312-63633c0e4a19';
            if(isset($role[$menuid]['r'])){
                ?>
                <li><a href="<?=base_url('master/opsi')?>">Data Opsi</a></li>
                <?php
            }
            $menuid='23ca0398-00b3-4547-9312-63633c0e4a10';
            if(isset($role[$menuid]['r'])){
                ?>
                <li><a href="<?=base_url('master/tk')?>">Rencana Tindak Terkait</a></li>
                <?php
            }
            $menuid='23ca0398-00b3-4547-9312-63633c0e4b29';
            if(isset($role[$menuid]['r'])){
                ?>
                <li><a href="<?=base_url('master/target')?>">Data Target Penurunan</a></li>
                <?php
            }
            $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e734';
            if(isset($role[$menuid]['r'])){
                ?>
                <li><a href="<?=base_url('master/program')?>">Data Program</a></li>
                <?php
            }?>

        </ul>
    </li>
    <?php
    }
    if(isset($role['923143fd-ca98-489d-88d7-642d2001b6ef']['r'])){
        ?>
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-account-box-outline"></i> <span> Manajemen Resiko</span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled"><?php
                $menuid='388deab8-36ff-4a8b-a1d7-b88eab8b4c26';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('resiko/program')?>">Program Satker</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e732';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('resiko/kegiatan')?>">Kegiatan Satker</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e7ab';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('resiko/opsi')?>">Opsi & Mitigasi</a></li>
                    <?php
                }
                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e7ac';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('resiko/monitoring')?>">Monitoring</a></li>
                    <?php
                }
                //                $menuid='0d59e730-4d54-4ef2-a892-2bdb88f4e7ac';
                //                if(isset($role[$menuid]['r'])){
                //                    ?>
                <!--                    <li><a href="--><?//=base_url('resiko/Mitigasi')?><!--">Mitigasi</a></li>-->
                <!--                    --><?php
                //                }
                $menuid='cb7ac7ac-9d00-4acd-a1fd-2afefb0c8a81';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('resiko/kejadian')?>">Kejadian Satker</a></li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    }
    if(isset($role['e964618e-29bd-4e17-91f2-1f42c0fa0488']['r'])){
        ?>
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-blinds"></i> <span> Laporan</span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <?php
                if(isset($role['8a1dfd71-6c85-412f-91de-970ca913dc60']['r'])){
                    ?>
                    <li><a href="<?=base_url('laporan/satker')?>"><span>Laporan Gabungan Resiko Per Satker</span></a></li>
                    <?php
                }
                if(isset($role['93391995-8d3d-447c-a50f-1cec916d3c27']['r'])){
                    ?>
                    <li><a href="<?=base_url('laporan/kejadian')?>"><span>Laporan Kejadian Satker</span></a></li>
                    <?php
                }
                if(isset($role['93391995-8d3d-447c-a50f-1cec916d3c27']['r'])){
                    ?>
                    <li><a href="<?=base_url('laporan/s_keg')?>"><span>Laporan Kegiatan Per Satker</span></a></li>
                    <?php
                }
                if(isset($role['e7dadcd0-c8ce-41e1-af4b-4fdc566a8262']['r'])){
                    ?>
                    <li><a href="<?=base_url('laporan/skala_resiko')?>"><span>Laporan Per Tipe Resiko</span></a></li>
                    <?php
                }
                if(isset($role['93391995-8d3d-447c-a50f-1cec916d3c27']['r'])){
                    ?>
                    <li><a href="<?=base_url('laporan/pr_p')?>"><span>Laporan Per Resiko & Kegiatan</span></a></li>
                    <?php
                }
                if(isset($role['cb7ac7ac-9d00-4acd-a1fd-2afefb0c8a81']['r'])){
                    ?>
                    <li><a href="<?=base_url('laporan/risiko_p')?>"><span>Laporan Resiko Per Satker</span></a></li>
                    <?php
                }$menuid='e964618e-29bd-4e17-91f2-1f42c0fa0428';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('laporan/lap_rgs')?>">Laporan Resiko Gabungan Per Satker</a></li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    }
    ?>
</ul>
