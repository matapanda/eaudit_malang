<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?=$config['d']?>">
    <meta name="author" content="<?=$config['a']?>">
    <title><?=$config['company']?></title>
    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <script src="<?=base_url()?>assets/js/modernizr.min.js"></script>
    <script src="<?=base_url()?>assets/js/jquery.min.js"></script>
</head>
<style>

    body{/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#00bc09+0,016111+100 */
        background: #353abc; /* Old browsers */
        background: -moz-radial-gradient(center, ellipse cover, #353abc 0%, #354961 100%)!important; /* FF3.6-15 */
        background: -webkit-radial-gradient(center, ellipse cover, #353abc 0%,#354961 100%)!important;; /* Chrome10-25,Safari5.1-6 */
        background: radial-gradient(ellipse at center, #353abc 0%,#354961 100%)!important;; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00bc09', endColorstr='#016111',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
    }
</style>
<body>
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="spinner-wrapper">
                <div class="rotator">
                    <div class="inner-spin"></div>
                    <div class="inner-spin"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<section><?php $this->load->view('public/'.$content['view'],@$content['data'])?></section>
<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(window).load(function() {
        $("#preloader").hide();
    });
</script>
</body>
</html>
