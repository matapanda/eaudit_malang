<div class="row">
    <div class="col-sm-12">
        <form method="post" action="?lhp=true">
        <div class="card-box">
            <div class="table-responsive">
                <table class="table">

                    <tr>
                        <td>1</td>
                        <td>Nomor SPT</td>
                        <td><?= @$laporan['spt_no'] ?> </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Tanggal SPT</td>
                        <td><?= format_tanggal(@$laporan['spt_date']) ?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Judul Laporan</td>
                        <td>
                           <input type="hidden" name="id_rencana" value="<?= @$laporan['id'] ?>">
                           <input type="text" name="jdl" class="form-control" value="<?= @$laporan2['jdl'] ?>">
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Nomor Laporan</td>
                       <td>
                           <input type="text" class="form-control" name="no" value="<?= @$laporan2['no'] ?>">
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Tgl Laporan</td>
                       <td>
                            <input type="text" autocomplete="off" class="form-control" id="date-range" name="tgl" value="<?=@$laporan2['tgl']?>">
                        </td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>6</td>
                        <td>Lampiran</td>
                        <td>
                            <input type="text" class="form-control" name="lampiran" value="<?= @$laporan2['lampiran'] ?>">
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Hal</td>
                        <td>
                            <input type="text" class="form-control" name="hal" value="<?= @$laporan2['hal'] ?>">
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Ditujukan kepada</td>
                        <td>
                            <input type="text" class="form-control" name="ditujukan" value="<?= @$laporan2['ditujukan'] ?>">
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>A. </td>
                        <td>Umum</td>
                        <td></td>
                        <td></td>
                    </tr>
                        <tr>
                            <td class="center"></td>
                            <td class="center">1 .</td>
                            <td>Dasar Penugasan</td>
                            <td>
                                <textarea class="form-control" name="dasar" >
                                    <?= @$laporan2['dasar'] ?>
                                </textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="center"></td>
                            <td class="center">2 .</td>
                            <td>Tujuan</td>
                            <td>
                                <textarea class="form-control" name="tujuan" >
                                    <?= @$laporan2['tujuan'] ?>
                                </textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="center"></td>
                            <td class="center">3 .</td>
                            <td>Sasaran</td>
                            <td>
                                <textarea class="form-control" name="sasaran" >
                                    <?= @$laporan2['sasaran'] ?>
                                </textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="center"></td>
                            <td class="center">4 .</td>
                            <td>Ruang Lingkup</td>
                            <td>
                                <textarea class="form-control" name="ruang" >
                                    <?= @$laporan2['ruang'] ?>
                                </textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="center"></td>
                            <td class="center">5 .</td>
                            <td>Waktu Pelaksanaan</td>
                            <td>
                                <textarea class="form-control" name="waktu" >
                                    <?= @$laporan2['waktu'] ?>
                                </textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="center"></td>
                            <td class="center">6 .</td>
                            <td>Metodologi</td>
                            <td>
                                <textarea class="form-control" name="metodologi" >
                                    <?= @$laporan2['metodologi'] ?>
                                </textarea>
                            </td>
                        </tr>
                    <tr>
                        <td>B. </td>
                        <td>Data Objek Penugasaan</td>
                        <td></td>
                        <td></td>
                    </tr>
                        <tr>
                            <td class="center"></td>
                            <td class="center">1 .</td>
                            <td>Keuangan</td>
                            <td>
                                <textarea class="form-control" name="keuangan" >
                                    <?= @$laporan2['keuangan'] ?>
                                </textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="center"></td>
                            <td class="center">2 .</td>
                            <td>Struktur Organisasi</td>
                            <td>
                                <textarea class="form-control" name="organisasi" >
                                    <?= @$laporan2['organisasi'] ?>
                                </textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="center"></td>
                            <td class="center">3 .</td>
                            <td>Data Lainya</td>
                            <td>
                                <textarea class="form-control" name="lainya" >
                                    <?= @$laporan2['lainya'] ?>
                                </textarea>
                            </td>
                        </tr>

                    <tr>
                        <td>F. </td>
                        <td>Temuan </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php
                    $urutan=1;
                    foreach ($temuan as $t){
                        ?>
                        <tr>
                        <td class="center"></td>
                        <td colspan="3">
                        <table class="table">
                        <tr>
                            <td style="width: 100px" rowspan="8">Temuan</td>
                            <td style="width: 200px">Judul Temuan</td>
                            <td style="width: 1px">:</td>
                            <td><?= $t['judul_temuan'] ?></td>
                        </tr>
                        <tr>
                            <td>Kode Temuan</td>
                            <td>:</td>
                            <td><?= $t['kode_t'] ?> - <?= $t['temuan'] ?></td>
                        </tr>
                        <tr>
                            <td>Uraian Temuan</td>
                            <td>:</td>
                            <td><?= $t['uraian_temuan'].$kode_kk ?></td>
                        </tr>
                        <tr>
                            <td>Nilai Temuan</td>
                            <td>:</td>
                            <td><?= $t['nilai_temuan'] ?></td>
                        </tr>
                        <tr>
                            <td>Kondisi</td>
                            <td>:</td>
                            <td><?= $t['kondisi'] ?></td>
                        </tr>
                        <tr>
                            <td>Kriteria</td>
                            <td>:</td>
                            <td><?= $t['kriteria'] ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Rekomendasi Temuan</td>
                        </tr>
                        <?php
                        foreach ($tl_rekom as $t){
                            if($t['urutan']==$urutan){
                                ?>
                                <tr>
                                    <td colspan="3">
                                        <table class="table">
                                            <tr>
                                                <td class="col-xs-2">Kode Rekom</td>
                                                <td class="col-xs-1">:</td>
                                                <td class="col-xs-9"><?= $t['kode_rekoms'] ?> - <?= $t['nama_rekom'] ?></td>
                                            </tr>
                                            <tr>
                                                <td class="col-xs-2">Uraian Rekom</td>
                                                <td class="col-xs-1">:</td>
                                                <td class="col-xs-9"><?= $t['uraian_rekom'] ?></td>
                                            </tr>
                                            <tr>
                                                <td class="col-xs-2">No Tindak Lanjut</td>
                                                <td class="col-xs-1">:</td>
                                                <td class="col-xs-9"><?= $t['kode_tl'] ?></td>
                                            </tr>
                                            <tr>
                                                <td class="col-xs-2">Uraian Tindak Lanjut</td>
                                                <td class="col-xs-1">:</td>
                                                <td class="col-xs-9"><?= $t['uraian_tindakl'] ?></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <?php
                            }
                        }

                        foreach ($tl_penyebab as $t){
                            if($t['urutan']==$urutan){
                                ?>
                                <tr>
                                    <td></td>
                                    <td colspan="3" style="font-weight: bold">Penyebab</td>
                                </tr>
                                <tr>
                                    <td class="center"></td>
                                    <td colspan="3">
                                        <table class="table">
                                            <tr>
                                                <td class="col-xs-2">Kode Penyebab</td>
                                                <td class="col-xs-1">:</td>
                                                <td class="col-xs-9"><?= $t['kode'] ?> - <?= $t['nama'] ?></td>
                                            </tr>
                                            <tr>
                                                <td class="col-xs-2">Uraian Penyebab</td>
                                                <td class="col-xs-1">:</td>
                                                <td class="col-xs-9"><?= $t['uraian_penyebab'] ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                <?php
                            }
                        }
                        $urutan++;}
                    ?>
                    <tr>
                        <td>G. </td>
                        <td>Penutup : </td>
                        <td>  <textarea class="form-control" name="penutup" >
                                    <?= @$laporan2['penutup'] ?>
                                </textarea></td>
                        <td></td>
                    </tr>
                </table>
                <input type="submit" class="btn btn-info" value="Simpan & Print">
            </div>
        </div>
        </form>
    </div>
</div>

<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>

        $('#date-range').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
        });
</script>