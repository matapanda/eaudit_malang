<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
           <hr>
            <?php
            show_alert();
            ?>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-4" colspan="4">TEMUAN</th>
                        <th class="center col-xs-3" colspan="3">PENYEBAB</th>
                        <th class="center col-xs-3" colspan="3">AKIBAT</th>
                        <th class="center col-xs-4" colspan="4">REKOMENDASI</th>
                        <th class="center col-xs-4" colspan="4">TINDAK LANJUT</th>
                        <th class="center col-xs-1">Print</th>
                    </tr>
                    <tr>
                        <th class="center col-xs-1">NO</th>
                        <th class="center col-xs-1">URAIAN TEMUAN</th>
                        <th class="center col-xs-1">KODE</th>
                        <th class="center col-xs-1">NILAI</th>
                        <th class="center col-xs-1">NO</th>
                        <th class="center col-xs-1">URAIAN</th>
                        <th class="center col-xs-1">KODE</th>
                        <th class="center col-xs-1">NO</th>
                        <th class="center col-xs-1">URAIAN</th>
                        <th class="center col-xs-1">KODE</th>
                        <th class="center col-xs-1">NO</th>
                        <th class="center col-xs-1">URAIAN</th>
                        <th class="center col-xs-1">KODE</th>
                        <th class="center col-xs-1">NILAI</th>
                        <th class="center col-xs-1">NO</th>
                        <th class="center col-xs-1">URAIAN</th>
                        <th class="center col-xs-1">KODE</th>
                        <th class="center col-xs-1">NILAI</th>
                        <th class="center col-xs-1">Print</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $id='';
                    $urutan=1;
                    foreach($tl as $r) {
                        if($r['id']!=$id){
                            $id=$r['id'];
                            ?>
                            <tr class="data">
                                <td class="center"><?=$urutan?></td>
                                <td class="center"><?=@$r['uraian_temuan']?></td>
                                <td class="center"><?=@$r['kode']?></td>
                                <td class="center"><?=@$r['nilai_temuan']?></td>
                                <?php
                                    $this->db->where('tlr.urutan',$r['urutan']);
                                    $this->db->where('tlr.id',$r['id']);
                                    $this->db->join('interpro.tindak_lanjut_penyebab tlp', 'tlp.id=tlr.id', 'left');
                                    $this->db->join('master.penyebab p', '(p.kode=(tlp.kode_penyebab)::varchar)', 'left');
                                    $this->db->select('p.kode,p.nama,tlr.urutan,tlp.uraian_penyebab');
                                    $peny = $this->db->get('interpro.tindak_lanjutd tlr')->row_array();
                                    ?>
                                <td class="center"><?=$peny['urutan']?></td>
                                <td class="center"><?=@$peny['uraian_penyebab']?></td>
                                <td class="center"><?=@$peny['kode']?></td>
                                <?php
                                $this->db->where('tlr.urutan',$r['urutan']);
                                $this->db->where('tlr.id',$r['id']);
                                $this->db->join('interpro.tindak_lanjut_akibat tlp', 'tlp.id=tlr.id', 'left');
                                $this->db->join('master.akibat p', '(p.kode=(tlp.kode_akibat)::varchar)', 'left');
                                $this->db->select('p.kode,p.nama,tlr.urutan,tlp.uraian_akibat');
                                $akibat = $this->db->get('interpro.tindak_lanjutd tlr')->row_array();
                                    ?>
                                <td class="center"><?=$akibat['urutan']?></td>
                                <td class="center"><?=@$akibat['uraian_akibat']?></td>
                                <td class="center"><?=@$akibat['kode']?></td>
                                <?php
                                $this->db->where('tlr.urutan',$r['urutan']);
                                $this->db->where('tlr.id',$r['id']);
                                $this->db->join('support.kode_temuan kt', 'tlr.kode_rekom=kt.id', 'left');
                                $this->db->join('master.tindak_lanjut tl', 'tl.id=tlr.no_tl', 'left');
                                $this->db->select('(kt.k1||kt.k2||kt.k3||kt.k4) as kode, tlr.uraian_rekom,tlr.nilai_rekom,tlr.nilai_tl,tlr.uraian_tl,tl.kode as kode_tl');
                                $rek = $this->db->get('interpro.tindak_lanjut_rekom tlr')->row_array();

                                ?>
                                <td class="center"><?=$r['urutan']?></td>
                                <td class="center"><?=$rek['uraian_rekom']?></td>
                                <td class="center"><?=$rek['kode']?></td>
                                <td class="center"><?=$rek['nilai_rekom']?></td>
                                <td class="center"><?=$r['urutan']?></td>
                                <td class="center"><?=$rek['uraian_tl']?></td>
                                <td class="center"><?=$rek['kode_tl']?></td>
                                <td class="center"><?=$rek['nilai_tl']?></td>

                                <td><a href="?id=<?=$r['id']?>" class="btn btn-xs btn-danger" ><i class="fa fa-print"></i></a></td>

                            </tr>
                            <?php
                    }

                        $urutan++;
                    }
                        ?>

                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" enctype="multipart/form-data" onsubmit="$('#preloader').show();">
                    <input type="hidden" name="id" value="">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Upload Berkas<span class="text-danger">*</span>
                            <br>
                          </label>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-xs-12 list-upload">
                                    <input type="file" name="file" required>
                                </div>
                            </div>
                            <div class="row list-uploads">

                            </div>
                            <div class="row past-uploads"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <button type="button" data-dismiss="modal" class="btn btn-default waves-effect m-l-5">Batal</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    function laksanakan(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(1).text()+' '+$('tr#'+_id).children('td').eq(2).text();
        $('.modal-title',_modal).html(_title);
        $('[name=id]',_modal).val(_id);
        _modal.modal('show');
    }
    function hapus(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang terhapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{delete:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_r) {
        $.post('?',{ri:_i,rn:_r},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html("Detail");
            $('.modal-body','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>