<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <hr>
            <?php
            show_alert();
            ?>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-1">TGL SPT/TGL LHP</th>
                        <th class="center col-xs-3">Judul</th>
                        <th class="center col-xs-1">No LHP</th>
                        <th class="center col-xs-3">Uraian Temuan</th>
                        <th class="center col-xs-2">Rekomendasi</th>
                        <th class="center col-xs-2">Uraian Tindak Lanjut</th>
                        <th class="center col-xs-1">Nilai Rekomendasi</th>
                        <th class="center col-xs-1">Nilai Tindak Lanjut</th>
                        <th class="center col-xs-1">Print</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $id='';
                    $urutan=1;
                    foreach($tl as $r) {
                        if($r['tgl_lhp']!=''){
                            if($r['id']!=$id){
                                $id=$r['id'];
                                ?>
                                <tr class="data">
                                    <td class="center"><?=$urutan?></td>
                                    <td class="center"><?=format_waktu(@$r['tgl_lhp'])?></td>
                                    <td class="judul"><?=@$r['skpd']?></td>
                                    <td class="center"><?=@$r['no_lhp']?></td>
                                    <td class=""><?=$r['uraian_temuan']?></td>
                                    <td class=""><?php
                                        $this->db->where('tlr.urutan',$r['urutan']);
                                        $this->db->where('tlr.id',$r['id']);
                                        $this->db->select('string_agg(tlr.uraian_rekom, \';\') as rekom');
                                        $rekom = $this->db->get('interpro.tindak_lanjut_rekom tlr')->row_array();
                                        $role2=explode(";",$rekom['rekom']);
                                        echo "<ol>";
                                        foreach ($role2 as $g){
                                            if(strlen($g)>0){
                                                echo "<li>$g</li>";
                                            }
                                        }
                                        echo "</ol>";
                                        ?></td>
                                    <td class=""><?php
                                        $this->db->where('tlr.urutan',$r['urutan']);
                                        $this->db->where('tlr.id',$r['id']);
                                        $this->db->select('string_agg(tlr.uraian_tl, \';\') as uraian_tl');
                                        $tls = $this->db->get('interpro.tindak_lanjut_rekom tlr')->row_array();
                                        $role3=explode(";",$tls['uraian_tl']);
                                        echo "<ol>";
                                        foreach ($role3 as $g){
                                            if(strlen($g)>0){
                                                echo "<li>$g</li>";
                                            }
                                        }
                                        echo "</ol>";
                                        ?></td>
                                    <td class=""><?php
                                        $this->db->where('tlr.urutan',$r['urutan']);
                                        $this->db->where('tlr.id',$r['id']);
                                        $this->db->select('string_agg((tlr.nilai_rekom)::varchar, \';\') as rekom');
                                        $rekom3 = $this->db->get('interpro.tindak_lanjut_rekom tlr')->row_array();
                                        $role4=explode(";",$rekom3['rekom']);
                                        echo "<ul>";
                                        foreach ($role4 as $g){
                                            if(strlen($g)>0){
                                                echo "<li>".format_uang($g)."</li>";
                                            }
                                        }
                                        echo "</ul>";
                                        ?></td>
                                    <td class=""><?php
                                        $this->db->where('tlr.urutan',$r['urutan']);
                                        $this->db->where('tlr.id',$r['id']);
                                        $this->db->select('string_agg((tlr.nilai_tl)::varchar, \';\') as rekom');
                                        $rekom4 = $this->db->get('interpro.tindak_lanjut_rekom tlr')->row_array();
                                        $role5=explode(";",$rekom4['rekom']);
                                        echo "<ul>";
                                        foreach ($role5 as $g){
                                            if(strlen($g)>0){
                                                echo "<li>".format_uang($g)."</li>";
                                            }
                                        }
                                        echo "</ul>";
                                        ?></td>
                                    <td><a href="?id=<?=$r['id']?>" class="btn btn-xs btn-danger" ><i class="fa fa-print"></i></a></td>

                                </tr>
                                <?php
                            }else{
                                ?>
                                <tr class="data">
                                    <td class="center"></td>
                                    <td class="center"></td>
                                    <td class="judul"></td>
                                    <td class="center"></td>
                                    <td class=""><?=$r['uraian_temuan']?></td>
                                    <td class=""><?php
                                        $this->db->where('tlr.urutan',$r['urutan']);
                                        $this->db->where('tlr.id',$r['id']);
                                        $this->db->select('string_agg(tlr.uraian_rekom, \';\') as rekom');
                                        $rekom = $this->db->get('interpro.tindak_lanjut_rekom tlr')->row_array();
                                        $role2=explode(";",$rekom['rekom']);
                                        echo "<ol>";
                                        foreach ($role2 as $g){
                                            if(strlen($g)>0){
                                                echo "<li>$g</li>";
                                            }
                                        }
                                        echo "</ol>";
                                        ?></td>
                                    <td class=""><?php
                                        $this->db->where('tlr.urutan',$r['urutan']);
                                        $this->db->where('tlr.id',$r['id']);
                                        $this->db->select('string_agg(tlr.uraian_tl, \';\') as uraian_tl');
                                        $tls = $this->db->get('interpro.tindak_lanjut_rekom tlr')->row_array();
                                        $role3=explode(";",$tls['uraian_tl']);
                                        echo "<ol>";
                                        foreach ($role3 as $g){
                                            if(strlen($g)>0){
                                                echo "<li>$g</li>";
                                            }
                                        }
                                        echo "</ol>";
                                        ?></td>
                                    <td class=""><?php
                                        $this->db->where('tlr.urutan',$r['urutan']);
                                        $this->db->where('tlr.id',$r['id']);
                                        $this->db->select('string_agg((tlr.nilai_rekom)::varchar, \';\') as rekom');
                                        $rekom3 = $this->db->get('interpro.tindak_lanjut_rekom tlr')->row_array();
                                        $role4=explode(";",$rekom3['rekom']);
                                        echo "<ul>";
                                        foreach ($role4 as $g){
                                            if(strlen($g)>0){
                                                echo "<li>".format_uang($g)."</li>";
                                            }
                                        }
                                        echo "</ul>";
                                        ?></td>
                                    <td class=""><?php
                                        $this->db->where('tlr.urutan',$r['urutan']);
                                        $this->db->where('tlr.id',$r['id']);
                                        $this->db->select('string_agg((tlr.nilai_tl)::varchar, \';\') as rekom');
                                        $rekom4 = $this->db->get('interpro.tindak_lanjut_rekom tlr')->row_array();
                                        $role5=explode(";",$rekom4['rekom']);
                                        echo "<ul>";
                                        foreach ($role5 as $g){
                                            if(strlen($g)>0){
                                                echo "<li>".format_uang($g)."</li>";
                                            }
                                        }
                                        echo "</ul>";
                                        ?></td>
                                </tr>
                                <?php

                                $urutan++;
                            }
                        }

                    }
                    ?>

                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" enctype="multipart/form-data" onsubmit="$('#preloader').show();">
                    <input type="hidden" name="id" value="">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Upload Berkas<span class="text-danger">*</span>
                            <br>
                        </label>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-xs-12 list-upload">
                                    <input type="file" name="file" required>
                                </div>
                            </div>
                            <div class="row list-uploads">

                            </div>
                            <div class="row past-uploads"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <button type="button" data-dismiss="modal" class="btn btn-default waves-effect m-l-5">Batal</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    function laksanakan(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(1).text()+' '+$('tr#'+_id).children('td').eq(2).text();
        $('.modal-title',_modal).html(_title);
        $('[name=id]',_modal).val(_id);
        _modal.modal('show');
    }
    function hapus(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang terhapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{delete:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_r) {
        $.post('?',{ri:_i,rn:_r},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html("Detail");
            $('.modal-body','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>