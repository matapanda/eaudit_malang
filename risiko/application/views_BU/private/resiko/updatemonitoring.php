<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-sm-12">
                <form role="form" method="post" action="?m=true">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Periode<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <select class="select2 form-control select2-multiple" name="periode" disabled data-placeholder="Pilih Periode" required>
                                <option value="">Pilih Periode</option>
                                <?php
                                foreach ($periode as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['periode']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Nama Kegiatan<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-5    ">
                            <select class="select2 form-control select2-multiple" onchange="kegiat()" id="keg" name="nama" data-placeholder="Pilih Nama Kegiatan" disabled required>
                                <option value="">Pilih Nama Kegiatan</option>
                                <?php
                                foreach ($keg as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['kegiatan']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>

                        </div>
                        <input type="hidden" name="jml_program" required class="form-control" id="jml_program" value="<?=@$jml_kr?$jml_kr:1?>">

                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Satker<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <select class="select2 form-control select2-multiple" name="satker" disabled data-placeholder="Pilih Satker" required>
                                <option value="">Pilih Satker</option>
                                <?php
                                foreach ($satker as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['satker']==$v['id']?'selected':'' ?>><?= "$v[kode] - $v[ket]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Nama Program<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <select class="select2 form-control select2-multiple" name="program" disabled data-placeholder="Pilih Program Satker" required>
                                <option value="">Pilih Program Satker</option>
                                <?php
                                foreach ($program as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['program_satker']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Prosedur Audit<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <select class="select2 form-control select2-multiple" name="prosedur" data-placeholder="Pilih Prosedur" required>
                                <option value="">Pilih Prosedur</option>
                                <?php
                                foreach ($prosedur as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['prosedur_audit']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <?php
                    for($no=1;$no<=$jml_kr;$no++){
                        foreach($resiko_kr as $r){
                            ?>
                            <div class="col-md-12 tambahan">
                                <div class="form-group <?=$no?>">
                                    <h3>Monitoring Terhadap Resiko </h3> <br>

                                    <div class="form-group resiko<?=$no?>">

                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Resiko<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select class="select2 form-control select2-multiple" disabled name="resiko<?=$no?>" data-placeholder="Pilih Tipe Resiko" required>
                                                    <option value="">Pilih Tipe Resiko</option>
                                                    <?php
                                                    foreach ($tipe_resiko as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= @$r['resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Penyebab Resiko<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select class="select2 form-control select2-multiple" disabled name="p_resiko<?=$no?>" data-placeholder="Pilih Penyebab Resiko" required>
                                                    <option value="">Pilih Penyebab Resiko</option>
                                                    <?php
                                                    foreach ($pe_resiko as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= @$r['p_resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Dampak Resiko<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select class="select2 form-control select2-multiple" name="da_resiko<?=$no?>" disabled data-placeholder="Pilih Dampak Resiko" required>
                                                    <option value="">Pilih Dampak Resiko</option>
                                                    <?php
                                                    foreach ($da_resiko as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= @$r['da_resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select disabled class="select2 form-control select2-multiple" name="kendali_a<?=$no?>[]" multiple="multiple" multiple data-placeholder="Pilih Kendali Yang Ada" required>
                                                    <option value="">Pilih Kendali Yang Ada</option>
                                                    <?php
                                                    foreach ($kendali as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= in_array($v['id'], json_decode($r['kendali_a'], true)) ? 'selected' : '' ?> ><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Belum Ada<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select disabled class="select2 form-control select2-multiple" name="kendali_ba<?=$no?>[]" multiple="multiple" multiple data-placeholder="Pilih Kendali Yang Belum Ada" required>
                                                    <option value="">Pilih Kendali Yang Belum Ada</option>
                                                    <?php
                                                    foreach ($kendali as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= in_array($v['id'], json_decode($r['kendali_ba'], true)) ? 'selected' : '' ?> ><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Rencana Tindak Lanjut<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <input type="hidden" name="idrs<?=$no?>" value="<?=$r['id']?>">
                                                <input type="hidden" name="rs<?=$no?>" value="<?=$r['resiko']?>">
                                                <select disabled class="select2 form-control" name="tindak<?=$no?>" data-placeholder="Rencana Tindak Lanjut" required>
                                                    <option value="">Pilih Rencana Tindak Lanjut</option>
                                                    <?php
                                                    foreach ($tindak as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= @$r['tindak']==$v['id']? 'selected' : '' ?> ><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Opsi Yang Dilakukan<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select class="select2 form-control" name="opsi<?=$no?>" disabled data-placeholder="Opsi Yang Dilakukan" required>
                                                    <option value="">Pilih Opsi Yang Dilakukan</option>
                                                    <?php
                                                    foreach ($opsi as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= @$r['opsi']==$v['id']? 'selected' : '' ?> ><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Target Penurunan<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select class="select2 form-control" name="target<?=$no?>" disabled data-placeholder="Target Penurunan" required>
                                                    <option value="">Pilih Target Penurunan</option>
                                                    <?php
                                                    foreach ($target as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= @$r['target']==$v['id']? 'selected' : '' ?> ><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Target Waktu<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <input type="text" placeholder="DD/MM/YYYY" readonly class="form-control date-range" disabled=""  name="end<?=$no?>"  value="<?=format_waktu($r['target_t'])?>">

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Jumlah Kejadian<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <input type="text" placeholder="Jumlah Kejadian" class="form-control "  name="jml<?=$no?>"  value="<?=@$r['jml']?>">

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Realisasi Kejadian<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <input type="text" placeholder="Realisasi Kejadian" class="form-control "  name="real<?=$no?>"  value="<?=@$r['real']?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $no++;
                        }
                    }
                    ?>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">

    $('.date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    $('.select2').select2();
    function tambah_kendali(i){
        var no=i;
        $.post('?',{kendali:i},function (data,status) {
            $('.kendali'+i).append(data);
            $('.select2').select2();
        });
    }function tambah_resiko(i){
        var no=$('#jml_program').val();
        var akhir=parseFloat(no)+1;
        $.post('?',{resiko:akhir},function (data,status) {
            $('.tambahan').append(data);
            $('.select2').select2();
            $('#jml_program').val(akhir);
            $("input[name='resiko"+akhir+"']").focus();
        });
    }
</script>