<style>
    .fileinput {
        margin-bottom: 9px;
        display: inline-block;
    }

    .fileinput .thumbnail {
        overflow: hidden;
        display: inline-block;
        margin-bottom: 5px;
        vertical-align: middle;
        text-align: center;
    }

    .img-rounded, .img-thumbnail, .thumb-lg, .thumb-md, .thumb-sm, .thumbnail {
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.16), 0 2px 10px rgba(0, 0, 0, 0.12);
    }

    .thumbnail {
        display: block;
        padding: 4px;
        margin-bottom: 20px;
        line-height: 1.42857143;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        -webkit-transition: border .2s ease-in-out;
        -o-transition: border .2s ease-in-out;
        transition: border .2s ease-in-out;
    }

    * {
        outline: none !important;
    }

    .fileinput-exists .fileinput-new, .fileinput-new .fileinput-exists {
        display: none;
    }

    .fileinput .btn {
        vertical-align: middle;
    }

    .btn-file {
        overflow: hidden;
        position: relative;
        vertical-align: middle;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-6 col-md-offset-3">
                <form role="form" method="post">
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 form-control-label">Email<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="email" name="email" required parsley-type="email" class="form-control"
                                   id="inputEmail" placeholder="Email" value="<?= $user['email'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 form-control-label">Username</label>
                        <div class="col-sm-7">
                            <input type="text" disabled value="<?= $user['username'] ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass1" class="col-sm-4 form-control-label">Password</label>
                        <div class="col-sm-7">
                            <input id="hori-pass1" name="password" type="password" placeholder="Password"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Confirm Password</label>
                        <div class="col-sm-7">
                            <input data-parsley-equalto="#hori-pass1" name="konfirm" type="password"
                                   placeholder="Password" class="form-control" id="hori-pass2">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 form-control-label"> Tanda Tangan</label>
                        <div class="col-sm-8">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail">
                                    <img src="<?= imageExist(@$user['ttd']) ?>">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 200px; max-height: 150px;"></div>
                                <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new" onclick="$('[name=imagefile2]').trigger('click');">Select image</span>
                                <span class="fileinput-exists" onclick="$('[name=imagefile2]').trigger('click');">Change</span>
                                <input type="file" name="imagefile2" id="imagefile2" class="hidden"
                                       onchange="uploadimage2('<?= $user['id'] ?>')">
                            </span>
                                    <a href="#" class="btn btn-default fileinput-exists"
                                       data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 form-control-label"> Avatar</label>
                        <div class="col-sm-8">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail">
                                    <img src="<?= imageExist(@$user['avatar']) ?>">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 200px; max-height: 150px;"></div>
                                <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new" onclick="$('[name=imagefile]').trigger('click');">Select image</span><span
                                    class="fileinput-exists" onclick="$('[name=imagefile]').trigger('click');">Change</span>
                                <input type="file" name="imagefile" id="imagefile" class="hidden"
                                       onchange="uploadimage('<?= $user['id'] ?>')">
                            </span>
                                    <a href="#" class="btn btn-default fileinput-exists"
                                       data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script src="<?= base_url() ?>assets/js/fileupload.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('.select2').select2();
        $('form').parsley();
        $("form").submit(function (event) {
            $(this).parsley().validate();
            if (!$(this).parsley().isValid()) {
                event.preventDefault();
            } else if ($('#hori-pass1').val() != $('#hori-pass2').val()) {
                event.preventDefault();
                $('#hori-pass2').focus();
            }
        });

    });
    function uploadimage(id) {
        if ($('#imagefile').val() !== '') {
            $.ajaxFileUpload({
                url: '<?= base_url("profile/upload") ?>',
                secureuri: false,
                fileElementId: 'imagefile',
                dataType: 'json',
                data: {
                    'id': id,
                },
                success: function (data, status) {
                    if (data.status === 'success') {
                        statusupload = '<div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><strong>' + data.msg + '</strong></div>';
                    } else {
                        statusupload = '<div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><strong>' + data.msg + '</strong></div>';
                    }
                    $("#status-pesan").append(statusupload);
                }
            });
        }
    }
    function uploadimage2(id) {
        if ($('#imagefile2').val() !== '') {
            $.ajaxFileUpload({
                url: '<?= base_url("profile/upload2") ?>',
                secureuri: false,
                fileElementId: 'imagefile2',
                dataType: 'json',
                data: {
                    'id': id,
                },
                success: function (data, status) {
                    if (data.status === 'success') {
                        statusupload = '<div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><strong>' + data.msg + '</strong></div>';
                    } else {
                        statusupload = '<div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><strong>' + data.msg + '</strong></div>';
                    }
                    $("#status-pesan").append(statusupload);
                }
            });
        }
    }
</script>