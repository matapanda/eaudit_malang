<style>

</style>
<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-6 col-md-offset-3">
                  <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 form-control-label">Nama <Periode></Periode></label>
                        <div class="col-sm-7">
                            <input type="hidden" name="edit" id="edit" value="<?=$edit['id']?>">
                            <?=@$edit['nama']?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Periode</label>
                        <div class="col-sm-7">
                           <?=@$daterange?>
                         </div>
                    </div>
                    <div class="form-group row liburan">
                        </div>
                    </div>
            <div class="col-md-12">
                <div id="calendar"></div>
            </div> <!-- end col -->
           </div>
            </div>
        <div class="col-md-12">
        </div>
        </div>
    </div>
</div><!-- BEGIN MODAL -->
<div class="modal fade none-border" id="event-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Hari Libur</h4>
            </div>
            <div class="modal-body p-20">
                <form method="post" id="tambah_hari">
                <div class="form-group row">
                    <label for="inputName" class="col-sm-4 form-control-label">Keterangan Hari libur<span class="text-danger">*</span></label>
                    <div class="col-sm-7">
                        <input type="text" name="ket" id="ket" required class="form-control" value=""/>
                    </div>
                </div><div class="form-group row">
                    <label for="inputName" class="col-sm-4 form-control-label">Hari libur<span class="text-danger">*</span></label>
                    <div class="col-sm-7">
                        <input type="text" name="hari_t" id="hari_t" disabled readonly class="form-control" value=""/>
                            <input type="hidden" name="hari" id="h" readonly class="form-control" value=""/>
                    </div>
                </div>
                    <div class="col-sm-offset-4">
                    <button type="button" id="tambah" class="btn btn-success save-event waves-effect waves-light">Simpan</button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                    <button type="button" onclick="delete_event()" id="deletes" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
                    <input type="submit" id="submit_tambah" class="hidden">
            </div>
            </div>
            </form>
        </div>
    </div>
</div>

<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/fullcalendar/css/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
<!--<link href="--><?//=base_url()?><!--assets/css/fullcalendar.print.css" rel="stylesheet" type="text/css"/>-->
<script src="<?= base_url() ?>assets/plugins/moment/moment.js"></script>
<script src="<?= base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?= base_url() ?>assets/plugins/fullcalendar/js/fullcalendar.min.js"></script>
<!--<script src="--><?//= base_url() ?><!--assets/pages/jquery.fullcalendar.js"></script>-->
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    function delete_event(){
        var id= $('#edit').val();
        var k= $('#ket').val();
        var h= $('#h').val();
        $.post('?deletes=true', {id:id,ket: k,hari: h}, function (data, status) {
            $('#ket').val('');
            $('#h').val('');
            $('#event-modal').modal('hide');
            window.location.href='<?=base_url('master/periode?e=')?>'+id;
        });
    }
    $(function () {

        $('#deletes').hide();
        $('#calendar').fullCalendar({
            dayClick: function(date, jsEvent, view) {
                if(date.format("YYYY-MM-DD")>='<?=$starts?>' && date.format("YYYY-MM-DD")<='<?=$ends?>'){
            date_last_clicked = $(this);
            $(this).css('background-color', '#bed7f3');
//                alert('Clicked on: ' + date.format("DD/MM/YYYY"));
                $('#hari_t').val(date.format("DD/MM/YYYY"));
                $('#h').val(date.format("YYYY-MM-DD"));
                var edit=$('#edit').val();
                $.post('?cek=true', {edit:edit,hari: date.format("YYYY-MM-DD")}, function (data, status) {
                        var par=JSON.parse(data);
                    if(par!=null){
                    $('#h').val(par.holiday);
                    $('#ket').val(par.ket);
                    $('#deletes').show();
                $('#event-modal').modal('show');
                    }else{
                        $('#ket').val('');
                        $('#deletes').hide();
                $('#event-modal').modal('show');
                    }
                });
        }else{
                    alert('Periode ini hanya berlaku dari <?=format_waktu($starts)?> sampai <?=format_waktu($ends)?>');
                }},
            events: [
                <?php
                foreach($list_h as $c){
                echo "{ id: '".$c['id']."', start: '".$c['holiday']."', end: '".$c['holiday']."', title: '".$c['ket']."' },";
                }?>
            ]
        });
        $('#tambah').on('click',function(e){
            $('#submit_tambah').click();
        });
        $('#event-modal').on('hidden.bs.modal', function () {
            $('#ket').val('');
            $('#h').val('');
            $('.fc-day').css('background-color', '#fff');
        })
        $("#tambah_hari").submit(function(event) {
            var id= $('#edit').val();
            var k= $('#ket').val();
            var h= $('#h').val();
            $.post('?holidayadd=true', {id:id,ket: k,hari: h}, function (data, status) {
                $('#ket').val('');
                $('#h').val('');
                $('#event-modal').modal('hide');
            });
        });
        $('.select2').select2();
        $('form').parsley();
        $("form").submit(function(event) {
            $(this).parsley().validate();
            if (!$(this).parsley().isValid()){
                event.preventDefault();
            }else if($('#hori-pass1').val()!=$('#hori-pass2').val()){
                event.preventDefault();
                $('#hori-pass2').focus();
            }
        });
        $('input[name="daterange"]').daterangepicker({locale: {
            format: 'DD/MM/YYYY'
        },});
        $('input[name="hari_t"]').daterangepicker({locale: {
            format: 'DD/MM/YYYY'
        },
            singleDatePicker: true,});
        $('input[name="hari"]').daterangepicker({locale: {
            format: 'DD-MM-YYYY'
        },
            singleDatePicker: true,});
    });
</script>