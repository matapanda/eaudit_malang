<div ng-app="gradinSA" ng-controller="orderController" ng-init="init()">
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box row">
                <?php
                include VIEWPATH.'alert.php';
                ?>
                <form id="form-order" role="form" method="post" ng-submit="simpanorder()">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">No. OPB</label>
                            <div class="col-sm-7">
                                <h5 class="nomargin"><?=$header['nota']?></h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">Tanggal Order</label>
                            <div class="col-sm-7">
                                <h5 class="nomargin"><?=format_waktu($header['tanggal'])?></h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">Supplier</label>
                            <div class="col-sm-7">
                                <h5 class="nomargin"><?=$header['supplier']?></h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-4 form-control-label">Tanggal Penyelesaian<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <input type="hidden" name="id" value="<?=$header['id']?>">
                                    <input type="text" name="tanggal" required class="form-control datepicker" placeholder="tanggal" value="<?=date('d/m/Y')?>" readonly>
                                    <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Mengetahui</label>
                            <div class="col-sm-7">
                                <input type="hidden" name="request_by">
                                <input type="text" name="karyawan" class="form-control" placeholder="Masukkan NIK / Nama" style="height: 20px">
                                <textarea name="items[]" class="hidden" ng-repeat="(key, item) in items">{{item}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-8 col-sm-offset-4">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                        <div id="detailppb">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="center col-xs-1" style="vertical-align: inherit;">PPB</th>
                                    <th class="center col-xs-2" style="vertical-align: inherit;">Nama Jasa</th>
                                    <th class="center col-xs-1">Jumlah</br>Order</th>
                                    <th class="center col-xs-1">Diselesaikan</br>Sebelumnya</th>
                                    <th class="center col-xs-1">Diselesaikan</br>Sekarang</th>
                                    <th class="center col-xs-1">Menunggu</br>Penyelesaian</th>
                                    <th class="center col-xs-2" style="vertical-align: inherit;">Catatan</th>
                                    <th class="center col-xs-2" style="vertical-align: inherit;">Beban</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-class="item.sekarang?'hand-cursor':'hand-cursor btn-warning'" ng-repeat="(key, item) in items" ng-click="edit(key)">
                                    <td class="center">{{item.nota}}</td>
                                    <td>{{item.barangjasa}}</td>
                                    <td class="center">{{item.jumlah}}</td>
                                    <td class="center">{{item.diterima}}</td>
                                    <td class="center">{{item.sekarang}}</td>
                                    <td class="center">{{(item.jumlah*1)-(item.diterima*1)-(item.sekarang*1)}}</td>
                                    <td style="white-space: pre-wrap">{{item.note}}</td>
                                    <td style="white-space: pre-wrap">{{item.bebanket}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan FPJ</button>
                        <a href="?permintaan=true" class="btn btn-default waves-effect m-l-5">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="formedit" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <form ng-submit="simpanedit()">
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">Nama Jasa</label>
                            <div class="col-sm-7">
                                <input type="text" ng-model="editbarangjasa" class="form-control" readonly="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">Beban</label>
                            <div class="col-sm-7">
                                <select name="beban" ng-model="editbeban" class="select2 form-control" required="" ng-disabled="editexist">
                                    <option value=''>-</option>
                                    <?php
                                    $last=array();
                                    $last_id=array(
                                        1=>@$beban[0]['k1'],
                                        2=>@$beban[0]['k2'],
                                        3=>@$beban[0]['k3']
                                    );
                                    $group=0;
                                    foreach ($beban[1] as $r){
                                        if($r['level']==1){
                                            $last_id[1]=$r['k1'];
                                            $last[1]=$r['nama'];
                                            continue;
                                        }elseif($r['level']==2 && "$r[k1]"=="$last_id[1]"){
                                            $last_id[2]=$r['k2'];
                                            $last[2]=$r['nama'];
                                            continue;
                                        }elseif($r['level']==3 && "$r[k1]$r[k2]"=="$last_id[1]$last_id[2]"){
                                            $last_id[3]=$r['k3'];
                                            $last[3]=$r['nama'];
                                            if($group>0){
                                                echo "</optgroup>";
                                            }
                                            $group++;
                                            echo "<optgroup label=\"".implode(" - ",$last)."\">";
                                            continue;
                                        }elseif($r['level']==4 && "$r[k1]$r[k2]$r[k3]"==implode("",$last_id)){
                                            echo "<option value='$r[id]'>" . format_coa($r['kode']) . " $r[nama]</option>";
                                        }
                                    }
                                    if($group>0){
                                        echo "</optgroup>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Jumlah Order</label>
                            <div class="col-sm-7">
                                <input type="text" ng-model="editjumlah" class="form-control" readonly="" style="height: 20px">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Penyelesaian Sebelumnya</label>
                            <div class="col-sm-7">
                                <input type="text" ng-model="editditerima" class="form-control" readonly="" style="height: 20px">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Penyelesaian Sekarang<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" ng-model="editsekarang" id="entrypenerimaan" required class="form-control" placeholder="" onclick="$(this).select()" style="height: 20px">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Menunggu Penyelesaian</label>
                            <div class="col-sm-7">
                                <h5>{{(editjumlah*1)-(editditerima*1)-(editsekarang*1)}}</h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Catatan</label>
                            <div class="col-sm-7">
                                <textarea ng-model="editnote" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-8 col-sm-offset-4">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">SIMPAN</button>
                                <button type="button" data-dismiss="modal" class="btn btn-default waves-effect waves-light">BATAL</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.bootcomplete.js"></script>
<script src="<?=base_url()?>assets/js/angular.min.js"></script>
<script type="text/javascript">
    $('#formedit').on('shown.bs.modal', function () {
        $('[ng-model=editharga]',this).focus();
        $('.select2').select2();
    });
    $('#formtambah').on('shown.bs.modal', function () {
        $('[name=barangjasa]',this).focus();
    });
    var app = angular.module("gradinSA", []);
    app.controller('orderController', function($scope,$http) {
        $scope.items=[];
        $scope.total=0;
        $scope.init = function(){
            <?php
            foreach ($data as $d){
                $d['nama']=htmlveryspecialchars($d['nama']);
                $d['jumlah']=is_numeric($d['jumlah'])?$d['jumlah']:0;
                $d['diterima']=is_numeric($d['diterima'])?$d['diterima']:0;
                $d['beban']=isset($penerimaan[$d['mstitem']])?$penerimaan[$d['mstitem']]:'';
                $d['exist']=isset($penerimaan[$d['mstitem']])?'true':'false';
                echo '$scope'.".items.unshift({permintaan:'$d[permintaan]',bebanket:'".@$d['bebanket']."',beban:'".@$d['beban']."',nota:'$d[nota]',mstitem:'$d[mstitem]',barangjasa:'$d[nama]',jumlah:'$d[jumlah]',note:'',exist:$d[exist],diterima:$d[diterima],sekarang:0});";
            }
            ?>
        };
        $scope.simpanorder = function() {
            $('#preloader').show();
            $('#status','#preloader').show();
            $scope.form=$('#form-order').serializeArray();
            $http({
                url: "?save=true",
                data: $scope.form,
                method: 'POST',
                headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}

            }).success(function(data){
                window.location.assign('<?=base_url('pembelian/penyelesaian')?>');
                setTimeout(function () {
                    $('#preloader').hide();
                    $('#status','#preloader').hide();
                },2000);
                console.log("OK", data);
            }).error(function(err){"ERR", console.log(err)})
        };
        $scope.simpanedit = function() {
            if(0<=(($scope.editjumlah*1)-($scope.editditerima*1)-($scope.editsekarang*1))){
                $scope.items[$scope.editindex].sekarang=$scope.editsekarang;
                $scope.items[$scope.editindex].note=$scope.editnote;
                $scope.items[$scope.editindex].beban=$scope.editbeban;
                $scope.items[$scope.editindex].bebanket=$('[value=\''+$scope.editbeban+'\']','[name=beban]').text();
                $scope.update();
                $('#formedit').modal('hide');
            }else{
                $('#entrypenerimaan').focus();
            }
        };
        $scope.edit = function(_id) {
            $scope.editindex=_id;
            $scope.editmstitem=$scope.items[_id].mstitem;
            $scope.editbarangjasa=$scope.items[_id].barangjasa;
            $scope.editjumlah=$scope.items[_id].jumlah;
            $scope.editditerima=$scope.items[_id].diterima;
            $scope.editsekarang=$scope.items[_id].sekarang;
            $scope.editnote=$scope.items[_id].note;
            $scope.editbeban=$scope.items[_id].beban;
            $scope.editexist=$scope.items[_id].exist;
            $('#formedit').modal('show');
            setTimeout(function () {
                $('#entrypenerimaan').focus();
            },500);
        };
        $scope.hapus = function(_id) {
            $scope.items.splice(_id, 1);
            $scope.update();
        };
        $scope.update=function () {
            $scope.total=0;
            for (var i in $scope.items) {
                $scope.total=parseFloat($scope.total)+parseFloat($scope.items[i].sekarang);
            }
        };
    });
    $(function () {
        $('.select2').select2();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('input[name=karyawan]').bootcomplete({
            url:'?k=true',
            minLength : 1,
            method: 'post',
            idFieldName: 'request_by',
            formParams: {
                'unit' : $('[name=request_unit]')
            }
        });
        $('input[name=barangjasa]').bootcomplete({
            url:'?bj=true',
            minLength : 1,
            method: 'post',
            idFieldName: 'mstitem'
        });
        $('[name=supplier]').select2('open');
    });
</script>