<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            include VIEWPATH.'alert.php';
            ?>
            <a href="#new-data" class="<?=is_authority(@$access['c'])?> btn btn-inverse" data-animation="blur" data-plugin="custommodal" data-overlayspeed="100" data-overlaycolor="#36404a"><i class="fa fa-plus"></i> ADD NEW DATA</a>
            <button onclick="$('#list-pending').toggle()" class="<?=is_authority(@$access['c'])?> ini-haram btn btn-inverse"><i class="fa fa-plus"></i> ADD NEW DATA</button>
            <a href="?" class="btn btn-primary"><i class="fa fa-refresh"></i> RELOAD</a>
            <hr>
            <form method="get" class="row" action="<?=base_url('pembelian/umk')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" readonly class="form-control input-sm" name="start" onchange="this.form.submit()" value="<?=$start?>">
                            <span class="input-group-addon input-sm">~</span>
                            <input type="text" readonly class="form-control input-sm" name="end" onchange="this.form.submit()" value="<?=$end?>">
                        </div>
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="">Semua status</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status aktif</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status closed</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">No. UMK: &nbsp;</label><input type="search" name="nota" class="form-control input-sm" autocomplete="off" value="<?=$nota?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-2">Tanggal</th>
                        <th class="center col-xs-2">UMK</th>
                        <th class="center col-xs-2">BKK/BBK/BMCK</th>
                        <th class="center col-xs-2">Jumlah</th>
                        <th class="center col-xs-2">Supplier</th>
                        <th class="center col-xs-1">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=$page;
                    foreach($data as $g) {
                        ?>
                        <tr>
                            <td scope="row" class="center"><?=++$no?></td>
                            <td class="center"><?=format_waktu($g['tanggal'])?></td>
                            <td class="center nota"><?=$g['nota']?></td>
                            <td class="center nota"><?=$g['jurnal_nota']?></td>
                            <td class="right"><?=format_uang($g['uangmuka'])?></td>
                            <td><?=$g['supplier']?></td>
                            <td class="center"><?= getLabelCLosed($g['closed'])?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <?=$pagination?>
        </div>
    </div>
</div>
<div id="detailpenerimaan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-2">Invoice</th>
                        <th class="center col-xs-2">PPn</th>
                        <th class="center col-xs-2">PPh</th>
                        <th class="center col-xs-2">Tagihan</th>
                        <th class="center col-xs-2">Diskon</th>
                        <th class="center col-xs-2">Sisa Tagihan</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div id="new-data" class="modal-gradin">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Form Pembayaran Supplier</h4>
    <div class="custom-modal-text">
        <form action="?new=true" method="post">
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" name="nota_id" required class="form-control" autocomplete="" placeholder="ID Supplier">
                </div>
                <div class="col-sm-12 right" style="padding-top: 1em">
                    <button type="button" onclick="Custombox.close();" class="btn btn-default">BATAL</button>
                    <button type="submit" class="btn btn-primary">BUKA</button>
                </div>
            </div>
        </form>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<style>
    .bc-menu .list-group-item{
        text-align: left;
    }
</style>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.bootcomplete.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    $('input[name=nota_id]','#new-data').bootcomplete({
        url:'?kode=true',
        minLength : 2,
        method: 'post'
    });
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function hapus(_i) {
        swal({
            title: "Batalkan Retur?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{delete:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    function rincian(_i,_nota) {
        $.post('?',{d:_i},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html(_nota);
            $('tbody','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
</script>