<div class="container-alt">
    <div class="row">
        <div class="col-sm-12">
            <div class="wrapper-page">
                <div class="m-t-40 account-pages">
                    <div class="text-center account-logo-box">
                        <h2 class="text-uppercase">
                            <span><img src="<?=$config['logo1']?>" alt="<?=$config['company']?>" style="max-width: 90%"></span>
                        </h2>
                    </div>
                    <div class="account-content">
                        <div class="text-center m-b-20">
                            <p class="text-muted m-b-0 font-13">Enter your email address and we'll send you an email with instructions to reset your password.</p>
                        </div>
                        <form class="form-horizontal" action="#">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <input class="form-control" type="email" required="" placeholder="Enter email">
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" name="username" required="" placeholder="Enter Username">
                                </div>
                            </div>
                            <div class="form-group account-btn text-center m-t-10">
                                <div class="col-xs-12">
                                    <button class="btn w-md btn-bordered btn-inverse waves-effect waves-light" type="submit">Send Email</button>
                                </div>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="row m-t-50">
                    <div class="col-sm-12 text-center">
                        <p class="text-muted"><a href="<?=base_url()?>" class="text-muted m-l-5"><b>Sign In</b></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>