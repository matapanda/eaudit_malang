<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        check_access('e964618e-29bd-4e17-91f2-1f42c0fa0488');
        $this->template->set_view('laporan');
        $this->load->model('audit');
    }


    public function index()
    {

        if ($this->input->get_post('nomor_spt')) {

            $this->db->order_by('nama','desc');
            $this->db->where('status','t');
            $this->db->select('nama as tahun');
            $data['list_tahun'] = $this->db->get('public.periode')->result_array();
            $data['tahun'] = @$this->input->get_post('tahun')?$this->input->get_post('tahun'):date('Y');

            $no = $this->input->get_post('nomor_spt');
            $data['no'] = $this->input->get_post('nomor_spt');
//            if($this->input->get_post('start') && $this->input->get_post('end')){
//                $data['start']=$this->input->get_post('start');
//                $data['end']=$this->input->get_post('end');
//
//                $tgl_start=explode('/',$data['end']);
//                $data['tahun']=$tgl_start[2];
//            }else{
//                $data['start']=date('d/m/Y',strtotime('-1 years'));
//                $data['end']=date('d/m/Y');
//            }
            $this->db->limit(100);
            $this->db->where('EXTRACT(year FROM spt_date) =', $data['tahun']);
            $this->db->order_by('proses desc');
            $this->db->like('spt_no', $no);
            $data['data'] = $this->db->get('interpro.v_pelaksanaan')->result_array();
            $this->db->order_by('name');
            $this->db->where('report', true);
            $this->db->where('parent', 'e964618e-29bd-4e17-91f2-1f42c0fa0488');
            $data['laporan'] = $this->db->get('public.menu')->result_array();
            $this->template->user('hasil', $data, array('title' => $no, 'breadcrumbs' => array('Laporan')));
            return;
        } else {
            redirect(base_url());
        }
    }
    public function cetak_spt()
    {
        if($this->input->get_post('start') && $this->input->get_post('end')){
            $data['start']=$this->input->get_post('start');
            $data['end']=$this->input->get_post('end');


            $tgl_start=explode('/',$data['start']);
            $tgl_m=$tgl_start[2].'-'.$tgl_start['1'].'-'.$tgl_start['0'];
            $tgl_end=explode('/',$data['end']);
            $tgl_e=$tgl_end[2].'-'.$tgl_end['1'].'-'.$tgl_end['0'];
        }else{
            $start=date('YYYY-mm-dd',strtotime('-1 years'));
            $end=date('YYYY-mm-dd');

            $data['start']=date('d/m/Y',strtotime('-1 years'));
            $data['end']=date('d/m/Y');
        }
        if ($this->input->get_post('search')) {
            $no = $this->input->get_post('search');
            $data['search']=$no;
//            $this->db->limit(100);
            $this->db->order_by('proses desc');
            $this->db->like('spt_no', $no);
            $this->db->where('end_date >=', $tgl_m);
            $this->db->where('start_date <=', $tgl_e);
            $data['data'] = $this->db->get('interpro.v_pelaksanaan')->result_array();
//            echo json_encode($data['data']);die();
            $this->db->order_by('name');
            $this->db->where('report', true);
            $this->db->where('parent', 'e964618e-29bd-4e17-91f2-1f42c0fa0488');
            $data['laporan'] = $this->db->get('public.menu')->result_array();
            }
        $no='Pencarian Nomor SPT';
        $data['titles']='Pencarian Nomor SPT';
        $this->template->user('hasil_p', $data, array('title' => $no, 'breadcrumbs' => array('Laporan')));

    }

    public function search()
    {
        if ($this->input->get_post('query')) {
            $data['start']=date('d/m/Y',strtotime('-1 years'));
            $data['end']=date('d/m/Y');
            $this->db->limit(10);
            $this->db->order_by('proses desc');
            $this->db->like('lower(spt_no)', strtolower($this->input->get_post('query')));
            $this->db->select("spt_no as id,spt_no as label");
            $data = $this->db->get('interpro.v_pelaksanaan')->result_array();

//            $this->db->limit(10);
//            $this->db->order_by('spt_no', 'asc');
//            $this->db->like('lower(spt_no)', strtolower($this->input->get_post('query')));
//            $this->db->where('status', TRUE);
//            $this->db->select("spt_no as id,spt_no as label");
//            $data = $this->db->get('interpro.perencanaan')->result_array();
            echo json_encode($data);
        } else {
            redirect(base_url());
        }
    }
    public function satker()
    {
        $menu = '8a1dfd71-6c85-412f-91de-970ca913dc60';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        $data['date'] = $this->input->get_post('date') ? $this->input->get_post('date') : date('Y');
        if ($data['search'] <> '') {
            $this->db->like('lower(master.satker.kode)', $data['search']);
            $this->db->or_like('lower(master.satker.ket)', $data['search']);
            $this->db->or_like('lower(master.satker.nama)', $data['search']);
            $this->db->or_like('lower(master.satker.alamat)', $data['search']);
            $this->db->or_like('lower(master.satker.email)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.satker.status', $data['closed']);
        }
        $data['satker'] = $this->user->get_list_satker2($data['date']);
        $data['periode'] = $this->db->get('periode')->result_array();
        $this->template->user('satker', $data, array('title' => 'Satker', 'breadcrumbs' => array('Master')));
    }
    public function s_keg()
    {
        $menu = '93391995-8d3d-447c-a50f-1cec916d3c27';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : (isset($_GET['closed']) ? '' : 'f');
        $data['satker_s'] = $this->input->get_post('satker') ? $this->input->get_post('satker') : '';
        $data['program_s'] = $this->input->get_post('program') ? $this->input->get_post('program') : '';

        $this->db->order_by('nama');
        $data['list_satker'] = $this->user->get_list_satker();
        $this->db->order_by('nama');
        $data['list_program'] = $this->db->get('support.program_satker')->result_array();


        if($data['satker_s']!=''){
            $this->db->where('k.satker', $data['satker_s']);
        }
        if($data['program_s']!=''){
            $this->db->where('k.program_satker', $data['program_s']);
        }
        $this->db->join('support.kegiatan_satker k', 'mk.id=k.kegiatan', 'left');
        $this->db->join('support.kegiatan_kr kr', 'kr.id=k.id', 'left');
        $this->db->group_by('k.jks,k.jka,k.jkb,mk.nama');
        $this->db->select('sum(k.jumlah) as jumlah,sum(kr.sd) as sd,k.jks,k.jka,k.jkb,mk.nama as nama_keg,sum(kr.jml) as jml,sum(kr.real) as real');
        $data['satker'] = $this->db->get("master.kegiatan mk")->result_array();

        $this->template->user('k_satker', $data, array('title' => 'Laporan Per Kegiatan', 'breadcrumbs' => array('Laporan')));
    }
    public function pr_p()
    {
        $menu = '93391995-8d3d-447c-a50f-1cec916d3c27';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        $data['closed'] = 't';
        $data['satker_s'] = $this->input->get_post('satker') ? $this->input->get_post('satker') : '';
        $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : '';

        $data['program_s'] = $this->input->get_post('program') ? $this->input->get_post('program') : '';
        $data['kegiatan_s'] = $this->input->get_post('kegiatan') ? $this->input->get_post('kegiatan') : '';

        $this->db->order_by('nama');
        $this->db->join('support.kegiatan_kr kr', 'kr.id=s.id');
        $this->db->join('support.setting_kegiatan sp', 'sp.id=s.kegiatan','left');
        $this->db->distinct();
        $this->db->select('s.*,sp.nama as nama_s');
        $data['list_kegiatan'] = $this->db->get('support.kegiatan_satker s')->result_array();

        $this->db->order_by('nama');
        $this->db->join('support.program_kr kr', 'kr.id=s.id');
        $this->db->join('support.setting_program sp', 'sp.id=s.kegiatan','left');
        $this->db->distinct();
        $this->db->select('s.*,sp.nama as nama_s');
        $data['list_program'] = $this->db->get('support.program_satker s')->result_array();
        $this->db->where('status',true);
        $data['list_periode'] = $this->db->get('periode')->result_array();


        $data['title']='kegiatan';
        if($data['kegiatan_s']!=''){
            $data['program_s'] ='';
            if($data['program_s']!='All'){
                $this->db->where('kr.id', $data['kegiatan_s']);
            }
        }
        if($data['periode_s']!=''){
                $this->db->where('s.periode', $data['periode_s']);
        }
        if($data['program_s']!=''){
                $data['satker_s'] ='';
                $data['title']='program';
            if($data['program_s']!='All'){
                $this->db->where('kr.id', $data['program_s']);
            }
        }

        $this->db->where('s.status', true);
        $this->db->group_by('rs.nama,p.nama,s.id,sa.ket,kr.sk,kr.sd,sp.nama,rs,s.eval');

        $this->db->order_by('p.nama,s.id asc,sp.nama asc,(COALESCE(kr.sk,0)*COALESCE(kr.sd,0)) desc');
        $this->db->join('support.'.strtolower($data['title']).'_satker s', 'sp.id=s.kegiatan');
        $this->db->join('support.'.strtolower($data['title']).'_kr kr', 'kr.id=s.id');
        $this->db->join('master.tipe_resiko rs', 'rs.id=kr.resiko');
        $this->db->join('public.periode p', 'p.id=s.periode');
        $this->db->join('master.satker sa', 'sa.id=s.satker');
        $this->db->select(' p.nama as periode,s.id,sa.ket,COALESCE(kr.sk,0) as sk,COALESCE(kr.sd,0) as sd,sp.nama as nama_s,rs.nama as nama_r,(COALESCE(kr.sk,0)*COALESCE(kr.sd,0)),(NULLIF(COUNT(kr.opsi),0)) as opsi,(NULLIF(COUNT(kr.sk),0)) as sk,s.eval');
        $this->db->distinct();
        $data['jml']=$this->db->get('support.setting_'.strtolower($data['title']).' sp')->result_array();

        $this->template->user('pr_p', $data, array('title' => 'Laporan Per Resiko & Kegiatan', 'breadcrumbs' => array('Laporan')));
    }
    public function skala_resiko()
    {
        $menu = 'e7dadcd0-c8ce-41e1-af4b-4fdc566a8262';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        $data['sd'] = $this->input->get_post('sd') ? $this->input->get_post('sd') : 'kem';
        $data['satker_s'] = $this->input->get_post('satker') ? $this->input->get_post('satker') : '';

        $this->db->order_by('nama');

//        $this->db->select('k.*');
//        $this->db->join('master.kegiatan k', "k.id=ks.kegiatan");
        $data['list_satker'] = $this->db->get('master.kegiatan')->result_array();
//        $data['list_satker'] = $this->user->get_list_satker();

        $this->db->group_by('t.nama');
        $this->db->select('sum(kr.sk) as tot,sum(kr.sd) as tot2,t.nama');
        if($data['satker_s']!=''){
            $this->db->join('support.kegiatan_kr kr', 'kr.resiko=t.id');
            $this->db->join('support.kegiatan_satker ks', "kr.id=ks.id and ks.satker='$data[satker_s]'");
        } else{
            $this->db->join('support.kegiatan_kr kr', 'kr.resiko=t.id', 'left');

        }
        $data['risk2'] = $this->db->get("master.tipe_resiko t")->result_array();

        $this->db->where('kr.sk <>',0);
        $this->db->where('kr.sd <>',0);
        $this->db->select('ks.id,(kr.sk) as tot,(kr.sd) as tot2,tr.nama');
        if($data['satker_s']!=''){
            $this->db->join('master.kegiatan k', "k.id=ks.kegiatan and ks.kegiatan='$data[satker_s]'");
            $this->db->join('support.kegiatan_kr kr', "kr.id=ks.id ");
            $this->db->join('master.tipe_resiko tr', "tr.id=kr.resiko");
        }else{
            $this->db->join('master.kegiatan k', "k.id=ks.kegiatan");
            $this->db->join('support.kegiatan_kr kr', "kr.id=ks.id");
            $this->db->join('master.tipe_resiko tr', "tr.id=kr.resiko");
        }
        $data['risk'] = $this->db->get("support.kegiatan_satker ks")->result_array();

//        echo json_encode($data['risk']);die();

        $this->template->user('risk', $data, array('title' => 'Laporan Tipe Resiko', 'breadcrumbs' => array('Laporan')));
    }
    public function pkpt($tahun=null)
    {
        $tahun=isset($tahun)?$tahun:date('Y');
        $this->load->library("PHPExcel");
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load(BASEPATH . "../assets/excel/pkpt.xlsx");

        $sheet = $objPHPExcel->getActiveSheet();
        $sheet->setCellValue('A9', "DATA PKPT TAHUN $tahun");
        $this->load->model('user');
        $pkpt = $this->user->get_list_pkpt($tahun);
        $mulai=12;
        foreach($pkpt as $no=>$t){
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
            $sheet->getStyle('A'.$mulai.":J".$mulai)->applyFromArray($styleArray);
            $sheet->setCellValue('A'.$mulai, $t['no']);
            $sheet->setCellValue('B'.$mulai, $t['nama']);
            $sheet->setCellValue('D'.$mulai, $t['tema']);
            $sheet->setCellValue('F'.$mulai, $t['kegiatan']);
            $sheet->setCellValue('H'.$mulai, $t['dana']);
            $sheet->setCellValue('J'.$mulai, $t['risiko']);
            $mulai++;

        }

        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=\"pkpt$tahun.xlsx\"");
        header("Cache-Control: max-age=0");
        $output = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $output->save('php://output');

    }
    public function tao()
    {
        $this->load->library("PHPExcel");
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load(BASEPATH . "../assets/excel/tao.xlsx");

        $sheet = $objPHPExcel->getActiveSheet();
//        $sheet->setCellValue('A9', "DATA PKPT TAHUN $tahun");
        $this->load->model('user');
        $this->db->order_by('kode,kertas_kerja');
        $this->db->select('support.v_tao_header.*,support.tao_detail.kertas_kerja,support.tao_detail.langkah as langkah_tao');
        $this->db->join('support.tao_detail','support.tao_detail.id_tao=support.v_tao_header.id');
        $ko=$this->db->get('support.v_tao_header')->result_array();
        $mulai=12;

        $temp = '';
        foreach($ko as $no=>$t){
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
            $sheet->getStyle('A'.$mulai.":F".$mulai)->applyFromArray($styleArray);
            if ($temp != $t['kode']) {
                $temp = $t['kode'];
            $sheet->setCellValue('A'.$mulai, $t['kode']);
            $sheet->setCellValue('B'.$mulai, $t['langkah']);
            }
            $sheet->setCellValue('D'.$mulai, $t['kertas_kerja']);
            $sheet->setCellValue('F'.$mulai, $t['langkah_tao']);
            $mulai++;

        }

        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=\"tao.xlsx\"");
        header("Cache-Control: max-age=0");
        $output = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $output->save('php://output');

    }

    public function export($type = null, $id = null,$test=null,$mulai=null,$akir=null)
    {
        if($this->input->get_post('lhp')){
            $this->db->trans_begin();
            $this->db->where('id',$this->input->get_post('id_rencana'));
            $lhp=$this->db->get('interpro.lap_lhp')->row_array();
            if($lhp['id']==''){
                $this->db->insert('interpro.lap_lhp',array(
                    'id'=>$this->input->get_post('id_rencana'),
                    'jdl'=>$this->input->get_post('jdl'),
                    'no'=>$this->input->get_post('no'),
                    'tgl'=>$this->input->get_post('tgl'),
                    'lampiran'=>$this->input->get_post('lampiran'),
                    'hal'=>$this->input->get_post('hal'),
                    'ditujukan'=>$this->input->get_post('ditujukan'),
                    'dasar'=>$this->input->get_post('dasar'),
                    'tujuan'=>$this->input->get_post('tujuan'),
                    'sasaran'=>$this->input->get_post('sasaran'),
                    'ruang'=>$this->input->get_post('ruang'),
                    'waktu'=>$this->input->get_post('waktu'),
                    'metodologi'=>$this->input->get_post('metodologi'),
                    'keuangan'=>$this->input->get_post('keuangan'),
                    'organisasi'=>$this->input->get_post('organisasi'),
                    'lainya'=>$this->input->get_post('lainya'),
                    'penutup'=>$this->input->get_post('penutup'),
                ));
            }
            else{
            $this->db->where('id',$this->input->get_post('id_rencana'));
            $this->db->update('interpro.lap_lhp',array(
                'id'=>$this->input->get_post('id_rencana'),
                'jdl'=>$this->input->get_post('jdl'),
                'no'=>$this->input->get_post('no'),
                'tgl'=>$this->input->get_post('tgl'),
                'lampiran'=>$this->input->get_post('lampiran'),
                'hal'=>$this->input->get_post('hal'),
                'ditujukan'=>$this->input->get_post('ditujukan'),
                'dasar'=>$this->input->get_post('dasar'),
                'tujuan'=>$this->input->get_post('tujuan'),
                'sasaran'=>$this->input->get_post('sasaran'),
                'ruang'=>$this->input->get_post('ruang'),
                'waktu'=>$this->input->get_post('waktu'),
                'metodologi'=>$this->input->get_post('metodologi'),
                'keuangan'=>$this->input->get_post('keuangan'),
                'organisasi'=>$this->input->get_post('organisasi'),
                'lainya'=>$this->input->get_post('lainya'),
                'penutup'=>$this->input->get_post('penutup'),
            ));
            }
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status_update'] = false;
            } else {
                $this->db->trans_commit();
                $data['status_update'] = true;
            }
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
                redirect(base_url());
            }
        }
        if (isset($id)) {
            $this->db->where('id', $id);
            $data['laporan'] = $this->db->get('interpro.perencanaan')->row_array();
//            echo json_encode($data['laporan']);die();
            switch ($type) {
                case '304c528b-aa08-466d-a766-45d3c102d733'://tracking process
                    $this->db->where('id', $data['laporan']['app_sv1']);
                    $data['reviu_dalnis'] = $this->db->get('users')->row_array();
                    $this->db->where('id', $data['laporan']['app_sv2']);
                    $data['reviu_irban'] = $this->db->get('users')->row_array();
                    $this->db->where('id', $data['laporan']['ketuatim']);
                    $data['reviu_ketua'] = $this->db->get('users')->row_array();

                    $this->db->order_by('t.order asc');
                    $this->db->where('pt.id', $id);
                    $this->db->join('master.tujuan t', 't.id=pt.tujuan');
                    $this->db->select('t.ket');
                    $data['tujuan'] = $this->db->get('interpro.perencanaan_tujuan pt')->result_array();

                    $this->db->order_by('s.order asc');
                    $this->db->where('ps.id', $id);
                    $this->db->join('master.sasaran s', 's.id=ps.sasaran');
                    $this->db->select('s.ket');
                    $data['sasaran'] = $this->db->get('interpro.perencanaan_sasaran ps')->result_array();

                    $this->db->where('tl.id_rencana', $id);
                    $this->db->order_by('tld.id', 'asc');
                    $this->db->join('support.kode_temuan kt', 'tld.kode_temuan=kt.id');
                    $this->db->join('interpro.tindak_lanjut tl', 'tld.id=tl.id', 'left');
                    $this->db->select("kt.id as id_t,(kt.k1||'.'||kt.k2||'.'||kt.k3||'.'||kt.k4) as kode_t,kt.nama as temuan, tld.tindak_lanjut as tl, tld.judul_temuan, tld.uraian_temuan, tld.nilai_temuan");
                    $data['temuan'] = $this->db->get('interpro.tindak_lanjutd tld')->result_array();
//echo json_encode($data['temuan']);die();
                    if(!empty($data['temuan'])){
                        $this->db->where('tl.id_rencana', $id);
                        $this->db->join('interpro.tindak_lanjut tl', 'tlp.id=tl.id', 'left');
                        $this->db->join('master.penyebab p', 'tlp.kode_penyebab=p.id', 'left');
                        $this->db->select('p.kode,p.nama,tlp.*');
                        $data['tl_penyebab'] = $this->db->get('interpro.tindak_lanjut_penyebab tlp')->result_array();

                        $this->db->where('tl.id_rencana', $id);
                        $this->db->join('interpro.tindak_lanjut tl', 'tlr.id=tl.id', 'left');
                        $this->db->join('support.kode_temuan kt', 'tlr.kode_rekom=kt.id', 'left');
                        $this->db->join('master.tindak_lanjut mtl', 'tlr.no_tl=mtl.id', 'left');
                        $this->db->select('(k1||k2||k3||k4) as kode_rekoms,kt.nama as nama_rekom,mtl.kode as kode_tl,mtl.nama as uraian_tindakl,tlr.*');
                        $data['tl_rekom'] = $this->db->get('interpro.tindak_lanjut_rekom tlr')->result_array();

                    }
//                    echo json_encode($data['tl_rekom']);die();

                    $this->db->order_by('v_tao_pelaksanaan.tahapan,v_tao_pelaksanaan.kode_kk', 'asc');
                    $this->db->join('interpro.perencanaan_detail pd', "v_tao_pelaksanaan.id=pd.tao AND pd.id='$id'");
                    $this->db->join('public.users u2', "u2.id=pd.realisasi_by", 'left');
                    $this->db->join('public.users u3', "u3.id=pd.app_sv1", 'left');
                    $this->db->join('public.users u4', "u4.id=pd.app_sv2", 'left');
                    $this->db->join('public.users u5', "u5.id=pd.app_sv3", 'left');
                    $this->db->select("v_tao_pelaksanaan.*,u2.name as realisasi_by,pd.realisasi_date,pd.progres,pd.file as isian,pd.kesimpulan,u3.name as dalnis_name,u4.name as irban_name,pd.app_sv1_tstamp,pd.app_sv1_note,pd.app_sv2_tstamp,pd.app_sv2_note");
                    $data['program'] = $this->db->get('support.v_tao_pelaksanaan')->result_array();

                    if ($this->input->get_post('pdf')) {
                        $html = $this->load->view('private/laporan/tracking_process_pdf', $data, true);
                        $pdf = $this->pdf->load('UTf-8', 'A4');
                        $pdf->SetDefaultFont('calibri');
                        $pdf->SetDefaultFontSize(11);
                        $pdf->WriteHTML($html);
                        $pdf->Output();
                    } else {
                        $this->load->library('pdf');
                        ini_set('memory_limit', '32M');
                        $this->template->user('tracking_process', $data, array('title' => 'Proses Penyelesaian Penugasan', 'breadcrumbs' => array('Laporan')));
                    }
                    break;
                case '5fc41cc2-2391-4ba9-a1d5-d04d0aeef216'://km3
                    $this->load->library("PHPExcel");
                    $this->db->where('id', $data['laporan']['satker']);
                    $satker = $this->db->get('master.satker')->row_array();
                    $pendahuluan = $this->db->get('interpro.range_tahapan(\'' . $data['laporan']['id'] . '\',\'1\')')->row_array();
                    $pelaksanaan = $this->db->get('interpro.range_tahapan(\'' . $data['laporan']['id'] . '\',\'2\')')->row_array();
                    $penyelesaian = $this->db->get('interpro.range_tahapan(\'' . $data['laporan']['id'] . '\',\'3\')')->row_array();
//                    echo json_encode($pelaksanaan);
//                    die();
                    $this->db->order_by('v_tao_pelaksanaan.tahapan,v_tao_pelaksanaan.kode_kk','asc');
                    $this->db->join('interpro.perencanaan_detail pd',"v_tao_pelaksanaan.id=pd.tao AND pd.id='$id'");
                    $this->db->join('interpro.perencanaan p',"pd.id=p.id");
                    $this->db->join('public.users u1',"u1.id=pd.rencana_by",'left');
                    $this->db->join('public.users u2',"u2.id=pd.realisasi_by",'left');
                    $this->db->select("v_tao_pelaksanaan.*,pd.jumlah_hari,p.app_sv1_tstamp,p.app_sv2_tstamp,p.created_at,p.modified_at,u1.name as rencana_by,to_char(pd.rencana_date,'dd/mm/yyyy') as rencana_date,u2.name as realisasi_by,to_char(pd.realisasi_date,'dd/mm/yyyy') as realisasi_date,pd.progres,pd.file as isian");
                    $tao=$this->db->get('support.v_tao_pelaksanaan')->result_array();
//echo json_encode($tao);die();
                    $this->db->where('id', $data['laporan']['app_sv1']);
                    $dalnis = $this->db->get('public.users')->row_array();
                    $this->db->where('id', $data['laporan']['app_sv2']);
                    $irban = $this->db->get('public.users')->row_array();
                    $this->db->where('id', $data['laporan']['ketuatim']);
                    $ketua = $this->db->get('public.users')->row_array();

                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    $objPHPExcel = $objReader->load(BASEPATH . "../assets/excel/km3.xlsx");
                    $sheet = $objPHPExcel->getActiveSheet();
                    $no=1;
                    $tambahcell=0;
                    $hari=0;
                    $mulai=14;
                    $total_dif_all=0;
                    $total_irban=0;
                    $total_dalnis=0;
                    $total_anggota=0;
                    $total_diff_all_univ=0;
                    foreach($tao as $t){
                        $date_dalnis=$t['app_sv1_tstamp'];
                        $date_irban=$t['app_sv2_tstamp'];
                        $diff_dalnis=hitung_hari(($t['modified_at']==null?$t['modified_at']:$t['created_at']),$date_dalnis);
                        $diff_irban=hitung_hari(($t['modified_at']==null?$t['modified_at']:$t['created_at']),$date_irban);
                        $total_anggota=$total_anggota+$t['jumlah_hari'];
                        $total_dalnis=$total_dalnis+$diff_dalnis;
                        $total_irban=$total_irban+$diff_irban;
                        $total_dif=$diff_irban+$diff_dalnis+$t['jumlah_hari'];
                        $total_dif_all=$total_dif_all+$total_dif;
                        $total_diff_all_univ=$total_dif_all+$total_diff_all_univ;
                        $sheet->setCellValue('B'.$mulai, $no);
                        $sheet->setCellValue('C'.$mulai, $t['langkah']);
                        $sheet->setCellValue('F'.$mulai, $diff_irban);
                        $sheet->setCellValue('H'.$mulai, $diff_dalnis);
                        $sheet->setCellValue('I'.$mulai, 0);
                        $sheet->setCellValue('L'.$mulai, $t['jumlah_hari']);
                        $sheet->setCellValue('M'.$mulai, $total_dif);
                        $no++;
                        $mulai++;
                        $tambahcell++;
                        $hari=$hari+$t['jumlah_hari'];
                    }
                    if($no<=11){
                        $tambahcell=0;
                        $cellsub=25;
                    }
                    else{
                        $cellsub=$mulai+1;
                        $tambahcell=$tambahcell-14;
                    }
                    $sheet->setCellValue('C'.$cellsub, 'Sub Jumlah : '.--$no);
                    $sheet->setCellValue('F'.(26+$tambahcell),format_uang($total_irban));
                    $sheet->setCellValue('H'.(26+$tambahcell),format_uang($total_dalnis));
                    $sheet->setCellValue('I'.(26+$tambahcell),format_uang(0));
                    $sheet->setCellValue('L'.(26+$tambahcell),format_uang($total_anggota));
                    $sheet->setCellValue('M'.(26+$tambahcell),format_uang($total_diff_all_univ));
                    $sheet->setCellValue('B'.($cellsub+1), 'Jumlah Hari yang dianggarkan');
                    $sheet->setCellValue('L'.($cellsub+1), $hari);
                    $sheet->setCellValue('B'.(32+$tambahcell), @$dalnis['name']);
                    $sheet->setCellValue('B'.(33+$tambahcell), "NIP: " . @$dalnis['nip']);
                    $sheet->setCellValue('E'.(39+$tambahcell), @$irban['name']);
                    $sheet->setCellValue('E'.(40+$tambahcell), "NIP: " . @$irban['nip']);
                    $sheet->setCellValue('B'.(28+$tambahcell), "Banyuwangi, " . format_tanggal($data['laporan']['app_sv1_tstamp']));
                    $sheet->setCellValue('B'.(29+$tambahcell), "Pengendali Teknis");
                    $sheet->setCellValue('I'.(28+$tambahcell), "Banyuwangi, " . format_tanggal($data['laporan']['created_at']));
                    $sheet->setCellValue('I'.(29+$tambahcell), "Ketua Tim");
                    $sheet->setCellValue('I'.(32+$tambahcell), @$ketua['name']);
                    $sheet->setCellValue('I'.(33+$tambahcell), "NIP: " . @$ketua['nip']);
                    $sheet->setCellValue('E'.(35+$tambahcell), "Banyuwangi, " . format_tanggal($data['laporan']['app_sv2_tstamp']));
                    $sheet->setCellValue('B7', "Nama Obyek/Satker: " . @$satker['ket']);
                    $sheet->setCellValue('G7', "Kegiatan: ");
                    $sheet->setCellValue('E10', format_tanggal($pendahuluan['awal']) . " sd " . format_tanggal($pendahuluan['akhir']));
                    $sheet->setCellValue('H10', format_tanggal($pelaksanaan['awal']) . " sd " . format_tanggal($pelaksanaan['akhir']));
                    $sheet->setCellValue('K10', format_tanggal($penyelesaian['awal']) . " sd " . format_tanggal($penyelesaian['akhir']));
                    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    header("Content-Disposition: attachment; filename=\"KM3.xlsx\"");
                    header("Cache-Control: max-age=0");
                    $output = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $output->save('php://output');
                    break;
//                case '5fc41cc2-2391-4ba9-a1d5-d04d0aeef216'://km3
//                    $this->load->library("PHPExcel");
//                    $this->db->where('id', $data['laporan']['satker']);
//                    $satker = $this->db->get('master.satker')->row_array();
//                    $pendahuluan = $this->db->get('interpro.range_tahapan(\'' . $data['laporan']['id'] . '\',\'1\')')->row_array();
//                    $pelaksanaan = $this->db->get('interpro.range_tahapan(\'' . $data['laporan']['id'] . '\',\'2\')')->row_array();
//                    $penyelesaian = $this->db->get('interpro.range_tahapan(\'' . $data['laporan']['id'] . '\',\'3\')')->row_array();
////echo json_encode($pendahuluan);
////                    die();
//                    $this->db->order_by('v_tao_pelaksanaan.tahapan,v_tao_pelaksanaan.kode_kk','asc');
//                    $this->db->join('interpro.perencanaan_detail pd',"v_tao_pelaksanaan.id=pd.tao AND pd.id='$id'");
//                    $this->db->join('public.users u1',"u1.id=pd.rencana_by",'left');
//                    $this->db->join('public.users u2',"u2.id=pd.realisasi_by",'left');
//                    $this->db->select("v_tao_pelaksanaan.*,pd.jumlah_hari,u1.name as rencana_by,to_char(pd.rencana_date,'dd/mm/yyyy') as rencana_date,u2.name as realisasi_by,to_char(pd.realisasi_date,'dd/mm/yyyy') as realisasi_date,pd.progres,pd.file as isian");
//                    $tao=$this->db->get('support.v_tao_pelaksanaan')->result_array();
////echo json_encode($tao);die();
//                    $this->db->where('id', $data['laporan']['app_sv1']);
//                    $dalnis = $this->db->get('public.users')->row_array();
//                    $this->db->where('id', $data['laporan']['app_sv2']);
//                    $irban = $this->db->get('public.users')->row_array();
//
//                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
//                    $objPHPExcel = $objReader->load(BASEPATH . "../assets/excel/km3.xlsx");
//                    $sheet = $objPHPExcel->getActiveSheet();
//                    $no=1;
//                    $tambahcell=0;
//                    $mulai=14;
//                    foreach($tao as $t){
//                        $sheet->setCellValue('B'.$mulai, $no);
//                        $sheet->setCellValue('C'.$mulai, $t['langkah']);
//                        $sheet->setCellValue('F'.$mulai, '-');
//                        $sheet->setCellValue('H'.$mulai, '-');
//                        $sheet->setCellValue('I'.$mulai, '-');
//                        $sheet->setCellValue('L'.$mulai, $t['jumlah_hari']);
//                        $no++;
//                        $tambahcell++;
//                    }
//                    if($no<=11){
//                        $tambahcell=0;
//                    }
//                    else{
//                        $tambahcell=$tambahcell-14;
//                    }
//                    $sheet->setCellValue('B32', @$dalnis['name']);
//                    $sheet->setCellValue('B33', "NIP: " . @$dalnis['nip']);
//                    $sheet->setCellValue('E39', @$irban['name']);
//                    $sheet->setCellValue('E40', "NIP: " . @$irban['nip']);
//                    $sheet->setCellValue('B28', "Banyuwangi, " . format_tanggal($data['laporan']['app_sv1_tstamp']));
//                    $sheet->setCellValue('B29', "Pengendali Teknis");
//                    $sheet->setCellValue('I28', "Banyuwangi, ......");
//                    $sheet->setCellValue('I29', "Ketua Tim");
//                    $sheet->setCellValue('E35', "Banyuwangi, " . format_tanggal($data['laporan']['app_sv2_tstamp']));
//                    $sheet->setCellValue('B7', "Nama Obyek/Satker: " . @$satker['ket']);
//                    $sheet->setCellValue('G7', "Kegiatan: ");
//                    $sheet->setCellValue('E10', format_tanggal($pendahuluan['awal']) . " sd " . format_tanggal($pendahuluan['akhir']));
//                    $sheet->setCellValue('H10', format_tanggal($pelaksanaan['awal']) . " sd " . format_tanggal($pelaksanaan['akhir']));
//                    $sheet->setCellValue('K10', format_tanggal($penyelesaian['awal']) . " sd " . format_tanggal($penyelesaian['akhir']));
//                    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//                    header("Content-Disposition: attachment; filename=\"KM3.xlsx\"");
//                    header("Cache-Control: max-age=0");
//                    $output = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//                    $output->save('php://output');
//                    break;
                case 'b280071d-87e7-44be-99f3-64cd75883e63'://km4
                    $this->load->library("PHPExcel");

                    $this->db->where('id', $id);
                    $team = $this->db->get('interpro.perencanaan')->row_array();

                    $this->db->where('id', $data['laporan']['app_sv1']);
                    $dalnis = $this->db->get('public.users')->row_array();
                    $this->db->where('id', $data['laporan']['ketuatim']);
                    $ketua = $this->db->get('public.users')->row_array();
                    $this->db->where('id', $data['laporan']['app_sv2']);
                    $irban = $this->db->get('public.users')->row_array();
                    $inspektur= $this->db->get('inspektur')->row_array();

                    $t1=str_replace('[','',$team['tim']);
                    $t2=str_replace(']','',$t1);
                    $t3=str_replace('"','',$t2);
                    $t4=explode(',',$t3);
                    $array_dump2=array($irban['id'],$ketua['id'],$dalnis['id']);
                    $dll=array();
                    foreach($t4 as $t){
                        if(!in_array($t,$array_dump2)){
                        $dll[]=$t;
                    }
                        $this->db->where('pd.id', $id);
                    $this->db->where('u.id',$t);
                    $this->db->where('rencana_by',$t);
                    $this->db->select('pd.*,u.name as nama_rencana, u2.name as nama_realisasi');
                    $this->db->join('users u2',"pd.realisasi_by=u2.id");
                    $this->db->join('users u',"pd.rencana_by=u.id");
                        $a=$this->db->get('interpro.perencanaan_detail pd')->row_array();
                        if($a!=null){
                        $array_dump[]=array('nama_rencana'=>$a['nama_rencana'],'nama_realisasi'=>$a['nama_realisasi'],'rencana_by'=>$a['rencana_by'],'rencana_date'=>$a['rencana_date'],'realisasi_by'=>$a['realisasi_by'],'realisasi_date'=>$a['realisasi_date'],'jumlah_hari'=>$a['jumlah_hari']);
                        }
                    }

                    $this->db->where('id', $data['laporan']['satker']);
                    $satker = $this->db->get('master.satker')->row_array();

                    $this->db->where('id', $data['laporan']['pkpt_no']);
                    $pkpt = $this->db->get('master.pkpt')->row_array();

                    $this->db->where('ps.id', $data['laporan']['id']);
                    $this->db->select("string_agg(s.ket,',') as ket");
                    $this->db->join("master.sasaran s", 's.id=ps.sasaran');
                    $sasaran = $this->db->get('interpro.perencanaan_sasaran ps')->row_array();

                    $this->db->where('pt.id', $data['laporan']['id']);
                    $this->db->select("string_agg(t.ket,',') as ket");
                    $this->db->join("master.tujuan t", 't.id=pt.tujuan');
                    $tujuan = $this->db->get('interpro.perencanaan_tujuan pt')->row_array();

                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    $objPHPExcel = $objReader->load(BASEPATH . "../assets/excel/km4.xlsx");

                    $pelaksanaan = $this->db->get('interpro.range_tahapan(\'' . $data['laporan']['id'] . '\',\'0\')')->row_array();

                    $sheet = $objPHPExcel->getActiveSheet();


                    $start=37;
                    foreach($array_dump as $a){
                        $sheet->setCellValue('B'.$start, $a['nama_rencana']);
                        $sheet->setCellValue('D'.$start, $a['nama_realisasi']);
                        $sheet->setCellValue('G'.$start, $a['jumlah_hari'].' hari');
                        $sheet->setCellValue('I'.$start, hitung_hari($a['rencana_date'],$a['realisasi_date']).' hari');
                        $start++;
                    }

                    $sheet->setCellValue('F12', ($data['laporan']['judul']));
                    $sheet->setCellValue('F14', @$satker['ket']);
                    $sheet->setCellValue('F15', @$satker['alamat']);
                    $sheet->setCellValue('F17', @$pkpt['no']);
                    $sheet->setCellValue('F19', @$pkpt['nama']);
                    $sheet->setCellValue('G49', @$data['laporan']['no_laporan']);
                    $sheet->setCellValue('G5', @$data['laporan']['kp_date']);
                    $sheet->setCellValue('F20', @$sasaran['ket']);
                    $sheet->setCellValue('F21', @$tujuan['ket']);
                    $sheet->setCellValue('F30', ($data['laporan']['spt_no']));
                    $sheet->setCellValue('F31', format_tanggal($data['laporan']['spt_date']));
                    $sheet->setCellValue('F32', format_tanggal($data['laporan']['start_date']));
                    $sheet->setCellValue('F33', format_tanggal($data['laporan']['end_date']));
                    $sheet->setCellValue('F34', format_tanggal($pelaksanaan['awal']));
                    $sheet->setCellValue('B44', 'Rencana mulai pengawasan bulan  : '.format_bulan($data['laporan']['start_date']));
                    $sheet->setCellValue('B45', 'Realisasi mulai pengawasan bulan  : '.format_bulan($pelaksanaan['awal']));
                    $sheet->setCellValue('H46', ': '.format_tanggal($data['laporan']['end_date']));
                    $sheet->setCellValue('H47', ': '.format_tanggal($pelaksanaan['awal']));
                    if(count($dll)>0){
                        $sa=27;
                        foreach($dll as $d){
                            $this->db->where('u.id',$d);
                            $a=$this->db->get('users u')->row_array();
                            $sheet->setCellValue('F'.$sa, @$a['name']);
                            $sa++;
                        }
                    }
                    $sheet->setCellValue('F'.(25), @$dalnis['name']);
                    $sheet->setCellValue('F'.(24), @$irban['name']);
                    $sheet->setCellValue('F'.(26), @$ketua['name']);
                    $sheet->setCellValue('H'.(56), @$dalnis['name']);
                    $sheet->setCellValue('H'.(57), "NIP: " . @$dalnis['nip']);
                    $sheet->setCellValue('H'.(52), "Banyuwangi, " . format_tanggal($data['laporan']['app_sv1_tstamp']));
                    $sheet->setCellValue('C'.(56), @$irban['name']);
                    $sheet->setCellValue('C'.(57), "NIP: " . @$irban['nip']);
                    $sheet->setCellValue('C'.(52), "Banyuwangi, " . format_tanggal($data['laporan']['app_sv2_tstamp']));
                    $sheet->setCellValue('F'.(64), @$inspektur['nama']);
                    $sheet->setCellValue('F'.(65), "NIP: " . @$inspektur['nip']);

                    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    header("Content-Disposition: attachment; filename=\"KM4.xlsx\"");
                    header("Cache-Control: max-age=0");
                    $output = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $output->save('php://output');
                    break;
                case 'e7dadcd0-c8ce-41e1-af4b-4fdc566a8868'://temuan_tl
                    $this->load->library("PHPExcel");
                    $this->db->where('id', $data['laporan']['satker']);
                    $satker = $this->db->get('master.satker')->row_array();

                    $this->db->where('id', $data['laporan']['pkpt_no']);
                    $pkpt = $this->db->get('master.pkpt')->row_array();

                    $this->db->order_by('kt.k1','asc');
                    $this->db->join('support.kode_temuan kt','tld.kode_temuan=kt.id');
                    $this->db->join('support.kode_temuan kt2','tld.kode_rekom=kt2.id');
                    $this->db->select("kt.id as id_t,kt2.id as id_r,(kt.k1||'.'||kt.k2||'.'||kt.k3||'.'||kt.k4) as kode_t,(kt2.k1||'.'||kt2.k2||'.'||kt2.k3||'.'||kt2.k4) as kode_r,kt.nama as temuan,kt2.nama as rekom, tld.tindak_lanjut as tl,  tld.hasil_rekom as kode_hr,  tld.keterangan as keterangan,  tld.nilai_rekom as nilai_r, ,  CASE WHEN tld.hasil_rekom=1 THEN 'Sesuai' WHEN tld.hasil_rekom=2 THEN 'Belum Sesuai' WHEN tld.hasil_rekom=3 THEN 'Belum Ditindaklanjuti' ELSE 'Tidak Dapat Ditindaklanjuti' END as hasil_r, tld.negara as nk1, tld.daerah as nk2, tld.nd as nd");
                    $temuan=$this->db->get('interpro.tindak_lanjutd tld')->result_array();

                    $this->db->where('ps.id', $data['laporan']['id']);
                    $this->db->select("string_agg(s.ket,',') as ket");
                    $this->db->join("master.sasaran s", 's.id=ps.sasaran');
                    $sasaran = $this->db->get('interpro.perencanaan_sasaran ps')->row_array();

                    $this->db->where('pt.id', $data['laporan']['id']);
                    $this->db->select("string_agg(t.ket,',') as ket");
                    $this->db->join("master.tujuan t", 't.id=pt.tujuan');
                    $tujuan = $this->db->get('interpro.perencanaan_tujuan pt')->row_array();

                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    $objPHPExcel = $objReader->load(BASEPATH . "../assets/excel/lap_pemantauan.xlsx");

                    $pelaksanaan = $this->db->get('interpro.range_tahapan(\'' . $data['laporan']['id'] . '\',\'0\')')->row_array();

                    $sheet = $objPHPExcel->getActiveSheet();
                    $sheet->setCellValue('C4', @$data['laporan']['no_laporan']);
                    $sheet->setCellValue('C5', format_waktu(@$data['laporan']['kp_date']));
                    $sheet->setCellValue('C6', @$data['laporan']['thn_laporan']);
                    $sheet->setCellValue('C7', @$satker['ket']);

                    $no=1;
                    $tambahcell=0;
                    $hari=0;
                    $mulai=12;
                    foreach($temuan as $t){

                        $styleArray = array(
                            'borders' => array(
                                'allborders' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                )
                            )
                        ); $styleArray_align_center = array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            )
                        );$styleArray_align_left = array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                            )
                        );
                        $sheet->getStyle('A'.$mulai.":Q".$mulai)->applyFromArray($styleArray);
                        $sheet->getStyle('A'.$mulai)->applyFromArray($styleArray_align_center);
                        $sheet->getStyle('B'.$mulai)->applyFromArray($styleArray_align_center);
                        $sheet->getStyle('D'.$mulai)->applyFromArray($styleArray_align_center);
                        $sheet->getStyle('H'.$mulai)->applyFromArray($styleArray_align_center);
                        $sheet->getStyle('O'.$mulai)->applyFromArray($styleArray_align_center);
                        $sheet->getStyle('Q'.$mulai)->applyFromArray($styleArray_align_left);
                        $sheet->setCellValue('A'.$mulai, $no);
                        $sheet->setCellValue('B'.$mulai, $t['kode_t']);
                        $sheet->setCellValue('C'.$mulai, $t['temuan']);
                        $sheet->setCellValue('D'.$mulai, $t['kode_r']);
                        $sheet->setCellValue('E'.$mulai, $t['rekom']);
                        $sheet->setCellValue('F'.$mulai, $t['nk1']);
                        $sheet->setCellValue('G'.$mulai, $t['nk2']);
                        $sheet->setCellValue('H'.$mulai, $t['tl']);
                        if($t['hasil_r']==1){
                            $sheet->setCellValue('I'.$mulai, $t['hasil_r']);
                            $sheet->setCellValue('J'.$mulai, $t['nilai_r']);
                        }elseif($t['hasil_r']==2){
                            $sheet->setCellValue('K'.$mulai, $t['hasil_r']);
                            $sheet->setCellValue('L'.$mulai, $t['nilai_r']);
                        }elseif($t['hasil_r']==3){
                            $sheet->setCellValue('M'.$mulai, $t['hasil_r']);
                            $sheet->setCellValue('N'.$mulai, $t['nilai_r']);
                        }else{
                            $sheet->setCellValue('O'.$mulai, $t['hasil_r']);
                            $sheet->setCellValue('P'.$mulai, $t['nilai_r']);
                        }
                        $sheet->setCellValue('Q'.$mulai, $t['keterangan']);
                        $no++;
                        $mulai++;

                    }

                    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    header("Content-Disposition: attachment; filename=\"lap_pemantauan.xlsx\"");
                    header("Cache-Control: max-age=0");
                    $output = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $output->save('php://output');
                    break;
                case 'd54451d1-3a31-442a-8279-1cdc0ed2b221'://km8


                    $this->db->where('ps.id', $data['laporan']['id']);
                    $this->db->select("string_agg(s.ket,',') as ket");
                    $this->db->join("master.sasaran s", 's.id=ps.sasaran');
                    $sasaran = $this->db->get('interpro.perencanaan_sasaran ps')->row_array();

                    $this->db->where('pt.id', $data['laporan']['id']);
                    $this->db->select("string_agg(t.ket,',') as ket");
                    $this->db->join("master.tujuan t", 't.id=pt.tujuan');
                    $tujuan = $this->db->get('interpro.perencanaan_tujuan pt')->row_array();

                    $this->db->where('id', $data['laporan']['jenis']);
                    $jenis = $this->db->get('master.jenis')->row_array();

                    $this->db->where('pd.id', $data['laporan']['id']);
                    $this->db->join("support.tao_detail t", 'pd.tao=t.id');
                    $this->db->select('t.langkah,pd.*');
                    $detail = $this->db->get('interpro.perencanaan_detail pd')->result_array();
//                    echo json_encode($data['laporan']['id']);die();


                    $this->load->library("PHPExcel");
                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    $objPHPExcel = $objReader->load(BASEPATH . "../assets/excel/km8.xlsx");
                    $sheet = $objPHPExcel->getActiveSheet();
                    $sheet->setCellValue('C4', @$data['laporan']['no_laporan']);
                    $sheet->setCellValue('C5', format_waktu(@$data['laporan']['kp_date']));
                    $sheet->setCellValue('C6', @$jenis['ket']);
                    $sheet->setCellValue('C7', @$data['laporan']['judul']);
                    $sheet->setCellValue('C8', @$sasaran['ket']);

                    $no=1;
                    $tambahcell=0;
                    $hari=0;
                    $mulai=13;
                    foreach($detail as $t){

                        $styleArray = array(
                            'borders' => array(
                                'allborders' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                )
                            )
                        );
                        $sheet->getStyle('A'.$mulai.":D".$mulai)->applyFromArray($styleArray);
                        $sheet->setCellValue('A'.$mulai, $t['langkah']);
                        $sheet->setCellValue('C'.$mulai, $t['app_sv1_note']);
                        $sheet->setCellValue('D'.$mulai, $t['app_sv2_note']);
                        $mulai++;

                        if(count($detail)==$no){
                            $styleArray = array(
                                'borders' => array(
                                    'allborders' => array(
                                        'style' => PHPExcel_Style_Border::BORDER_THIN
                                    )
                                )
                            );
                            $sheet->getStyle('A'.$mulai.":D".$mulai)->applyFromArray($styleArray);
                        }
                        $no++;


                    }


                    $sheet->setCellValue('A'.($mulai+4), 'KETUA TIM');
                    $sheet->setCellValue('C'.($mulai+4), 'DALNIS/SUPERVISI');
                    $sheet->setCellValue('D'.($mulai+4), 'DALTU/IRBAN');


                    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    header("Content-Disposition: attachment; filename=\"KM8.xlsx\"");
                    header("Cache-Control: max-age=0");

                    $output = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $output->save('php://output');
                    break;
                case '23ca0398-00b3-4547-9312-63633b0e4a09'://km9

                    $id_rencana=$id;
                    $this->db->where('id',$id_rencana);
                    $data['rencana']=$this->db->get('interpro.perencanaan')->row_array();

                    $this->db->where('id',$id_rencana);
                    $data['rencana']['sasaran']=$this->db->get('interpro.perencanaan_sasaran')->result_array();

                    $this->db->where('id',$id_rencana);
                    $data['rencana']['tujuan']=$this->db->get('interpro.perencanaan_tujuan')->result_array();

                    $this->db->order_by('v_tao_pelaksanaan.tahapan,v_tao_pelaksanaan.kode_kk','asc');
                    $this->db->join('interpro.perencanaan_detail pd',"v_tao_pelaksanaan.id=pd.tao AND pd.id='$id_rencana'");
                    $this->db->join('public.users u1',"u1.id=pd.rencana_by",'left');
                    $this->db->join('public.users u2',"u2.id=pd.realisasi_by",'left');
                    $this->db->select("v_tao_pelaksanaan.*,u1.name as rencana_by,to_char(pd.rencana_date,'dd/mm/yyyy') as rencana_date,u2.name as realisasi_by,to_char(pd.realisasi_date,'dd/mm/yyyy') as realisasi_date,pd.progres,pd.file as isian");
                    $data['tao']=$this->db->get('support.v_tao_pelaksanaan')->result_array();

                    $this->load->library("PHPExcel");
                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    $objPHPExcel = $objReader->load(BASEPATH . "../assets/excel/km9_n.xlsx");

                    $sheet = $objPHPExcel->getActiveSheet();
                    $sheet->setCellValue('C7', @$data['rencana']['spt_no']);
                    $sheet->setCellValue('C8', format_waktu(@$data['rencana']['spt_date']));
                    $sheet->setCellValue('C6', @$data['rencana']['judul']);

                    $no=1;
                    $tambahcell=0;
                    $hari=0;
                    $mulai=11;
                    foreach($data['tao'] as $t){

                        $styleArray = array(
                            'borders' => array(
                                'allborders' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                )
                            )
                        ); $styleArray_align_center = array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            )
                        );$styleArray_align_left = array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                            )
                        );
                        $sheet->getStyle('A'.$mulai.":I".$mulai)->applyFromArray($styleArray);
                        $sheet->mergeCells('B'.$mulai.":D".$mulai);
                        $sheet->getStyle('A'.$mulai)->applyFromArray($styleArray_align_center);
                        $sheet->getStyle('I'.$mulai)->applyFromArray($styleArray_align_left);
                        $sheet->setCellValue('A'.$mulai, $no);
                        $sheet->setCellValue('B'.$mulai, $t['langkah']);
                        $sheet->setCellValue('E'.$mulai, $t['rencana_by']);
                        $sheet->setCellValue('F'.$mulai, $t['rencana_date']);
                        $sheet->setCellValue('G'.$mulai, $t['realisasi_by']);
                        $sheet->setCellValue('H'.$mulai, $t['realisasi_date']);
                            $sheet->setCellValue('I'.$mulai, $t['kode_kk']);
                        $no++;
                        $mulai++;

                    }

                    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    header("Content-Disposition: attachment; filename=\"KM9.xlsx\"");
                    header("Cache-Control: max-age=0");
                    $output = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $output->save('php://output');
                    break;
                case '5281099c-88c4-417d-9e0d-285fcc8ffc41'://SPT
                    $this->db->where('interpro.perencanaan_tujuan.id',$data['laporan']['id']);
                    $this->db->join('interpro.perencanaan_tujuan','interpro.perencanaan_tujuan.tujuan=master.tujuan.id');
                    $data['tujuan']= $this->db->get('master.tujuan')->result_array();
                    $data['tim']= $this->db->get('interpro.daftar_tim_dan_jabatan(\''.$data['laporan']['id'].'\')')->result_array();
                    $data['inspektur']= $this->db->get('inspektur')->row_array();
                    $this->db->where('id',$data['laporan']['id']);
                    $data['anggota']= $this->db->get('interpro.perencanaan')->row_array();

                    foreach($data['tim'] as $g){
                        $timr[]=$g['id'];
                    }
                    //tim yg penanggung jawab dll

                    $tims=json_decode($data['anggota']['tim'],true);
                    $max=count($tims);
                    for($i=0;$i<$max;$i++){
                        echo $tims[$i].' : '.json_encode($timr).'<br>';
                        if (in_array($tims[$i], $timr, TRUE)) {
                            unset($tims[$i]);
                        }
                    }
//                    echo json_encode(count($tims));die();
                    $this->db->where_in('id',$tims);
                    $data['anggota_tim']=$this->db->get('users')->result_array();

                    $this->db->where('status','t');
//                    $data['conf_spt']= $this->db->get('master.conf_spt')->row_array();
                    $data['conf_spt']= $this->db->get('master.conf_spt')->result_array();
//echo json_encode($data['tim']);die();
                    $this->load->library('pdf');
                    ini_set('memory_limit','32M');
                    $html = $this->load->view('private/laporan/cetak_spt', $data, true);
                    $pdf = $this->pdf->load('UTf-8', 'A4');
                    $pdf->SetDefaultFont('calibri');
                    $pdf->SetDefaultFontSize(12);
                    $pdf->WriteHTML($html);
                    $pdf->Output();
                    break;
                case '5281099c-88c4-417d-9e0d-285fcc8ffc42'://LHP
                    $this->db->where('interpro.perencanaan_tujuan.id',$data['laporan']['id']);
                    $this->db->join('interpro.perencanaan_tujuan','interpro.perencanaan_tujuan.tujuan=master.tujuan.id');
                    $data['tujuan']= $this->db->get('master.tujuan')->result_array();
                    $data['tim']= $this->db->get('interpro.daftar_tim_dan_jabatan(\''.$data['laporan']['id'].'\')')->result_array();

                    $this->db->where('tl.id_rencana', $id);
                    $this->db->order_by('tld.id', 'asc');
                    $this->db->join('support.kode_temuan kt', 'tld.kode_temuan=kt.id');
                    $this->db->join('interpro.tindak_lanjut tl', 'tld.id=tl.id', 'left');
                    $this->db->select("kt.id as id_t,(kt.k1||'.'||kt.k2||'.'||kt.k3||'.'||kt.k4) as kode_t,kt.nama as temuan, tld.tindak_lanjut as tl, tld.judul_temuan, tld.uraian_temuan, tld.nilai_temuan");
                    $data['temuan'] = $this->db->get('interpro.tindak_lanjutd tld')->result_array();

                    $this->db->where('id', $id);
                    $data['laporan2'] = $this->db->get('interpro.lap_lhp')->row_array();

                    if(!empty($data['temuan'])){
                        $this->db->where('tl.id_rencana', $id);
                        $this->db->join('interpro.tindak_lanjut tl', 'tlp.id=tl.id', 'left');
                        $this->db->join('master.penyebab p', 'tlp.kode_penyebab=p.id', 'left');
                        $this->db->select('p.kode,p.nama,tlp.*');
                        $data['tl_penyebab'] = $this->db->get('interpro.tindak_lanjut_penyebab tlp')->result_array();

                        $this->db->where('tl.id_rencana', $id);
                        $this->db->join('interpro.tindak_lanjut tl', 'tlr.id=tl.id', 'left');
                        $this->db->join('support.kode_temuan kt', 'tlr.kode_rekom=kt.id', 'left');
                        $this->db->join('master.tindak_lanjut mtl', 'tlr.no_tl=mtl.id', 'left');
                        $this->db->select('(k1||k2||k3||k4) as kode_rekoms,kt.nama as nama_rekom,mtl.kode as kode_tl,mtl.nama as uraian_tindakl,tlr.*');
                        $data['tl_rekom'] = $this->db->get('interpro.tindak_lanjut_rekom tlr')->result_array();

                    }

                    $this->template->user('lhp2', $data, array('title' => 'Form LHP', 'breadcrumbs' => array('Laporan')));

                    break;
                    //otw
                case 'e7dadcd0-c8ce-41e1-af4b-4fdc566a8268'://tEMUAN TL2
                    $this->load->library("PHPExcel");
                    $this->db->where('id', $data['laporan']['satker']);
                    $satker = $this->db->get('master.satker')->row_array();

                    $this->db->where('id', $data['laporan']['pkpt_no']);
                    $pkpt = $this->db->get('master.pkpt')->row_array();

                    $this->db->order_by('kt.k1','asc');
                    $this->db->where('id_rencana', $data['laporan']['id']);
                    $this->db->join('support.kode_temuan kt','tld.kode_temuan=kt.id');
                    $this->db->join('interpro.tindak_lanjut tl','tl.id=tld.id');
                    $this->db->join('interpro.tindak_lanjut_rekom tlr','tl.id=tlr.id');
                    $this->db->join('support.kode_temuan kt2','tlr.kode_rekom=kt2.id');
                    $this->db->select("(kt.k1||'.'||kt.k2||'.'||kt.k3||'.'||kt.k4) as kode_t,(kt2.k1||'.'||kt2.k2||'.'||kt2.k3||'.'||kt2.k4) as kode_r,tlr.uraian_rekom,kt.nama as temuan,kt2.nama as rekom, tld.judul_temuan,  tld.kondisi,tld.kriteria,tld.akibat");
                    $data['temuan']=$this->db->get('interpro.tindak_lanjutd tld')->result_array();

//                    echo json_encode($data['temuan']);die();

                    $this->db->where('ps.id', $data['laporan']['id']);
                    $this->db->select("string_agg(s.ket,',') as ket");
                    $this->db->join("master.sasaran s", 's.id=ps.sasaran');
                    $sasaran = $this->db->get('interpro.perencanaan_sasaran ps')->row_array();

                    $this->db->where('pt.id', $data['laporan']['id']);
                    $this->db->select("string_agg(t.ket,',') as ket");
                    $this->db->join("master.tujuan t", 't.id=pt.tujuan');
                    $tujuan = $this->db->get('interpro.perencanaan_tujuan pt')->row_array();


                    $pelaksanaan = $this->db->get('interpro.range_tahapan(\'' . $data['laporan']['id'] . '\',\'0\')')->row_array();

                    $this->load->library('pdf');
                    ini_set('memory_limit','32M');
                    $html = $this->load->view('private/laporan/cetak_tl2', $data, true);
                    $pdf = $this->pdf->load('UTf-8', 'A4');
                    $pdf->SetDefaultFont('calibri');
                    $pdf->SetDefaultFontSize(12);
                    $pdf->WriteHTML($html);
                    $pdf->Output();
                    break;
                case '':
                    break;
                default:
                    redirect(base_url());
                    break;
            }
        }
        else {
            redirect(base_url());
        }
    }

    public function lap_rgs(){
        $menu = 'e964618e-29bd-4e17-91f2-1f42c0fa0428';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        if ($data['closed'] <> '') {
            $this->db->where('master.satker.status', $data['closed']);
        }
        $data['satker'] = $this->user->get_list_satker();
        $this->template->user('satker', $data, array('title' => 'Laporan Resiko Gabungan Per Satker', 'breadcrumbs' => array('Laporan')));


    }
    public function spt()
    {
        $menu = '82f810bf-86ce-4c88-927e-5e9a4ea82587';
        $data['access'] = list_access($menu);
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : (isset($_GET['closed']) ? '' : 'f');
        $data['search'] = $this->input->get_post('search');
        $this->db->order_by('no');
        if ($data['search']) {
            $this->db->like('nama', $data['search']);
        }
        $this->db->where('status',true);
        $this->db->where('id not in (select pkpt_no::uuid from interpro.perencanaan where pkpt_no != null)', null,false);
        $data['pkpt'] = $this->db->get('master.pkpt')->result_array();
        $data['spt'] = $this->db->get('interpro.v_perencanaan')->result_array();
//        echo json_encode($data['spt']);die();
        $this->template->user('spt', $data, array('title' => 'Monitoring SPT', 'breadcrumbs' => array('Laporan')));
    }
    public function file_manager()
    {
        $menu = '59b034e9-a1ed-4267-afba-606ead1b9500';
        $data['access'] = list_access($menu);
        if($this->input->get_post('ajax')){
            $mode=$this->input->get_post('ajax');
            $val=$this->input->get_post('val');
            switch($mode){
                case 'periode':
                    $mode="PKPT";
                    $this->db->like('no', $val, 'after');
                    $this->db->select('id,no||\' - \'||tema||\' \'||nama as nama');
                    $data=$this->db->get('master.pkpt')->result_array();
                    break;
                case 'pkpt':
                    $mode="SPT";
                    $this->db->where('pkpt_no', $val);
                    $this->db->select('id,spt_no||\' - \'||judul as nama');
                    $data=$this->db->get('interpro.perencanaan')->result_array();
                    break;
                default:
                    break;
            }
            if(isset($data)){
                echo "<option value=''>Semua $mode</option>";
                foreach($data as $d){
                    echo "<option value='$d[id]'>$d[nama]</option>";
                }
            }
            return;
        }elseif($this->input->get_post('periode')){
            $this->db->select('spt_no||\' - \'||judul as nama,t.langkah,pd.file');
            $this->db->join('interpro.perencanaan p','p.id=pd.id');
            if($this->input->get_post('pkpt')){
                $this->db->where('p.pkpt_no',$this->input->get_post('pkpt'));
            }elseif($this->input->get_post('spt')){
                $this->db->where('pd.id',$this->input->get_post('spt'));
            }else{
                $this->db->like('pkpt.no',$this->input->get_post('periode'),'after');
                $this->db->where('(pkpt.id)::varchar=p.pkpt_no',null,false);
                $this->db->join('master.pkpt','1=1');
            }
            $this->db->where('pd.file is not null',null,false);
            $this->db->join('support.tao_detail t','t.id=pd.tao');
            $data=$this->db->get('interpro.perencanaan_detail pd')->result_array();
            header('Content-Type: application/json');
            echo json_encode($data);
            return;
        }
        $this->db->order_by('nama','desc');
        $data['list_periode'] = $this->db->get('periode')->result_array();
        $this->template->user('file_m', $data, array('title' => 'File Manager', 'breadcrumbs' => array('Laporan')));
    }
    public function rekap()
    {
//        $menu = '08f6f5e6-0933-48e2-88ce-ce52f2709041';
//        $data['access'] = list_access($menu);
        $data['tahun_a'] =date('Y');
        $data['tahun_e'] =date('Y')+1;
        if ($this->input->get_post('tahun_a')) {
            $data['tahun_as'] = $this->input->get_post('tahun_a');
            $data['tahun_a'] = $this->input->get_post('tahun_a');
            $data['tahun_e'] = $this->input->get_post('tahun_e');
            $arr=array();
        for($i=0;$data['tahun_a']<=$data['tahun_e'];$data['tahun_a']++){
            $hasil=$this->db->get("interpro.rekap_tl('$data[tahun_a]')")->result_array();
            if($hasil!=null){
            $arr[]=$hasil;
            }
            $i++;
        }
           $data['data'] = $arr;
//            echo json_encode($data['data']);die();

        }
        $this->template->user('rekap', $data, array('title' => 'Rekap Temuan dan Tindak Lanjut', 'breadcrumbs' => array('Laporan')));
    }
    public function temuan()
    {
//        $menu = '08f6f5e6-0933-48e2-88ce-ce52f2709041';
//        $data['access'] = list_access($menu);
        $this->db->order_by('nama','desc');
        $this->db->where('status','t');
        $this->db->select('nama as tahun');
        $data['list_tahun'] = $this->db->get('public.periode')->result_array();
        $data['tahun'] = @$this->input->get_post('tahun')?$this->input->get_post('tahun'):date('Y');


        $this->db->order_by('tl.skpd');
//        $this->db->order_by('kt.k1','asc');
        $this->db->where('EXTRACT(year FROM tgl_lhp) =', $data['tahun']);
        $this->db->join('support.kode_temuan kt','tld.kode_temuan=kt.id');
        $this->db->join('interpro.tindak_lanjut tl','tl.id=tld.id');
        $this->db->join('interpro.tindak_lanjut_rekom tlr','tl.id=tlr.id');
        $this->db->join('support.kode_temuan kt2','tlr.kode_rekom=kt2.id');
        $this->db->select("tl.no_lhp,tl.created_at,tl.skpd,kt.id as id_t,kt2.id as id_r,(kt.k1||'.'||kt.k2||'.'||kt.k3||'.'||kt.k4) as kode_t,(kt2.k1||'.'||kt2.k2||'.'||kt2.k3||'.'||kt2.k4) as kode_r,kt.nama as temuan,kt2.nama as rekom, tld.tindak_lanjut as tl,  tld.hasil_rekom as kode_hr,  tld.keterangan as keterangan,  tld.nilai_rekom as nilai_r, ,  CASE WHEN tld.hasil_rekom=1 THEN 'Sesuai' WHEN tld.hasil_rekom=2 THEN 'Belum Sesuai' WHEN tld.hasil_rekom=3 THEN 'Belum Ditindaklanjuti' ELSE 'Tidak Dapat Ditindaklanjuti' END as hasil_r, tld.negara as nk1, tld.daerah as nk2, tld.nd as nd");//        $this->db->select("(kt.k1||'.'||kt.k2||'.'||kt.k3||'.'||kt.k4) as kode_t,(kt2.k1||'.'||kt2.k2||'.'||kt2.k3||'.'||kt2.k4) as kode_r,tl.skpd,tlr.uraian_rekom,kt.nama as temuan,kt2.nama as rekom, tld.judul_temuan,  tld.kondisi,tld.kriteria,tld.akibat");
        $data['temuan']=$this->db->get('interpro.tindak_lanjutd tld')->result_array();
//        echo json_encode($data);die();

        $this->template->user('rekap_s', $data, array('title' => 'Rekap Temuan Satker', 'breadcrumbs' => array('Laporan')));
    }
    public function lap_tl()
    {
        $menu = '08f6f5e6-0933-48e2-88ce-ce52f2709040';
        $data['access'] = list_access($menu);

        if($this->input->get_post('id')){
            $this->load->library("PHPExcel");
            $this->db->distinct();
            $start=date('Y').'-01-01';
            $end = date("Y-m-d", strtotime(date("Y-m-d", strtotime($start)) . " + 365 day"));
            $this->db->where('id',$this->input->get_post('id'));
            $tem = $this->db->get("interpro.tindak_lanjut")->row_array();
            $this->db->where('id',$this->input->get_post('id'));
            $rencana = $this->db->get("interpro.tindak_lanjut_v7('$start ','$end')")->result_array();
            $this->db->where('tld.id',$this->input->get_post('id'));
            $this->db->join('interpro.tindak_lanjut tl', 'tld.id=tl.id', 'left');
            $this->db->select('tld.*');
            $temuan_list = $this->db->get('interpro.tindak_lanjutd tld')->result_array();
            $this->db->where('tld.id',$this->input->get_post('id'));
            $this->db->order_by('tl.created_at','desc');
            $this->db->join('interpro.tindak_lanjut tl', 'tld.id=tl.id', 'left');
            $this->db->join('support.kode_temuan kt', 'tld.kode_temuan=kt.id', 'left');
            $this->db->select('(kt.k1||kt.k2||kt.k3||kt.k4) as kode,tld.kode_temuan,tld.akibat,tld.kriteria,tld.tanggapan,tld.akibat,tld.judul_temuan,tld.uraian_temuan,tld.nilai_temuan,tld.urutan,tl.*');
            $tl = $this->db->get('interpro.tindak_lanjutd tld')->result_array();
//echo json_encode($data['tem']);die();

            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $objPHPExcel = $objReader->load(BASEPATH . "../assets/excel/lap_tl.xlsx");

            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A4', 'PER TANGGAL : '.format_waktu($tem['tgl_lhp']));
            $sheet->setCellValue('A6','NAMA OPD/SATKER : '.$tem['skpd']);
            $sheet->setCellValue('A11', "LHP NO :   $tem[no_lhp]");

            $id='';
            $urutan=1;
            $mulai=12;
            foreach($tl as $r){

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                ); $styleArray_align_center = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );$styleArray_align_left = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    )
                );
                $sheet->getStyle('A'.$mulai.":X".$mulai)->applyFromArray($styleArray);
                $sheet->getStyle('A'.$mulai)->applyFromArray($styleArray_align_center);
                $sheet->getStyle('B'.$mulai)->applyFromArray($styleArray_align_center);
                $sheet->getStyle('D'.$mulai)->applyFromArray($styleArray_align_center);
                $sheet->getStyle('H'.$mulai)->applyFromArray($styleArray_align_center);
                $sheet->getStyle('O'.$mulai)->applyFromArray($styleArray_align_center);
                $sheet->getStyle('Q'.$mulai)->applyFromArray($styleArray_align_left);

                $sheet->setCellValue('A'.$mulai, $urutan);
                $sheet->setCellValue('B'.$mulai, $r['uraian_temuan']);
                $sheet->setCellValue('C'.$mulai, $r['kode']);
                $sheet->setCellValue('D'.$mulai, $r['nilai_temuan']);

                $this->db->where('tlr.urutan',$r['urutan']);
                $this->db->where('tlr.id',$r['id']);
                $this->db->join('interpro.tindak_lanjut_penyebab tlp', 'tlp.id=tlr.id', 'left');
                $this->db->join('master.penyebab p', '(p.kode=(tlp.kode_penyebab)::varchar)', 'left');
                $this->db->select('p.kode,p.nama,tlr.urutan,tlp.uraian_penyebab');
                $peny = $this->db->get('interpro.tindak_lanjutd tlr')->row_array();

                $sheet->setCellValue('E'.$mulai, $peny['urutan']);
                $sheet->setCellValue('F'.$mulai, $peny['uraian_penyebab']);
                $sheet->setCellValue('G'.$mulai, $peny['kode']);

                $this->db->where('tlr.urutan',$r['urutan']);
                $this->db->where('tlr.id',$r['id']);
                $this->db->join('interpro.tindak_lanjut_akibat tlp', 'tlp.id=tlr.id', 'left');
                $this->db->join('master.akibat p', '(p.kode=(tlp.kode_akibat)::varchar)', 'left');
                $this->db->select('p.kode,p.nama,tlr.urutan,tlp.uraian_akibat');
                $akibat = $this->db->get('interpro.tindak_lanjutd tlr')->row_array();

                $sheet->setCellValue('H'.$mulai, $akibat['urutan']);
                $sheet->setCellValue('I'.$mulai, $akibat['uraian_akibat']);
                $sheet->setCellValue('J'.$mulai, $akibat['kode']);

                $this->db->where('tlr.urutan',$r['urutan']);
                $this->db->where('tlr.id',$r['id']);
                $this->db->join('support.kode_temuan kt', 'tlr.kode_rekom=kt.id', 'left');
                $this->db->join('master.tindak_lanjut tl', 'tl.id=tlr.no_tl', 'left');
                $this->db->select('(kt.k1||kt.k2||kt.k3||kt.k4) as kode, tlr.uraian_rekom,tlr.nilai_rekom,tlr.nilai_tl,tlr.uraian_tl,tl.kode as kode_tl');
                $rek = $this->db->get('interpro.tindak_lanjut_rekom tlr')->row_array();

                $sheet->setCellValue('K'.$mulai, $r['urutan']);
                $sheet->setCellValue('L'.$mulai, $rek['uraian_rekom']);
                $sheet->setCellValue('M'.$mulai, $rek['kode']);
                $sheet->setCellValue('N'.$mulai, $rek['nilai_rekom']);
                $sheet->setCellValue('O'.$mulai, $r['urutan']);
                $sheet->setCellValue('P'.$mulai, $rek['uraian_tl']);
                $sheet->setCellValue('Q'.$mulai, $rek['kode_tl']);
                $sheet->setCellValue('R'.$mulai, $rek['nilai_tl']);

                if(($rek['nilai_rekom']-$rek['nilai_tl'])==0){
                    $sheet->setCellValue('S'.$mulai, 1);
                    $sheet->setCellValue('T'.$mulai, $rek['nilai_rekom']);
                }elseif(($rek['nilai_rekom']-$rek['nilai_tl'])==$rek['nilai_rekom']){
                    $sheet->setCellValue('W'.$mulai, 1);
                    $sheet->setCellValue('X'.$mulai, $rek['nilai_rekom']);
                }else{
                    $sheet->setCellValue('U'.$mulai, 1);
                    $sheet->setCellValue('V'.$mulai, $rek['nilai_rekom']-$rek['nilai_tl']);
                }
                $mulai++;
                $urutan++;

            }

            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            header("Content-Disposition: attachment; filename=\"lap_tl.xls\"");
            header("Cache-Control: max-age=0");
            $output = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $output->save('php://output');
            return;
        }

//        $this->db->distinct();
//        $start=date('Y').'-01-01';
//        $end = date("Y-m-d", strtotime(date("Y-m-d", strtotime($start)) . " + 365 day"));
//        $data['rencana'] = $this->db->get("interpro.tindak_lanjut_v7('$start ','$end')")->result_array();
//        $this->db->join('interpro.tindak_lanjut tl', 'tld.id=tl.id', 'left');
//        $this->db->select('tld.*');
//        $data['temuan_list'] = $this->db->get('interpro.tindak_lanjutd tld')->result_array();
//        $this->db->order_by('tl.created_at','desc');
//        $this->db->join('interpro.tindak_lanjut tl', 'tld.id=tl.id', 'left');
//        $this->db->select('tld.kode_temuan,tld.akibat,tld.kriteria,tld.tanggapan,tld.akibat,tld.judul_temuan,tld.uraian_temuan,tld.nilai_temuan,tld.urutan,tl.*');
//        $data['tl'] = $this->db->get('interpro.tindak_lanjutd tld')->result_array();

//        $this->db->where('tld.id',$this->input->get_post('id'));
        $this->db->order_by('tl.created_at','desc');
        $this->db->join('interpro.tindak_lanjut tl', 'tld.id=tl.id', 'left');
        $this->db->join('support.kode_temuan kt', 'tld.kode_temuan=kt.id', 'left');
        $this->db->select('(kt.k1||kt.k2||kt.k3||kt.k4) as kode,tld.kode_temuan,tld.akibat,tld.kriteria,tld.tanggapan,tld.akibat,tld.judul_temuan,tld.uraian_temuan,tld.nilai_temuan,tld.urutan,tl.*');
        $data['tl'] = $this->db->get('interpro.tindak_lanjutd tld')->result_array();


//        foreach($tl as $r){
//            $this->db->where('tlr.urutan',$r['urutan']);
//            $this->db->where('tlr.id',$r['id']);
//            $this->db->join('interpro.tindak_lanjut_penyebab tlp', 'tlp.id=tlr.id', 'left');
//            $this->db->join('master.penyebab p', '(p.kode=(tlp.kode_penyebab)::varchar)', 'left');
//            $this->db->select('p.kode,p.nama,tlr.urutan,tlp.uraian_penyebab');
//            $peny = $this->db->get('interpro.tindak_lanjutd tlr')->row_array();
//
//            $this->db->where('tlr.urutan',$r['urutan']);
//            $this->db->where('tlr.id',$r['id']);
//            $this->db->join('interpro.tindak_lanjut_akibat tlp', 'tlp.id=tlr.id', 'left');
//            $this->db->join('master.akibat p', '(p.kode=(tlp.kode_akibat)::varchar)', 'left');
//            $this->db->select('p.kode,p.nama,tlr.urutan,tlp.uraian_akibat');
//            $akibat = $this->db->get('interpro.tindak_lanjutd tlr')->row_array();
//
//            $this->db->where('tlr.urutan',$r['urutan']);
//            $this->db->where('tlr.id',$r['id']);
//            $this->db->join('support.kode_temuan kt', 'tlr.kode_rekom=kt.id', 'left');
//            $this->db->join('master.tindak_lanjut tl', 'tl.id=tlr.no_tl', 'left');
//            $this->db->select('(kt.k1||kt.k2||kt.k3||kt.k4) as kode, tlr.uraian_rekom,tlr.nilai_rekom,tlr.nilai_tl,tlr.uraian_tl,tl.kode as kode_tl');
//            $rek = $this->db->get('interpro.tindak_lanjut_rekom tlr')->row_array();
//
//        }

        $this->template->user('lap_tl2', $data, array('title' => 'Laporan Temuan & Tindak Lanjut', 'breadcrumbs' => array('Internal Proses')));
    }
    public function kejadian()
    {
            $menu = 'cb7ac7ac-9d00-4acd-a1fd-2afefb0c8a81';
        $data['access'] = list_access($menu);
        $this->db->order_by('nama');
        $this->db->where('status', true);
        $data['periode'] = $this->db->get('periode')->result_array();
        $this->db->order_by('nama','desc');
        $data['p_sekarang'] = $this->db->get('periode')->row_array();
        $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : $data['p_sekarang']['id'];

        if ($this->input->get_post('tipes')) {
            if($this->input->get_post('tipes')=='K'){
                $this->db->where('tr.status', true);
                $this->db->where('periode', $this->input->get_post('periode'));
                $this->db->join('support.kegiatan_kr kr', 'kr.id=s.id');
                $this->db->join('master.tipe_resiko tr', 'kr.resiko=tr.id');
                $this->db->select('tr.*');
                $this->db->distinct();
                $kegiatan = $this->db->get('support.kegiatan_satker s')->result_array();
echo '<select class="select2 form-control select2-multiple" name="pk" data-placeholder="Pilih Resiko Satker">
                                <option value="">Pilih Resiko Satker</option>';
foreach ($kegiatan as $v){
echo '<option value="'.$v['id'].'">'.$v['nama'].'</option>';
}
echo'</select>';return;
            }
            elseif($this->input->get_post('tipes')=='K'){
                $this->db->where('tr.status', true);
                $this->db->where('periode', $this->input->get_post('periode'));
                $this->db->join('support.program_kr kr', 'kr.id=s.id');
                $this->db->join('master.tipe_resiko tr', 'kr.resiko=tr.id');
                $this->db->distinct();
                $this->db->select('tr.*');
                $kegiatan = $this->db->get('support.program_satker s')->result_array();
                echo '<select class="select2 form-control select2-multiple" name="pk" data-placeholder="Pilih Resiko Satker">
                                <option value="">Pilih Resiko Satker</option>';
                foreach ($kegiatan as $v){
                    echo '<option value="'.$v['id'].'">'.$v['nama'].'</option>';
                }
                echo'</select>';
                return;
        }
        else{
            $this->db->where('status', true);
            $kegiatan = $this->db->get('master.tipe_resiko')->result_array();
            echo '<select class="select2 form-control select2-multiple" name="pk" data-placeholder="Pilih Resiko Satker">
                                <option value="">Pilih Resiko Satker</option>';
            foreach ($kegiatan as $v){
                echo '<option value="'.$v['id'].'">'.$v['nama'].'</option>';
            }
            echo'</select>';
            return;
        }
        }
        elseif($this->input->get_post('cek')){
            $this->db->where('resiko', $this->input->get_post('pk'));
            $this->db->where('support.kejadian_satker.status', true);
            $this->db->select('sum(support.kejadian_satker.jumlah) as kejadian');
            $data['kejadian']=$this->db->get('support.kejadian_satker')->row_array();
//echo json_encode($data['kejadian']);die();

            if($this->input->get_post('tipe')=='K'){
                $this->db->where('tr.id', $this->input->get_post('pk'));
                $this->db->where('s.status', true);
                $this->db->join('support.kegiatan_kr kr', 'kr.id=s.id');
                $this->db->join('master.tipe_resiko tr', 'kr.resiko=tr.id');
                $this->db->select('sum(s.jumlah) as total');
                $data['total']=$this->db->get('support.kegiatan_satker s')->row_array();
                $this->db->where('status', true);
                $data['kegiatan'] = $this->db->get('master.tipe_resiko')->result_array();

                $data['title']='Kegiatan';
            }
            elseif($this->input->get_post('tipe')=='P'){
                $this->db->where('tr.id', $this->input->get_post('pk'));
                $this->db->where('s.status', true);
                $this->db->join('support.program_kr kr', 'kr.id=s.id');
                $this->db->join('master.tipe_resiko tr', 'kr.resiko=tr.id');
                $this->db->select('sum(s.jumlah) as total');
                $data['total']=$this->db->get('support.program_satker s')->row_array();
                $this->db->where('status', true);
                $data['kegiatan'] = $this->db->get('master.tipe_resiko')->result_array();
                $this->db->where('support.kejadian_satker.status', true);
                $this->db->join('support.kegiatan_satker',"support.kejadian_satker.pk=support.kegiatan_satker.id and hitung='t' and support.kegiatan_satker.program_satker='".$this->input->get_post('pk')."'");
                $this->db->select('sum(support.kejadian_satker.jumlah) as kejadian');
                $data['tmbh_kejadian']=$this->db->get('support.kejadian_satker')->row('kejadian');

                $data['title']='Program';
            }else{
                $this->db->where('tr.id', $this->input->get_post('pk'));
                $this->db->where('s.status', true);
                $this->db->join('support.kegiatan_kr kr', 'kr.id=s.id');
                $this->db->join('master.tipe_resiko tr', 'kr.resiko=tr.id');
                $this->db->select('sum(s.jumlah) as total');
                $data['total_kegiatan']=$this->db->get('support.kegiatan_satker s')->row_array();
                $this->db->where('status', true);
                $data['kegiatan'] = $this->db->get('master.tipe_resiko')->result_array();
                $this->db->where('tr.id', $this->input->get_post('pk'));
                $this->db->where('s.status', true);
                $this->db->join('support.program_kr kr', 'kr.id=s.id');
                $this->db->join('master.tipe_resiko tr', 'kr.resiko=tr.id');
                $this->db->select('sum(s.jumlah) as total');
                $data['total_program']=$this->db->get('support.program_satker s')->row_array();

                $data['title']='Kegiatan & Program';
            }
//            echo json_encode($data['kejadian']);
//            echo 'kejadian';
//            echo json_encode($data['tmbh_kejadian']);
//            echo 'tambahan';
//            echo json_encode($data['total']);
//            echo 'total';die();
           $data['id_s']=$this->input->get_post('pk');
           $data['tipe']=$this->input->get_post('tipe');
//            echo $data['tipe'];die();
            $this->template->user('kejadian', $data, array('title' => 'Kejadian', 'breadcrumbs' => array('Laporan')));
        return;
        }
        $this->template->user('kejadian', $data, array('title' => 'Kejadian', 'breadcrumbs' => array('Laporan')));
    }
    public function risiko_p()
    {
            $menu = 'cb7ac7ac-9d00-4acd-a1fd-2afefb0c8a81';
        $data['access'] = list_access($menu);
        $this->db->order_by('nama');
        $this->db->where('status', true);
        $data['periode'] = $this->db->get('periode')->result_array();
        $this->db->order_by('nama');
        $this->db->where('status', true);
        $data['satker'] = $this->db->get('master.satker')->result_array();
        $this->db->order_by('nama','desc');
        $data['p_sekarang'] = $this->db->get('periode')->row_array();
        $data['periode_s'] = $this->input->get_post('periode') ? $this->input->get_post('periode') : $data['p_sekarang']['id'];

        if ($this->input->get_post('tipes')) {
            if($this->input->get_post('tipes')=='K'){
                $this->db->where('s.status', true);
                $this->db->where('s.periode', $this->input->get_post('periode'));
                $this->db->join('support.setting_kegiatan sk', 'sk.id=s.kegiatan','left');
                $this->db->join('support.kegiatan_kr kr', 'kr.id=s.id','left');
                $this->db->select('s.*,sk.nama as nama_s');
                $this->db->distinct();
                $kegiatan = $this->db->get('support.kegiatan_satker s')->result_array();
//                echo json_encode($kegiatan);die();
echo '<select class="select2 form-control select2-multiple" name="pk" data-placeholder="Pilih Kegiatan Satker">
                                <option value="All">All</option>';
foreach ($kegiatan as $v){
echo '<option value="'.$v['id'].'">'.$v['nama_s'].'</option>';
}
echo'</select>';return;
            }
            elseif($this->input->get_post('tipes')=='P'){
                $this->db->where('s.status', true);
                $this->db->where('s.periode', $this->input->get_post('periode'));
                $this->db->join('support.program_kr kr', 'kr.id=s.id','left');
                $this->db->join('support.setting_program sp', 'sp.id=s.kegiatan','left');
                $this->db->distinct();
                $this->db->select('s.*,sp.nama as nama_s');
                $kegiatan = $this->db->get('support.program_satker s')->result_array();
                echo '<select class="select2 form-control select2-multiple" name="pk" data-placeholder="Pilih Program Satker">
                                <option value="All">All</option>';
                foreach ($kegiatan as $v){
                    echo '<option value="'.$v['id'].'">'.$v['nama_s'].'</option>';
                }
                echo'</select>';
                return;
        }
        }
        elseif($this->input->get_post('cek')){

            if($this->input->get_post('tipe')=='K'){
                if($this->input->get_post('pk')!='All'){
                    $this->db->where('kr.id', $this->input->get_post('pk'));
                }
                $this->db->order_by('sp.nama');
                $this->db->where('sp.status', true);
                $this->db->join('support.kegiatan_kr kr', 'kr.id=s.id');
                $this->db->join('support.setting_kegiatan sp', 'sp.id=s.kegiatan','left');
                $this->db->distinct();
                $this->db->select('s.*,sp.nama as nama_s');
                $data['kejadian'] = $this->db->get('support.kegiatan_satker s')->result_array();

                $data['title']='Kegiatan';
            }
            elseif($this->input->get_post('tipe')=='P'){

                if($this->input->get_post('pk')!=''){
                    $this->db->where('kr.id', $this->input->get_post('pk'));
                }
                $this->db->order_by('sp.nama');
                $this->db->where('sp.status', true);
                $this->db->join('support.program_kr kr', 'kr.id=s.id');
                $this->db->join('support.setting_program sp', 'sp.id=s.kegiatan','left');
                $this->db->distinct();
                $this->db->select('s.*,sp.nama as nama_s');
                $data['kejadian'] = $this->db->get('support.program_satker s')->result_array();

                $data['title']='Program';
            }
//            echo json_encode($data['kejadian']);die();
           $data['id_s']=$this->input->get_post('pk');
           $data['tipe']=$this->input->get_post('tipe');
            $this->template->user('r_resiko', $data, array('title' => 'Ranking Resiko', 'breadcrumbs' => array('Laporan')));
        return;
        }
        $this->template->user('r_resiko', $data, array('title' => 'Ranking Resiko', 'breadcrumbs' => array('Laporan')));
    }


    public function lhp_o($page = 0)
    {
        $menu = 'a9aeebbb-d0d1-4640-9867-2024c2ca71bf';
        $data['access'] = list_access($menu);

        $this->db->order_by('kode');
        $data['akibat'] = $this->db->get('master.akibat')->result_array();

        if ($this->input->get_post('save')) {
            check_access($menu, 'c');
            $status = $this->audit->lhp_o();
            if (!$status) {
                $this->session->set_flashdata('status_update', $status);
                echo 500;
            } else {
                $this->session->set_flashdata('status_update', $status);
                echo 200;
            }
        }
        elseif ($this->input->get_post('id_test')) {
            check_access($menu, 'u');
            $status = $this->audit->update_lhp_o($this->input->get_post(null));
            if ($status) {
                $this->session->set_flashdata('status_update', $status);
            }
            redirect(base_url('laporan/lhp_o'));
            return;
        }
        elseif ($this->input->get_post('export')) {
            header("Content-Type: application/vnd.msword");
            header("Expires: 0");//no-cache
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");//no-cache
            header("content-disposition: attachment;filename=LHP_Online.doc");
            $this->db->where('id', $this->input->get_post('export'));
            $data=$this->db->get('interpro.lhpo')->row_array();

            $this->db->where('id', $data['id']);
            $rekom=$this->db->get('interpro.lhpo_rekom')->result_array();
            $this->db->where('id', $data['id']);
            $temuan=$this->db->get('interpro.lhpod')->result_array();
//            echo json_encode($array);
//            die();
            echo"
<div style='width: 100%; text-align: center'>
 <img src='".base_url('assets/logo_text.JPG')."'>
</div>
<table style='width: 100%;border: none'>
<tr>
<td style='width: 20%'></td>
<td style='width: 10%'></td>
<td style='width: 40%'></td>
<td  style='width: 30%;alignment: right'>
Banyuwangi, ".format_waktu($data['tgl_lhp'])."
</td>
</tr>
<tr>
<td>Nomor</td>
<td>:</td>
<td>".@$data['no_laporan']."</td>
<td>Kepada</td>
</tr>
<tr>
<td>Sifat</td>
<td>:</td>
<td>".@$data['jenis']."</td>
<td>Yth. ".@$data['ditujukan']."</td>
</tr>
<tr>
<td>Lampiran</td>
<td>:</td>
<td>".@$data['lampiran']."</td>
<td></td>
</tr>
<tr>
<td>Perihal</td>
<td>:</td>
<td>".@$data['hal']."</td>
<td></td>
</tr>
</table>
<p>".@$data['pendahuluan']."</p><br>
<h1><u><b>I. Dasar Pemeriksaan</b></u></h1><br>
<p>".@$data['dasar']."</p><br><br>
<h1><u><b>II. Tujuan Pemeriksaan</b></u></h1><br>
<p>".@$data['tujuan']."</p><br><br>
<h1><u><b>III. Ruang Lingkup Pemeriksaan</b></u></h1><br>
<p>".@$data['ruang']."</p><br><br>
<h1><u><b>IV. Batasan Pemeriksaan</b></u></h1><br>
<p>1. Pemeriksaan dilakukan untuk ".@$data['judul']."</p><br>
<p>2. ".@$data['pelaksanaan']."</p><br><br>
<h1><u><b>V. Metodologi Pemeriksaan</b></u></h1><br>
<p>".@$data['metodologi']."</p><br><br>
<h1><u><b>VI. Data Obyek Pemeriksaan</b></u></h1><br>
<h5><u><b>1. Organisasi Pelaksana</b></u></h5><br>
<p>".@$data['organisasi']."</p><br>
<h5><u><b>2. Perencanaan</b></u></h5><br>
<p>".@$data['keuangan']."</p><br><br>
<h1><u><b>VII. Temuan Hasil Pemeriksaan</b></u></h1><br>
<p>Berdasarkan hasil pemeriksaan terhadap ".@$data['judul']." terdapat beberapa hal untuk ditindaklanjuti yaitu ;</p><br><br>
<br>
";
            $no=1;
            foreach($temuan as $t){
                echo"
<h5><u><b>".$no." ".$t['judul_temuan']."</b></u></h5><br>
<p>".@$data['uraian_temuan']."</p><br><br>
";
                $inspektur= $this->db->get('inspektur')->row_array();
                $this->db->where('id', $data['id']);
                $this->db->where('urutan', $t['urutan']);
                $penyebab=$this->db->get('interpro.lhpo_penyebab')->result_array();
                $this->db->where('id', $data['id']);
                $this->db->where('urutan', $t['urutan']);
                $akibat=$this->db->get('interpro.lhpo_akibat')->result_array();
                $nop=1;
                $noa=1;
                if(count(json_encode($penyebab))>0){
                    echo"<p>Penyebab dari temuan ini dikarenakan</p><br><br>";
                foreach ($penyebab as $p){
                    echo $nop." ".$p['uraian_penyebab']."<br>";
                    $nop++;
                }
                }
                if(count(json_encode($penyebab))>0){
                    echo"<p>Akibatnya :</p><br><br>";
                foreach ($akibat as $a){
                    echo $noa." ".$a['uraian_akibat']."<br>";
                    $noa++;
                }
                }
                echo"<h5><u><b>Direkomendasikan </b></u></h5>
<p>".@$data['tanggapan']."</p><br><br>";
            }
            echo "
<h1><u><b>VIII. Hal-hal yang perlu diperhatikan</b></u></h1><br>
<p>".@$data['data']."</p><br><br>

<p>Demikian Laporan Hasil ".@$data['judul']." untuk dipergunakan sebagaimana mestinya.</p><br><br>
<table style='width: 100%;border: none'>
<tr>
<td style='width: 20%'></td>
<td style='width: 10%'></td>
<td style='width: 40%'></td>
<td  style='width: 30%;alignment: right'><h5><b>INSPEKTUR <br> KABUPATEN BANYUWANGI</b></h5>
</td>
</tr>
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td><h5><b>".@$inspektur['nama']." <br> NIP: ".@$inspektur['nip']."</b></h5></td>
</tr>
</table>
";
            return;
        }
        elseif ($this->input->get_post('baru')) {
            check_access($menu, 'u');

            $total_temuan=$this->input->get_post('no_temuan');

            $nomor=1;
            if(isset($_FILES["file1"])&&json_encode($_FILES["file1"]['size'])>0){
                for($i=1;$i<=$total_temuan;$i++){
                    for($j=0;$j<count($this->input->get_post("kr$nomor"));$j++){
                        if(isset($_FILES["file$nomor"])){

                            $_FILES['userfile']['name']     = $_FILES["file$nomor"]['name'];
                            $_FILES['userfile']['type']     = $_FILES["file$nomor"]['type'];
                            $_FILES['userfile']['tmp_name'] = $_FILES["file$nomor"]['tmp_name'];
                            $_FILES['userfile']['error']    = $_FILES["file$nomor"]['error'];
                            $_FILES['userfile']['size']     = $_FILES["file$nomor"]['size'];

                            $config['upload_path'] = './img/';
                            $config['allowed_types'] = '*';
                            $config['max_size'] = 1024 * 8;
                            $config['encrypt_name'] = TRUE;
                            $this->load->library('upload', $config);
                            if (!$this->upload->do_upload('userfile')) {
//                        echo json_encode($this->upload->display_errors());
//                        die();
                                redirect(base_url('laporan/lhp_o'));
                                return array('danger', $this->upload->display_errors());
                            } else {
                                $file = $this->upload->data();
                                $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);

                                $update = array(
                                    'file_tl' => (@$input['isian']?json_encode(@$input['isian']):null),
                                );
                            }
                        }
                    }

                    $nomor++;
                }
            }

            $status = $this->audit->insert_lhp_o($this->input->get_post(null));
            if ($status) {
                $this->session->set_flashdata('status_update', $status);
            }
            redirect(base_url('laporan/lhp_o'));
            return;
        }
        elseif ($this->input->get_post('baru_edit')) {
            check_access($menu, 'u');
            $status = $this->audit->insert_lhp_o_edit($this->input->get_post(null));
            if ($status) {
                $this->session->set_flashdata('status_update', $status);
            }
            redirect(base_url('laporan/lhp_o'));
            return;
        }
        elseif ($this->input->get_post('i')) {
//            check_access($menu, 'c');

            $this->template->user('addlhp_o', $data, array('title' => 'Laporan LHP Online', 'breadcrumbs' => array('Laporan', 'LHP Online')));
        }
        elseif ($this->input->get_post('rekom')) {
            $no=$this->input->get_post('rekom');
            check_access($menu, 'c');
            ?>

            <div class="form-group">
                <label for="inputName" class="col-sm-3 form-control-label right">Uraian Rekom<span class="text-danger">*</span></label>
                <div class="col-sm-5">
                    <input type="text" name="uk<?=$no?>[]" autocomplete="off" placeholder="Rekomendasi" required value="" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="inputName" class="col-sm-3 form-control-label right">Nilai Rekom<span class="text-danger">*</span></label>
                <div class="col-sm-5">
                    <input type="number" name="nilai_r<?=$no?>" autocomplete="off" placeholder="Nilai Rekomendasi" required value="" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="inputName" class="col-sm-3 form-control-label right">Uraian Tindak Lanjut<span class="text-danger">*</span></label>
                <div class="col-sm-5">
                    <input type="text" name="utl<?=$no?>[]" autocomplete="off" placeholder="Rekomendasi" required value="" class="form-control">
                </div>
            </div><div class="form-group">
                <label for="inputName" class="col-sm-3 form-control-label right">Nilai Tindak Lanjut<span class="text-danger">*</span></label>
                <div class="col-sm-5">
                    <input type="number" name="nilai_tl<?=$no?>" autocomplete="off" placeholder="Nilai Tindak Lanjut" required value="" class="form-control">
                </div>
            </div><div class="form-group">
                <label class="col-md-3 control-label">Upload File Temuan</label>
                <div class="col-md-7">
                    <input type="file" name="file<?=$no?>[]">
                </div>
            </div>
            <hr>
            <?php
            return;
        }
        elseif ($this->input->get_post('penyebab')) {
            $no=$this->input->get_post('penyebab');
            check_access($menu, 'c');

            ?>
            <div class="form-group">
                <label for="inputName" class="col-sm-3 form-control-label right">Uraian penyebab<span class="text-danger">*</span></label>
                <div class="col-sm-5">
                    <textarea class="form-control" class="mytextarea" name="up<?=$no?>[]" placeholder="Uraian penyebab" required><?=(@@$rencana['rekom'])?></textarea>
                </div>
            </div>
            <hr>
            <?php
            return;
        }
        elseif ($this->input->get_post('akibat')) {
            $no=$this->input->get_post('akibat');
            check_access($menu, 'c');

            ?>
            <div class="form-group">
                <label for="inputName" class="col-sm-3 form-control-label right">Uraian akibat<span class="text-danger">*</span></label>
                <div class="col-sm-5">
                    <textarea class="form-control" class="mytextarea" name="ap<?=$no?>[]" placeholder="Uraian akibat" required><?=(@@$rencana['rekom'])?></textarea>
                </div>
            </div>
            <hr>
            <?php
            return;
        }
        elseif ($this->input->get_post('temuan_baru')) {
            $no=$this->input->get_post('temuan_baru')+1;
            check_access($menu, 'c');
            ?>

            <div class="form-group <?=$no?>">
                <h3>TEMUAN</h3> <br>
                <div class="form-group">
                    <label for="inputName" class="col-sm-3 form-control-label right">Judul Temuan<span
                            class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <input  type="text" class="form-control" name="judul_t<?=$no?>" placeholder="Judul Temuan" required value="<?=@$rencana['judul_tl']?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputName" class="col-sm-3 form-control-label right">Uraian Temuan<span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <input type="text" name="ut<?=$no?>" autocomplete="off" placeholder="Temuan" required value="<?=(@@$rencana['rekom'])?>" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputName" class="col-sm-3 form-control-label right">Nilai Temuan<span
                            class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <input  type="number" class="form-control" name="nilai_tl<?=$no?>" placeholder="Nilai Temuan" required value="<?=@@$rencana['nilai_tl']?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputName" class="col-sm-3 form-control-label right">Kondisi<span
                            class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <textarea class="form-control" class="mytextarea" name="kondisi<?=$no?>" placeholder="Kondisi" required><?=@@$rencana['kondisi']?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-3 form-control-label right">Kriteria<span
                            class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <textarea class="form-control" class="mytextarea" name="kriteria<?=$no?>" placeholder="Kriteria" required><?=@$rencana['kriteria']?></textarea>
                    </div>
                </div>

                <h4>Penyebab <button type="button" onclick="tambah_penyebab(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h4><br>

                <div class="form-group penyebab<?=$no?>">

                    <div class="form-group">
                        <label for="inputName" class="col-sm-3 form-control-label right">Uraian penyebab<span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <textarea class="form-control" class="mytextarea" name="up<?=$no?>[]" placeholder="Uraian penyebab" required><?=(@@$rencana['rekom'])?></textarea>
                        </div>
                    </div>
                </div>
                <hr>

                <div class="form-group akibat<?=$no?>">

                    <div class="form-group">
                        <label for="inputName" class="col-sm-3 form-control-label right">Uraian akibat<span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <textarea class="form-control" class="mytextarea" name="ap<?=$no?>[]" placeholder="Uraian akibat" required></textarea>
                        </div>
                    </div>
                </div>


                <hr>

                <!--                                //rekom-->
                <h4>Rekomendasi Temuan <button type="button" onclick="tambah_rekom(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h4><br>

                <div class="form-group rekom<?=$no?>">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-3 form-control-label right">Uraian Rekom<span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <input type="text" name="uk<?=$no?>[]" autocomplete="off" placeholder="Rekomendasi" required value="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputName" class="col-sm-3 form-control-label right">Nilai Rekom<span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <input type="number" name="nilai_r<?=$no?>" autocomplete="off" placeholder="Nilai Rekomendasi" required value="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputName" class="col-sm-3 form-control-label right">Uraian Tindak Lanjut<span class="text-danger">*</span></label>
                        <div class="col-sm-5">

                            <input type="text" name="utl<?=$no?>[]" autocomplete="off" placeholder="Uraian Tindak Lanjut" required value="" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputName" class="col-sm-3 form-control-label right">Nilai Tindak Lanjut<span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <input type="number" name="nilai_tl<?=$no?>" autocomplete="off" placeholder="Nilai Tindak Lanjut" required value="" class="form-control">
                        </div>
                    </div><div class="form-group">
                        <label class="col-md-3 control-label">Upload File Temuan</label>
                        <div class="col-md-7">
                            <input type="file" name="file<?=$no?>[]">
                        </div>
                    </div>
                    <hr>
                </div>


                <div class="form-group">
                    <label for="inputName" class="col-sm-3 form-control-label right">Hal Yang Perlu diperhatikan<span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                        <input type="text" name="tang<?=$no?>" autocomplete="off" placeholder="Hal Yang Perlu diperhatikan" required value="<?=(@@$rencana['tanggapan'])?>" class="form-control">
                    </div>
                </div>
            </div>
            <?php
            return;
        }
        elseif ($this->input->get_post('del')) {
            $id_rencana = $this->input->get_post('del');

            $this->db->trans_begin();
            $this->db->where('id', $id_rencana);
            $this->db->delete('interpro.lhpo');
            $this->db->where('id', $id_rencana);
            $this->db->delete('interpro.lhpod');
            $this->db->where('id', $id_rencana);
            $this->db->delete('interpro.lhpo_rekom');
            $this->db->where('id', $id_rencana);
            $this->db->delete('interpro.lhpo_akibat');
            $this->db->where('id', $id_rencana);
            $this->db->delete('interpro.lhpo_penyebab');
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                redirect(base_url('laporan/lhp_o'));
                return array('danger', 'Laporan LHP Gagal Dihapus');
            } else {
                $this->db->trans_commit();
                redirect(base_url('laporan/lhp_o'));
                return array('success', 'Laporan LHP Berhasil Dihapus');
            }
            return;
        }
        elseif ($this->input->get_post('v')) {
            $id_rencana = $this->input->get_post('v');

            $user = $this->session->userdata('user');

            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['penyebab'] = $this->db->get('master.penyebab')->result_array();
            $this->db->order_by('nama');
            $this->db->where('status', true);
            $data['tl'] = $this->db->get('master.tindak_lanjut')->result_array();
            $this->db->where('id', $id_rencana);
            $data['rencana'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('tl.id_rencana', $id_rencana);
            $this->db->join('interpro.tindak_lanjut tl', 'tld.id=tl.id', 'left');
            $this->db->select('tld.*');
            $data['temuan_list'] = $this->db->get('interpro.tindak_lanjutd tld')->result_array();
            $this->db->where('tl.id_rencana', $id_rencana);
            $this->db->join('interpro.tindak_lanjut tl', 'tlr.id=tl.id', 'left');
            $this->db->select('tlr.*');
            $data['rekom_list'] = $this->db->get('interpro.tindak_lanjut_rekom tlr')->result_array();
            $this->db->where('tl.id_rencana', $id_rencana);
            $this->db->join('interpro.tindak_lanjut tl', 'tlr.id=tl.id', 'left');
            $this->db->select('tlr.*');
            $data['penyebab_list'] = $this->db->get('interpro.tindak_lanjut_penyebab tlr')->result_array();
            $this->db->where('tl.id_rencana', $id_rencana);
            $this->db->join('interpro.tindak_lanjut tl', 'tlr.id=tl.id', 'left');
            $this->db->select('tlr.*');
            $data['akibat_list'] = $this->db->get('interpro.tindak_lanjut_akibat tlr')->result_array();

            $id_rencana2 = '';
            if($data['rencana']['id']==''){
                $this->db->where('id', $id_rencana);
                $data['rencana'] = $this->db->get('interpro.tindak_lanjut')->row_array();
                $id_rencana2=$id_rencana;
                $data['id_rencana'] ='';
                $data['jml_temuan'] =@$data['rencana']['jml_temuan']!=''?$data['rencana']['jml_temuan']:0;
                $data['id_tindak'] =$id_rencana;
                $this->db->where('tl.id', $id_rencana);
                $this->db->join('interpro.tindak_lanjut tl', 'tld.id=tl.id', 'left');
                $this->db->select('tld.*');
                $data['temuan_list'] = $this->db->get('interpro.tindak_lanjutd tld')->result_array();
                $this->db->where('tl.id', $id_rencana);
                $this->db->join('interpro.tindak_lanjut tl', 'tlr.id=tl.id', 'left');
                $this->db->select('tlr.*');
                $data['rekom_list'] = $this->db->get('interpro.tindak_lanjut_rekom tlr')->result_array();
                $this->db->where('tl.id', $id_rencana);
                $this->db->join('interpro.tindak_lanjut tl', 'tlr.id=tl.id', 'left');
                $this->db->select('tlr.*');
                $data['penyebab_list'] = $this->db->get('interpro.tindak_lanjut_penyebab tlr')->result_array();

            }

//            echo json_encode($data['rekom_list']);die();
            $this->db->where('tl.id_rencana', $id_rencana);
            $this->db->join('interpro.tindak_lanjut tl', 'tlp.id=tl.id', 'left');
            $data['tl_penyebab'] = $this->db->get('interpro.tindak_lanjut_penyebab tlp')->result_array();

            $this->db->where('tl.id_rencana', $id_rencana);
            $this->db->join('interpro.tindak_lanjut tl', 'tlr.id=tl.id', 'left');
            $data['tl_rekom'] = $this->db->get('interpro.tindak_lanjut_rekom tlr')->result_array();
            $this->db->where('status', true);
            $data['tlb'] = $this->db->get('master.tindak_lanjut')->result_array();


            $this->db->where('user', $user['id']);
            $this->db->where('role', '108ce450-d740-4bcb-9aaa-be9504bf94d8');
            $this->db->join('role', 'roleusers.role=role.id');
            $this->db->select('roleusers.role');
            $data['dalnis'] = $this->db->get('roleusers')->num_rows();
            $this->db->where('user', $user['id']);
            $this->db->where('role', '2332800e-edb0-49cd-92d7-1abc3d981207');
            $this->db->join('role', 'roleusers.role=role.id');
            $this->db->select('roleusers.role');
            $data['irban'] = $this->db->get('roleusers')->num_rows();

            if($id_rencana2!=''){

                $this->db->where('t.id', $id_rencana);
            }else{
                $this->db->where('id_rencana', $id_rencana);
            }
            $this->db->join('users u', 't.app_sv1=u.id','left');
            $this->db->join('users u2', 't.app_sv2=u2.id','left');
            $this->db->select('t.*,u.name as dalnis_n,u2.name as irban_n');
            $data['tl'] = $this->db->get('interpro.tindak_lanjut t')->row_array();
//            echo json_encode($data['tl']);die();
//            echo $data['irban'];
            $this->db->where('interpro.perencanaan.id', $id_rencana);
            $this->db->join('users', 'interpro.perencanaan.dalnis=users.id');
            $data['dalnis_nama'] = $this->db->get('interpro.perencanaan')->row('name');
            $this->db->where('interpro.perencanaan.id', $id_rencana);
            $this->db->join('users', 'interpro.perencanaan.ketuatim=users.id');
            $data['ketuatim_nama'] = $this->db->get('interpro.perencanaan')->row('name');
            $this->db->where('interpro.perencanaan.id', $id_rencana);
            $this->db->join('users', 'interpro.perencanaan.irban=users.id');
            $data['irban_nama'] = $this->db->get('interpro.perencanaan')->row('name');

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(sasaran)) as v');
            $raw = $this->db->get('interpro.perencanaan_sasaran')->row_array();
            $data['rencana']['sasaran'] = $raw['v'];

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(tujuan)) as v');
            $raw = $this->db->get('interpro.perencanaan_tujuan')->row_array();
            $data['rencana']['tujuan'] = $raw['v'];

            $this->db->order_by('no', 'asc');
            $this->db->where('status', true);
            $data['pkpt'] = $this->db->get('master.pkpt')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['jenis'] = $this->db->get('master.jenis')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();

            $this->db->order_by('sasaran.order,sasaran.kode', 'asc');
            $this->db->where('sasaran.status', true);
            $data['sasaran'] = $this->db->get('master.sasaran')->result_array();

            $this->db->order_by('tujuan.order,tujuan.kode', 'asc');
            $this->db->where('tujuan.status', true);
            $data['tujuan'] = $this->db->get('master.tujuan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['aturan'] = $this->db->get('support.aturan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['pedoman'] = $this->db->get('support.pedoman')->result_array();

            //            $this->db->order_by('v_tao.tahapan,v_tao.kode_kk', 'asc');
            //            $this->db->where('v_tao.status_tao', true);
            //            $this->db->where('v_tao.status_kk', true);
            //            $this->db->join('interpro.perencanaan_detail pd', "v_tao.id=pd.tao AND pd.id='$id_rencana'", 'left');
            //            $this->db->select("v_tao.*,pd.jumlah_hari as jumlah,pd.rencana_by as pelaksana,to_char(pd.rencana_date,'dd/mm/yyyy') as pelaksanaan");
            //            $data['tao'] = $this->db->get('support.v_tao')->result_array();

            $this->db->order_by('kode', 'asc');
            $data['tahapan'] = $this->db->get('master.tahapan_audit')->result_array();

            $data['tim'] = $this->db->get('support.tim()')->result_array();

            $this->db->select('id,name as nama');
            $this->db->where('status', true);
            $data['listtim'] = $this->db->get('public.users')->result_array();

            $this->db->where('status', true);
            $data['periode'] = $this->db->get('public.periode')->result_array();
//echo $id_rencana2;die();

            if($id_rencana2!=''){

                $this->db->where('tl.id', $id_rencana);
            }else{
                $this->db->where('tl.id_rencana', $id_rencana);
            }
            $this->db->order_by('tld.id', 'asc');
            $this->db->join('support.kode_temuan kt', 'tld.kode_temuan=kt.id');
            $this->db->join('support.kode_temuan kt2', 'tld.kode_rekom=kt2.id');
            $this->db->join('interpro.tindak_lanjut tl', 'tld.id=tl.id', 'left');
            $this->db->select("kt.id as id_t,kt2.id as id_r,(kt.k1||'.'||kt.k2||'.'||kt.k3||'.'||kt.k4) as kode_t,(kt2.k1||'.'||kt2.k2||'.'||kt2.k3||'.'||kt2.k4) as kode_r,kt.nama as temuan,kt2.nama as rekom, tld.tindak_lanjut as tl,  tld.hasil_rekom as kode_hr,  tld.keterangan as keterangan,  tld.nilai_rekom as nilai_r, ,  CASE WHEN tld.hasil_rekom=1 THEN 'Sesuai' WHEN tld.hasil_rekom=2 THEN 'Belum Sesuai' WHEN tld.hasil_rekom=3 THEN 'Belum Ditindaklanjuti' ELSE 'Tidak Dapat Ditindaklanjuti' END as hasil_r, tld.negara as nk1, tld.daerah as nk2, tld.nd as nd, tld.obrik, tld.akibat, tld.penyebab, tld.kondisi, tld.kriteria, tld.tujuan_tl, tld.evaluasi, tld.ruang, tld.tanggapan, tld.tindak_lanjut_s, tld.judul_tl, tld.sasaran_tl");
            $data['temuan'] = $this->db->get('interpro.tindak_lanjutd tld')->result_array();

            $this->db->order_by('k1,k2,k3,k4', 'asc');
            $this->db->where('level', 4);
            $this->db->where('k1 <>', '9');
            $this->db->where('status', true);
            $this->db->where('deleted', false);
            $this->db->select("support.kode_temuan.id,support.kode_temuan.nama,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
            $data['parent'] = $this->db->get('support.kode_temuan')->result_array();

            $this->db->order_by('k1,k2,k3,k4', 'asc');
            $this->db->where('level', 4);
            $this->db->where('k1', '9');
            $this->db->where('status', true);
            $this->db->where('deleted', false);
            $this->db->select("support.kode_temuan.id,support.kode_temuan.nama,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
            $data['parent2'] = $this->db->get('support.kode_temuan')->result_array();

            $this->template->user('viewtl', $data, array('title' => 'Lihat Temuan', 'breadcrumbs' => array('Internal Proses', 'Temuan & Tindak Lanjut')));
        }
        else {
            $data['search'] = $this->input->get_post('search') ? $this->input->get_post('search') : '';
            if ($this->input->get_post('start') && $this->input->get_post('end')) {
                $data['start'] = $this->input->get_post('start');
                $data['end'] = $this->input->get_post('end');
            } else {
                $data['start'] = date('d/m/Y', strtotime('-1 months'));;
                $data['end'] = date('d/m/Y', strtotime('+1 months'));
            }
            $start = format_waktu($data['start'], true);
            $end = format_waktu($data['end'], true);
            $perpage = 100;
            if ($data['search']!='') {
                $this->db->where('judul', $data['search']);
            }
            $this->db->where('status', 't');
            $this->db->where('created_at >= ', $start);
            $this->db->where('created_at <= ', $end);
            $total = $this->db->get('interpro.lhpo')->num_rows();
            $this->db->limit($perpage, $page);
            $this->db->order_by('tgl_laporan');
            if ($data['search']!='') {
                $this->db->where('judul', $data['search']);
            }
            $this->db->distinct();
            $data['rencana'] = $this->db->get("interpro.lhpo_v1('$start ','$end')")->result_array();
//            echo json_encode($data['rencana']);die();

            $data['pagination'] = $this->template->pagination('laporan/lhpo', $total, $perpage);
            $data['page'] = $page;
            $this->template->user('lhpo', $data, array('title' => 'LHP Online', 'breadcrumbs' => array('Laporan')));
        }
    }
        public function lhp($page = 0)
    {
        $menu = '97c15763-d7b1-4846-9fc9-f5a36dfa5468';
        $data['access'] = list_access($menu);
        $this->db->order_by('nama','desc');
        $this->db->where('status','t');
        $this->db->select('nama as tahun');
        $data['list_tahun'] = $this->db->get('public.periode')->result_array();
        $data['tahun'] = @$this->input->get_post('tahun')?$this->input->get_post('tahun'):date('Y');

        if ($this->input->get_post('lhp_edit')) {
            check_access($menu, 'u');
            $data['id_edit']=$this->input->get_post('lhp_edit');
            $data['rencana']['id']=$data['id_edit'];
            $this->db->where('id', $this->input->get_post('lhp_edit'));
            $data['edit']= $this->db->get('interpro.lhp')->row_array();

            $this->db->order_by('k1,k2,k3,k4', 'asc');
            $this->db->where('level', 4);
            $this->db->where('k1 <>', '9');
            $this->db->where('status', true);
            $this->db->where('deleted', false);
            $this->db->select("support.kode_temuan.id,support.kode_temuan.nama,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
            $data['parent'] = $this->db->get('support.kode_temuan')->result_array();

            $this->db->order_by('k1,k2,k3,k4', 'asc');
            $this->db->where('level', 4);
            $this->db->where('k1', '9');
            $this->db->where('status', true);
            $this->db->where('deleted', false);
            $this->db->select("support.kode_temuan.id,support.kode_temuan.nama,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
            $data['parent2'] = $this->db->get('support.kode_temuan')->result_array();

            $this->db->where('id', $this->input->get_post('lhp_edit'));
            $data['no_edit']= $this->db->get('interpro.v_perencanaan')->row_array();
            $this->template->user('addlhp', $data, array('title' => 'Tambah LHP - '.$data['no_edit']['spt_no'], 'breadcrumbs' => array('Laporan')));
       return;
        }
        elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');

            $this->template->user('addlhp_o', $data, array('title' => 'Laporan LHP Online', 'breadcrumbs' => array('Laporan', 'LHP Online')));
        }
        elseif($this->input->get_post('edit')){

            $tgl_start=explode('/',$this->input->get_post('tgl_lhp'));
            $tgl_m=$tgl_start[2].'-'.$tgl_start['1'].'-'.$tgl_start['0'];
            $users = array(
                'no' => $this->input->get_post('no_lhp'),
                'id_rencana' => $this->input->get_post('spt'),
                'tgl' => $tgl_m,
            );
            $this->db->where('no', $this->input->get_post('no_lhp'));
            $total = $this->db->get('interpro.lhp')->num_rows();
            if($total>1){
                redirect(base_url('laporan/lhp'));
                return array('danger', 'No LHP Gagal Disimpan Karena sudah ada');
            }else{
                $this->db->trans_begin();
                $this->db->where('id', $this->input->get_post('id_lhp'));
                $this->db->update('interpro.lhp', $users);
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    redirect(base_url('laporan/lhp'));
                    return array('danger', 'No LHP Gagal Disimpan');
                } else {
                    $this->db->trans_commit();
                    redirect(base_url('laporan/lhp'));
                    return array('success', 'No LHP Berhasil Disimpan');
                }
            }
        }
        elseif($this->input->get_post('no_lhp')){
            $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
            $id = $raw['id'];

            $tgl_start=explode('/',$this->input->get_post('tgl_lhp'));
            $tgl_m=$tgl_start[2].'-'.$tgl_start['1'].'-'.$tgl_start['0'];
            $users = array(
                'id' => $id,
                'no' => $this->input->get_post('no_lhp'),
                'id_rencana' => $this->input->get_post('spt'),
                'tgl' => $tgl_m,
            );
            $this->db->where('no', $this->input->get_post('no_lhp'));
            $total = $this->db->get('interpro.lhp')->num_rows();
            if($total>1){
                redirect(base_url('laporan/lhp'));
                return array('danger', 'No LHP Gagal Disimpan Karena sudah ada');
            }else{
            $this->db->trans_begin();
            $this->db->insert('interpro.lhp', $users);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                redirect(base_url('laporan/lhp'));
                return array('danger', 'No LHP Gagal Disimpan');
            } else {
                $this->db->trans_commit();
                redirect(base_url('laporan/lhp'));
                return array('success', 'No LHP Berhasil Disimpan');
            }
            }
        }
        elseif($this->input->get_post('query')){
                $this->db->limit(100);
                $this->db->order_by('spt_no','asc');
                $this->db->where('status',true);
                $this->db->like('lower(spt_no)',strtolower($this->input->get_post('query')));
                $this->db->select("id as id,spt_no");
                $data=$this->db->get('interpro.perencanaan')->result_array();
            echo json_encode($data);
            return;
        }
        else {
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : (isset($_GET['closed']) ? '' : 'f');
            $data['search'] = $this->input->get_post('search') ? $this->input->get_post('search') :'';
            $perpage = 100;
            $this->db->where('proses', 3);
            $this->db->where('EXTRACT(year FROM tgl) =', $data['tahun']);
            $this->db->join('interpro.v_perencanaan v','l.id_rencana=v.id');
            $this->db->select('v.*, l.id as id_lhp, l.no,l.id_rencana,l.tgl');
            $total = $this->db->get('interpro.lhp l')->num_rows();

            $this->db->where('no_laporan is null');
            $this->db->where('proses', 3);
            $data['rencana'] = $this->db->get('interpro.v_perencanaan')->result_array();

            $this->db->where('proses', 3);
            $this->db->where('EXTRACT(year FROM tgl) =', $data['tahun']);
            if($data['search']!=''){
                $this->db->where("(v.spt_no like '%$data[search]%' or l.no like '%$data[search]%' or v.judul like '%$data[search]%')");
            }

            $this->db->join('interpro.v_perencanaan v','l.id_rencana=v.id');
            $this->db->select('v.*, l.id as id_lhp, l.no,l.id_rencana,l.tgl');
            $data['lhp'] = $this->db->get('interpro.lhp l')->result_array();

            $data['pagination'] = $this->template->pagination('laporan/lhp', $total, $perpage);

            $data['page'] = $page;

            $this->template->user('lhp', $data, array('title' => 'Monitoring LHP', 'breadcrumbs' => array('Laporan')));
        }
    }

    public function per_user()
    {
        $data['tahun'] = $this->db->get('master.v_pkpt')->result_array();
        if ($this->input->get_post('tahun')) {
            $data['selected_tahun'] = $this->input->get_post('tahun');
        } else {
            $data['selected_tahun'] = @$data['tahun'][0]['tahun'];
        }

        $menu = 'e741e54c-9c5f-454e-a2f9-a38b8806091b';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        $data['user'] = $this->user->get_list_user_kinerja($data['selected_tahun']);
        $this->template->user('laporan_user', $data, array('title' => 'Laporan Kinerja', 'breadcrumbs' => array('Laporan')));
    }
}
