<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
    private $data;
    public function __construct()
    {
        parent::__construct();
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");
        $header=$_SERVER;
//        $this->key=@$header['HTTP_AUTHORIZATION'];
//        $header=getallheaders();
//        $this->data=json_decode(gradinDecrypt(@$header['data']),TRUE);
        $this->data=json_decode(gradinDecrypt(@$header['HTTP_DATA']),TRUE);
        if('Bearer '.md5($this->config->item('id'))!=@@$header['HTTP_AUTHORIZATION'] || !isset($this->data['action'])){
//        if('Bearer '.md5($this->config->item('id'))!=@$header['Authorization'] || !isset($this->data['action'])){
            exit('403');
        }
    }
    public function index(){
        error_reporting(0);
        switch ($this->data['action']){
            case 'clearsession':
                $this->db->truncate('sessions');
                echo json_encode(array('status'=>200));
                break;
            default:
                echo json_encode(array('status'=>404));
                break;
        }
        $this->session->sess_destroy();
    }

}
