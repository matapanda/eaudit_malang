<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        if (get_authority('17c15763-d7b1-4846-9fc9-f5a36dfa5468')) {
            $this->db->order_by('nama','desc');
            $this->db->where('status','t');
            $this->db->select('nama as tahun');
            $data['list_tahun'] = $this->db->get('public.periode')->result_array();
            $data['tahun'] = @$this->input->get_post('tahun')?$this->input->get_post('tahun'):date('Y');

            $this->db->where('status', true);
            $this->db->where('id not in (select pkpt_no::uuid from interpro.perencanaan WHERE pkpt_no <> \'\' and EXTRACT(year FROM spt_date) ='.$data['tahun'].')', null, false);
            $this->db->select('count(id) as total');
            $data['tidak'] = $this->db->get('master.pkpt')->row_array();

            //baruu skala dampak dan resiko

            $this->db->where('nama',$data['tahun']);
            $periode = $this->db->get('public.periode')->row_array();
//            $this->db->where('periode', $periode['id']);
            $this->db->where('kr.sk <>',0);
            $this->db->where('kr.sd <>',0);
            $this->db->select('ks.id,(kr.sk) as tot,(kr.sd) as tot2,tr.nama');
                $this->db->join('master.kegiatan k', "k.id=ks.kegiatan");
                $this->db->join('support.kegiatan_kr kr', "kr.id=ks.id");
                $this->db->join('master.tipe_resiko tr', "tr.id=kr.resiko");
            $data['risk'] = $this->db->get("support.kegiatan_satker ks")->result_array();
            //end baru

            //awal baru
            $this->db->join('support.kegiatan_satker k', 'mk.id=k.kegiatan', 'left');
            $this->db->join('support.kegiatan_kr kr', 'kr.id=k.id', 'left');
            $this->db->group_by('k.jks,k.jka,k.jkb,mk.nama');
            $this->db->select('sum(k.jumlah) as jumlah,sum(kr.sd) as sd,k.jks,k.jka,k.jkb,mk.nama as nama_keg,sum(kr.jml) as jml,sum(kr.real) as real');
            $data['satker'] = $this->db->get("master.kegiatan mk")->result_array();

            $this->db->where('status', true);
            $this->db->where('id in (select pkpt_no::uuid from interpro.perencanaan WHERE pkpt_no <> \'\' and EXTRACT(year FROM spt_date) ='.$data['tahun'].')', null, false);
            $this->db->select('count(id) as total');
            $data['sesuai'] = $this->db->get('master.pkpt')->row_array();

            $this->db->where('EXTRACT(year FROM interpro.perencanaan.spt_date) =', $data['tahun']);
            $this->db->where('no_laporan is not null');
            $this->db->where('proses', 3);
            $this->db->select('count(id) as total');
            $data['lhp_sesuai'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('EXTRACT(year FROM interpro.perencanaan.spt_date) =', $data['tahun']);
            $this->db->where('no_laporan is null');
            $this->db->where('proses', 3);
            $this->db->select('count(id) as total');
            $data['lhp_tidak'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('EXTRACT(year FROM interpro.perencanaan.spt_date) =', $data['tahun']);
            $this->db->where('proses <>', 0);
            $this->db->select('count(id) as total');
            $data['perencanaan'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('EXTRACT(year FROM interpro.perencanaan.spt_date) =', $data['tahun']);
            $this->db->select('count(id) as total');
            $data['perencanaan_all'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('EXTRACT(year FROM interpro.perencanaan.spt_date) =', $data['tahun']);
            $this->db->where('proses >=', 3);
            $this->db->select('count(id) as total');
            $data['pelaksanaan'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('EXTRACT(year FROM interpro.perencanaan.spt_date) =', $data['tahun']);
            $this->db->where('proses >=', 4);
            $this->db->select('count(id) as total');
            $data['pelaporan'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->order_by("realisasi,jumlah", 'desc');
            $data['kinerja'] = $this->db->get("interpro.kinerja('$data[tahun]')")->result_array();

            $this->template->user('dashboard', $data, array('title' => 'Dashboard'));

        } else {
            $this->notif();
        }
    }

    public function test2()
    {
        $url = 'http://akuntan.salvition.com/welcome/test2';
        $data['config']=$this->session->userdata('config');
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_POSTFIELDS => ['input'=>json_encode($data['config'])],
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
        ));
        $text = curl_exec($curl);
        echo json_encode($text);die();
        $hasil=json_decode($text,true);
        echo json_encode($text);return;
        curl_close($curl);
        if($hasil['status']==true &&$hasil['comp']!=''){
            echo't';die();
        }else{
            echo'f';die();
        }
    }
    public function notif()
    {
        if ($this->input->get_post('id')) {
            $link = base_url();
            $this->db->where('readed_at is null', null, false);
            $this->db->where('id', $this->input->get_post('id'));
            $this->db->update('support.notif', array('readed_at' => date('Y-m-d H:i:s') . "+07"));
            $this->db->where('id', $this->input->get_post('id'));
            $data = $this->db->get('support.notif')->row_array();
            switch ($data['type']) {
                case 'rencana':
                    $link .= "internalproses/pelaksanaan?i=$data[reff_id1]";
                    break;
            }
            redirect($link);
        }
        $user = $this->session->userdata('user');
        $this->db->limit(100);
        $this->db->order_by('created_at', 'desc');
        $this->db->where('id_user', $user['id']);
        $data['notif'] = $this->db->get("support.notif")->result_array();
        $this->template->user('notif', $data, array('title' => 'Pemberitahuan'));
    }

    public function haram()
    {
        $this->template->guest('haram', array(), array('title' => '403 Akses Haram'));
    }
}
